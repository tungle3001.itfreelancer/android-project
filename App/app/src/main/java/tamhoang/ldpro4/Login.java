package tamhoang.ldpro4;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Xml;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Login extends AppCompatActivity {
    public static String Imei = null;
    public static String serial = null;
    Database db;
    Intent intent;
    Button login;

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.db = new Database(this);
        this.login = (Button) findViewById(R.id.btn_login);
        final int[] contact = {ContextCompat.checkSelfPermission(this, "android.permission.READ_CONTACTS")};
        final int[] storage = {ContextCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE")};
        final int[] receive_sms = {ContextCompat.checkSelfPermission(this, "android.permission.RECEIVE_SMS")};
        final int[] send_sms = {ContextCompat.checkSelfPermission(this, "android.permission.SEND_SMS")};
        final int[] readCard = {ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE")};
        if (contact[0] == -1 || storage[0] == -1 || receive_sms[0] == -1 || readCard[0] == -1 || send_sms[0] == -1) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.INTERNET", "android.permission.READ_CONTACTS", "android.permission.RECEIVE_SMS", "android.permission.SEND_SMS", "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
        }
        this.login.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Login.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                contact[0] = ContextCompat.checkSelfPermission(Login.this, "android.permission.READ_CONTACTS");
                storage[0] = ContextCompat.checkSelfPermission(Login.this, "android.permission.READ_PHONE_STATE");
                receive_sms[0] = ContextCompat.checkSelfPermission(Login.this, "android.permission.RECEIVE_SMS");
                send_sms[0] = ContextCompat.checkSelfPermission(Login.this, "android.permission.SEND_SMS");
                readCard[0] = ContextCompat.checkSelfPermission(Login.this, "android.permission.WRITE_EXTERNAL_STORAGE");
                if (contact[0] != 0 || storage[0] != 0 || receive_sms[0] != 0 || readCard[0] != 0 || send_sms[0] != 0) {
                    ActivityCompat.requestPermissions(Login.this, new String[]{"android.permission.INTERNET", "android.permission.READ_CONTACTS", "android.permission.RECEIVE_SMS", "android.permission.SEND_SMS", "android.permission.READ_PHONE_STATE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
                } else if (Login.this.getImei() != null) {
                    Login.this.intent = new Intent(Login.this, MainActivity.class);
                    Login login = Login.this;
                    login.startActivities(new Intent[]{login.intent});
                }
            }
        });
        try {
            Create_Table_database();
            if (contact[0] == 0 && storage[0] == 0 && receive_sms[0] == 0 && readCard[0] == 0 && send_sms[0] == 0 && getImei() != null) {
                Intent intent = new Intent(this, MainActivity.class);
                this.intent = intent;
                startActivities(new Intent[]{intent});
            }
        } catch (SQLException e) {
        }
    }

    public String getImei() {
        if (ActivityCompat.checkSelfPermission(this, "android.permission.READ_PHONE_STATE") != -1) {
            try {
                TelephonyManager telemamanger = (TelephonyManager) getSystemService("phone");
                Imei = telemamanger.getDeviceId();
                serial = Settings.Secure.getString(getContentResolver(), "android_id");
            } catch (Exception e) {
                e.getMessage();
            }
        }
        if (Imei != null) {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                serializer.setOutput(writer);
                serializer.startDocument("UTF-8", true);
                String result = Imei;
                FileOutputStream fos = openFileOutput("new.xml", 0);
                fos.write(result.getBytes(), 0, result.length());
                fos.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
                Toast.makeText(this, "Loi tao file", 0).show();
            }
        } else {
            try {
                FileInputStream fis = openFileInput("new.xml");
                Imei = "";
                while (true) {
                    int c = fis.read();
                    if (c == -1) {
                        break;
                    }
                    Imei += ((char) c);
                }
            } catch (FileNotFoundException e4) {
                checkDefaultSettings();
                e4.printStackTrace();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        }
        return Imei;
    }

    public AlertDialog.Builder showAlertBox(String title, String message) {
        return new AlertDialog.Builder(this).setTitle(title).setMessage(message);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, "android.permission.READ_SMS") != 0 && !ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.READ_SMS")) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.READ_SMS"}, 1);
        }
    }

    public boolean checkDefaultSettings() {
        if (Build.VERSION.SDK_INT < 19) {
            return false;
        }
        if (Telephony.Sms.getDefaultSmsPackage(getApplicationContext()).equals(getApplicationContext().getPackageName())) {
            return true;
        }
        showAlertBox("Cài đặt mặc định!", "Để ứng dụng thành quản lý tin nhắn mặc định để quản lý tin nhắn tốt hơn!").setPositiveButton("Ok", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Login.3
            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent("android.provider.Telephony.ACTION_CHANGE_DEFAULT");
                intent.putExtra("package", Login.this.getPackageName());
                Login.this.startActivity(intent);
                Login.this.checkPermissions();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Login.2
            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show().setCanceledOnTouchOutside(false);
        return false;
    }

    public void Create_Table_database() {
        this.db.Creat_TinNhanGoc();
        this.db.Create_table_Chat();
        this.db.Creat_SoCT();
        this.db.Creat_So_Om();
        this.db.List_Khach_Hang();
        this.db.Bang_KQ();
        this.db.ThayThePhu();
        this.db.Another_setting();
        this.db.Creat_Chaytrang_acc();
        this.db.Creat_Chaytrang_ticket();
        try {
            Cursor c = this.db.GetData("Select * From so_om");
            if (c.getCount() < 1) {
                for (int i = 0; i < 10; i++) {
                    String str = "Insert into so_om Values (null, '0" + i + "', 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null)";
                    this.db.QueryData(str);
                }
                for (int i2 = 10; i2 < 100; i2++) {
                    String str2 = "Insert into so_om Values (null, '" + i2 + "', 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null)";
                    this.db.QueryData(str2);
                }
            }
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (SQLException e) {
        }
        try {
            Cursor cursor = this.db.GetData("Select Om_Xi3 FROM So_om WHERE So = '05'");
            cursor.moveToFirst();
            if (cursor.getInt(0) == 0) {
                this.db.QueryData("UPDATE So_om SET Om_Xi3 = 18, Om_Xi4 = 15 WHERE So = '05'");
            }
        } catch (SQLException e2) {
        }
    }
}
