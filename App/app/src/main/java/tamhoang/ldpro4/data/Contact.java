package tamhoang.ldpro4.data;

import android.app.PendingIntent;
import android.app.RemoteInput;
import android.os.Bundle;

/* loaded from: classes2.dex */
public class Contact {
    public String app;
    public String name;
    public PendingIntent pendingIntent;
    public Bundle remoteExtras;
    public RemoteInput remoteInput;

    public String getName() {
        return this.name;
    }

    public String getApp() {
        return this.app;
    }

    public PendingIntent getPendingIntent() {
        return this.pendingIntent;
    }

    public RemoteInput getRemoteInput() {
        return this.remoteInput;
    }

    public Bundle getRemoteExtras() {
        return this.remoteExtras;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public void setPendingIntent(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
    }

    public void setRemoteInput(RemoteInput remoteInput) {
        this.remoteExtras = this.remoteExtras;
    }

    public void setRemoteExtras(Bundle remoteExtras) {
        this.remoteExtras = remoteExtras;
    }
}
