package tamhoang.ldpro4.data;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.internal.view.SupportMenu;
import android.telephony.SmsManager;
import java.io.File;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.NotificationReader;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.constants.Constants;

/* loaded from: classes2.dex */
public class Database extends SQLiteOpenHelper {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    protected static SQLiteDatabase db;
    JSONObject caidat_gia;
    JSONObject caidat_tg;
    JSONObject json;
    JSONObject jsonDanSo;
    JSONObject json_Tralai;
    public String[][] mang;
    private Context mcontext;

    public Database(Context context) {
        super(context, Environment.getExternalStorageDirectory() + File.separator + "ldpro", (SQLiteDatabase.CursorFactory) null, 1);
        this.mcontext = context;
    }

    public void SendSMS(String Sdt, String Mess) {
        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(Mess);
        sms.sendMultipartTextMessage(Sdt, null, parts, null, null);
    }

    public void Update_TinNhanGoc(int id, int type_kh) throws JSONException {
        Cursor c = GetData("Select * From tbl_tinnhanS WHERE id = " + id);
        c.moveToFirst();
        if (c.getString(11).indexOf("ok1") > -1) {
            Cursor c1 = GetData("Select nd_phantich FROM tbl_tinnhanS WHERE id = " + id);
            c1.moveToFirst();
            String str = c1.getString(0);
            for (int i = 1; i < 6; i++) {
                str = str.replaceAll("\\*", "");
            }
            QueryData("Update tbl_tinnhanS set nd_phantich = '" + str + "', nd_sua = '" + str + "' WHERE id = " + id);
            NhapSoChiTiet(id);
            StringBuilder sb = new StringBuilder();
            sb.append("Update tbl_tinnhanS set phat_hien_loi = 'ok' WHERE id = ");
            sb.append(id);
            QueryData(sb.toString());
        } else {
            String str2 = c.getString(10);
            String str3 = Congthuc.fixTinNhan1(Congthuc.convertKhongDau(str2));
            String Loi = null;
            Cursor cursor = GetData("Select * From thay_the_phu");
            while (cursor.moveToNext()) {
                str3 = str3.replaceAll(cursor.getString(1), cursor.getString(2)).replace("  ", " ");
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            String str4 = Congthuc.fixTinNhan1(str3);
            QueryData("Update tbl_tinnhanS set nd_phantich = '" + str4 + "', nd_sua = '" + str4 + "' WHERE id = " + id);
            if (str4.indexOf("bo") > -1 && str4.indexOf("bor") == -1) {
                for (int j = str4.indexOf("bo") + 3; j < str4.length(); j++) {
                    if (str4.substring(j, j + 1).indexOf(" ") == -1 && !Congthuc.isNumeric(str4.substring(j, j + 1))) {
                        Loi = "Không hiểu " + str4.substring(str4.indexOf("bo"), str4.length());
                    }
                }
            }
            if (str4.indexOf("Không hiểu") > -1) {
                String S = "Update tbl_tinnhanS set nd_phantich = '" + str4 + "', nd_sua = '" + str4 + "',  phat_hien_loi ='" + Loi + "' Where id = " + id;
                QueryData(S);
                createNotification(str4, this.mcontext);
            } else {
                NhanTinNhan(Integer.valueOf(id), type_kh);
                c = GetData("Select * From tbl_tinnhanS WHERE id = " + id);
                c.moveToFirst();
                String _xulytin = c.getString(11);
                if (_xulytin.indexOf("Không hiểu") > -1) {
                    createNotification(_xulytin, this.mcontext);
                } else {
                    NhapSoChiTiet(id);
                }
                if (!(c == null || c.isClosed())) {
                    c.close();
                    return;
                }
            }
        }
        if (c == null) {
        }
    }

    private void createNotification(String aMessage, Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
        taskStackBuilder.addParentStack(MainActivity.class);
        taskStackBuilder.addNextIntent(intent);
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(Constants.ALL_SMS_LOADER, 134217728);
        NotificationCompat.Builder nBuider = new NotificationCompat.Builder(context);
        nBuider.setContentTitle("Ld.pro");
        nBuider.setContentText(aMessage);
        nBuider.setSmallIcon(R.drawable.icon);
        nBuider.setContentIntent(pendingIntent);
        nBuider.setDefaults(1);
        nBuider.setVibrate(new long[]{100, 2000, 500, 2000});
        nBuider.setLights(-16711936, 400, 400);
        @SuppressWarnings("ResourceType")
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService("notification");
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(SupportMenu.CATEGORY_MASK);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 2000, 500, 2000});
            nBuider.setChannelId("10001");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        mNotificationManager.notify(0, nBuider.build());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:79:0x0321
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void NhanTinNhan(Integer r55, int r56) throws JSONException {
        /*
            Method dump skipped, instructions count: 8246
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.NhanTinNhan(java.lang.Integer, int):void");
    }

    public void TralaiSO(int ID) {
        String Tralai = "";
        this.json_Tralai = new JSONObject();
        Cursor cursor = GetData("Select * from tbl_tinnhanS where id = " + ID);
        cursor.moveToFirst();
        try {
            String sql = "Select * From tbl_kh_new Where ten_kh = '" + cursor.getString(4) + "'";
            Cursor Thongtin = GetData(sql);
            if (Thongtin.getCount() > 0 && Thongtin.moveToFirst()) {
                JSONObject jSONObject = new JSONObject(Thongtin.getString(6));
                this.json = jSONObject;
                if (jSONObject.getString("danDe").length() > 0) {
                    String DanDeKhong = this.json.getString("soDe");
                    String TralaiDe = TraDe(Thongtin.getString(0), DanDeKhong);
                    if (TralaiDe.length() > 0) {
                        Tralai = Tralai + "\n" + TralaiDe;
                    }
                }
                if (this.json.getString("danLo").length() > 0) {
                    String DanLoKhong = this.json.getString("soLo");
                    String TralaiLo = TraLo(Thongtin.getString(0), DanLoKhong);
                    if (TralaiLo.length() > 0) {
                        Tralai = Tralai + "\n" + TralaiLo;
                    }
                }
                if (this.json.getInt("xien2") > 0 || this.json.getInt("xien3") > 0 || this.json.getInt("xien4") > 0) {
                    JSONObject JsonXien = new JSONObject();
                    JsonXien.put("xien2", this.json.getInt("xien2"));
                    JsonXien.put("xien3", this.json.getInt("xien3"));
                    JsonXien.put("xien4", this.json.getInt("xien4"));
                    String KhongXien = JsonXien.toString();
                    String TenKH = Thongtin.getString(0);
                    String TralaiXi = TraXi(TenKH, KhongXien);
                    if (TralaiXi.length() > 0) {
                        Tralai = Tralai + "\n" + TralaiXi;
                    }
                }
                if (this.json.getInt("cang") > 0) {
                    String TralaiCang = TraCang(Thongtin.getString(0), this.json.getInt("cang"));
                    if (TralaiCang.length() > 0) {
                        Tralai = Tralai + "\n" + TralaiCang;
                    }
                }
                if (this.json_Tralai.length() > 0) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(new Date());
                    SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
                    dmyFormat.setTimeZone(TimeZone.getDefault());
                    hourFormat.setTimeZone(TimeZone.getDefault());
                    String mNgayNhan = dmyFormat.format(calendar.getTime());
                    String mGionhan = hourFormat.format(calendar.getTime());
                    String sql1 = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mNgayNhan + "' AND ten_kh = '" + Thongtin.getString(0) + "' and type_kh = 2";
                    Cursor getSoTN = GetData(sql1);
                    getSoTN.moveToFirst();
                    int soTN = getSoTN.getInt(0) + 1;
                    final String Tralai2 = "Tra lai " + soTN + ":" + Tralai;
                    String SQL = "Insert Into tbl_tinnhanS values (null, '" + mNgayNhan + "', '" + mGionhan + "',2, '" + Thongtin.getString(0) + "', '" + Thongtin.getString(1) + "','" + Thongtin.getString(2) + "', " + soTN + ", '" + Tralai2.trim() + "','" + Tralai2.substring(Tralai2.indexOf(":") + 1) + "','" + Tralai2.substring(Tralai2.indexOf(":") + 1) + "', 'ko',0,0,0, '" + this.json_Tralai.toString() + "')";
                    QueryData(SQL);
                    String SSSS = "Select id From tbl_tinnhanS where ngay_nhan = '" + mNgayNhan + "' AND type_kh = 2 AND ten_kh ='" + Thongtin.getString(0) + "' AND nd_goc = '" + Tralai2.trim() + "'";
                    Cursor ccc = GetData(SSSS);
                    ccc.moveToFirst();
                    Update_TinNhanGoc(ccc.getInt(0), 2);
                    if (Thongtin.getString(2).indexOf("TL") > -1) {
                        final Long TralaiID = Long.valueOf(Thongtin.getLong(1));
                        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.data.Database.1
                            @Override // java.lang.Runnable
                            public void run() {
                                new MainActivity();
                                MainActivity.sendMessage(TralaiID.longValue(), Tralai2);
                            }
                        });
                    } else if (Thongtin.getString(2).indexOf("sms") > -1) {
                        SendSMS(Thongtin.getString(1), Tralai2);
                    } else {
                        JSONObject jsonObject = new JSONObject(MainActivity.json_Tinnhan.getString(Thongtin.getString(1)));
                        if (jsonObject.getInt("Time") > 3) {
                            NotificationReader notificationReader = new NotificationReader();
                            notificationReader.NotificationWearReader(Thongtin.getString(1), Tralai2);
                        } else {
                            jsonObject.put(Tralai2, "OK");
                            MainActivity.json_Tinnhan.put(Thongtin.getString(1), jsonObject);
                        }
                    }
                    NhapSoChiTiet(ccc.getInt(0));
                    ccc.close();
                }
            }
            Thongtin.close();
        } catch (Exception e) {
            e.getMessage();
        }
        cursor.close();
    }

    public String TraDe(String TenKH, String DanDe) {
        JSONException e;
        JSONObject json_DeKhong = null;
        String Str1 = null;
        Cursor cursor = null;
        String Str12;
        String Str = null;
        String Str2 = null;
        int i;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        dmyFormat.setTimeZone(TimeZone.getDefault());
        hourFormat.setTimeZone(TimeZone.getDefault());
        String mDate = dmyFormat.format(calendar.getTime());
        String Str3 = "";
        JSONObject jSon_Deb = new JSONObject();
        JSONObject jsonSoCt = new JSONObject();
        String TheLoai = "De";
        String DangLoc = "deb";
        String LoaiDe = "de dit db";
        try {
            if (this.caidat_tg.getInt("khach_de") == 1) {
                TheLoai = "Det";
                DangLoc = "det";
                LoaiDe = "de 8";
            }
        } catch (JSONException e2) {
        }
        try {
            json_DeKhong = new JSONObject(DanDe);
            StringBuilder sb = new StringBuilder();
            try {
                sb.append("Select the_loai, so_chon, Sum(diem_ton*(type_kh = 1)) as Nhan, Sum(diem_ton *(type_kh = 2)) As Tra \nFROM tbl_soctS Where ten_kh = '");
                sb.append(TenKH);
                sb.append("' AND ngay_nhan = '");
                sb.append(mDate);
                sb.append("' AND the_loai = '");
                sb.append(DangLoc);
                sb.append("' Group by so_chon Order by so_chon");
                Str1 = sb.toString();
                cursor = GetData(Str1);
            } catch (Exception e3) {
               // e = e3;
            }
        } catch (JSONException e4) {
            e = e4;
        }
        while (true) {
            Str12 = "So_chon";
            if (!cursor.moveToNext()) {
                break;
            }
            try {
                if (json_DeKhong.has(cursor.getString(1))) {
                    Str = Str3;
                    try {
                        jsonSoCt.put(Str12, cursor.getString(1));
                        jsonSoCt.put("Da_nhan", cursor.getInt(2));
                        jsonSoCt.put("Da_tra", cursor.getInt(3));
                        jsonSoCt.put("Khong_Tien", json_DeKhong.getInt(cursor.getString(1)));
                        jsonSoCt.put("Se_tra", (jsonSoCt.getInt("Da_nhan") - jsonSoCt.getInt("Da_tra")) - jsonSoCt.getInt("Khong_Tien"));
                        if (jsonSoCt.getInt("Se_tra") > 0) {
                            jSon_Deb.put(cursor.getString(1), jsonSoCt.toString());
                            Str3 = Str;
                            Str1 = Str1;
                            dmyFormat = dmyFormat;
                            json_DeKhong = json_DeKhong;
                            mDate = mDate;
                            cursor = cursor;
                            hourFormat = hourFormat;
                        } else {
                            Str3 = Str;
                            Str1 = Str1;
                            dmyFormat = dmyFormat;
                            json_DeKhong = json_DeKhong;
                            mDate = mDate;
                            cursor = cursor;
                            hourFormat = hourFormat;
                        }
                    } catch (JSONException e5) {
                        e = e5;
                        Str3 = Str;
                    }
                } else {
                    Str1 = Str1;
                    dmyFormat = dmyFormat;
                    json_DeKhong = json_DeKhong;
                    mDate = mDate;
                    cursor = cursor;
                    hourFormat = hourFormat;
                }
            } catch (Exception e6) {
               // e = e6;
            }

            Str3 = Str;
          //  e.printStackTrace();
            return Str3;
        }
        Cursor cursor2 = cursor;
        JSONObject json_DeKhong2 = json_DeKhong;
        Str = Str3;
        try {
            if (jSon_Deb.length() <= 0) {
                return Str;
            }
            Iterator<String> iter = jSon_Deb.keys();
            List<JSONObject> jsonValues = new ArrayList<>();
            while (iter.hasNext()) {
                try {
                    String key = iter.next();
                    try {
                        jsonSoCt = new JSONObject(jSon_Deb.getString(key));
                        jsonValues.add(jsonSoCt);
                    } catch (JSONException e7) {
                        e7.printStackTrace();
                    }
                } catch (Exception e8) {
                  //  e = e8;
                    Str3 = Str;
                }
            }
            Collections.sort(jsonValues, new Comparator<JSONObject>() { // from class: tamhoang.ldpro4.data.Database.2
                public int compare(JSONObject a, JSONObject b) {
                    int valA = 0;
                    Integer valB = 0;
                    try {
                        valA = Integer.valueOf(a.getInt("Se_tra"));
                        valB = Integer.valueOf(b.getInt("Se_tra"));
                    } catch (JSONException e9) {
                    }
                    return valB.compareTo(valA);
                }
            });
            int tien = 0;
            String Str4 = "";
            int i2 = 0;
            String Str111 = Str;
            while (i2 < jsonValues.size()) {
                try {
                    try {
                        JSONObject soCT = jsonValues.get(i2);
                        if (tien > soCT.getInt("Se_tra")) {
                            JSONObject json_Tra = new JSONObject();
                            String[] sss = Str4.split(",");
                            i = i2;
                            json_Tra.put("du_lieu", Str4 + "x" + tien + "n");
                            json_Tra.put("the_loai", LoaiDe);
                            json_Tra.put("dan_so", Str4);
                            json_Tra.put("so_tien", tien);
                            json_Tra.put("so_luong", sss.length);
                            this.json_Tralai.put(String.valueOf(this.json_Tralai.length() + 1), json_Tra.toString());
                            String Str1112 = Str4 + "x" + tien + "n ";
                            StringBuilder sb2 = new StringBuilder();
                            Str3 = Str111;
                            try {
                                sb2.append(Str3);
                                sb2.append(Str1112);
                                String Str5 = sb2.toString();
                                try {
                                    StringBuilder sb3 = new StringBuilder();
                                    Str12 = Str12;
                                    sb3.append(soCT.getString(Str12));
                                    sb3.append(",");
                                    String Str1113 = sb3.toString();
                                    tien = soCT.getInt("Se_tra");
                                    Str4 = Str1113;
                                    Str111 = Str5;
                                } catch (JSONException e9) {
                                    e = e9;
                                    Str3 = Str5;
                                }
                            } catch (Exception e10) {
                              //  e = e10;
                            }
                        } else {
                            i = i2;
                            Str12 = Str12;
                            String Str1114 = Str4 + soCT.getString(Str12) + ",";
                            tien = soCT.getInt("Se_tra");
                            Str4 = Str1114;
                            Str111 = Str111;
                        }
                        i2 = i + 1;
                        cursor2 = cursor2;
                        json_DeKhong2 = json_DeKhong2;
                        jSon_Deb = jSon_Deb;
                        jsonSoCt = jsonSoCt;
                        DangLoc = DangLoc;
                        TheLoai = TheLoai;
                        jsonValues = jsonValues;
                    } catch (JSONException e11) {
                        e = e11;
                        Str3 = Str111;
                    }
                } catch (Exception e12) {
                 //   e = e12;
                    Str3 = Str111;
                }
            }
            Str3 = Str111;
            try {
                try {
                    try {
                        StringBuilder sb4 = null;
                        if (Str4.length() > 0) {
                            JSONObject json_Tra2 = new JSONObject();
                            String[] sss2 = Str4.split(",");
                            json_Tra2.put("du_lieu", Str4 + "x" + tien + "n");
                            json_Tra2.put("the_loai", LoaiDe);
                            json_Tra2.put("dan_so", Str4);
                            json_Tra2.put("so_tien", tien);
                            json_Tra2.put("so_luong", sss2.length);
                            if (tien > 0) {
                                this.json_Tralai.put(String.valueOf(this.json_Tralai.length() + 1), json_Tra2.toString());
                                Str2 = Str3 + Str4 + "x" + tien + "n ";
                                sb4 = new StringBuilder();
                                sb4.append(TheLoai);
                                sb4.append(": ");
                                sb4.append(Str2);
                                return sb4.toString();
                            }
                        }
                        sb4.append(TheLoai);
                        sb4.append(": ");
                        sb4.append(Str2);
                        return sb4.toString();
                    } catch (JSONException e13) {
                        e = e13;
                        Str3 = Str2;
                    }
                    StringBuilder sb42 = new StringBuilder();
                } catch (Exception e14) {
                 //   e = e14;
                    Str3 = Str2;
                }
                Str2 = Str3;
            } catch (Exception e15) {
             //   e = e15;
            }
        } catch (Exception e16) {
          //  e = e16;
            Str3 = Str;
        }
        return Str3;
    }

    /* JADX WARN: Can't wrap try/catch for region: R(8:2|77|3|(7:4|5|(5:7|87|8|(3:101|10|(4:100|12|13|103)(3:97|14|104))(3:98|15|105)|102)(1:99)|16|17|75|76)|18|91|19|(15:21|22|(8:25|26|89|27|111|30|95|23)|109|31|32|(8:81|37|(8:39|40|93|41|85|42|43|108)(4:46|47|48|107)|49|33|34|83|35)|106|52|79|53|(2:55|(4:57|58|60|61))|59|60|61)(2:70|76)) */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x02de, code lost:
        r0 = e;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TraLo(String r33, String r34) {
        /*
            Method dump skipped, instructions count: 753
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.TraLo(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARN: Removed duplicated region for block: B:117:0x013a A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:29:0x012e A[Catch: JSONException -> 0x0190, TRY_LEAVE, TryCatch #11 {JSONException -> 0x0190, blocks: (B:14:0x00af, B:16:0x00ba, B:17:0x00d3, B:19:0x00d9, B:21:0x00e5, B:22:0x00fe, B:24:0x0104, B:26:0x0110, B:27:0x0128, B:29:0x012e, B:41:0x0177, B:45:0x018c, B:43:0x017e), top: B:113:0x00af, inners: #7 }] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TraXi(String r33, String r34) {
        /*
            Method dump skipped, instructions count: 936
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.TraXi(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARN: Can't wrap try/catch for region: R(7:2|(2:94|3)|(7:4|5|(7:86|7|8|98|9|(4:108|11|12|110)(3:107|13|111)|109)(1:106)|34|16|80|81)|17|88|18|(16:20|21|(11:24|25|84|26|27|102|28|117|33|104|22)|115|35|36|37|(8:90|42|(8:44|45|100|46|96|47|48|114)(4:49|50|51|113)|52|40|38|39|92)|112|57|82|58|(2:60|(4:62|63|65|66))|64|65|66)(2:75|81)) */
    /* JADX WARN: Code restructure failed: missing block: B:76:0x02d4, code lost:
        r0 = e;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TraCang(String r32, int r33) {
        /*
            Method dump skipped, instructions count: 747
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.TraCang(java.lang.String, int):java.lang.String");
    }

    /* JADX WARN: Removed duplicated region for block: B:123:0x074d  */
    /* JADX WARN: Removed duplicated region for block: B:247:0x0bcb  */
    /* JADX WARN: Removed duplicated region for block: B:351:0x0f2e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private void XulyMang(int r22) {
        /*
            Method dump skipped, instructions count: 4517
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.XulyMang(int):void");
    }

    private void BaoLoiDan(int rw) {
        if (this.mang[rw][4].indexOf("Không hiểu") > -1) {
            String[][] strArr = this.mang;
            strArr[rw][0] = Congthuc.ToMauError(strArr[rw][4].substring(11), this.mang[rw][0]);
        }
    }

    private void BaoLoiTien(int rw) {
        try {
            this.mang[rw][5] = Congthuc.XulyTien(this.mang[rw][3]);
            if (this.mang[rw][5].indexOf("Không hiểu") > -1 && this.mang[rw][5].trim().length() < 13) {
                this.mang[rw][0] = Congthuc.ToMauError(this.mang[rw][5].substring(11), this.mang[rw][0]);
            } else if (this.mang[rw][5].indexOf("Không hiểu") > -1 && this.mang[rw][5].trim().length() > 12) {
                this.mang[rw][0] = Congthuc.ToMauError(this.mang[rw][3], this.mang[rw][0]);
            }
        } catch (Exception e) {
            String[][] strArr = this.mang;
            strArr[rw][0] = Congthuc.ToMauError(strArr[rw][3], strArr[rw][0]);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:116:0x0332 A[Catch: all -> 0x0366, Exception -> 0x0376, TRY_ENTER, TRY_LEAVE, TryCatch #45 {Exception -> 0x0376, all -> 0x0366, blocks: (B:89:0x025a, B:90:0x026b, B:92:0x0273, B:95:0x027c, B:97:0x0282, B:98:0x029e, B:100:0x02a5, B:102:0x02c7, B:116:0x0332, B:129:0x038d, B:134:0x03a4, B:139:0x03bb, B:144:0x03d2, B:149:0x03e9, B:154:0x0400, B:156:0x0407, B:161:0x041e, B:163:0x0426, B:168:0x043e, B:170:0x0446, B:175:0x045e, B:180:0x0476), top: B:359:0x025a }] */
    /* JADX WARN: Removed duplicated region for block: B:126:0x0386 A[Catch: all -> 0x0866, Exception -> 0x087a, TRY_ENTER, TRY_LEAVE, TryCatch #46 {Exception -> 0x087a, all -> 0x0866, blocks: (B:112:0x030b, B:113:0x0325, B:126:0x0386, B:131:0x039d, B:136:0x03b4, B:141:0x03cb, B:146:0x03e2, B:151:0x03f9, B:158:0x0417, B:165:0x0437, B:172:0x0457, B:177:0x046f), top: B:357:0x030b }] */
    /* JADX WARN: Removed duplicated region for block: B:208:0x04f8  */
    /* JADX WARN: Removed duplicated region for block: B:209:0x0510  */
    /* JADX WARN: Removed duplicated region for block: B:20:0x00f6 A[Catch: all -> 0x0987, Exception -> 0x099f, TRY_LEAVE, TryCatch #36 {Exception -> 0x099f, all -> 0x0987, blocks: (B:18:0x00f0, B:20:0x00f6), top: B:377:0x00f0 }] */
    /* JADX WARN: Removed duplicated region for block: B:219:0x0559 A[Catch: all -> 0x07be, Exception -> 0x07d0, TRY_ENTER, TryCatch #30 {Exception -> 0x07d0, all -> 0x07be, blocks: (B:211:0x052d, B:219:0x0559, B:220:0x055f, B:222:0x0563, B:227:0x058e, B:234:0x05af, B:241:0x05d3, B:249:0x05fb), top: B:389:0x052d }] */
    /* JADX WARN: Removed duplicated region for block: B:222:0x0563 A[Catch: all -> 0x07be, Exception -> 0x07d0, TRY_LEAVE, TryCatch #30 {Exception -> 0x07d0, all -> 0x07be, blocks: (B:211:0x052d, B:219:0x0559, B:220:0x055f, B:222:0x0563, B:227:0x058e, B:234:0x05af, B:241:0x05d3, B:249:0x05fb), top: B:389:0x052d }] */
    /* JADX WARN: Removed duplicated region for block: B:333:0x09f2  */
    /* JADX WARN: Removed duplicated region for block: B:336:0x09fb  */
    /* JADX WARN: Removed duplicated region for block: B:373:0x049b A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:387:0x0538 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void NhapSoChiTiet(int r53) {
        /*
            Method dump skipped, instructions count: 2590
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.NhapSoChiTiet(int):void");
    }

    private String xuly_Xq(String str) {
        String dan_xien = "";
        String[] so_xien = str.split(",");
        if (so_xien.length == 3) {
            for (int i1 = 0; i1 < so_xien.length - 1; i1++) {
                for (int i2 = i1 + 1; i2 < so_xien.length; i2++) {
                    dan_xien = dan_xien + so_xien[i1] + "," + so_xien[i2] + " ";
                }
            }
            return dan_xien + so_xien[0] + "," + so_xien[1] + "," + so_xien[2];
        }
        int i12 = so_xien.length;
        if (i12 != 4) {
            return dan_xien;
        }
        for (int i13 = 0; i13 < so_xien.length - 1; i13++) {
            for (int i22 = i13 + 1; i22 < so_xien.length; i22++) {
                dan_xien = dan_xien + so_xien[i13] + "," + so_xien[i22] + " ";
            }
        }
        for (int i14 = 0; i14 < so_xien.length - 2; i14++) {
            for (int i23 = i14 + 1; i23 < so_xien.length - 1; i23++) {
                for (int i3 = i23 + 1; i3 < so_xien.length; i3++) {
                    dan_xien = dan_xien + so_xien[i14] + "," + so_xien[i23] + "," + so_xien[i3] + " ";
                }
            }
        }
        return dan_xien + so_xien[0] + "," + so_xien[1] + "," + so_xien[2] + "," + so_xien[3];
    }

    public String XuatDanTon2(String TheLoai, String Tienxuat, int mFrom, int mTo) {
        String xuatDan;
        int tien;
        String xuatDan2;
        int Dem;
        int mFrom2;
        int mFrom3;
        int MaxTien;
        int tien2;
        String xuatDan3;
        String str = Tienxuat;
        int DemPhu = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        dmyFormat.setTimeZone(TimeZone.getDefault());
        String mDate = dmyFormat.format(calendar.getTime());
        String my_str = Tienxuat;
        if (my_str.length() == 0) {
            my_str = "0";
        }
        int TienChuyen = Integer.parseInt(my_str.replaceAll("%", "").replaceAll("n", "").replaceAll("k", "").replaceAll("d", "").replaceAll(">", ""));
        String SoOm = "";
        String tloai = null;
        String donvi = "n ";
        if (TheLoai == "deb") {
            xuatDan = "De:";
            SoOm = "Om_deB";
            tloai = "(the_loai = 'deb' or the_loai = 'det')";
            donvi = "n ";
        } else if (TheLoai == "dea") {
            xuatDan = "Dau DB:";
            SoOm = "Om_deA";
            tloai = "the_loai = 'dea'";
            donvi = "n ";
        } else if (TheLoai == "dec") {
            xuatDan = "Dau nhat:";
            SoOm = "Om_deC";
            tloai = "the_loai = 'dec'";
            donvi = "n ";
        } else if (TheLoai == "ded") {
            xuatDan = "Dit nhat:";
            SoOm = "Om_deD";
            tloai = "the_loai = 'ded'";
            donvi = "n ";
        } else if (TheLoai == "lo") {
            xuatDan = "Lo:";
            SoOm = "Om_lo";
            tloai = "the_loai = 'lo'";
            donvi = "d ";
        } else {
            xuatDan = "";
        }
        String str2 = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om." + SoOm + " + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om." + SoOm + " as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So\n Where tbl_soctS.ngay_nhan='" + mDate + "' AND " + tloai + " GROUP by so_om.So Order by ton DESC, diem DESC";
        Cursor cursor = GetData(str2);
        int mLamtron = 1;
        try {
            if (MainActivity.jSon_Setting.getInt("lam_tron") == 0) {
                mLamtron = 1;
            } else if (MainActivity.jSon_Setting.getInt("lam_tron") == 1) {
                mLamtron = 10;
            } else if (MainActivity.jSon_Setting.getInt("lam_tron") == 2) {
                mLamtron = 50;
            } else if (MainActivity.jSon_Setting.getInt("lam_tron") == 3) {
                mLamtron = 100;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mFrom > 0) {
            mFrom2 = mFrom - 1;
            xuatDan2 = xuatDan;
            Dem = 0;
            tien = 0;
        } else {
            mFrom2 = mFrom;
            xuatDan2 = xuatDan;
            Dem = 0;
            tien = 0;
        }
        while (cursor.moveToNext()) {
            if (Dem < mFrom2 || Dem > mTo - 1) {
                mFrom3 = mFrom2;
            } else {
                if (TienChuyen == 0) {
                    mFrom3 = mFrom2;
                    MaxTien = (cursor.getInt(4) / mLamtron) * mLamtron;
                } else {
                    mFrom3 = mFrom2;
                    if (str.indexOf("%") > -1) {
                        MaxTien = (((cursor.getInt(4) * TienChuyen) / mLamtron) / 100) * mLamtron;
                    } else if (str.indexOf(">") > -1) {
                        MaxTien = ((cursor.getInt(4) - TienChuyen) / mLamtron) * mLamtron;
                    } else if (cursor.getInt(4) > TienChuyen) {
                        MaxTien = (TienChuyen / mLamtron) * mLamtron;
                    } else {
                        MaxTien = (cursor.getInt(4) / mLamtron) * mLamtron;
                    }
                }
                if (MaxTien > 0) {
                    if (tien > MaxTien) {
                        DemPhu = 0;
                        xuatDan3 = (xuatDan2 + "x" + tien + donvi) + cursor.getString(0) + ",";
                        tien2 = MaxTien;
                    } else {
                        xuatDan3 = xuatDan2 + cursor.getString(0) + ",";
                        tien2 = MaxTien;
                    }
                    DemPhu++;
                    tien = tien2;
                    xuatDan2 = xuatDan3;
                }
            }
            Dem++;
            str = Tienxuat;
            mFrom2 = mFrom3;
            dmyFormat = dmyFormat;
            mDate = mDate;
        }
        if (xuatDan2.length() > 4 && DemPhu > 0) {
            xuatDan2 = xuatDan2 + "x" + tien + donvi;
        }
        if (cursor != null) {
            cursor.close();
        }
        return xuatDan2;
    }

    /* JADX WARN: Removed duplicated region for block: B:105:0x0385  */
    /* JADX WARN: Removed duplicated region for block: B:125:0x051e  */
    /* JADX WARN: Removed duplicated region for block: B:128:0x0552  */
    /* JADX WARN: Removed duplicated region for block: B:13:0x0115 A[Catch: JSONException -> 0x0309, TRY_LEAVE, TryCatch #13 {JSONException -> 0x0309, blocks: (B:11:0x010f, B:13:0x0115), top: B:184:0x010f }] */
    /* JADX WARN: Removed duplicated region for block: B:140:0x0610  */
    /* JADX WARN: Removed duplicated region for block: B:153:0x06e6  */
    /* JADX WARN: Removed duplicated region for block: B:156:0x06ef  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String Tin_Chottien(String r41) throws JSONException {
        /*
            Method dump skipped, instructions count: 1779
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.Tin_Chottien(java.lang.String):java.lang.String");
    }

    /* JADX WARN: Removed duplicated region for block: B:104:0x03e0  */
    /* JADX WARN: Removed duplicated region for block: B:123:0x056e  */
    /* JADX WARN: Removed duplicated region for block: B:126:0x05a2  */
    /* JADX WARN: Removed duplicated region for block: B:138:0x0675  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0113 A[Catch: JSONException -> 0x0343, TRY_LEAVE, TryCatch #0 {JSONException -> 0x0343, blocks: (B:12:0x010d, B:14:0x0113), top: B:156:0x010d }] */
    /* JADX WARN: Removed duplicated region for block: B:151:0x075b  */
    /* JADX WARN: Removed duplicated region for block: B:154:0x0764  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String Tin_Chottien_xien(String r45) throws JSONException {
        /*
            Method dump skipped, instructions count: 1896
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.Tin_Chottien_xien(java.lang.String):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:7:0x00db
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public String Tin_Chottien_CT(String r22) {
        /*
            Method dump skipped, instructions count: 703
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.Tin_Chottien_CT(java.lang.String):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:7:0x00c1
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public String Tin_Chottien_CT11(String r43) {
        /*
            Method dump skipped, instructions count: 1302
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.Tin_Chottien_CT11(java.lang.String):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:324:0x0e45
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void Gui_Tin_Nhan(int r43) {
        /*
            Method dump skipped, instructions count: 4424
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.data.Database.Gui_Tin_Nhan(int):void");
    }

    public void Tinhtien(String mDate) throws JSONException {
        int i;
        int i2;
        int i1;
        int l;
        boolean errKQ;
        Cursor c1;
        String BaCang;
        String BaCangDau = "";
        Cursor c12 = null;
        boolean errKQ2 = false;
        Cursor cursor = GetData("Select * From KetQua WHERE ngay = '" + mDate + "'");
        cursor.moveToFirst();
        int i3 = 2;
        while (true) {
            if (i3 >= 29) {
                break;
            } else if (cursor.getString(i3) == null) {
                errKQ2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (!errKQ2) {
            String[][] mang = (String[][]) Array.newInstance(String.class, 1000, 8);
            String str = "Select * From KetQua WHERE ngay = '" + mDate + "'";
            Cursor cursor1 = GetData(str);
            cursor1.moveToFirst();
            int i4 = 0;
            while (true) {
                i = 1;
                if (i4 >= mang.length) {
                    break;
                }
                mang[i4][0] = "";
                mang[i4][1] = "";
                mang[i4][2] = "";
                mang[i4][3] = "";
                mang[i4][4] = "";
                mang[i4][5] = "";
                mang[i4][6] = "";
                mang[i4][7] = "";
                i4++;
            }
            QueryData("Delete FROM tbl_tinnhanS WHERE Length(nd_phantich) <5 ");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = 0 WHERE ngay_nhan = '" + mDate + "' AND the_loai <> 'tt' AND the_loai <> 'cn'");
            int i5 = 2;
            while (i5 < 29) {
                if (i5 <= i || i5 >= 12) {
                    BaCang = BaCangDau;
                    c1 = c12;
                    errKQ = errKQ2;
                    if (i5 > 11 && i5 < 22) {
                        mang[Integer.parseInt(cursor1.getString(i5).substring(2, 4))][0] = mang[Integer.parseInt(cursor1.getString(i5).substring(2, 4))][0] + "*";
                        mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] = mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] + "*";
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'lo' AND so_chon = '" + cursor1.getString(i5).substring(2, 4) + "'");
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'loa' AND so_chon = '" + cursor1.getString(i5).substring(0, 2) + "'");
                    } else if (i5 <= 21 || i5 >= 25) {
                        mang[Integer.parseInt(cursor1.getString(i5))][0] = mang[Integer.parseInt(cursor1.getString(i5))][0] + "*";
                        mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] = mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] + "*";
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'lo' AND so_chon = '" + cursor1.getString(i5) + "'");
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'loa' AND so_chon = '" + cursor1.getString(i5) + "'");
                    } else {
                        mang[Integer.parseInt(cursor1.getString(i5).substring(1, 3))][0] = mang[Integer.parseInt(cursor1.getString(i5).substring(1, 3))][0] + "*";
                        mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] = mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] + "*";
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'lo' AND so_chon = '" + cursor1.getString(i5).substring(1, 3) + "'");
                        QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'loa' AND so_chon = '" + cursor1.getString(i5).substring(0, 2) + "'");
                    }
                } else {
                    BaCang = BaCangDau;
                    String[] strArr = mang[Integer.parseInt(cursor1.getString(i5).substring(3, 5))];
                    StringBuilder sb = new StringBuilder();
                    c1 = c12;
                    errKQ = errKQ2;
                    sb.append(mang[Integer.parseInt(cursor1.getString(i5).substring(3, 5))][0]);
                    sb.append("*");
                    strArr[0] = sb.toString();
                    mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] = mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][6] + "*";
                    String ssss = "Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'lo' AND so_chon = '" + cursor1.getString(i5).substring(3, 5) + "'";
                    QueryData(ssss);
                    String ssss2 = "Update tbl_soctS Set so_nhay = so_nhay + 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'loa' AND so_chon = '" + cursor1.getString(i5).substring(0, 2) + "'";
                    QueryData(ssss2);
                }
                if (i5 == 2) {
                    mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][1] = "*";
                    mang[Integer.parseInt(cursor1.getString(i5).substring(3, 5))][2] = "*";
                    String BaCang2 = cursor1.getString(i5).substring(2, 5);
                    QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'bc' AND so_chon = '" + BaCang2 + "'");
                    mang[Integer.parseInt(cursor1.getString(i5).substring(2, 5))][5] = "*";
                    String BaCangDau2 = cursor1.getString(i5).substring(0, 3);
                    QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'bca' AND so_chon = '" + BaCangDau2 + "'");
                    mang[Integer.parseInt(cursor1.getString(i5).substring(0, 3))][7] = "*";
                    if (MainActivity.jSon_Setting.getInt("ap_man") > 0) {
                        int j = 0;
                        while (j < 10) {
                            if (Integer.parseInt(j + cursor1.getString(i5).substring(3, 5)) != Integer.parseInt(BaCang2)) {
                                QueryData("Update tbl_soctS Set so_nhay = 1, lan_an = " + (MainActivity.jSon_Setting.getInt("ap_man") * 1000) + " WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'bc' AND so_chon = '" + j + cursor1.getString(i5).substring(3, 5) + "'");
                            }
                            mang[Integer.parseInt(j + cursor1.getString(i5).substring(3, 5))][5] = "*";
                            j++;
                            BaCangDau2 = BaCangDau2;
                        }
                    }
                    BaCangDau = BaCang2;
                } else {
                    BaCangDau = BaCang;
                }
                if (i5 == 3) {
                    mang[Integer.parseInt(cursor1.getString(i5).substring(0, 2))][3] = "*";
                    mang[Integer.parseInt(cursor1.getString(i5).substring(3, 5))][4] = "*";
                }
                i5++;
                c12 = c1;
                errKQ2 = errKQ;
                i = 1;
            }
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dea' AND so_chon <> '" + cursor1.getString(2).substring(0, 2) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = diem * lan_an -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dea' AND so_chon ='" + cursor1.getString(2).substring(0, 2) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dea' AND so_chon <> '" + cursor1.getString(2).substring(0, 2) + "' AND type_kh = 2");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Update tbl_soctS Set so_nhay = 1, ket_qua = -diem * lan_an +tong_tien WHERE ngay_nhan = '");
            sb2.append(mDate);
            sb2.append("' AND the_loai = 'dea' AND so_chon = '");
            String str2 = str;
            sb2.append(cursor1.getString(2).substring(0, 2));
            sb2.append("' AND type_kh = 2");
            QueryData(sb2.toString());
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'deb' AND so_chon <> '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = diem * lan_an -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'deb' AND so_chon ='" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'deb' AND so_chon <> '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = -diem * lan_an +tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'deb' AND so_chon = '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dec' AND so_chon <> '" + cursor1.getString(3).substring(0, 2) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = diem * lan_an -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dec' AND so_chon ='" + cursor1.getString(3).substring(0, 2) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dec' AND so_chon <> '" + cursor1.getString(3).substring(0, 2) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = -diem * lan_an +tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'dec' AND so_chon = '" + cursor1.getString(3).substring(0, 2) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'ded' AND so_chon <> '" + cursor1.getString(3).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = diem * lan_an -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'ded' AND so_chon ='" + cursor1.getString(3).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'ded' AND so_chon <> '" + cursor1.getString(3).substring(3, 5) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = -diem * lan_an +tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'ded' AND so_chon = '" + cursor1.getString(3).substring(3, 5) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = -tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'det' AND so_chon <> '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = diem * lan_an-tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'det' AND so_chon ='" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 1");
            QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'det' AND so_chon <> '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 2");
            QueryData("Update tbl_soctS Set so_nhay = 1, ket_qua = -diem * lan_an+tong_tien WHERE ngay_nhan = '" + mDate + "' AND the_loai = 'det' AND so_chon = '" + cursor1.getString(2).substring(3, 5) + "' AND type_kh = 2");
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Select * From tbl_soctS Where ngay_nhan = '");
            sb3.append(mDate);
            sb3.append("' AND (the_loai = 'xn' OR the_loai = 'xi')");
            Cursor cursor2 = GetData(sb3.toString());
            while (true) {
                i2 = -1;
                if (!cursor2.moveToNext()) {
                    break;
                } else if ("xi".indexOf(cursor2.getString(6)) > -1) {
                    String[] str22 = cursor2.getString(7).split(",");
                    boolean check = true;
                    int j2 = 0;
                    while (true) {
                        if (j2 >= str22.length) {
                            break;
                        } else if (mang[Integer.parseInt(str22[j2])][0].length() == 0) {
                            check = false;
                            break;
                        } else {
                            j2++;
                        }
                    }
                    if (check) {
                        QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ID = " + cursor2.getString(0));
                    }
                } else if ("xn".indexOf(cursor2.getString(6)) > -1) {
                    String[] str23 = cursor2.getString(7).split(",");
                    boolean check2 = false;
                    if (mang[Integer.parseInt(str23[0])][0].length() > 1 || mang[Integer.parseInt(str23[1])][0].length() > 1 || (mang[Integer.parseInt(str23[0])][0].length() > 0 && mang[Integer.parseInt(str23[1])][0].length() > 0)) {
                        check2 = true;
                    }
                    if (check2) {
                        QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ID = " + cursor2.getString(0));
                    }
                }
            }
            Cursor cursor22 = GetData("Select * From tbl_soctS Where ngay_nhan = '" + mDate + "' AND the_loai = 'xia'");
            while (cursor22.moveToNext()) {
                String[] str24 = cursor22.getString(7).split(",");
                boolean check3 = true;
                int j3 = 0;
                while (true) {
                    if (j3 >= str24.length) {
                        break;
                    } else if (mang[Integer.parseInt(str24[j3])][6].length() == 0) {
                        check3 = false;
                        break;
                    } else {
                        j3++;
                    }
                }
                if (check3) {
                    QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ID = " + cursor22.getString(0));
                }
            }
            if (MainActivity.jSon_Setting.getInt("tra_thuong_lo") > 0) {
                int Sonhaymax = MainActivity.jSon_Setting.getInt("tra_thuong_lo") + 1;
                String max_nhay = "Update tbl_soctS Set so_nhay = " + Sonhaymax + " Where so_nhay > " + Sonhaymax;
                QueryData(max_nhay);
            }
            QueryData("Update tbl_soctS set ket_qua = diem * lan_an * so_nhay - tong_tien WHERE ngay_nhan = '" + mDate + "' AND type_kh = 1 AND the_loai <> 'tt' AND the_loai <> 'cn'");
            QueryData("Update tbl_soctS set ket_qua = -diem * lan_an * so_nhay + tong_tien WHERE ngay_nhan = '" + mDate + "' AND type_kh = 2 AND the_loai <> 'tt' AND the_loai <> 'cn'");
            QueryData("UPDATE tbl_tinnhanS set tinh_tien = 0 Where ngay_nhan = '" + mDate + "'");
            Cursor cursor23 = GetData("Select * From tbl_tinnhanS Where ngay_nhan = '" + mDate + "' AND tinh_tien = 0 AND phat_hien_loi = 'ok'");
            while (cursor23.moveToNext()) {
                if (cursor23.getInt(12) == 0) {
                    String str3 = cursor23.getString(10);
                    if (str3.indexOf("Bỏ ") == 0) {
                        str3 = str3.substring(str3.indexOf("\n") + 1);
                    }
                    for (int i6 = 0; i6 < 9; i6++) {
                        str3 = str3.replaceAll("\\*", "");
                    }
                    String Laydan = "";
                    int k = 0;
                    int i12 = -1;
                    while (true) {
                        int indexOf = str3.indexOf("\n", i12 + 1);
                        int i13 = indexOf;
                        if (indexOf == i2) {
                            break;
                        }
                        String strT = str3.substring(k);
                        String str1 = strT.substring(0, strT.indexOf("\n") + 1);
                        int l2 = str1.indexOf("\n") + 1;
                        int k2 = k + l2;
                        if (str1.indexOf("de dau db") > -1) {
                            String[] str25 = str1.substring(10, str1.indexOf(",x")).split(",");
                            String str32 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append(Laydan);
                            l = l2;
                            sb4.append("de dau db:");
                            String Laydan2 = sb4.toString();
                            int j4 = 0;
                            while (j4 < str25.length) {
                                Laydan2 = Laydan2 + str25[j4] + mang[Integer.parseInt(str25[j4])][1] + ",";
                                j4++;
                                i13 = i13;
                            }
                            i1 = i13;
                            Laydan = Laydan2 + "x" + str32 + "\n";
                        } else {
                            l = l2;
                            i1 = i13;
                            if (str1.indexOf("de dit db") > -1) {
                                String str4 = str1.substring(10, str1.indexOf(",x"));
                                String[] str26 = str4.split(",");
                                String str33 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan3 = Laydan + "de dit db:";
                                int j5 = 0;
                                while (j5 < str26.length) {
                                    Laydan3 = Laydan3 + str26[j5] + mang[Integer.parseInt(str26[j5])][2] + ",";
                                    j5++;
                                    str4 = str4;
                                }
                                Laydan = Laydan3 + "x" + str33 + "\n";
                            } else if (str1.indexOf("de 8") > -1) {
                                String str42 = str1.substring(5, str1.indexOf(",x"));
                                String[] str27 = str42.split(",");
                                String str34 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan4 = Laydan + "de 8:";
                                int j6 = 0;
                                while (j6 < str27.length) {
                                    Laydan4 = Laydan4 + str27[j6] + mang[Integer.parseInt(str27[j6])][2] + ",";
                                    j6++;
                                    str42 = str42;
                                }
                                Laydan = Laydan4 + "x" + str34 + "\n";
                            } else if (str1.indexOf("de dau nhat") > -1) {
                                String str43 = str1.substring(12, str1.indexOf(",x"));
                                String[] str28 = str43.split(",");
                                String str35 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan5 = Laydan + "de dau nhat:";
                                int j7 = 0;
                                while (j7 < str28.length) {
                                    Laydan5 = Laydan5 + str28[j7] + mang[Integer.parseInt(str28[j7])][3] + ",";
                                    j7++;
                                    str43 = str43;
                                }
                                Laydan = Laydan5 + "x" + str35 + "\n";
                            } else if (str1.indexOf("de dit nhat") > -1) {
                                String str44 = str1.substring(12, str1.indexOf(",x"));
                                String[] str29 = str44.split(",");
                                String str36 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan6 = Laydan + "de dit nhat:";
                                int j8 = 0;
                                while (j8 < str29.length) {
                                    Laydan6 = Laydan6 + str29[j8] + mang[Integer.parseInt(str29[j8])][4] + ",";
                                    j8++;
                                    str44 = str44;
                                }
                                Laydan = Laydan6 + "x" + str36 + "\n";
                            } else if (str1.indexOf("bc dau") > -1) {
                                String str45 = str1.substring(7, str1.indexOf(",x"));
                                String[] str210 = str45.split(",");
                                String str37 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan7 = Laydan + "bc dau:";
                                int j9 = 0;
                                while (j9 < str210.length) {
                                    Laydan7 = Laydan7 + str210[j9] + mang[Integer.parseInt(str210[j9])][7] + ",";
                                    j9++;
                                    str45 = str45;
                                }
                                Laydan = Laydan7 + "x" + str37 + "\n";
                            } else if (str1.indexOf("bc") > -1) {
                                String str46 = str1.substring(3, str1.indexOf(",x"));
                                String[] str211 = str46.split(",");
                                String str38 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan8 = Laydan + "bc:";
                                int j10 = 0;
                                while (j10 < str211.length) {
                                    Laydan8 = Laydan8 + str211[j10] + mang[Integer.parseInt(str211[j10])][5] + ",";
                                    j10++;
                                    str46 = str46;
                                }
                                Laydan = Laydan8 + "x" + str38 + "\n";
                            } else if (str1.indexOf("lo dau") > -1) {
                                String str47 = str1.substring(7, str1.indexOf(",x"));
                                String[] str212 = str47.split(",");
                                String str39 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan9 = Laydan + "lo dau:";
                                int j11 = 0;
                                while (j11 < str212.length) {
                                    Laydan9 = Laydan9 + str212[j11] + mang[Integer.parseInt(str212[j11])][6] + ",";
                                    j11++;
                                    str47 = str47;
                                }
                                Laydan = Laydan9 + "x" + str39 + "\n";
                            } else if (str1.indexOf("lo") > -1) {
                                String str48 = str1.substring(3, str1.indexOf(",x"));
                                String[] str213 = str48.split(",");
                                String str310 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan10 = Laydan + "lo:";
                                int j12 = 0;
                                while (j12 < str213.length) {
                                    Laydan10 = Laydan10 + str213[j12] + mang[Integer.parseInt(str213[j12])][0] + ",";
                                    j12++;
                                    str48 = str48;
                                }
                                Laydan = Laydan10 + "x" + str310 + "\n";
                            } else if (str1.indexOf("xien dau") > -1) {
                                String str49 = str1.substring(9, str1.indexOf(",x"));
                                str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String[] str214 = str49.split(",");
                                boolean check4 = true;
                                int j13 = 0;
                                while (true) {
                                    if (j13 >= str214.length) {
                                        break;
                                    } else if (mang[Integer.parseInt(str214[j13])][6].length() == 0) {
                                        check4 = false;
                                        break;
                                    } else {
                                        j13++;
                                    }
                                }
                                Laydan = check4 ? Laydan + str1.substring(0, str1.indexOf("\n")) + "*\n" : Laydan + str1;
                            } else if (str1.indexOf("xi") > -1) {
                                String str410 = str1.substring(3, str1.indexOf(",x"));
                                str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String[] str215 = str410.split(",");
                                boolean check5 = true;
                                int j14 = 0;
                                while (true) {
                                    if (j14 >= str215.length) {
                                        break;
                                    } else if (mang[Integer.parseInt(str215[j14])][0].length() == 0) {
                                        check5 = false;
                                        break;
                                    } else {
                                        j14++;
                                    }
                                }
                                Laydan = check5 ? Laydan + str1.substring(0, str1.indexOf("\n")) + "*\n" : Laydan + str1;
                            } else if (str1.indexOf("xn") > -1) {
                                String str411 = str1.substring(3, str1.indexOf(",x"));
                                str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String[] str216 = str411.split(",");
                                boolean check6 = false;
                                if (mang[Integer.parseInt(str216[0])][0].length() > 1 || mang[Integer.parseInt(str216[1])][0].length() > 1 || (mang[Integer.parseInt(str216[0])][0].length() > 0 && mang[Integer.parseInt(str216[1])][0].length() > 0)) {
                                    check6 = true;
                                }
                                Laydan = check6 ? Laydan + str1.substring(0, str1.indexOf("\n")) + "*\n" : Laydan + str1;
                            } else if (str1.indexOf("xq dau") > -1) {
                                String str412 = str1.substring(7, str1.indexOf(",x"));
                                String[] str217 = str412.split(",");
                                String str311 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan11 = Laydan + "xq dau:";
                                int j15 = 0;
                                while (j15 < str217.length) {
                                    Laydan11 = Laydan11 + str217[j15] + mang[Integer.parseInt(str217[j15])][6] + ",";
                                    j15++;
                                    str412 = str412;
                                }
                                Laydan = Laydan11 + "x" + str311 + "\n";
                            } else if (str1.indexOf("xq") > -1) {
                                String[] str218 = str1.substring(3, str1.indexOf(",x")).split(",");
                                String str312 = str1.substring(str1.indexOf(",x") + 2, str1.indexOf("\n"));
                                String Laydan12 = Laydan + "xq:";
                                int j16 = 0;
                                while (j16 < str218.length) {
                                    Laydan12 = Laydan12 + str218[j16] + mang[Integer.parseInt(str218[j16])][0] + ",";
                                    j16++;
                                    str1 = str1;
                                }
                                Laydan = Laydan12 + "x" + str312 + "\n";
                            }
                        }
                        str3 = str3;
                        cursor1 = cursor1;
                        k = k2;
                        i12 = i1;
                        i2 = -1;
                    }
                    str2 = str3;
                    QueryData("Update tbl_tinnhanS Set nd_phantich ='" + Laydan + "', tinh_tien = 1 WHERE ID = " + cursor23.getString(0));
                    i2 = -1;
                } else {
                    i2 = -1;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            if (cursor23 != null) {
                cursor23.close();
            }
        }
    }

    public void LayDanhsachKH() {
        MainActivity.DSkhachhang = new ArrayList<>();
        Cursor cursor = GetData("Select * From tbl_kh_new WHERE type_kh <> 2");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                MainActivity.DSkhachhang.add(cursor.getString(1));
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    public void Create_table_Chat() {
        try {
            Cursor cursor = GetData("Select * From Chat_database");
            if (cursor.getColumnCount() == 8) {
                QueryData("Drop table Chat_database");
            }
            cursor.close();
        } catch (Exception e) {
        }
        QueryData("CREATE TABLE IF NOT EXISTS Chat_database( ID INTEGER PRIMARY KEY AUTOINCREMENT,\n ngay_nhan DATE NOT NULL,\n gio_nhan VARCHAR(8) NOT NULL,\n type_kh INTEGER DEFAULT 0,\n ten_kh VARCHAR(20) NOT NULL,\n so_dienthoai VARCHAR(20) NOT NULL,\n use_app VARCHAR(20) NOT NULL,\n nd_goc VARCHAR(500) DEFAULT NULL,\n del_sms INTEGER DEFAULT 0);");
    }

    public void Creat_TinNhanGoc() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_tinnhanS(\n ID INTEGER PRIMARY KEY AUTOINCREMENT,\n ngay_nhan DATE NOT NULL,\n gio_nhan VARCHAR(8) NOT NULL,\n type_kh INTEGER DEFAULT 0,\n ten_kh VARCHAR(20) NOT NULL,\n so_dienthoai VARCHAR(20) NOT NULL,\n use_app VARCHAR(20) NOT NULL,\n so_tin_nhan INTEGER DEFAULT 0,\n nd_goc VARCHAR(500) DEFAULT NULL,\n nd_sua VARCHAR(500) DEFAULT NULL,\n nd_phantich VARCHAR(500) DEFAULT NULL,\n phat_hien_loi VARCHAR(100) DEFAULT NULL,\n tinh_tien INTEGER DEFAULT 0,\n ok_tn INTEGER DEFAULT 0,\n del_sms INTEGER DEFAULT 0,  phan_tich TEXT);");
    }

    public void Creat_SoCT() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_soctS(\n ID INTEGER PRIMARY KEY AUTOINCREMENT,\n ngay_nhan DATE NOT NULL,\n type_kh INTEGER DEFAULT 1,\n ten_kh VARCHAR(20) NOT NULL,\n so_dienthoai VARCHAR(20) NOT NULL,\n so_tin_nhan INTEGER DEFAULT 0,\n the_loai VARCHAR(4) DEFAULT NULL,\n so_chon VARCHAR(20) DEFAULT NULL,\n diem DOUBLE DEFAULT 0,\n diem_quydoi DOUBLE DEFAULT 0,\n diem_khachgiu DOUBLE DEFAULT 0,\n diem_dly_giu DOUBLE DEFAULT 0,\n diem_ton DOUBLE DEFAULT 0,\n gia DOUBLE DEFAULT 0,\n lan_an DOUBLE DEFAULT 0,\n so_nhay DOUBLE DEFAULT 0,\n tong_tien DOUBLE DEFAULT 0,\n ket_qua DOUBLE DEFAULT 0)");
        QueryData("CREATE TABLE IF NOT EXISTS tbl_chuyenthang ( ID INTEGER PRIMARY KEY AUTOINCREMENT, kh_nhan VARCHAR(20) NOT NULL, sdt_nhan VARCHAR(15) NOT NULL, kh_chuyen VARCHAR(20) NOT NULL, sdt_chuyen VARCHAR(15) NOT NULL)");
    }

    public void Creat_So_Om() {
        QueryData("CREATE TABLE IF NOT EXISTS So_om(  ID INTEGER PRIMARY KEY AUTOINCREMENT,  So VARCHAR(2) DEFAULT NULL,  Om_DeA INTEGER DEFAULT 0,  Om_DeB INTEGER DEFAULT 0,  Om_DeC INTEGER DEFAULT 0,  Om_DeD INTEGER DEFAULT 0,  Om_Lo INTEGER Default 0,  Om_Xi2 INTEGER Default 0,  Om_Xi3 INTEGER Default 0,  Om_Xi4 INTEGER Default 0,  Om_bc INTEGER Default 0,  Sphu1 VARCHAR(200) DEFAULT NULL,  Sphu2 VARCHAR(200) DEFAULT NULL)");
    }

    public void Creat_Chaytrang_acc() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_chaytrang_acc( \n Username VARCHAR(30) PRIMARY KEY,\n Password VARCHAR(20) NOT NULL,\n Setting TEXT NOT NULL,\n Status VARCHAR(20) DEFAULT NULL)");
    }

    public void Creat_Chaytrang_ticket() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_chaytrang_ticket( ID INTEGER PRIMARY KEY AUTOINCREMENT, \nngay_nhan DATE NOT NULL, \nCreatedAt VARCHAR(20) DEFAULT NULL, \nUsername VARCHAR(30), \nTicketNumber INTEGER DEFAULT 0, \nGameType INTEGER DEFAULT 0,\nNumbers Text DEFAULT NULL, \nPoint DOUBLE DEFAULT 0, \nAmount DOUBLE DEFAULT 0, \nCancelledAt INTEGER DEFAULT 1)");
    }

    public void ThayThePhu() {
        QueryData("CREATE TABLE IF NOT EXISTS thay_the_phu(  ID INTEGER PRIMARY KEY AUTOINCREMENT,  str VARCHAR(20) NOT NULL,  str_rpl VARCHAR(20) NOT NULL)");
    }

    public void List_Khach_Hang() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_kh_new (ten_kh VARCHAR(30) PRIMARY KEY,sdt VARCHAR(15),use_app Varchar(10), type_kh INTEGER default 0, type_pt Integer default 0, tbl_MB TEXT, tbl_XS TEXT)");
        QueryData("Delete From tbl_kh_new Where substr(sdt,0,3) = 'TL'");
    }

    public void Another_setting() {
        QueryData("CREATE TABLE IF NOT EXISTS tbl_Setting(\n ID INTEGER PRIMARY KEY AUTOINCREMENT,\n Setting TEXT)");
        Cursor cursor = GetData("SELECT * FROM 'tbl_Setting'");
        if (cursor.getCount() == 0) {
            JSONObject setting = new JSONObject();
            try {
                setting.put("ap_man", 0);
                setting.put("chuyen_xien", 0);
                setting.put("lam_tron", 0);
                setting.put("gioi_han_tin", 1);
                setting.put("tin_qua_gio", 0);
                setting.put("tin_trung", 0);
                setting.put("kieu_bao_cao", 0);
                setting.put("bao_cao_so", 0);
                setting.put("tra_thuong_lo", 0);
                setting.put("canhbaodonvi", 0);
                setting.put("tudongxuly", 0);
                setting.put("tachxien_tinchot", 0);
                setting.put("baotinthieu", 0);
                QueryData("insert into tbl_Setting Values( null,'" + setting.toString() + "')");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            cursor.close();
            return;
        }
        try {
            cursor.moveToFirst();
            JSONObject setting2 = new JSONObject(cursor.getString(1));
            if (!setting2.has("canhbaodonvi")) {
                setting2.put("canhbaodonvi", 0);
            }
            if (!setting2.has("tachxien_tinchot")) {
                setting2.put("tachxien_tinchot", 0);
            }
            if (!setting2.has("baotinthieu")) {
                setting2.put("baotinthieu", 0);
            }
            QueryData("Update tbl_Setting set Setting = '" + setting2.toString() + "'");
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void Save_Setting(String Keys, int values) {
        Cursor cursor = GetData("Select * From tbl_Setting WHERE ID = 1");
        if (cursor != null && cursor.moveToFirst()) {
            try {
                MainActivity.jSon_Setting.put(Keys, values);
                QueryData("Update tbl_Setting set Setting = '" + MainActivity.jSon_Setting.toString() + "'");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        cursor.close();
    }

    public void Bang_KQ() {
        QueryData("CREATE TABLE IF NOT EXISTS KetQua(  ID INTEGER PRIMARY KEY AUTOINCREMENT,  Ngay DATE DEFAULT NULL,  GDB VARCHAR(5) DEFAULT NULL,  G11 VARCHAR(5) DEFAULT NULL,  G21 VARCHAR(5) DEFAULT NULL,  G22 VARCHAR(5) DEFAULT NULL,  G31 VARCHAR(5) DEFAULT NULL,  G32 VARCHAR(5) DEFAULT NULL,  G33 VARCHAR(5) DEFAULT NULL,  G34 VARCHAR(5) DEFAULT NULL,  G35 VARCHAR(5) DEFAULT NULL,  G36 VARCHAR(5) DEFAULT NULL,  G41 VARCHAR(4) DEFAULT NULL,  G42 VARCHAR(4) DEFAULT NULL,  G43 VARCHAR(4) DEFAULT NULL,  G44 VARCHAR(4) DEFAULT NULL,  G51 VARCHAR(4) DEFAULT NULL,  G52 VARCHAR(4) DEFAULT NULL,  G53 VARCHAR(4) DEFAULT NULL,  G54 VARCHAR(4) DEFAULT NULL,  G55 VARCHAR(4) DEFAULT NULL,  G56 VARCHAR(4) DEFAULT NULL,  G61 VARCHAR(3) DEFAULT NULL,  G62 VARCHAR(3) DEFAULT NULL,  G63 VARCHAR(3) DEFAULT NULL,  G71 VARCHAR(2) DEFAULT NULL,  G72 VARCHAR(2) DEFAULT NULL,  G73 VARCHAR(2) DEFAULT NULL,  G74 VARCHAR(2) DEFAULT NULL);");
    }

    public Cursor GetData(String sql) {
        SQLiteDatabase db2 = getReadableDatabase();
        return db2.rawQuery(sql, null);
    }

    public void QueryData(String sql) {
        SQLiteDatabase db2 = getWritableDatabase();
        db2.execSQL(sql);
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    @Override // android.database.sqlite.SQLiteOpenHelper
    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
        if (oldVersion > 1) {
        }
    }
}
