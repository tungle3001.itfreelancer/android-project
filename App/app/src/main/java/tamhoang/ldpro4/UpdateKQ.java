package tamhoang.ldpro4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class UpdateKQ extends BroadcastReceiver {
    Database db;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        this.db = new Database(context);
        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        String mGio_Nhan = hourFormat.format(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("dd-MM-yyyy");
        if ((Integer.valueOf(mGio_Nhan.substring(0, 2)).intValue() <= 17 || Integer.valueOf(mGio_Nhan.substring(3, 5)).intValue() <= 18) && Integer.valueOf(mGio_Nhan.substring(0, 2)).intValue() <= 18) {
            Calendar cal = Calendar.getInstance();
            cal.add(5, -1);
            dmyFormat.format(cal.getTime());
            return;
        }
        Calendar cal2 = Calendar.getInstance();
        cal2.add(5, 0);
        dmyFormat.format(cal2.getTime());
    }
}
