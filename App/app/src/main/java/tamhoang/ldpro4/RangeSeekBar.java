package tamhoang.ldpro4;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import java.lang.Number;
import java.math.BigDecimal;

/* loaded from: classes2.dex */
public class RangeSeekBar<T extends Number> extends ImageView {
    public static final int ACTION_POINTER_INDEX_MASK = 65280;
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;
    public static final int ACTION_POINTER_UP = 6;
    private static final int DEFAULT_TEXT_DISTANCE_TO_BUTTON_IN_DP = 8;
    private static final int DEFAULT_TEXT_DISTANCE_TO_TOP_IN_DP = 8;
    private static final int DEFAULT_TEXT_SIZE_IN_DP = 14;
    public static final int HEIGHT_IN_DP = 30;
    private static final int INITIAL_PADDING_IN_DP = 8;
    public static final int INVALID_POINTER_ID = 255;
    public static final int TEXT_LATERAL_PADDING_IN_DP = 3;
    private float INITIAL_PADDING;
    private T absoluteMaxValue;
    private double absoluteMaxValuePrim;
    private T absoluteMinValue;
    private double absoluteMinValuePrim;
    private OnRangeSeekBarChangeListener<T> listener;
    private int mDistanceToTop;
    private float mDownMotionX;
    private boolean mIsDragging;
    private RectF mRect;
    private int mScaledTouchSlop;
    private boolean mSingleThumb;
    private int mTextOffset;
    private int mTextSize;
    private NumberType numberType;
    private float padding;
    private final float thumbHalfWidth;
    private final float thumbWidth;
    public static final Integer DEFAULT_MINIMUM = 0;
    public static final Integer DEFAULT_MAXIMUM = 100;
    public static final int DEFAULT_COLOR = Color.argb(255, 51, 181, 229);
    private final int LINE_HEIGHT_IN_DP = 1;
    private final Paint paint = new Paint(1);
    private final Bitmap thumbImage = BitmapFactory.decodeResource(getResources(), R.drawable.seek_thumb_normal);
    private final Bitmap thumbPressedImage = BitmapFactory.decodeResource(getResources(), R.drawable.seek_thumb_pressed);
    private final Bitmap thumbDisabledImage = BitmapFactory.decodeResource(getResources(), R.drawable.seek_thumb_disabled);
    private final float thumbHalfHeight = this.thumbImage.getHeight() * 0.5f;
    private double normalizedMinValue = 0.0d;
    private double normalizedMaxValue = 1.0d;
    private Thumb pressedThumb = null;
    private boolean notifyWhileDragging = false;
    private int mActivePointerId = 255;

    /* loaded from: classes2.dex */
    public interface OnRangeSeekBarChangeListener<T> {
        void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, T t, T t2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: classes2.dex */
    public enum Thumb {
        MIN,
        MAX
    }

    public RangeSeekBar(Context context) {
        super(context);
        float width = this.thumbImage.getWidth();
        this.thumbWidth = width;
        this.thumbHalfWidth = width * 0.5f;
        init(context, null);
    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        float width = this.thumbImage.getWidth();
        this.thumbWidth = width;
        this.thumbHalfWidth = width * 0.5f;
        init(context, attrs);
    }

    public RangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        float width = this.thumbImage.getWidth();
        this.thumbWidth = width;
        this.thumbHalfWidth = width * 0.5f;
        init(context, attrs);
    }

    private T extractNumericValueFromAttributes(TypedArray a, int attribute, int defaultValue) {
        TypedValue tv = a.peekValue(attribute);
        if (tv == null) {
            return Integer.valueOf(defaultValue);
        }
        int type = tv.type;
        if (type == 4) {
            return Float.valueOf(a.getFloat(attribute, defaultValue));
        }
        return Integer.valueOf(a.getInteger(attribute, defaultValue));
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs == null) {
            setRangeToDefaultValues();
        } else {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.RangeSeekBar, 0, 0);
            setRangeValues(extractNumericValueFromAttributes(a, 1, DEFAULT_MINIMUM.intValue()), extractNumericValueFromAttributes(a, 0, DEFAULT_MAXIMUM.intValue()));
            this.mSingleThumb = a.getBoolean(2, false);
            a.recycle();
        }
        setValuePrimAndNumberType();
        this.INITIAL_PADDING = PixelUtil.dpToPx(context, 8);
        this.mTextSize = PixelUtil.dpToPx(context, 14);
        this.mDistanceToTop = PixelUtil.dpToPx(context, 8);
        this.mTextOffset = this.mTextSize + PixelUtil.dpToPx(context, 8) + this.mDistanceToTop;
        float lineHeight = PixelUtil.dpToPx(context, 1);
        this.mRect = new RectF(this.padding, (this.mTextOffset + this.thumbHalfHeight) - (lineHeight / 2.0f), getWidth() - this.padding, this.mTextOffset + this.thumbHalfHeight + (lineHeight / 2.0f));
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public void setRangeValues(T minValue, T maxValue) {
        this.absoluteMinValue = minValue;
        this.absoluteMaxValue = maxValue;
        setValuePrimAndNumberType();
    }

    private void setRangeToDefaultValues() {
        this.absoluteMinValue = DEFAULT_MINIMUM;
        this.absoluteMaxValue = DEFAULT_MAXIMUM;
        setValuePrimAndNumberType();
    }

    private void setValuePrimAndNumberType() {
        this.absoluteMinValuePrim = this.absoluteMinValue.doubleValue();
        this.absoluteMaxValuePrim = this.absoluteMaxValue.doubleValue();
        this.numberType = NumberType.fromNumber(this.absoluteMinValue);
    }

    public void resetSelectedValues() {
        setSelectedMinValue(this.absoluteMinValue);
        setSelectedMaxValue(this.absoluteMaxValue);
    }

    public boolean isNotifyWhileDragging() {
        return this.notifyWhileDragging;
    }

    public void setNotifyWhileDragging(boolean flag) {
        this.notifyWhileDragging = flag;
    }

    public T getAbsoluteMinValue() {
        return this.absoluteMinValue;
    }

    public T getAbsoluteMaxValue() {
        return this.absoluteMaxValue;
    }

    public T getSelectedMinValue() {
        return normalizedToValue(this.normalizedMinValue);
    }

    public void setSelectedMinValue(T value) {
        if (0.0d == this.absoluteMaxValuePrim - this.absoluteMinValuePrim) {
            setNormalizedMinValue(0.0d);
        } else {
            setNormalizedMinValue(valueToNormalized(value));
        }
    }

    public T getSelectedMaxValue() {
        return normalizedToValue(this.normalizedMaxValue);
    }

    public void setSelectedMaxValue(T value) {
        if (0.0d == this.absoluteMaxValuePrim - this.absoluteMinValuePrim) {
            setNormalizedMaxValue(1.0d);
        } else {
            setNormalizedMaxValue(valueToNormalized(value));
        }
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener<T> listener) {
        this.listener = listener;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent event) {
        OnRangeSeekBarChangeListener<T> onRangeSeekBarChangeListener;
        if (!isEnabled()) {
            return false;
        }
        int action = event.getAction();
        int i = action & 255;
        if (i == 0) {
            int pointerId = event.getPointerId(event.getPointerCount() - 1);
            this.mActivePointerId = pointerId;
            int pointerIndex = event.findPointerIndex(pointerId);
            float x = event.getX(pointerIndex);
            this.mDownMotionX = x;
            Thumb evalPressedThumb = evalPressedThumb(x);
            this.pressedThumb = evalPressedThumb;
            if (evalPressedThumb == null) {
                return super.onTouchEvent(event);
            }
            setPressed(true);
            invalidate();
            onStartTrackingTouch();
            trackTouchEvent(event);
            attemptClaimDrag();
        } else if (i == 1) {
            if (this.mIsDragging) {
                trackTouchEvent(event);
                onStopTrackingTouch();
                setPressed(false);
            } else {
                onStartTrackingTouch();
                trackTouchEvent(event);
                onStopTrackingTouch();
            }
            this.pressedThumb = null;
            invalidate();
            OnRangeSeekBarChangeListener<T> onRangeSeekBarChangeListener2 = this.listener;
            if (onRangeSeekBarChangeListener2 != null) {
                onRangeSeekBarChangeListener2.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue());
            }
        } else if (i != 2) {
            if (i == 3) {
                if (this.mIsDragging) {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate();
            } else if (i == 5) {
                int index = event.getPointerCount() - 1;
                this.mDownMotionX = event.getX(index);
                this.mActivePointerId = event.getPointerId(index);
                invalidate();
            } else if (i == 6) {
                onSecondaryPointerUp(event);
                invalidate();
            }
        } else if (this.pressedThumb != null) {
            if (this.mIsDragging) {
                trackTouchEvent(event);
            } else {
                int pointerIndex2 = event.findPointerIndex(this.mActivePointerId);
                float x2 = event.getX(pointerIndex2);
                if (Math.abs(x2 - this.mDownMotionX) > this.mScaledTouchSlop) {
                    setPressed(true);
                    invalidate();
                    onStartTrackingTouch();
                    trackTouchEvent(event);
                    attemptClaimDrag();
                }
            }
            if (this.notifyWhileDragging && (onRangeSeekBarChangeListener = this.listener) != null) {
                onRangeSeekBarChangeListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue());
            }
        }
        return true;
    }

    private final void onSecondaryPointerUp(MotionEvent ev) {
        int pointerIndex = (ev.getAction() & 65280) >> 8;
        int pointerId = ev.getPointerId(pointerIndex);
        if (pointerId == this.mActivePointerId) {
            int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            this.mDownMotionX = ev.getX(newPointerIndex);
            this.mActivePointerId = ev.getPointerId(newPointerIndex);
        }
    }

    private final void trackTouchEvent(MotionEvent event) {
        int pointerIndex = event.findPointerIndex(this.mActivePointerId);
        float x = event.getX(pointerIndex);
        if (Thumb.MIN.equals(this.pressedThumb) && !this.mSingleThumb) {
            setNormalizedMinValue(screenToNormalized(x));
        } else if (Thumb.MAX.equals(this.pressedThumb)) {
            setNormalizedMaxValue(screenToNormalized(x));
        }
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    void onStartTrackingTouch() {
        this.mIsDragging = true;
    }

    void onStopTrackingTouch() {
        this.mIsDragging = false;
    }

    @Override // android.widget.ImageView, android.view.View
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        if (MeasureSpec.getMode(widthMeasureSpec) != 0) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        int height = this.thumbImage.getHeight() + PixelUtil.dpToPx(getContext(), 30);
        if (MeasureSpec.getMode(heightMeasureSpec) != 0) {
            height = Math.min(height, MeasureSpec.getSize(heightMeasureSpec));
        }
        setMeasuredDimension(width, height);
    }

    @Override // android.widget.ImageView, android.view.View
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.paint.setTextSize(this.mTextSize);
        this.paint.setStyle(Paint.Style.FILL);
        int colorToUseForButtonsAndHighlightedLine = -7829368;
        this.paint.setColor(-7829368);
        boolean selectedValuesAreDefault = true;
        this.paint.setAntiAlias(true);
        String minLabel = getContext().getString(R.string.demo_min_label);
        String maxLabel = getContext().getString(R.string.demo_max_label);
        float minMaxLabelSize = Math.max(this.paint.measureText(minLabel), this.paint.measureText(maxLabel));
        float minMaxHeight = this.mTextOffset + this.thumbHalfHeight + (this.mTextSize / 3);
        canvas.drawText(minLabel, 0.0f, minMaxHeight, this.paint);
        canvas.drawText(maxLabel, getWidth() - minMaxLabelSize, minMaxHeight, this.paint);
        float f = this.INITIAL_PADDING + minMaxLabelSize + this.thumbHalfWidth;
        this.padding = f;
        this.mRect.left = f;
        this.mRect.right = getWidth() - this.padding;
        canvas.drawRect(this.mRect, this.paint);
        if (!getSelectedMinValue().equals(getAbsoluteMinValue()) || !getSelectedMaxValue().equals(getAbsoluteMaxValue())) {
            selectedValuesAreDefault = false;
        }
        if (!selectedValuesAreDefault) {
            colorToUseForButtonsAndHighlightedLine = DEFAULT_COLOR;
        }
        this.mRect.left = normalizedToScreen(this.normalizedMinValue);
        this.mRect.right = normalizedToScreen(this.normalizedMaxValue);
        this.paint.setColor(colorToUseForButtonsAndHighlightedLine);
        canvas.drawRect(this.mRect, this.paint);
        if (!this.mSingleThumb) {
            drawThumb(normalizedToScreen(this.normalizedMinValue), Thumb.MIN.equals(this.pressedThumb), canvas, selectedValuesAreDefault);
        }
        drawThumb(normalizedToScreen(this.normalizedMaxValue), Thumb.MAX.equals(this.pressedThumb), canvas, selectedValuesAreDefault);
        if (!selectedValuesAreDefault) {
            this.paint.setTextSize(this.mTextSize);
            this.paint.setColor(ViewCompat.MEASURED_STATE_MASK);
            int offset = PixelUtil.dpToPx(getContext(), 3);
            String minText = String.valueOf(getSelectedMinValue());
            String maxText = String.valueOf(getSelectedMaxValue());
            float minTextWidth = this.paint.measureText(minText) + offset;
            float maxTextWidth = this.paint.measureText(maxText) + offset;
            if (!this.mSingleThumb) {
                canvas.drawText(minText, normalizedToScreen(this.normalizedMinValue) - (minTextWidth * 0.5f), this.mDistanceToTop + this.mTextSize, this.paint);
            }
            canvas.drawText(maxText, normalizedToScreen(this.normalizedMaxValue) - (0.5f * maxTextWidth), this.mDistanceToTop + this.mTextSize, this.paint);
        }
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", this.normalizedMinValue);
        bundle.putDouble("MAX", this.normalizedMaxValue);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcel) {
        Bundle bundle = (Bundle) parcel;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        this.normalizedMinValue = bundle.getDouble("MIN");
        this.normalizedMaxValue = bundle.getDouble("MAX");
    }

    private void drawThumb(float screenCoord, boolean pressed, Canvas canvas, boolean areSelectedValuesDefault) {
        Bitmap buttonToDraw;
        if (areSelectedValuesDefault) {
            buttonToDraw = this.thumbDisabledImage;
        } else {
            buttonToDraw = pressed ? this.thumbPressedImage : this.thumbImage;
        }
        canvas.drawBitmap(buttonToDraw, screenCoord - this.thumbHalfWidth, this.mTextOffset, this.paint);
    }

    private Thumb evalPressedThumb(float touchX) {
        boolean minThumbPressed = isInThumbRange(touchX, this.normalizedMinValue);
        boolean maxThumbPressed = isInThumbRange(touchX, this.normalizedMaxValue);
        if (minThumbPressed && maxThumbPressed) {
            Thumb result = touchX / ((float) getWidth()) > 0.5f ? Thumb.MIN : Thumb.MAX;
            return result;
        } else if (minThumbPressed) {
            Thumb result2 = Thumb.MIN;
            return result2;
        } else if (!maxThumbPressed) {
            return null;
        } else {
            Thumb result3 = Thumb.MAX;
            return result3;
        }
    }

    private boolean isInThumbRange(float touchX, double normalizedThumbValue) {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= this.thumbHalfWidth;
    }

    private void setNormalizedMinValue(double value) {
        this.normalizedMinValue = Math.max(0.0d, Math.min(1.0d, Math.min(value, this.normalizedMaxValue)));
        invalidate();
    }

    private void setNormalizedMaxValue(double value) {
        this.normalizedMaxValue = Math.max(0.0d, Math.min(1.0d, Math.max(value, this.normalizedMinValue)));
        invalidate();
    }

    private T normalizedToValue(double normalized) {
        double d = this.absoluteMinValuePrim;
        double v = d + ((this.absoluteMaxValuePrim - d) * normalized);
        NumberType numberType = this.numberType;
        double round = Math.round(v * 100.0d);
        Double.isNaN(round);
        return (T) numberType.toNumber(round / 100.0d);
    }

    private double valueToNormalized(T value) {
        if (0.0d == this.absoluteMaxValuePrim - this.absoluteMinValuePrim) {
            return 0.0d;
        }
        double doubleValue = value.doubleValue();
        double d = this.absoluteMinValuePrim;
        return (doubleValue - d) / (this.absoluteMaxValuePrim - d);
    }

    private float normalizedToScreen(double normalizedCoord) {
        double d = this.padding;
        double width = getWidth() - (this.padding * 2.0f);
        Double.isNaN(width);
        Double.isNaN(d);
        return (float) (d + (width * normalizedCoord));
    }

    private double screenToNormalized(float screenCoord) {
        int width = getWidth();
        float f = this.padding;
        if (width <= f * 2.0f) {
            return 0.0d;
        }
        double result = (screenCoord - f) / (width - (f * 2.0f));
        return Math.min(1.0d, Math.max(0.0d, result));
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: classes2.dex */
    public enum NumberType {
        LONG,
        DOUBLE,
        INTEGER,
        FLOAT,
        SHORT,
        BYTE,
        BIG_DECIMAL;

        public static <E extends Number> NumberType fromNumber(E value) throws IllegalArgumentException {
            if (value instanceof Long) {
                return LONG;
            }
            if (value instanceof Double) {
                return DOUBLE;
            }
            if (value instanceof Integer) {
                return INTEGER;
            }
            if (value instanceof Float) {
                return FLOAT;
            }
            if (value instanceof Short) {
                return SHORT;
            }
            if (value instanceof Byte) {
                return BYTE;
            }
            if (value instanceof BigDecimal) {
                return BIG_DECIMAL;
            }
            throw new IllegalArgumentException("Number class '" + value.getClass().getName() + "' is not supported");
        }

        public Number toNumber(double value) {
            switch (AnonymousClass1.$SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[ordinal()]) {
                case 1:
                    return Long.valueOf((long) value);
                case 2:
                    return Double.valueOf(value);
                case 3:
                    return Integer.valueOf((int) value);
                case 4:
                    return Float.valueOf((float) value);
                case 5:
                    return Short.valueOf((short) value);
                case 6:
                    return Byte.valueOf((byte) value);
                case 7:
                    return BigDecimal.valueOf(value);
                default:
                    throw new InstantiationError("can't convert " + this + " to a Number object");
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* renamed from: tamhoang.ldpro4.RangeSeekBar$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType;

        static {
            int[] iArr = new int[NumberType.values().length];
            $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType = iArr;
            try {
                iArr[NumberType.LONG.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.DOUBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.INTEGER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.SHORT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.BYTE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$tamhoang$ldpro4$RangeSeekBar$NumberType[NumberType.BIG_DECIMAL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }
}
