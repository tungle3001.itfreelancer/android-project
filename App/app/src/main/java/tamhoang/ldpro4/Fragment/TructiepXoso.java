package tamhoang.ldpro4.Fragment;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.text.Html;
import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.NotificationBindObject;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class TructiepXoso extends Fragment {
    public static final String EXTRA_FROM_NOTIFICATION = "EXTRA_FROM_NOTIFICATION";
    Switch Switch1;
    Database db;
    Handler handler;
    List<JSONObject> jsonValues;
    ListView listView;
    WebView mWebView;
    RadioButton rdb_XemLo;
    RadioButton rdb_XemXien;
    View v;
    ArrayList<String> listSo = new ArrayList<>();
    String DangXuat = "lo";
    String mDate = "";
    int So_giai = 0;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.6
        @Override // java.lang.Runnable
        public void run() {
            if (TructiepXoso.this.listSo.size() > 26) {
                TructiepXoso.this.handler.removeCallbacks(TructiepXoso.this.runnable);
                return;
            }
            TructiepXoso tructiepXoso = TructiepXoso.this;
            tructiepXoso.loadJavascript("(function() { return document.getElementsByClassName('firstlast-mb fl')[0].innerText;; })();");
            TructiepXoso.this.handler.postDelayed(this, 2000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.v = inflater.inflate(R.layout.tructiepxoso, container, false);
        this.db = new Database(getActivity());
        init();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        dmyFormat.setTimeZone(TimeZone.getDefault());
        this.Switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (TructiepXoso.this.Switch1.isChecked()) {
                    TructiepXoso.this.mWebView.setVisibility(0);
                } else {
                    TructiepXoso.this.mWebView.setVisibility(8);
                }
            }
        });
        this.rdb_XemLo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (TructiepXoso.this.rdb_XemLo.isChecked()) {
                    TructiepXoso.this.DangXuat = "lo";
                    TructiepXoso.this.Xem_lv();
                }
            }
        });
        this.rdb_XemXien.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (TructiepXoso.this.rdb_XemXien.isChecked()) {
                    TructiepXoso.this.DangXuat = "xi";
                    TructiepXoso.this.Xem_lv();
                }
            }
        });
        this.handler = new Handler();
        if (!Congthuc.CheckTime("18:14") || Congthuc.CheckTime("24:30")) {
            this.mWebView.setVisibility(8);
        } else {
            this.Switch1.setText("Ẩn/hiện bảng Kết quả");
            this.handler.postDelayed(this.runnable, 3000L);
            this.mWebView.setVisibility(8);
        }
        this.mWebView.addJavascriptInterface(new NotificationBindObject(getActivity().getApplicationContext()), "NotificationBind");
        setUpWebViewDefaults(this.mWebView);
        if (savedInstanceState != null) {
            this.mWebView.restoreState(savedInstanceState);
        }
        if (this.mWebView.getUrl() == null) {
            this.mWebView.loadUrl("https://xoso.me/embedded/kq-mienbac");
        }
        this.mWebView.setWebViewClient(new WebViewClient() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.4
            @Override // android.webkit.WebViewClient
            public void onPageFinished(WebView view, String url) {
                TructiepXoso.this.loadJavascript("document.getElementsByClassName('embeded-breadcrumb')[0].style.display = 'none';\ndocument.getElementsByClassName('tit-mien')[0].style.display = 'none';");
                TructiepXoso.this.mWebView.setVisibility(0);
                TructiepXoso.this.Switch1.setEnabled(true);
            }
        });
        this.mWebView.setEnabled(false);
        Xem_lv();
        return this.v;
    }

    private void setUpWebViewDefaults(WebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        if (Build.VERSION.SDK_INT > 11) {
            settings.setDisplayZoomControls(false);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    public void loadJavascript(String javascript) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.mWebView.evaluateJavascript(javascript, new ValueCallback<String>() { // from class: tamhoang.ldpro4.Fragment.TructiepXoso.5
                /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:41:0x0111 -> B:46:0x0121). Please submit an issue!!! */
                public void onReceiveValue(String s) {
                    String msg;
                    JsonReader reader = new JsonReader(new StringReader(s));
                    reader.setLenient(true);
                    try {
                        try {
                            try {
                                if (reader.peek() != JsonToken.NULL && reader.peek() == JsonToken.STRING && (msg = reader.nextString()) != null && msg.indexOf("\n") > -1) {
                                    String[] SSS = msg.substring(msg.indexOf("0")).split("\n");
                                    for (int i = 0; i < SSS.length; i++) {
                                        if (SSS[i].length() > 2) {
                                            SSS[i] = SSS[i].substring(2);
                                        } else {
                                            SSS[i] = "";
                                        }
                                    }
                                    int i2 = SSS.length;
                                    if (i2 == 10) {
                                        TructiepXoso.this.listSo = new ArrayList<>();
                                        for (int i3 = 0; i3 < SSS.length; i3++) {
                                            String[] Sodit = SSS[i3].replaceAll(" ", "").split(",");
                                            for (int j = 0; j < Sodit.length; j++) {
                                                if (Sodit[j].length() == 1) {
                                                    ArrayList<String> arrayList = TructiepXoso.this.listSo;
                                                    arrayList.add(i3 + Sodit[j]);
                                                } else if (Sodit[j].length() == 2) {
                                                    ArrayList<String> arrayList2 = TructiepXoso.this.listSo;
                                                    arrayList2.add(i3 + Sodit[j].substring(1));
                                                }
                                            }
                                        }
                                        if (TructiepXoso.this.listSo.size() != TructiepXoso.this.So_giai) {
                                            TructiepXoso.this.TinhTienTuDong(TructiepXoso.this.listSo);
                                            TructiepXoso.this.Xem_lv();
                                            TructiepXoso.this.So_giai = TructiepXoso.this.listSo.size();
                                        }
                                    } else {
                                        Toast.makeText(TructiepXoso.this.getActivity(), "Trang xoso.me đang bị lỗi!", 1).show();
                                        TructiepXoso.this.handler.removeCallbacks(TructiepXoso.this.runnable);
                                    }
                                }
                                reader.close();
                            } catch (IOException e) {
                                Log.e("TAG", "MainActivity: IOException", e);
                                reader.close();
                            }
                        } catch (Throwable th) {
                            try {
                                reader.close();
                            } catch (IOException e2) {
                            }
                            throw th;
                        }
                    } catch (IOException e3) {
                    }
                }
            });
            return;
        }
        WebView webView = this.mWebView;
        webView.loadUrl("javascript:" + javascript);
    }

    @Override // android.support.v4.app.Fragment
    public void onStop() {
        super.onStop();
        this.mWebView.clearCache(true);
        this.handler.removeCallbacks(this.runnable);
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        super.onResume();
        if (Congthuc.CheckTime("18:14") && !Congthuc.CheckTime("18:30") && isNetworkConnected()) {
            this.Switch1.setText("Ẩn/hiện bảng Kết quả");
            this.handler.postDelayed(this.runnable, 3000L);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void TinhTienTuDong(ArrayList<String> ArraySo) {
        Database database = this.db;
        database.QueryData("Update tbl_soctS Set so_nhay = 0, ket_qua = 0 WHERE ngay_nhan = '" + this.mDate + "' AND the_loai <> 'tt' AND the_loai <> 'cn'");
        String Ketqua = "";
        for (int i = 0; i < ArraySo.size(); i++) {
            Database database2 = this.db;
            database2.QueryData("Update tbl_soctS Set so_nhay = so_nhay + 1 Where the_loai = 'lo' and ngay_nhan = '" + this.mDate + "' And so_chon ='" + ArraySo.get(i) + "'");
            StringBuilder sb = new StringBuilder();
            sb.append(Ketqua);
            sb.append(ArraySo.get(i));
            sb.append(",");
            Ketqua = sb.toString();
        }
        Database database3 = this.db;
        Cursor cursor = database3.GetData("Select * From tbl_soctS Where ngay_nhan = '" + this.mDate + "' AND the_loai = 'xi'");
        while (cursor.moveToNext()) {
            String[] str2 = cursor.getString(7).split(",");
            boolean check = true;
            int j = 0;
            while (true) {
                if (j >= str2.length) {
                    break;
                } else if (Ketqua.indexOf(str2[j]) == -1) {
                    check = false;
                    break;
                } else {
                    j++;
                }
            }
            if (check) {
                Database database4 = this.db;
                database4.QueryData("Update tbl_soctS Set so_nhay = 1 WHERE ID = " + cursor.getString(0));
            }
        }
        Database database5 = this.db;
        database5.QueryData("Update tbl_soctS set ket_qua = diem * lan_an * so_nhay - tong_tien WHERE ngay_nhan = '" + this.mDate + "' AND type_kh = 1 AND the_loai <> 'tt' AND the_loai <> 'cn'");
        Database database6 = this.db;
        database6.QueryData("Update tbl_soctS set ket_qua = -diem * lan_an * so_nhay + tong_tien WHERE ngay_nhan = '" + this.mDate + "' AND type_kh = 2 AND the_loai <> 'tt' AND the_loai <> 'cn'");
    }

    /* JADX WARN: Removed duplicated region for block: B:101:0x022e  */
    /* JADX WARN: Removed duplicated region for block: B:119:? A[RETURN, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:95:0x021a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void Xem_lv() {
        /*
            Method dump skipped, instructions count: 578
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.TructiepXoso.Xem_lv():void");
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class So_OmAdapter extends ArrayAdapter {
        public So_OmAdapter(Context context, int resource, List<JSONObject> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView tview1;
            TextView tview2;
            TextView tview4;
            TextView tview5;
            TextView tview7;
            TextView tview8;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View mView, ViewGroup parent) {
            ViewHolder holder;
            if (mView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                mView = inflater.inflate(R.layout.frag_canchuyen_lv, (ViewGroup) null);
                holder = new ViewHolder();
                holder.tview5 = (TextView) mView.findViewById(R.id.Tv_so);
                holder.tview7 = (TextView) mView.findViewById(R.id.tv_diemNhan);
                holder.tview8 = (TextView) mView.findViewById(R.id.tv_diemOm);
                holder.tview1 = (TextView) mView.findViewById(R.id.tv_diemChuyen);
                holder.tview4 = (TextView) mView.findViewById(R.id.tv_diemTon);
                holder.tview2 = (TextView) mView.findViewById(R.id.stt);
                mView.setTag(holder);
            } else {
                holder = (ViewHolder) mView.getTag();
            }
            JSONObject Json = TructiepXoso.this.jsonValues.get(position);
            try {
                if (Json.getInt("so_nhay") > 0) {
                    holder.tview5.setTextColor(SupportMenu.CATEGORY_MASK);
                    holder.tview7.setTextColor(SupportMenu.CATEGORY_MASK);
                    holder.tview8.setTextColor(SupportMenu.CATEGORY_MASK);
                    holder.tview1.setTextColor(SupportMenu.CATEGORY_MASK);
                    holder.tview4.setTextColor(SupportMenu.CATEGORY_MASK);
                    if (Json.getInt("so_nhay") == 1) {
                        TextView textView = holder.tview5;
                        textView.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "*");
                    } else if (Json.getInt("so_nhay") == 2) {
                        TextView textView2 = holder.tview5;
                        textView2.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "**");
                    } else if (Json.getInt("so_nhay") == 3) {
                        TextView textView3 = holder.tview5;
                        textView3.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "***");
                    } else if (Json.getInt("so_nhay") == 4) {
                        TextView textView4 = holder.tview5;
                        textView4.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "****");
                    } else if (Json.getInt("so_nhay") == 5) {
                        TextView textView5 = holder.tview5;
                        textView5.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "*****");
                    } else if (Json.getInt("so_nhay") == 6) {
                        TextView textView6 = holder.tview5;
                        textView6.setText(((Object) Html.fromHtml(Json.getString("so_chon"))) + "******");
                    }
                    holder.tview7.setText(Json.getString("tien_nhan"));
                    holder.tview8.setText(Json.getString("tien_om"));
                    holder.tview1.setText(Json.getString("tien_chuyen"));
                    holder.tview4.setText(Json.getString("tien_ton"));
                    TextView textView7 = holder.tview2;
                    textView7.setText((position + 1) + "");
                } else {
                    holder.tview5.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                    holder.tview7.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                    holder.tview8.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                    holder.tview1.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                    holder.tview4.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                    holder.tview5.setText(Html.fromHtml(Json.getString("so_chon")));
                    holder.tview7.setText(Json.getString("tien_nhan"));
                    holder.tview8.setText(Json.getString("tien_om"));
                    holder.tview1.setText(Json.getString("tien_chuyen"));
                    holder.tview4.setText(Json.getString("tien_ton"));
                    TextView textView8 = holder.tview2;
                    textView8.setText((position + 1) + "");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return mView;
        }
    }

    public void init() {
        this.Switch1 = (Switch) this.v.findViewById(R.id.Switch1);
        this.rdb_XemLo = (RadioButton) this.v.findViewById(R.id.rdb_XemLo);
        this.rdb_XemXien = (RadioButton) this.v.findViewById(R.id.rdb_XemXien);
        this.listView = (ListView) this.v.findViewById(R.id.ListviewTructiep);
        this.mWebView = (WebView) this.v.findViewById(R.id.fragment_main_webview);
    }
}
