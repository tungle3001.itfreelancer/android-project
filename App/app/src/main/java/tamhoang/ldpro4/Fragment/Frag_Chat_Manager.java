package tamhoang.ldpro4.Fragment;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Activity.Activity_AddKH;
import tamhoang.ldpro4.Activity.Chatbox;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.NotificationReader;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_Chat_Manager extends Fragment {
    Button btn_Thongbao;
    Button btn_login;
    Database db;
    Handler handler;
    ListView listviewKH;
    public View v;
    private List<String> mTenKH = new ArrayList();
    private List<String> mSDT = new ArrayList();
    private List<String> mNoiDung = new ArrayList();
    private List<String> mApp = new ArrayList();
    boolean Running = true;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.3
        @Override // java.lang.Runnable
        public void run() {
            if (MainActivity.sms) {
                Frag_Chat_Manager.this.XemListview();
                MainActivity.sms = false;
            }
            Frag_Chat_Manager.this.handler.postDelayed(this, 1000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_chat_manager, container, false);
        this.db = new Database(getActivity());
        this.btn_Thongbao = (Button) this.v.findViewById(R.id.btn_Thongbao);
        this.btn_login = (Button) this.v.findViewById(R.id.btn_login);
        this.listviewKH = (ListView) this.v.findViewById(R.id.listviewKH);
        this.btn_Thongbao.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.1
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 22) {
                    Frag_Chat_Manager.this.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                } else {
                    Frag_Chat_Manager.this.startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                }
            }
        });
        this.listviewKH.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.2
            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(Frag_Chat_Manager.this.getActivity(), Chatbox.class);
                intent.putExtra("tenKH", (String) Frag_Chat_Manager.this.mTenKH.get(i));
                intent.putExtra("so_dienthoai", (String) Frag_Chat_Manager.this.mSDT.get(i));
                intent.putExtra("app", (String) Frag_Chat_Manager.this.mApp.get(i));
                Frag_Chat_Manager.this.startActivity(intent);
            }
        });
        notificationPermission();
        Handler handler = new Handler();
        this.handler = handler;
        handler.postDelayed(this.runnable, 1000L);
        return this.v;
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.runnable);
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        getSMS();
        XemListview();
        super.onResume();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:48:0x0286
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    private void getSMS() {
        /*
            Method dump skipped, instructions count: 821
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.getSMS():void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void XemListview() {
        this.mTenKH.clear();
        this.mNoiDung.clear();
        this.mApp.clear();
        this.mSDT.clear();
        new MainActivity();
        String mDate = MainActivity.Get_date();
        JSONObject jsonObject = new JSONObject();
        Database database = this.db;
        Cursor cursor = database.GetData("SELECT * FROM Chat_database WHERE ngay_nhan = '" + mDate + "' ORDER BY Gio_nhan DESC, ID DESC");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                if (MainActivity.arr_TenKH.indexOf(cursor.getString(4)) > -1 || cursor.getString(6).indexOf("sms") > -1 || cursor.getString(6).indexOf("TL") > -1) {
                    if (!jsonObject.has(cursor.getString(4))) {
                        try {
                            jsonObject.put(cursor.getString(4), "OK");
                            this.mTenKH.add(cursor.getString(4));
                            this.mSDT.add(cursor.getString(5));
                            this.mApp.add(cursor.getString(6));
                            this.mNoiDung.add(cursor.getString(7));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            cursor.close();
        }
        Iterator<String> it = MainActivity.arr_TenKH.iterator();
        while (it.hasNext()) {
            String ten_kh = it.next();
            if (this.mTenKH.indexOf(ten_kh) == -1) {
                this.mTenKH.add(ten_kh);
                this.mNoiDung.add("Hôm nay chưa có tin nhắn!");
                if (ten_kh.indexOf("ZL") > -1) {
                    this.mApp.add("ZL");
                } else if (ten_kh.indexOf("VB") > -1) {
                    this.mApp.add("VB");
                } else if (ten_kh.indexOf("WA") > -1) {
                    this.mApp.add("WA");
                }
            }
        }
        if (getActivity() != null) {
            this.listviewKH.setAdapter((ListAdapter) new Chat_Main(getActivity(), R.layout.frag_chat_manager_lv, this.mTenKH));
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class Chat_Main extends ArrayAdapter {
        public Chat_Main(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView TenKH;
            ImageButton add_contacts;
            ImageView imageView;
            TextView ndChat;
            TextView tv_delete;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder holder;
            View view2 = null;
            if (0 == 0) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                view2 = inflater.inflate(R.layout.frag_chat_manager_lv, (ViewGroup) null);
                holder = new ViewHolder();
                holder.add_contacts = (ImageButton) view2.findViewById(R.id.add_contacts);
                holder.tv_delete = (TextView) view2.findViewById(R.id.tv_delete);
                holder.imageView = (ImageView) view2.findViewById(R.id.imv_app);
                holder.TenKH = (TextView) view2.findViewById(R.id.tv_KhachHang);
                holder.ndChat = (TextView) view2.findViewById(R.id.tv_NoiDung);
            } else {
                holder = (ViewHolder) view2.getTag();
            }
            if (((String) Frag_Chat_Manager.this.mApp.get(position)).indexOf("WA") > -1) {
                holder.imageView.setBackgroundResource(R.drawable.whatsapp);
            } else if (((String) Frag_Chat_Manager.this.mApp.get(position)).indexOf("VI") > -1) {
                holder.imageView.setBackgroundResource(R.drawable.viicon);
            } else if (((String) Frag_Chat_Manager.this.mApp.get(position)).indexOf("ZL") > -1) {
                holder.imageView.setBackgroundResource(R.drawable.zaloicon);
            } else if (((String) Frag_Chat_Manager.this.mApp.get(position)).indexOf("TL") > -1) {
                holder.imageView.setBackgroundResource(R.drawable.tele);
                holder.tv_delete.setVisibility(8);
            } else if (((String) Frag_Chat_Manager.this.mApp.get(position)).indexOf("sms") > -1) {
                holder.imageView.setBackgroundResource(R.drawable.sms1);
                holder.add_contacts.setVisibility(8);
                holder.tv_delete.setVisibility(8);
            }
            holder.add_contacts.setFocusable(false);
            holder.add_contacts.setFocusableInTouchMode(false);
            holder.add_contacts.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.Chat_Main.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view3) {
                    Intent intent = new Intent(Frag_Chat_Manager.this.getActivity(), Activity_AddKH.class);
                    intent.putExtra("tenKH", (String) Frag_Chat_Manager.this.mTenKH.get(position));
                    intent.putExtra("so_dienthoai", (String) Frag_Chat_Manager.this.mSDT.get(position));
                    intent.putExtra("use_app", (String) Frag_Chat_Manager.this.mApp.get(position));
                    Frag_Chat_Manager.this.startActivity(intent);
                }
            });
            if (MainActivity.DSkhachhang.indexOf(Frag_Chat_Manager.this.mTenKH.get(position)) > -1) {
                holder.add_contacts.setVisibility(8);
            }
            holder.tv_delete.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.Chat_Main.2
                @Override // android.view.View.OnClickListener
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Frag_Chat_Manager.this.getActivity());
                    builder.setTitle("Xoá Khách");
                    builder.setMessage("Sẽ xóa hết dữ liệu chat từ khách này, không thể khôi phục và không thể tải lại tin nhắn!");
                    builder.setNegativeButton("Có", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.Chat_Main.2.1
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            int TTkhachhang = MainActivity.arr_TenKH.indexOf(Frag_Chat_Manager.this.mTenKH.get(position));
                            MainActivity.arr_TenKH.remove(TTkhachhang);
                            MainActivity.contactslist.remove(TTkhachhang);
                            Frag_Chat_Manager.this.XemListview();
                            dialog.dismiss();
                            Toast.makeText(Frag_Chat_Manager.this.getActivity(), "Đã xóa!", 1).show();
                        }
                    });
                    builder.setPositiveButton("Không", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.Chat_Main.2.2
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            });
            holder.TenKH.setText((CharSequence) Frag_Chat_Manager.this.mTenKH.get(position));
            holder.ndChat.setText((CharSequence) Frag_Chat_Manager.this.mNoiDung.get(position));
            return view2;
        }
    }

    private void notificationPermission() {
        boolean enabled;
        ComponentName cn = new ComponentName(getActivity(), NotificationReader.class);
        String flat = Settings.Secure.getString(getActivity().getContentResolver(), "enabled_notification_listeners");
        if (flat == null || !flat.contains(cn.flattenToString())) {
            enabled = false;
        } else {
            enabled = true;
        }
        if (!enabled) {
            showAlertBox("Truy cập thông báo!", "Hãy cho phép phần mềm được truy cập thông báo của điện thoại để kích hoạt chức năng nhắn tin.").setPositiveButton("Ok", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.5
                @Override // android.content.DialogInterface.OnClickListener
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (Build.VERSION.SDK_INT >= 22) {
                        Frag_Chat_Manager.this.getActivity().startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                    } else {
                        Frag_Chat_Manager.this.getActivity().startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
                    }
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chat_Manager.4
                @Override // android.content.DialogInterface.OnClickListener
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show().setCanceledOnTouchOutside(false);
        }
    }

    public AlertDialog.Builder showAlertBox(String title, String message) {
        return new AlertDialog.Builder(getActivity()).setTitle(title).setMessage(message);
    }
}
