package tamhoang.ldpro4.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.helper.HttpConnection;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.Fragment.Frag_Chaytrang;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_Chaytrang extends Fragment {
    static long Curent_date_time = 0;
    int MaxChay;
    Button btn_MaXuat;
    Button btn_Xuatso;
    Database db;
    EditText edt_tien;
    Handler handler;
    LinearLayout li_loaide;
    LinearLayout li_loaixi;
    ListView lview;
    RadioButton radio_de;
    RadioButton radio_dea;
    RadioButton radio_deb;
    RadioButton radio_dec;
    RadioButton radio_ded;
    RadioButton radio_lo;
    RadioButton radio_xi;
    RadioButton radio_xi2;
    RadioButton radio_xi3;
    RadioButton radio_xi4;
    Spinner spr_trang;
    View v;
    String xuatDan = "De:";
    String Dieukien = "(the_loai = 'deb' or the_loai = 'det')";
    String donvi = "n ";
    double myBalance = 0.0d;
    int GameType = 0;
    int Price = 0;
    int PriceLive = 0;
    String ToDay = "";
    String DangXuat = null;
    String myMax = "";
    String the_loai = "deb";
    boolean LoLive = false;
    public List<String> mSo = new ArrayList();
    public List<String> mTienNhan = new ArrayList();
    public List<String> mTienOm = new ArrayList();
    public List<String> mTienchuyen = new ArrayList();
    public List<String> mTienTon = new ArrayList();
    public List<Integer> mNhay = new ArrayList();
    public List<String> mGia = new ArrayList();
    public List<String> mMax = new ArrayList();
    public List<String> mwebsite = new ArrayList();
    public List<String> mpassword = new ArrayList();
    public List<String> SoTin = new ArrayList();
    public List<Integer> TheLoai = new ArrayList();
    public List<String> NoiDung = new ArrayList();
    public List<String> ThoiGian = new ArrayList();
    public List<String> TienCuoc = new ArrayList();
    public List<Integer> HuyCuoc = new ArrayList();
    int spin_pointion = -1;
    String lay_xien = " length(so_chon) = 5 ";
    JSONObject jsonGia = new JSONObject();
    JSONObject jsonChayTrang = new JSONObject();
    JSONArray jsonArray = new JSONArray();
    JSONObject jsonTienxien = new JSONObject();
    int Dem = 0;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.20
        @Override // java.lang.Runnable
        public void run() {
            boolean Running = true;
            if (Frag_Chaytrang.Curent_date_time > 0) {
                DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                Date gioBatdau = Frag_Chaytrang.parseDate("01:00");
                Date gioLoxien = Frag_Chaytrang.parseDate("18:14");
                Date gioKetthuc = Frag_Chaytrang.parseDate("18:28");
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Frag_Chaytrang.Curent_date_time * 1000);
                int hour = calendar.get(11);
                int minute = calendar.get(12);
                Date date = Frag_Chaytrang.parseDate(hour + ":" + minute);
                if (date.after(gioLoxien) && date.before(gioKetthuc) && !Frag_Chaytrang.this.LoLive) {
                    Frag_Chaytrang.this.radio_xi.setEnabled(false);
                    Frag_Chaytrang.this.radio_lo.setText("Lô Live");
                    Frag_Chaytrang.this.LoLive = true;
                } else if (date.after(gioKetthuc)) {
                    Frag_Chaytrang.this.handler.removeCallbacks(Frag_Chaytrang.this.runnable);
                    Running = false;
                    Frag_Chaytrang.this.btn_Xuatso.setEnabled(false);
                    Frag_Chaytrang.this.btn_Xuatso.setText("Hết giờ");
                    Frag_Chaytrang.this.btn_Xuatso.setTextColor(-7829368);
                } else if (date.before(gioBatdau)) {
                    Frag_Chaytrang.this.btn_Xuatso.setEnabled(false);
                    Frag_Chaytrang.this.btn_Xuatso.setText("Chưa mở");
                    Frag_Chaytrang.this.btn_Xuatso.setTextColor(-7829368);
                    Running = false;
                }
                if (Frag_Chaytrang.this.LoLive && Frag_Chaytrang.this.radio_lo.isChecked()) {
                    Frag_Chaytrang.this.Dem++;
                    if (Frag_Chaytrang.this.Dem >= 4) {
                        Frag_Chaytrang.this.Dem = 0;
                        Frag_Chaytrang.this.Laygia();
                    }
                }
                if (Running) {
                    Frag_Chaytrang.this.btn_Xuatso.setText("Chạy trang (" + formatter.format(calendar.getTime()) + ")");
                }
            } else {
                Frag_Chaytrang.Curent_date_time = new Timestamp(System.currentTimeMillis()).getTime() / 1000;
            }
            if (Running) {
                Frag_Chaytrang.Curent_date_time++;
                Frag_Chaytrang.this.handler.postDelayed(this, 1000L);
            }
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_chaytrang, container, false);
        init();
        this.db = new Database(getActivity());
        new MainActivity();
        this.ToDay = MainActivity.Get_date();
        this.radio_de.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_de.isChecked()) {
                    Frag_Chaytrang.this.li_loaide.setVisibility(0);
                    try {
                        Database database = Frag_Chaytrang.this.db;
                        Cursor cursor = database.GetData("Select sum((the_loai = 'dea')* diem) as de_a\n,sum((the_loai = 'deb')* diem) as de_b\n,sum((the_loai = 'det')* diem) as de_t\n,sum((the_loai = 'dec')* diem) as de_c\n,sum((the_loai = 'ded')* diem) as de_d\nFrom tbl_soctS \nWhere ngay_nhan = '" + Frag_Chaytrang.this.ToDay + "'");
                        if (!cursor.moveToFirst() || cursor == null) {
                            Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_Chaytrang.this.GameType = 0;
                            if (MainActivity.MyToken.length() > 0) {
                                Frag_Chaytrang.this.Laygia();
                                return;
                            }
                            return;
                        }
                        int[] dem = new int[5];
                        if (cursor.getDouble(0) > 0.0d) {
                            dem[0] = 1;
                            Frag_Chaytrang.this.radio_dea.setEnabled(true);
                        } else {
                            dem[0] = 0;
                            Frag_Chaytrang.this.radio_dea.setEnabled(false);
                        }
                        if (cursor.getDouble(1) > 0.0d) {
                            dem[1] = 1;
                            Frag_Chaytrang.this.radio_deb.setEnabled(true);
                        } else {
                            dem[1] = 0;
                            Frag_Chaytrang.this.radio_deb.setEnabled(false);
                        }
                        if (cursor.getDouble(2) > 0.0d) {
                            dem[2] = 1;
                        } else {
                            dem[2] = 0;
                        }
                        if (cursor.getDouble(3) > 0.0d) {
                            dem[3] = 1;
                            Frag_Chaytrang.this.radio_dec.setEnabled(true);
                        } else {
                            dem[3] = 0;
                            Frag_Chaytrang.this.radio_dec.setEnabled(false);
                        }
                        if (cursor.getDouble(4) > 0.0d) {
                            dem[4] = 1;
                            Frag_Chaytrang.this.radio_ded.setEnabled(true);
                        } else {
                            dem[4] = 0;
                            Frag_Chaytrang.this.radio_ded.setEnabled(false);
                        }
                        if (dem[0] == 0 && ((dem[1] == 1 || dem[2] == 1) && dem[3] == 0 && dem[4] == 0)) {
                            Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                            Frag_Chaytrang.this.li_loaide.setVisibility(8);
                            Frag_Chaytrang.this.radio_deb.setChecked(true);
                            Frag_Chaytrang.this.xem_RecycView();
                        } else if (dem[0] == 0 && dem[1] == 0 && dem[2] == 0 && dem[3] == 0 && dem[4] == 0) {
                            Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                            Frag_Chaytrang.this.li_loaide.setVisibility(8);
                            Frag_Chaytrang.this.radio_deb.setChecked(true);
                            Frag_Chaytrang.this.xem_RecycView();
                        } else {
                            Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                            Frag_Chaytrang.this.li_loaide.setVisibility(0);
                            Frag_Chaytrang.this.radio_deb.setChecked(true);
                            Frag_Chaytrang.this.xem_RecycView();
                        }
                        if (!cursor.isClosed() && cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                        Frag_Chaytrang.this.GameType = 0;
                        if (MainActivity.MyToken.length() > 0) {
                            Frag_Chaytrang.this.Laygia();
                        }
                    } catch (SQLException e) {
                        Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                        Frag_Chaytrang.this.GameType = 0;
                        if (MainActivity.MyToken.length() > 0) {
                            Frag_Chaytrang.this.Laygia();
                        }
                    }
                }
            }
        });
        this.radio_dea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_dea.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'dea'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                    Frag_Chaytrang.this.GameType = 21;
                    Frag_Chaytrang.this.Laygia();
                }
            }
        });
        try {
            this.mwebsite.clear();
            this.mpassword.clear();
            Cursor cursor = this.db.GetData("Select * From tbl_chaytrang_acc");
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    this.mwebsite.add(cursor.getString(0));
                    this.mpassword.add(cursor.getString(1));
                }
                if (cursor != null) {
                    cursor.close();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), (int) R.layout.spinner_item, this.mwebsite);
                this.spr_trang.setAdapter((SpinnerAdapter) adapter);
                if (this.mwebsite.size() > 0) {
                    this.spr_trang.setSelection(0);
                    this.spin_pointion = 0;
                }
            }
            cursor.close();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Đang copy dữ liệu bản mới!", 1).show();
        }
        this.radio_deb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_deb.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                    Frag_Chaytrang.this.GameType = 0;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_dec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_dec.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'dec'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                    Frag_Chaytrang.this.GameType = 23;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_ded.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.5
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_ded.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'ded'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                    Frag_Chaytrang.this.GameType = 22;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_lo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_Chaytrang.this.radio_lo.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'lo'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(8);
                    Frag_Chaytrang.this.li_loaide.setVisibility(8);
                    if (!Frag_Chaytrang.this.LoLive) {
                        Frag_Chaytrang.this.GameType = 1;
                    } else {
                        Frag_Chaytrang.this.GameType = 20;
                    }
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_xi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.7
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_Chaytrang.this.radio_xi.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'xi'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(0);
                    Frag_Chaytrang.this.li_loaide.setVisibility(8);
                    Frag_Chaytrang.this.radio_xi2.setChecked(true);
                    Frag_Chaytrang.this.GameType = 2;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_xi2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.8
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_xi2.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'xi'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(0);
                    Frag_Chaytrang.this.li_loaide.setVisibility(8);
                    Frag_Chaytrang.this.lay_xien = " length(so_chon) = 5 ";
                    Frag_Chaytrang.this.GameType = 2;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_xi3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.9
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_xi3.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'xi'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(0);
                    Frag_Chaytrang.this.li_loaide.setVisibility(8);
                    Frag_Chaytrang.this.lay_xien = " length(so_chon) = 8 ";
                    Frag_Chaytrang.this.GameType = 3;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.radio_xi4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.10
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_Chaytrang.this.radio_xi4.isChecked()) {
                    Frag_Chaytrang.this.DangXuat = "the_loai = 'xi'";
                    Frag_Chaytrang.this.li_loaixi.setVisibility(0);
                    Frag_Chaytrang.this.li_loaide.setVisibility(8);
                    Frag_Chaytrang.this.lay_xien = " length(so_chon) = 11 ";
                    Frag_Chaytrang.this.GameType = 4;
                    if (MainActivity.MyToken.length() > 0) {
                        Frag_Chaytrang.this.Laygia();
                    }
                }
            }
        });
        this.spr_trang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.11
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Frag_Chaytrang.this.spin_pointion = position;
                Frag_Chaytrang frag_Chaytrang = Frag_Chaytrang.this;
                frag_Chaytrang.login(frag_Chaytrang.mwebsite.get(position), Frag_Chaytrang.this.mpassword.get(position));
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        if (this.mwebsite.size() > 0) {
            login(this.mwebsite.get(this.spin_pointion), this.mpassword.get(this.spin_pointion));
        }
        this.btn_Xuatso.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.12
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                if (Frag_Chaytrang.this.spin_pointion == -1) {
                    Toast.makeText(Frag_Chaytrang.this.getActivity(), "Không có trang để xuất", 0).show();
                }
                if (MainActivity.MyToken.length() > 0) {
                    int i = Frag_Chaytrang.this.GameType;
                    if (i != 0) {
                        if (i != 1) {
                            if (i == 2) {
                                Frag_Chaytrang.this.the_loai = "xi2";
                                Frag_Chaytrang.this.xuatDan = "Xi:";
                                Frag_Chaytrang.this.donvi = "n ";
                                Frag_Chaytrang.this.Dieukien = "the_loai = 'xi' AND length(so_chon) = 5";
                                Frag_Chaytrang.this.TaoTinXi();
                            } else if (i == 3) {
                                Frag_Chaytrang.this.the_loai = "xi3";
                                Frag_Chaytrang.this.xuatDan = "Xi:";
                                Frag_Chaytrang.this.donvi = "n ";
                                Frag_Chaytrang.this.Dieukien = "the_loai = 'xi' AND length(so_chon) = 8";
                                Frag_Chaytrang.this.TaoTinXi();
                            } else if (i != 4) {
                                switch (i) {
                                    case 21:
                                        Frag_Chaytrang.this.the_loai = "dea";
                                        Frag_Chaytrang.this.xuatDan = "De dau:";
                                        Frag_Chaytrang.this.donvi = "n ";
                                        Frag_Chaytrang.this.Dieukien = "the_loai = 'dea'";
                                        Frag_Chaytrang.this.TaoTinDe();
                                        break;
                                    case 22:
                                        Frag_Chaytrang.this.the_loai = "ded";
                                        Frag_Chaytrang.this.xuatDan = "De giai 1:";
                                        Frag_Chaytrang.this.donvi = "n ";
                                        Frag_Chaytrang.this.Dieukien = "the_loai = 'ded'";
                                        Frag_Chaytrang.this.TaoTinDe();
                                        break;
                                    case 23:
                                        Frag_Chaytrang.this.the_loai = "dec";
                                        Frag_Chaytrang.this.xuatDan = "De dau giai 1:";
                                        Frag_Chaytrang.this.donvi = "n ";
                                        Frag_Chaytrang.this.Dieukien = "the_loai = 'dec'";
                                        Frag_Chaytrang.this.TaoTinDe();
                                        break;
                                }
                            } else {
                                Frag_Chaytrang.this.the_loai = "xi4";
                                Frag_Chaytrang.this.xuatDan = "Xi:";
                                Frag_Chaytrang.this.donvi = "n ";
                                Frag_Chaytrang.this.Dieukien = "the_loai = 'xi' AND length(so_chon) = 11";
                                Frag_Chaytrang.this.TaoTinXi();
                            }
                        }
                        Frag_Chaytrang.this.the_loai = "lo";
                        Frag_Chaytrang.this.xuatDan = "Lo:";
                        Frag_Chaytrang.this.donvi = "d ";
                        Frag_Chaytrang.this.Dieukien = "the_loai = 'lo'";
                        Frag_Chaytrang.this.TaoTinDe();
                    } else {
                        Frag_Chaytrang.this.the_loai = "deb";
                        Frag_Chaytrang.this.xuatDan = "De:";
                        Frag_Chaytrang.this.donvi = "n ";
                        Frag_Chaytrang.this.Dieukien = "(the_loai = 'deb' or the_loai = 'det')";
                        Frag_Chaytrang.this.TaoTinDe();
                    }
                    Frag_Chaytrang.this.Dialog();
                }
            }
        });
        this.btn_MaXuat.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.13
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Frag_Chaytrang.this.Dialog2();
            }
        });
        Handler handler = new Handler();
        this.handler = handler;
        handler.postDelayed(this.runnable, 1000L);
        xemlv();
        return this.v;
    }

    /* JADX WARN: Removed duplicated region for block: B:30:0x00b9 A[Catch: JSONException -> 0x00e9, TRY_ENTER, TryCatch #0 {JSONException -> 0x00e9, blocks: (B:14:0x0066, B:16:0x006e, B:19:0x007c, B:21:0x0084, B:24:0x0092, B:27:0x0099, B:30:0x00b9, B:32:0x00c4, B:35:0x00d0, B:36:0x00d6, B:38:0x00df), top: B:65:0x0066 }] */
    /* JADX WARN: Removed duplicated region for block: B:31:0x00c3  */
    /* JADX WARN: Removed duplicated region for block: B:34:0x00ce  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00d0 A[Catch: JSONException -> 0x00e9, TryCatch #0 {JSONException -> 0x00e9, blocks: (B:14:0x0066, B:16:0x006e, B:19:0x007c, B:21:0x0084, B:24:0x0092, B:27:0x0099, B:30:0x00b9, B:32:0x00c4, B:35:0x00d0, B:36:0x00d6, B:38:0x00df), top: B:65:0x0066 }] */
    /* JADX WARN: Removed duplicated region for block: B:38:0x00df A[Catch: JSONException -> 0x00e9, TRY_LEAVE, TryCatch #0 {JSONException -> 0x00e9, blocks: (B:14:0x0066, B:16:0x006e, B:19:0x007c, B:21:0x0084, B:24:0x0092, B:27:0x0099, B:30:0x00b9, B:32:0x00c4, B:35:0x00d0, B:36:0x00d6, B:38:0x00df), top: B:65:0x0066 }] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x00e4 A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TaoTinDe() {
        /*
            Method dump skipped, instructions count: 422
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_Chaytrang.TaoTinDe():java.lang.String");
    }

    public String TaoTinXi() {
        int dem = 0;
        JSONObject jSon = new JSONObject();
        List<JSONObject> jsonValues = new ArrayList<>();
        int maxDang = Integer.parseInt(this.myMax.replace(".", ""));
        try {
            if (this.edt_tien.getText().toString().trim().length() > 0 && Congthuc.isNumeric(this.edt_tien.getText().toString().trim())) {
                if (Integer.parseInt(this.edt_tien.getText().toString()) > maxDang) {
                    return "Số tiền vượt quá max ";
                }
                maxDang = Integer.parseInt(this.edt_tien.getText().toString());
            }
            for (int i = 0; i < this.mSo.size() && dem < 50; i++) {
                String Ktra = this.mSo.get(i);
                int TienTon = Integer.parseInt(this.mTienTon.get(i).replace(".", ""));
                if (TienTon > 0 && Integer.parseInt(this.mGia.get(i)) <= this.MaxChay) {
                    JSONObject soCT = new JSONObject();
                    soCT.put("So_chon", Ktra);
                    soCT.put("Da_chuyen", jSon.has(Ktra) ? jSon.getJSONObject(Ktra).getInt("Da_chuyen") + TienTon : 0);
                    soCT.put("Se_chuyen", soCT.getInt("Da_chuyen") + TienTon <= maxDang ? TienTon : maxDang - soCT.getInt("Da_chuyen"));
                    if (soCT.getInt("Se_chuyen") > 0) {
                        jsonValues.add(soCT);
                        dem++;
                    }
                }
            }
            Collections.sort(jsonValues, new Comparator<JSONObject>() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.15
                public int compare(JSONObject a, JSONObject b) {
                    int valA = 0;
                    Integer valB = 0;
                    try {
                        valA = Integer.valueOf(a.getInt("Se_chuyen"));
                        valB = Integer.valueOf(b.getInt("Se_chuyen"));
                    } catch (JSONException e) {
                    }
                    return valB.compareTo(valA);
                }
            });
            for (int i2 = 0; i2 < jsonValues.size(); i2++) {
                this.xuatDan += jsonValues.get(i2).getString("So_chon") + "x" + jsonValues.get(i2).getString("Se_chuyen") + this.donvi;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
        return this.xuatDan.length() > 5 ? this.xuatDan : "";
    }

    public void Dialog() {
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.frag_chaytrang_diaglog);
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        EditText edt_XuatDan = (EditText) dialog.findViewById(R.id.edt_XuatDan);
        final TextView taikhoan = (TextView) dialog.findViewById(R.id.taikhoan);
        final TextView CreditLimit = (TextView) dialog.findViewById(R.id.CreditLimit);
        final TextView Balance = (TextView) dialog.findViewById(R.id.Balance);
        TextView edt_XuatErr = (TextView) dialog.findViewById(R.id.edt_XuatErr);
        edt_XuatErr.setVisibility(8);
        Button btn_chuyendi = (Button) dialog.findViewById(R.id.btn_chuyendi);
        final OkHttpClient okHttpClient = new OkHttpClient();
        if (MainActivity.MyToken.length() > 0 && Build.VERSION.SDK_INT >= 24) {
            CompletableFuture.runAsync(new Runnable() { // from class: tamhoang.ldpro4.Fragment.-$$Lambda$Frag_Chaytrang$DGn7QqO0Xk9idVJ8MyHUxv0XV3o
                @Override // java.lang.Runnable
                public final void run() {
                    Frag_Chaytrang.this.lambda$Dialog$0$Frag_Chaytrang(okHttpClient, taikhoan, CreditLimit, Balance);
                }
            });
        }
        edt_XuatDan.setText("");
        edt_XuatDan.setText(this.xuatDan.replaceAll(",x", "x"));
        btn_chuyendi.setOnClickListener(new AnonymousClass18(btn_chuyendi, edt_XuatDan, dialog, edt_XuatErr));
        dialog.setCancelable(true);
        dialog.setTitle("Xem dạng:");
        dialog.show();
    }

    public /* synthetic */ void lambda$Dialog$0$Frag_Chaytrang(OkHttpClient okHttpClient, final TextView taikhoan, final TextView CreditLimit, final TextView Balance) {
        try {
            if (MainActivity.MyToken.length() > 0) {
                Request.Builder builder = new Request.Builder();
                ResponseBody body = okHttpClient.newCall(builder.header("Authorization", "Bearer " + MainActivity.MyToken).url("https://id.lotusapi.com/wallets/player/my-wallet").get().build()).execute().body();
                if (body != null) {
                    final JSONObject json = new JSONObject(body.string());
                    if (!json.has("message")) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.17
                            @Override // java.lang.Runnable
                            public void run() {
                                try {
                                    DecimalFormat decimalFormat = new DecimalFormat("###,###");
                                    taikhoan.setText(Frag_Chaytrang.this.mwebsite.get(Frag_Chaytrang.this.spin_pointion));
                                    CreditLimit.setText(decimalFormat.format(json.getDouble("CreditLimit")));
                                    Balance.setText(decimalFormat.format(json.getDouble("Balance")));
                                    Frag_Chaytrang.this.myBalance = json.getDouble("Balance");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else if (json.getString("message").indexOf("Unauthorized") > -1) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.16
                            @Override // java.lang.Runnable
                            public void run() {
                                Toast.makeText(Frag_Chaytrang.this.getActivity(), "Tài khoản đăng nhập lỗi!", 0).show();
                            }
                        });
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* renamed from: tamhoang.ldpro4.Fragment.Frag_Chaytrang$18  reason: invalid class name */
    /* loaded from: classes2.dex */
    public class AnonymousClass18 implements View.OnClickListener {
        final /* synthetic */ Button val$btn_chuyendi;
        final /* synthetic */ Dialog val$dialog;
        final /* synthetic */ EditText val$edt_XuatDan;
        final /* synthetic */ TextView val$edt_XuatErr;

        AnonymousClass18(Button button, EditText editText, Dialog dialog, TextView textView) {
            this.val$btn_chuyendi = button;
            this.val$edt_XuatDan = editText;
            this.val$dialog = dialog;
            this.val$edt_XuatErr = textView;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            String Kiermtra;
            this.val$btn_chuyendi.setEnabled(false);
            final SQLiteDatabase database = Frag_Chaytrang.this.db.getWritableDatabase();
            final DatabaseUtils.InsertHelper ih = new DatabaseUtils.InsertHelper(database, "tbl_soctS");
            String Kiermtra2 = null;
            try {
                try {
                    Kiermtra2 = Frag_Chaytrang.this.KiemTraTruocKhiChayTrang(this.val$edt_XuatDan.getText().toString().replaceAll("'", " ").trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Kiermtra2 == "") {
                    String Kiermtra3 = Frag_Chaytrang.this.Laygia();
                    Kiermtra = Kiermtra3;
                } else {
                    Kiermtra = Kiermtra2;
                }
                if (Kiermtra == "") {
                    Frag_Chaytrang.this.jsonArray = new JSONArray();
                    final String Postjson = Frag_Chaytrang.this.CreateJson();
                    final OkHttpClient okHttpClient = new OkHttpClient();
                    final AtomicReference<String> str3 = new AtomicReference<>("");
                    if (Build.VERSION.SDK_INT >= 24) {
                        final EditText editText = this.val$edt_XuatDan;
                        final Dialog dialog = this.val$dialog;
                        final TextView textView = this.val$edt_XuatErr;
                        final Button button = this.val$btn_chuyendi;
                        CompletableFuture.runAsync(new Runnable() { // from class: tamhoang.ldpro4.Fragment.-$$Lambda$Frag_Chaytrang$18$Aa1GizXhuIa4XrlZ0t5mv9Cb18Q
                            @Override // java.lang.Runnable
                            public final void run() {
                                AnonymousClass18.this.lambda$onClick$0$Frag_Chaytrang$18(str3, okHttpClient, Postjson, database, ih, editText, dialog, textView, button);
                            }
                        });
                    }
                    return;
                }
                this.val$edt_XuatErr.setText(Kiermtra);
                this.val$edt_XuatErr.setVisibility(0);
                this.val$btn_chuyendi.setEnabled(true);
            } catch (Exception e2) {
                this.val$edt_XuatErr.setText("Có lỗi khi xuất tin!");
                this.val$edt_XuatErr.setVisibility(0);
                this.val$btn_chuyendi.setEnabled(true);
            }
        }

        public /* synthetic */ void lambda$onClick$0$Frag_Chaytrang$18(AtomicReference str3, OkHttpClient okHttpClient, String Postjson, final SQLiteDatabase database, final DatabaseUtils.InsertHelper ih, final EditText edt_XuatDan, final Dialog dialog, final TextView edt_XuatErr, final Button btn_chuyendi) {
            Exception e;
            try {
                Request.Builder header = new Request.Builder().url("https://lotto.lotusapi.com/game-play/player/play").header(HttpConnection.CONTENT_TYPE, "application/json");
                try {
                    try {
                    } catch (Exception e2) {
                        e = e2;
                    }
                } catch (Exception e3) {
                    e = e3;
                }
                try {
                    str3.set(okHttpClient.newCall(header.header("Authorization", "Bearer " + MainActivity.MyToken).post(RequestBody.Companion.create(Postjson, MediaType.Companion.parse("application/json"))).build()).execute().body().string());
                    String Str = str3.toString();
                    if (Str.startsWith("[")) {
                        JSONArray json = new JSONArray(Str);
                        JSONObject jsonObject = json.getJSONObject(0);
                        if (jsonObject.has("Tx")) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.18.1
                                @Override // java.lang.Runnable
                                public void run() {
                                    String str = "ngay_nhan";
                                    String str2 = " ";
                                    String str4 = "'";
                                    try {
                                        database.beginTransaction();
                                        JSONObject Json = null;
                                        int i = 0;
                                        while (i < Frag_Chaytrang.this.jsonArray.length()) {
                                            Json = Frag_Chaytrang.this.jsonArray.getJSONObject(i);
                                            ih.prepareForInsert();
                                            ih.bind(ih.getColumnIndex("ID"), (byte[]) null);
                                            ih.bind(ih.getColumnIndex(str), Json.getString(str));
                                            ih.bind(ih.getColumnIndex("type_kh"), 2);
                                            ih.bind(ih.getColumnIndex("ten_kh"), Frag_Chaytrang.this.mwebsite.get(Frag_Chaytrang.this.spin_pointion));
                                            ih.bind(ih.getColumnIndex("so_dienthoai"), Frag_Chaytrang.this.mwebsite.get(Frag_Chaytrang.this.spin_pointion));
                                            ih.bind(ih.getColumnIndex("so_tin_nhan"), Json.getInt("so_tin_nhan"));
                                            ih.bind(ih.getColumnIndex("the_loai"), Frag_Chaytrang.this.the_loai.indexOf("xi") > -1 ? "xi" : Frag_Chaytrang.this.the_loai);
                                            ih.bind(ih.getColumnIndex("so_chon"), Json.getString("so_chon"));
                                            ih.bind(ih.getColumnIndex("diem"), Json.getInt("diem"));
                                            ih.bind(ih.getColumnIndex("diem_quydoi"), Json.getInt("diem"));
                                            ih.bind(ih.getColumnIndex("diem_khachgiu"), 0);
                                            ih.bind(ih.getColumnIndex("diem_dly_giu"), 0);
                                            ih.bind(ih.getColumnIndex("diem_ton"), Json.getInt("diem"));
                                            ih.bind(ih.getColumnIndex("gia"), Json.getInt("gia"));
                                            ih.bind(ih.getColumnIndex("lan_an"), Json.getInt("lan_an"));
                                            ih.bind(ih.getColumnIndex("so_nhay"), 0);
                                            ih.bind(ih.getColumnIndex("tong_tien"), Json.getInt("tong_tien"));
                                            ih.bind(ih.getColumnIndex("ket_qua"), 0);
                                            ih.execute();
                                            i++;
                                            str = str;
                                            str2 = str2;
                                            str4 = str4;
                                        }
                                        database.setTransactionSuccessful();
                                        database.endTransaction();
                                        ih.close();
                                        database.close();
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.setTime(new Date());
                                        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
                                        dmyFormat.setTimeZone(TimeZone.getDefault());
                                        hourFormat.setTimeZone(TimeZone.getDefault());
                                        String mNgayNhan = dmyFormat.format(calendar.getTime());
                                        String mGionhan = hourFormat.format(calendar.getTime());
                                        String S = "Insert Into tbl_tinnhanS values (null, '" + mNgayNhan + "', '" + mGionhan + "', 2, '" + Frag_Chaytrang.this.mwebsite.get(Frag_Chaytrang.this.spin_pointion) + "', '" + Frag_Chaytrang.this.mwebsite.get(Frag_Chaytrang.this.spin_pointion) + "', 'ChayTrang', " + Json.getInt("so_tin_nhan") + ", '" + edt_XuatDan.getText().toString().replace(str4, str2).trim() + "', '" + edt_XuatDan.getText().toString().replace(str4, str2).trim() + "', '" + edt_XuatDan.getText().toString().replace(str4, str2).trim().toLowerCase() + "', 'ok',0, 0, 0, null)";
                                        Frag_Chaytrang.this.db.QueryData(S);
                                    } catch (JSONException e4) {
                                        e4.printStackTrace();
                                    }
                                    Frag_Chaytrang.this.xem_RecycView();
                                    dialog.dismiss();
                                    Toast.makeText(Frag_Chaytrang.this.getActivity(), "Đã chạy thành công!", 0).show();
                                }
                            });
                        } else {
                            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.18.2
                                @Override // java.lang.Runnable
                                public void run() {
                                    edt_XuatErr.setText("Kết nối kém, hãy xuất lại.");
                                    edt_XuatErr.setVisibility(0);
                                    btn_chuyendi.setEnabled(true);
                                }
                            });
                        }
                        return;
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.18.3
                        @Override // java.lang.Runnable
                        public void run() {
                            edt_XuatErr.setText("Kết nối kém, hãy xuất lại.");
                            edt_XuatErr.setVisibility(0);
                            btn_chuyendi.setEnabled(true);
                        }
                    });
                } catch (Exception e4) {
                    e = e4;
                    new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.18.4
                        @Override // java.lang.Runnable
                        public void run() {
                            edt_XuatErr.setText("Kết nối kém, hãy xuất lại.");
                            edt_XuatErr.setVisibility(0);
                            btn_chuyendi.setEnabled(true);
                        }
                    });
                    e.printStackTrace();
                }
            } catch (Exception e5) {
                e = e5;
            }
        }
    }

    public void Dialog2() {
        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.frag_chaytrang_tinchay);
        final DecimalFormat decimalFormat = new DecimalFormat("###,###");
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        final ListView lv_cacmachay = (ListView) dialog.findViewById(R.id.lv_cacmachay);
        this.SoTin.clear();
        this.TheLoai.clear();
        this.NoiDung.clear();
        this.ThoiGian.clear();
        this.TienCuoc.clear();
        try {
            final OkHttpClient okHttpClient = new OkHttpClient();
            if (Build.VERSION.SDK_INT >= 24) {
                CompletableFuture.runAsync(new Runnable() { // from class: tamhoang.ldpro4.Fragment.-$$Lambda$Frag_Chaytrang$XywKntZqBQRIghYKYLeRhEnIabQ
                    @Override // java.lang.Runnable
                    public final void run() {
                        Frag_Chaytrang.this.lambda$Dialog2$1$Frag_Chaytrang(okHttpClient, decimalFormat, lv_cacmachay);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dialog.getWindow().setLayout(-1, -2);
        dialog.setCancelable(true);
        dialog.setTitle("Xem dạng:");
        dialog.show();
    }

    public /* synthetic */ void lambda$Dialog2$1$Frag_Chaytrang(OkHttpClient okHttpClient, DecimalFormat decimalFormat, final ListView lv_cacmachay) {
        try {
            ResponseBody body = okHttpClient.newCall(new Request.Builder().header("Authorization", "Bearer " + MainActivity.MyToken).url("https://lotto.lotusapi.com/game-play/player/tickets/current?limit=100").get().build()).execute().body();
            if (body != null) {
                JSONArray jsonArray = new JSONArray(body.string());
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    this.SoTin.add(jsonObject.getString("TicketNumber"));
                    this.TheLoai.add(Integer.valueOf(jsonObject.getInt("BetType")));
                    this.NoiDung.add(jsonObject.getString("Numbers"));
                    String CreatedAt = jsonObject.getString("CreatedAt");
                    String[] SSS = CreatedAt.substring(11).substring(0, 8).split(":");
                    SSS[0] = (Integer.parseInt(SSS[0]) + 7) + "";
                    String CreatedAt2 = SSS[0] + ":" + SSS[1] + ":" + SSS[2];
                    this.ThoiGian.add(CreatedAt2);
                    this.TienCuoc.add(decimalFormat.format(jsonObject.getLong("Amount")));
                    if (jsonObject.has("CancelledAt")) {
                        this.HuyCuoc.add(0);
                    } else {
                        this.HuyCuoc.add(1);
                    }
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.19
                    @Override // java.lang.Runnable
                    public void run() {
                        ListView listView = lv_cacmachay;
                        Frag_Chaytrang frag_Chaytrang = Frag_Chaytrang.this;
                        listView.setAdapter((ListAdapter) new Ma_da_chay(frag_Chaytrang.getActivity(), R.layout.frag_chaytrang_tinchay_lv, Frag_Chaytrang.this.NoiDung));
                    }
                });
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    /* loaded from: classes2.dex */
    class Ma_da_chay extends ArrayAdapter {
        public Ma_da_chay(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView tv_HuyCuoc;
            TextView tv_NoiDung;
            TextView tv_SoTin;
            TextView tv_ThoiGian;
            TextView tv_TienCuoc;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View view, ViewGroup parent) {
            ViewHolder holder;
            View view2 = null;
            if (0 == 0) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                view2 = inflater.inflate(R.layout.frag_chaytrang_tinchay_lv, (ViewGroup) null);
                holder = new ViewHolder();
                holder.tv_SoTin = (TextView) view2.findViewById(R.id.tv_SoTin);
                holder.tv_NoiDung = (TextView) view2.findViewById(R.id.tv_NoiDung);
                holder.tv_ThoiGian = (TextView) view2.findViewById(R.id.tv_ThoiGan);
                holder.tv_TienCuoc = (TextView) view2.findViewById(R.id.tv_TienCuoc);
                holder.tv_HuyCuoc = (TextView) view2.findViewById(R.id.tv_HuyCuoc);
            } else {
                holder = (ViewHolder) view2.getTag();
            }
            holder.tv_HuyCuoc.setFocusable(false);
            holder.tv_HuyCuoc.setFocusableInTouchMode(false);
            holder.tv_HuyCuoc.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.Ma_da_chay.1
                @Override // android.view.View.OnClickListener
                public void onClick(View view3) {
                }
            });
            if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 0) {
                TextView textView = holder.tv_NoiDung;
                textView.setText("Đề: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 1) {
                TextView textView2 = holder.tv_NoiDung;
                textView2.setText("Lô: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 2) {
                TextView textView3 = holder.tv_NoiDung;
                textView3.setText("Xiên 2: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 3) {
                TextView textView4 = holder.tv_NoiDung;
                textView4.setText("Xiên 3: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 4) {
                TextView textView5 = holder.tv_NoiDung;
                textView5.setText("Xiên 4: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 20) {
                TextView textView6 = holder.tv_NoiDung;
                textView6.setText("Lô Live: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 21) {
                TextView textView7 = holder.tv_NoiDung;
                textView7.setText("Đề đầu: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 22) {
                TextView textView8 = holder.tv_NoiDung;
                textView8.setText("Giải nhất: " + Frag_Chaytrang.this.NoiDung.get(position));
            } else if (Frag_Chaytrang.this.TheLoai.get(position).intValue() == 23) {
                TextView textView9 = holder.tv_NoiDung;
                textView9.setText("Đầu giải nhất: " + Frag_Chaytrang.this.NoiDung.get(position));
            }
            TextView textView10 = holder.tv_ThoiGian;
            textView10.setText("Time: " + Frag_Chaytrang.this.ThoiGian.get(position));
            TextView textView11 = holder.tv_TienCuoc;
            textView11.setText("Tổng: " + Frag_Chaytrang.this.TienCuoc.get(position));
            holder.tv_SoTin.setText(Frag_Chaytrang.this.SoTin.get(position));
            if (Frag_Chaytrang.this.HuyCuoc.get(position).intValue() == 0) {
                holder.tv_HuyCuoc.setTextColor(-7829368);
                holder.tv_HuyCuoc.setEnabled(false);
                holder.tv_HuyCuoc.setText("Đã huỷ");
            } else {
                holder.tv_HuyCuoc.setVisibility(8);
            }
            return view2;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static Date parseDate(String date) {
        SimpleDateFormat inputParser = new SimpleDateFormat("HH:mm", Locale.US);
        try {
            return inputParser.parse(date);
        } catch (ParseException e) {
            return new Date(0L);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.runnable);
    }

    /* JADX WARN: Removed duplicated region for block: B:178:0x0539 A[Catch: all -> 0x0548, TryCatch #30 {all -> 0x0548, blocks: (B:176:0x0530, B:178:0x0539), top: B:199:0x0530 }] */
    /* JADX WARN: Removed duplicated region for block: B:182:0x0544  */
    /* JADX WARN: Removed duplicated region for block: B:187:0x054f  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String KiemTraTruocKhiChayTrang(String r35) throws JSONException {
        /*
            Method dump skipped, instructions count: 1365
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_Chaytrang.KiemTraTruocKhiChayTrang(java.lang.String):java.lang.String");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Can't wrap try/catch for region: R(18:2|(4:146|3|4|(4:152|5|(3:144|7|(2:9|(2:11|12)))|16))|(13:18|52|(11:150|55|56|162|57|58|148|59|(25:61|62|138|63|64|(4:156|66|(3:68|69|169)(2:70|168)|71)|167|74|158|75|(2:77|78)(1:79)|80|(1:82)(1:83)|84|85|154|86|87|(3:89|(1:91)(1:92)|93)(2:94|(1:96)(1:97))|98|(1:100)(1:101)|102|103|104|166)(2:110|165)|111|53)|164|122|140|123|124|160|125|126|136|137)|19|(1:30)(4:31|32|(2:37|(1:39)(2:40|(1:42)(2:43|(1:45)(3:46|47|(1:49)(1:50)))))|51)|52|(1:53)|164|122|140|123|124|160|125|126|136|137|(1:(0))) */
    /* JADX WARN: Can't wrap try/catch for region: R(24:2|146|3|4|152|5|(3:144|7|(2:9|(2:11|12)))|16|(13:18|52|(11:150|55|56|162|57|58|148|59|(25:61|62|138|63|64|(4:156|66|(3:68|69|169)(2:70|168)|71)|167|74|158|75|(2:77|78)(1:79)|80|(1:82)(1:83)|84|85|154|86|87|(3:89|(1:91)(1:92)|93)(2:94|(1:96)(1:97))|98|(1:100)(1:101)|102|103|104|166)(2:110|165)|111|53)|164|122|140|123|124|160|125|126|136|137)|19|(1:30)(4:31|32|(2:37|(1:39)(2:40|(1:42)(2:43|(1:45)(3:46|47|(1:49)(1:50)))))|51)|52|(1:53)|164|122|140|123|124|160|125|126|136|137|(1:(0))) */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x0340, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:128:0x0341, code lost:
        r3 = r3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:129:0x0343, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:130:0x0344, code lost:
        r3 = r3;
     */
    /* JADX WARN: Code restructure failed: missing block: B:13:0x00b0, code lost:
        if (r26.GameType != 23) goto L19;
     */
    /* JADX WARN: Removed duplicated region for block: B:150:0x013a A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String CreateJson() {
        /*
            Method dump skipped, instructions count: 868
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_Chaytrang.CreateJson():java.lang.String");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void login(final String Username, final String PassWord) {
        final OkHttpClient okHttpClient = new OkHttpClient();
        final JSONObject Json = new JSONObject();
        final AtomicReference<String> str3 = new AtomicReference<>("");
        if (Build.VERSION.SDK_INT >= 24) {
            CompletableFuture.runAsync(new Runnable() { // from class: tamhoang.ldpro4.Fragment.-$$Lambda$Frag_Chaytrang$l-QEsOD1ZvBMJIMuUzyYsFMIunc
                @Override // java.lang.Runnable
                public final void run() {
                    Frag_Chaytrang.this.lambda$login$2$Frag_Chaytrang(Json, Username, PassWord, str3, okHttpClient);
                }
            });
        }
    }

    public /* synthetic */ void lambda$login$2$Frag_Chaytrang(JSONObject Json, String Username, String PassWord, AtomicReference str3, OkHttpClient okHttpClient) {
        try {
            Json.put("Username", Username);
            Json.put("Password", PassWord);
            str3.set(okHttpClient.newCall(new Request.Builder().url("https://id.lotusapi.com/auth/sign-in").header(HttpConnection.CONTENT_TYPE, "application/json").post(RequestBody.Companion.create(Json.toString(), MediaType.Companion.parse("application/json"))).build()).execute().body().string());
            JSONObject jsonObject = new JSONObject(str3.toString());
            if (jsonObject.has("IdToken")) {
                MainActivity.MyToken = jsonObject.getString("IdToken");
                Laygia();
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.21
                    @Override // java.lang.Runnable
                    public void run() {
                        Toast.makeText(Frag_Chaytrang.this.getActivity(), "Đăng nhập thất bại.", 0).show();
                        MainActivity.MyToken = "";
                    }
                });
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public String Laygia() {
        this.jsonGia = new JSONObject();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        dmyFormat.setTimeZone(TimeZone.getDefault());
        final String mNgayNhan = dmyFormat.format(calendar.getTime());
        final String[] loi = {""};
        final OkHttpClient okHttpClient = new OkHttpClient();
        if (MainActivity.MyToken.length() > 0 && Build.VERSION.SDK_INT >= 24) {
            CompletableFuture.runAsync(new Runnable() { // from class: tamhoang.ldpro4.Fragment.-$$Lambda$Frag_Chaytrang$hzE2FSVfsxb0QfhTAHA5cgHOUQM
                @Override // java.lang.Runnable
                public final void run() {
                    Frag_Chaytrang.this.lambda$Laygia$3$Frag_Chaytrang(mNgayNhan, okHttpClient, loi);
                }
            });
        }
        return loi[0];
    }

    public /* synthetic */ void lambda$Laygia$3$Frag_Chaytrang(String mNgayNhan, OkHttpClient okHttpClient, String[] loi) {
        String Url;
        try {
            if (MainActivity.MyToken.length() > 0) {
                if (!this.radio_lo.isChecked() || !this.LoLive) {
                    Url = "https://lotto.lotusapi.com/odds/player?term=" + mNgayNhan + "&gameTypes=0&betTypes=" + this.GameType;
                } else {
                    this.GameType = 20;
                    Url = "https://lotto.lotusapi.com/odds/player/live?term=" + mNgayNhan + "&gameType=0&betType=20";
                }
                ResponseBody body = okHttpClient.newCall(new Request.Builder().header("Authorization", "Bearer " + MainActivity.MyToken).url(Url).get().build()).execute().body();
                if (body != null) {
                    if (!this.LoLive || !this.radio_lo.isChecked()) {
                        JSONArray JArray = new JSONArray(body.string());
                        JSONObject jsonObject = JArray.getJSONObject(0);
                        this.Price = jsonObject.getInt("Price");
                        this.PriceLive = 0;
                        JSONArray numbers = jsonObject.getJSONArray("Numbers");
                        this.jsonGia = new JSONObject();
                        int i = 0;
                        while (i < numbers.length()) {
                            JSONObject number = numbers.getJSONObject(i);
                            this.jsonGia.put(number.getString("Number"), number.getString("ExtraPrice"));
                            i++;
                            JArray = JArray;
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.24
                            @Override // java.lang.Runnable
                            public void run() {
                                Frag_Chaytrang.this.xem_RecycView();
                            }
                        });
                    } else {
                        JSONObject jsonObject2 = new JSONObject(body.string());
                        if (!jsonObject2.getBoolean("Active")) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.22
                                @Override // java.lang.Runnable
                                public void run() {
                                    Frag_Chaytrang.this.btn_Xuatso.setText("Trang đã đóng");
                                    Frag_Chaytrang.this.btn_Xuatso.setEnabled(false);
                                    Frag_Chaytrang.this.handler.removeCallbacks(Frag_Chaytrang.this.runnable);
                                }
                            });
                        }
                        this.Price = jsonObject2.getInt("Price");
                        JSONArray numbers2 = jsonObject2.getJSONArray("Numbers");
                        this.jsonGia = new JSONObject();
                        for (int i2 = 0; i2 < numbers2.length(); i2++) {
                            JSONObject number2 = numbers2.getJSONObject(i2);
                            this.jsonGia.put(number2.getString("Number"), number2.getString("ExtraPrice"));
                        }
                        if (this.Price != this.PriceLive) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Chaytrang.23
                                @Override // java.lang.Runnable
                                public void run() {
                                    Frag_Chaytrang.this.xem_RecycView();
                                }
                            });
                            this.PriceLive = this.Price;
                        }
                    }
                }
                ResponseBody body2 = okHttpClient.newCall(new Request.Builder().header("Authorization", "Bearer " + MainActivity.MyToken).url("https://comm.lotusapi.com/servers/current-date-time").get().build()).execute().body();
                if (body2 != null) {
                    JSONObject json = new JSONObject(body2.string());
                    if (json.has("Timestamp")) {
                        Curent_date_time = json.getLong("Timestamp");
                    }
                }
            }
        } catch (IOException | JSONException e) {
            loi[0] = "Kết nối kém, kiểm tra lại Internet";
            e.printStackTrace();
        }
    }

    public void xemlv() {
        if (this.DangXuat != null) {
            xem_RecycView();
        } else {
            this.radio_de.setChecked(true);
        }
    }

    public void xem_RecycView() {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        new MainActivity();
        String mDate = MainActivity.Get_date();
        String str7 = null;
        this.jsonTienxien = new JSONObject();
        this.mSo.clear();
        this.mTienNhan.clear();
        this.mTienOm.clear();
        this.mTienchuyen.clear();
        this.mTienTon.clear();
        this.mMax.clear();
        this.mGia.clear();
        this.mNhay.clear();
        String str8 = this.DangXuat;
        if (str8 == "(the_loai = 'deb' or the_loai = 'det')") {
            StringBuilder sb = new StringBuilder();
            sb.append("Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_deB + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2)");
            if (this.spin_pointion > -1) {
                str6 = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str6 = "";
            }
            sb.append(str6);
            sb.append("* tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_deB as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So\n Where tbl_soctS.ngay_nhan='");
            sb.append(mDate);
            sb.append("' AND (tbl_soctS.the_loai='deb' OR tbl_soctS.the_loai='det') GROUP by so_om.So Order by ton DESC");
            str7 = sb.toString();
        } else if (str8 == "the_loai = 'lo'") {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_Lo + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2)");
            if (this.spin_pointion > -1) {
                str5 = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str5 = "";
            }
            sb2.append(str5);
            sb2.append(" * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_Lo as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='");
            sb2.append(mDate);
            sb2.append("' AND tbl_soctS.the_loai='lo' \n GROUP by so_om.So Order by ton DESC");
            str7 = sb2.toString();
        } else if (str8 == "the_loai = 'dea'") {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeA + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2)");
            if (this.spin_pointion > -1) {
                str4 = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str4 = "";
            }
            sb3.append(str4);
            sb3.append(" * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeA as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='");
            sb3.append(mDate);
            sb3.append("' AND tbl_soctS.the_loai='dea' GROUP by so_chon Order by ton DESC");
            str7 = sb3.toString();
        } else if (str8 == "the_loai = 'dec'") {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeC + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2)");
            if (this.spin_pointion > -1) {
                str3 = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str3 = "";
            }
            sb4.append(str3);
            sb4.append(" * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeC as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='");
            sb4.append(mDate);
            sb4.append("' AND tbl_soctS.the_loai='dec' GROUP by so_chon Order by ton DESC");
            str7 = sb4.toString();
        } else if (str8 == "the_loai = 'ded'") {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeD + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2)");
            if (this.spin_pointion > -1) {
                str2 = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str2 = "";
            }
            sb5.append(str2);
            sb5.append(" * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeD as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='");
            sb5.append(mDate);
            sb5.append("' AND tbl_soctS.the_loai='ded' GROUP by so_chon Order by ton DESC");
            str7 = sb5.toString();
        } else if (str8 == "the_loai = 'xi'") {
            Cursor c1 = this.db.GetData("Select * From So_om WHERE ID = 1");
            c1.moveToFirst();
            StringBuilder sb6 = new StringBuilder();
            sb6.append("SELECT so_chon, sum((type_kh =1)*(100-diem_khachgiu)*diem_quydoi)/100 AS diem, ((length(so_chon) = 5) * ");
            sb6.append(c1.getString(7));
            sb6.append(" +(length(so_chon) = 8) * ");
            sb6.append(c1.getString(8));
            sb6.append(" +(length(so_chon) = 11) * ");
            sb6.append(c1.getString(9));
            sb6.append(" + sum(diem_dly_giu*diem_quydoi/100)) AS Om, SUm((type_kh =2)");
            if (this.spin_pointion > -1) {
                str = "*(tbl_soctS.ten_kh='" + this.mwebsite.get(this.spin_pointion) + "')";
            } else {
                str = "";
            }
            sb6.append(str);
            sb6.append(" *diem) as chuyen , SUm((type_kh =1)*(100-diem_khachgiu-diem_dly_giu)*diem_quydoi/100)-SUm((type_kh =2)*diem) -  ((length(so_chon) = 5) * ");
            sb6.append(c1.getString(7));
            sb6.append(" +(length(so_chon) = 8) * ");
            sb6.append(c1.getString(8));
            sb6.append(" +(length(so_chon) = 11) * ");
            sb6.append(c1.getString(9));
            sb6.append(") AS ton, so_nhay   From tbl_soctS Where ngay_nhan='");
            sb6.append(mDate);
            sb6.append("' AND the_loai='xi' AND");
            sb6.append(this.lay_xien);
            sb6.append("  GROUP by so_chon Order by ton DESC, diem DESC");
            str7 = sb6.toString();
            if (c1 != null && !c1.isClosed()) {
                c1.close();
            }
        }
        Cursor Laymax = this.db.GetData(str7);
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        int i = 1;
        if (this.spin_pointion > -1) {
            Laymax = this.db.GetData("select * from tbl_chaytrang_acc Where Username = '" + this.mwebsite.get(this.spin_pointion) + "'");
            Laymax.moveToFirst();
            try {
                try {
                    JSONObject jsonMax = new JSONObject(Laymax.getString(2));
                    int i2 = this.GameType;
                    if (i2 != 0) {
                        if (i2 != 1) {
                            if (i2 == 2) {
                                this.myMax = decimalFormat.format(jsonMax.getInt("max_xi2"));
                                this.MaxChay = jsonMax.has("gia_xi2") ? jsonMax.getInt("gia_xi2") : 560;
                            } else if (i2 == 3) {
                                this.myMax = decimalFormat.format(jsonMax.getInt("max_xi3"));
                                this.MaxChay = jsonMax.has("gia_xi3") ? jsonMax.getInt("gia_xi3") : 520;
                            } else if (i2 != 4) {
                                switch (i2) {
                                    case 21:
                                        this.myMax = decimalFormat.format(jsonMax.getInt("max_dea"));
                                        this.MaxChay = jsonMax.getInt("gia_dea");
                                        break;
                                    case 22:
                                        this.myMax = decimalFormat.format(jsonMax.getInt("max_ded"));
                                        this.MaxChay = jsonMax.getInt("gia_ded");
                                        break;
                                    case 23:
                                        this.myMax = decimalFormat.format(jsonMax.getInt("max_dec"));
                                        this.MaxChay = jsonMax.getInt("gia_dec");
                                        break;
                                }
                            } else {
                                this.myMax = decimalFormat.format(jsonMax.getInt("max_xi4"));
                                this.MaxChay = jsonMax.has("gia_xi4") ? jsonMax.getInt("gia_xi4") : 450;
                            }
                        }
                        this.myMax = decimalFormat.format(jsonMax.getInt("max_lo"));
                        this.MaxChay = jsonMax.getInt("gia_lo");
                    } else {
                        this.myMax = decimalFormat.format(jsonMax.getInt("max_deb"));
                        this.MaxChay = jsonMax.getInt("gia_deb");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } finally {
                Laymax.close();
            }
        } else {
            this.myMax = "0";
        }
        if (Laymax != null) {
            int GiaSo1 = 5;
            if (this.radio_xi.isChecked()) {
                while (Laymax.moveToNext()) {
                    try {
                        this.mSo.add(Laymax.getString(0));
                        this.mTienNhan.add(decimalFormat.format(Laymax.getInt(i)));
                        this.mTienOm.add(decimalFormat.format(Laymax.getInt(2)));
                        this.mTienchuyen.add(decimalFormat.format(Laymax.getInt(3)));
                        this.mTienTon.add(decimalFormat.format(Laymax.getInt(4)));
                        this.mNhay.add(Integer.valueOf(Laymax.getInt(GiaSo1)));
                        this.mMax.add(this.myMax);
                        if (this.radio_xi2.isChecked()) {
                            String[] SoXien = Laymax.getString(0).split(",");
                            if (SoXien.length >= 2) {
                                int GiaSo12 = !this.jsonGia.has(SoXien[0]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien[0]);
                                int GiaSo2 = !this.jsonGia.has(SoXien[i]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien[i]);
                                this.mGia.add("" + ((GiaSo12 + GiaSo2) / 2));
                                this.jsonTienxien.put(Laymax.getString(0), (GiaSo12 + GiaSo2) / 2);
                            }
                        }
                        if (this.radio_xi3.isChecked()) {
                            String[] SoXien2 = Laymax.getString(0).split(",");
                            if (SoXien2.length < 3) {
                                GiaSo1 = 5;
                            } else {
                                int GiaSo13 = !this.jsonGia.has(SoXien2[0]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien2[0]);
                                int GiaSo22 = !this.jsonGia.has(SoXien2[i]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien2[i]);
                                int GiaSo3 = !this.jsonGia.has(SoXien2[2]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien2[2]);
                                this.mGia.add("" + (((GiaSo13 + GiaSo22) + GiaSo3) / 3));
                                this.jsonTienxien.put(Laymax.getString(0), ((GiaSo13 + GiaSo22) + GiaSo3) / 3);
                            }
                        }
                        if (this.radio_xi4.isChecked()) {
                            String[] SoXien3 = Laymax.getString(0).split(",");
                            if (SoXien3.length < 4) {
                                GiaSo1 = 5;
                                i = 1;
                            } else {
                                int GiaSo14 = !this.jsonGia.has(SoXien3[0]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien3[0]);
                                int GiaSo23 = !this.jsonGia.has(SoXien3[1]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien3[1]);
                                int GiaSo32 = !this.jsonGia.has(SoXien3[2]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien3[2]);
                                int GiaSo4 = !this.jsonGia.has(SoXien3[3]) ? this.Price : this.Price + this.jsonGia.getInt(SoXien3[3]);
                                this.mGia.add("" + ((((GiaSo14 + GiaSo23) + GiaSo32) + GiaSo4) / 4));
                                this.jsonTienxien.put(Laymax.getString(0), (((GiaSo14 + GiaSo23) + GiaSo32) + GiaSo4) / 4);
                                GiaSo1 = 5;
                                i = 1;
                            }
                        } else {
                            GiaSo1 = 5;
                            i = 1;
                        }
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
            } else {
                while (Laymax.moveToNext()) {
                    this.mSo.add(Laymax.getString(0));
                    this.mTienNhan.add(decimalFormat.format(Laymax.getInt(1)));
                    this.mTienOm.add(decimalFormat.format(Laymax.getInt(2)));
                    this.mTienchuyen.add(decimalFormat.format(Laymax.getInt(3)));
                    this.mTienTon.add(decimalFormat.format(Laymax.getInt(4)));
                    this.mNhay.add(Integer.valueOf(Laymax.getInt(5)));
                    if (this.jsonGia.has(Laymax.getString(0))) {
                        try {
                            this.mGia.add("" + (this.Price + this.jsonGia.getInt(Laymax.getString(0))));
                        } catch (JSONException e3) {
                            e3.printStackTrace();
                        }
                    } else {
                        this.mGia.add(this.Price + "");
                    }
                    this.mMax.add(this.myMax);
                }
            }
            if (Laymax != null && !Laymax.isClosed()) {
            }
        }
        if (getActivity() != null) {
            this.lview.setAdapter((ListAdapter) new So_OmAdapter(getActivity(), R.layout.frag_canchuyen_lv, this.mSo));
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class So_OmAdapter extends ArrayAdapter {
        public So_OmAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView tview1;
            TextView tview2;
            TextView tview4;
            TextView tview5;
            TextView tview7;
            TextView tview8;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
            ViewHolder holder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.frag_canchuyen_lv, (ViewGroup) null);
                holder.tview2 = (TextView) view.findViewById(R.id.stt);
                holder.tview5 = (TextView) view.findViewById(R.id.Tv_so);
                holder.tview7 = (TextView) view.findViewById(R.id.tv_diemNhan);
                holder.tview8 = (TextView) view.findViewById(R.id.tv_diemOm);
                holder.tview1 = (TextView) view.findViewById(R.id.tv_diemChuyen);
                holder.tview4 = (TextView) view.findViewById(R.id.tv_diemTon);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (Frag_Chaytrang.this.mNhay.get(position).intValue() > 0) {
                holder.tview5.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview7.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview8.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview1.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview4.setTextColor(SupportMenu.CATEGORY_MASK);
                if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 1) {
                    TextView textView = holder.tview5;
                    textView.setText(Frag_Chaytrang.this.mSo.get(position) + "*");
                } else if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 2) {
                    TextView textView2 = holder.tview5;
                    textView2.setText(Frag_Chaytrang.this.mSo.get(position) + "**");
                } else if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 3) {
                    TextView textView3 = holder.tview5;
                    textView3.setText(Frag_Chaytrang.this.mSo.get(position) + "***");
                } else if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 4) {
                    TextView textView4 = holder.tview5;
                    textView4.setText(Frag_Chaytrang.this.mSo.get(position) + "****");
                } else if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 5) {
                    TextView textView5 = holder.tview5;
                    textView5.setText(Frag_Chaytrang.this.mSo.get(position) + "*****");
                } else if (Frag_Chaytrang.this.mNhay.get(position).intValue() == 6) {
                    TextView textView6 = holder.tview5;
                    textView6.setText(Frag_Chaytrang.this.mSo.get(position) + "******");
                }
                TextView textView7 = holder.tview2;
                textView7.setText((position + 1) + "");
                holder.tview7.setText(Frag_Chaytrang.this.mTienTon.get(position));
                holder.tview8.setText("0");
                holder.tview1.setText(Frag_Chaytrang.this.mMax.get(position));
                holder.tview4.setText(Frag_Chaytrang.this.mGia.get(position));
            } else {
                holder.tview5.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview7.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview8.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview1.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview4.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                TextView textView8 = holder.tview2;
                textView8.setText((position + 1) + "");
                holder.tview5.setText(Frag_Chaytrang.this.mSo.get(position));
                holder.tview7.setText(Frag_Chaytrang.this.mTienTon.get(position));
                holder.tview8.setText(Frag_Chaytrang.this.mTienchuyen.get(position));
                holder.tview1.setText(Frag_Chaytrang.this.mMax.get(position));
                if (Frag_Chaytrang.this.mGia.size() > 0) {
                    holder.tview4.setText(Frag_Chaytrang.this.mGia.get(position));
                    if (Integer.parseInt(Frag_Chaytrang.this.mGia.get(position)) > Frag_Chaytrang.this.Price) {
                        holder.tview4.setTextColor(SupportMenu.CATEGORY_MASK);
                    }
                }
            }
            return view;
        }
    }

    private void init() {
        this.radio_de = (RadioButton) this.v.findViewById(R.id.radio_de);
        this.radio_lo = (RadioButton) this.v.findViewById(R.id.radio_lo);
        this.radio_xi = (RadioButton) this.v.findViewById(R.id.radio_xi);
        this.radio_dea = (RadioButton) this.v.findViewById(R.id.radio_dea);
        this.radio_deb = (RadioButton) this.v.findViewById(R.id.radio_deb);
        this.radio_dec = (RadioButton) this.v.findViewById(R.id.radio_dec);
        this.radio_ded = (RadioButton) this.v.findViewById(R.id.radio_ded);
        this.radio_xi2 = (RadioButton) this.v.findViewById(R.id.radio_xi2);
        this.radio_xi3 = (RadioButton) this.v.findViewById(R.id.radio_xi3);
        this.radio_xi4 = (RadioButton) this.v.findViewById(R.id.radio_xi4);
        this.spr_trang = (Spinner) this.v.findViewById(R.id.spr_trang);
        this.btn_Xuatso = (Button) this.v.findViewById(R.id.btn_Xuatso);
        this.lview = (ListView) this.v.findViewById(R.id.lview);
        this.li_loaide = (LinearLayout) this.v.findViewById(R.id.li_loaide);
        this.li_loaixi = (LinearLayout) this.v.findViewById(R.id.li_loaixi);
        this.spr_trang = (Spinner) this.v.findViewById(R.id.spr_trang);
        this.btn_MaXuat = (Button) this.v.findViewById(R.id.btn_MaXuat);
        this.edt_tien = (EditText) this.v.findViewById(R.id.edt_tien);
    }
}
