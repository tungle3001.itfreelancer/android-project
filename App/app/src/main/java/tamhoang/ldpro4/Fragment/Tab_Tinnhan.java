package tamhoang.ldpro4.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import java.util.Vector;
import tamhoang.ldpro4.MyFragmentPagerAdapter;
import tamhoang.ldpro4.R;

/* loaded from: classes2.dex */
public class Tab_Tinnhan extends Fragment implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    int i = 0;
    private MyFragmentPagerAdapter myViewpagerAdapter;
    private TabHost tabHost;
    View v;
    private ViewPager viewPager;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_mo_report, container, false);
        initializeTabHost(savedInstanceState);
        initializeViewPager();
        this.tabHost.setCurrentTab(0);
        return this.v;
    }

    private void initializeViewPager() {
        Vector fragments = new Vector();
        fragments.add(new Frag_Suatin());
        fragments.add(new Frag_NoRP3());
        this.myViewpagerAdapter = new MyFragmentPagerAdapter(getChildFragmentManager(), fragments);
        ViewPager viewPager = (ViewPager) this.v.findViewById(R.id.viewPager);
        this.viewPager = viewPager;
        viewPager.setAdapter(this.myViewpagerAdapter);
        this.viewPager.setOnPageChangeListener(this);
    }

    private void initializeTabHost(Bundle args) {
        TabHost tabHost = (TabHost) this.v.findViewById(16908306);
        this.tabHost = tabHost;
        tabHost.setup();
        TabHost.TabSpec tabSpec1 = this.tabHost.newTabSpec("Sửa tin");
        tabSpec1.setIndicator("Sửa tin");
        tabSpec1.setContent(new FakeContent(getActivity()));
        this.tabHost.addTab(tabSpec1);
        TabHost.TabSpec tabSpec2 = this.tabHost.newTabSpec("Tin nhắn");
        tabSpec2.setIndicator("Tin chi tiết");
        tabSpec2.setContent(new FakeContent(getActivity()));
        this.tabHost.addTab(tabSpec2);
        this.tabHost.setOnTabChangedListener(this);
    }

    @Override // android.widget.TabHost.OnTabChangeListener
    public void onTabChanged(String tabId) {
        int pos = this.tabHost.getCurrentTab();
        this.viewPager.setCurrentItem(pos);
        HorizontalScrollView hScrollView = (HorizontalScrollView) this.v.findViewById(R.id.hScrollView);
        View tabView = this.tabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft() - ((hScrollView.getWidth() - tabView.getWidth()) / 2);
        hScrollView.smoothScrollTo(scrollPos, 0);
    }

    @Override // android.support.v4.view.ViewPager.OnPageChangeListener
    public void onPageScrollStateChanged(int arg0) {
    }

    @Override // android.support.v4.view.ViewPager.OnPageChangeListener
    public void onPageScrolled(int arg0, float arg1, int arg2) {
    }

    @Override // android.support.v4.view.ViewPager.OnPageChangeListener
    public void onPageSelected(int position) {
        this.tabHost.setCurrentTab(position);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class FakeContent implements TabHost.TabContentFactory {
        private final Context mContext;

        public FakeContent(Context context) {
            this.mContext = context;
        }

        @Override // android.widget.TabHost.TabContentFactory
        public View createTabContent(String tag) {
            View v = new View(this.mContext);
            v.setMinimumHeight(0);
            v.setMinimumWidth(0);
            return v;
        }
    }
}
