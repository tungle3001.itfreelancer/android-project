package tamhoang.ldpro4.Fragment;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Livestream extends Fragment {
    MainActivity activity;
    Database db;
    Handler handler;
    LinearLayout ln1;
    ListView lvLivestrem;
    String mDate;
    RadioButton radio_dea;
    RadioButton radio_deb;
    RadioButton radio_dec;
    RadioButton radio_ded;
    int so;
    Switch switch1;
    TextView tvChuy;
    public List<String> mDiem = new ArrayList();
    public List<Integer> mDemso = new ArrayList();
    public List<Double> mTongTien = new ArrayList();
    public List<Double> mThangthua = new ArrayList();
    String th_loai = "the_loai = 'deb'";
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Livestream.7
        @Override // java.lang.Runnable
        public void run() {
            new MainActivity();
            if (MainActivity.sms) {
                Livestream.this.xem_lv();
                MainActivity.sms = false;
            }
            Livestream.this.handler.postDelayed(this, 1000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_livestream, container, false);
        this.db = new Database(getActivity());
        this.lvLivestrem = (ListView) v.findViewById(R.id.lv_livestrem);
        this.tvChuy = (TextView) v.findViewById(R.id.tv_chu_y);
        this.switch1 = (Switch) v.findViewById(R.id.switch1);
        this.ln1 = (LinearLayout) v.findViewById(R.id.ln1);
        this.radio_dea = (RadioButton) v.findViewById(R.id.radio_Dea);
        this.radio_deb = (RadioButton) v.findViewById(R.id.radio_Deb);
        this.radio_dec = (RadioButton) v.findViewById(R.id.radio_Dec);
        this.radio_ded = (RadioButton) v.findViewById(R.id.radio_Ded);
        this.activity = new MainActivity();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
        dmyFormat.setTimeZone(TimeZone.getDefault());
        this.mDate = dmyFormat.format(calendar.getTime());
        this.lvLivestrem.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.1
            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            }
        });
        this.switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Livestream.this.switch1.isChecked()) {
                    Livestream.this.ln1.setVisibility(0);
                } else {
                    Livestream.this.ln1.setVisibility(8);
                }
            }
        });
        Handler handler = new Handler();
        this.handler = handler;
        handler.postDelayed(this.runnable, 1000L);
        this.radio_dea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Livestream.this.radio_dea.isChecked()) {
                    Livestream.this.th_loai = "the_loai = 'dea'";
                    Livestream.this.xem_lv();
                }
            }
        });
        this.radio_deb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Livestream.this.radio_deb.isChecked()) {
                    Livestream.this.th_loai = "the_loai = 'deb'";
                    Livestream.this.xem_lv();
                }
            }
        });
        this.radio_dec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.5
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Livestream.this.radio_dec.isChecked()) {
                    Livestream.this.th_loai = "the_loai = 'dec'";
                    Livestream.this.xem_lv();
                }
            }
        });
        this.radio_ded.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Livestream.6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Livestream.this.radio_ded.isChecked()) {
                    Livestream.this.th_loai = "the_loai = 'ded'";
                    Livestream.this.xem_lv();
                }
            }
        });
        xem_lv();
        return v;
    }

    @Override // android.support.v4.app.Fragment
    public void onStop() {
        super.onStop();
        this.handler.removeCallbacks(this.runnable);
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        this.mDiem.clear();
        this.mDemso.clear();
        this.mTongTien.clear();
        this.mThangthua.clear();
        this.lvLivestrem.setAdapter((ListAdapter) null);
        super.onDestroy();
    }

    public void xem_lv() {
        String pattern;
        String pattern2 = "###,###";
        DecimalFormat decimalFormat = new DecimalFormat(pattern2);
        MainActivity mainActivity = new MainActivity();
        String mDate = MainActivity.Get_date();
        String str = "SELECT count(mycount) AS dem FROM (SELECT sum((type_kh = 1) *diem_ton) - sum((type_kh = 2) * diem_ton) AS mycount FROM tbl_soctS WHERE " + this.th_loai + " AND ngay_nhan = '" + mDate + "' GROUP BY so_chon ) a";
        Cursor Demso = this.db.GetData(str);
        Demso.moveToFirst();
        int i = Demso.getInt(0);
        this.so = i;
        if (i > 0) {
            this.tvChuy.setText("Có " + (100 - Demso.getInt(0)) + " số 0 đồng");
        } else {
            this.tvChuy.setText("Chưa có dữ liệu ngày hôm nay.");
        }
        this.mDiem.clear();
        this.mDemso.clear();
        this.mTongTien.clear();
        this.mThangthua.clear();
        String str1 = "SELECT moctien, count(moctien) AS dem\nFROM (Select (Sum((tbl_soctS.type_kh =1) * tbl_soctS.diem_ton) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_ton) - so_om.Om_DeB ) as moctien\nFrom so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \nWhere tbl_soctS.ngay_nhan='" + mDate + "' AND (tbl_soctS." + this.th_loai + " OR tbl_soctS.the_loai='det') \nGROUP by so_om.So order by moctien DESC) as a \nGROUP BY moctien ORDER BY moctien DESC";
        Cursor cursor = this.db.GetData(str1);
        while (cursor.moveToNext()) {
            this.mDiem.add(decimalFormat.format(cursor.getDouble(0)));
            this.mDemso.add(Integer.valueOf(cursor.getInt(1)));
        }
        if (this.mDiem.size() > 0) {
            int i2 = 0;
            while (i2 < this.mDiem.size()) {
                int diem = Integer.parseInt(this.mDiem.get(i2).replaceAll("\\.", ""));
                double tongtien = diem * 100;
                if (i2 < this.mDiem.size()) {
                    int j = i2 + 1;
                    while (true) {
                        pattern = pattern2;
                        if (j < this.mDiem.size()) {
                            double parseInt = (diem - Integer.parseInt(this.mDiem.get(j).replaceAll("\\.", ""))) * this.mDemso.get(j).intValue();
                            Double.isNaN(parseInt);
                            tongtien -= parseInt;
                            j++;
                            pattern2 = pattern;
                            decimalFormat = decimalFormat;
                        }
                    }
                } else {
                    pattern = pattern2;
                }
                List<Double> list = this.mTongTien;
                double d = (100 - this.so) * diem;
                Double.isNaN(d);
                list.add(Double.valueOf(((tongtien - d) * 715.0d) / 1000.0d));
                List<Double> list2 = this.mThangthua;
                double d2 = (100 - this.so) * diem;
                Double.isNaN(d2);
                double d3 = diem * 70;
                Double.isNaN(d3);
                list2.add(Double.valueOf((((tongtien - d2) * 715.0d) / 1000.0d) - d3));
                i2++;
                mainActivity = mainActivity;
                pattern2 = pattern;
                decimalFormat = decimalFormat;
            }
        }
        if (getActivity() != null) {
            this.lvLivestrem.setAdapter((ListAdapter) new TNGAdapter(getActivity(), R.layout.activity_livestream_lv, this.mDiem));
        }
        if (cursor != null) {
            cursor.close();
        }
        if (Demso != null) {
            Demso.close();
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class TNGAdapter extends ArrayAdapter {
        String pattern = "###,###";
        DecimalFormat decimalFormat = new DecimalFormat(this.pattern);

        public TNGAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.activity_livestream_lv, (ViewGroup) null);
            TextView tview1 = (TextView) v.findViewById(R.id.tv_diem);
            tview1.setText(Livestream.this.mDiem.get(position));
            TextView tview2 = (TextView) v.findViewById(R.id.tv_so);
            tview2.setText(Livestream.this.mDemso.get(position) + "");
            TextView tview3 = (TextView) v.findViewById(R.id.tv_tiengiu);
            tview3.setText(this.decimalFormat.format(Livestream.this.mTongTien.get(position)));
            TextView tview4 = (TextView) v.findViewById(R.id.tv_ThangThua);
            tview4.setText(this.decimalFormat.format(Livestream.this.mThangthua.get(position)));
            return v;
        }
    }
}
