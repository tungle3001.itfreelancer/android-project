package tamhoang.ldpro4.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.support.graphics.drawable.PathInterpolatorCompat;
import android.support.v4.app.Fragment;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.RangeSeekBar;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_CanChuyen extends Fragment {
    Button btn_Xuatso;
    CheckBox check_x2;
    CheckBox check_x3;
    CheckBox check_x4;
    CheckBox check_xn;
    Database db;
    EditText edt_tien;
    Handler handler;
    JSONObject jsonKhongmax;
    String lay_x2;
    String lay_x3;
    String lay_x4;
    LinearLayout layout;
    LinearLayout li_loaide;
    LinearLayout ln_xi;
    ListView no_rp_number;
    RadioButton radio_bc;
    RadioButton radio_de;
    RadioButton radio_dea;
    RadioButton radio_deb;
    RadioButton radio_dec;
    RadioButton radio_ded;
    RadioButton radio_lo;
    RadioButton radio_xi;
    RangeSeekBar<Integer> rangeSeekBar;
    String sapxep;
    public View v;
    String xuatDan;
    int min = 0;
    int max = 100;
    String DangXuat = null;
    int mSpiner = 0;
    public List<String> mSo = new ArrayList();
    public List<String> mTienNhan = new ArrayList();
    public List<String> mTienOm = new ArrayList();
    public List<String> mTienchuyen = new ArrayList();
    public List<String> mTienTon = new ArrayList();
    public List<Integer> mNhay = new ArrayList();
    public List<String> mContact = new ArrayList();
    public List<String> mMobile = new ArrayList();
    public List<String> mKhongMax = new ArrayList();
    public List<String> mAppuse = new ArrayList();
    boolean Dachuyen = false;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.16
        @Override // java.lang.Runnable
        public void run() {
            new MainActivity();
            if (MainActivity.sms) {
                Frag_CanChuyen.this.xemlv();
                MainActivity.sms = false;
            }
            Frag_CanChuyen.this.handler.postDelayed(this, 1000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.frag_canchuyen, container, false);
        this.v = inflate;
        this.radio_de = (RadioButton) inflate.findViewById(R.id.radio_de);
        this.radio_lo = (RadioButton) this.v.findViewById(R.id.radio_lo);
        this.radio_xi = (RadioButton) this.v.findViewById(R.id.radio_xi);
        this.radio_bc = (RadioButton) this.v.findViewById(R.id.radio_bc);
        this.radio_dea = (RadioButton) this.v.findViewById(R.id.radio_dea);
        this.radio_deb = (RadioButton) this.v.findViewById(R.id.radio_deb);
        this.radio_dec = (RadioButton) this.v.findViewById(R.id.radio_dec);
        this.radio_ded = (RadioButton) this.v.findViewById(R.id.radio_ded);
        this.btn_Xuatso = (Button) this.v.findViewById(R.id.btn_Xuatso);
        LinearLayout linearLayout = (LinearLayout) this.v.findViewById(R.id.ln_xi);
        this.ln_xi = linearLayout;
        linearLayout.setVisibility(8);
        LinearLayout linearLayout2 = (LinearLayout) this.v.findViewById(R.id.li_loaide);
        this.li_loaide = linearLayout2;
        linearLayout2.setVisibility(8);
        this.edt_tien = (EditText) this.v.findViewById(R.id.edt_tien);
        this.check_x2 = (CheckBox) this.v.findViewById(R.id.check_x2);
        this.check_x3 = (CheckBox) this.v.findViewById(R.id.check_x3);
        this.check_x4 = (CheckBox) this.v.findViewById(R.id.check_x4);
        this.check_xn = (CheckBox) this.v.findViewById(R.id.check_xn);
        this.no_rp_number = (ListView) this.v.findViewById(R.id.lview);
        this.db = new Database(getActivity());
        Handler handler = new Handler();
        this.handler = handler;
        handler.postDelayed(this.runnable, 1000L);
        RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<>(getActivity());
        this.rangeSeekBar = rangeSeekBar;
        rangeSeekBar.setRangeValues(0, 100);
        this.rangeSeekBar.setSelectedMinValue(0);
        this.rangeSeekBar.setSelectedMaxValue(100);
        LinearLayout linearLayout3 = (LinearLayout) this.v.findViewById(R.id.seekbar);
        this.layout = linearLayout3;
        linearLayout3.addView(this.rangeSeekBar);
        this.rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.1
            @Override // tamhoang.ldpro4.RangeSeekBar.OnRangeSeekBarChangeListener
            public /* bridge */ /* synthetic */ void onRangeSeekBarValuesChanged(RangeSeekBar rangeSeekBar2, Integer num, Integer num2) {
                onRangeSeekBarValuesChanged2((RangeSeekBar<?>) rangeSeekBar2, num, num2);
            }

            /* renamed from: onRangeSeekBarValuesChanged  reason: avoid collision after fix types in other method */
            public void onRangeSeekBarValuesChanged2(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                Frag_CanChuyen.this.min = minValue.intValue();
                Frag_CanChuyen.this.max = maxValue.intValue();
            }
        });
        this.radio_de.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.radio_de.isChecked()) {
                    Frag_CanChuyen.this.layout.setVisibility(0);
                    new MainActivity();
                    String mDate = MainActivity.Get_date();
                    try {
                        Database database = Frag_CanChuyen.this.db;
                        Cursor cursor = database.GetData("Select sum((the_loai = 'dea')* diem) as de_a\n,sum((the_loai = 'deb')* diem) as de_b\n,sum((the_loai = 'det')* diem) as de_t\n,sum((the_loai = 'dec')* diem) as de_c\n,sum((the_loai = 'ded')* diem) as de_d\nFrom tbl_soctS \nWhere ngay_nhan = '" + mDate + "'");
                        if (!cursor.moveToFirst() || cursor == null) {
                            Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            return;
                        }
                        int[] dem = new int[5];
                        if (cursor.getDouble(0) > 0.0d) {
                            dem[0] = 1;
                            Frag_CanChuyen.this.radio_dea.setEnabled(true);
                        } else {
                            dem[0] = 0;
                            Frag_CanChuyen.this.radio_dea.setEnabled(false);
                        }
                        if (cursor.getDouble(1) > 0.0d) {
                            dem[1] = 1;
                            Frag_CanChuyen.this.radio_deb.setEnabled(true);
                        } else {
                            dem[1] = 0;
                            Frag_CanChuyen.this.radio_deb.setEnabled(false);
                        }
                        if (cursor.getDouble(2) > 0.0d) {
                            dem[2] = 1;
                        } else {
                            dem[2] = 0;
                        }
                        if (cursor.getDouble(3) > 0.0d) {
                            dem[3] = 1;
                            Frag_CanChuyen.this.radio_dec.setEnabled(true);
                        } else {
                            dem[3] = 0;
                            Frag_CanChuyen.this.radio_dec.setEnabled(false);
                        }
                        if (cursor.getDouble(4) > 0.0d) {
                            dem[4] = 1;
                            Frag_CanChuyen.this.radio_ded.setEnabled(true);
                        } else {
                            dem[4] = 0;
                            Frag_CanChuyen.this.radio_ded.setEnabled(false);
                        }
                        if (dem[0] == 0 && ((dem[1] == 1 || dem[2] == 1) && dem[3] == 0 && dem[4] == 0)) {
                            Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_CanChuyen.this.ln_xi.setVisibility(8);
                            Frag_CanChuyen.this.li_loaide.setVisibility(8);
                            Frag_CanChuyen.this.radio_deb.setChecked(true);
                            Frag_CanChuyen.this.xem_RecycView();
                        } else if (dem[0] == 0 && dem[1] == 0 && dem[2] == 0 && dem[3] == 0 && dem[4] == 0) {
                            Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_CanChuyen.this.ln_xi.setVisibility(8);
                            Frag_CanChuyen.this.li_loaide.setVisibility(8);
                            Frag_CanChuyen.this.radio_deb.setChecked(true);
                            Frag_CanChuyen.this.xem_RecycView();
                        } else {
                            Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                            Frag_CanChuyen.this.ln_xi.setVisibility(8);
                            Frag_CanChuyen.this.li_loaide.setVisibility(0);
                            Frag_CanChuyen.this.radio_deb.setChecked(true);
                            Frag_CanChuyen.this.xem_RecycView();
                        }
                        if (!cursor.isClosed() && cursor != null && !cursor.isClosed()) {
                            cursor.close();
                        }
                    } catch (SQLException e) {
                        Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                    }
                }
            }
        });
        this.radio_dea.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_CanChuyen.this.radio_dea.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'dea'";
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.radio_deb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_CanChuyen.this.radio_deb.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.radio_dec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.5
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_CanChuyen.this.radio_dec.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'dec'";
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.radio_ded.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (Frag_CanChuyen.this.radio_ded.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'ded'";
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.radio_lo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.7
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.radio_lo.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'lo'";
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.li_loaide.setVisibility(8);
                    Frag_CanChuyen.this.layout.setVisibility(0);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.radio_xi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.8
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.radio_xi.isChecked()) {
                    new MainActivity();
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'xi'";
                    Frag_CanChuyen.this.layout.setVisibility(8);
                    Frag_CanChuyen.this.ln_xi.setVisibility(0);
                    Frag_CanChuyen.this.li_loaide.setVisibility(8);
                    try {
                        Database database = Frag_CanChuyen.this.db;
                        Cursor cursor = database.GetData("Select count(id) From tbl_soctS WHERE the_loai = 'xn' AND ngay_nhan = '" + MainActivity.Get_date() + "'");
                        if (cursor.moveToFirst() && cursor.getInt(0) > 0) {
                            Frag_CanChuyen.this.check_xn.setVisibility(0);
                        }
                        Frag_CanChuyen.this.xem_RecycView();
                    } catch (SQLException e) {
                    }
                }
            }
        });
        this.radio_bc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.9
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.radio_bc.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'bc'";
                    Frag_CanChuyen.this.layout.setVisibility(8);
                    Frag_CanChuyen.this.ln_xi.setVisibility(8);
                    Frag_CanChuyen.this.li_loaide.setVisibility(8);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.check_x2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.10
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.check_x2.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'xi'";
                    Frag_CanChuyen.this.lay_x2 = "length(so_chon) = 5 ";
                    Frag_CanChuyen.this.check_xn.setChecked(false);
                } else {
                    Frag_CanChuyen.this.lay_x2 = "";
                }
                Frag_CanChuyen.this.xem_RecycView();
            }
        });
        this.check_x3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.11
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.check_x3.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'xi'";
                    Frag_CanChuyen.this.lay_x3 = "OR length(so_chon) = 8 ";
                    Frag_CanChuyen.this.check_xn.setChecked(false);
                } else {
                    Frag_CanChuyen.this.lay_x3 = "";
                }
                Frag_CanChuyen.this.xem_RecycView();
            }
        });
        this.check_x4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.12
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Frag_CanChuyen.this.check_x4.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'xi'";
                    Frag_CanChuyen.this.lay_x4 = "OR length(so_chon) = 11 ";
                    Frag_CanChuyen.this.check_xn.setChecked(false);
                } else {
                    Frag_CanChuyen.this.lay_x4 = "";
                }
                Frag_CanChuyen.this.xem_RecycView();
            }
        });
        this.check_xn.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.13
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                if (Frag_CanChuyen.this.check_xn.isChecked()) {
                    Frag_CanChuyen.this.DangXuat = "the_loai = 'xn'";
                    Frag_CanChuyen.this.check_x2.setChecked(false);
                    Frag_CanChuyen.this.check_x3.setChecked(false);
                    Frag_CanChuyen.this.check_x4.setChecked(false);
                    Frag_CanChuyen.this.xem_RecycView();
                }
            }
        });
        this.btn_Xuatso.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.14
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                String str = Frag_CanChuyen.this.edt_tien.getText().toString();
                if (Congthuc.isNumeric(str.replaceAll("%", "").replaceAll("n", "").replaceAll("k", "").replaceAll("d", "").replaceAll(">", "").replaceAll("\\.", "")) || Frag_CanChuyen.this.edt_tien.getText().toString().length() == 0) {
                    Frag_CanChuyen.this.btn_click();
                } else {
                    Toast.makeText(Frag_CanChuyen.this.getActivity(), "Kiểm tra lại tiền!", 1).show();
                }
            }
        });
        this.lay_x2 = "length(so_chon) = 5 ";
        this.lay_x3 = "OR length(so_chon) = 8 ";
        this.lay_x4 = "OR length(so_chon) = 11 ";
        this.no_rp_number.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.15
            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    new MainActivity();
                    String mDate = MainActivity.Get_date();
                    String s = "Select ten_kh, sum(diem_quydoi) From tbl_soctS WHERE so_chon = '" + Frag_CanChuyen.this.mSo.get(position) + "' AND ngay_nhan = '" + mDate + "' AND type_kh = 1 AND " + Frag_CanChuyen.this.DangXuat + " GROUP BY so_dienthoai";
                    Cursor c = Frag_CanChuyen.this.db.GetData(s);
                    String s1 = "";
                    while (c.moveToNext()) {
                        s1 = s1 + c.getString(0) + ": " + c.getString(1) + "\n";
                    }
                    Toast.makeText(Frag_CanChuyen.this.getActivity(), s1, 1).show();
                } catch (SQLException e) {
                }
            }
        });
        try {
            if (MainActivity.jSon_Setting.getInt("bao_cao_so") == 0) {
                this.sapxep = "diem DESC";
            } else {
                this.sapxep = "ton DESC, diem DESC";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        xemlv();
        return this.v;
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.runnable);
    }

    public void xemlv() {
        if (this.DangXuat != null) {
            xem_RecycView();
        } else {
            this.radio_de.setChecked(true);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:174:0x06a5  */
    /* JADX WARN: Removed duplicated region for block: B:177:0x06af  */
    /* JADX WARN: Removed duplicated region for block: B:178:0x06c8  */
    /* JADX WARN: Removed duplicated region for block: B:188:0x073e  */
    /* JADX WARN: Removed duplicated region for block: B:32:0x00fe  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void btn_click() {
        /*
            Method dump skipped, instructions count: 1934
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.btn_click():void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:52:0x0163
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public String TaoTinDe(String r24) {
        /*
            Method dump skipped, instructions count: 672
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.TaoTinDe(java.lang.String):java.lang.String");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:52:0x0163
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public String TaoTinLo(String r24) {
        /*
            Method dump skipped, instructions count: 672
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.TaoTinLo(java.lang.String):java.lang.String");
    }

    /* JADX WARN: Code restructure failed: missing block: B:15:0x007a, code lost:
        if (r12.isClosed() != false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:16:0x007d, code lost:
        r0 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0084, code lost:
        if (r0 >= r23.mSo.size()) goto L133;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0086, code lost:
        r14 = r23.mSo.get(r0);
        r5 = r23.jsonKhongmax.getInt("cang");
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x00a3, code lost:
        r8 = java.lang.Integer.parseInt(r23.mTienTon.get(r0).replace(".", ""));
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x00b1, code lost:
        if (r7.has(r14) == false) goto L37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x00b3, code lost:
        if (r8 <= 0) goto L37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x00b5, code lost:
        r13 = new org.json.JSONObject();
        r17 = r7.getJSONObject(r14).getDouble("Da_chuyen");
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x00c2, code lost:
        r15 = r9;
        r9 = r8;
        java.lang.Double.isNaN(r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x00cb, code lost:
        if ((r17 + r9) > r5) goto L28;
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x00cd, code lost:
        r13.put("So_chon", r14);
        r13.put("Da_chuyen", r7.getJSONObject(r14).getInt("Da_chuyen") + r8);
        r13.put(r3, r8);
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x00e0, code lost:
        r13.put("So_chon", r14);
        r13.put("Da_chuyen", r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x00ee, code lost:
        r9 = r7.getJSONObject(r14).getInt("Da_chuyen");
        java.lang.Double.isNaN(r5);
        java.lang.Double.isNaN(r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x00f7, code lost:
        r13.put(r3, r5 - r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x00fa, code lost:
        r7.put(r14, r13);
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00fd, code lost:
        r13 = r11;
     */
    /* JADX WARN: Code restructure failed: missing block: B:33:0x00ff, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:35:0x0102, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x0106, code lost:
        r15 = r9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x0107, code lost:
        r9 = new org.json.JSONObject();
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x010c, code lost:
        r13 = r11;
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0110, code lost:
        if (r8 > r5) goto L42;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x0112, code lost:
        r9.put("So_chon", r14);
        r9.put("Da_chuyen", r8);
        r9.put(r3, r8);
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x011c, code lost:
        r9.put("So_chon", r14);
        r9.put("Da_chuyen", r5);
        r9.put(r3, r5);
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x0125, code lost:
        r7.put(r14, r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x0128, code lost:
        r0 = r0 + 1;
        r11 = r13;
        r9 = r15;
        r8 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x0133, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x0135, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x0138, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x0141, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x0146, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x0149, code lost:
        r4 = r7.keys();
        r0 = new java.util.ArrayList<>();
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x0157, code lost:
        if (r4.hasNext() != false) goto L57;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x0159, code lost:
        r9 = r4.next();
     */
    /* JADX WARN: Code restructure failed: missing block: B:59:0x0168, code lost:
        if (r7.getJSONObject(r9).getInt(r3) > 0) goto L127;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x016a, code lost:
        r0.add(r7.getJSONObject(r9));
     */
    /* JADX WARN: Code restructure failed: missing block: B:61:0x0172, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x0173, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x0177, code lost:
        java.util.Collections.sort(r0, new tamhoang.ldpro4.Fragment.Frag_CanChuyen.AnonymousClass19(r23));
        r23.xuatDan = "Cang:";
        r9 = 0;
        r0 = 0;
        r11 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0196, code lost:
        if (r0 < r0.size()) goto L105;
     */
    /* JADX WARN: Code restructure failed: missing block: B:67:0x0198, code lost:
        r18 = r0.get(r0).getInt(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x01a8, code lost:
        if (r18 > 0) goto L70;
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x01aa, code lost:
        r19 = r5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x01ae, code lost:
        if (r9 > r18) goto L103;
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x01b0, code lost:
        r6 = new java.lang.StringBuilder();
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x01b5, code lost:
        r21 = r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x01b7, code lost:
        r6.append(r23.xuatDan);
        r6.append("x");
        r6.append(r9);
        r6.append("n ");
        r23.xuatDan = r6.toString();
        r23.xuatDan += r14.getString("So_chon") + ",";
        r4 = r18;
        r11 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x01e8, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:77:0x01ed, code lost:
        r21 = r7;
        r23.xuatDan += r14.getString("So_chon") + ",";
        r4 = r18;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x020a, code lost:
        r11 = r11 + 1;
        r9 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x020e, code lost:
        r19 = r5;
        r21 = r7;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x0213, code lost:
        r0 = r0 + 1;
        r4 = r4;
        r3 = r3;
        r5 = r19;
        r7 = r21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x021f, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:84:0x0230, code lost:
        if (r23.xuatDan.length() > 4) goto L85;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x0234, code lost:
        r23.xuatDan += "x" + r9 + "n ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:87:0x024e, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:88:0x0250, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0063, code lost:
        if (r12.isClosed() == false) goto L9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:90:0x0257, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:92:0x025c, code lost:
        return r23.xuatDan;
     */
    /* JADX WARN: Code restructure failed: missing block: B:9:0x0065, code lost:
        r12.close();
     */
    /* JADX WARN: Removed duplicated region for block: B:105:0x0198 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:57:0x0159  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x026a  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TaoTinCang(String r24) {
        /*
            Method dump skipped, instructions count: 624
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.TaoTinCang(java.lang.String):java.lang.String");
    }

    /* JADX WARN: Code restructure failed: missing block: B:14:0x007c, code lost:
        if (r13.isClosed() != false) goto L15;
     */
    /* JADX WARN: Code restructure failed: missing block: B:15:0x007f, code lost:
        r0 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x0086, code lost:
        if (r0 >= r23.mSo.size()) goto L117;
     */
    /* JADX WARN: Code restructure failed: missing block: B:18:0x0088, code lost:
        r14 = r23.mSo.get(r0);
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x0090, code lost:
        r15 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x009a, code lost:
        if (r14.length() != 5) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x00a2, code lost:
        if (r23.jsonKhongmax.getInt(r4) != 0) goto L26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x00a4, code lost:
        r15 = 100000;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x00a8, code lost:
        r15 = r23.jsonKhongmax.getInt(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x00b6, code lost:
        if (r14.length() != 8) goto L33;
     */
    /* JADX WARN: Code restructure failed: missing block: B:30:0x00be, code lost:
        if (r23.jsonKhongmax.getInt(r3) != 0) goto L32;
     */
    /* JADX WARN: Code restructure failed: missing block: B:31:0x00c0, code lost:
        r15 = 100000;
     */
    /* JADX WARN: Code restructure failed: missing block: B:32:0x00c4, code lost:
        r15 = r23.jsonKhongmax.getInt(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x00d2, code lost:
        if (r14.length() != 11) goto L39;
     */
    /* JADX WARN: Code restructure failed: missing block: B:36:0x00da, code lost:
        if (r23.jsonKhongmax.getInt(r2) != 0) goto L38;
     */
    /* JADX WARN: Code restructure failed: missing block: B:37:0x00dc, code lost:
        r15 = 100000;
     */
    /* JADX WARN: Code restructure failed: missing block: B:38:0x00e0, code lost:
        r15 = r23.jsonKhongmax.getInt(r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:39:0x00e7, code lost:
        r2 = java.lang.Integer.parseInt(r23.mTienTon.get(r0).replace(".", ""));
     */
    /* JADX WARN: Code restructure failed: missing block: B:40:0x0101, code lost:
        if (r0.has(r14) == false) goto L48;
     */
    /* JADX WARN: Code restructure failed: missing block: B:41:0x0103, code lost:
        if (r2 <= 0) goto L48;
     */
    /* JADX WARN: Code restructure failed: missing block: B:42:0x0105, code lost:
        r9 = new org.json.JSONObject();
        r19 = r0.getJSONObject(r14).getDouble("Da_chuyen");
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x0112, code lost:
        r10 = r3;
        r21 = r4;
        r3 = r2;
        java.lang.Double.isNaN(r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:44:0x011e, code lost:
        if ((r19 + r3) > r15) goto L46;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x0120, code lost:
        r9.put("So_chon", r14);
        r9.put("Da_chuyen", r0.getJSONObject(r14).getInt("Da_chuyen") + r2);
        r9.put("Se_chuyen", r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:46:0x0133, code lost:
        r9.put("So_chon", r14);
        r9.put("Da_chuyen", r15);
        r9.put("Se_chuyen", r15 - r0.getJSONObject(r14).getInt("Da_chuyen"));
     */
    /* JADX WARN: Code restructure failed: missing block: B:47:0x0146, code lost:
        r0.put(r14, r9);
     */
    /* JADX WARN: Code restructure failed: missing block: B:48:0x014b, code lost:
        r10 = r3;
        r21 = r4;
     */
    /* JADX WARN: Code restructure failed: missing block: B:49:0x014e, code lost:
        if (r2 <= 0) goto L118;
     */
    /* JADX WARN: Code restructure failed: missing block: B:50:0x0150, code lost:
        r3 = new org.json.JSONObject();
     */
    /* JADX WARN: Code restructure failed: missing block: B:51:0x0155, code lost:
        if (r2 > r15) goto L53;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x0157, code lost:
        r3.put("So_chon", r14);
        r3.put("Da_chuyen", r2);
        r3.put("Se_chuyen", r2);
     */
    /* JADX WARN: Code restructure failed: missing block: B:53:0x0161, code lost:
        r3.put("So_chon", r14);
        r3.put("Da_chuyen", r15);
        r3.put("Se_chuyen", r15);
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x016a, code lost:
        r0.put(r14, r3);
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x016d, code lost:
        r0 = r0 + 1;
        r9 = r9;
        r10 = r10;
        r2 = r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:56:0x017a, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:57:0x017c, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:60:0x0185, code lost:
        r0 = e;
     */
    /* JADX WARN: Code restructure failed: missing block: B:62:0x018a, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:63:0x018d, code lost:
        r2 = r0.keys();
        r0 = new java.util.ArrayList<>();
     */
    /* JADX WARN: Code restructure failed: missing block: B:65:0x019b, code lost:
        if (r2.hasNext() != false) goto L66;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x019d, code lost:
        r4 = r2.next();
     */
    /* JADX WARN: Code restructure failed: missing block: B:68:0x01ac, code lost:
        if (r0.getJSONObject(r4).getInt("Se_chuyen") > 0) goto L107;
     */
    /* JADX WARN: Code restructure failed: missing block: B:69:0x01ae, code lost:
        r0.add(r0.getJSONObject(r4));
     */
    /* JADX WARN: Code restructure failed: missing block: B:70:0x01b6, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x01b7, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:72:0x01bb, code lost:
        java.util.Collections.sort(r0, new tamhoang.ldpro4.Fragment.Frag_CanChuyen.AnonymousClass20(r23));
        r23.xuatDan = "Xien:\n";
        r4 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:74:0x01cd, code lost:
        if (r4 < r0.size()) goto L75;
     */
    /* JADX WARN: Code restructure failed: missing block: B:75:0x01cf, code lost:
        r7 = r0.get(r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x01e0, code lost:
        if (tamhoang.ldpro4.MainActivity.jSon_Setting.getInt("chuyen_xien") > 0) goto L79;
     */
    /* JADX WARN: Code restructure failed: missing block: B:79:0x01e2, code lost:
        r23.xuatDan += r7.getString("So_chon") + "x" + (r7.getInt("Se_chuyen") / 10) + "d ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:7:0x0067, code lost:
        if (r13.isClosed() == false) goto L8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:80:0x020b, code lost:
        r23.xuatDan += r7.getString("So_chon") + "x" + r7.getInt("Se_chuyen") + "n ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:81:0x0232, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:82:0x0233, code lost:
        r0.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:83:0x0236, code lost:
        r4 = r4 + 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:85:0x023b, code lost:
        return r23.xuatDan;
     */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0069, code lost:
        r13.close();
     */
    /* JADX WARN: Removed duplicated region for block: B:90:0x0247  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String TaoTinXi(String r24) {
        /*
            Method dump skipped, instructions count: 589
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.TaoTinXi(java.lang.String):java.lang.String");
    }

    public void Dialog(int poin) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.frag_canchuyen1);
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        final String Chuyendi = this.xuatDan.replaceAll(",x", "x");
        this.Dachuyen = false;
        Spinner sprin_tenkhach = (Spinner) dialog.findViewById(R.id.sprin_tenkhach);
        final EditText edt_XuatDan = (EditText) dialog.findViewById(R.id.edt_XuatDan);
        Button btn_chuyendi = (Button) dialog.findViewById(R.id.btn_chuyendi);
        edt_XuatDan.setText("");
        edt_XuatDan.setText(this.xuatDan.replaceAll(",x", "x"));
        try {
            Cursor cur = this.db.GetData("Select * From tbl_kh_new WHERE type_kh <> 1 ORDER BY ten_kh");
            this.mContact.clear();
            this.mMobile.clear();
            this.mKhongMax.clear();
            this.mAppuse.clear();
            while (cur.moveToNext()) {
                if (cur.getString(2).indexOf("sms") <= -1 && cur.getString(2).indexOf("TL") <= -1) {
                    if (MainActivity.arr_TenKH.indexOf(cur.getString(1)) > -1) {
                        this.mContact.add(cur.getString(0));
                        this.mMobile.add(cur.getString(1));
                        this.mKhongMax.add(cur.getString(6));
                        this.mAppuse.add(cur.getString(2));
                    }
                }
                this.mContact.add(cur.getString(0));
                this.mMobile.add(cur.getString(1));
                this.mKhongMax.add(cur.getString(6));
                this.mAppuse.add(cur.getString(2));
            }
            if (cur != null && !cur.isClosed()) {
                cur.close();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), (int) R.layout.spinner_item, this.mContact);
            sprin_tenkhach.setAdapter((SpinnerAdapter) adapter);
        } catch (SQLException e) {
        }
        sprin_tenkhach.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.21
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Frag_CanChuyen.this.mSpiner = position;
                try {
                    Frag_CanChuyen.this.jsonKhongmax = new JSONObject(Frag_CanChuyen.this.mKhongMax.get(Frag_CanChuyen.this.mSpiner));
                    if (Frag_CanChuyen.this.radio_deb.isChecked() && Frag_CanChuyen.this.radio_de.isChecked() && Frag_CanChuyen.this.jsonKhongmax.getString("danDe").length() > 0) {
                        edt_XuatDan.setText(Frag_CanChuyen.this.TaoTinDe(Frag_CanChuyen.this.mContact.get(Frag_CanChuyen.this.mSpiner)));
                    } else if (Frag_CanChuyen.this.radio_lo.isChecked() && Frag_CanChuyen.this.jsonKhongmax.getString("danLo").length() > 0) {
                        edt_XuatDan.setText(Frag_CanChuyen.this.TaoTinLo(Frag_CanChuyen.this.mContact.get(Frag_CanChuyen.this.mSpiner)));
                    } else if (Frag_CanChuyen.this.radio_xi.isChecked() && (Frag_CanChuyen.this.jsonKhongmax.getInt("xien2") > 0 || Frag_CanChuyen.this.jsonKhongmax.getInt("xien3") > 0 || Frag_CanChuyen.this.jsonKhongmax.getInt("xien4") > 0)) {
                        edt_XuatDan.setText(Frag_CanChuyen.this.TaoTinXi(Frag_CanChuyen.this.mContact.get(Frag_CanChuyen.this.mSpiner)));
                    } else if (!Frag_CanChuyen.this.radio_bc.isChecked() || Frag_CanChuyen.this.jsonKhongmax.getInt("cang") <= 0) {
                        edt_XuatDan.setText(Chuyendi);
                    } else {
                        edt_XuatDan.setText(Frag_CanChuyen.this.TaoTinCang(Frag_CanChuyen.this.mContact.get(Frag_CanChuyen.this.mSpiner)));
                    }
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        btn_chuyendi.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_CanChuyen.22
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                String str;
                new MainActivity();
                String mDate = MainActivity.Get_date();
                int NganDai = 0;
                try {
                    NganDai = MainActivity.jSon_Setting.getInt("gioi_han_tin");
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                int dodai = 2000;
                if (NganDai == 1) {
                    dodai = PathInterpolatorCompat.MAX_NUM_POINTS;
                } else if (NganDai == 2) {
                    dodai = 155;
                } else if (NganDai == 3) {
                    dodai = 315;
                } else if (NganDai == 4) {
                    dodai = 475;
                } else if (NganDai == 5) {
                    dodai = 995;
                } else if (NganDai == 6) {
                    dodai = 2000;
                }
                String str2 = "";
                if (Frag_CanChuyen.this.mMobile.size() <= 0 || edt_XuatDan.getText().toString().length() <= 0 || Frag_CanChuyen.this.Dachuyen) {
                    str = str2;
                    if (edt_XuatDan.getText().toString().length() != 0) {
                        if (Frag_CanChuyen.this.Dachuyen) {
                            dialog.cancel();
                        } else {
                            Toast.makeText(Frag_CanChuyen.this.getActivity(), "Chưa có chủ để chuyển tin!", 1).show();
                        }
                    }
                } else {
                    Frag_CanChuyen.this.Dachuyen = true;
                    String TinNhan = edt_XuatDan.getText().toString().replaceAll("'", " ").trim();
                    edt_XuatDan.setText(str2);
                    dialog.dismiss();
                    if (TinNhan.trim().length() < dodai) {
                        String sql1 = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_CanChuyen.this.mMobile.get(Frag_CanChuyen.this.mSpiner) + "' AND type_kh = 2";
                        Cursor getSoTN = Frag_CanChuyen.this.db.GetData(sql1);
                        getSoTN.moveToFirst();
                        Frag_CanChuyen.this.Xulytin(getSoTN.getInt(0) + 1, TinNhan.replaceAll("'", " ").trim(), 1);
                        if (getSoTN != null) {
                            getSoTN.close();
                        }
                        str = str2;
                    } else {
                        String TienChiTiet = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_CanChuyen.this.mMobile.get(Frag_CanChuyen.this.mSpiner) + "' AND type_kh = 2";
                        Cursor getSoTN2 = Frag_CanChuyen.this.db.GetData(TienChiTiet);
                        getSoTN2.moveToFirst();
                        int SotinNhan = getSoTN2.getInt(0) + 1;
                        String DangGi = "";
                        String[] Chitiet = null;
                        if (TinNhan.substring(0, 3).indexOf("De") > -1) {
                            DangGi = "De:";
                            TinNhan = TinNhan.replaceAll("De:", str2);
                            Chitiet = TinNhan.split(" ");
                        } else if (TinNhan.substring(0, 3).indexOf("Lo") > -1) {
                            DangGi = "Lo:";
                            TinNhan = TinNhan.replaceAll("Lo:", str2);
                            Chitiet = TinNhan.split(" ");
                        } else if (TinNhan.substring(0, 5).indexOf("Cang") > -1) {
                            DangGi = "Cang:";
                            TinNhan = TinNhan.replaceAll("Cang:\n", str2);
                            Chitiet = TinNhan.split(" ");
                        } else if (TinNhan.substring(0, 3).indexOf("Xi") > -1) {
                            DangGi = "Xien:";
                            TinNhan = TinNhan.replaceAll("Xien:\n", str2).replaceAll("d:", "0").replaceAll("\n", " ");
                            Chitiet = TinNhan.split(" ");
                        }
                        String ndung = "";
                        if (DangGi != "Xien:") {
                            int k = 0;
                            while (k < Chitiet.length) {
                                String str3 = "x";
                                String DanChiTiet = Chitiet[k].substring(0, Chitiet[k].indexOf(str3));
                                String TienChiTiet2 = Chitiet[k].substring(Chitiet[k].indexOf(str3)).replaceAll(",", str2);
                                String[] str_so = DanChiTiet.split(",");
                                int j = 0;
                                while (true) {
                                    if (j < str_so.length) {
                                        String ndung2 = ndung.replaceAll(",x", str3);
                                        if (ndung2.length() != 0) {
                                            str3 = str3;
                                            if (ndung2.length() + TienChiTiet2.length() + TienChiTiet2.length() < dodai) {
                                                if (j >= str_so.length - 1) {
                                                    ndung = ndung2 + str_so[j] + "," + TienChiTiet2 + " ";
                                                    break;
                                                }
                                                ndung = ndung2 + str_so[j] + ",";
                                            } else {
                                                if (j > 0) {
                                                    ndung2 = ndung2 + TienChiTiet2;
                                                }
                                                Frag_CanChuyen.this.Xulytin(SotinNhan, ndung2, 1);
                                                SotinNhan++;
                                                if (j >= str_so.length - 1) {
                                                    ndung = DangGi + "\n" + str_so[j] + "," + TienChiTiet2 + " ";
                                                    break;
                                                }
                                                ndung = DangGi + "\n" + str_so[j] + ",";
                                            }
                                        } else {
                                            str3 = str3;
                                            ndung = str_so.length == 1 ? DangGi + "\n" + str_so[j] + "," + TienChiTiet2 + " " : DangGi + "\n" + str_so[j] + ",";
                                        }
                                        j++;
                                        str2 = str2;
                                    }
                                }
                                k++;
                                mDate = mDate;
                                NganDai = NganDai;
                                TienChiTiet = TienChiTiet;
                                TinNhan = TinNhan;
                                str2 = str2;
                            }
                            str = str2;
                            if (ndung.length() > 0) {
                                Frag_CanChuyen.this.Xulytin(SotinNhan, ndung, 1);
                            }
                        } else {
                            str = str2;
                            for (int k2 = 0; k2 < Chitiet.length; k2++) {
                                if (ndung.length() == 0) {
                                    ndung = DangGi + "\n" + Chitiet[k2] + " ";
                                } else if (ndung.length() + Chitiet[k2].length() < dodai) {
                                    ndung = ndung + Chitiet[k2] + " ";
                                } else {
                                    Frag_CanChuyen.this.Xulytin(SotinNhan, ndung, 1);
                                    SotinNhan++;
                                    ndung = DangGi + "\n" + Chitiet[k2] + " ";
                                }
                            }
                            int k3 = ndung.length();
                            if (k3 > 0) {
                                Frag_CanChuyen.this.Xulytin(SotinNhan, ndung, 1);
                            }
                        }
                        if (getSoTN2 != null) {
                            getSoTN2.close();
                        }
                    }
                    Toast.makeText(Frag_CanChuyen.this.getActivity(), "Đã chuyển tin!", 1).show();
                }
                Frag_CanChuyen.this.xemlv();
                Frag_CanChuyen.this.min = 0;
                Frag_CanChuyen.this.max = 100;
                Frag_CanChuyen.this.rangeSeekBar.setSelectedMinValue(0);
                Frag_CanChuyen.this.rangeSeekBar.setSelectedMaxValue(100);
                Frag_CanChuyen.this.edt_tien.setText(str);
            }
        });
        dialog.getWindow().setLayout(-1, -2);
        dialog.setCancelable(true);
        dialog.setTitle("Xem dạng:");
        dialog.show();
    }

    /* JADX WARN: Removed duplicated region for block: B:46:0x02fb  */
    /* JADX WARN: Removed duplicated region for block: B:48:0x0300  */
    /* JADX WARN: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void Xulytin(int r22, String r23, int r24) {
        /*
            Method dump skipped, instructions count: 788
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_CanChuyen.Xulytin(int, java.lang.String, int):void");
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        xem_RecycView();
        super.onResume();
    }

    public void xem_RecycView() {
        String Noi;
        new MainActivity();
        String mDate = MainActivity.Get_date();
        String str = null;
        this.mSo.clear();
        this.mTienNhan.clear();
        this.mTienOm.clear();
        this.mTienchuyen.clear();
        this.mTienTon.clear();
        this.mNhay.clear();
        String str2 = this.DangXuat;
        if (str2 == "(the_loai = 'deb' or the_loai = 'det')") {
            str = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_deB + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_deB as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So\n Where tbl_soctS.ngay_nhan='" + mDate + "' AND (tbl_soctS.the_loai='deb' OR tbl_soctS.the_loai='det') GROUP by so_om.So Order by " + this.sapxep;
        } else if (str2 == "the_loai = 'lo'") {
            str = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_Lo + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_Lo as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='" + mDate + "' AND tbl_soctS.the_loai='lo' \n GROUP by so_om.So Order by " + this.sapxep;
        } else if (str2 == "the_loai = 'dea'") {
            str = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeA + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeA as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='" + mDate + "' AND tbl_soctS.the_loai='dea' GROUP by so_chon Order by " + this.sapxep;
        } else if (str2 == "the_loai = 'dec'") {
            str = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeC + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeC as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='" + mDate + "' AND tbl_soctS.the_loai='dec' GROUP by so_chon Order by " + this.sapxep;
        } else if (str2 == "the_loai = 'ded'") {
            str = "Select tbl_soctS.So_chon\n, Sum((tbl_soctS.type_kh = 1) * (100-tbl_soctS.diem_khachgiu)*diem_quydoi/100) as diem\n, so_om.Om_DeD + sum(tbl_soctS.diem_dly_giu*tbl_soctS.diem_quydoi/100) as So_Om\n, Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) as chuyen\n, Sum((tbl_soctS.type_kh =1) * (100-tbl_soctS.diem_khachgiu-tbl_soctS.diem_dly_giu)*diem_quydoi/100) - Sum((tbl_soctS.type_kh =2) * tbl_soctS.diem_quydoi) - so_om.Om_DeD as ton\n, so_nhay  From so_om Left Join tbl_soctS On tbl_soctS.so_chon = so_om.So \n Where tbl_soctS.ngay_nhan='" + mDate + "' AND tbl_soctS.the_loai='ded' GROUP by so_chon Order by " + this.sapxep;
        } else if (str2 == "the_loai = 'xi'") {
            if (this.lay_x2 == "" && this.lay_x3 == "" && this.lay_x4 == "") {
                Noi = "";
            } else {
                String Noi2 = " And (" + this.lay_x2 + this.lay_x3 + this.lay_x4 + ")";
                Noi = Noi2.replaceAll("\\(OR", "(");
            }
            Cursor c1 = this.db.GetData("Select * From So_om WHERE ID = 1");
            c1.moveToFirst();
            str = "SELECT so_chon, sum((type_kh =1)*(100-diem_khachgiu)*diem_quydoi)/100 AS diem, ((length(so_chon) = 5) * " + c1.getString(7) + " +(length(so_chon) = 8) * " + c1.getString(8) + " +(length(so_chon) = 11) * " + c1.getString(9) + " + sum(diem_dly_giu*diem_quydoi/100)) AS Om, SUm((type_kh =2)*diem) as chuyen , SUm((type_kh =1)*(100-diem_khachgiu-diem_dly_giu)*diem_quydoi/100)-SUm((type_kh =2)*diem) -  ((length(so_chon) = 5) * " + c1.getString(7) + " +(length(so_chon) = 8) * " + c1.getString(8) + " +(length(so_chon) = 11) * " + c1.getString(9) + ") AS ton, so_nhay   From tbl_soctS Where ngay_nhan='" + mDate + "' AND the_loai='xi'" + Noi + "  GROUP by so_chon Order by ton DESC, diem DESC";
            if (c1 != null && !c1.isClosed()) {
                c1.close();
            }
        } else if (str2 == "the_loai = 'bc'") {
            Cursor c12 = this.db.GetData("Select * From So_om WHERE ID = 1");
            c12.moveToFirst();
            if (c12.getInt(10) == 1) {
                this.db.QueryData("Update so_om set om_bc=0 WHERE id = 1");
                c12 = this.db.GetData("Select * From So_om WHERE ID = 1");
                c12.moveToFirst();
            }
            str = "SELECT so_chon, sum((type_kh = 1)*(100-diem_khachgiu)*diem_quydoi/100) AS diem, " + c12.getString(10) + " + sum(diem_dly_giu*diem_quydoi)/100 AS Om, SUm((type_kh = 2)*diem) as Chuyen, sum((type_kh =1)*(100-diem_khachgiu-diem_dly_giu)*diem_quydoi/100) - sum((type_kh =2)*diem) -" + c12.getString(10) + " AS ton, so_nhay   From tbl_soctS Where ngay_nhan='" + mDate + "' AND the_loai='bc' GROUP by so_chon Order by ton DESC, diem DESC";
            if (c12 != null && !c12.isClosed()) {
                c12.close();
            }
        } else if (str2 == "the_loai = 'xn'") {
            str = "SELECT so_chon, sum((type_kh =1)*(diem_quydoi)) AS diem, sum(tbl_soctS.diem_dly_giu) AS Om, SUm((type_kh =2)*diem) as chuyen , SUm((type_kh =1)*diem_ton-(type_kh =2)*diem_ton) AS ton, so_nhay   From tbl_soctS Where ngay_nhan='" + mDate + "' AND the_loai='xn' GROUP by so_chon Order by ton DESC, diem DESC";
        }
        Cursor cursor = this.db.GetData(str);
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                this.mSo.add(cursor.getString(0));
                this.mTienNhan.add(decimalFormat.format(cursor.getInt(1)));
                this.mTienOm.add(decimalFormat.format(cursor.getInt(2)));
                this.mTienchuyen.add(decimalFormat.format(cursor.getInt(3)));
                this.mTienTon.add(decimalFormat.format(cursor.getInt(4)));
                this.mNhay.add(Integer.valueOf(cursor.getInt(5)));
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        if (getActivity() != null) {
            this.no_rp_number.setAdapter((ListAdapter) new So_OmAdapter(getActivity(), R.layout.frag_canchuyen_lv, this.mSo));
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class So_OmAdapter extends ArrayAdapter {
        public So_OmAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView tview1;
            TextView tview2;
            TextView tview4;
            TextView tview5;
            TextView tview7;
            TextView tview8;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
            ViewHolder holder = new ViewHolder();
            if (view == null) {
                view = inflater.inflate(R.layout.frag_canchuyen_lv, (ViewGroup) null);
                holder.tview5 = (TextView) view.findViewById(R.id.Tv_so);
                holder.tview7 = (TextView) view.findViewById(R.id.tv_diemNhan);
                holder.tview8 = (TextView) view.findViewById(R.id.tv_diemOm);
                holder.tview1 = (TextView) view.findViewById(R.id.tv_diemChuyen);
                holder.tview4 = (TextView) view.findViewById(R.id.tv_diemTon);
                holder.tview2 = (TextView) view.findViewById(R.id.stt);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            if (Frag_CanChuyen.this.mNhay.get(position).intValue() > 0) {
                holder.tview5.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview7.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview8.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview1.setTextColor(SupportMenu.CATEGORY_MASK);
                holder.tview4.setTextColor(SupportMenu.CATEGORY_MASK);
                if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 1) {
                    TextView textView = holder.tview5;
                    textView.setText(Frag_CanChuyen.this.mSo.get(position) + "*");
                } else if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 2) {
                    TextView textView2 = holder.tview5;
                    textView2.setText(Frag_CanChuyen.this.mSo.get(position) + "**");
                } else if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 3) {
                    TextView textView3 = holder.tview5;
                    textView3.setText(Frag_CanChuyen.this.mSo.get(position) + "***");
                } else if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 4) {
                    TextView textView4 = holder.tview5;
                    textView4.setText(Frag_CanChuyen.this.mSo.get(position) + "****");
                } else if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 5) {
                    TextView textView5 = holder.tview5;
                    textView5.setText(Frag_CanChuyen.this.mSo.get(position) + "*****");
                } else if (Frag_CanChuyen.this.mNhay.get(position).intValue() == 6) {
                    TextView textView6 = holder.tview5;
                    textView6.setText(Frag_CanChuyen.this.mSo.get(position) + "******");
                }
                holder.tview7.setText(Frag_CanChuyen.this.mTienNhan.get(position));
                holder.tview8.setText(Frag_CanChuyen.this.mTienOm.get(position));
                holder.tview1.setText(Frag_CanChuyen.this.mTienchuyen.get(position));
                holder.tview4.setText(Frag_CanChuyen.this.mTienTon.get(position));
                TextView textView7 = holder.tview2;
                textView7.setText((position + 1) + "");
            } else {
                holder.tview5.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview7.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview8.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview1.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview4.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                holder.tview5.setText(Frag_CanChuyen.this.mSo.get(position));
                holder.tview7.setText(Frag_CanChuyen.this.mTienNhan.get(position));
                holder.tview8.setText(Frag_CanChuyen.this.mTienOm.get(position));
                holder.tview1.setText(Frag_CanChuyen.this.mTienchuyen.get(position));
                holder.tview4.setText(Frag_CanChuyen.this.mTienTon.get(position));
                TextView textView8 = holder.tview2;
                textView8.setText((position + 1) + "");
            }
            return view;
        }
    }
}
