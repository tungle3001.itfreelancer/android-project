package tamhoang.ldpro4.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Activity.Activity_khach;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.NotificationReader;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_No_new extends Fragment {
    TextView bc_Chuyen;
    TextView bc_ChuyenAn;
    TextView bc_Nhan;
    TextView bc_NhanAn;
    TextView bca_Chuyen;
    TextView bca_ChuyenAn;
    TextView bca_Nhan;
    TextView bca_NhanAn;
    Button btn_nt;
    JSONObject caidat_tg;
    int currentIndex;
    Database db;
    TextView dea_Chuyen;
    TextView dea_ChuyenAn;
    TextView dea_Nhan;
    TextView dea_NhanAn;
    TextView deb_Chuyen;
    TextView deb_ChuyenAn;
    TextView deb_Nhan;
    TextView deb_NhanAn;
    TextView dec_Chuyen;
    TextView dec_ChuyenAn;
    TextView dec_Nhan;
    TextView dec_NhanAn;
    TextView ded_Chuyen;
    TextView ded_ChuyenAn;
    TextView ded_Nhan;
    TextView ded_NhanAn;
    TextView det_Chuyen;
    TextView det_ChuyenAn;
    TextView det_Nhan;
    TextView det_NhanAn;
    Handler handler;
    JSONObject json;
    List<JSONObject> jsonKhachHang;
    LinearLayout li_bca;
    LinearLayout li_dea;
    LinearLayout li_dec;
    LinearLayout li_ded;
    LinearLayout li_det;
    LinearLayout li_loa;
    LinearLayout li_xia2;
    LinearLayout li_xn;
    TextView lo_Chuyen;
    TextView lo_ChuyenAn;
    TextView lo_Nhan;
    TextView lo_NhanAn;
    TextView loa_Chuyen;
    TextView loa_ChuyenAn;
    TextView loa_Nhan;
    TextView loa_NhanAn;
    ListView lv_baocaoKhach;
    LayoutInflater mInflate;
    int position;
    private ProgressBar progressBar;
    TextView tv_TongGiu;
    TextView tv_TongTienChuyen;
    TextView tv_TongTienNhan;
    View v;
    TextView xi2_Chuyen;
    TextView xi2_ChuyenAn;
    TextView xi2_Nhan;
    TextView xi2_NhanAn;
    TextView xia2_Chuyen;
    TextView xia2_ChuyenAn;
    TextView xia2_Nhan;
    TextView xia2_NhanAn;
    TextView xn_Chuyen;
    TextView xn_ChuyenAn;
    TextView xn_Nhan;
    TextView xn_NhanAn;
    private List<String> mTenKH = new ArrayList();
    private List<String> mSDT = new ArrayList();
    boolean Running = true;
    boolean isSuccess = false;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.3
        @Override // java.lang.Runnable
        public void run() {
            new MainActivity();
            if (MainActivity.sms) {
                Frag_No_new.this.lv_baoCao();
                MainActivity.sms = false;
            }
            Frag_No_new.this.handler.postDelayed(this, 1000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_norp1, container, false);
        this.db = new Database(getActivity());
        this.mInflate = inflater;
        init();
        this.progressBar = (ProgressBar) this.v.findViewById(R.id.progressBar);
        this.btn_nt = (Button) this.v.findViewById(R.id.btn_nt);
        this.lv_baocaoKhach.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.1
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Frag_No_new.this.position = i;
                Frag_No_new frag_No_new = Frag_No_new.this;
                frag_No_new.Dialog((String) frag_No_new.mTenKH.get(i));
                return false;
            }
        });
        this.btn_nt.setOnClickListener(new AnonymousClass2());
        if (!Congthuc.CheckTime("18:30")) {
            Handler handler = new Handler();
            this.handler = handler;
            handler.postDelayed(this.runnable, 1000L);
        }
        lv_baoCao();
        return this.v;
    }

    /* renamed from: tamhoang.ldpro4.Fragment.Frag_No_new$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    class AnonymousClass2 implements View.OnClickListener {
        AnonymousClass2() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (Frag_No_new.this.jsonKhachHang.size() >= 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Frag_No_new.this.getActivity());
                builder.setMessage("Bạn có muốn nhắn tin chốt tiền tất cả không?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.2.2
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Frag_No_new.this.progressBar.setVisibility(0);
                        Frag_No_new.this.getActivity().getWindow().setFlags(16, 16);
                        for (int i = 0; i < Frag_No_new.this.jsonKhachHang.size(); i++) {
                            Frag_No_new.this.currentIndex = i;
                            Database database = Frag_No_new.this.db;
                            Cursor cursor2 = database.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)) + "'");
                            if (cursor2 != null && cursor2.getCount() > 0) {
                                cursor2.moveToFirst();
                                if (cursor2.getString(2).indexOf("sms") > -1) {
                                    try {
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            Frag_No_new.this.db.SendSMS((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.currentIndex), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        } else {
                                            Frag_No_new.this.db.SendSMS((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.currentIndex), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        }
                                        Frag_No_new.this.isSuccess = true;
                                    } catch (JSONException e) {
                                        Frag_No_new.this.isSuccess = false;
                                        e.printStackTrace();
                                    }
                                } else if (cursor2.getString(2).indexOf("TL") > -1) {
                                    try {
                                        new MainActivity();
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.currentIndex)), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        } else {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.currentIndex)), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        }
                                        Frag_No_new.this.isSuccess = true;
                                        try {
                                            Thread.sleep(1000L);
                                        } catch (InterruptedException e2) {
                                            e2.printStackTrace();
                                        }
                                    } catch (JSONException e3) {
                                        Frag_No_new.this.isSuccess = false;
                                        e3.printStackTrace();
                                    }
                                } else if (MainActivity.arr_TenKH.indexOf(cursor2.getString(1)) > -1) {
                                    NotificationReader notificationReader = new NotificationReader();
                                    try {
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        } else {
                                            notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.currentIndex)));
                                        }
                                    } catch (JSONException e4) {
                                        Frag_No_new.this.isSuccess = false;
                                        e4.printStackTrace();
                                    }
                                    Frag_No_new.this.isSuccess = true;
                                } else {
                                    Frag_No_new.this.isSuccess = false;
                                    Toast.makeText(Frag_No_new.this.getActivity(), "Không có người này trong Chatbox", 1).show();
                                }
                            }
                        }
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.2.2.1
                            @Override // java.lang.Runnable
                            public void run() {
                                if (Frag_No_new.this.isSuccess) {
                                    Toast.makeText(Frag_No_new.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                                }
                                Frag_No_new.this.progressBar.setVisibility(8);
                                Frag_No_new.this.getActivity().getWindow().clearFlags(16);
                            }
                        }, 2000L);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.2.1
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onStop() {
        this.Running = false;
        super.onStop();
        try {
            this.handler.removeCallbacks(this.runnable);
        } catch (Exception e) {
        }
    }

    public void Dialog(final String tenkh) {
        SeekBar xi_dly;
        SeekBar bc_dly;
        final TextView pt_giu_bc_dly;
        final TextView pt_giu_xi_dly;
        SeekBar xi_khach;
        SeekBar lo_khach;
        final TextView pt_giu_lo_khach;
        SeekBar de_khach;
        final TextView pt_giu_de_khach;
        final TextView pt_giu_xi_khach;
        SeekBar bc_khach;
        final TextView pt_giu_bc_khach;
        JSONException e;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.frag_no_menu);
        new MainActivity();
        final String mDate = MainActivity.Get_date();
        final ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService("clipboard");
        Database database = this.db;
        Cursor cursor = database.GetData("Select * From tbl_kh_new WHERE ten_kh ='" + tenkh + "'");
        cursor.moveToFirst();
        Button Baocaochitiet = (Button) dialog.findViewById(R.id.Baocaochitiet);
        Button tinhlaitien = (Button) dialog.findViewById(R.id.tinhlaitien);
        Button nhanchottien = (Button) dialog.findViewById(R.id.nhanchottien);
        Button copytinchitiet = (Button) dialog.findViewById(R.id.copytinchitiet);
        Button copytinchotien = (Button) dialog.findViewById(R.id.copytinchotien);
        Button xoadulieu = (Button) dialog.findViewById(R.id.xoadulieu);
        final Switch switch1 = (Switch) dialog.findViewById(R.id.switch1);
        final LinearLayout ln2 = (LinearLayout) dialog.findViewById(R.id.ln2);
        SeekBar de_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuDekhach);
        SeekBar lo_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuLokhach);
        SeekBar xi_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuXikhach);
        SeekBar bc_khach2 = (SeekBar) dialog.findViewById(R.id.seek_Giu3ckhach);
        SeekBar de_dly = (SeekBar) dialog.findViewById(R.id.seek_GiuDedly);
        SeekBar lo_dly = (SeekBar) dialog.findViewById(R.id.seek_GiuLodly);
        SeekBar xi_dly2 = (SeekBar) dialog.findViewById(R.id.seek_GiuXidly);
        SeekBar bc_dly2 = (SeekBar) dialog.findViewById(R.id.seek_Giu3cdly);
        TextView pt_giu_de_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_de_khach);
        TextView pt_giu_lo_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_lo_khach);
        TextView pt_giu_xi_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_xi_khach);
        TextView pt_giu_bc_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_bc_khach);
        final TextView pt_giu_de_dly = (TextView) dialog.findViewById(R.id.pt_giu_de_dly);
        final TextView pt_giu_lo_dly = (TextView) dialog.findViewById(R.id.pt_giu_lo_dly);
        TextView pt_giu_xi_dly2 = (TextView) dialog.findViewById(R.id.pt_giu_xi_dly);
        TextView pt_giu_bc_dly2 = (TextView) dialog.findViewById(R.id.pt_giu_bc_dly);
        try {
            bc_dly = bc_dly2;
            xi_dly = xi_dly2;
            try {
                JSONObject jSONObject = new JSONObject(cursor.getString(5));
                this.json = jSONObject;
                this.caidat_tg = jSONObject.getJSONObject("caidat_tg");
                pt_giu_de_dly.setText(this.caidat_tg.getInt("dlgiu_de") + "%");
                pt_giu_lo_dly.setText(this.caidat_tg.getInt("dlgiu_lo") + "%");
                pt_giu_xi_dly2.setText(this.caidat_tg.getInt("dlgiu_xi") + "%");
                pt_giu_bc_dly2.setText(this.caidat_tg.getInt("dlgiu_bc") + "%");
                de_dly.setProgress(this.caidat_tg.getInt("dlgiu_de") / 5);
                lo_dly.setProgress(this.caidat_tg.getInt("dlgiu_lo") / 5);
                try {
                    xi_dly.setProgress(this.caidat_tg.getInt("dlgiu_xi") / 5);
                    try {
                        bc_dly.setProgress(this.caidat_tg.getInt("dlgiu_bc") / 5);
                        pt_giu_de_khach = pt_giu_de_khach2;
                        try {
                            pt_giu_de_khach.setText(this.caidat_tg.getInt("khgiu_de") + "%");
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            try {
                                pt_giu_lo_khach.setText(this.caidat_tg.getInt("khgiu_lo") + "%");
                                StringBuilder sb = new StringBuilder();
                                bc_dly = bc_dly;
                                try {
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    try {
                                        sb.append(this.caidat_tg.getInt("khgiu_xi"));
                                        sb.append("%");
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        try {
                                            pt_giu_xi_khach.setText(sb.toString());
                                            StringBuilder sb2 = new StringBuilder();
                                            xi_dly = xi_dly;
                                            try {
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                try {
                                                    sb2.append(this.caidat_tg.getInt("khgiu_bc"));
                                                    sb2.append("%");
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                    try {
                                                        pt_giu_bc_khach.setText(sb2.toString());
                                                        de_khach = de_khach2;
                                                        try {
                                                            de_khach.setProgress(this.caidat_tg.getInt("khgiu_de") / 5);
                                                            lo_khach = lo_khach2;
                                                            try {
                                                                lo_khach.setProgress(this.caidat_tg.getInt("khgiu_lo") / 5);
                                                                xi_khach = xi_khach2;
                                                                try {
                                                                    xi_khach.setProgress(this.caidat_tg.getInt("khgiu_xi") / 5);
                                                                    bc_khach = bc_khach2;
                                                                    try {
                                                                        bc_khach.setProgress(this.caidat_tg.getInt("khgiu_bc") / 5);
                                                                        cursor.close();
                                                                    } catch (JSONException e2) {
                                                                        e = e2;
                                                                        e.printStackTrace();
                                                                    } catch (Exception e3) {
                                                                    }
                                                                } catch (JSONException e4) {
                                                                    e = e4;
                                                                    bc_khach = bc_khach2;
                                                                } catch (Exception e5) {
                                                                    bc_khach = bc_khach2;
                                                                }
                                                            } catch (JSONException e6) {
                                                                e = e6;
                                                                bc_khach = bc_khach2;
                                                                xi_khach = xi_khach2;
                                                            } catch (Exception e7) {
                                                                bc_khach = bc_khach2;
                                                                xi_khach = xi_khach2;
                                                            }
                                                        } catch (JSONException e8) {
                                                            e = e8;
                                                            bc_khach = bc_khach2;
                                                            xi_khach = xi_khach2;
                                                            lo_khach = lo_khach2;
                                                        } catch (Exception e9) {
                                                            bc_khach = bc_khach2;
                                                            xi_khach = xi_khach2;
                                                            lo_khach = lo_khach2;
                                                        }
                                                    } catch (JSONException e10) {
                                                        e = e10;
                                                        bc_khach = bc_khach2;
                                                        xi_khach = xi_khach2;
                                                        lo_khach = lo_khach2;
                                                        de_khach = de_khach2;
                                                    } catch (Exception e11) {
                                                        bc_khach = bc_khach2;
                                                        xi_khach = xi_khach2;
                                                        lo_khach = lo_khach2;
                                                        de_khach = de_khach2;
                                                    }
                                                } catch (JSONException e12) {
                                                    e = e12;
                                                    bc_khach = bc_khach2;
                                                    xi_khach = xi_khach2;
                                                    lo_khach = lo_khach2;
                                                    de_khach = de_khach2;
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                } catch (Exception e13) {
                                                    bc_khach = bc_khach2;
                                                    xi_khach = xi_khach2;
                                                    lo_khach = lo_khach2;
                                                    de_khach = de_khach2;
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                }
                                            } catch (JSONException e14) {
                                                e = e14;
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                bc_khach = bc_khach2;
                                                xi_khach = xi_khach2;
                                                lo_khach = lo_khach2;
                                                de_khach = de_khach2;
                                                pt_giu_bc_khach = pt_giu_bc_khach2;
                                            } catch (Exception e15) {
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                bc_khach = bc_khach2;
                                                xi_khach = xi_khach2;
                                                lo_khach = lo_khach2;
                                                de_khach = de_khach2;
                                                pt_giu_bc_khach = pt_giu_bc_khach2;
                                            }
                                        } catch (JSONException e16) {
                                            e = e16;
                                            xi_dly = xi_dly;
                                            pt_giu_xi_dly = pt_giu_xi_dly2;
                                            bc_khach = bc_khach2;
                                            xi_khach = xi_khach2;
                                            lo_khach = lo_khach2;
                                            de_khach = de_khach2;
                                            pt_giu_bc_khach = pt_giu_bc_khach2;
                                        } catch (Exception e17) {
                                            xi_dly = xi_dly;
                                            pt_giu_xi_dly = pt_giu_xi_dly2;
                                            bc_khach = bc_khach2;
                                            xi_khach = xi_khach2;
                                            lo_khach = lo_khach2;
                                            de_khach = de_khach2;
                                            pt_giu_bc_khach = pt_giu_bc_khach2;
                                        }
                                    } catch (JSONException e18) {
                                        e = e18;
                                        xi_dly = xi_dly;
                                        pt_giu_xi_dly = pt_giu_xi_dly2;
                                        bc_khach = bc_khach2;
                                        xi_khach = xi_khach2;
                                        lo_khach = lo_khach2;
                                        de_khach = de_khach2;
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        pt_giu_bc_khach = pt_giu_bc_khach2;
                                    } catch (Exception e19) {
                                        xi_dly = xi_dly;
                                        pt_giu_xi_dly = pt_giu_xi_dly2;
                                        bc_khach = bc_khach2;
                                        xi_khach = xi_khach2;
                                        lo_khach = lo_khach2;
                                        de_khach = de_khach2;
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        pt_giu_bc_khach = pt_giu_bc_khach2;
                                    }
                                } catch (JSONException e20) {
                                    e = e20;
                                    xi_dly = xi_dly;
                                    pt_giu_xi_dly = pt_giu_xi_dly2;
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    bc_khach = bc_khach2;
                                    xi_khach = xi_khach2;
                                    lo_khach = lo_khach2;
                                    de_khach = de_khach2;
                                    pt_giu_xi_khach = pt_giu_xi_khach2;
                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                } catch (Exception e21) {
                                    xi_dly = xi_dly;
                                    pt_giu_xi_dly = pt_giu_xi_dly2;
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    bc_khach = bc_khach2;
                                    xi_khach = xi_khach2;
                                    lo_khach = lo_khach2;
                                    de_khach = de_khach2;
                                    pt_giu_xi_khach = pt_giu_xi_khach2;
                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                }
                            } catch (JSONException e22) {
                                e = e22;
                                xi_dly = xi_dly;
                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                bc_dly = bc_dly;
                                pt_giu_bc_dly = pt_giu_bc_dly2;
                                bc_khach = bc_khach2;
                                xi_khach = xi_khach2;
                                lo_khach = lo_khach2;
                                de_khach = de_khach2;
                                pt_giu_xi_khach = pt_giu_xi_khach2;
                                pt_giu_bc_khach = pt_giu_bc_khach2;
                            } catch (Exception e23) {
                                xi_dly = xi_dly;
                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                bc_dly = bc_dly;
                                pt_giu_bc_dly = pt_giu_bc_dly2;
                                bc_khach = bc_khach2;
                                xi_khach = xi_khach2;
                                lo_khach = lo_khach2;
                                de_khach = de_khach2;
                                pt_giu_xi_khach = pt_giu_xi_khach2;
                                pt_giu_bc_khach = pt_giu_bc_khach2;
                            }
                        } catch (JSONException e24) {
                            e = e24;
                            xi_dly = xi_dly;
                            pt_giu_xi_dly = pt_giu_xi_dly2;
                            bc_dly = bc_dly;
                            pt_giu_bc_dly = pt_giu_bc_dly2;
                            bc_khach = bc_khach2;
                            xi_khach = xi_khach2;
                            lo_khach = lo_khach2;
                            de_khach = de_khach2;
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            pt_giu_xi_khach = pt_giu_xi_khach2;
                            pt_giu_bc_khach = pt_giu_bc_khach2;
                        } catch (Exception e25) {
                            xi_dly = xi_dly;
                            pt_giu_xi_dly = pt_giu_xi_dly2;
                            bc_dly = bc_dly;
                            pt_giu_bc_dly = pt_giu_bc_dly2;
                            bc_khach = bc_khach2;
                            xi_khach = xi_khach2;
                            lo_khach = lo_khach2;
                            de_khach = de_khach2;
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            pt_giu_xi_khach = pt_giu_xi_khach2;
                            pt_giu_bc_khach = pt_giu_bc_khach2;
                        }
                    } catch (JSONException e26) {
                        e = e26;
                        xi_dly = xi_dly;
                        pt_giu_xi_dly = pt_giu_xi_dly2;
                        bc_dly = bc_dly;
                        pt_giu_bc_dly = pt_giu_bc_dly2;
                        bc_khach = bc_khach2;
                        xi_khach = xi_khach2;
                        lo_khach = lo_khach2;
                        de_khach = de_khach2;
                        pt_giu_lo_khach = pt_giu_lo_khach2;
                        pt_giu_xi_khach = pt_giu_xi_khach2;
                        pt_giu_bc_khach = pt_giu_bc_khach2;
                        pt_giu_de_khach = pt_giu_de_khach2;
                    } catch (Exception e27) {
                        xi_dly = xi_dly;
                        pt_giu_xi_dly = pt_giu_xi_dly2;
                        bc_dly = bc_dly;
                        pt_giu_bc_dly = pt_giu_bc_dly2;
                        bc_khach = bc_khach2;
                        xi_khach = xi_khach2;
                        lo_khach = lo_khach2;
                        de_khach = de_khach2;
                        pt_giu_lo_khach = pt_giu_lo_khach2;
                        pt_giu_xi_khach = pt_giu_xi_khach2;
                        pt_giu_bc_khach = pt_giu_bc_khach2;
                        pt_giu_de_khach = pt_giu_de_khach2;
                    }
                } catch (JSONException e28) {
                    e = e28;
                    xi_dly = xi_dly;
                    pt_giu_xi_dly = pt_giu_xi_dly2;
                    pt_giu_bc_dly = pt_giu_bc_dly2;
                    bc_khach = bc_khach2;
                    xi_khach = xi_khach2;
                    lo_khach = lo_khach2;
                    de_khach = de_khach2;
                    pt_giu_lo_khach = pt_giu_lo_khach2;
                    pt_giu_xi_khach = pt_giu_xi_khach2;
                    pt_giu_bc_khach = pt_giu_bc_khach2;
                    pt_giu_de_khach = pt_giu_de_khach2;
                } catch (Exception e29) {
                    xi_dly = xi_dly;
                    pt_giu_xi_dly = pt_giu_xi_dly2;
                    pt_giu_bc_dly = pt_giu_bc_dly2;
                    bc_khach = bc_khach2;
                    xi_khach = xi_khach2;
                    lo_khach = lo_khach2;
                    de_khach = de_khach2;
                    pt_giu_lo_khach = pt_giu_lo_khach2;
                    pt_giu_xi_khach = pt_giu_xi_khach2;
                    pt_giu_bc_khach = pt_giu_bc_khach2;
                    pt_giu_de_khach = pt_giu_de_khach2;
                }
            } catch (JSONException e30) {
                e = e30;
                pt_giu_xi_dly = pt_giu_xi_dly2;
                pt_giu_bc_dly = pt_giu_bc_dly2;
                bc_khach = bc_khach2;
                xi_khach = xi_khach2;
                lo_khach = lo_khach2;
                de_khach = de_khach2;
                pt_giu_lo_khach = pt_giu_lo_khach2;
                pt_giu_xi_khach = pt_giu_xi_khach2;
                pt_giu_bc_khach = pt_giu_bc_khach2;
                pt_giu_de_khach = pt_giu_de_khach2;
            } catch (Exception e31) {
                pt_giu_xi_dly = pt_giu_xi_dly2;
                pt_giu_bc_dly = pt_giu_bc_dly2;
                bc_khach = bc_khach2;
                xi_khach = xi_khach2;
                lo_khach = lo_khach2;
                de_khach = de_khach2;
                pt_giu_lo_khach = pt_giu_lo_khach2;
                pt_giu_xi_khach = pt_giu_xi_khach2;
                pt_giu_bc_khach = pt_giu_bc_khach2;
                pt_giu_de_khach = pt_giu_de_khach2;
            }
        } catch (JSONException e32) {
            e = e32;
            xi_dly = xi_dly2;
            pt_giu_xi_dly = pt_giu_xi_dly2;
            pt_giu_bc_dly = pt_giu_bc_dly2;
            bc_dly = bc_dly2;
            bc_khach = bc_khach2;
            xi_khach = xi_khach2;
            lo_khach = lo_khach2;
            de_khach = de_khach2;
            pt_giu_lo_khach = pt_giu_lo_khach2;
            pt_giu_xi_khach = pt_giu_xi_khach2;
            pt_giu_bc_khach = pt_giu_bc_khach2;
            pt_giu_de_khach = pt_giu_de_khach2;
        } catch (Exception e33) {
            xi_dly = xi_dly2;
            pt_giu_xi_dly = pt_giu_xi_dly2;
            pt_giu_bc_dly = pt_giu_bc_dly2;
            bc_dly = bc_dly2;
            bc_khach = bc_khach2;
            xi_khach = xi_khach2;
            lo_khach = lo_khach2;
            de_khach = de_khach2;
            pt_giu_lo_khach = pt_giu_lo_khach2;
            pt_giu_xi_khach = pt_giu_xi_khach2;
            pt_giu_bc_khach = pt_giu_bc_khach2;
            pt_giu_de_khach = pt_giu_de_khach2;
        }
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switch1.isChecked()) {
                    ln2.setVisibility(0);
                } else {
                    ln2.setVisibility(8);
                }
            }
        });
        Baocaochitiet.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.5
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Intent intent = new Intent(Frag_No_new.this.getActivity(), Activity_khach.class);
                intent.putExtra("tenKH", tenkh);
                Frag_No_new.this.startActivity(intent);
                dialog.cancel();
            }
        });
        tinhlaitien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.6
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                dialog.cancel();
            }
        });
        nhanchottien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.7
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Database database2 = Frag_No_new.this.db;
                Cursor cursor2 = database2.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)) + "'");
                if (cursor2 != null && cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    if (cursor2.getString(2).indexOf("sms") > -1) {
                        try {
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                Frag_No_new.this.db.SendSMS((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.position), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                            } else {
                                Frag_No_new.this.db.SendSMS((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.position), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                            }
                            Toast.makeText(Frag_No_new.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                        } catch (JSONException e34) {
                            e34.printStackTrace();
                        }
                    } else if (cursor2.getString(2).indexOf("TL") > -1) {
                        try {
                            final MainActivity mainActivity = new MainActivity();
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.7.1
                                    @Override // java.lang.Runnable
                                    public void run() {
                                        try {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.position)), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                                        } catch (JSONException e35) {
                                            e35.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.7.2
                                    @Override // java.lang.Runnable
                                    public void run() {
                                        try {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_new.this.mSDT.get(Frag_No_new.this.position)), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                                        } catch (JSONException e35) {
                                            e35.printStackTrace();
                                        }
                                    }
                                });
                            }
                            Toast.makeText(Frag_No_new.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                        } catch (JSONException e35) {
                            e35.printStackTrace();
                        }
                    } else if (MainActivity.arr_TenKH.indexOf(cursor2.getString(1)) > -1) {
                        NotificationReader notificationReader = new NotificationReader();
                        try {
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                            } else {
                                notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                            }
                        } catch (JSONException e36) {
                            e36.printStackTrace();
                        }
                    } else {
                        Toast.makeText(Frag_No_new.this.getActivity(), "Không có người này trong Chatbox", 1).show();
                    }
                }
                dialog.cancel();
            }
        });
        copytinchitiet.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.8
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ClipData clip = ClipData.newPlainText("Tin chốt:", Frag_No_new.this.db.Tin_Chottien_CT((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                clipboard.setPrimaryClip(clip);
                Toast.makeText(Frag_No_new.this.getActivity(), "Đã copy vào bộ nhớ tạm!", 1).show();
                dialog.cancel();
            }
        });
        copytinchotien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.9
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Database database2 = Frag_No_new.this.db;
                Cursor cursor2 = database2.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)) + "'");
                cursor2.moveToFirst();
                try {
                    ClipData clip = MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0 ? ClipData.newPlainText("Tin chốt:", Frag_No_new.this.db.Tin_Chottien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position))) : ClipData.newPlainText("Tin chốt:", Frag_No_new.this.db.Tin_Chottien_xien((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)));
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(Frag_No_new.this.getActivity(), "Đã copy vào bộ nhớ tạm!", 1).show();
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
                dialog.cancel();
            }
        });
        xoadulieu.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.10
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                AlertDialog.Builder bui = new AlertDialog.Builder(Frag_No_new.this.getActivity());
                bui.setTitle("Xoá dữ liệu của KH này?");
                bui.setPositiveButton("YES", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.10.1
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface mdialog, int which) {
                        String str = "DELETE FROM tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND ten_kh = '" + ((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)) + "'";
                        Frag_No_new.this.db.QueryData(str);
                        String str2 = "DELETE FROM tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND ten_kh = '" + ((String) Frag_No_new.this.mTenKH.get(Frag_No_new.this.position)) + "'";
                        Frag_No_new.this.db.QueryData(str2);
                        Frag_No_new.this.lv_baoCao();
                        dialog.cancel();
                        Toast.makeText(Frag_No_new.this.getActivity(), "Đã xoá", 1).show();
                    }
                });
                bui.setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.10.2
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface mdialog, int which) {
                        mdialog.cancel();
                    }
                });
                bui.create().show();
            }
        });
        de_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.11
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_de_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("khgiu_de", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        lo_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.12
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_lo_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("khgiu_lo", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        xi_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.13
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_xi_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("khgiu_xi", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        bc_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.14
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_bc_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("khgiu_bc", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        de_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.15
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_de_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("dlgiu_de", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        lo_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.16
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_lo_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("dlgiu_lo", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        xi_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.17
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_xi_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("dlgiu_xi", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        bc_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_new.18
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_bc_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_new.this.caidat_tg.put("dlgiu_bc", this.max);
                    Database database2 = Frag_No_new.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_new.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_new.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        dialog.setCancelable(true);
        dialog.setTitle("Xem dạng:");
        dialog.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void TinhlaitienKhachnay(String mDate, String tenkh) {
        Database database = this.db;
        database.QueryData("Delete From tbl_soctS WHERE  ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.mTenKH.get(this.position) + "'");
        Database database2 = this.db;
        Cursor cur = database2.GetData("Select * FROM tbl_tinnhanS WHERE  ngay_nhan = '" + mDate + "' AND phat_hien_loi = 'ok' AND ten_kh = '" + this.mTenKH.get(this.position) + "'");
        while (cur.moveToNext()) {
            String str = cur.getString(10);
            String str2 = str.replaceAll("\\*", "");
            Database database3 = this.db;
            database3.QueryData("Update tbl_tinnhanS set nd_phantich = '" + str2 + "' WHERE id = " + cur.getInt(0));
            this.db.NhapSoChiTiet(cur.getInt(0));
        }
        Tinhtien();
        lv_baoCao();
        if (!cur.isClosed()) {
            cur.close();
        }
    }

    private void Tinhtien() {
        new MainActivity();
        String mDate = MainActivity.Get_date();
        Database database = this.db;
        Cursor cursor = database.GetData("Select * From Ketqua WHERE ngay = '" + mDate + "'");
        cursor.moveToFirst();
        int i2 = 2;
        while (i2 < 29) {
            try {
                if (cursor.isNull(i2) || !Congthuc.isNumeric(cursor.getString(i2))) {
                    break;
                }
                i2++;
            } catch (Exception e) {
            }
        }
        if (i2 >= 29) {
            this.db.Tinhtien(mDate);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        try {
            this.mTenKH.clear();
            this.mSDT.clear();
            this.lv_baocaoKhach.setAdapter((ListAdapter) null);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void lv_baoCao() {
        Cursor cursor;
        double TienNhan;
        String str = "bc";
        String str2 = "xi";
        String str3 = "loa";
        String str4 = "lo";
        String str5 = "ded";
        String str6 = "dec";
        String str7 = "det";
        String str8 = "deb";
        new MainActivity();
        String mDate = MainActivity.Get_date();
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        String getBC = "Select the_loai\n, sum((type_kh = 1)*(100-diem_khachgiu)*diem/100) as mDiem\n, CASE WHEN the_loai = 'xi' OR the_loai = 'xia' \n THEN sum((type_kh = 1)*(100-diem_khachgiu)*diem/100*so_nhay*lan_an/1000) \n ELSE sum((type_kh = 1)*(100-diem_khachgiu)*diem/100*so_nhay)  END nAn\n, sum((type_kh = 1)*ket_qua*(100-diem_khachgiu)/100/1000) as mKetqua\n, sum((type_kh = 2)*(100-diem_khachgiu)*diem/100) as mDiem\n, CASE WHEN the_loai = 'xi' OR the_loai = 'xia' \n THEN sum((type_kh = 2)*(100-diem_khachgiu)*diem/100*so_nhay*lan_an/1000) \n ELSE sum((type_kh = 2)*(100-diem_khachgiu)*diem/100*so_nhay)  END nAn\n, sum((type_kh = 2)*ket_qua*(100-diem_khachgiu)/100/1000) as mKetqua\n  From tbl_soctS Where ngay_nhan = '" + mDate + "'\n  AND the_loai <> 'tt' GROUP by the_loai";
        Cursor cursor2 = this.db.GetData(getBC);
        if (cursor2 != null) {
            JSONObject json = new JSONObject();
            double TienNhan2 = 0.0d;
            double TienChuyen = 0.0d;
            while (cursor2.moveToNext()) {
                try {
                    try {
                        JSONObject jsonDang = new JSONObject();
                        jsonDang.put("DiemNhan", decimalFormat.format(cursor2.getDouble(1)));
                        jsonDang.put("AnNhan", decimalFormat.format(cursor2.getDouble(2)));
                        jsonDang.put("KQNhan", decimalFormat.format(cursor2.getDouble(3)));
                        jsonDang.put("DiemChuyen", decimalFormat.format(cursor2.getDouble(4)));
                        jsonDang.put("AnChuyen", decimalFormat.format(cursor2.getDouble(5)));
                        jsonDang.put("KQChuyen", decimalFormat.format(cursor2.getDouble(6)));
                        TienNhan2 += cursor2.getDouble(3);
                        TienChuyen += cursor2.getDouble(6);
                        try {
                            json.put(cursor2.getString(0), jsonDang.toString());
                            json = json;
                            str7 = str7;
                            str = str;
                            str2 = str2;
                            str3 = str3;
                            str4 = str4;
                            str5 = str5;
                            str6 = str6;
                            str8 = str8;
                        } catch (JSONException e) {
                            cursor = cursor2;
                        }
                    } catch (JSONException e2) {
                        cursor = cursor2;
                    }
                } catch (JSONException e3) {
                    cursor = cursor2;
                }
            }
            try {
                if (json.length() > 0) {
                    boolean has = json.has("dea");
                    cursor = cursor2;
                    if (has) {
                        try {
                            this.li_dea.setVisibility(0);
                            JSONObject jsonDang2 = new JSONObject(json.getString("dea"));
                            if (jsonDang2.getString("DiemNhan").length() > 0) {
                                TextView textView = this.dea_Nhan;
                                StringBuilder sb = new StringBuilder();
                                TienNhan = TienNhan2;
                                try {
                                    sb.append(jsonDang2.getString("DiemNhan"));
                                    sb.append("(");
                                    sb.append(jsonDang2.getString("AnNhan"));
                                    sb.append(")");
                                    textView.setText(sb.toString());
                                    this.dea_NhanAn.setText(jsonDang2.getString("KQNhan"));
                                } catch (JSONException e4) {
                                }
                            } else {
                                TienNhan = TienNhan2;
                            }
                            if (jsonDang2.getString("DiemChuyen").length() > 0) {
                                this.dea_Chuyen.setText(jsonDang2.getString("DiemChuyen") + "(" + jsonDang2.getString("AnChuyen") + ")");
                                this.dea_ChuyenAn.setText(jsonDang2.getString("KQChuyen"));
                            }
                        } catch (JSONException e5) {
                        }
                    } else {
                        TienNhan = TienNhan2;
                    }
                    try {
                        if (json.has(str8)) {
                            JSONObject jsonDang3 = new JSONObject(json.getString(str8));
                            if (jsonDang3.getString("DiemNhan").length() > 0) {
                                this.deb_Nhan.setText(jsonDang3.getString("DiemNhan") + "(" + jsonDang3.getString("AnNhan") + ")");
                                this.deb_NhanAn.setText(jsonDang3.getString("KQNhan"));
                            }
                            if (jsonDang3.getString("DiemChuyen").length() > 0) {
                                this.deb_Chuyen.setText(jsonDang3.getString("DiemChuyen") + "(" + jsonDang3.getString("AnChuyen") + ")");
                                this.deb_ChuyenAn.setText(jsonDang3.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str7)) {
                            this.li_det.setVisibility(0);
                            JSONObject jsonDang4 = new JSONObject(json.getString(str7));
                            if (jsonDang4.getString("DiemNhan").length() > 0) {
                                this.det_Nhan.setText(jsonDang4.getString("DiemNhan") + "(" + jsonDang4.getString("AnNhan") + ")");
                                this.det_NhanAn.setText(jsonDang4.getString("KQNhan"));
                            }
                            if (jsonDang4.getString("DiemChuyen").length() > 0) {
                                this.det_Chuyen.setText(jsonDang4.getString("DiemChuyen") + "(" + jsonDang4.getString("AnChuyen") + ")");
                                this.det_ChuyenAn.setText(jsonDang4.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str6)) {
                            this.li_dec.setVisibility(0);
                            JSONObject jsonDang5 = new JSONObject(json.getString(str6));
                            if (jsonDang5.getString("DiemNhan").length() > 0) {
                                this.dec_Nhan.setText(jsonDang5.getString("DiemNhan") + "(" + jsonDang5.getString("AnNhan") + ")");
                                this.dec_NhanAn.setText(jsonDang5.getString("KQNhan"));
                            }
                            if (jsonDang5.getString("DiemChuyen").length() > 0) {
                                this.dec_Chuyen.setText(jsonDang5.getString("DiemChuyen") + "(" + jsonDang5.getString("AnChuyen") + ")");
                                this.dec_ChuyenAn.setText(jsonDang5.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str5)) {
                            this.li_ded.setVisibility(0);
                            JSONObject jsonDang6 = new JSONObject(json.getString(str5));
                            if (jsonDang6.getString("DiemNhan").length() > 0) {
                                this.ded_Nhan.setText(jsonDang6.getString("DiemNhan") + "(" + jsonDang6.getString("AnNhan") + ")");
                                this.ded_NhanAn.setText(jsonDang6.getString("KQNhan"));
                            }
                            if (jsonDang6.getString("DiemChuyen").length() > 0) {
                                this.ded_Chuyen.setText(jsonDang6.getString("DiemChuyen") + "(" + jsonDang6.getString("AnChuyen") + ")");
                                this.ded_ChuyenAn.setText(jsonDang6.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str4)) {
                            JSONObject jsonDang7 = new JSONObject(json.getString(str4));
                            if (jsonDang7.getString("DiemNhan").length() > 0) {
                                this.lo_Nhan.setText(jsonDang7.getString("DiemNhan") + "(" + jsonDang7.getString("AnNhan") + ")");
                                this.lo_NhanAn.setText(jsonDang7.getString("KQNhan"));
                            }
                            if (jsonDang7.getString("DiemChuyen").length() > 0) {
                                this.lo_Chuyen.setText(jsonDang7.getString("DiemChuyen") + "(" + jsonDang7.getString("AnChuyen") + ")");
                                this.lo_ChuyenAn.setText(jsonDang7.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str3)) {
                            this.li_loa.setVisibility(0);
                            JSONObject jsonDang8 = new JSONObject(json.getString(str3));
                            if (jsonDang8.getString("DiemNhan").length() > 0) {
                                this.loa_Nhan.setText(jsonDang8.getString("DiemNhan") + "(" + jsonDang8.getString("AnNhan") + ")");
                                this.loa_NhanAn.setText(jsonDang8.getString("KQNhan"));
                            }
                            if (jsonDang8.getString("DiemChuyen").length() > 0) {
                                this.loa_Chuyen.setText(jsonDang8.getString("DiemChuyen") + "(" + jsonDang8.getString("AnChuyen") + ")");
                                this.loa_ChuyenAn.setText(jsonDang8.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str2)) {
                            JSONObject jsonDang9 = new JSONObject(json.getString(str2));
                            if (jsonDang9.getString("DiemNhan").length() > 0) {
                                this.xi2_Nhan.setText(jsonDang9.getString("DiemNhan") + "(" + jsonDang9.getString("AnNhan") + ")");
                                this.xi2_NhanAn.setText(jsonDang9.getString("KQNhan"));
                            }
                            if (jsonDang9.getString("DiemChuyen").length() > 0) {
                                this.xi2_Chuyen.setText(jsonDang9.getString("DiemChuyen") + "(" + jsonDang9.getString("AnChuyen") + ")");
                                this.xi2_ChuyenAn.setText(jsonDang9.getString("KQChuyen"));
                            }
                        }
                        if (json.has("xn")) {
                            this.li_xn.setVisibility(0);
                            JSONObject jsonDang10 = new JSONObject(json.getString("xn"));
                            if (jsonDang10.getString("DiemNhan").length() > 0) {
                                this.xn_Nhan.setText(jsonDang10.getString("DiemNhan") + "(" + jsonDang10.getString("AnNhan") + ")");
                                this.xn_NhanAn.setText(jsonDang10.getString("KQNhan"));
                            }
                            if (jsonDang10.getString("DiemChuyen").length() > 0) {
                                this.xn_Chuyen.setText(jsonDang10.getString("DiemChuyen") + "(" + jsonDang10.getString("AnChuyen") + ")");
                                this.xn_ChuyenAn.setText(jsonDang10.getString("KQChuyen"));
                            }
                        }
                        if (json.has("xia")) {
                            this.li_xia2.setVisibility(0);
                            JSONObject jsonDang11 = new JSONObject(json.getString("xia"));
                            if (jsonDang11.getString("DiemNhan").length() > 0) {
                                this.xia2_Nhan.setText(jsonDang11.getString("DiemNhan") + "(" + jsonDang11.getString("AnNhan") + ")");
                                this.xia2_NhanAn.setText(jsonDang11.getString("KQNhan"));
                            }
                            if (jsonDang11.getString("DiemChuyen").length() > 0) {
                                this.xia2_Chuyen.setText(jsonDang11.getString("DiemChuyen") + "(" + jsonDang11.getString("AnChuyen") + ")");
                                this.xia2_ChuyenAn.setText(jsonDang11.getString("KQChuyen"));
                            }
                        }
                        if (json.has(str)) {
                            JSONObject jsonDang12 = new JSONObject(json.getString(str));
                            if (jsonDang12.getString("DiemNhan").length() > 0) {
                                this.bc_Nhan.setText(jsonDang12.getString("DiemNhan") + "(" + jsonDang12.getString("AnNhan") + ")");
                                this.bc_NhanAn.setText(jsonDang12.getString("KQNhan"));
                            }
                            if (jsonDang12.getString("DiemChuyen").length() > 0) {
                                this.bc_Chuyen.setText(jsonDang12.getString("DiemChuyen") + "(" + jsonDang12.getString("AnChuyen") + ")");
                                this.bc_ChuyenAn.setText(jsonDang12.getString("KQChuyen"));
                            }
                        }
                        if (json.has("bca")) {
                            this.li_bca.setVisibility(0);
                            JSONObject jsonDang13 = new JSONObject(json.getString("bca"));
                            if (jsonDang13.getString("DiemNhan").length() > 0) {
                                this.bca_Nhan.setText(jsonDang13.getString("DiemNhan") + "(" + jsonDang13.getString("AnNhan") + ")");
                                this.bca_NhanAn.setText(jsonDang13.getString("KQNhan"));
                            }
                            if (jsonDang13.getString("DiemChuyen").length() > 0) {
                                this.bca_Chuyen.setText(jsonDang13.getString("DiemChuyen") + "(" + jsonDang13.getString("AnChuyen") + ")");
                                this.bca_ChuyenAn.setText(jsonDang13.getString("KQChuyen"));
                            }
                        }
                        try {
                            this.tv_TongTienNhan.setText(decimalFormat.format(TienNhan));
                            try {
                                this.tv_TongTienChuyen.setText(decimalFormat.format(TienChuyen));
                                this.tv_TongGiu.setText(decimalFormat.format((-TienNhan) - TienChuyen));
                            } catch (JSONException e6) {
                            }
                        } catch (JSONException e7) {
                        }
                    } catch (JSONException e8) {
                    }
                } else {
                    cursor = cursor2;
                }
            } catch (JSONException e9) {
                cursor = cursor2;
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
            XemListview();
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:71:0x01e7  */
    /* JADX WARN: Removed duplicated region for block: B:92:? A[RETURN, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    private void XemListview() {
        /*
            Method dump skipped, instructions count: 507
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_No_new.XemListview():void");
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class NoRP_TN_Adapter extends ArrayAdapter {
        TextView bc_Chuyen;
        TextView bc_ChuyenAn;
        TextView bc_Nhan;
        TextView bc_NhanAn;
        TextView bca_Chuyen;
        TextView bca_ChuyenAn;
        TextView bca_Nhan;
        TextView bca_NhanAn;
        TextView dea_Chuyen;
        TextView dea_ChuyenAn;
        TextView dea_Nhan;
        TextView dea_NhanAn;
        TextView deb_Chuyen;
        TextView deb_ChuyenAn;
        TextView deb_Nhan;
        TextView deb_NhanAn;
        TextView dec_Chuyen;
        TextView dec_ChuyenAn;
        TextView dec_Nhan;
        TextView dec_NhanAn;
        TextView ded_Chuyen;
        TextView ded_ChuyenAn;
        TextView ded_Nhan;
        TextView ded_NhanAn;
        TextView det_Chuyen;
        TextView det_ChuyenAn;
        TextView det_Nhan;
        TextView det_NhanAn;
        LinearLayout li_bca;
        LinearLayout li_dea;
        LinearLayout li_dec;
        LinearLayout li_ded;
        LinearLayout li_det;
        LinearLayout li_loa;
        LinearLayout li_xi2;
        LinearLayout li_xia2;
        TextView lo_Chuyen;
        TextView lo_ChuyenAn;
        TextView lo_Nhan;
        TextView lo_NhanAn;
        TextView loa_Chuyen;
        TextView loa_ChuyenAn;
        TextView loa_Nhan;
        TextView loa_NhanAn;
        TextView tv_TongKhach;
        TextView tv_TongTienChuyen;
        TextView tv_TongTienNhan;
        TextView tv_ket_qua;
        TextView tv_tenKH;
        TextView tv_tongtien;
        TextView xi2_Chuyen;
        TextView xi2_ChuyenAn;
        TextView xi2_Nhan;
        TextView xi2_NhanAn;
        TextView xia2_Chuyen;
        TextView xia2_ChuyenAn;
        TextView xia2_Nhan;
        TextView xia2_NhanAn;

        public NoRP_TN_Adapter(Context context, int resource, List<JSONObject> objects) {
            super(context, resource, objects);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View mView, ViewGroup parent) {
            View mView2;
            String str;
            String str2;
            View mView3 = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.frag_norp1_2, (ViewGroup) null);
            this.tv_tongtien = (TextView) mView3.findViewById(R.id.tv_no_Tong);
            this.tv_ket_qua = (TextView) mView3.findViewById(R.id.tv_ThangThua);
            this.tv_tenKH = (TextView) mView3.findViewById(R.id.tv_tenKH);
            this.dea_Nhan = (TextView) mView3.findViewById(R.id.dea_Nhan);
            this.deb_Nhan = (TextView) mView3.findViewById(R.id.deb_Nhan);
            this.det_Nhan = (TextView) mView3.findViewById(R.id.det_Nhan);
            this.dec_Nhan = (TextView) mView3.findViewById(R.id.dec_Nhan);
            this.ded_Nhan = (TextView) mView3.findViewById(R.id.ded_Nhan);
            this.lo_Nhan = (TextView) mView3.findViewById(R.id.lo_Nhan);
            this.loa_Nhan = (TextView) mView3.findViewById(R.id.loa_Nhan);
            this.bc_Nhan = (TextView) mView3.findViewById(R.id.bc_Nhan);
            this.bca_Nhan = (TextView) mView3.findViewById(R.id.bca_Nhan);
            this.dea_NhanAn = (TextView) mView3.findViewById(R.id.dea_NhanAn);
            this.deb_NhanAn = (TextView) mView3.findViewById(R.id.deb_NhanAn);
            this.det_NhanAn = (TextView) mView3.findViewById(R.id.det_NhanAn);
            this.dec_NhanAn = (TextView) mView3.findViewById(R.id.dec_NhanAn);
            this.ded_NhanAn = (TextView) mView3.findViewById(R.id.ded_NhanAn);
            this.lo_NhanAn = (TextView) mView3.findViewById(R.id.lo_NhanAn);
            this.loa_NhanAn = (TextView) mView3.findViewById(R.id.loa_NhanAn);
            this.bc_NhanAn = (TextView) mView3.findViewById(R.id.bc_NhanAn);
            this.dea_Chuyen = (TextView) mView3.findViewById(R.id.dea_Chuyen);
            this.deb_Chuyen = (TextView) mView3.findViewById(R.id.deb_Chuyen);
            this.det_Chuyen = (TextView) mView3.findViewById(R.id.det_Chuyen);
            this.dec_Chuyen = (TextView) mView3.findViewById(R.id.dec_Chuyen);
            this.ded_Chuyen = (TextView) mView3.findViewById(R.id.ded_Chuyen);
            this.lo_Chuyen = (TextView) mView3.findViewById(R.id.lo_Chuyen);
            this.loa_Chuyen = (TextView) mView3.findViewById(R.id.loa_Chuyen);
            this.bc_Chuyen = (TextView) mView3.findViewById(R.id.bc_Chuyen);
            this.dea_ChuyenAn = (TextView) mView3.findViewById(R.id.dea_ChuyenAn);
            this.deb_ChuyenAn = (TextView) mView3.findViewById(R.id.deb_ChuyenAn);
            this.det_ChuyenAn = (TextView) mView3.findViewById(R.id.det_ChuyenAn);
            this.dec_ChuyenAn = (TextView) mView3.findViewById(R.id.dec_ChuyenAn);
            this.ded_ChuyenAn = (TextView) mView3.findViewById(R.id.ded_ChuyenAn);
            this.lo_ChuyenAn = (TextView) mView3.findViewById(R.id.lo_ChuyenAn);
            this.loa_ChuyenAn = (TextView) mView3.findViewById(R.id.loa_ChuyenAn);
            this.bc_ChuyenAn = (TextView) mView3.findViewById(R.id.bc_ChuyenAn);
            this.tv_TongKhach = (TextView) mView3.findViewById(R.id.tv_TongTien);
            this.tv_TongTienNhan = (TextView) mView3.findViewById(R.id.tv_TongTienNhan);
            this.tv_TongTienChuyen = (TextView) mView3.findViewById(R.id.tv_TongTienChuyen);
            this.li_dea = (LinearLayout) mView3.findViewById(R.id.li_dea);
            this.li_det = (LinearLayout) mView3.findViewById(R.id.li_det);
            this.li_dec = (LinearLayout) mView3.findViewById(R.id.li_dec);
            this.li_ded = (LinearLayout) mView3.findViewById(R.id.li_ded);
            this.li_loa = (LinearLayout) mView3.findViewById(R.id.li_loa);
            this.li_bca = (LinearLayout) mView3.findViewById(R.id.li_bca);
            this.li_xi2 = (LinearLayout) mView3.findViewById(R.id.li_xi2);
            this.li_xia2 = (LinearLayout) mView3.findViewById(R.id.li_xia2);
            this.xi2_Nhan = (TextView) mView3.findViewById(R.id.xi2_Nhan);
            this.xi2_NhanAn = (TextView) mView3.findViewById(R.id.xi2_NhanAn);
            this.xi2_Chuyen = (TextView) mView3.findViewById(R.id.xi2_Chuyen);
            this.xi2_ChuyenAn = (TextView) mView3.findViewById(R.id.xi2_ChuyenAn);
            this.xia2_Nhan = (TextView) mView3.findViewById(R.id.xia2_Nhan);
            this.xia2_NhanAn = (TextView) mView3.findViewById(R.id.xia2_NhanAn);
            this.xia2_Chuyen = (TextView) mView3.findViewById(R.id.xia2_Chuyen);
            this.xia2_ChuyenAn = (TextView) mView3.findViewById(R.id.xia2_ChuyenAn);
            this.bca_Nhan = (TextView) mView3.findViewById(R.id.bca_Nhan);
            this.bca_NhanAn = (TextView) mView3.findViewById(R.id.bca_NhanAn);
            this.bca_Chuyen = (TextView) mView3.findViewById(R.id.bca_Chuyen);
            this.bca_ChuyenAn = (TextView) mView3.findViewById(R.id.bca_ChuyenAn);
            JSONObject json = Frag_No_new.this.jsonKhachHang.get(position);
            try {
                mView2 = mView3;
            } catch (Exception e) {
                mView2 = mView3;
            }
            try {
                this.tv_TongTienNhan.setText(json.getString("Tien_Nhan"));
                this.tv_TongTienChuyen.setText(json.getString("Tien_Chuyen"));
                this.tv_TongKhach.setText(json.getString("Tong_Tien"));
                this.tv_tenKH.setText((CharSequence) Frag_No_new.this.mTenKH.get(position));
                if (json.has("dea")) {
                    str2 = "ded";
                    this.li_dea.setVisibility(0);
                    JSONObject jsonDang = new JSONObject(json.getString("dea"));
                    if (jsonDang.getString("DiemNhan").length() > 0) {
                        TextView textView = this.dea_Nhan;
                        StringBuilder sb = new StringBuilder();
                        str = "dec";
                        sb.append(jsonDang.getString("DiemNhan"));
                        sb.append("(");
                        sb.append(jsonDang.getString("AnNhan"));
                        sb.append(")");
                        textView.setText(sb.toString());
                        this.dea_NhanAn.setText(jsonDang.getString("KQNhan"));
                    } else {
                        str = "dec";
                    }
                    if (jsonDang.getString("DiemChuyen").length() > 0) {
                        TextView textView2 = this.dea_Chuyen;
                        textView2.setText(jsonDang.getString("DiemChuyen") + "(" + jsonDang.getString("AnChuyen") + ")");
                        this.dea_ChuyenAn.setText(jsonDang.getString("KQChuyen"));
                    }
                } else {
                    str2 = "ded";
                    str = "dec";
                }
                if (json.has("deb")) {
                    JSONObject jsonDang2 = new JSONObject(json.getString("deb"));
                    if (jsonDang2.getString("DiemNhan").length() > 0) {
                        TextView textView3 = this.deb_Nhan;
                        textView3.setText(jsonDang2.getString("DiemNhan") + "(" + jsonDang2.getString("AnNhan") + ")");
                        this.deb_NhanAn.setText(jsonDang2.getString("KQNhan"));
                    }
                    if (jsonDang2.getString("DiemChuyen").length() > 0) {
                        TextView textView4 = this.deb_Chuyen;
                        textView4.setText(jsonDang2.getString("DiemChuyen") + "(" + jsonDang2.getString("AnChuyen") + ")");
                        this.deb_ChuyenAn.setText(jsonDang2.getString("KQChuyen"));
                    }
                }
                if (json.has("det")) {
                    this.li_det.setVisibility(0);
                    JSONObject jsonDang3 = new JSONObject(json.getString("det"));
                    if (jsonDang3.getString("DiemNhan").length() > 0) {
                        TextView textView5 = this.det_Nhan;
                        textView5.setText(jsonDang3.getString("DiemNhan") + "(" + jsonDang3.getString("AnNhan") + ")");
                        this.det_NhanAn.setText(jsonDang3.getString("KQNhan"));
                    }
                    if (jsonDang3.getString("DiemChuyen").length() > 0) {
                        TextView textView6 = this.det_Chuyen;
                        textView6.setText(jsonDang3.getString("DiemChuyen") + "(" + jsonDang3.getString("AnChuyen") + ")");
                        this.det_ChuyenAn.setText(jsonDang3.getString("KQChuyen"));
                    }
                }
                if (json.has(str)) {
                    this.li_dec.setVisibility(0);
                    JSONObject jsonDang4 = new JSONObject(json.getString(str));
                    if (jsonDang4.getString("DiemNhan").length() > 0) {
                        TextView textView7 = this.dec_Nhan;
                        textView7.setText(jsonDang4.getString("DiemNhan") + "(" + jsonDang4.getString("AnNhan") + ")");
                        this.dec_NhanAn.setText(jsonDang4.getString("KQNhan"));
                    }
                    if (jsonDang4.getString("DiemChuyen").length() > 0) {
                        TextView textView8 = this.dec_Chuyen;
                        textView8.setText(jsonDang4.getString("DiemChuyen") + "(" + jsonDang4.getString("AnChuyen") + ")");
                        this.dec_ChuyenAn.setText(jsonDang4.getString("KQChuyen"));
                    }
                }
                if (json.has(str2)) {
                    this.li_ded.setVisibility(0);
                    JSONObject jsonDang5 = new JSONObject(json.getString(str2));
                    if (jsonDang5.getString("DiemNhan").length() > 0) {
                        TextView textView9 = this.ded_Nhan;
                        textView9.setText(jsonDang5.getString("DiemNhan") + "(" + jsonDang5.getString("AnNhan") + ")");
                        this.ded_NhanAn.setText(jsonDang5.getString("KQNhan"));
                    }
                    if (jsonDang5.getString("DiemChuyen").length() > 0) {
                        TextView textView10 = this.ded_Chuyen;
                        textView10.setText(jsonDang5.getString("DiemChuyen") + "(" + jsonDang5.getString("AnChuyen") + ")");
                        this.ded_ChuyenAn.setText(jsonDang5.getString("KQChuyen"));
                    }
                }
                if (json.has("lo")) {
                    JSONObject jsonDang6 = new JSONObject(json.getString("lo"));
                    if (jsonDang6.getString("DiemNhan").length() > 0) {
                        TextView textView11 = this.lo_Nhan;
                        textView11.setText(jsonDang6.getString("DiemNhan") + "(" + jsonDang6.getString("AnNhan") + ")");
                        this.lo_NhanAn.setText(jsonDang6.getString("KQNhan"));
                    }
                    if (jsonDang6.getString("DiemChuyen").length() > 0) {
                        TextView textView12 = this.lo_Chuyen;
                        textView12.setText(jsonDang6.getString("DiemChuyen") + "(" + jsonDang6.getString("AnChuyen") + ")");
                        this.lo_ChuyenAn.setText(jsonDang6.getString("KQChuyen"));
                    }
                }
                if (json.has("loa")) {
                    this.li_loa.setVisibility(0);
                    JSONObject jsonDang7 = new JSONObject(json.getString("loa"));
                    if (jsonDang7.getString("DiemNhan").length() > 0) {
                        TextView textView13 = this.loa_Nhan;
                        textView13.setText(jsonDang7.getString("DiemNhan") + "(" + jsonDang7.getString("AnNhan") + ")");
                        this.loa_NhanAn.setText(jsonDang7.getString("KQNhan"));
                    }
                    if (jsonDang7.getString("DiemChuyen").length() > 0) {
                        TextView textView14 = this.loa_Chuyen;
                        textView14.setText(jsonDang7.getString("DiemChuyen") + "(" + jsonDang7.getString("AnChuyen") + ")");
                        this.loa_ChuyenAn.setText(jsonDang7.getString("KQChuyen"));
                    }
                }
                if (json.has("xi")) {
                    this.li_xi2.setVisibility(0);
                    JSONObject jsonDang8 = new JSONObject(json.getString("xi"));
                    if (jsonDang8.getString("DiemNhan").length() > 0) {
                        TextView textView15 = this.xi2_Nhan;
                        textView15.setText(jsonDang8.getString("DiemNhan") + "(" + jsonDang8.getString("AnNhan") + ")");
                        this.xi2_NhanAn.setText(jsonDang8.getString("KQNhan"));
                    }
                    if (jsonDang8.getString("DiemChuyen").length() > 0) {
                        TextView textView16 = this.xi2_Chuyen;
                        textView16.setText(jsonDang8.getString("DiemChuyen") + "(" + jsonDang8.getString("AnChuyen") + ")");
                        this.xi2_ChuyenAn.setText(jsonDang8.getString("KQChuyen"));
                    }
                }
                if (json.has("xia")) {
                    this.li_xia2.setVisibility(0);
                    JSONObject jsonDang9 = new JSONObject(json.getString("xia"));
                    if (jsonDang9.getString("DiemNhan").length() > 0) {
                        TextView textView17 = this.xia2_Nhan;
                        textView17.setText(jsonDang9.getString("DiemNhan") + "(" + jsonDang9.getString("AnNhan") + ")");
                        this.xia2_NhanAn.setText(jsonDang9.getString("KQNhan"));
                    }
                    if (jsonDang9.getString("DiemChuyen").length() > 0) {
                        TextView textView18 = this.xia2_Chuyen;
                        textView18.setText(jsonDang9.getString("DiemChuyen") + "(" + jsonDang9.getString("AnChuyen") + ")");
                        this.xia2_ChuyenAn.setText(jsonDang9.getString("KQChuyen"));
                    }
                }
                if (json.has("bc")) {
                    JSONObject jsonDang10 = new JSONObject(json.getString("bc"));
                    if (jsonDang10.getString("DiemNhan").length() > 0) {
                        TextView textView19 = this.bc_Nhan;
                        textView19.setText(jsonDang10.getString("DiemNhan") + "(" + jsonDang10.getString("AnNhan") + ")");
                        this.bc_NhanAn.setText(jsonDang10.getString("KQNhan"));
                    }
                    if (jsonDang10.getString("DiemChuyen").length() > 0) {
                        TextView textView20 = this.bc_Chuyen;
                        textView20.setText(jsonDang10.getString("DiemChuyen") + "(" + jsonDang10.getString("AnChuyen") + ")");
                        this.bc_ChuyenAn.setText(jsonDang10.getString("KQChuyen"));
                    }
                }
                if (json.has("bca")) {
                    this.li_bca.setVisibility(0);
                    JSONObject jsonDang11 = new JSONObject(json.getString("bca"));
                    if (jsonDang11.getString("DiemNhan").length() > 0) {
                        TextView textView21 = this.bca_Nhan;
                        textView21.setText(jsonDang11.getString("DiemNhan") + "(" + jsonDang11.getString("AnNhan") + ")");
                        this.bca_NhanAn.setText(jsonDang11.getString("KQNhan"));
                    }
                    if (jsonDang11.getString("DiemChuyen").length() > 0) {
                        TextView textView22 = this.bca_Chuyen;
                        textView22.setText(jsonDang11.getString("DiemChuyen") + "(" + jsonDang11.getString("AnChuyen") + ")");
                        this.bca_ChuyenAn.setText(jsonDang11.getString("KQChuyen"));
                    }
                }
            } catch (Exception e2) {
                Toast.makeText(Frag_No_new.this.getActivity(), "OK", 1).show();
                return mView2;
            }
            return mView2;
        }
    }

    private void init() {
        this.xn_Nhan = (TextView) this.v.findViewById(R.id.xn_Nhan);
        this.xn_NhanAn = (TextView) this.v.findViewById(R.id.xn_NhanAn);
        this.xn_Chuyen = (TextView) this.v.findViewById(R.id.xn_Chuyen);
        this.xn_ChuyenAn = (TextView) this.v.findViewById(R.id.xn_ChuyenAn);
        this.dea_Nhan = (TextView) this.v.findViewById(R.id.dea_Nhan);
        this.deb_Nhan = (TextView) this.v.findViewById(R.id.deb_Nhan);
        this.det_Nhan = (TextView) this.v.findViewById(R.id.det_Nhan);
        this.dec_Nhan = (TextView) this.v.findViewById(R.id.dec_Nhan);
        this.ded_Nhan = (TextView) this.v.findViewById(R.id.ded_Nhan);
        this.lo_Nhan = (TextView) this.v.findViewById(R.id.lo_Nhan);
        this.loa_Nhan = (TextView) this.v.findViewById(R.id.loa_Nhan);
        this.bc_Nhan = (TextView) this.v.findViewById(R.id.bc_Nhan);
        this.bca_Nhan = (TextView) this.v.findViewById(R.id.bca_Nhan);
        this.dea_NhanAn = (TextView) this.v.findViewById(R.id.dea_NhanAn);
        this.deb_NhanAn = (TextView) this.v.findViewById(R.id.deb_NhanAn);
        this.det_NhanAn = (TextView) this.v.findViewById(R.id.det_NhanAn);
        this.dec_NhanAn = (TextView) this.v.findViewById(R.id.dec_NhanAn);
        this.ded_NhanAn = (TextView) this.v.findViewById(R.id.ded_NhanAn);
        this.lo_NhanAn = (TextView) this.v.findViewById(R.id.lo_NhanAn);
        this.loa_NhanAn = (TextView) this.v.findViewById(R.id.loa_NhanAn);
        this.bc_NhanAn = (TextView) this.v.findViewById(R.id.bc_NhanAn);
        this.dea_Chuyen = (TextView) this.v.findViewById(R.id.dea_Chuyen);
        this.deb_Chuyen = (TextView) this.v.findViewById(R.id.deb_Chuyen);
        this.det_Chuyen = (TextView) this.v.findViewById(R.id.det_Chuyen);
        this.dec_Chuyen = (TextView) this.v.findViewById(R.id.dec_Chuyen);
        this.ded_Chuyen = (TextView) this.v.findViewById(R.id.ded_Chuyen);
        this.lo_Chuyen = (TextView) this.v.findViewById(R.id.lo_Chuyen);
        this.loa_Chuyen = (TextView) this.v.findViewById(R.id.loa_Chuyen);
        this.bc_Chuyen = (TextView) this.v.findViewById(R.id.bc_Chuyen);
        this.dea_ChuyenAn = (TextView) this.v.findViewById(R.id.dea_ChuyenAn);
        this.deb_ChuyenAn = (TextView) this.v.findViewById(R.id.deb_ChuyenAn);
        this.det_ChuyenAn = (TextView) this.v.findViewById(R.id.det_ChuyenAn);
        this.dec_ChuyenAn = (TextView) this.v.findViewById(R.id.dec_ChuyenAn);
        this.ded_ChuyenAn = (TextView) this.v.findViewById(R.id.ded_ChuyenAn);
        this.lo_ChuyenAn = (TextView) this.v.findViewById(R.id.lo_ChuyenAn);
        this.loa_ChuyenAn = (TextView) this.v.findViewById(R.id.loa_ChuyenAn);
        this.bc_ChuyenAn = (TextView) this.v.findViewById(R.id.bc_ChuyenAn);
        this.tv_TongGiu = (TextView) this.v.findViewById(R.id.tv_TongGiu);
        this.tv_TongTienNhan = (TextView) this.v.findViewById(R.id.tv_TongTienNhan);
        this.tv_TongTienChuyen = (TextView) this.v.findViewById(R.id.tv_TongTienChuyen);
        this.li_dea = (LinearLayout) this.v.findViewById(R.id.li_dea);
        this.li_det = (LinearLayout) this.v.findViewById(R.id.li_det);
        this.li_dec = (LinearLayout) this.v.findViewById(R.id.li_dec);
        this.li_ded = (LinearLayout) this.v.findViewById(R.id.li_ded);
        this.li_loa = (LinearLayout) this.v.findViewById(R.id.li_loa);
        this.li_bca = (LinearLayout) this.v.findViewById(R.id.li_bca);
        this.li_xia2 = (LinearLayout) this.v.findViewById(R.id.li_xia2);
        this.li_xn = (LinearLayout) this.v.findViewById(R.id.li_xn);
        this.xi2_Nhan = (TextView) this.v.findViewById(R.id.xi2_Nhan);
        this.xi2_NhanAn = (TextView) this.v.findViewById(R.id.xi2_NhanAn);
        this.xi2_Chuyen = (TextView) this.v.findViewById(R.id.xi2_Chuyen);
        this.xi2_ChuyenAn = (TextView) this.v.findViewById(R.id.xi2_ChuyenAn);
        this.xia2_Nhan = (TextView) this.v.findViewById(R.id.xia2_Nhan);
        this.xia2_NhanAn = (TextView) this.v.findViewById(R.id.xia2_NhanAn);
        this.xia2_Chuyen = (TextView) this.v.findViewById(R.id.xia2_Chuyen);
        this.xia2_ChuyenAn = (TextView) this.v.findViewById(R.id.xia2_ChuyenAn);
        this.bca_Nhan = (TextView) this.v.findViewById(R.id.bca_Nhan);
        this.bca_NhanAn = (TextView) this.v.findViewById(R.id.bca_NhanAn);
        this.bca_Chuyen = (TextView) this.v.findViewById(R.id.bca_Chuyen);
        this.bca_ChuyenAn = (TextView) this.v.findViewById(R.id.bca_ChuyenAn);
        this.lv_baocaoKhach = (ListView) this.v.findViewById(R.id.lv_baocaoKhach);
    }
}
