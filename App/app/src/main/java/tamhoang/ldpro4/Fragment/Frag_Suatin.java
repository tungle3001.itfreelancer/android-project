package tamhoang.ldpro4.Fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.json.JSONException;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_Suatin extends Fragment {
    String CurDate;
    String Cur_date;
    Button btn_LoadTin;
    Button btn_suatin;
    Button btn_tai_All;
    Database db;
    EditText editTsuatin;
    boolean error;
    Handler handler;
    ListView lv_suatin;
    RadioButton radio_SuaTin;
    RadioButton radio_TaiTin;
    Spinner sp_TenKH;
    String str;
    int type_kh;
    View v;
    public List<Integer> mID = new ArrayList();
    public List<String> mNgay = new ArrayList();
    public List<String> mSDT = new ArrayList();
    public List<String> mTenKH = new ArrayList();
    public List<Integer> mSoTinNhan = new ArrayList();
    public List<String> mTinNhanGoc = new ArrayList();
    public List<String> mND_DaSua = new ArrayList();
    public List<String> mND_PhanTich = new ArrayList();
    public List<String> mPhatHienLoi = new ArrayList();
    public List<Integer> mTypeKH = new ArrayList();
    int lv_position = -1;
    int spin_pointion = -1;
    public List<String> mContact = new ArrayList();
    public List<String> mMobile = new ArrayList();
    public List<String> mApp = new ArrayList();
    public List<Integer> mType_kh = new ArrayList();
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.13
        @Override // java.lang.Runnable
        public void run() {
            new MainActivity();
            if (MainActivity.sms) {
                Frag_Suatin.this.xem_lv();
                MainActivity.sms = false;
            }
            Frag_Suatin.this.handler.postDelayed(this, 1000L);
        }
    };
    private Runnable xulyTinnhan = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.14
        @Override // java.lang.Runnable
        public void run() {
            Cursor cur = null;
            Cursor cursor = null;
            Frag_Suatin.this.error = true;
            if (Frag_Suatin.this.editTsuatin.getText().toString().length() < 6) {
                Frag_Suatin.this.error = false;
            } else if (Frag_Suatin.this.lv_position < 0 || !Congthuc.CheckDate(MainActivity.myDate)) {
                Frag_Suatin.this.error = false;
                if (!Congthuc.CheckDate(MainActivity.myDate)) {
                    try {
                        AlertDialog.Builder bui = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                        bui.setTitle("Thông báo:");
                        bui.setMessage("Đã hết hạn sử dụng phần mềm\n\nHãy liên hệ đại lý hoặc SĐT: " + MainActivity.listKH.getString("k_tra") + " để gia hạn");
                        bui.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.14.2
                            @Override // android.content.DialogInterface.OnClickListener
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        bui.create().show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (MainActivity.Acc_manager.length() == 0) {
                    AlertDialog.Builder bui2 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                    bui2.setTitle("Thông báo:");
                    bui2.setMessage("Kiểm tra kết nối Internet!");
                    bui2.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.14.1
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    bui2.create().show();
                } else {
                    Frag_Suatin.this.Add_tin();
                }
            } else {
                String S = "Update tbl_tinnhanS Set nd_phantich = '" + Frag_Suatin.this.editTsuatin.getText().toString() + "', nd_sua = '" + Frag_Suatin.this.editTsuatin.getText().toString() + "' WHERE id = " + Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position);
                Frag_Suatin.this.db.QueryData(S);
                cur = Frag_Suatin.this.db.GetData("Select type_kh From tbl_tinnhanS WHERE id = " + Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position));
                cur.moveToFirst();
                try {
                    Frag_Suatin.this.db.Update_TinNhanGoc(Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position).intValue(), cur.getInt(0));
                } catch (Exception e2) {
                    Frag_Suatin.this.db.QueryData("Update tbl_tinnhanS set phat_hien_loi = 'ko' WHERE id = " + Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position));
                    String str = "Delete From tbl_soctS WHERE ngay_nhan = '" + Frag_Suatin.this.mNgay.get(Frag_Suatin.this.lv_position) + "' AND so_dienthoai = '" + Frag_Suatin.this.mSDT.get(Frag_Suatin.this.lv_position) + "' AND so_tin_nhan = " + Frag_Suatin.this.mSoTinNhan.get(Frag_Suatin.this.lv_position) + " AND type_kh = " + cur.getString(0);
                    Frag_Suatin.this.db.QueryData(str);
                    Frag_Suatin.this.error = false;
                    Toast.makeText(Frag_Suatin.this.getActivity(), "Đã xảy ra lỗi!", 1).show();
                }
                if (!Congthuc.CheckTime("18:30") && Frag_Suatin.this.Cur_date.contains(Frag_Suatin.this.CurDate)) {
                    Frag_Suatin.this.db.Gui_Tin_Nhan(Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position).intValue());
                }
                cursor = Frag_Suatin.this.db.GetData("Select * FROM tbl_tinnhanS Where id = " + Frag_Suatin.this.mID.get(Frag_Suatin.this.lv_position));
                cursor.moveToFirst();
                if (cursor.getString(11).indexOf("Không hiểu") > -1) {
                    String str1 = cursor.getString(10).replace("ldpro", "<font color='#FF0000'>");
                    Frag_Suatin.this.editTsuatin.setText(Html.fromHtml(str1));
                    if (cursor.getString(10).indexOf("ldpro") > -1) {
                        try {
                            Frag_Suatin.this.editTsuatin.setSelection(str1.indexOf("<font"));
                        } catch (Exception e3) {
                        }
                    }
                    Frag_Suatin.this.error = false;
                } else {
                    Frag_Suatin.this.editTsuatin.setText("");
                    Frag_Suatin.this.xem_lv();
                    if (Frag_Suatin.this.mND_DaSua.size() > 0) {
                        Frag_Suatin.this.lv_position = 0;
                        if (Frag_Suatin.this.mPhatHienLoi.get(0).indexOf("Không hiểu") > -1) {
                            String NoiDung = Frag_Suatin.this.mND_PhanTich.get(0).replace("ldpro", "<font color='#FF0000'>");
                            Frag_Suatin.this.editTsuatin.setText(Html.fromHtml(NoiDung));
                            int KKK = Frag_Suatin.this.mND_PhanTich.get(0).indexOf("ldpro");
                            if (KKK > -1) {
                                try {
                                    Frag_Suatin.this.editTsuatin.setSelection(KKK);
                                } catch (Exception e4) {
                                }
                            }
                            int a = Frag_Suatin.this.mContact.indexOf(Frag_Suatin.this.mTenKH.get(0));
                            Frag_Suatin.this.sp_TenKH.setSelection(a);
                            Frag_Suatin.this.error = false;
                        } else {
                            Frag_Suatin.this.editTsuatin.setText(Frag_Suatin.this.mND_DaSua.get(0));
                        }
                    } else {
                        Frag_Suatin.this.lv_position = -1;
                        Frag_Suatin.this.error = false;
                    }
                }
            }
            if (cur != null) {
                cur.close();
            }
            if (cursor != null) {
                cursor.close();
            }
            if (!Frag_Suatin.this.error) {
                Frag_Suatin.this.handler.removeCallbacks(Frag_Suatin.this.xulyTinnhan);
            } else {
                Frag_Suatin.this.handler.postDelayed(this, 300L);
            }
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_suatin, container, false);
        this.db = new Database(getActivity());
        this.btn_suatin = (Button) this.v.findViewById(R.id.btn_suatin_suatin);
        this.btn_LoadTin = (Button) this.v.findViewById(R.id.btn_loadtin);
        this.editTsuatin = (EditText) this.v.findViewById(R.id.editText_suatin);
        this.btn_tai_All = (Button) this.v.findViewById(R.id.btn_tai_All);
        this.sp_TenKH = (Spinner) this.v.findViewById(R.id.spr_KH);
        this.radio_SuaTin = (RadioButton) this.v.findViewById(R.id.radio_suaTin);
        this.radio_TaiTin = (RadioButton) this.v.findViewById(R.id.radio_TaiTin);
        this.lv_suatin = (ListView) this.v.findViewById(R.id.lv_suatin);
        Handler handler = new Handler();
        this.handler = handler;
        handler.postDelayed(this.runnable, 1000L);
        new MainActivity();
        final String mDate = MainActivity.Get_date();
        this.btn_suatin.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                new MainActivity();
                Frag_Suatin.this.CurDate = MainActivity.Get_date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Frag_Suatin.this.Cur_date = sdf.format(new Date());
                Frag_Suatin.this.handler = new Handler();
                Frag_Suatin.this.handler.postDelayed(Frag_Suatin.this.xulyTinnhan, 300L);
            }
        });
        this.lv_suatin.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.2
            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Frag_Suatin.this.lv_position = i;
                String NoiDung = Frag_Suatin.this.mND_PhanTich.get(i).replace("ldpro", "<font color='#FF0000'>");
                Frag_Suatin.this.editTsuatin.setText(Html.fromHtml(NoiDung));
                int KKK = Frag_Suatin.this.mND_PhanTich.get(i).indexOf("ldpro");
                if (KKK > -1) {
                    try {
                        Frag_Suatin.this.editTsuatin.setSelection(KKK);
                    } catch (Exception e) {
                    }
                }
                int a = Frag_Suatin.this.mContact.indexOf(Frag_Suatin.this.mTenKH.get(i));
                Frag_Suatin.this.sp_TenKH.setSelection(a);
            }
        });
        this.lv_suatin.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.3
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Frag_Suatin.this.lv_position = position;
                return false;
            }
        });
        this.mContact.clear();
        this.mMobile.clear();
        this.mType_kh.clear();
        this.mApp.clear();
        try {
            Cursor cursor = this.db.GetData("Select * From tbl_kh_new Order by type_kh, ten_kh");
            while (cursor.moveToNext()) {
                this.mContact.add(cursor.getString(0));
                this.mMobile.add(cursor.getString(1));
                this.mApp.add(cursor.getString(2));
                this.mType_kh.add(Integer.valueOf(cursor.getInt(3)));
            }
            if (cursor != null) {
                cursor.close();
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), (int) R.layout.spinner_item, this.mContact);
            this.sp_TenKH.setAdapter((SpinnerAdapter) adapter);
            if (this.mContact.size() > 0) {
                this.sp_TenKH.setSelection(0);
            }
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Đang copy dữ liệu bản mới!", 1).show();
        }
        this.sp_TenKH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.4
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Frag_Suatin.this.spin_pointion = position;
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        this.radio_SuaTin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.5
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Frag_Suatin.this.control_RadioButton();
            }
        });
        this.radio_TaiTin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Frag_Suatin.this.control_RadioButton();
            }
        });
        this.btn_LoadTin.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.7
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                if (Frag_Suatin.this.spin_pointion <= -1 || Frag_Suatin.this.mContact.size() <= 0) {
                    Toast.makeText(Frag_Suatin.this.getActivity(), "Chưa có tên khách hàng!", 1).show();
                } else if (Frag_Suatin.this.mApp.get(Frag_Suatin.this.spin_pointion).toString().indexOf("sms") > -1) {
                    AlertDialog.Builder bui = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                    bui.setTitle("Tải lại tin nhắn khách này?");
                    bui.setPositiveButton("YES", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.7.1
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                Frag_Suatin.this.getFullSms(Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion));
                                Database database = Frag_Suatin.this.db;
                                database.QueryData("Update chat_database set del_sms = 1 WHERE ten_kh = '" + Frag_Suatin.this.mContact.get(Frag_Suatin.this.spin_pointion) + "' AND ngay_nhan = '" + mDate + "'");
                            } catch (ParseException e2) {
                                e2.printStackTrace();
                            }
                        }
                    });
                    bui.setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.7.2
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    bui.create().show();
                } else {
                    AlertDialog.Builder bui2 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                    bui2.setTitle("Tải lại tin nhắn khách này?");
                    bui2.setPositiveButton("YES", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.7.3
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            Frag_Suatin.this.getAllChat(Frag_Suatin.this.mType_kh.get(Frag_Suatin.this.spin_pointion).intValue());
                            Database database = Frag_Suatin.this.db;
                            database.QueryData("Update chat_database set del_sms = 1 WHERE ten_kh = '" + Frag_Suatin.this.mContact.get(Frag_Suatin.this.spin_pointion) + "' AND ngay_nhan = '" + mDate + "'");
                        }
                    });
                    bui2.setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.7.4
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    bui2.create().show();
                }
            }
        });
        this.btn_tai_All.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.8
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                AlertDialog.Builder bui = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                bui.setTitle("Tải lại tin nhắn của tất cả khách?");
                bui.setPositiveButton("YES", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.8.1
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Frag_Suatin.this.getFullSms("Full");
                            Database database = Frag_Suatin.this.db;
                            database.QueryData("Update chat_database set del_sms = 1 WHERE ngay_nhan = '" + mDate + "'");
                        } catch (ParseException e2) {
                            e2.printStackTrace();
                        }
                    }
                });
                bui.setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.8.2
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                bui.create().show();
            }
        });
        if (ContextCompat.checkSelfPermission(getContext(), "android.permission.READ_SMS") != 0) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), "android.permission.READ_SMS")) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.READ_SMS"}, 1);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{"android.permission.READ_SMS"}, 1);
            }
        }
        control_RadioButton();
        registerForContextMenu(this.lv_suatin);
        return this.v;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void Add_tin() {
        final MainActivity activity = new MainActivity();
        if (this.mContact.size() > 0 && this.editTsuatin.getText().toString().length() > 6) {
            new MainActivity();
            final String mDate = MainActivity.Get_date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            hourFormat.setTimeZone(TimeZone.getDefault());
            final String mGionhan = hourFormat.format(calendar.getTime());
            String str = "Select * From tbl_tinnhanS WHERE nd_goc = '" + this.editTsuatin.getText().toString().replaceAll("'", "").trim() + "' AND ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "'";
            this.str = str;
            Cursor Ktratin = this.db.GetData(str);
            Ktratin.moveToFirst();
            final Cursor cur1 = this.db.GetData("Select * From tbl_kh_new Where sdt = '" + this.mMobile.get(this.spin_pointion) + "'");
            cur1.moveToFirst();
            if (this.spin_pointion <= -1 || Ktratin.getCount() != 0) {
                if (Ktratin.getCount() > 0) {
                    Toast.makeText(getActivity(), "Đã có tin này trong CSDL!", 1).show();
                } else {
                    Toast.makeText(getActivity(), "Hãy chọn tên khách hàng", 1).show();
                }
            } else if (cur1.getInt(3) == 3) {
                AlertDialog.Builder bui = new AlertDialog.Builder(getActivity());
                bui.setTitle("Chọn loại tin nhắn:");
                bui.setMessage("Đây là khách vừa nhận vừa chuyển, thêm tin nhận hay tin chuyển?");
                bui.setNeutralButton("Tin nhận", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.9
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int which) {
                        Frag_Suatin.this.type_kh = 1;
                        String sql1 = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND type_kh = " + Frag_Suatin.this.type_kh;
                        Cursor getSoTN = Frag_Suatin.this.db.GetData(sql1);
                        getSoTN.moveToFirst();
                        String S = "Insert Into tbl_tinnhanS values (null, '" + mDate + "', '" + mGionhan + "', " + Frag_Suatin.this.type_kh + ", '" + Frag_Suatin.this.mContact.get(Frag_Suatin.this.spin_pointion) + "', '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "', '" + cur1.getString(2) + "', " + (getSoTN.getInt(0) + 1) + ", '" + Frag_Suatin.this.editTsuatin.getText().toString().replace("'", " ").trim() + "', '" + Frag_Suatin.this.editTsuatin.getText().toString().replace("'", " ").trim() + "', '" + Frag_Suatin.this.editTsuatin.getText().toString().replace("'", " ").trim() + "', 'ko',0, 0, 0, null)";
                        Frag_Suatin.this.db.QueryData(S);
                        Frag_Suatin.this.editTsuatin.setText("");
                        Cursor c = Frag_Suatin.this.db.GetData("Select id From tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + Frag_Suatin.this.type_kh);
                        c.moveToFirst();
                        if (Congthuc.CheckDate(MainActivity.myDate)) {
                            try {
                                Frag_Suatin.this.db.Update_TinNhanGoc(c.getInt(0), cur1.getInt(3));
                            } catch (Exception e) {
                                Frag_Suatin.this.db.QueryData("Update tbl_tinnhanS set phat_hien_loi = 'ko' WHERE id = " + c.getInt(0));
                                Frag_Suatin.this.str = "Delete From tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + Frag_Suatin.this.type_kh;
                                Frag_Suatin.this.db.QueryData(Frag_Suatin.this.str);
                            }
                        } else if (MainActivity.Acc_manager.length() == 0) {
                            AlertDialog.Builder bui2 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                            bui2.setTitle("Thông báo:");
                            bui2.setMessage("Kiểm tra kết nối Internet!");
                            bui2.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.9.1
                                @Override // android.content.DialogInterface.OnClickListener
                                public void onClick(DialogInterface dialog2, int which2) {
                                    dialog2.cancel();
                                }
                            });
                            bui2.create().show();
                        } else {
                            try {
                                AlertDialog.Builder bui3 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                                bui3.setTitle("Thông báo:");
                                bui3.setMessage("Đã hết hạn sử dụng phần mềm\n\nHãy liên hệ đại lý hoặc SĐT: " + MainActivity.listKH.getString("k_tra") + " để gia hạn");
                                bui3.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.9.2
                                    @Override // android.content.DialogInterface.OnClickListener
                                    public void onClick(DialogInterface dialog2, int which2) {
                                        dialog2.cancel();
                                    }
                                });
                                bui3.create().show();
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                            }
                        }
                        getSoTN.close();
                        cur1.close();
                        c.close();
                        dialog.cancel();
                        MainActivity.sms = true;
                    }
                });
                bui.setPositiveButton("Tin Chuyển", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.10
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int which) {
                        Frag_Suatin.this.type_kh = 2;
                        String sql1 = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND type_kh = " + Frag_Suatin.this.type_kh;
                        Cursor getSoTN = Frag_Suatin.this.db.GetData(sql1);
                        getSoTN.moveToFirst();
                        String S = "Insert Into tbl_tinnhanS values (null, '" + mDate + "', '" + mGionhan + "', " + Frag_Suatin.this.type_kh + ", '" + Frag_Suatin.this.mContact.get(Frag_Suatin.this.spin_pointion) + "', '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "', '" + cur1.getString(2) + "', " + (getSoTN.getInt(0) + 1) + ", '" + Frag_Suatin.this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', '" + Frag_Suatin.this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', '" + Frag_Suatin.this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', 'ko',0, 0, 0, null)";
                        Frag_Suatin.this.db.QueryData(S);
                        Frag_Suatin.this.editTsuatin.setText("");
                        Cursor c = Frag_Suatin.this.db.GetData("Select id From tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + Frag_Suatin.this.type_kh);
                        c.moveToFirst();
                        if (Congthuc.CheckDate(MainActivity.myDate)) {
                            try {
                                Frag_Suatin.this.db.Update_TinNhanGoc(c.getInt(0), cur1.getInt(3));
                            } catch (Exception e) {
                                Frag_Suatin.this.db.QueryData("Update tbl_tinnhanS set phat_hien_loi = 'ko' WHERE id = " + c.getInt(0));
                                Frag_Suatin.this.str = "Delete From tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + Frag_Suatin.this.mMobile.get(Frag_Suatin.this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + Frag_Suatin.this.type_kh;
                                Frag_Suatin.this.db.QueryData(Frag_Suatin.this.str);
                                Toast.makeText(Frag_Suatin.this.getActivity(), "Đã xảy ra lỗi!", 1).show();
                            }
                        } else if (MainActivity.Acc_manager.length() == 0) {
                            AlertDialog.Builder bui2 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                            bui2.setTitle("Thông báo:");
                            bui2.setMessage("Kiểm tra kết nối Internet!");
                            bui2.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.10.1
                                @Override // android.content.DialogInterface.OnClickListener
                                public void onClick(DialogInterface dialog2, int which2) {
                                    dialog2.cancel();
                                }
                            });
                            bui2.create().show();
                        } else {
                            try {
                                AlertDialog.Builder bui3 = new AlertDialog.Builder(Frag_Suatin.this.getActivity());
                                bui3.setTitle("Thông báo:");
                                bui3.setMessage("Đã hết hạn sử dụng phần mềm\n\nHãy liên hệ đại lý hoặc SĐT: " + MainActivity.listKH.getString("k_tra") + " để gia hạn");
                                bui3.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.10.2
                                    @Override // android.content.DialogInterface.OnClickListener
                                    public void onClick(DialogInterface dialog2, int which2) {
                                        dialog2.cancel();
                                    }
                                });
                                bui3.create().show();
                            } catch (JSONException e2) {
                                e2.printStackTrace();
                            }
                        }
                        getSoTN.close();
                        cur1.close();
                        c.close();
                        dialog.cancel();
                        MainActivity.sms = true;
                    }
                });
                bui.create().show();
            } else {
                this.type_kh = cur1.getInt(3);
                String sql1 = "Select max(so_tin_nhan) from tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "' AND type_kh = " + this.type_kh;
                Cursor getSoTN = this.db.GetData(sql1);
                getSoTN.moveToFirst();
                String S = "Insert Into tbl_tinnhanS values (null, '" + mDate + "', '" + mGionhan + "', " + this.type_kh + ", '" + this.mContact.get(this.spin_pointion) + "', '" + this.mMobile.get(this.spin_pointion) + "', '" + cur1.getString(2) + "', " + (getSoTN.getInt(0) + 1) + ", '" + this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', '" + this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', '" + this.editTsuatin.getText().toString().replaceAll("'", " ").trim() + "', 'ko',0, 0, 0, null)";
                this.db.QueryData(S);
                this.editTsuatin.setText("");
                Cursor c = this.db.GetData("Select id From tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + this.type_kh);
                c.moveToFirst();
                if (Congthuc.CheckDate(MainActivity.myDate)) {
                    try {
                        this.db.Update_TinNhanGoc(c.getInt(0), cur1.getInt(3));
                    } catch (Exception e) {
                        this.db.QueryData("Update tbl_tinnhanS set phat_hien_loi = 'ko' WHERE id = " + c.getInt(0));
                        String str2 = "Delete From tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "' AND so_tin_nhan = " + (getSoTN.getInt(0) + 1) + " AND type_kh = " + this.type_kh;
                        this.str = str2;
                        this.db.QueryData(str2);
                        Toast.makeText(getActivity(), "Đã xảy ra lỗi!", 1).show();
                    }
                } else if (MainActivity.Acc_manager.length() == 0) {
                    AlertDialog.Builder bui2 = new AlertDialog.Builder(getActivity());
                    bui2.setTitle("Thông báo:");
                    bui2.setMessage("Kiểm tra kết nối Internet!");
                    bui2.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.11
                        @Override // android.content.DialogInterface.OnClickListener
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    bui2.create().show();
                } else {
                    try {
                        AlertDialog.Builder bui3 = new AlertDialog.Builder(getActivity());
                        bui3.setTitle("Thông báo:");
                        bui3.setMessage("Đã hết hạn sử dụng phần mềm\n\nHãy liên hệ đại lý hoặc SĐT: " + MainActivity.listKH.getString("k_tra") + " để gia hạn");
                        bui3.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Suatin.12
                            @Override // android.content.DialogInterface.OnClickListener
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        bui3.create().show();
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                }
                MainActivity.sms = true;
                getSoTN.close();
                cur1.close();
                c.close();
            }
            xem_lv();
            if (Ktratin != null) {
                Ktratin.close();
            }
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.handler.removeCallbacks(this.runnable);
    }

    @Override // android.support.v4.app.Fragment, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 1, 0, "Xóa tin này?");
    }

    @Override // android.support.v4.app.Fragment
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        if (item.getItemId() == 1) {
            if (this.lv_position >= 0) {
                Database database = this.db;
                database.QueryData("Delete FROM tbl_tinnhanS WHERE id = " + this.mID.get(this.lv_position));
                this.lv_position = -1;
                xem_lv();
                Toast.makeText(getActivity(), "Xoá thành công", 1).show();
                this.editTsuatin.setText("");
            }
            xem_lv();
            if (this.mND_DaSua.size() > 0) {
                this.lv_position = 0;
                if (this.mPhatHienLoi.get(0).indexOf("Không hiểu") > -1) {
                    String NoiDung = this.mND_PhanTich.get(0).replace("ldpro", "<font color='#FF0000'>");
                    this.editTsuatin.setText(Html.fromHtml(NoiDung));
                    int KKK = this.mND_PhanTich.get(0).indexOf("ldpro");
                    if (KKK > -1) {
                        try {
                            this.editTsuatin.setSelection(KKK);
                        } catch (Exception e) {
                        }
                    }
                    int a = this.mContact.indexOf(this.mTenKH.get(0));
                    this.sp_TenKH.setSelection(a);
                    this.error = false;
                } else {
                    this.editTsuatin.setText(this.mND_DaSua.get(0));
                }
            } else {
                this.lv_position = -1;
                this.error = false;
            }
        }
        if (item.getItemId() == 2) {
            if (this.lv_position >= 0) {
                new MainActivity();
                String mDate = MainActivity.Get_date();
                Database database2 = this.db;
                database2.QueryData("Delete FROM tbl_tinnhanS WHERE phat_hien_loi <> 'ok' And ngay_nhan = '" + mDate + "'");
                this.lv_position = -1;
                xem_lv();
                Toast.makeText(getActivity(), "Xoá thành công", 1).show();
                this.editTsuatin.setText("");
            }
            xem_lv();
        }
        return true;
    }

    public void control_RadioButton() {
        LinearLayout li_KhachHang = (LinearLayout) this.v.findViewById(R.id.li_KhachHang);
        LinearLayout li_Button = (LinearLayout) this.v.findViewById(R.id.li_button);
        LinearLayout li_edittinnhan = (LinearLayout) this.v.findViewById(R.id.edittinnhan);
        if (this.radio_SuaTin.isChecked()) {
            li_edittinnhan.setVisibility(0);
            li_KhachHang.setVisibility(0);
            li_Button.setVisibility(0);
            this.btn_LoadTin.setVisibility(8);
            this.btn_suatin.setVisibility(0);
            this.editTsuatin.setVisibility(0);
            this.btn_tai_All.setVisibility(8);
            xem_lv();
        } else if (this.radio_TaiTin.isChecked()) {
            li_edittinnhan.setVisibility(0);
            li_KhachHang.setVisibility(0);
            li_Button.setVisibility(0);
            this.btn_suatin.setVisibility(8);
            this.btn_LoadTin.setVisibility(0);
            this.editTsuatin.setVisibility(8);
            this.btn_tai_All.setVisibility(0);
            xem_lv();
        }
    }

    /* JADX WARN: Finally extract failed */
    public void getAllChat(int Type_kh) {
        String sql;
        new MainActivity();
        String mDate = MainActivity.Get_date();
        int soTN = 0;
        String str = "DELETE FROM tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "'";
        this.db.QueryData(str);
        String str2 = "DELETE FROM tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND so_dienthoai = '" + this.mMobile.get(this.spin_pointion) + "'";
        this.db.QueryData(str2);
        Cursor cur1 = this.db.GetData("Select * From tbl_kh_new Where sdt = '" + this.mMobile.get(this.spin_pointion) + "'");
        cur1.moveToFirst();
        int i = 3;
        if (Type_kh == 1) {
            String sql2 = "Select * From Chat_database Where ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.mContact.get(this.spin_pointion) + "' and type_kh = 1";
            sql = sql2;
        } else if (Type_kh == 2) {
            String sql3 = "Select * From Chat_database Where ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.mContact.get(this.spin_pointion) + "' and type_kh = 2";
            sql = sql3;
        } else if (Type_kh == 3) {
            String sql4 = "Select * From Chat_database Where ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.mContact.get(this.spin_pointion) + "'";
            sql = sql4;
        } else {
            sql = null;
        }
        Cursor curSQL = this.db.GetData(sql);
        SQLiteDatabase database = this.db.getWritableDatabase();
        DatabaseUtils.InsertHelper ih = new DatabaseUtils.InsertHelper(database, "tbl_tinnhanS");
        database.beginTransaction();
        try {
            try {
                if (curSQL.getCount() > 0) {
                    while (curSQL.moveToNext()) {
                        soTN++;
                        ih.prepareForInsert();
                        ih.bind(ih.getColumnIndex("ngay_nhan"), mDate);
                        ih.bind(ih.getColumnIndex("gio_nhan"), curSQL.getString(2));
                        ih.bind(ih.getColumnIndex("type_kh"), curSQL.getString(i));
                        ih.bind(ih.getColumnIndex("ten_kh"), this.mContact.get(this.spin_pointion));
                        ih.bind(ih.getColumnIndex("so_dienthoai"), this.mMobile.get(this.spin_pointion));
                        ih.bind(ih.getColumnIndex("use_app"), cur1.getInt(2));
                        ih.bind(ih.getColumnIndex("so_tin_nhan"), soTN);
                        ih.bind(ih.getColumnIndex("nd_goc"), curSQL.getString(7));
                        ih.bind(ih.getColumnIndex("nd_sua"), curSQL.getString(7));
                        ih.bind(ih.getColumnIndex("nd_phantich"), curSQL.getString(7));
                        ih.bind(ih.getColumnIndex("phat_hien_loi"), "ko");
                        ih.bind(ih.getColumnIndex("tinh_tien"), 0);
                        ih.bind(ih.getColumnIndex("ok_tn"), 0);
                        ih.bind(ih.getColumnIndex("del_sms"), 0);
                        ih.execute();
                        i = 3;
                    }
                }
                curSQL.close();
                database.setTransactionSuccessful();
            } catch (Exception e) {
                e.printStackTrace();
            }
            database.endTransaction();
            ih.close();
            database.close();
            this.db.QueryData("Delete From tbl_tinnhanS where substr(nd_goc,0,7) = 'Ok Tin'");
            this.db.QueryData("Delete From tbl_tinnhanS where length(nd_goc) < 4");
            xem_lv();
            Toast.makeText(getActivity(), "Đã tải xong tin nhắn!", 1).show();
            cur1.close();
        } catch (Throwable th) {
            database.endTransaction();
            ih.close();
            database.close();
            throw th;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockProcessor
        jadx.core.utils.exceptions.JadxRuntimeException: Unreachable block: B:164:0x059e
        	at jadx.core.dex.visitors.blocks.BlockProcessor.checkForUnreachableBlocks(BlockProcessor.java:92)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.processBlocksTree(BlockProcessor.java:52)
        	at jadx.core.dex.visitors.blocks.BlockProcessor.visit(BlockProcessor.java:44)
        */
    public void getFullSms(String r46) throws ParseException {
        /*
            Method dump skipped, instructions count: 2250
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Fragment.Frag_Suatin.getFullSms(java.lang.String):void");
    }

    public void xem_lv() {
        new MainActivity();
        String mDate = MainActivity.Get_date();
        this.mID.clear();
        this.mNgay.clear();
        this.mSDT.clear();
        this.mTenKH.clear();
        this.mSoTinNhan.clear();
        this.mTinNhanGoc.clear();
        this.mND_DaSua.clear();
        this.mND_PhanTich.clear();
        this.mPhatHienLoi.clear();
        this.mTypeKH.clear();
        Database database = this.db;
        Cursor cursor = database.GetData("select * from tbl_tinnhanS WHERE phat_hien_loi <> 'ok' AND ngay_nhan = '" + mDate + "'");
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                this.mID.add(Integer.valueOf(cursor.getInt(0)));
                this.mNgay.add(cursor.getString(1));
                this.mTenKH.add(cursor.getString(4));
                this.mSDT.add(cursor.getString(5));
                this.mSoTinNhan.add(Integer.valueOf(Integer.parseInt(cursor.getString(7))));
                this.mTinNhanGoc.add(cursor.getString(8));
                this.mND_DaSua.add(cursor.getString(9));
                this.mND_PhanTich.add(cursor.getString(10));
                this.mPhatHienLoi.add(cursor.getString(11));
                this.mTypeKH.add(Integer.valueOf(cursor.getInt(3)));
            }
            cursor.close();
        }
        if (getActivity() != null) {
            this.lv_suatin.setAdapter((ListAdapter) new TNGAdapter(getActivity(), R.layout.frag_suatin_lv, this.mTinNhanGoc));
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class TNGAdapter extends ArrayAdapter {
        public TNGAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        /* loaded from: classes2.dex */
        class ViewHolder {
            TextView tview5;
            TextView tview7;

            ViewHolder() {
            }
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View mView, ViewGroup parent) {
            ViewHolder holder;
            if (mView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService("layout_inflater");
                mView = inflater.inflate(R.layout.frag_suatin_lv, (ViewGroup) null);
                holder = new ViewHolder();
                holder.tview5 = (TextView) mView.findViewById(R.id.tv_suatin_nd);
                holder.tview7 = (TextView) mView.findViewById(R.id.tv_suatin_err);
                mView.setTag(holder);
            } else {
                holder = (ViewHolder) mView.getTag();
            }
            holder.tview5.setText(Frag_Suatin.this.mTinNhanGoc.get(position));
            holder.tview7.setText(Frag_Suatin.this.mPhatHienLoi.get(position));
            return mView;
        }
    }
}
