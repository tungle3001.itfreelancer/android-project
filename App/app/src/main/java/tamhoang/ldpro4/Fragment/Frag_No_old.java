package tamhoang.ldpro4.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Activity.Activity_khach;
import tamhoang.ldpro4.Congthuc.Congthuc;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.NotificationReader;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_No_old extends Fragment {
    Button btn_nt;
    JSONObject caidat_tg;
    int currentIndex;
    Database db;
    Handler handler;
    JSONObject json;
    List<JSONObject> jsonKhachHang;
    LinearLayout lnDea;
    LinearLayout lnDec;
    LinearLayout lnDed;
    LinearLayout lnDet;
    LinearLayout lnXn;
    LinearLayout ln_bca;
    LinearLayout ln_loa;
    LinearLayout ln_xi2;
    LinearLayout ln_xia2;
    ListView lv_no_tinnhan;
    LayoutInflater mInflate;
    int position;
    private ProgressBar progressBar;
    TextView tvAnBC1;
    TextView tvAnBCa;
    TextView tvAnDea;
    TextView tvAnDeb;
    TextView tvAnDec;
    TextView tvAnDed;
    TextView tvAnDet;
    TextView tvAnLo1;
    TextView tvAnLoa;
    TextView tvAnXi2;
    TextView tvAnXia2;
    TextView tvAnXn1;
    TextView tvDiemBc1;
    TextView tvDiemBca;
    TextView tvDiemDea;
    TextView tvDiemDeb;
    TextView tvDiemDec;
    TextView tvDiemDed;
    TextView tvDiemDet;
    TextView tvDiemLo1;
    TextView tvDiemLoa;
    TextView tvDiemXi2;
    TextView tvDiemXia2;
    TextView tvDiemXn1;
    TextView tvKQBc1;
    TextView tvKQBca;
    TextView tvKQDea;
    TextView tvKQDeb;
    TextView tvKQDec;
    TextView tvKQDed;
    TextView tvKQDet;
    TextView tvKQLo1;
    TextView tvKQLoa;
    TextView tvKQXi2;
    TextView tvKQXia2;
    TextView tvKQXn1;
    TextView tvTongTien1;
    View v;
    private List<String> mTenKH = new ArrayList();
    private List<String> mSDT = new ArrayList();
    private List<String> mTypeKH = new ArrayList();
    boolean isSuccess = false;
    private Runnable runnable = new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.18
        @Override // java.lang.Runnable
        public void run() {
            new MainActivity();
            if (MainActivity.sms) {
                Frag_No_old.this.lv_No2_KH();
                MainActivity.sms = false;
            }
            Frag_No_old.this.handler.postDelayed(this, 1000L);
        }
    };

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.frag_norp2, container, false);
        this.db = new Database(getActivity());
        this.mInflate = inflater;
        istart();
        this.progressBar = (ProgressBar) this.v.findViewById(R.id.progressBar);
        this.lv_no_tinnhan = (ListView) this.v.findViewById(R.id.no_rp2_lv);
        this.btn_nt = (Button) this.v.findViewById(R.id.btn_nt);
        this.lv_no_tinnhan.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.1
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                Frag_No_old.this.position = i;
                Frag_No_old frag_No_old = Frag_No_old.this;
                frag_No_old.Dialog((String) frag_No_old.mTenKH.get(i));
                return false;
            }
        });
        if (!Congthuc.CheckTime("18:30")) {
            Handler handler = new Handler();
            this.handler = handler;
            handler.postDelayed(this.runnable, 1000L);
        }
        lv_No2_KH();
        this.btn_nt.setOnClickListener(new AnonymousClass2());
        return this.v;
    }

    /* renamed from: tamhoang.ldpro4.Fragment.Frag_No_old$2  reason: invalid class name */
    /* loaded from: classes2.dex */
    class AnonymousClass2 implements View.OnClickListener {
        AnonymousClass2() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (Frag_No_old.this.jsonKhachHang.size() >= 1) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Frag_No_old.this.getActivity());
                builder.setMessage("Bạn có muốn nhắn tin chốt tiền tất cả không?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.2.2
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Frag_No_old.this.progressBar.setVisibility(0);
                        Frag_No_old.this.getActivity().getWindow().setFlags(16, 16);
                        for (int i = 0; i < Frag_No_old.this.jsonKhachHang.size(); i++) {
                            Frag_No_old.this.currentIndex = i;
                            Database database = Frag_No_old.this.db;
                            Cursor cursor2 = database.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)) + "'");
                            if (cursor2 != null && cursor2.getCount() > 0) {
                                cursor2.moveToFirst();
                                if (cursor2.getString(2).indexOf("sms") > -1) {
                                    try {
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            Frag_No_old.this.db.SendSMS((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.currentIndex), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        } else {
                                            Frag_No_old.this.db.SendSMS((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.currentIndex), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        }
                                        Frag_No_old.this.isSuccess = true;
                                    } catch (JSONException e) {
                                        Frag_No_old.this.isSuccess = false;
                                        e.printStackTrace();
                                    }
                                } else if (cursor2.getString(2).indexOf("TL") > -1) {
                                    try {
                                        new MainActivity();
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.currentIndex)), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        } else {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.currentIndex)), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        }
                                        Frag_No_old.this.isSuccess = true;
                                        try {
                                            Thread.sleep(1000L);
                                        } catch (InterruptedException e2) {
                                            e2.printStackTrace();
                                        }
                                    } catch (JSONException e3) {
                                        Frag_No_old.this.isSuccess = false;
                                        e3.printStackTrace();
                                    }
                                } else if (MainActivity.arr_TenKH.indexOf(cursor2.getString(1)) > -1) {
                                    NotificationReader notificationReader = new NotificationReader();
                                    try {
                                        if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                            notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        } else {
                                            notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.currentIndex)));
                                        }
                                    } catch (JSONException e4) {
                                        Frag_No_old.this.isSuccess = false;
                                        e4.printStackTrace();
                                    }
                                    Frag_No_old.this.isSuccess = true;
                                } else {
                                    Frag_No_old.this.isSuccess = false;
                                    Toast.makeText(Frag_No_old.this.getActivity(), "Không có người này trong Chatbox", 1).show();
                                }
                            }
                        }
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.postDelayed(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.2.2.1
                            @Override // java.lang.Runnable
                            public void run() {
                                if (Frag_No_old.this.isSuccess) {
                                    Toast.makeText(Frag_No_old.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                                }
                                Frag_No_old.this.progressBar.setVisibility(8);
                                Frag_No_old.this.getActivity().getWindow().clearFlags(16);
                            }
                        }, 2000L);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.2.1
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    public void Dialog(final String tenkh) {
        SeekBar xi_dly;
        SeekBar bc_dly;
        final TextView pt_giu_bc_dly;
        final TextView pt_giu_xi_dly;
        SeekBar xi_khach;
        SeekBar lo_khach;
        final TextView pt_giu_lo_khach;
        SeekBar de_khach;
        final TextView pt_giu_de_khach;
        final TextView pt_giu_xi_khach;
        SeekBar bc_khach;
        final TextView pt_giu_bc_khach;
        JSONException e;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.frag_no_menu);
        new MainActivity();
        final String mDate = MainActivity.Get_date();
        final ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService("clipboard");
        Database database = this.db;
        Cursor cursor = database.GetData("Select * From tbl_kh_new WHERE ten_kh ='" + tenkh + "'");
        cursor.moveToFirst();
        Button Baocaochitiet = (Button) dialog.findViewById(R.id.Baocaochitiet);
        Button tinhlaitien = (Button) dialog.findViewById(R.id.tinhlaitien);
        Button nhanchottien = (Button) dialog.findViewById(R.id.nhanchottien);
        Button copytinchitiet = (Button) dialog.findViewById(R.id.copytinchitiet);
        Button copytinchotien = (Button) dialog.findViewById(R.id.copytinchotien);
        Button xoadulieu = (Button) dialog.findViewById(R.id.xoadulieu);
        final Switch switch1 = (Switch) dialog.findViewById(R.id.switch1);
        final LinearLayout ln2 = (LinearLayout) dialog.findViewById(R.id.ln2);
        SeekBar de_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuDekhach);
        SeekBar lo_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuLokhach);
        SeekBar xi_khach2 = (SeekBar) dialog.findViewById(R.id.seek_GiuXikhach);
        SeekBar bc_khach2 = (SeekBar) dialog.findViewById(R.id.seek_Giu3ckhach);
        SeekBar de_dly = (SeekBar) dialog.findViewById(R.id.seek_GiuDedly);
        SeekBar lo_dly = (SeekBar) dialog.findViewById(R.id.seek_GiuLodly);
        SeekBar xi_dly2 = (SeekBar) dialog.findViewById(R.id.seek_GiuXidly);
        SeekBar bc_dly2 = (SeekBar) dialog.findViewById(R.id.seek_Giu3cdly);
        TextView pt_giu_de_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_de_khach);
        TextView pt_giu_lo_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_lo_khach);
        TextView pt_giu_xi_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_xi_khach);
        TextView pt_giu_bc_khach2 = (TextView) dialog.findViewById(R.id.pt_giu_bc_khach);
        final TextView pt_giu_de_dly = (TextView) dialog.findViewById(R.id.pt_giu_de_dly);
        final TextView pt_giu_lo_dly = (TextView) dialog.findViewById(R.id.pt_giu_lo_dly);
        TextView pt_giu_xi_dly2 = (TextView) dialog.findViewById(R.id.pt_giu_xi_dly);
        TextView pt_giu_bc_dly2 = (TextView) dialog.findViewById(R.id.pt_giu_bc_dly);
        try {
            bc_dly = bc_dly2;
            xi_dly = xi_dly2;
            try {
                JSONObject jSONObject = new JSONObject(cursor.getString(5));
                this.json = jSONObject;
                this.caidat_tg = jSONObject.getJSONObject("caidat_tg");
                pt_giu_de_dly.setText(this.caidat_tg.getInt("dlgiu_de") + "%");
                pt_giu_lo_dly.setText(this.caidat_tg.getInt("dlgiu_lo") + "%");
                pt_giu_xi_dly2.setText(this.caidat_tg.getInt("dlgiu_xi") + "%");
                pt_giu_bc_dly2.setText(this.caidat_tg.getInt("dlgiu_bc") + "%");
                de_dly.setProgress(this.caidat_tg.getInt("dlgiu_de") / 5);
                lo_dly.setProgress(this.caidat_tg.getInt("dlgiu_lo") / 5);
                try {
                    xi_dly.setProgress(this.caidat_tg.getInt("dlgiu_xi") / 5);
                    try {
                        bc_dly.setProgress(this.caidat_tg.getInt("dlgiu_bc") / 5);
                        pt_giu_de_khach = pt_giu_de_khach2;
                        try {
                            pt_giu_de_khach.setText(this.caidat_tg.getInt("khgiu_de") + "%");
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            try {
                                pt_giu_lo_khach.setText(this.caidat_tg.getInt("khgiu_lo") + "%");
                                StringBuilder sb = new StringBuilder();
                                bc_dly = bc_dly;
                                try {
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    try {
                                        sb.append(this.caidat_tg.getInt("khgiu_xi"));
                                        sb.append("%");
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        try {
                                            pt_giu_xi_khach.setText(sb.toString());
                                            StringBuilder sb2 = new StringBuilder();
                                            xi_dly = xi_dly;
                                            try {
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                try {
                                                    sb2.append(this.caidat_tg.getInt("khgiu_bc"));
                                                    sb2.append("%");
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                    try {
                                                        pt_giu_bc_khach.setText(sb2.toString());
                                                        de_khach = de_khach2;
                                                        try {
                                                            de_khach.setProgress(this.caidat_tg.getInt("khgiu_de") / 5);
                                                            lo_khach = lo_khach2;
                                                            try {
                                                                lo_khach.setProgress(this.caidat_tg.getInt("khgiu_lo") / 5);
                                                                xi_khach = xi_khach2;
                                                                try {
                                                                    xi_khach.setProgress(this.caidat_tg.getInt("khgiu_xi") / 5);
                                                                    bc_khach = bc_khach2;
                                                                    try {
                                                                        bc_khach.setProgress(this.caidat_tg.getInt("khgiu_bc") / 5);
                                                                        cursor.close();
                                                                    } catch (JSONException e2) {
                                                                        e = e2;
                                                                        e.printStackTrace();
                                                                    } catch (Exception e3) {
                                                                    }
                                                                } catch (JSONException e4) {
                                                                    e = e4;
                                                                    bc_khach = bc_khach2;
                                                                } catch (Exception e5) {
                                                                    bc_khach = bc_khach2;
                                                                }
                                                            } catch (JSONException e6) {
                                                                e = e6;
                                                                bc_khach = bc_khach2;
                                                                xi_khach = xi_khach2;
                                                            } catch (Exception e7) {
                                                                bc_khach = bc_khach2;
                                                                xi_khach = xi_khach2;
                                                            }
                                                        } catch (JSONException e8) {
                                                            e = e8;
                                                            bc_khach = bc_khach2;
                                                            xi_khach = xi_khach2;
                                                            lo_khach = lo_khach2;
                                                        } catch (Exception e9) {
                                                            bc_khach = bc_khach2;
                                                            xi_khach = xi_khach2;
                                                            lo_khach = lo_khach2;
                                                        }
                                                    } catch (JSONException e10) {
                                                        e = e10;
                                                        bc_khach = bc_khach2;
                                                        xi_khach = xi_khach2;
                                                        lo_khach = lo_khach2;
                                                        de_khach = de_khach2;
                                                    } catch (Exception e11) {
                                                        bc_khach = bc_khach2;
                                                        xi_khach = xi_khach2;
                                                        lo_khach = lo_khach2;
                                                        de_khach = de_khach2;
                                                    }
                                                } catch (JSONException e12) {
                                                    e = e12;
                                                    bc_khach = bc_khach2;
                                                    xi_khach = xi_khach2;
                                                    lo_khach = lo_khach2;
                                                    de_khach = de_khach2;
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                } catch (Exception e13) {
                                                    bc_khach = bc_khach2;
                                                    xi_khach = xi_khach2;
                                                    lo_khach = lo_khach2;
                                                    de_khach = de_khach2;
                                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                                }
                                            } catch (JSONException e14) {
                                                e = e14;
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                bc_khach = bc_khach2;
                                                xi_khach = xi_khach2;
                                                lo_khach = lo_khach2;
                                                de_khach = de_khach2;
                                                pt_giu_bc_khach = pt_giu_bc_khach2;
                                            } catch (Exception e15) {
                                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                                bc_khach = bc_khach2;
                                                xi_khach = xi_khach2;
                                                lo_khach = lo_khach2;
                                                de_khach = de_khach2;
                                                pt_giu_bc_khach = pt_giu_bc_khach2;
                                            }
                                        } catch (JSONException e16) {
                                            e = e16;
                                            xi_dly = xi_dly;
                                            pt_giu_xi_dly = pt_giu_xi_dly2;
                                            bc_khach = bc_khach2;
                                            xi_khach = xi_khach2;
                                            lo_khach = lo_khach2;
                                            de_khach = de_khach2;
                                            pt_giu_bc_khach = pt_giu_bc_khach2;
                                        } catch (Exception e17) {
                                            xi_dly = xi_dly;
                                            pt_giu_xi_dly = pt_giu_xi_dly2;
                                            bc_khach = bc_khach2;
                                            xi_khach = xi_khach2;
                                            lo_khach = lo_khach2;
                                            de_khach = de_khach2;
                                            pt_giu_bc_khach = pt_giu_bc_khach2;
                                        }
                                    } catch (JSONException e18) {
                                        e = e18;
                                        xi_dly = xi_dly;
                                        pt_giu_xi_dly = pt_giu_xi_dly2;
                                        bc_khach = bc_khach2;
                                        xi_khach = xi_khach2;
                                        lo_khach = lo_khach2;
                                        de_khach = de_khach2;
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        pt_giu_bc_khach = pt_giu_bc_khach2;
                                    } catch (Exception e19) {
                                        xi_dly = xi_dly;
                                        pt_giu_xi_dly = pt_giu_xi_dly2;
                                        bc_khach = bc_khach2;
                                        xi_khach = xi_khach2;
                                        lo_khach = lo_khach2;
                                        de_khach = de_khach2;
                                        pt_giu_xi_khach = pt_giu_xi_khach2;
                                        pt_giu_bc_khach = pt_giu_bc_khach2;
                                    }
                                } catch (JSONException e20) {
                                    e = e20;
                                    xi_dly = xi_dly;
                                    pt_giu_xi_dly = pt_giu_xi_dly2;
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    bc_khach = bc_khach2;
                                    xi_khach = xi_khach2;
                                    lo_khach = lo_khach2;
                                    de_khach = de_khach2;
                                    pt_giu_xi_khach = pt_giu_xi_khach2;
                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                } catch (Exception e21) {
                                    xi_dly = xi_dly;
                                    pt_giu_xi_dly = pt_giu_xi_dly2;
                                    pt_giu_bc_dly = pt_giu_bc_dly2;
                                    bc_khach = bc_khach2;
                                    xi_khach = xi_khach2;
                                    lo_khach = lo_khach2;
                                    de_khach = de_khach2;
                                    pt_giu_xi_khach = pt_giu_xi_khach2;
                                    pt_giu_bc_khach = pt_giu_bc_khach2;
                                }
                            } catch (JSONException e22) {
                                e = e22;
                                xi_dly = xi_dly;
                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                bc_dly = bc_dly;
                                pt_giu_bc_dly = pt_giu_bc_dly2;
                                bc_khach = bc_khach2;
                                xi_khach = xi_khach2;
                                lo_khach = lo_khach2;
                                de_khach = de_khach2;
                                pt_giu_xi_khach = pt_giu_xi_khach2;
                                pt_giu_bc_khach = pt_giu_bc_khach2;
                            } catch (Exception e23) {
                                xi_dly = xi_dly;
                                pt_giu_xi_dly = pt_giu_xi_dly2;
                                bc_dly = bc_dly;
                                pt_giu_bc_dly = pt_giu_bc_dly2;
                                bc_khach = bc_khach2;
                                xi_khach = xi_khach2;
                                lo_khach = lo_khach2;
                                de_khach = de_khach2;
                                pt_giu_xi_khach = pt_giu_xi_khach2;
                                pt_giu_bc_khach = pt_giu_bc_khach2;
                            }
                        } catch (JSONException e24) {
                            e = e24;
                            xi_dly = xi_dly;
                            pt_giu_xi_dly = pt_giu_xi_dly2;
                            bc_dly = bc_dly;
                            pt_giu_bc_dly = pt_giu_bc_dly2;
                            bc_khach = bc_khach2;
                            xi_khach = xi_khach2;
                            lo_khach = lo_khach2;
                            de_khach = de_khach2;
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            pt_giu_xi_khach = pt_giu_xi_khach2;
                            pt_giu_bc_khach = pt_giu_bc_khach2;
                        } catch (Exception e25) {
                            xi_dly = xi_dly;
                            pt_giu_xi_dly = pt_giu_xi_dly2;
                            bc_dly = bc_dly;
                            pt_giu_bc_dly = pt_giu_bc_dly2;
                            bc_khach = bc_khach2;
                            xi_khach = xi_khach2;
                            lo_khach = lo_khach2;
                            de_khach = de_khach2;
                            pt_giu_lo_khach = pt_giu_lo_khach2;
                            pt_giu_xi_khach = pt_giu_xi_khach2;
                            pt_giu_bc_khach = pt_giu_bc_khach2;
                        }
                    } catch (JSONException e26) {
                        e = e26;
                        xi_dly = xi_dly;
                        pt_giu_xi_dly = pt_giu_xi_dly2;
                        bc_dly = bc_dly;
                        pt_giu_bc_dly = pt_giu_bc_dly2;
                        bc_khach = bc_khach2;
                        xi_khach = xi_khach2;
                        lo_khach = lo_khach2;
                        de_khach = de_khach2;
                        pt_giu_lo_khach = pt_giu_lo_khach2;
                        pt_giu_xi_khach = pt_giu_xi_khach2;
                        pt_giu_bc_khach = pt_giu_bc_khach2;
                        pt_giu_de_khach = pt_giu_de_khach2;
                    } catch (Exception e27) {
                        xi_dly = xi_dly;
                        pt_giu_xi_dly = pt_giu_xi_dly2;
                        bc_dly = bc_dly;
                        pt_giu_bc_dly = pt_giu_bc_dly2;
                        bc_khach = bc_khach2;
                        xi_khach = xi_khach2;
                        lo_khach = lo_khach2;
                        de_khach = de_khach2;
                        pt_giu_lo_khach = pt_giu_lo_khach2;
                        pt_giu_xi_khach = pt_giu_xi_khach2;
                        pt_giu_bc_khach = pt_giu_bc_khach2;
                        pt_giu_de_khach = pt_giu_de_khach2;
                    }
                } catch (JSONException e28) {
                    e = e28;
                    xi_dly = xi_dly;
                    pt_giu_xi_dly = pt_giu_xi_dly2;
                    pt_giu_bc_dly = pt_giu_bc_dly2;
                    bc_khach = bc_khach2;
                    xi_khach = xi_khach2;
                    lo_khach = lo_khach2;
                    de_khach = de_khach2;
                    pt_giu_lo_khach = pt_giu_lo_khach2;
                    pt_giu_xi_khach = pt_giu_xi_khach2;
                    pt_giu_bc_khach = pt_giu_bc_khach2;
                    pt_giu_de_khach = pt_giu_de_khach2;
                } catch (Exception e29) {
                    xi_dly = xi_dly;
                    pt_giu_xi_dly = pt_giu_xi_dly2;
                    pt_giu_bc_dly = pt_giu_bc_dly2;
                    bc_khach = bc_khach2;
                    xi_khach = xi_khach2;
                    lo_khach = lo_khach2;
                    de_khach = de_khach2;
                    pt_giu_lo_khach = pt_giu_lo_khach2;
                    pt_giu_xi_khach = pt_giu_xi_khach2;
                    pt_giu_bc_khach = pt_giu_bc_khach2;
                    pt_giu_de_khach = pt_giu_de_khach2;
                }
            } catch (JSONException e30) {
                e = e30;
                pt_giu_xi_dly = pt_giu_xi_dly2;
                pt_giu_bc_dly = pt_giu_bc_dly2;
                bc_khach = bc_khach2;
                xi_khach = xi_khach2;
                lo_khach = lo_khach2;
                de_khach = de_khach2;
                pt_giu_lo_khach = pt_giu_lo_khach2;
                pt_giu_xi_khach = pt_giu_xi_khach2;
                pt_giu_bc_khach = pt_giu_bc_khach2;
                pt_giu_de_khach = pt_giu_de_khach2;
            } catch (Exception e31) {
                pt_giu_xi_dly = pt_giu_xi_dly2;
                pt_giu_bc_dly = pt_giu_bc_dly2;
                bc_khach = bc_khach2;
                xi_khach = xi_khach2;
                lo_khach = lo_khach2;
                de_khach = de_khach2;
                pt_giu_lo_khach = pt_giu_lo_khach2;
                pt_giu_xi_khach = pt_giu_xi_khach2;
                pt_giu_bc_khach = pt_giu_bc_khach2;
                pt_giu_de_khach = pt_giu_de_khach2;
            }
        } catch (JSONException e32) {
            e = e32;
            xi_dly = xi_dly2;
            pt_giu_xi_dly = pt_giu_xi_dly2;
            pt_giu_bc_dly = pt_giu_bc_dly2;
            bc_dly = bc_dly2;
            bc_khach = bc_khach2;
            xi_khach = xi_khach2;
            lo_khach = lo_khach2;
            de_khach = de_khach2;
            pt_giu_lo_khach = pt_giu_lo_khach2;
            pt_giu_xi_khach = pt_giu_xi_khach2;
            pt_giu_bc_khach = pt_giu_bc_khach2;
            pt_giu_de_khach = pt_giu_de_khach2;
        } catch (Exception e33) {
            xi_dly = xi_dly2;
            pt_giu_xi_dly = pt_giu_xi_dly2;
            pt_giu_bc_dly = pt_giu_bc_dly2;
            bc_dly = bc_dly2;
            bc_khach = bc_khach2;
            xi_khach = xi_khach2;
            lo_khach = lo_khach2;
            de_khach = de_khach2;
            pt_giu_lo_khach = pt_giu_lo_khach2;
            pt_giu_xi_khach = pt_giu_xi_khach2;
            pt_giu_bc_khach = pt_giu_bc_khach2;
            pt_giu_de_khach = pt_giu_de_khach2;
        }
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (switch1.isChecked()) {
                    ln2.setVisibility(0);
                } else {
                    ln2.setVisibility(8);
                }
            }
        });
        Baocaochitiet.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.4
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Intent intent = new Intent(Frag_No_old.this.getActivity(), Activity_khach.class);
                intent.putExtra("tenKH", tenkh);
                Frag_No_old.this.startActivity(intent);
                dialog.cancel();
            }
        });
        tinhlaitien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.5
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                dialog.cancel();
            }
        });
        nhanchottien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.6
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Database database2 = Frag_No_old.this.db;
                Cursor cursor2 = database2.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)) + "'");
                if (cursor2 != null && cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    if (cursor2.getString(2).indexOf("sms") > -1) {
                        try {
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                Frag_No_old.this.db.SendSMS((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.position), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                            } else {
                                Frag_No_old.this.db.SendSMS((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.position), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                            }
                            Toast.makeText(Frag_No_old.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                        } catch (JSONException e34) {
                            e34.printStackTrace();
                        }
                    } else if (cursor2.getString(2).indexOf("TL") > -1) {
                        try {
                            final MainActivity mainActivity = new MainActivity();
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.6.1
                                    @Override // java.lang.Runnable
                                    public void run() {
                                        try {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.position)), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                                        } catch (JSONException e35) {
                                            e35.printStackTrace();
                                        }
                                    }
                                });
                            } else {
                                new Handler(Looper.getMainLooper()).post(new Runnable() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.6.2
                                    @Override // java.lang.Runnable
                                    public void run() {
                                        try {
                                            MainActivity.sendMessage(Long.parseLong((String) Frag_No_old.this.mSDT.get(Frag_No_old.this.position)), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                                        } catch (JSONException e35) {
                                            e35.printStackTrace();
                                        }
                                    }
                                });
                            }
                            Toast.makeText(Frag_No_old.this.getActivity(), "Đã nhắn chốt tiền!", 1).show();
                        } catch (JSONException e35) {
                            e35.printStackTrace();
                        }
                    } else if (MainActivity.arr_TenKH.indexOf(cursor2.getString(1)) > -1) {
                        NotificationReader notificationReader = new NotificationReader();
                        try {
                            if (MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0) {
                                notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                            } else {
                                notificationReader.NotificationWearReader(cursor2.getString(1), Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                            }
                        } catch (JSONException e36) {
                            e36.printStackTrace();
                        }
                    } else {
                        Toast.makeText(Frag_No_old.this.getActivity(), "Không có người này trong Chatbox", 1).show();
                    }
                }
                dialog.cancel();
            }
        });
        copytinchitiet.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.7
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ClipData clip = ClipData.newPlainText("Tin chốt:", Frag_No_old.this.db.Tin_Chottien_CT((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                clipboard.setPrimaryClip(clip);
                Toast.makeText(Frag_No_old.this.getActivity(), "Đã copy vào bộ nhớ tạm!", 1).show();
                dialog.cancel();
            }
        });
        copytinchotien.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.8
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Database database2 = Frag_No_old.this.db;
                Cursor cursor2 = database2.GetData("Select * From tbl_kh_new Where ten_kh = '" + ((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)) + "'");
                cursor2.moveToFirst();
                try {
                    ClipData clip = MainActivity.jSon_Setting.getInt("tachxien_tinchot") == 0 ? ClipData.newPlainText("Tin chốt:", Frag_No_old.this.db.Tin_Chottien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position))) : ClipData.newPlainText("Tin chốt:", Frag_No_old.this.db.Tin_Chottien_xien((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)));
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(Frag_No_old.this.getActivity(), "Đã copy vào bộ nhớ tạm!", 1).show();
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
                dialog.cancel();
            }
        });
        xoadulieu.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.9
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                AlertDialog.Builder bui = new AlertDialog.Builder(Frag_No_old.this.getActivity());
                bui.setTitle("Xoá dữ liệu của KH này?");
                bui.setPositiveButton("YES", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.9.1
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface mdialog, int which) {
                        String str = "DELETE FROM tbl_soctS WHERE ngay_nhan = '" + mDate + "' AND ten_kh = '" + ((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)) + "'";
                        Frag_No_old.this.db.QueryData(str);
                        String str2 = "DELETE FROM tbl_tinnhanS WHERE ngay_nhan = '" + mDate + "' AND ten_kh = '" + ((String) Frag_No_old.this.mTenKH.get(Frag_No_old.this.position)) + "'";
                        Frag_No_old.this.db.QueryData(str2);
                        Frag_No_old.this.lv_No2_KH();
                        dialog.cancel();
                        Toast.makeText(Frag_No_old.this.getActivity(), "Đã xoá", 1).show();
                    }
                });
                bui.setNegativeButton("No", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.9.2
                    @Override // android.content.DialogInterface.OnClickListener
                    public void onClick(DialogInterface mdialog, int which) {
                        mdialog.cancel();
                    }
                });
                bui.create().show();
            }
        });
        de_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.10
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_de_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("khgiu_de", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        lo_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.11
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_lo_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("khgiu_lo", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        xi_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.12
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_xi_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("khgiu_xi", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        bc_khach.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.13
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_bc_khach;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("khgiu_bc", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        de_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.14
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_de_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("dlgiu_de", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        lo_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.15
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_lo_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("dlgiu_lo", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        xi_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.16
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_xi_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("dlgiu_xi", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        bc_dly.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() { // from class: tamhoang.ldpro4.Fragment.Frag_No_old.17
            int max;

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = pt_giu_bc_dly;
                textView.setText((progress * 5) + "%");
                this.max = progress * 5;
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override // android.widget.SeekBar.OnSeekBarChangeListener
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    Frag_No_old.this.caidat_tg.put("dlgiu_bc", this.max);
                    Database database2 = Frag_No_old.this.db;
                    database2.QueryData("update tbl_kh_new set tbl_MB = '" + Frag_No_old.this.json.toString() + "' WHERE ten_kh = '" + tenkh + "'");
                    Frag_No_old.this.TinhlaitienKhachnay(mDate, tenkh);
                } catch (JSONException e34) {
                    e34.printStackTrace();
                }
            }
        });
        Window window = dialog.getWindow();
        window.setLayout(-1, -2);
        dialog.setCancelable(true);
        dialog.setTitle("Xem dạng:");
        dialog.show();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public void TinhlaitienKhachnay(String mDate, String tenkh) {
        Database database = this.db;
        database.QueryData("Delete From tbl_soctS WHERE  ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.mTenKH.get(this.position) + "'");
        Database database2 = this.db;
        Cursor cur = database2.GetData("Select * FROM tbl_tinnhanS WHERE  ngay_nhan = '" + mDate + "' AND phat_hien_loi = 'ok' AND ten_kh = '" + this.mTenKH.get(this.position) + "'");
        while (cur.moveToNext()) {
            String str = cur.getString(10);
            String str2 = str.replaceAll("\\*", "");
            Database database3 = this.db;
            database3.QueryData("Update tbl_tinnhanS set nd_phantich = '" + str2 + "' WHERE id = " + cur.getInt(0));
            this.db.NhapSoChiTiet(cur.getInt(0));
        }
        Tinhtien();
        lv_No2_KH();
        if (!cur.isClosed()) {
            cur.close();
        }
    }

    private void Tinhtien() {
        new MainActivity();
        String mDate = MainActivity.Get_date();
        Database database = this.db;
        Cursor cursor = database.GetData("Select * From Ketqua WHERE ngay = '" + mDate + "'");
        cursor.moveToFirst();
        int i2 = 2;
        while (i2 < 29) {
            try {
                if (cursor.isNull(i2) || !Congthuc.isNumeric(cursor.getString(i2))) {
                    break;
                }
                i2++;
            } catch (Exception e) {
            }
        }
        if (i2 >= 29) {
            this.db.Tinhtien(mDate);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        try {
            this.handler.removeCallbacks(this.runnable);
        } catch (Exception e) {
        }
    }

    public void lv_No2_KH() {
        JSONException e;
        String str = "xi";
        String str2 = "xn";
        String str3 = "loa";
        String str4 = "lo";
        String str5 = "ded";
        new MainActivity();
        String mDate = MainActivity.Get_date();
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        String str6 = "Select the_loai\n, sum((type_kh = 1)*(100-diem_khachgiu)*diem/100) as mDiem\n, CASE WHEN the_loai = 'xi' OR the_loai = 'xia' \n THEN sum((type_kh = 1)*(100-diem_khachgiu)*diem/100*so_nhay*lan_an/1000) \n ELSE sum((type_kh = 1)*(100-diem_khachgiu)*diem/100*so_nhay)  END nAn\n, sum((type_kh = 1)*ket_qua*(100-diem_khachgiu)/100) as mKetqua\n, sum((type_kh = 2)*(100-diem_khachgiu)*diem/100) as mDiem\n, CASE WHEN the_loai = 'xi' OR the_loai = 'xia' \n THEN sum((type_kh = 2)*(100-diem_khachgiu)*diem/100*so_nhay*lan_an/1000) \n ELSE sum((type_kh = 2)*(100-diem_khachgiu)*diem/100*so_nhay)  END nAn \n, sum((type_kh = 2)*ket_qua*(100-diem_khachgiu)/100) as mKetqua\n  From tbl_soctS Where ngay_nhan = '" + mDate + "'\n   AND the_loai <> 'tt' GROUP by the_loai";
        Cursor cursor = this.db.GetData(str6);
        try {
            JSONObject json = new JSONObject();
            if (cursor != null) {
                double TienNhan = 0.0d;
                while (cursor.moveToNext()) {
                    try {
                        JSONObject jsonDang = new JSONObject();
                        jsonDang.put("DiemNhan", decimalFormat.format(cursor.getDouble(1) - cursor.getDouble(4)));
                        jsonDang.put("AnNhan", decimalFormat.format(cursor.getDouble(2) - cursor.getDouble(5)));
                        jsonDang.put("KQNhan", decimalFormat.format(-(cursor.getDouble(3) + cursor.getDouble(6))));
                        TienNhan = (TienNhan - cursor.getDouble(3)) - cursor.getDouble(6);
                        json.put(cursor.getString(0), jsonDang.toString());
                        str6 = str6;
                        str = str;
                        str2 = str2;
                        str5 = str5;
                        str3 = str3;
                        str4 = str4;
                    } catch (SQLException e2) {
                    } catch (JSONException e3) {
                        e = e3;
                        e.printStackTrace();
                        if (cursor != null) {
                            cursor.close();
                        }
                        xemListview();
                    }
                }
                if (json.has("dea")) {
                    this.lnDea.setVisibility(0);
                    JSONObject jsonDang2 = new JSONObject(json.getString("dea"));
                    this.tvDiemDea.setText(jsonDang2.getString("DiemNhan"));
                    this.tvAnDea.setText(jsonDang2.getString("AnNhan"));
                    this.tvKQDea.setText(jsonDang2.getString("KQNhan"));
                }
                if (json.has("deb")) {
                    JSONObject jsonDang3 = new JSONObject(json.getString("deb"));
                    this.tvDiemDeb.setText(jsonDang3.getString("DiemNhan"));
                    this.tvAnDeb.setText(jsonDang3.getString("AnNhan"));
                    this.tvKQDeb.setText(jsonDang3.getString("KQNhan"));
                }
                if (json.has("det")) {
                    this.lnDet.setVisibility(0);
                    JSONObject jsonDang4 = new JSONObject(json.getString("det"));
                    this.tvDiemDet.setText(jsonDang4.getString("DiemNhan"));
                    this.tvAnDet.setText(jsonDang4.getString("AnNhan"));
                    this.tvKQDet.setText(jsonDang4.getString("KQNhan"));
                }
                if (json.has("dec")) {
                    this.lnDec.setVisibility(0);
                    JSONObject jsonDang5 = new JSONObject(json.getString("dec"));
                    this.tvDiemDec.setText(jsonDang5.getString("DiemNhan"));
                    this.tvAnDec.setText(jsonDang5.getString("AnNhan"));
                    this.tvKQDec.setText(jsonDang5.getString("KQNhan"));
                }
                if (json.has(str5)) {
                    this.lnDed.setVisibility(0);
                    JSONObject jsonDang6 = new JSONObject(json.getString(str5));
                    this.tvDiemDed.setText(jsonDang6.getString("DiemNhan"));
                    this.tvAnDed.setText(jsonDang6.getString("AnNhan"));
                    this.tvKQDed.setText(jsonDang6.getString("KQNhan"));
                }
                if (json.has(str4)) {
                    JSONObject jsonDang7 = new JSONObject(json.getString(str4));
                    this.tvDiemLo1.setText(jsonDang7.getString("DiemNhan"));
                    this.tvAnLo1.setText(jsonDang7.getString("AnNhan"));
                    this.tvKQLo1.setText(jsonDang7.getString("KQNhan"));
                }
                if (json.has(str3)) {
                    this.ln_loa.setVisibility(0);
                    JSONObject jsonDang8 = new JSONObject(json.getString(str3));
                    this.tvDiemLoa.setText(jsonDang8.getString("DiemNhan"));
                    this.tvAnLoa.setText(jsonDang8.getString("AnNhan"));
                    this.tvKQLoa.setText(jsonDang8.getString("KQNhan"));
                }
                if (json.has(str2)) {
                    this.lnXn.setVisibility(0);
                    JSONObject jsonDang9 = new JSONObject(json.getString(str2));
                    this.tvDiemXn1.setText(jsonDang9.getString("DiemNhan"));
                    this.tvAnXn1.setText(jsonDang9.getString("AnNhan"));
                    this.tvKQXn1.setText(jsonDang9.getString("KQNhan"));
                }
                if (json.has(str)) {
                    this.ln_xi2.setVisibility(0);
                    JSONObject jsonDang10 = new JSONObject(json.getString(str));
                    this.tvDiemXi2.setText(jsonDang10.getString("DiemNhan"));
                    this.tvAnXi2.setText(jsonDang10.getString("AnNhan"));
                    this.tvKQXi2.setText(jsonDang10.getString("KQNhan"));
                }
                if (json.has("xia")) {
                    this.ln_xia2.setVisibility(0);
                    JSONObject jsonDang11 = new JSONObject(json.getString("xia"));
                    this.tvDiemXia2.setText(jsonDang11.getString("DiemNhan"));
                    this.tvAnXia2.setText(jsonDang11.getString("AnNhan"));
                    this.tvKQXia2.setText(jsonDang11.getString("KQNhan"));
                }
                if (json.has("bc")) {
                    JSONObject jsonDang12 = new JSONObject(json.getString("bc"));
                    this.tvDiemBc1.setText(jsonDang12.getString("DiemNhan"));
                    this.tvAnBC1.setText(jsonDang12.getString("AnNhan"));
                    this.tvKQBc1.setText(jsonDang12.getString("KQNhan"));
                }
                if (json.has("bca")) {
                    this.ln_bca.setVisibility(0);
                    JSONObject jsonDang13 = new JSONObject(json.getString("bca"));
                    this.tvDiemBca.setText(jsonDang13.getString("DiemNhan"));
                    this.tvAnBCa.setText(jsonDang13.getString("AnNhan"));
                    this.tvKQBca.setText(jsonDang13.getString("KQNhan"));
                }
                this.tvTongTien1.setText(decimalFormat.format(TienNhan));
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        } catch (SQLException e4) {
        } catch (JSONException e5) {
            e = e5;
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        xemListview();
    }

    public void xemListview() {
        new MainActivity();
        String mDate = MainActivity.Get_date();
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        this.jsonKhachHang = new ArrayList();
        this.mTenKH.clear();
        this.mSDT.clear();
        this.mTypeKH.clear();
        String str = "Select ten_kh, so_dienthoai, type_kh, the_loai\n, sum((100-diem_khachgiu)*diem/100) as mDiem\n, CASE WHEN the_loai = 'xi' OR the_loai = 'xia' \n THEN sum((100-diem_khachgiu)*diem/100*so_nhay*lan_an/1000) \n ELSE sum((100-diem_khachgiu)*diem/100*so_nhay)  END nAn\n, sum(ket_qua*(100-diem_khachgiu)/100) as mKetqua\n  From tbl_soctS Where ngay_nhan = '" + mDate + "'\n  AND the_loai <> 'tt' GROUP by type_kh, ten_kh, the_loai ORDER by type_kh DESC, ten_kh";
        Cursor cursor = this.db.GetData(str);
        try {
            JSONObject JsonChiTietKhach = new JSONObject();
            double TienNhan = 0.0d;
            if (cursor != null) {
                String myKhachHang = "";
                while (cursor.moveToNext()) {
                    if (myKhachHang.length() == 0) {
                        this.mTenKH.add(cursor.getString(0));
                        this.mSDT.add(cursor.getString(1));
                        this.mTypeKH.add(cursor.getString(2));
                        myKhachHang = cursor.getString(0);
                    } else if (myKhachHang.indexOf(cursor.getString(0)) != 0) {
                        JsonChiTietKhach.put("Tien_Nhan", decimalFormat.format(TienNhan));
                        this.jsonKhachHang.add(JsonChiTietKhach);
                        TienNhan = 0.0d;
                        this.mTenKH.add(cursor.getString(0));
                        this.mSDT.add(cursor.getString(1));
                        this.mTypeKH.add(cursor.getString(2));
                        myKhachHang = cursor.getString(0);
                        JsonChiTietKhach = new JSONObject();
                    }
                    JSONObject jsonDang = new JSONObject();
                    jsonDang.put("DiemNhan", decimalFormat.format(cursor.getDouble(4)));
                    jsonDang.put("AnNhan", decimalFormat.format(cursor.getDouble(5)));
                    jsonDang.put("KQNhan", decimalFormat.format(cursor.getDouble(6)));
                    TienNhan += cursor.getDouble(6);
                    JsonChiTietKhach.put(cursor.getString(3), jsonDang.toString());
                }
                JsonChiTietKhach.put("Tien_Nhan", decimalFormat.format(TienNhan));
                if (cursor.getCount() > 0) {
                    this.jsonKhachHang.add(JsonChiTietKhach);
                }
                if (cursor != null && !cursor.isClosed()) {
                    cursor.close();
                }
            }
        } catch (SQLException e) {
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (getActivity() != null) {
            this.lv_no_tinnhan.setAdapter((ListAdapter) new NoRP_TN_Adapter(getActivity(), R.layout.frag_norp2_lv, this.jsonKhachHang));
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class NoRP_TN_Adapter extends ArrayAdapter {
        public NoRP_TN_Adapter(Context context, int resource, List<JSONObject> objects) {
            super(context, resource, objects);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View v, ViewGroup parent) {
            String str;
            View v2 = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.frag_norp2_lv, (ViewGroup) null);
            TextView tview0 = (TextView) v2.findViewById(R.id.tv_KhachHang);
            tview0.setText(((String) Frag_No_old.this.mTenKH.get(position)) + "");
            if (((String) Frag_No_old.this.mTypeKH.get(position)).indexOf("2") > -1) {
                tview0.setTextColor(-16776961);
            }
            JSONObject json = Frag_No_old.this.jsonKhachHang.get(position);
            try {
                if (json.has("dea")) {
                    try {
                        LinearLayout ln = (LinearLayout) v2.findViewById(R.id.ln_dea);
                        str = "xn";
                        ln.setVisibility(0);
                        JSONObject jsonDang = new JSONObject(json.getString("dea"));
                        TextView tview1 = (TextView) v2.findViewById(R.id.tv_diemDea);
                        tview1.setText(jsonDang.getString("DiemNhan"));
                        TextView tview8 = (TextView) v2.findViewById(R.id.tv_AnDea);
                        tview8.setText(jsonDang.getString("AnNhan"));
                        TextView tview15 = (TextView) v2.findViewById(R.id.tv_KQDea);
                        tview15.setText(jsonDang.getString("KQNhan"));
                    } catch (JSONException e) {
                    }
                } else {
                    str = "xn";
                }
                if (json.has("deb")) {
                    JSONObject jsonDang2 = new JSONObject(json.getString("deb"));
                    TextView tview12 = (TextView) v2.findViewById(R.id.tv_diemDeb);
                    tview12.setText(jsonDang2.getString("DiemNhan"));
                    TextView tview82 = (TextView) v2.findViewById(R.id.tv_AnDeb);
                    tview82.setText(jsonDang2.getString("AnNhan"));
                    TextView tview152 = (TextView) v2.findViewById(R.id.tv_KQDeb);
                    tview152.setText(jsonDang2.getString("KQNhan"));
                }
                if (json.has("det")) {
                    LinearLayout ln2 = (LinearLayout) v2.findViewById(R.id.ln_det);
                    ln2.setVisibility(0);
                    JSONObject jsonDang3 = new JSONObject(json.getString("det"));
                    TextView tview13 = (TextView) v2.findViewById(R.id.tv_diemDet);
                    tview13.setText(jsonDang3.getString("DiemNhan"));
                    TextView tview83 = (TextView) v2.findViewById(R.id.tv_AnDet);
                    tview83.setText(jsonDang3.getString("AnNhan"));
                    TextView tview153 = (TextView) v2.findViewById(R.id.tv_KQDet);
                    tview153.setText(jsonDang3.getString("KQNhan"));
                }
                if (json.has("dec")) {
                    LinearLayout ln3 = (LinearLayout) v2.findViewById(R.id.ln_dec);
                    ln3.setVisibility(0);
                    JSONObject jsonDang4 = new JSONObject(json.getString("dec"));
                    TextView tview14 = (TextView) v2.findViewById(R.id.tv_diemDec);
                    tview14.setText(jsonDang4.getString("DiemNhan"));
                    TextView tview84 = (TextView) v2.findViewById(R.id.tv_AnDec);
                    tview84.setText(jsonDang4.getString("AnNhan"));
                    TextView tview154 = (TextView) v2.findViewById(R.id.tv_KQDec);
                    tview154.setText(jsonDang4.getString("KQNhan"));
                }
                if (json.has("ded")) {
                    LinearLayout ln4 = (LinearLayout) v2.findViewById(R.id.ln_ded);
                    ln4.setVisibility(0);
                    JSONObject jsonDang5 = new JSONObject(json.getString("ded"));
                    TextView tview16 = (TextView) v2.findViewById(R.id.tv_diemDed);
                    tview16.setText(jsonDang5.getString("DiemNhan"));
                    TextView tview85 = (TextView) v2.findViewById(R.id.tv_AnDed);
                    tview85.setText(jsonDang5.getString("AnNhan"));
                    TextView tview155 = (TextView) v2.findViewById(R.id.tv_KQDed);
                    tview155.setText(jsonDang5.getString("KQNhan"));
                }
                if (json.has("lo")) {
                    JSONObject jsonDang6 = new JSONObject(json.getString("lo"));
                    TextView tview17 = (TextView) v2.findViewById(R.id.tv_diemLo1);
                    tview17.setText(jsonDang6.getString("DiemNhan"));
                    TextView tview86 = (TextView) v2.findViewById(R.id.tv_AnLo1);
                    tview86.setText(jsonDang6.getString("AnNhan"));
                    TextView tview156 = (TextView) v2.findViewById(R.id.tv_KQLo1);
                    tview156.setText(jsonDang6.getString("KQNhan"));
                }
                if (json.has("loa")) {
                    LinearLayout ln5 = (LinearLayout) v2.findViewById(R.id.ln_loa);
                    ln5.setVisibility(0);
                    JSONObject jsonDang7 = new JSONObject(json.getString("loa"));
                    TextView tview18 = (TextView) v2.findViewById(R.id.tv_diemLoa1);
                    tview18.setText(jsonDang7.getString("DiemNhan"));
                    TextView tview87 = (TextView) v2.findViewById(R.id.tv_AnLoa1);
                    tview87.setText(jsonDang7.getString("AnNhan"));
                    TextView tview157 = (TextView) v2.findViewById(R.id.tv_KQLoa1);
                    tview157.setText(jsonDang7.getString("KQNhan"));
                }
                if (json.has("xi")) {
                    LinearLayout ln6 = (LinearLayout) v2.findViewById(R.id.ln_xi2);
                    ln6.setVisibility(0);
                    JSONObject jsonDang8 = new JSONObject(json.getString("xi"));
                    TextView tview19 = (TextView) v2.findViewById(R.id.tv_diemXi2);
                    tview19.setText(jsonDang8.getString("DiemNhan"));
                    TextView tview88 = (TextView) v2.findViewById(R.id.tv_AnXi2);
                    tview88.setText(jsonDang8.getString("AnNhan"));
                    TextView tview158 = (TextView) v2.findViewById(R.id.tv_KQXi2);
                    tview158.setText(jsonDang8.getString("KQNhan"));
                }
                if (json.has("xia")) {
                    LinearLayout ln7 = (LinearLayout) v2.findViewById(R.id.ln_xia2);
                    ln7.setVisibility(0);
                    JSONObject jsonDang9 = new JSONObject(json.getString("xia"));
                    TextView tview110 = (TextView) v2.findViewById(R.id.tv_diemXia2);
                    tview110.setText(jsonDang9.getString("DiemNhan"));
                    TextView tview89 = (TextView) v2.findViewById(R.id.tv_AnXia2);
                    tview89.setText(jsonDang9.getString("AnNhan"));
                    TextView tview159 = (TextView) v2.findViewById(R.id.tv_KQXia2);
                    tview159.setText(jsonDang9.getString("KQNhan"));
                }
                if (json.has(str)) {
                    LinearLayout ln8 = (LinearLayout) v2.findViewById(R.id.lnXn);
                    ln8.setVisibility(0);
                    JSONObject jsonDang10 = new JSONObject(json.getString(str));
                    TextView tview111 = (TextView) v2.findViewById(R.id.tv_diemXn1);
                    tview111.setText(jsonDang10.getString("DiemNhan"));
                    TextView tview810 = (TextView) v2.findViewById(R.id.tv_AnXn1);
                    tview810.setText(jsonDang10.getString("AnNhan"));
                    TextView tview1510 = (TextView) v2.findViewById(R.id.tv_KQXn1);
                    tview1510.setText(jsonDang10.getString("KQNhan"));
                }
                if (json.has("bc")) {
                    JSONObject jsonDang11 = new JSONObject(json.getString("bc"));
                    TextView tview112 = (TextView) v2.findViewById(R.id.tv_diemBc1);
                    tview112.setText(jsonDang11.getString("DiemNhan"));
                    TextView tview811 = (TextView) v2.findViewById(R.id.tv_AnBc1);
                    tview811.setText(jsonDang11.getString("AnNhan"));
                    TextView tview1511 = (TextView) v2.findViewById(R.id.tv_KQBc1);
                    tview1511.setText(jsonDang11.getString("KQNhan"));
                }
                if (json.has("bca")) {
                    LinearLayout ln9 = (LinearLayout) v2.findViewById(R.id.ln_bca);
                    ln9.setVisibility(0);
                    JSONObject jsonDang12 = new JSONObject(json.getString("bca"));
                    TextView tview113 = (TextView) v2.findViewById(R.id.tv_diemBca);
                    tview113.setText(jsonDang12.getString("DiemNhan"));
                    TextView tview812 = (TextView) v2.findViewById(R.id.tv_AnBca);
                    tview812.setText(jsonDang12.getString("AnNhan"));
                    TextView tview1512 = (TextView) v2.findViewById(R.id.tv_KQBca);
                    tview1512.setText(jsonDang12.getString("KQNhan"));
                }
                TextView tview114 = (TextView) v2.findViewById(R.id.tv_TongTien1);
                tview114.setText(json.getString("Tien_Nhan"));
            } catch (JSONException e2) {
            }
            return v2;
        }
    }

    public void istart() {
        this.lnDea = (LinearLayout) this.v.findViewById(R.id.ln_dea);
        this.lnDet = (LinearLayout) this.v.findViewById(R.id.ln_det);
        this.lnDec = (LinearLayout) this.v.findViewById(R.id.ln_dec);
        this.lnDed = (LinearLayout) this.v.findViewById(R.id.ln_ded);
        this.lnXn = (LinearLayout) this.v.findViewById(R.id.lnXn);
        this.ln_xi2 = (LinearLayout) this.v.findViewById(R.id.ln_xi2);
        this.ln_xia2 = (LinearLayout) this.v.findViewById(R.id.ln_xia2);
        this.ln_loa = (LinearLayout) this.v.findViewById(R.id.ln_loa);
        this.ln_bca = (LinearLayout) this.v.findViewById(R.id.ln_bca);
        this.tvDiemDea = (TextView) this.v.findViewById(R.id.tv_diemDea);
        this.tvDiemDeb = (TextView) this.v.findViewById(R.id.tv_diemDeb);
        this.tvDiemDet = (TextView) this.v.findViewById(R.id.tv_diemDet);
        this.tvDiemDec = (TextView) this.v.findViewById(R.id.tv_diemDec);
        this.tvDiemDed = (TextView) this.v.findViewById(R.id.tv_diemDed);
        this.tvDiemLo1 = (TextView) this.v.findViewById(R.id.tv_diemLo1);
        this.tvDiemLoa = (TextView) this.v.findViewById(R.id.tv_diemLoa1);
        this.tvDiemXi2 = (TextView) this.v.findViewById(R.id.tv_diemXi2);
        this.tvDiemXia2 = (TextView) this.v.findViewById(R.id.tv_diemXia2);
        this.tvDiemXn1 = (TextView) this.v.findViewById(R.id.tv_diemXn1);
        this.tvDiemBc1 = (TextView) this.v.findViewById(R.id.tv_diemBc1);
        this.tvDiemBca = (TextView) this.v.findViewById(R.id.tv_diemBca);
        this.tvAnDea = (TextView) this.v.findViewById(R.id.tv_AnDea);
        this.tvAnDeb = (TextView) this.v.findViewById(R.id.tv_AnDeb);
        this.tvAnDet = (TextView) this.v.findViewById(R.id.tv_AnDet);
        this.tvAnDec = (TextView) this.v.findViewById(R.id.tv_AnDec);
        this.tvAnDed = (TextView) this.v.findViewById(R.id.tv_AnDed);
        this.tvAnLo1 = (TextView) this.v.findViewById(R.id.tv_AnLo1);
        this.tvAnLoa = (TextView) this.v.findViewById(R.id.tv_AnLoa1);
        this.tvAnXi2 = (TextView) this.v.findViewById(R.id.tv_AnXi2);
        this.tvAnXia2 = (TextView) this.v.findViewById(R.id.tv_AnXia2);
        this.tvAnXn1 = (TextView) this.v.findViewById(R.id.tv_AnXn1);
        this.tvAnBC1 = (TextView) this.v.findViewById(R.id.tv_AnBc1);
        this.tvAnBCa = (TextView) this.v.findViewById(R.id.tv_AnBca);
        this.tvKQDea = (TextView) this.v.findViewById(R.id.tv_KQDea);
        this.tvKQDeb = (TextView) this.v.findViewById(R.id.tv_KQDeb);
        this.tvKQDet = (TextView) this.v.findViewById(R.id.tv_KQDet);
        this.tvKQDec = (TextView) this.v.findViewById(R.id.tv_KQDec);
        this.tvKQDed = (TextView) this.v.findViewById(R.id.tv_KQDed);
        this.tvKQLo1 = (TextView) this.v.findViewById(R.id.tv_KQLo1);
        this.tvKQLoa = (TextView) this.v.findViewById(R.id.tv_KQLoa1);
        this.tvKQXi2 = (TextView) this.v.findViewById(R.id.tv_KQXi2);
        this.tvKQXia2 = (TextView) this.v.findViewById(R.id.tv_KQXia2);
        this.tvKQXn1 = (TextView) this.v.findViewById(R.id.tv_KQXn1);
        this.tvKQBc1 = (TextView) this.v.findViewById(R.id.tv_KQBc1);
        this.tvKQBca = (TextView) this.v.findViewById(R.id.tv_KQBca);
        this.tvTongTien1 = (TextView) this.v.findViewById(R.id.tv_TongTien1);
    }
}
