package tamhoang.ldpro4.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Login;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Frag_Home extends Fragment {
    String Imei = null;
    String TK = "";
    Button bt_trangchu;
    Button button_default;
    Database db;
    TextView edtImei;
    TextView tvHansd;
    TextView tvTaiKhoan;
    TextView tv_sodienthoai;
    String viewDate;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_home, container, false);
        this.db = new Database(getActivity());
        MainActivity mainActivity = new MainActivity();
        String mLink = mainActivity.Get_link();
        this.viewDate = mLink + "json_date1.php";
        this.tvTaiKhoan = (TextView) v.findViewById(R.id.tv_taikhoan);
        this.tvHansd = (TextView) v.findViewById(R.id.tv_hansudung);
        this.edtImei = (TextView) v.findViewById(R.id.edt_imei);
        this.tv_sodienthoai = (TextView) v.findViewById(R.id.tv_sodienthoai);
        this.button_default = (Button) v.findViewById(R.id.button_default);
        this.bt_trangchu = (Button) v.findViewById(R.id.bt_trangchu);
        this.db = new Database(getActivity());
        Kiemtra();
        this.bt_trangchu.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.1
            @Override // android.view.View.OnClickListener
            public void onClick(View v2) {
                Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.ldpro.me"));
                Frag_Home.this.startActivity(browserIntent);
            }
        });
        this.button_default.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.2
            @Override // android.view.View.OnClickListener
            public void onClick(View v2) {
                MainActivity.myDate = "";
                Frag_Home.this.Kiemtra();
                if (MainActivity.myDate.length() > 5) {
                    FragmentActivity activity = Frag_Home.this.getActivity();
                    Toast.makeText(activity, "Sử dụng đến: " + MainActivity.myDate, 1).show();
                }
            }
        });
        return v;
    }

    public void Kiemtra() {
        String str = Login.Imei;
        this.Imei = str;
        this.edtImei.setText(str);
        if (isNetworkConnected() && this.Imei != null) {
            check();
        } else if (this.Imei == null) {
            Intent intent = new Intent(getActivity(), Login.class);
            startActivity(intent);
        } else {
            Toast.makeText(getActivity(), "Kiểm tra kết nối Internet!", 1).show();
        }
        this.tvHansd.setText(MainActivity.myDate);
        this.tvTaiKhoan.setText(MainActivity.Acc_manager);
    }

    public void check() {
        if (this.Imei != null) {
            try {
                StringRequest request = new StringRequest(1, this.viewDate, new Response.Listener<String>() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.3
                    public void onResponse(String response) {
                        JSONException e;
                        try {
                            try {
                                JSONObject outerObject = new JSONObject(response);
                                JSONArray listKHs = outerObject.getJSONArray("listKHs");
                                MainActivity.listKH = listKHs.getJSONObject(0);
                                String str_ngay = MainActivity.listKH.getString("date").replaceAll("-", "");
                                String str_date_data = str_ngay.substring(6) + "/" + str_ngay.substring(4, 6) + "/" + str_ngay.substring(0, 4);
                                Frag_Home.this.tvHansd.setText(str_date_data);
                                Frag_Home.this.TK = "";
                                Frag_Home.this.TK = MainActivity.listKH.getString("acc");
                                MainActivity.myDate = str_date_data;
                                MainActivity.Acc_manager = MainActivity.listKH.getString("acc");
                                Frag_Home.this.tvTaiKhoan.setText(Frag_Home.this.TK);
                                Frag_Home.this.tv_sodienthoai.setText(MainActivity.listKH.getString("k_tra"));
                                try {
                                    MainActivity.listKH.getString("date");
                                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                    Date date1 = new Date();
                                    Date date2 = df.parse(MainActivity.listKH.getString("date"));
                                    long diff = date2.getTime() - date1.getTime();
                                    float I = (float) ((((diff / 1000) / 60) / 60) / 24);
                                    if (I >= 6.0f || I <= 0.0f) {
                                        if (I < 0.0f && Frag_Home.this.getActivity() != null) {
                                            AlertDialog.Builder bui = new AlertDialog.Builder(Frag_Home.this.getActivity());
                                            bui.setTitle("Thông báo hạn sử dụng:");
                                            bui.setMessage("Đã hết hạn sử dụng phần mềm\n\nHãy liên hệ Đại lý hoặc SĐT: 0936.023.645 để gia hạn");
                                            bui.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.3.2
                                                @Override // android.content.DialogInterface.OnClickListener
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                            bui.create().show();
                                        }
                                    } else if (Frag_Home.this.getActivity() != null) {
                                        AlertDialog.Builder bui2 = new AlertDialog.Builder(Frag_Home.this.getActivity());
                                        bui2.setTitle("Thông báo hạn sử dụng:");
                                        bui2.setMessage("Hạn sử dụng còn lại " + ((int) I) + " ngày! \nHãy liên hệ Đại lý hoặc SĐT: 0936.023.645 để gia hạn");
                                        bui2.setNegativeButton("Đóng", new DialogInterface.OnClickListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.3.1
                                            @Override // android.content.DialogInterface.OnClickListener
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        bui2.create().show();
                                    }
                                } catch (ParseException e2) {
                                    e2.printStackTrace();
                                }
                            } catch (JSONException e3) {
                                e = e3;
                                e.printStackTrace();
                            }
                        } catch (JSONException e4) {
                            e = e4;
                        }
                    }
                }, new Response.ErrorListener() { // from class: tamhoang.ldpro4.Fragment.Frag_Home.4
                    @Override // com.android.volley.Response.ErrorListener
                    public void onErrorResponse(VolleyError error) {
                    }
                }) { // from class: tamhoang.ldpro4.Fragment.Frag_Home.5
                    @Override // com.android.volley.Request
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> parameters = new HashMap<>();
                        parameters.put("imei", Frag_Home.this.Imei);
                        parameters.put("serial", Login.serial);
                        return parameters;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(request);
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Kiểm tra kết nối mạng!", 1).show();
            }
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
