package tamhoang.ldpro4;

import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationCompat;
import java.util.ArrayList;
import org.json.JSONObject;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class NotificationReader extends NotificationListenerService {
    public static final String VIBER = "com.viber.voip";
    public static final String WHATSAPP = "com.whatsapp";
    public static final String ZALO = "com.zing.zalo";
    static boolean replied;
    String Ten_KH;
    private ArrayList<NotificationCompat.Action> actions;
    JSONObject caidat_tg;
    Context context;
    Database db;
    JSONObject json;
    int soTN;
    String body = "";
    String mWhat = "";
    String ID = "";

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        this.actions = new ArrayList<>();
        this.db = new Database(getBaseContext());
    }

    /* JADX WARN: Code restructure failed: missing block: B:191:0x06d3, code lost:
        if (r31.body.startsWith("Success") != false) goto L194;
     */
    /* JADX WARN: Removed duplicated region for block: B:101:0x03a7 A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:104:0x03d5 A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:108:0x03fd A[Catch: Exception -> 0x0513, LOOP:0: B:106:0x03f7->B:108:0x03fd, LOOP_END, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:112:0x0419 A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:125:0x048d A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:130:0x04e5 A[Catch: Exception -> 0x0513, TRY_LEAVE, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:136:0x053e A[Catch: Exception -> 0x0513, TRY_ENTER, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:141:0x055f A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:151:0x05c0 A[Catch: Exception -> 0x0513, TRY_ENTER, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:267:0x05d7 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:271:0x0678 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:77:0x0287 A[Catch: Exception -> 0x034c, TryCatch #1 {Exception -> 0x034c, blocks: (B:43:0x0148, B:45:0x016a, B:48:0x019b, B:50:0x01c5, B:67:0x0253, B:69:0x025c, B:71:0x0264, B:73:0x026c, B:75:0x0274, B:77:0x0287, B:78:0x0299), top: B:249:0x0148 }] */
    /* JADX WARN: Removed duplicated region for block: B:80:0x02a4  */
    /* JADX WARN: Removed duplicated region for block: B:94:0x0361 A[Catch: Exception -> 0x0513, TRY_ENTER, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    /* JADX WARN: Removed duplicated region for block: B:98:0x037b A[Catch: Exception -> 0x0513, TryCatch #8 {Exception -> 0x0513, blocks: (B:81:0x02a6, B:83:0x02c6, B:86:0x02f5, B:88:0x031e, B:94:0x0361, B:96:0x036a, B:98:0x037b, B:99:0x039e, B:101:0x03a7, B:102:0x03ca, B:104:0x03d5, B:105:0x03ea, B:106:0x03f7, B:108:0x03fd, B:110:0x0411, B:112:0x0419, B:114:0x0423, B:116:0x042d, B:117:0x044d, B:118:0x0450, B:120:0x0460, B:123:0x046b, B:125:0x048d, B:128:0x04bc, B:130:0x04e5, B:136:0x053e, B:138:0x0544, B:139:0x0559, B:141:0x055f, B:143:0x0574, B:145:0x0583, B:147:0x05af, B:151:0x05c0, B:153:0x05ca), top: B:263:0x02a6 }] */
    @Override // android.service.notification.NotificationListenerService
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onNotificationPosted(StatusBarNotification r32) {
        /*
            Method dump skipped, instructions count: 2583
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.NotificationReader.onNotificationPosted(android.service.notification.StatusBarNotification):void");
    }

    @Override // android.service.notification.NotificationListenerService
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
    }

    public void NotificationWearReader(String mName, String message) {
        int indexName = MainActivity.arr_TenKH.indexOf(mName);
        if (indexName > -1) {
            try {
                Intent localIntent = new Intent();
                Bundle localBundle = MainActivity.contactslist.get(indexName).remoteExtras;
                RemoteInput[] remoteInputs = {MainActivity.contactslist.get(indexName).remoteInput};
                if (Build.VERSION.SDK_INT >= 20) {
                    localBundle.putCharSequence(remoteInputs[0].getResultKey(), message);
                    RemoteInput.addResultsToIntent(remoteInputs, localIntent, localBundle);
                }
                MainActivity.contactslist.get(indexName).pendingIntent.send(MainActivity.Notifi, 0, localIntent);
                if (MainActivity.Json_Tinnhan.has(mName)) {
                    MainActivity.Json_Tinnhan.remove(mName);
                }
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
    }
}
