package tamhoang.ldpro4;

/* loaded from: classes2.dex */
public class NavItem {
    private int resIcons;
    private String subtitle;
    private String title;

    public NavItem(String title, String subtitle, int resIcons) {
        this.title = title;
        this.subtitle = subtitle;
        this.resIcons = resIcons;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public int getResIcons() {
        return this.resIcons;
    }

    public void setResIcons(int resIcons) {
        this.resIcons = resIcons;
    }
}
