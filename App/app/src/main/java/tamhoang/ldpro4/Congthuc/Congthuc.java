package tamhoang.ldpro4.Congthuc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import tamhoang.ldpro4.MainActivity;

/* loaded from: classes2.dex */
public class Congthuc {
    public static String[][] mang;

    /* JADX WARN: Code restructure failed: missing block: B:180:0x04f0, code lost:
        r2 = r6 + r5;
        r3 = r19;
        r2 = r0;
        r0 = r2;
     */
    /* JADX WARN: Removed duplicated region for block: B:144:0x0359  */
    /* JADX WARN: Removed duplicated region for block: B:145:0x0371  */
    /* JADX WARN: Removed duplicated region for block: B:172:0x049a  */
    /* JADX WARN: Removed duplicated region for block: B:218:0x04ee A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static String NhanTinNhan(String r36) {
        /*
            Method dump skipped, instructions count: 1570
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Congthuc.Congthuc.NhanTinNhan(java.lang.String):java.lang.String");
    }

    public static final String convertKhongDau(String stringInput) {
        String stringInput2 = (stringInput.toLowerCase() + " ").replaceAll("bỏ", "bor").replaceAll("bộ", "boj").replaceAll("\\.", ",").replaceAll("́", "").replaceAll("̀", "").replaceAll("̉", "").replaceAll("̣", "").replaceAll("̃", "").replaceAll("\\+", "!");
        for (int i = 0; i < "aaaaaaaaaaaaaaaaaeeeeeeeeeeeooooooooooooooooouuuuuuuuuuuiiiiiyyyyydx".length(); i++) {
            stringInput2 = stringInput2.replace("ăâàằầáắấảẳẩãẵẫạặậễẽểẻéêèềếẹệôòồơờóốớỏổởõỗỡọộợưúùứừủửũữụựìíỉĩịỳýỷỹỵđ×".charAt(i), "aaaaaaaaaaaaaaaaaeeeeeeeeeeeooooooooooooooooouuuuuuuuuuuiiiiiyyyyydx".charAt(i));
        }
        String stringInput3 = stringInput2.replaceAll("\n d", "\nd");
        if (stringInput3.indexOf("\nđ") > -1 || stringInput3.indexOf("\nd") > -1) {
            int i1 = -1;
            while (true) {
                int indexOf = stringInput3.indexOf("\nd", i1 + 1);
                i1 = indexOf;
                if (indexOf == -1) {
                    break;
                } else if (i1 < stringInput3.length() - 1) {
                    String SSS = stringInput3.substring(i1 + 2, i1 + 3);
                    if (isNumeric(SSS) || SSS.indexOf(" ") > -1 || SSS.indexOf(":") > -1) {
                        stringInput3 = stringInput3.replaceAll("\nd" + SSS, "!d" + SSS);
                    }
                }
            }
        }
        for (int i2 = 1; i2 < 10; i2++) {
            stringInput3 = stringInput3.replaceAll("  ", " ");
        }
        return stringInput3.replaceAll("\\s+", " ").replace("d e", "de").replace("d au", "dau").replace("d it", "dit").replace("ja", "ia").replace("dich", "dit").replace("je", "ie").replace("nde", "n de").replace("nlo", "n lo").replace("nxi", "n xi").replace("nda", "n da").replace("ndi", "n di").replace("nto", "n to").replace("x i", "xi").replace("x j", "xi").replace("xj", "xi").replace("x 3 bc", "x 3, bc");
    }

    public static String Xuly_DauTN(String str) {
        String str2 = str.replaceAll(" ̂ ", " ").replaceAll("tong k ", "tong ko ").replaceAll("tong 0 chia", "tong ko chia").replaceAll("botrung", "bor trung").replaceAll(" ̂", "");
        for (int i = 0; i < MainActivity.formList.size(); i++) {
            str2 = str2.replaceAll(MainActivity.formList.get(i).get("datas"), MainActivity.formList.get(i).get("type")).replaceAll("  ", " ");
        }
        for (int i2 = 0; i2 < MainActivity.formArray.size(); i2++) {
            str2 = str2.replaceAll(MainActivity.formArray.get(i2).get("str"), MainActivity.formArray.get(i2).get("repl_str")).replaceAll("  ", " ");
        }
        for (int i3 = 1; i3 < 10; i3++) {
            str2 = str2.replaceAll("  ", " ");
        }
        String str3 = str2.replaceAll("xie n", "xi").replaceAll("le ch", "lech").replace("\n", " ").replace("\\.", ",").replaceAll(";,", ";").replaceAll("; ,", ";").replaceAll("; lo", "lo").replaceAll("va ", ";").replaceAll(";lo", "lo").replaceAll("; de", "de").replaceAll(";de", "de").replaceAll("; xi", "xi").replaceAll("dedau", "de dau").replaceAll("dedit", "de dit").replaceAll("decham", "de cham").replaceAll("dedinh", "de cham").replaceAll(";xn", "xn").replaceAll(";xi", "xi").replaceAll("; bc", "bc").replaceAll(";bc", "bc");
        str3.replaceAll("bc", " bc ").replace("dan", " dan ").replace("cua", " trung ").replace("chia", " chia ").replace("dau", " dau ").replace("dit", " dit ").replace("tong", " tong ").replace("cham", " cham ").replace("boj", " boj ").replace("bor", " bor ").replace("dea", " dea ").replaceAll("deb", " deb ").replaceAll("dec", " dec ").replaceAll("ded", " ded ").replace("lo ", " lo ").replaceAll("xg", " xg ").replaceAll("xn", " xn ");
        if (str3.indexOf("dea") == -1 && str3.indexOf("deb") == -1 && str3.indexOf("dec") == -1 && str3.indexOf("ded") == -1 && str3.indexOf("det") == -1 && str3.indexOf("de") > -1) {
            return str3.replaceAll("de", "deb ");
        }
        return str3;
    }

    public static String fixTinNhan1(String str) {
        String str2 = str.replaceAll(" ,", ", ");
        int i = 0;
        int j = str2.length();
        while (i < j - 1) {
            i++;
            j = str2.length() - 1;
            if (Character.isLetter(str2.charAt(i)) && !Character.isLetter(str2.charAt(i + 1))) {
                str2 = str2.substring(0, i + 1) + " " + str2.substring(i + 1);
                i++;
            } else if (!Character.isLetter(str2.charAt(i)) && Character.isLetter(str2.charAt(i + 1))) {
                str2 = str2.substring(0, i + 1) + " " + str2.substring(i + 1);
                i++;
            }
        }
        String str3 = str2 + " ";
        for (int i2 = 1; i2 < 10; i2++) {
            str3 = str3.replaceAll("  ", " ");
        }
        if (str3.indexOf("(") > -1 && str3.indexOf(")") > -1) {
            int i1 = -1;
            while (true) {
                int indexOf = str3.indexOf("(", i1 + 1);
                i1 = indexOf;
                if (indexOf == -1) {
                    break;
                }
                int i22 = i1;
                while (i22 < str3.length() && str3.substring(i22, i22 + 1).indexOf(")") <= -1) {
                    i22++;
                }
                if (isNumeric(str3.substring(i1 + 1, i22).replaceAll(" ", ""))) {
                    for (int i3 = i1; i3 < i22; i3++) {
                        if (isNumeric(str3.substring(i3 - 1, i3)) && str3.substring(i3, i3 + 1).indexOf(" ") > -1 && isNumeric(str3.substring(i3 + 1, i3 + 2))) {
                            str3 = str3.substring(0, i3) + "," + str3.substring(i3 + 1);
                        }
                    }
                }
            }
        }
        return str3;
    }

    public static String fixTinNhan(String str) {
        String str2 = str + " ";
        String str1 = str2.replaceAll(" ", ",");
        str1.replaceAll("\\.", ",").replaceAll(":", ",").replaceAll(";", ",").replaceAll("/", ",").split(",");
        int i = -1;
        if (str2.indexOf("Không hiểu") != -1) {
            return str2;
        }
        for (int i2 = 0; i2 < str2.length(); i2++) {
            if (str2.charAt(i2) > 127 || str2.charAt(i2) < 31) {
                str2 = str2.substring(0, i2) + " " + str2.substring(i2 + 1);
            }
        }
        String str3 = str2.trim();
        if (str3.charAt(str3.length() - 1) == 'x') {
            str3 = str3.substring(0, str3.length() - 1);
        }
        int dem = -1;
        while (true) {
            int indexOf = str3.indexOf("x ", dem + 1);
            dem = indexOf;
            if (indexOf == -1) {
                break;
            }
            int i3 = dem + 2;
            while (i3 < str3.length() && !isNumeric(str3.substring(i3, i3 + 1))) {
                i3++;
            }
            int j = i3;
            while (j < str3.length() && (isNumeric(str3.substring(j, j + 1)) || " tr".indexOf(str3.substring(j, j + 1)) != -1)) {
                j++;
            }
            if (isNumeric(str3.substring(dem + 1, j).trim()) && str3.substring(dem + 1, j).trim().length() > 1) {
                str3 = str3.substring(0, j) + " " + str3.substring(j);
            } else if (j - i3 > 1 && str3.substring(dem).indexOf("to") != (j - dem) - 1 && str3.substring(dem).indexOf("tin") != (j - dem) - 1 && str3.substring(dem).indexOf(",") != j - dem) {
                str3 = str3.substring(0, j) + " " + str3.substring(j);
            } else if (j - i3 == 1 && str3.substring(dem).indexOf("tr") == -1) {
                str3 = str3.substring(0, j) + " " + str3.substring(j);
            }
        }
        String str4 = str3 + " ";
        int dem2 = str4.length();
        while (dem2 > str4.length() - 9) {
            String Sss = str4.substring(dem2);
            if (Sss.trim().indexOf("t ") > i) {
                String Sss1 = "";
                for (int i4 = dem2; i4 > 0; i4--) {
                    Sss1 = str4.substring(i4, dem2);
                    if (!isNumeric(Sss1) && Sss1.trim().length() > 0) {
                        break;
                    }
                }
                if (Sss1.trim().length() > 1 || !isNumeric(Sss1)) {
                    String Sss2 = Sss.replaceAll("t", "").replaceAll(" ", "").replaceAll(",", "");
                    if (!isNumeric(Sss2) || Integer.parseInt(Sss2) >= 99) {
                        if (Sss2.length() != 0) {
                            break;
                        }
                        str4 = str4.substring(0, dem2 + 1) + "?";
                    } else {
                        str4 = str4.substring(0, dem2);
                    }
                }
            }
            dem2--;
            i = -1;
        }
        String str5 = str4.trim();
        try {
            String SSS = str5.substring(str5.length() - 1);
            if (SSS.indexOf("@") > -1) {
                int i5 = str5.length() - 2;
                while (i5 > 0 && str5.substring(i5, i5 + 1).indexOf("@") <= -1) {
                    i5--;
                }
                if (i5 > str5.length() - 13) {
                    String SS = str5.substring(i5);
                    if (isNumeric(SS.replaceAll("@", ""))) {
                        str5 = str5.substring(0, i5);
                    }
                }
            }
        } catch (Exception e) {
        }
        try {
            if (MainActivity.jSon_Setting.getInt("baotinthieu") == 0) {
                str5 = str5.trim();
                for (int i6 = 6; i6 > 0; i6--) {
                    String Sss3 = str5.substring(0, i6);
                    if (Sss3.trim().indexOf("t") > -1 && isNumeric(Sss3.replaceAll("t", "").replaceAll(",", ""))) {
                        str5 = str5.substring(i6);
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        String str6 = str5 + " ";
        int i1 = -1;
        while (true) {
            try {
                int indexOf2 = str6.indexOf("tin", i1 + 1);
                i1 = indexOf2;
                if (indexOf2 == -1) {
                    break;
                }
                int i7 = i1 + 5;
                while (i7 < i1 + 10 && isNumeric(str6.substring(i1 + 4, i7))) {
                    i7++;
                }
                if (i7 - i1 > 5) {
                    str6 = str6.substring(0, i1) + str6.substring(i7);
                }
            } catch (Exception e3) {
            }
        }
        String str7 = str6.trim();
        if (str7.substring(0, 1).indexOf(",") > -1) {
            return str7.substring(1);
        }
        return str7;
    }

    public static String FixDan(String str) {
        String DaySo = "";
        String[] array = str.replaceAll(":", "").replaceAll(" //. ", "").replaceAll(" , ", "").replaceAll("\\.", ",").split(",");
        for (int i = 0; i < array.length; i++) {
            if (isNumeric(array[i]) && array[i].length() == 2) {
                DaySo = DaySo + array[i] + ",";
            } else if (isNumeric(array[i]) && array[i].length() == 3) {
                if (array[i].charAt(0) != array[i].charAt(2)) {
                    String DaySo2 = "Không hiểu " + array[i];
                    return DaySo2;
                }
                DaySo = (DaySo + array[i].substring(0, 2) + ",") + array[i].substring(1, 3) + ",";
            } else if (array[i].length() > 0) {
                String DaySo3 = "Không hiểu " + array[i];
                return DaySo3;
            }
        }
        return DaySo;
    }

    public static String PhanTichTinNhan(String str) {
        String str2 = str.replace("  ", " ");
        if (str2.indexOf("Không hiểu") > -1) {
            return str2;
        }
        if (str2.substring(0, 5).indexOf("de") == -1 && str2.substring(0, 5).indexOf("lo") == -1 && str2.substring(0, 5).indexOf("hc") == -1 && str2.substring(0, 5).indexOf("xi") == -1 && str2.substring(0, 5).indexOf("xq") == -1 && str2.substring(0, 5).indexOf("xn") == -1 && str2.substring(0, 5).indexOf("bc") == -1 && str2.substring(0, 5).indexOf("xg") == -1) {
            return "Không hiểu dạng";
        }
        String str3 = str2 + "      ";
        str3.toCharArray();
        int I2 = 3;
        while (I2 < str3.length() - 4) {
            if (isNumeric(str3.substring(I2, I2 + 1)) && str3.charAt(I2 + 1) == ' ' && "ndk".indexOf(str3.substring(I2 + 2, I2 + 3)) > -1 && str3.charAt(I2 + 3) == ' ') {
                int I3 = I2;
                while (I3 > 0 && isNumeric(str3.substring(I3, I3 + 1))) {
                    I3--;
                }
                str3 = str3.substring(0, I3) + " x " + str3.substring(I3);
                I2 += 6;
            }
            I2++;
        }
        for (int i = 1; i < 10; i++) {
            str3 = str3.replaceAll("  ", " ");
        }
        for (int i2 = 1; i2 < 4; i2++) {
            str3 = str3.replaceAll("  ", " ").replaceAll(": x", " x").replaceAll(":x", " x").replaceAll("x x", "x").replaceAll("xx", "x").replaceAll(", x", " x").replaceAll(",x", " x").replaceAll("-x", " x").replaceAll("- x", " x");
        }
        return str3;
    }

    public static String Xu3cang(String str) {
        String DaySo = "";
        if (str.length() < 2) {
            DaySo = "Không hiểu";
        }
        if (str.replaceAll(" ", "").length() > 0) {
            String str1 = str.trim();
            String[] array = str1.replaceAll(":", " ").replaceAll(" //. ", "").replaceAll(" , ", "").replaceAll(";", " ").replaceAll("/", "").replaceAll("\\.", ",").replaceAll(" ", ",").split(",");
            for (int i = 0; i < array.length; i++) {
                if (isNumeric(array[i]) && array[i].length() == 3) {
                    DaySo = DaySo + array[i] + ",";
                } else if (array[i].length() > 0) {
                    return "Không hiểu " + array[i];
                }
            }
        }
        return DaySo;
    }

    /* JADX WARN: Code restructure failed: missing block: B:52:0x0175, code lost:
        r4 = "";
        r5 = false;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static String XulyXien(String r20) {
        /*
            Method dump skipped, instructions count: 1242
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Congthuc.Congthuc.XulyXien(java.lang.String):java.lang.String");
    }

    public static String sortXien(String xien) {
        ArrayList numberList = new ArrayList();
        String newXien = "";
        String[] sort = xien.split(",");
        for (String str : sort) {
            numberList.add(str);
        }
        Collections.sort(numberList);
        for (int i = 0; i < numberList.size(); i++) {
            newXien = newXien + numberList.get(i) + ",";
        }
        return newXien;
    }

    public static ArrayList<String> XulyXienGhep(String str, int ghep) {
        ArrayList<String> listXien = new ArrayList<>();
        if (ghep == 2) {
            String[] ArrXien = str.split(",");
            for (int s1 = 0; s1 < ArrXien.length - 1; s1++) {
                for (int s2 = s1 + 1; s2 < ArrXien.length; s2++) {
                    if (ArrXien[s1] != ArrXien[s2]) {
                        listXien.add(sortXien(ArrXien[s1] + "," + ArrXien[s2]));
                    }
                }
            }
        } else if (ghep == 3) {
            String[] ArrXien2 = str.split(",");
            for (int s12 = 0; s12 < ArrXien2.length - 2; s12++) {
                for (int s22 = s12 + 1; s22 < ArrXien2.length - 1; s22++) {
                    for (int s3 = s22 + 1; s3 < ArrXien2.length; s3++) {
                        if (!(ArrXien2[s12] == ArrXien2[s22] || ArrXien2[s12] == ArrXien2[s3] || ArrXien2[s22] == ArrXien2[s3])) {
                            listXien.add(sortXien(ArrXien2[s12] + "," + ArrXien2[s22] + "," + ArrXien2[s3]));
                        }
                    }
                }
            }
        } else if (ghep == 4) {
            String[] ArrXien3 = str.split(",");
            for (int s13 = 0; s13 < ArrXien3.length - 3; s13++) {
                for (int s23 = s13 + 1; s23 < ArrXien3.length - 2; s23++) {
                    for (int s32 = s23 + 1; s32 < ArrXien3.length - 1; s32++) {
                        for (int s4 = s32 + 1; s4 < ArrXien3.length; s4++) {
                            if (!(ArrXien3[s13] == ArrXien3[s23] || ArrXien3[s13] == ArrXien3[s32] || ArrXien3[s13] == ArrXien3[s4] || ArrXien3[s23] == ArrXien3[s32] || ArrXien3[s23] == ArrXien3[s4] || ArrXien3[s32] == ArrXien3[s4])) {
                                listXien.add(sortXien(ArrXien3[s13] + "," + ArrXien3[s23] + "," + ArrXien3[s32] + "," + ArrXien3[s4]));
                            }
                        }
                    }
                }
            }
        }
        return listXien;
    }

    public static String XulyLoDe(String str) {
        String DanGoc;
        String sauloc = "";
        if (str.indexOf("bor trung") > -1) {
            String[] ArrBT = str.split("bor trung ");
            String[] strArr = new String[0];
            String DanBo = "";
            String DanTrung = "";
            if (ArrBT[0].indexOf("bor trung") > -1) {
                DanGoc = XulySo(ArrBT[0].replaceAll("bor trung", sauloc));
            } else {
                DanGoc = XulySo(ArrBT[0]);
            }
            if (DanGoc.indexOf("Không hiểu") > -1) {
                return DanGoc;
            }
            String[] ArrBorTrung = DanGoc.split(",");
            if (ArrBT.length > 1) {
                if (ArrBT[1].length() > 0 && ArrBT[1].indexOf("bor") > -1) {
                    DanBo = XulySo(ArrBT[1].replaceAll("bor", sauloc));
                    if (DanBo.indexOf("Không hiểu") > -1) {
                        return DanBo;
                    }
                } else if (ArrBT[1].length() > 0 && ArrBT[1].indexOf("trung") > -1) {
                    DanTrung = XulySo(ArrBT[1].replaceAll("trung", sauloc));
                    if (DanTrung.indexOf("Không hiểu") > -1) {
                        return DanTrung;
                    }
                }
            }
            String sauloc2 = "";
            for (int k2 = 0; k2 < ArrBorTrung.length; k2++) {
                try {
                    if (DanBo.length() == 0 && DanTrung.length() == 0) {
                        if (sauloc2.indexOf(ArrBorTrung[k2]) == -1) {
                            sauloc2 = sauloc2 + ArrBorTrung[k2] + ",";
                        }
                    } else if (DanBo.length() <= 0 || DanTrung.length() != 0) {
                        if (DanBo.length() == 0 && DanTrung.length() > 0 && sauloc2.indexOf(ArrBorTrung[k2]) == -1 && DanTrung.indexOf(ArrBorTrung[k2]) > -1) {
                            sauloc2 = sauloc2 + ArrBorTrung[k2] + ",";
                        }
                    } else if (sauloc2.indexOf(ArrBorTrung[k2]) == -1 && DanBo.indexOf(ArrBorTrung[k2]) == -1) {
                        sauloc2 = sauloc2 + ArrBorTrung[k2] + ",";
                    }
                } catch (Exception e) {
                    return "Không hiểu " + str;
                }
            }
            return sauloc2;
        }
        if (str.indexOf("trung") <= -1 && str.indexOf("bor") <= -1) {
            try {
                String mDanSo = XulySo(str);
                if (mDanSo.indexOf("Không hiểu") > -1) {
                    if (mDanSo.length() > 11) {
                        return mDanSo;
                    }
                    return "Không hiểu " + str;
                }
            } catch (Exception e2) {
                return "Không hiểu " + str;
            }
        } else if (str.indexOf("trung") <= -1 || str.indexOf("bor") <= -1) {
            if (str.indexOf("trung") == -1 && str.indexOf("bor") > -1) {
                String[] ArrBor = str.split("bor");
                List<String> mBor = new ArrayList<>();
                for (String str2 : ArrBor) {
                    String ss = XulySo(str2);
                    if (ss.indexOf("Không hiểu") != -1) {
                        return ss;
                    }
                    mBor.add(ss);
                }
                try {
                    String[] ArrSoBor = mBor.get(0).split(",");
                    for (int k22 = 0; k22 < ArrSoBor.length; k22++) {
                        int m_Dem = 0;
                        for (int k3 = 1; k3 < mBor.size() && mBor.get(k3).indexOf(ArrSoBor[k22]) <= -1; k3++) {
                            m_Dem++;
                        }
                        int k32 = mBor.size();
                        if (m_Dem == k32 - 1) {
                            sauloc = sauloc + ArrSoBor[k22] + ",";
                        }
                    }
                    int k23 = sauloc.length();
                    if (k23 > 0) {
                        return sauloc;
                    }
                    return "Không hiểu " + str.substring(str.indexOf("bor"));
                } catch (Exception e3) {
                    return "Không hiểu " + str;
                }
            } else if (str.indexOf("trung") > -1 && str.indexOf("bor") == -1) {
                String[] ArrTrung = str.split("trung");
                List<String> mTrung = new ArrayList<>();
                for (String str3 : ArrTrung) {
                    String ss2 = XulySo(str3);
                    if (ss2.indexOf("Không hiểu") != -1) {
                        return ss2;
                    }
                    mTrung.add(ss2);
                }
                try {
                    String[] ArrSoTrung = mTrung.get(0).split(",");
                    for (int k24 = 0; k24 < ArrSoTrung.length; k24++) {
                        int m_Dem2 = 0;
                        for (int k33 = 1; k33 < mTrung.size() && mTrung.get(k33).indexOf(ArrSoTrung[k24]) > -1; k33++) {
                            m_Dem2++;
                        }
                        int k34 = mTrung.size();
                        if (m_Dem2 == k34 - 1) {
                            sauloc = sauloc + ArrSoTrung[k24] + ",";
                        }
                    }
                    return sauloc;
                } catch (Exception e4) {
                    return "Không hiểu " + str;
                }
            }
        } else if (str.indexOf("trung") < str.indexOf("bor")) {
            if (str.substring(0, str.indexOf("trung")).length() > 1) {
                try {
                    String DanGoc2 = XulySo(str.substring(0, str.indexOf("trung")));
                    if (DanGoc2.indexOf("Không hiểu") > -1) {
                        if (DanGoc2.length() > 11) {
                            return DanGoc2;
                        }
                        return "Không hiểu " + str;
                    } else if (str.substring(str.indexOf("trung") + 5, str.indexOf("bor")).length() > 1) {
                        try {
                            String DanTrung2 = XulySo(str.substring(str.indexOf("trung") + 5, str.indexOf("bor")));
                            if (DanTrung2.indexOf("Không hiểu") > -1) {
                                if (DanTrung2.length() > 11) {
                                    return DanTrung2;
                                }
                                return "Không hiểu " + str;
                            } else if (str.substring(str.indexOf("bor") + 3).replaceAll("bor", sauloc).length() > 1) {
                                try {
                                    String DanBo2 = XulySo(str.substring(str.indexOf("bor") + 3).replaceAll("bor", sauloc));
                                    if (DanBo2.indexOf("Không hiểu") <= -1) {
                                        String[] danlayS = DanGoc2.split(",");
                                        String sauloc3 = "";
                                        for (int i = 0; i < danlayS.length; i++) {
                                            if (DanTrung2.indexOf(danlayS[i]) > -1 && DanBo2.indexOf(danlayS[i]) == -1) {
                                                sauloc3 = sauloc3 + danlayS[i] + ",";
                                            }
                                        }
                                        return sauloc3;
                                    } else if (DanBo2.length() > 11) {
                                        return DanBo2;
                                    } else {
                                        return "Không hiểu " + str;
                                    }
                                } catch (Exception e5) {
                                    return "Không hiểu " + str.substring(str.indexOf("bor") + 3).replaceAll("bor", sauloc);
                                }
                            } else {
                                return "Không hiểu " + str;
                            }
                        } catch (Exception e6) {
                            return "Không hiểu " + str;
                        }
                    } else {
                        return "Không hiểu " + str;
                    }
                } catch (Exception e7) {
                    return "Không hiểu " + str;
                }
            } else {
                return "Không hiểu " + str;
            }
        } else if (str.substring(0, str.indexOf("bor")).length() > 1) {
            try {
                String DanGoc3 = XulySo(str.substring(0, str.indexOf("bor")));
                if (DanGoc3.indexOf("Không hiểu") > -1) {
                    if (DanGoc3.length() > 11) {
                        return DanGoc3;
                    }
                    return "Không hiểu " + str;
                } else if (str.substring(str.indexOf("bor") + 4, str.indexOf("trung")).length() > 1) {
                    try {
                        String DanBo3 = XulySo(str.substring(str.indexOf("bor") + 4, str.indexOf("trung")));
                        if (DanBo3.indexOf("Không hiểu") > -1) {
                            if (DanBo3.length() > 11) {
                                return DanBo3;
                            }
                            return "Không hiểu " + str;
                        } else if (str.substring(str.indexOf("trung") + 5).length() > 1) {
                            try {
                                String DanTrung3 = XulySo(str.substring(str.indexOf("trung") + 5).replaceAll("trung", sauloc));
                                if (DanTrung3.indexOf("Không hiểu") <= -1) {
                                    String[] danlayS2 = DanGoc3.split(",");
                                    String sauloc4 = "";
                                    for (int i2 = 0; i2 < danlayS2.length; i2++) {
                                        if (DanTrung3.indexOf(danlayS2[i2]) > -1 && DanBo3.indexOf(danlayS2[i2]) == -1) {
                                            sauloc4 = sauloc4 + danlayS2[i2] + ",";
                                        }
                                    }
                                    return sauloc4;
                                } else if (DanTrung3.length() > 11) {
                                    return DanTrung3;
                                } else {
                                    return "Không hiểu " + str;
                                }
                            } catch (Exception e8) {
                                return "Không hiểu " + str;
                            }
                        } else {
                            return "Không hiểu " + str;
                        }
                    } catch (Exception e9) {
                        return "Không hiểu " + str;
                    }
                } else {
                    return "Không hiểu " + str;
                }
            } catch (Exception e10) {
                return "Không hiểu " + str;
            }
        } else {
            return "Không hiểu " + str;
        }
        return XulySo(str);
    }

    public static String ToMauError(String value, String NoiDung) {
        String tomau = "ldpro" + value + "</font>";
        if (NoiDung.indexOf(value) > -1) {
            return NoiDung.replace(value, tomau);
        }
        return "ldpro" + NoiDung + "</font>";
    }

    /* JADX WARN: Code restructure failed: missing block: B:1208:0x2716, code lost:
        if (r3.replaceAll(" ", "").length() <= 0) goto L1293;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1209:0x2718, code lost:
        r4 = r3.trim().replaceAll(":", " ").replaceAll(" //. ", "").replaceAll(" , ", "").replaceAll(";", " ").replaceAll("/", "").replaceAll("\\.", ",").replaceAll(" ", ",").replaceAll(" ", ",").replaceAll(" ", ",");
     */
    /* JADX WARN: Code restructure failed: missing block: B:1210:0x2753, code lost:
        if (r3.indexOf("so") <= (-1)) goto L1221;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1212:0x275c, code lost:
        if (r3.indexOf("so") >= 3) goto L1221;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1214:0x2762, code lost:
        if (r4.length() <= 3) goto L1219;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1216:0x2771, code lost:
        if (r3.substring(0, 4).indexOf("so") != (-1)) goto L1233;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1218:0x2782, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1220:0x2792, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1222:0x279a, code lost:
        if (r3.indexOf("con") <= (-1)) goto L1233;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1224:0x27a3, code lost:
        if (r3.indexOf("con") >= 4) goto L1233;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1226:0x27a9, code lost:
        if (r4.length() <= 4) goto L1231;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1228:0x27b8, code lost:
        if (r3.substring(0, 5).indexOf("con") != (-1)) goto L1233;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1230:0x27c9, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1232:0x27d9, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1233:0x27da, code lost:
        r0 = r4.split(",");
        r5 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1235:0x27e0, code lost:
        if (r5 >= r0.length) goto L1595;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1237:0x27eb, code lost:
        if (isNumeric(r0[r5]) != true) goto L1241;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1239:0x27f4, code lost:
        if (r0[r5].length() != 2) goto L1241;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1240:0x27f6, code lost:
        r12 = r12 + r0[r5] + ",";
        r22 = r8;
        r18 = r9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1241:0x2812, code lost:
        r7 = r0[r5];
     */
    /* JADX WARN: Code restructure failed: missing block: B:1242:0x2819, code lost:
        if (isNumeric(r7) != true) goto L1254;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1244:0x2822, code lost:
        if (r0[r5].length() != 3) goto L1254;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1245:0x2824, code lost:
        r22 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1246:0x2834, code lost:
        if (r0[r5].charAt(0) == r0[r5].charAt(2)) goto L1248;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1249:0x2848, code lost:
        if (r0[r5].charAt(0) != r0[r5].charAt(1)) goto L1253;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1251:0x2858, code lost:
        if (r0[r5].charAt(0) != r0[r5].charAt(2)) goto L1253;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1253:0x285d, code lost:
        r7 = new java.lang.StringBuilder();
        r7.append(r12);
        r18 = r9;
        r7.append(r0[r5].substring(0, 2));
        r7.append(",");
        r7 = r7.toString();
        r12 = r7 + r0[r5].substring(1, 3) + ",";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1254:0x2895, code lost:
        r22 = r8;
        r18 = r9;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1255:0x28a0, code lost:
        if (r0[r5].length() != 0) goto L1596;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1256:0x28a2, code lost:
        r5 = r5 + 1;
        r9 = r18;
        r7 = r7;
        r8 = r22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1259:0x28b3, code lost:
        if (r5 >= r0.length) goto L1292;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1260:0x28b5, code lost:
        r3 = "  " + r3 + "  ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1261:0x28d5, code lost:
        if (r4.trim().length() <= 10) goto L1286;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1263:0x28de, code lost:
        if (r0[r5].length() != 1) goto L1284;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1264:0x28e0, code lost:
        r7 = -1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1265:0x28e1, code lost:
        r8 = r3.indexOf(r0[r5], r7 + 1);
        r7 = r8;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1266:0x28eb, code lost:
        if (r8 == (-1)) goto L1606;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1268:0x2905, code lost:
        if (r3.indexOf(" " + r0[r5] + " ") <= (-1)) goto L1271;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1270:0x291d, code lost:
        return "Không hiểu  " + r0[r5] + " ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1272:0x2937, code lost:
        if (r3.indexOf(" " + r0[r5] + ",") <= (-1)) goto L1275;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1274:0x294f, code lost:
        return "Không hiểu  " + r0[r5] + ",";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1276:0x2969, code lost:
        if (r3.indexOf("," + r0[r5] + " ") <= (-1)) goto L1279;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1278:0x2981, code lost:
        return "Không hiểu ," + r0[r5] + " ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1280:0x299b, code lost:
        if (r3.indexOf("," + r0[r5] + ",") <= (-1)) goto L1265;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1282:0x29b3, code lost:
        return "Không hiểu ," + r0[r5] + ",";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1285:0x29c8, code lost:
        return "Không hiểu " + r0[r5];
     */
    /* JADX WARN: Code restructure failed: missing block: B:1287:0x29d2, code lost:
        if (r3.trim().length() != 1) goto L1290;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1289:0x29ec, code lost:
        return "Không hiểu  " + r3.trim() + " ";
     */
    /* JADX WARN: Code restructure failed: missing block: B:1291:0x2a00, code lost:
        return "Không hiểu " + r3.trim();
     */
    /* JADX WARN: Code restructure failed: missing block: B:1295:0x2a0e, code lost:
        if (r12.length() <= 0) goto L1297;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1296:0x2a10, code lost:
        return r12;
     */
    /* JADX WARN: Code restructure failed: missing block: B:1298:0x2a20, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x010b, code lost:
        r7 = r26;
     */
    /* JADX WARN: Code restructure failed: missing block: B:334:0x0ae1, code lost:
        return "Không hiểu " + r3.substring(r4, r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:337:0x0aff, code lost:
        return "Không hiểu " + r3.substring(r3.indexOf(r8[r9]), r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:379:0x0c44, code lost:
        if (isNumeric(r8[r9]) != false) goto L382;
     */
    /* JADX WARN: Code restructure failed: missing block: B:381:0x0c59, code lost:
        return "Không hiểu " + r3.substring(r7, r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:383:0x0c73, code lost:
        return "Không hiểu " + r3.substring(r3.indexOf(r8[r9]), r7);
     */
    /* JADX WARN: Code restructure failed: missing block: B:469:0x0ec1, code lost:
        if (r10.indexOf(r0 + r11) > (-1)) goto L470;
     */
    /* JADX WARN: Code restructure failed: missing block: B:554:0x1141, code lost:
        if (r10.indexOf(r0 + r11) > (-1)) goto L555;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x0220, code lost:
        return "Không hiểu " + r37;
     */
    /* JADX WARN: Code restructure failed: missing block: B:899:0x1abc, code lost:
        r25 = r7;
        r31 = r11;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static String XulySo(String r37) {
        /*
            Method dump skipped, instructions count: 10807
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Congthuc.Congthuc.XulySo(java.lang.String):java.lang.String");
    }

    public static String XulyTien(String str) {
        String str1 = "";
        String tien = "";
        if (str.length() - str.replaceAll("x", "").length() > 1) {
            return "Không hiểu " + str;
        } else if (str.length() == 0) {
            return "Không hiểu ";
        } else {
            String str2 = str.replaceAll("x", "").replaceAll(" ", "").trim();
            if (str2.length() <= 0) {
                return "Không hiểu";
            }
            if (str2.endsWith("tr")) {
                String str3 = str2.replaceAll("tr", "").trim().replaceAll("\\.", "");
                str3.indexOf(",");
                String[] Mtien = str3.split(",");
                if (Mtien.length > 2) {
                    return "Không hiểu " + str3;
                } else if (Mtien.length == 1) {
                    return Mtien[0] + "000";
                } else if (Mtien.length == 2) {
                    if (Mtien[1].length() == 0) {
                        return Mtien[0] + "000";
                    } else if (Mtien[1].length() == 1) {
                        return Mtien[0] + Mtien[1] + "00";
                    } else if (Mtien[1].length() == 2) {
                        return Mtien[0] + Mtien[1] + "0";
                    } else if (Mtien[1].length() == 3) {
                        return Mtien[0] + Mtien[1];
                    } else {
                        return "Không hiểu " + str3;
                    }
                }
            } else {
                int i = 0;
                while (i < str2.length() && !isNumeric(str2.substring(i, i + 1))) {
                    i++;
                }
                for (int j = i; j < str2.length() && isNumeric(str2.substring(j, j + 1)); j++) {
                    tien = tien + str2.substring(j, j + 1);
                }
                if (str2.replaceAll(tien, "").replaceAll("ng", "").replaceAll("n", "").replaceAll("d", "").replaceAll("k", "").replaceAll(",", "").replaceAll("\\.", "").replaceAll("/", "").replaceAll(" ", "").length() > 0) {
                    return "Không hiểu " + str;
                }
                str1 = tien;
            }
            try {
                int i2 = Integer.parseInt(str1);
                if (i2 > 0) {
                    return str1;
                }
                return "Không hiểu " + str;
            } catch (Exception e) {
                return "Không hiểu " + str;
            }
        }
    }

    public static String GhepDau(String str) {
        String[] arr = new String[15];
        String str1 = "";
        if (!xuLymem(str).booleanValue()) {
            return "Không hiểu " + str;
        }
        arr[0] = "00,01,02,03,04,05,06,07,08,09,";
        arr[1] = "10,11,12,13,14,15,16,17,18,19,";
        arr[2] = "20,21,22,23,24,25,26,27,28,29,";
        arr[3] = "30,31,32,33,34,35,36,37,38,39,";
        arr[4] = "40,41,42,43,44,45,46,47,48,49,";
        arr[5] = "50,51,52,53,54,55,56,57,58,59,";
        arr[6] = "60,61,62,63,64,65,66,67,68,69,";
        arr[7] = "70,71,72,73,74,75,76,77,78,79,";
        arr[8] = "80,81,82,83,84,85,86,87,88,89,";
        arr[9] = "90,91,92,93,94,95,96,97,98,99,";
        for (int i = 0; i < str.length(); i++) {
            if (isNumeric(str.substring(i, i + 1))) {
                str1 = str1 + arr[Character.getNumericValue(str.charAt(i))];
            }
        }
        return str1;
    }

    public static String GhepDit(String str) {
        String[] arr = new String[15];
        String str1 = "";
        if (!xuLymem(str).booleanValue()) {
            return "Không hiểu " + str;
        }
        arr[0] = "00,10,20,30,40,50,60,70,80,90,";
        arr[1] = "01,11,21,31,41,51,61,71,81,91,";
        arr[2] = "02,12,22,32,42,52,62,72,82,92,";
        arr[3] = "03,13,23,33,43,53,63,73,83,93,";
        arr[4] = "04,14,24,34,44,54,64,74,84,94,";
        arr[5] = "05,15,25,35,45,55,65,75,85,95,";
        arr[6] = "06,16,26,36,46,56,66,76,86,96,";
        arr[7] = "07,17,27,37,47,57,67,77,87,97,";
        arr[8] = "08,18,28,38,48,58,68,78,88,98,";
        arr[9] = "09,19,29,39,49,59,69,79,89,99,";
        for (int i = 0; i < str.length(); i++) {
            if (isNumeric(str.substring(i, i + 1))) {
                str1 = str1 + arr[Character.getNumericValue(str.charAt(i))];
            }
        }
        return str1;
    }

    public static String GhepTong(String str) {
        String[] arr = new String[15];
        String str1 = "";
        if (!xuLymem(str).booleanValue()) {
            return "Không hiểu " + str;
        }
        arr[0] = "00,19,28,37,46,55,64,73,82,91,";
        arr[1] = "01,10,29,38,47,56,65,74,83,92,";
        arr[2] = "02,11,20,39,48,57,66,75,84,93,";
        arr[3] = "03,12,21,30,49,58,67,76,85,94,";
        arr[4] = "04,13,22,31,40,59,68,77,86,95,";
        arr[5] = "05,14,23,32,41,50,69,78,87,96,";
        arr[6] = "06,15,24,33,42,51,60,79,88,97,";
        arr[7] = "07,16,25,34,43,52,61,70,89,98,";
        arr[8] = "08,17,26,35,44,53,62,71,80,99,";
        arr[9] = "09,18,27,36,45,54,63,72,81,90,";
        for (int i = 0; i < str.length(); i++) {
            if (isNumeric(str.substring(i, i + 1))) {
                str1 = str1 + arr[Character.getNumericValue(str.charAt(i))];
            }
        }
        return str1;
    }

    public static String GhepBo(String str) {
        String str1 = "";
        String[] arr = {"00,050,55", "010,060,515,565", "020,070,525,575", "030,080,535,585", "040,090,545,595", "11,66,161", "121,171,626,676", "131,181,636,686", "141,191,646,696", "22,77,272", "232,282,737,787", "242,292,747,797", "33,88,383", "343,393,848,898", "44,494,99"};
        for (int i = 0; i < str.length() - 1; i++) {
            if (isNumeric(str.substring(i, i + 2))) {
                int j = 0;
                while (true) {
                    if (j >= arr.length) {
                        break;
                    } else if (arr[j].indexOf(str.substring(i, i + 2)) > -1) {
                        str1 = str1 + arr[j] + ",";
                        break;
                    } else {
                        j++;
                    }
                }
            }
        }
        return FixDan(str1);
    }

    public static boolean isNumeric(String str) {
        char[] charArray;
        Boolean B = true;
        if (str == null || str.length() == 0) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return B.booleanValue();
    }

    public static Boolean xuLymem(String str) {
        boolean check = true;
        if (str.length() == 0) {
            check = false;
        }
        String str2 = str.replaceAll(",", "").trim();
        int i = 0;
        while (true) {
            if (i >= 10) {
                break;
            }
            String ktra = str2.replaceAll(i + "", "");
            if (str2.length() - ktra.length() > 1) {
                check = false;
                break;
            }
            str2 = ktra;
            i++;
        }
        if (str2.length() > 0) {
            return false;
        }
        return check;
    }

    private static Date parseDate(String date) {
        SimpleDateFormat inputParser = new SimpleDateFormat("HH:mm", Locale.US);
        try {
            return inputParser.parse(date);
        } catch (ParseException e) {
            return new Date(0L);
        }
    }

    public static boolean CheckTime(String time) {
        Date gioKT = parseDate(time);
        Calendar now = Calendar.getInstance();
        int hour = now.get(11);
        int minute = now.get(12);
        Date date = parseDate(hour + ":" + minute);
        if (date.after(gioKT)) {
            return true;
        }
        return false;
    }

    public static boolean CheckDate(String time) {
        if (time == null) {
            time = "01/01/2018";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(time));
            c.add(5, 1);
            String Str = sdf.format(c.getTime());
            Date strDate = sdf.parse(Str);
            return new Date().before(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
