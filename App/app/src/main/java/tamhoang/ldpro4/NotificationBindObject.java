package tamhoang.ldpro4;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.webkit.JavascriptInterface;
import tamhoang.ldpro4.Fragment.TructiepXoso;

/* loaded from: classes2.dex */
public class NotificationBindObject {
    private Context mContext;

    public NotificationBindObject(Context context) {
        this.mContext = context;
    }

    @JavascriptInterface
    public void showNotification(String message) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.mContext).setContentText(message).setAutoCancel(true);
        Intent resultIntent = new Intent(this.mContext, MainActivity.class);
        resultIntent.putExtra(TructiepXoso.EXTRA_FROM_NOTIFICATION, true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this.mContext);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, 134217728);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) this.mContext.getSystemService("notification");
        mNotificationManager.notify(-1, mBuilder.build());
    }
}
