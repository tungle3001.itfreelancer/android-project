package tamhoang.ldpro4.Telegram;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

/* loaded from: classes2.dex */
public class TelegramClient {
    private static Client client;

    /* loaded from: classes2.dex */
    public interface Callback extends Client.ResultHandler {
        @Override // org.drinkless.td.libcore.telegram.Client.ResultHandler
        void onResult(TdApi.Object object);
    }

    private TelegramClient() {
    }

    public static Client getClient(Callback activity) {
        Client client2 = client;
        if (client2 == null) {
            try {
                client = Client.create(activity, null, null);
            } catch (Exception e) {
            }
        } else {
            client2.setUpdatesHandler(activity);
        }
        return client;
    }
}
