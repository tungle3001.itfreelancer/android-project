package tamhoang.ldpro4.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import org.json.JSONObject;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;
import tamhoang.ldpro4.services.SaveSmsService;

/* loaded from: classes2.dex */
public class SMSReceiver extends BroadcastReceiver {
    String Ten_KH;
    JSONObject caidat_gia;
    JSONObject caidat_tg;
    Database db;
    JSONObject json;
    Context mContext;
    String mGionhan;
    String mNgayNhan;
    String mSDT;
    int soTN;
    String body = "";
    SmsMessage[] messages = null;

    /* JADX WARN: Can't wrap try/catch for region: R(16:134|9|10|(3:128|12|(1:14)(12:15|(2:18|16)|142|19|23|(1:25)|119|26|(1:28)|30|31|(2:115|147)(12:41|137|42|(4:44|(1:46)|47|(1:49))|130|53|54|123|(10:121|56|57|133|58|132|64|65|125|(6:139|67|(1:69)(1:70)|71|(6:73|135|74|78|(1:82)|83)|(1:87))(5:126|89|(1:93)|94|(1:98)))(1:105)|(1:109)|112|148)))|22|23|(0)|119|26|(0)|30|31|(1:33)|39|115|147) */
    /* JADX WARN: Can't wrap try/catch for region: R(9:121|56|57|(2:133|58)|132|64|65|125|(6:139|67|(1:69)(1:70)|71|(6:73|135|74|78|(1:82)|83)|(1:87))(5:126|89|(1:93)|94|(1:98))) */
    /* JADX WARN: Code restructure failed: missing block: B:100:0x047a, code lost:
        r2 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:102:0x047e, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x047f, code lost:
        r18 = r5;
        r2 = r0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x048e, code lost:
        r2.printStackTrace();
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x0479, code lost:
        r0 = move-exception;
     */
    /* JADX WARN: Removed duplicated region for block: B:107:0x04a4 A[Catch: Exception -> 0x04ae, TryCatch #2 {Exception -> 0x04ae, blocks: (B:67:0x023d, B:69:0x027d, B:70:0x02c8, B:71:0x0312, B:73:0x031f, B:77:0x035e, B:78:0x03a4, B:80:0x03aa, B:82:0x03b3, B:83:0x03bd, B:85:0x03c2, B:87:0x03c8, B:89:0x03cf, B:91:0x0453, B:93:0x0459, B:94:0x045c, B:96:0x0462, B:98:0x046d, B:104:0x048e, B:107:0x04a4, B:109:0x04aa), top: B:123:0x01dc }] */
    /* JADX WARN: Removed duplicated region for block: B:126:0x03cf A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:139:0x023d A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0108 A[Catch: Exception -> 0x00d7, TRY_ENTER, TRY_LEAVE, TryCatch #5 {Exception -> 0x00d7, blocks: (B:12:0x00a7, B:15:0x00ae, B:16:0x00b7, B:18:0x00bc, B:19:0x00cc, B:25:0x0108, B:33:0x013f, B:35:0x0149, B:37:0x0153, B:42:0x0169, B:44:0x0173, B:47:0x01ad, B:49:0x01b3, B:56:0x01de, B:58:0x01e3), top: B:128:0x00a7 }] */
    /* JADX WARN: Removed duplicated region for block: B:28:0x012a A[Catch: Exception -> 0x0130, TRY_LEAVE, TryCatch #0 {Exception -> 0x0130, blocks: (B:26:0x0122, B:28:0x012a), top: B:119:0x0122 }] */
    @Override // android.content.BroadcastReceiver
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public void onReceive(Context r29, Intent r30) {
        /*
            Method dump skipped, instructions count: 1248
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.receivers.SMSReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }

    private void saveSmsInInbox(Context context, SmsMessage sms) {
        Intent serviceIntent = new Intent(context, SaveSmsService.class);
        serviceIntent.putExtra("sender_no", sms.getDisplayOriginatingAddress());
        serviceIntent.putExtra("message", sms.getDisplayMessageBody());
        serviceIntent.putExtra("date", sms.getTimestampMillis());
        context.startService(serviceIntent);
    }

    private void issueNotification(Context context, String senderNo, String message) {
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context).setLargeIcon(icon).setSmallIcon(R.mipmap.ic_launcher).setContentTitle(senderNo).setStyle(new NotificationCompat.BigTextStyle().bigText(message)).setAutoCancel(true).setContentText(message);
        NotificationManager mNotifyMgr = (NotificationManager) context.getSystemService("notification");
        mNotifyMgr.notify(101, mBuilder.build());
    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 23) {
            String format = bundle.getString("format");
            SmsMessage currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
            return currentSMS;
        }
        SmsMessage currentSMS2 = SmsMessage.createFromPdu((byte[]) aObject);
        return currentSMS2;
    }
}
