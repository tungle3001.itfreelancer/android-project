package tamhoang.ldpro4.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import tamhoang.ldpro4.R;

/* loaded from: classes2.dex */
public class DeliverReceiver extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent arg1) {
        if (getResultCode() == 0) {
            Toast.makeText(context, (int) R.string.sms_not_delivered, 0).show();
        }
    }
}
