package tamhoang.ldpro4.Activity;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import tamhoang.ldpro4.Congthuc.BaseToolBarActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Activity_ChuyenThang extends BaseToolBarActivity {
    Button add_chuyen;
    Database db;
    ListView lv_chuyenthang;
    RadioButton rad_chuyenthang;
    RadioButton rad_sauxuly;
    int sp_Chu;
    int sp_KH;
    Spinner spin_Chu;
    Spinner spin_KH;
    public List<String> nameKhach = new ArrayList();
    public List<String> sdtKhach = new ArrayList();
    public List<String> nameChu = new ArrayList();
    public List<String> sdtChu = new ArrayList();
    public List<String> ten_KH = new ArrayList();
    public List<String> sdt_KH = new ArrayList();
    public List<String> ten_Chu = new ArrayList();
    public List<String> sdt_Chu = new ArrayList();

    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity
    protected int getLayoutId() {
        return R.layout.activity_chuyenthang;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity, android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chuyenthang);
        this.db = new Database(this);
        this.add_chuyen = (Button) findViewById(R.id.add_Chuyenthang);
        this.spin_KH = (Spinner) findViewById(R.id.spinter_KH);
        this.spin_Chu = (Spinner) findViewById(R.id.spinter_Chu);
        this.lv_chuyenthang = (ListView) findViewById(R.id.lv_ChuyenThang);
        this.nameKhach.clear();
        this.sdtKhach.clear();
        Cursor cur = this.db.GetData("Select * From tbl_kh_new WHERE type_kh = 1 ORDER by ten_kh");
        if (cur != null && cur.getCount() > 0) {
            while (cur.moveToNext()) {
                this.nameKhach.add(cur.getString(0));
                this.sdtKhach.add(cur.getString(1));
            }
            cur.close();
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, (int) R.layout.spinner_item, this.nameKhach);
        this.spin_KH.setAdapter((SpinnerAdapter) adapter1);
        this.nameChu.clear();
        this.sdtChu.clear();
        Cursor cur2 = this.db.GetData("Select * From tbl_kh_new WHERE type_kh <> 1 ORDER by ten_kh");
        if (cur2 != null && cur2.getCount() > 0) {
            while (cur2.moveToNext()) {
                this.nameChu.add(cur2.getString(0));
                this.sdtChu.add(cur2.getString(1));
            }
            cur2.close();
        }
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, (int) R.layout.spinner_item, this.nameChu);
        this.spin_Chu.setAdapter((SpinnerAdapter) adapter2);
        this.spin_KH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.1
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Activity_ChuyenThang.this.sp_KH = position;
                Database database = Activity_ChuyenThang.this.db;
                Cursor cursor = database.GetData("Select * from tbl_chuyenthang where sdt_nhan = '" + Activity_ChuyenThang.this.sdtKhach.get(Activity_ChuyenThang.this.sp_KH) + "'");
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    int point = Activity_ChuyenThang.this.sdtChu.indexOf(cursor.getString(4));
                    Activity_ChuyenThang.this.spin_Chu.setSelection(point);
                    cursor.close();
                }
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        this.spin_Chu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.2
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Activity_ChuyenThang.this.sp_Chu = position;
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        this.add_chuyen.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.3
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                try {
                    Cursor cursor = Activity_ChuyenThang.this.db.GetData("Select * From tbl_chuyenthang WHERE kh_nhan = '" + Activity_ChuyenThang.this.nameKhach.get(Activity_ChuyenThang.this.sp_KH) + "'");
                    cursor.moveToFirst();
                    if (cursor.getCount() != 0 || Activity_ChuyenThang.this.sp_Chu <= -1) {
                        String Str = "UPDATE tbl_chuyenthang set kh_chuyen = '" + Activity_ChuyenThang.this.nameChu.get(Activity_ChuyenThang.this.sp_Chu) + "', sdt_chuyen = '" + Activity_ChuyenThang.this.sdtChu.get(Activity_ChuyenThang.this.sp_Chu) + "' Where sdt_nhan = '" + Activity_ChuyenThang.this.sdtKhach.get(Activity_ChuyenThang.this.sp_KH) + "'";
                        Activity_ChuyenThang.this.db.QueryData(Str);
                        Toast.makeText(Activity_ChuyenThang.this, "Đã sửa!", 1).show();
                    } else {
                        String Str2 = "Insert into tbl_chuyenthang Values (null, '" + Activity_ChuyenThang.this.nameKhach.get(Activity_ChuyenThang.this.sp_KH) + "', '" + Activity_ChuyenThang.this.sdtKhach.get(Activity_ChuyenThang.this.sp_KH) + "', '" + Activity_ChuyenThang.this.nameChu.get(Activity_ChuyenThang.this.sp_Chu) + "', '" + Activity_ChuyenThang.this.sdtChu.get(Activity_ChuyenThang.this.sp_Chu) + "')";
                        Activity_ChuyenThang.this.db.QueryData(Str2);
                        Toast.makeText(Activity_ChuyenThang.this, "Đã thêm!", 1).show();
                    }
                    cursor.close();
                } catch (Exception e) {
                    Toast.makeText(Activity_ChuyenThang.this, "Thêm lỗi!", 1).show();
                }
                Activity_ChuyenThang.this.xem_lv();
            }
        });
        this.lv_chuyenthang.setOnItemClickListener(new AdapterView.OnItemClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.4
            @Override // android.widget.AdapterView.OnItemClickListener
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int k_hang = Activity_ChuyenThang.this.nameKhach.indexOf(Activity_ChuyenThang.this.ten_KH.get(position));
                int chu_nhan = Activity_ChuyenThang.this.nameChu.indexOf(Activity_ChuyenThang.this.ten_Chu.get(position));
                Activity_ChuyenThang.this.spin_KH.setSelection(k_hang);
                Activity_ChuyenThang.this.spin_Chu.setSelection(chu_nhan);
            }
        });
        this.rad_chuyenthang = (RadioButton) findViewById(R.id.rad_chuyenngay);
        this.rad_sauxuly = (RadioButton) findViewById(R.id.rad_chuyensauxl);
        this.rad_chuyenthang.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.5
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_ChuyenThang.this.rad_chuyenthang.isChecked()) {
                    Activity_ChuyenThang.this.db.QueryData("UPDATE So_Om set Om_Xi3 = 0 WHERE ID = 13");
                }
            }
        });
        this.rad_sauxuly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_ChuyenThang.this.rad_sauxuly.isChecked()) {
                    Activity_ChuyenThang.this.db.QueryData("UPDATE So_Om set Om_Xi3 = 1 WHERE ID = 13");
                }
            }
        });
        Cursor chuyenthang = this.db.GetData("Select Om_Xi3 From so_om WHERE id = 13");
        if (chuyenthang != null && chuyenthang.moveToFirst()) {
            if (chuyenthang.getInt(0) == 0) {
                this.rad_chuyenthang.setChecked(true);
                this.rad_sauxuly.setChecked(false);
            } else {
                this.rad_sauxuly.setChecked(true);
                this.rad_chuyenthang.setChecked(false);
            }
            if (chuyenthang != null && !chuyenthang.isClosed()) {
                chuyenthang.close();
            }
        }
        xem_lv();
    }

    public void xem_lv() {
        this.ten_KH.clear();
        this.sdt_KH.clear();
        this.ten_Chu.clear();
        this.sdt_Chu.clear();
        Cursor cursor = this.db.GetData("Select * From tbl_chuyenthang");
        while (cursor.moveToNext()) {
            this.ten_KH.add(cursor.getString(1));
            this.sdt_KH.add(cursor.getString(2));
            this.ten_Chu.add(cursor.getString(3));
            this.sdt_Chu.add(cursor.getString(4));
        }
        cursor.close();
        this.lv_chuyenthang.setAdapter((ListAdapter) new CTAdapter(this, R.layout.activity_chuyenthang_lv, this.ten_KH));
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class CTAdapter extends ArrayAdapter {
        public CTAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.activity_chuyenthang_lv, (ViewGroup) null);
            TextView tvstt = (TextView) v.findViewById(R.id.tv_stt);
            tvstt.setText((position + 1) + "");
            TextView tview5 = (TextView) v.findViewById(R.id.tv_khach);
            tview5.setText(Activity_ChuyenThang.this.ten_KH.get(position));
            TextView tview7 = (TextView) v.findViewById(R.id.tv_chu);
            tview7.setText(Activity_ChuyenThang.this.ten_Chu.get(position));
            TextView tv_delete = (TextView) v.findViewById(R.id.tv_delete);
            tv_delete.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_ChuyenThang.CTAdapter.1
                @Override // android.view.View.OnClickListener
                public void onClick(View v2) {
                    Database database = Activity_ChuyenThang.this.db;
                    database.QueryData("Delete FROM tbl_chuyenthang WHERE sdt_nhan = '" + Activity_ChuyenThang.this.sdt_KH.get(position) + "'");
                    Activity_ChuyenThang.this.xem_lv();
                }
            });
            return v;
        }
    }
}
