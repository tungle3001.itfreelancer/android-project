package tamhoang.ldpro4.Activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Congthuc.BaseToolBarActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Activity_AddKH extends BaseToolBarActivity {
    String app_use;
    Button btn_danhba;
    Button btn_them_KH;
    JSONObject caidat_gia;
    JSONObject caidat_tg;
    Cursor cursor;
    Database db;
    EditText edt_an3c;
    EditText edt_anLo;
    EditText edt_anXN;
    EditText edt_andea;
    EditText edt_andeb;
    EditText edt_andec;
    EditText edt_anded;
    EditText edt_andet;
    EditText edt_anx2;
    EditText edt_anx3;
    EditText edt_anx4;
    EditText edt_gia3c;
    EditText edt_giaXN;
    EditText edt_giadea;
    EditText edt_giadeb;
    EditText edt_giadec;
    EditText edt_giaded;
    EditText edt_giadet;
    EditText edt_gialo;
    EditText edt_giax2;
    EditText edt_giax3;
    EditText edt_giax4;
    EditText edt_sdt;
    EditText edt_ten;
    JSONObject json;
    JSONObject json_KhongMax;
    LinearLayout linner_sodienthoai;
    RadioButton rad_chu;
    RadioButton rad_chu_khach;
    RadioButton rad_khach;
    String so_dienthoai;
    String ten_khach;
    int type;

    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity
    protected int getLayoutId() {
        return R.layout.activity_add_kh;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity, android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kh);
        this.db = new Database(this);
        init();
        Intent intent = getIntent();
        this.ten_khach = intent.getStringExtra("tenKH");
        this.so_dienthoai = intent.getStringExtra("so_dienthoai");
        this.app_use = intent.getStringExtra("use_app");
        this.db = new Database(this);
        int Khachhang_moi = 0;
        if (this.ten_khach.length() > 0) {
            this.edt_ten.setText(this.ten_khach);
            this.edt_sdt.setText(this.so_dienthoai);
            Database database = this.db;
            Cursor GetData = database.GetData("Select * From tbl_kh_new where ten_kh = '" + this.ten_khach + "'");
            this.cursor = GetData;
            GetData.moveToFirst();
            Khachhang_moi = this.cursor.getCount();
        }
        if (Khachhang_moi > 0) {
            this.edt_sdt.setText(this.cursor.getString(1));
            if (this.cursor.getString(2).indexOf("sms") == -1) {
                this.linner_sodienthoai.setEnabled(false);
                this.edt_ten.setEnabled(false);
                this.edt_sdt.setEnabled(false);
                this.btn_danhba.setEnabled(false);
            }
            if (this.cursor.getCount() > 0) {
                try {
                    JSONObject jSONObject = new JSONObject(this.cursor.getString(5));
                    this.json = jSONObject;
                    JSONObject jSONObject2 = jSONObject.getJSONObject("caidat_gia");
                    this.caidat_gia = jSONObject2;
                    this.edt_giadea.setText(jSONObject2.getString("dea"));
                    this.edt_andea.setText(this.caidat_gia.getString("an_dea"));
                    this.edt_giadeb.setText(this.caidat_gia.getString("deb"));
                    this.edt_andeb.setText(this.caidat_gia.getString("an_deb"));
                    this.edt_giadec.setText(this.caidat_gia.getString("dec"));
                    this.edt_andec.setText(this.caidat_gia.getString("an_dec"));
                    this.edt_giaded.setText(this.caidat_gia.getString("ded"));
                    this.edt_anded.setText(this.caidat_gia.getString("an_ded"));
                    this.edt_giadet.setText(this.caidat_gia.getString("det"));
                    this.edt_andet.setText(this.caidat_gia.getString("an_det"));
                    this.edt_gialo.setText(this.caidat_gia.getString("lo"));
                    this.edt_anLo.setText(this.caidat_gia.getString("an_lo"));
                    this.edt_giax2.setText(this.caidat_gia.getString("gia_x2"));
                    this.edt_anx2.setText(this.caidat_gia.getString("an_x2"));
                    this.edt_giax3.setText(this.caidat_gia.getString("gia_x3"));
                    this.edt_anx3.setText(this.caidat_gia.getString("an_x3"));
                    this.edt_giax4.setText(this.caidat_gia.getString("gia_x4"));
                    this.edt_anx4.setText(this.caidat_gia.getString("an_x4"));
                    this.edt_giaXN.setText(this.caidat_gia.getString("gia_xn"));
                    this.edt_anXN.setText(this.caidat_gia.getString("an_xn"));
                    this.edt_gia3c.setText(this.caidat_gia.getString("gia_bc"));
                    this.edt_an3c.setText(this.caidat_gia.getString("an_bc"));
                    if (this.cursor.getInt(3) == 1) {
                        this.rad_khach.setChecked(true);
                        this.rad_chu.setChecked(false);
                        this.rad_chu_khach.setChecked(false);
                    } else if (this.cursor.getInt(3) == 2) {
                        this.rad_khach.setChecked(false);
                        this.rad_chu.setChecked(true);
                        this.rad_chu_khach.setChecked(false);
                    } else if (this.cursor.getInt(3) == 3) {
                        this.rad_khach.setChecked(false);
                        this.rad_chu.setChecked(false);
                        this.rad_chu_khach.setChecked(true);
                    }
                    this.json_KhongMax = new JSONObject(this.cursor.getString(6));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Cursor cursor = this.cursor;
                if (cursor != null && !cursor.isClosed()) {
                    this.cursor.close();
                }
            }
        } else if (this.app_use.indexOf("sms") <= -1) {
            this.edt_ten.setText(this.ten_khach);
            this.edt_sdt.setText(this.so_dienthoai);
            this.edt_ten.setEnabled(false);
            this.edt_sdt.setEnabled(false);
            this.btn_danhba.setEnabled(false);
        }
        this.btn_danhba.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_AddKH.1
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                Activity_AddKH.this.startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI), 2015);
            }
        });
        this.btn_them_KH.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_AddKH.2
            /* JADX WARN: Removed duplicated region for block: B:70:0x0582 A[Catch: Exception -> 0x05dc, TRY_LEAVE, TryCatch #10 {Exception -> 0x05dc, blocks: (B:68:0x0534, B:70:0x0582), top: B:134:0x0534 }] */
            /* JADX WARN: Removed duplicated region for block: B:73:0x05b0  */
            /* JADX WARN: Removed duplicated region for block: B:76:0x05be A[Catch: Exception -> 0x05d9, TryCatch #11 {Exception -> 0x05d9, blocks: (B:72:0x05a5, B:74:0x05b4, B:76:0x05be, B:77:0x05cb), top: B:136:0x05a5 }] */
            /* JADX WARN: Removed duplicated region for block: B:77:0x05cb A[Catch: Exception -> 0x05d9, TRY_LEAVE, TryCatch #11 {Exception -> 0x05d9, blocks: (B:72:0x05a5, B:74:0x05b4, B:76:0x05be, B:77:0x05cb), top: B:136:0x05a5 }] */
            @Override // android.view.View.OnClickListener
            /*
                Code decompiled incorrectly, please refer to instructions dump.
                To view partially-correct code enable 'Show inconsistent code' option in preferences
            */
            public void onClick(View r36) {
                /*
                    Method dump skipped, instructions count: 1652
                    To view this dump change 'Code comments level' option to 'DEBUG'
                */
                throw new UnsupportedOperationException("Method not decompiled: tamhoang.ldpro4.Activity.Activity_AddKH.AnonymousClass2.onClick(android.view.View):void");
            }
        });
    }

    public AlertDialog.Builder showAlertBox(String title, String message) {
        return new AlertDialog.Builder(this).setTitle(title).setMessage(message);
    }

    @Override // android.support.v4.app.FragmentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 2015 && i2 == -1) {
            Cursor query = getContentResolver().query(intent.getData(), null, null, null, null);
            query.moveToFirst();
            int columnIndex = query.getColumnIndex("data1");
            query.getColumnIndex("display_name");
            String name = query.getString(query.getColumnIndex("display_name"));
            String sdt = query.getString(columnIndex).replaceAll(" ", "");
            if (sdt.length() < 12) {
                sdt = "+84" + sdt.substring(1);
            }
            this.edt_sdt.setText(sdt);
            this.edt_ten.setText(name);
        }
    }

    public void init() {
        this.linner_sodienthoai = (LinearLayout) findViewById(R.id.linner_sodienthoai);
        this.edt_ten = (EditText) findViewById(R.id.edt_ten);
        this.edt_sdt = (EditText) findViewById(R.id.edt_sdt);
        this.rad_chu = (RadioButton) findViewById(R.id.rad_chu);
        this.rad_khach = (RadioButton) findViewById(R.id.rad_khach);
        this.rad_chu_khach = (RadioButton) findViewById(R.id.rad_chu_khach);
        this.edt_giadea = (EditText) findViewById(R.id.edt_giadea);
        this.edt_andea = (EditText) findViewById(R.id.edt_andea);
        this.edt_giadeb = (EditText) findViewById(R.id.edt_giadeb);
        this.edt_andeb = (EditText) findViewById(R.id.edt_andeb);
        this.edt_giadec = (EditText) findViewById(R.id.edt_giadec);
        this.edt_andec = (EditText) findViewById(R.id.edt_andec);
        this.edt_giaded = (EditText) findViewById(R.id.edt_giaded);
        this.edt_anded = (EditText) findViewById(R.id.edt_anded);
        this.edt_giadet = (EditText) findViewById(R.id.edt_giadet);
        this.edt_andet = (EditText) findViewById(R.id.edt_andet);
        this.edt_gialo = (EditText) findViewById(R.id.edt_giaLo);
        this.edt_anLo = (EditText) findViewById(R.id.edt_anLo);
        this.edt_giax2 = (EditText) findViewById(R.id.edt_giaXien2);
        this.edt_anx2 = (EditText) findViewById(R.id.edt_anXien2);
        this.edt_giax3 = (EditText) findViewById(R.id.edt_giaXien3);
        this.edt_anx3 = (EditText) findViewById(R.id.edt_anXien3);
        this.edt_giax4 = (EditText) findViewById(R.id.edt_giaXien4);
        this.edt_anx4 = (EditText) findViewById(R.id.edt_anXien4);
        this.edt_giaXN = (EditText) findViewById(R.id.edt_giaXienNhay);
        this.edt_anXN = (EditText) findViewById(R.id.edt_anXienNhay);
        this.edt_gia3c = (EditText) findViewById(R.id.edt_gia3c);
        this.edt_an3c = (EditText) findViewById(R.id.edt_an3c);
        this.btn_them_KH = (Button) findViewById(R.id.btn_them_KH);
        this.btn_danhba = (Button) findViewById(R.id.btn_danhba);
    }
}
