package tamhoang.ldpro4.Activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.receivers.DeliverReceiver;
import tamhoang.ldpro4.receivers.SentReceiver;

/* loaded from: classes2.dex */
public class NewSMSActivity extends AppCompatActivity implements View.OnClickListener {
    private String message;
    private String phoneNo;
    private EditText txtMessage;
    private EditText txtphoneNo;
    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliveryBroadcastReciever = new DeliverReceiver();

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_sms_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        Button sendBtn = (Button) findViewById(R.id.btnSendSMS);
        this.txtphoneNo = (EditText) findViewById(R.id.editText);
        this.txtMessage = (EditText) findViewById(R.id.editText2);
        ImageButton contact = (ImageButton) findViewById(R.id.contact);
        contact.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.NewSMSActivity.1
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                NewSMSActivity.this.startActivityForResult(intent, 85);
            }
        });
        sendBtn.setOnClickListener(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        if (view.getId() == R.id.btnSendSMS) {
            this.phoneNo = this.txtphoneNo.getText().toString();
            this.message = this.txtMessage.getText().toString();
            String str = this.phoneNo;
            if (str == null || str.trim().length() <= 0) {
                this.txtphoneNo.setError(getString(R.string.please_write_number));
                return;
            }
            String str2 = this.message;
            if (str2 == null || str2.trim().length() <= 0) {
                this.txtMessage.setError(getString(R.string.please_write_message));
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        try {
            unregisterReceiver(this.sendBroadcastReceiver);
            unregisterReceiver(this.deliveryBroadcastReciever);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pickContact(View v) {
        Intent contactPickerIntent = new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(contactPickerIntent, 85);
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity, android.app.Activity
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != -1) {
            Log.e("MainActivity", "Failed to pick contact");
        } else if (requestCode == 85) {
            contactPicked(data);
        }
    }

    private void contactPicked(Intent data) {
        try {
            Uri uri = data.getData();
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int phoneIndex = cursor.getColumnIndex("data1");
            int nameIndex = cursor.getColumnIndex("display_name");
            String phoneNo = cursor.getString(phoneIndex);
            cursor.getString(nameIndex);
            this.txtphoneNo.setText(phoneNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
