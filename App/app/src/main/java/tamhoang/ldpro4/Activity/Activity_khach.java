package tamhoang.ldpro4.Activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import tamhoang.ldpro4.Congthuc.BaseToolBarActivity;
import tamhoang.ldpro4.MainActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Activity_khach extends BaseToolBarActivity {
    String DangXuat;
    Database db;
    ListView lv_khach;
    String message;
    RadioButton radio_bc;
    RadioButton radio_de;
    RadioButton radio_lo;
    RadioButton radio_xi;
    TextView textView;
    private List<String> mSo = new ArrayList();
    private List<String> mDiem = new ArrayList();
    private List<String> mDiemGiu = new ArrayList();
    private List<String> mThanhTien = new ArrayList();
    private List<Integer> mNhay = new ArrayList();

    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity
    protected int getLayoutId() {
        return R.layout.activity_khach;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity, android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_khach);
        this.db = new Database(this);
        init();
        Intent intent = getIntent();
        this.message = intent.getStringExtra("tenKH");
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText("Khách hàng: " + this.message);
        this.radio_de.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_khach.1
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_khach.this.radio_de.isChecked()) {
                    Activity_khach.this.DangXuat = "(the_loai = 'deb' or the_loai = 'det')";
                    Activity_khach.this.lv_Khach();
                }
            }
        });
        this.radio_lo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_khach.2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_khach.this.radio_lo.isChecked()) {
                    Activity_khach.this.DangXuat = "the_loai = 'lo'";
                    Activity_khach.this.lv_Khach();
                }
            }
        });
        this.radio_xi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_khach.3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_khach.this.radio_xi.isChecked()) {
                    Activity_khach.this.DangXuat = "the_loai = 'xi'";
                    Activity_khach.this.lv_Khach();
                }
            }
        });
        this.radio_bc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: tamhoang.ldpro4.Activity.Activity_khach.4
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Activity_khach.this.radio_bc.isChecked()) {
                    Activity_khach.this.DangXuat = "the_loai = 'bc'";
                    Activity_khach.this.lv_Khach();
                }
            }
        });
        this.radio_de.setChecked(true);
    }

    public void lv_Khach() {
        new MainActivity();
        String mDate = MainActivity.Get_date();
        String str = "Select so_chon, sum(diem_quydoi) as diem, sum(diem_khachgiu*diem_quydoi)/100 as diemgiu, sum((100-diem_khachgiu)*diem_quydoi/100) as diemton, so_nhay From tbl_soctS where ngay_nhan = '" + mDate + "' AND ten_kh = '" + this.message + "' AND " + this.DangXuat + " GRoup by so_chon order by diem DESC;";
        Cursor cursor = this.db.GetData(str);
        this.mSo.clear();
        this.mDiem.clear();
        this.mDiemGiu.clear();
        this.mThanhTien.clear();
        this.mNhay.clear();
        DecimalFormat decimalFormat = new DecimalFormat("###,###");
        while (cursor.moveToNext()) {
            this.mSo.add(cursor.getString(0));
            this.mDiem.add(decimalFormat.format(cursor.getDouble(1)));
            this.mDiemGiu.add(decimalFormat.format(cursor.getDouble(2)));
            this.mThanhTien.add(decimalFormat.format(cursor.getDouble(3)));
            this.mNhay.add(Integer.valueOf(cursor.getInt(4)));
        }
        this.lv_khach.setAdapter((ListAdapter) new Khach_Adapter(this, R.layout.activity_khach_lv, this.mSo));
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public class Khach_Adapter extends ArrayAdapter {
        private ViewHolder holder;
        private LayoutInflater mInflater;

        public Khach_Adapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
            this.mInflater = LayoutInflater.from(context);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View v, ViewGroup parent) {
            if (v == null) {
                v = this.mInflater.inflate(R.layout.activity_khach_lv, (ViewGroup) null);
                ViewHolder viewHolder = new ViewHolder();
                this.holder = viewHolder;
                viewHolder.stt = (TextView) v.findViewById(R.id.stt);
                this.holder.sochon = (TextView) v.findViewById(R.id.Tv_so);
                this.holder.tv_diemnhan = (TextView) v.findViewById(R.id.tv_diemNhan);
                this.holder.tv_diemgiu = (TextView) v.findViewById(R.id.tv_diemGiu);
                this.holder.tv_thanhtien = (TextView) v.findViewById(R.id.tv_thanhtien);
                v.setTag(this.holder);
            } else {
                this.holder = (ViewHolder) v.getTag();
            }
            if (((Integer) Activity_khach.this.mNhay.get(position)).intValue() > 0) {
                this.holder.sochon.setTextColor(SupportMenu.CATEGORY_MASK);
                this.holder.tv_diemnhan.setTextColor(SupportMenu.CATEGORY_MASK);
                this.holder.tv_diemgiu.setTextColor(SupportMenu.CATEGORY_MASK);
                this.holder.tv_thanhtien.setTextColor(SupportMenu.CATEGORY_MASK);
                if (((Integer) Activity_khach.this.mNhay.get(position)).intValue() == 1) {
                    TextView textView = this.holder.sochon;
                    textView.setText(((String) Activity_khach.this.mSo.get(position)) + "*");
                } else if (((Integer) Activity_khach.this.mNhay.get(position)).intValue() == 2) {
                    TextView textView2 = this.holder.sochon;
                    textView2.setText(((String) Activity_khach.this.mSo.get(position)) + "**");
                } else if (((Integer) Activity_khach.this.mNhay.get(position)).intValue() == 3) {
                    TextView textView3 = this.holder.sochon;
                    textView3.setText(((String) Activity_khach.this.mSo.get(position)) + "***");
                } else if (((Integer) Activity_khach.this.mNhay.get(position)).intValue() == 4) {
                    TextView textView4 = this.holder.sochon;
                    textView4.setText(((String) Activity_khach.this.mSo.get(position)) + "****");
                }
                TextView textView5 = this.holder.stt;
                textView5.setText((position + 1) + "");
                this.holder.tv_diemnhan.setText((CharSequence) Activity_khach.this.mDiem.get(position));
                this.holder.tv_diemgiu.setText((CharSequence) Activity_khach.this.mDiemGiu.get(position));
                this.holder.tv_thanhtien.setText((CharSequence) Activity_khach.this.mThanhTien.get(position));
            } else {
                this.holder.sochon.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.holder.tv_diemnhan.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.holder.tv_diemgiu.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.holder.tv_thanhtien.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                TextView textView6 = this.holder.stt;
                textView6.setText((position + 1) + "");
                this.holder.sochon.setText((CharSequence) Activity_khach.this.mSo.get(position));
                this.holder.tv_diemnhan.setText((CharSequence) Activity_khach.this.mDiem.get(position));
                this.holder.tv_diemgiu.setText((CharSequence) Activity_khach.this.mDiemGiu.get(position));
                this.holder.tv_thanhtien.setText((CharSequence) Activity_khach.this.mThanhTien.get(position));
            }
            return v;
        }

        /* loaded from: classes2.dex */
        public class ViewHolder {
            private TextView sochon;
            private TextView stt;
            private TextView tv_diemgiu;
            private TextView tv_diemnhan;
            private TextView tv_thanhtien;

            public ViewHolder() {
            }
        }
    }

    public void init() {
        this.radio_de = (RadioButton) findViewById(R.id.radio_de);
        this.radio_lo = (RadioButton) findViewById(R.id.radio_lo);
        this.radio_xi = (RadioButton) findViewById(R.id.radio_xi);
        this.radio_bc = (RadioButton) findViewById(R.id.radio_bc);
        this.lv_khach = (ListView) findViewById(R.id.lv_khach);
        this.textView = (TextView) findViewById(R.id.textView);
    }
}
