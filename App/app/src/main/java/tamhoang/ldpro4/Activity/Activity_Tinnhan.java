package tamhoang.ldpro4.Activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Html;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import tamhoang.ldpro4.Congthuc.BaseToolBarActivity;
import tamhoang.ldpro4.R;
import tamhoang.ldpro4.data.Database;

/* loaded from: classes2.dex */
public class Activity_Tinnhan extends BaseToolBarActivity {
    Button btn_suatin;
    Button btn_xoatin;
    Database db;
    EditText editText_suatin;
    JSONObject json;
    ListView lv_suatin;
    int typeKH;
    String id = "";
    private List<String> mDanGoc = new ArrayList();
    private List<String> mPhantich = new ArrayList();
    String ngay_nhan = "";
    String tenKH = "";
    String soTN = "";
    int lv_position = -1;

    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity
    protected int getLayoutId() {
        return R.layout.activity_tinnhan;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // tamhoang.ldpro4.Congthuc.BaseToolBarActivity, android.support.v7.app.AppCompatActivity, android.support.v4.app.FragmentActivity, android.support.v4.app.SupportActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinnhan);
        Intent intent = getIntent();
        this.id = intent.getStringExtra("m_ID");
        this.db = new Database(this);
        this.btn_suatin = (Button) findViewById(R.id.btn_suatin_suatin);
        this.btn_xoatin = (Button) findViewById(R.id.btn_suatin_xoatin);
        this.editText_suatin = (EditText) findViewById(R.id.editText_suatin);
        this.lv_suatin = (ListView) findViewById(R.id.lv_suatin);
        Database database = this.db;
        Cursor cursor = database.GetData("Select * From tbl_tinnhanS WHere id = " + this.id);
        cursor.moveToFirst();
        if (cursor.getString(6).indexOf("ChayTrang") > -1) {
            Toast.makeText(this, "Không sửa được tin chạy vào trang", 0).show();
            cursor.close();
            finish();
            return;
        }
        this.ngay_nhan = cursor.getString(1);
        this.tenKH = cursor.getString(4);
        this.soTN = cursor.getString(7);
        this.typeKH = cursor.getInt(3);
        if (cursor.getString(11).indexOf("ok") > -1) {
            try {
                this.mDanGoc.clear();
                this.mPhantich.clear();
                this.json = new JSONObject(cursor.getString(15));
                this.editText_suatin.setText(cursor.getString(9));
                Iterator<String> keys = this.json.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    JSONObject dan = this.json.getJSONObject(key);
                    List<String> list = this.mDanGoc;
                    list.add(dan.getString("du_lieu") + " (" + dan.getString("so_luong") + ")");
                    List<String> list2 = this.mPhantich;
                    list2.add(dan.getString("dan_so") + "x" + dan.getString("so_tien"));
                }
                this.lv_suatin.setAdapter((ListAdapter) new TN_Adapter(this, R.layout.frag_suatin_lv1, this.mDanGoc));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            this.editText_suatin.setText(cursor.getString(9));
        }
        InputMethodManager imm = (InputMethodManager) getSystemService("input_method");
        View view = getCurrentFocus();
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        this.btn_suatin.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_Tinnhan.1
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                Database database2 = Activity_Tinnhan.this.db;
                database2.QueryData("DELETE FROM tbl_soctS WHERE ngay_nhan = '" + Activity_Tinnhan.this.ngay_nhan + "' AND ten_kh = '" + Activity_Tinnhan.this.tenKH + "'  AND so_tin_nhan = " + Activity_Tinnhan.this.soTN + " And type_kh = " + Activity_Tinnhan.this.typeKH);
                StringBuilder sb = new StringBuilder();
                sb.append("Update tbl_tinnhanS Set nd_phantich = '");
                sb.append(Activity_Tinnhan.this.editText_suatin.getText().toString());
                sb.append("', phat_hien_loi = 'ko' WHERE id = ");
                sb.append(Activity_Tinnhan.this.id);
                String S = sb.toString();
                Activity_Tinnhan.this.db.QueryData(S);
                try {
                    Activity_Tinnhan.this.db.Update_TinNhanGoc(Integer.parseInt(Activity_Tinnhan.this.id), Activity_Tinnhan.this.typeKH);
                } catch (Exception e2) {
                    Toast.makeText(Activity_Tinnhan.this, "Đã xảy ra lỗi!", 1).show();
                }
                Database database3 = Activity_Tinnhan.this.db;
                Cursor cur = database3.GetData("Select * FROM tbl_tinnhanS Where id = " + Activity_Tinnhan.this.id);
                cur.moveToFirst();
                if (cur.getString(11).indexOf("Không hiểu") > -1) {
                    String str1 = cur.getString(10).replace("ldpro", "<font color='#FF0000'>");
                    Activity_Tinnhan.this.editText_suatin.setText(Html.fromHtml(str1));
                    if (cur.getString(10).indexOf("ldpro") > -1) {
                        Activity_Tinnhan.this.editText_suatin.setSelection(cur.getString(10).indexOf("ldpro"));
                    }
                    Activity_Tinnhan.this.mDanGoc.clear();
                    Activity_Tinnhan.this.mPhantich.clear();
                    ListView listView = Activity_Tinnhan.this.lv_suatin;
                    Activity_Tinnhan activity_Tinnhan = Activity_Tinnhan.this;
                    listView.setAdapter((ListAdapter) new TN_Adapter(activity_Tinnhan, R.layout.frag_suatin_lv1, activity_Tinnhan.mDanGoc));
                    return;
                }
                Activity_Tinnhan.this.editText_suatin.setText(cur.getString(9));
                Activity_Tinnhan.this.mDanGoc.clear();
                Activity_Tinnhan.this.mPhantich.clear();
                try {
                    Activity_Tinnhan.this.json = new JSONObject(cur.getString(15));
                    Iterator<String> keys2 = Activity_Tinnhan.this.json.keys();
                    while (keys2.hasNext()) {
                        String key2 = keys2.next();
                        JSONObject dan2 = Activity_Tinnhan.this.json.getJSONObject(key2);
                        List list3 = Activity_Tinnhan.this.mDanGoc;
                        list3.add(dan2.getString("du_lieu") + " (" + dan2.getString("so_luong") + ")");
                        List list4 = Activity_Tinnhan.this.mPhantich;
                        list4.add(dan2.getString("dan_so") + "x" + dan2.getString("so_tien"));
                    }
                    Activity_Tinnhan.this.lv_suatin.setAdapter((ListAdapter) new TN_Adapter(Activity_Tinnhan.this, R.layout.frag_suatin_lv1, Activity_Tinnhan.this.mDanGoc));
                } catch (JSONException e3) {
                    e3.printStackTrace();
                }
            }
        });
        this.lv_suatin.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_Tinnhan.2
            @Override // android.widget.AdapterView.OnItemLongClickListener
            public boolean onItemLongClick(AdapterView<?> parent, View view2, int position, long id) {
                Activity_Tinnhan.this.lv_position = position;
                return false;
            }
        });
        this.btn_xoatin.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_Tinnhan.3
            @Override // android.view.View.OnClickListener
            public void onClick(View v) {
                Activity_Tinnhan.this.finish();
            }
        });
        registerForContextMenu(this.lv_suatin);
        cursor.close();
    }

    @Override // android.app.Activity, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 1, 0, "Copy ?");
    }

    @Override // android.app.Activity
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        if (item.getItemId() == 1) {
            ClipboardManager clipboard = (ClipboardManager) getSystemService("clipboard");
            ClipData clip = ClipData.newPlainText("Tin chốt:", this.mDanGoc.get(this.lv_position) + "\n" + this.mPhantich.get(this.lv_position));
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, "Đã copy thành công", 1).show();
        }
        return true;
    }

    /* loaded from: classes2.dex */
    class TN_Adapter extends ArrayAdapter {
        public TN_Adapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override // android.widget.ArrayAdapter, android.widget.Adapter
        public View getView(int position, View v, ViewGroup parent) {
            View v2 = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.frag_suatin_lv1, (ViewGroup) null);
            TextView tv_dangoc = (TextView) v2.findViewById(R.id.dan_goc);
            final TextView tv_danpt = (TextView) v2.findViewById(R.id.dan_phantich);
            tv_dangoc.setText((CharSequence) Activity_Tinnhan.this.mDanGoc.get(position));
            tv_dangoc.setOnClickListener(new View.OnClickListener() { // from class: tamhoang.ldpro4.Activity.Activity_Tinnhan.TN_Adapter.1
                @Override // android.view.View.OnClickListener
                public void onClick(View v3) {
                    if (tv_danpt.getVisibility() == 0) {
                        tv_danpt.setVisibility(8);
                    } else {
                        tv_danpt.setVisibility(0);
                    }
                }
            });
            tv_danpt.setText((CharSequence) Activity_Tinnhan.this.mPhantich.get(position));
            tv_danpt.setVisibility(8);
            return v2;
        }
    }
}
