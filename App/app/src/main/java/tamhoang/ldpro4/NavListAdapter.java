package tamhoang.ldpro4;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

/* loaded from: classes2.dex */
public class NavListAdapter extends ArrayAdapter<NavItem> {
    Context context;
    List<NavItem> listNavItems;
    int resLayout;

    public NavListAdapter(Context context, int resLayout, List<NavItem> listNavItems) {
        super(context, resLayout, listNavItems);
        this.context = context;
        this.resLayout = resLayout;
        this.listNavItems = listNavItems;
    }

    @Override // android.widget.ArrayAdapter, android.widget.Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = View.inflate(this.context, this.resLayout, null);
        TextView tvTitle = (TextView) v.findViewById(R.id.tittle);
        TextView tvSubTitle = (TextView) v.findViewById(R.id.sub_tittle);
        ImageView navIcon = (ImageView) v.findViewById(R.id.icon);
        NavItem navItem = this.listNavItems.get(position);
        tvTitle.setText(navItem.getTitle());
        tvSubTitle.setText(navItem.getSubtitle());
        navIcon.setImageResource(navItem.getResIcons());
        return v;
    }
}
