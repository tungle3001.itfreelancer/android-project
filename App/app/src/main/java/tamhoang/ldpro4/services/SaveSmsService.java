package tamhoang.ldpro4.services;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import tamhoang.ldpro4.constants.SmsContract;

/* loaded from: classes2.dex */
public class SaveSmsService extends IntentService {
    public SaveSmsService() {
        super("SaveService");
    }

    @Override // android.app.IntentService
    protected void onHandleIntent(Intent intent) {
        String senderNo = intent.getStringExtra("sender_no");
        String message = intent.getStringExtra("message");
        long time = intent.getLongExtra("date", 0L);
        ContentValues values = new ContentValues();
        values.put("address", senderNo);
        values.put("body", message);
        values.put("date_sent", Long.valueOf(time));
        getContentResolver().insert(SmsContract.ALL_SMS_URI, values);
        Intent i = new Intent("android.intent.action.MAIN").putExtra("new_sms", true);
        sendBroadcast(i);
    }
}
