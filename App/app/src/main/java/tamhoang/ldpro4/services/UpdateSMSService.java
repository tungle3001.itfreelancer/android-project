package tamhoang.ldpro4.services;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import tamhoang.ldpro4.constants.Constants;

/* loaded from: classes2.dex */
public class UpdateSMSService extends IntentService {
    public UpdateSMSService() {
        super("UpdateSMSReceiver");
    }

    @Override // android.app.IntentService
    protected void onHandleIntent(Intent intent) {
        markSmsRead(intent.getLongExtra("id", -123L));
    }

    public void markSmsRead(long messageId) {
        try {
            ContentValues cv = new ContentValues();
            cv.put(Constants.READ, "1");
            ContentResolver contentResolver = getContentResolver();
            contentResolver.update(Uri.parse("content://sms/" + messageId), cv, null, null);
        } catch (Exception e) {
        }
    }
}
