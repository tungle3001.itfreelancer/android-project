package okhttp3.internal.http;

import kotlin.Metadata;
import okhttp3.Interceptor;

/* compiled from: CallServerInterceptor.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lokhttp3/internal/http/CallServerInterceptor;", "Lokhttp3/Interceptor;", "forWebSocket", "", "(Z)V", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "okhttp"}, k = 1, mv = {1, 4, 1})
/* loaded from: classes2.dex */
public final class CallServerInterceptor implements Interceptor {
    private final boolean forWebSocket;

    public CallServerInterceptor(boolean forWebSocket) {
        this.forWebSocket = forWebSocket;
    }

    /* JADX WARN: Removed duplicated region for block: B:61:0x0196 A[Catch: IOException -> 0x01d5, TryCatch #1 {IOException -> 0x01d5, blocks: (B:42:0x0135, B:46:0x0141, B:47:0x0150, B:48:0x0164, B:50:0x0176, B:54:0x0185, B:59:0x0190, B:61:0x0196, B:65:0x01a3, B:67:0x01bd, B:69:0x01c7, B:70:0x01d3), top: B:86:0x0135 }] */
    /* JADX WARN: Removed duplicated region for block: B:62:0x019b  */
    /* JADX WARN: Removed duplicated region for block: B:65:0x01a3 A[Catch: IOException -> 0x01d5, TryCatch #1 {IOException -> 0x01d5, blocks: (B:42:0x0135, B:46:0x0141, B:47:0x0150, B:48:0x0164, B:50:0x0176, B:54:0x0185, B:59:0x0190, B:61:0x0196, B:65:0x01a3, B:67:0x01bd, B:69:0x01c7, B:70:0x01d3), top: B:86:0x0135 }] */
    /* JADX WARN: Removed duplicated region for block: B:76:0x01dc  */
    /* JADX WARN: Removed duplicated region for block: B:78:0x01e9  */
    @Override // okhttp3.Interceptor
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public okhttp3.Response intercept(Chain r19) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 505
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okhttp3.internal.http.CallServerInterceptor.intercept(okhttp3.Interceptor$Chain):okhttp3.Response");
    }
}
