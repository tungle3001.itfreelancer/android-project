package org.jsoup.nodes;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import kotlin.text.Typography;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;

/* loaded from: classes2.dex */
public class Attribute implements Map.Entry<String, String>, Cloneable {
    private static final String[] booleanAttributes = {"allowfullscreen", "async", "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", "hidden", "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch"};
    private String key;
    Attributes parent;
    private String val;

    public Attribute(String key, String value) {
        this(key, value, null);
    }

    public Attribute(String key, String val, Attributes parent) {
        Validate.notNull(key);
        String key2 = key.trim();
        Validate.notEmpty(key2);
        this.key = key2;
        this.val = val;
        this.parent = parent;
    }

    @Override // java.util.Map.Entry
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        int i;
        Validate.notNull(key);
        String key2 = key.trim();
        Validate.notEmpty(key2);
        Attributes attributes = this.parent;
        if (!(attributes == null || (i = attributes.indexOfKey(this.key)) == -1)) {
            this.parent.keys[i] = key2;
        }
        this.key = key2;
    }

    @Override // java.util.Map.Entry
    public String getValue() {
        return Attributes.checkNotNull(this.val);
    }

    public boolean hasDeclaredValue() {
        return this.val != null;
    }

    public String setValue(String val) {
        String oldVal = this.val;
        Attributes attributes = this.parent;
        if (attributes != null) {
            oldVal = attributes.get(this.key);
            int i = this.parent.indexOfKey(this.key);
            if (i != -1) {
                this.parent.vals[i] = val;
            }
        }
        this.val = val;
        return Attributes.checkNotNull(oldVal);
    }

    public String html() {
        StringBuilder sb = StringUtil.borrowBuilder();
        try {
            html(sb, new Document("").outputSettings());
            return StringUtil.releaseBuilder(sb);
        } catch (IOException exception) {
            throw new SerializationException(exception);
        }
    }

    protected static void html(String key, String val, Appendable accum, Document.OutputSettings out) throws IOException {
        accum.append(key);
        if (!shouldCollapseAttribute(key, val, out)) {
            accum.append("=\"");
            Entities.escape(accum, Attributes.checkNotNull(val), out, true, false, false);
            accum.append(Typography.quote);
        }
    }

    /* JADX INFO: Access modifiers changed from: protected */
    public void html(Appendable accum, Document.OutputSettings out) throws IOException {
        html(this.key, this.val, accum, out);
    }

    public String toString() {
        return html();
    }

    public static Attribute createFromEncoded(String unencodedKey, String encodedValue) {
        String value = Entities.unescape(encodedValue, true);
        return new Attribute(unencodedKey, value, null);
    }

    /* JADX INFO: Access modifiers changed from: protected */
    public boolean isDataAttribute() {
        return isDataAttribute(this.key);
    }

    protected static boolean isDataAttribute(String key) {
        return key.startsWith("data-") && key.length() > "data-".length();
    }

    protected final boolean shouldCollapseAttribute(Document.OutputSettings out) {
        return shouldCollapseAttribute(this.key, this.val, out);
    }

    /* JADX INFO: Access modifiers changed from: protected */
    public static boolean shouldCollapseAttribute(String key, String val, Document.OutputSettings out) {
        return out.syntax() == Document.OutputSettings.Syntax.html && (val == null || (("".equals(val) || val.equalsIgnoreCase(key)) && isBooleanAttribute(key)));
    }

    protected static boolean isBooleanAttribute(String key) {
        return Arrays.binarySearch(booleanAttributes, key) >= 0;
    }

    @Override // java.util.Map.Entry
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Attribute attribute = (Attribute) o;
        String str = this.key;
        if (str == null ? attribute.key != null : !str.equals(attribute.key)) {
            return false;
        }
        String str2 = this.val;
        return str2 != null ? str2.equals(attribute.val) : attribute.val == null;
    }

    @Override // java.util.Map.Entry
    public int hashCode() {
        String str = this.key;
        int i = 0;
        int result = str != null ? str.hashCode() : 0;
        int i2 = result * 31;
        String str2 = this.val;
        if (str2 != null) {
            i = str2.hashCode();
        }
        int result2 = i2 + i;
        return result2;
    }

    public Attribute clone() {
        try {
            return (Attribute) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
