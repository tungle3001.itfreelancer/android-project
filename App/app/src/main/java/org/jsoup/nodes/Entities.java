package org.jsoup.nodes;

import java.io.IOException;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;
import java.util.HashMap;
import kotlin.text.Typography;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.parser.CharacterReader;
import org.jsoup.parser.Parser;

/* loaded from: classes2.dex */
public class Entities {
    static final int codepointRadix = 36;
    private static final int empty = -1;
    private static final String emptyName = "";
    private static final char[] codeDelims = {',', ';'};
    private static final HashMap<String, String> multipoints = new HashMap<>();
    private static final Document.OutputSettings DefaultOutput = new Document.OutputSettings();

    /* loaded from: classes2.dex */
    public enum EscapeMode {
        xhtml(EntitiesData.xmlPoints, 4),
        base(EntitiesData.basePoints, 106),
        extended(EntitiesData.fullPoints, 2125);
        
        private int[] codeKeys;
        private int[] codeVals;
        private String[] nameKeys;
        private String[] nameVals;

        EscapeMode(String file, int size) {
            Entities.load(this, file, size);
        }

        int codepointForName(String name) {
            int index = Arrays.binarySearch(this.nameKeys, name);
            if (index >= 0) {
                return this.codeVals[index];
            }
            return -1;
        }

        String nameForCodepoint(int codepoint) {
            int index = Arrays.binarySearch(this.codeKeys, codepoint);
            if (index < 0) {
                return "";
            }
            String[] strArr = this.nameVals;
            return (index >= strArr.length + (-1) || this.codeKeys[index + 1] != codepoint) ? this.nameVals[index] : strArr[index + 1];
        }

        private int size() {
            return this.nameKeys.length;
        }
    }

    private Entities() {
    }

    public static boolean isNamedEntity(String name) {
        return EscapeMode.extended.codepointForName(name) != -1;
    }

    public static boolean isBaseNamedEntity(String name) {
        return EscapeMode.base.codepointForName(name) != -1;
    }

    public static String getByName(String name) {
        String val = multipoints.get(name);
        if (val != null) {
            return val;
        }
        int codepoint = EscapeMode.extended.codepointForName(name);
        if (codepoint != -1) {
            return new String(new int[]{codepoint}, 0, 1);
        }
        return "";
    }

    public static int codepointsForName(String name, int[] codepoints) {
        String val = multipoints.get(name);
        if (val != null) {
            codepoints[0] = val.codePointAt(0);
            codepoints[1] = val.codePointAt(1);
            return 2;
        }
        int codepoint = EscapeMode.extended.codepointForName(name);
        if (codepoint == -1) {
            return 0;
        }
        codepoints[0] = codepoint;
        return 1;
    }

    public static String escape(String string, Document.OutputSettings out) {
        if (string == null) {
            return "";
        }
        StringBuilder accum = StringUtil.borrowBuilder();
        try {
            escape(accum, string, out, false, false, false);
            return StringUtil.releaseBuilder(accum);
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    public static String escape(String string) {
        return escape(string, DefaultOutput);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static void escape(Appendable accum, String string, Document.OutputSettings out, boolean inAttribute, boolean normaliseWhite, boolean stripLeadingWhite) throws IOException {
        boolean lastWasWhite = false;
        boolean reachedNonWhite = false;
        EscapeMode escapeMode = out.escapeMode();
        CharsetEncoder encoder = out.encoder();
        CoreCharset coreCharset = out.coreCharset;
        int length = string.length();
        int offset = 0;
        while (offset < length) {
            int codePoint = string.codePointAt(offset);
            if (normaliseWhite) {
                if (StringUtil.isWhitespace(codePoint)) {
                    if ((!stripLeadingWhite || reachedNonWhite) && !lastWasWhite) {
                        accum.append(' ');
                        lastWasWhite = true;
                    }
                    offset += Character.charCount(codePoint);
                } else {
                    lastWasWhite = false;
                    reachedNonWhite = true;
                }
            }
            if (codePoint < 65536) {
                char c = (char) codePoint;
                if (c != '\"') {
                    if (c == '&') {
                        accum.append("&amp;");
                    } else if (c != '<') {
                        if (c != '>') {
                            if (c != 160) {
                                if (canEncode(coreCharset, c, encoder)) {
                                    accum.append(c);
                                } else {
                                    appendEncoded(accum, escapeMode, codePoint);
                                }
                            } else if (escapeMode != EscapeMode.xhtml) {
                                accum.append("&nbsp;");
                            } else {
                                accum.append("&#xa0;");
                            }
                        } else if (!inAttribute) {
                            accum.append("&gt;");
                        } else {
                            accum.append(c);
                        }
                    } else if (!inAttribute || escapeMode == EscapeMode.xhtml) {
                        accum.append("&lt;");
                    } else {
                        accum.append(c);
                    }
                } else if (inAttribute) {
                    accum.append("&quot;");
                } else {
                    accum.append(c);
                }
            } else {
                String c2 = new String(Character.toChars(codePoint));
                if (encoder.canEncode(c2)) {
                    accum.append(c2);
                } else {
                    appendEncoded(accum, escapeMode, codePoint);
                }
            }
            offset += Character.charCount(codePoint);
        }
    }

    private static void appendEncoded(Appendable accum, EscapeMode escapeMode, int codePoint) throws IOException {
        String name = escapeMode.nameForCodepoint(codePoint);
        if (!"".equals(name)) {
            accum.append(Typography.amp).append(name).append(';');
        } else {
            accum.append("&#x").append(Integer.toHexString(codePoint)).append(';');
        }
    }

    public static String unescape(String string) {
        return unescape(string, false);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static String unescape(String string, boolean strict) {
        return Parser.unescapeEntities(string, strict);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* renamed from: org.jsoup.nodes.Entities$1  reason: invalid class name */
    /* loaded from: classes2.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$jsoup$nodes$Entities$CoreCharset;

        static {
            int[] iArr = new int[CoreCharset.values().length];
            $SwitchMap$org$jsoup$nodes$Entities$CoreCharset = iArr;
            try {
                iArr[CoreCharset.ascii.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jsoup$nodes$Entities$CoreCharset[CoreCharset.utf.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    private static boolean canEncode(CoreCharset charset, char c, CharsetEncoder fallback) {
        int i = AnonymousClass1.$SwitchMap$org$jsoup$nodes$Entities$CoreCharset[charset.ordinal()];
        if (i == 1) {
            return c < 128;
        }
        if (i != 2) {
            return fallback.canEncode(c);
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public enum CoreCharset {
        ascii,
        utf,
        fallback;

        /* JADX INFO: Access modifiers changed from: package-private */
        public static CoreCharset byName(String name) {
            if (name.equals("US-ASCII")) {
                return ascii;
            }
            if (name.startsWith("UTF-")) {
                return utf;
            }
            return fallback;
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void load(EscapeMode e, String pointsData, int size) {
        boolean z;
        int cp2;
        e.nameKeys = new String[size];
        e.codeVals = new int[size];
        e.codeKeys = new int[size];
        e.nameVals = new String[size];
        int i = 0;
        CharacterReader reader = new CharacterReader(pointsData);
        while (true) {
            z = true;
            if (reader.isEmpty()) {
                break;
            }
            String name = reader.consumeTo('=');
            reader.advance();
            int cp1 = Integer.parseInt(reader.consumeToAny(codeDelims), 36);
            char codeDelim = reader.current();
            reader.advance();
            if (codeDelim == ',') {
                cp2 = Integer.parseInt(reader.consumeTo(';'), 36);
                reader.advance();
            } else {
                cp2 = -1;
            }
            String indexS = reader.consumeTo(Typography.amp);
            int index = Integer.parseInt(indexS, 36);
            reader.advance();
            e.nameKeys[i] = name;
            e.codeVals[i] = cp1;
            e.codeKeys[index] = cp1;
            e.nameVals[index] = name;
            if (cp2 != -1) {
                multipoints.put(name, new String(new int[]{cp1, cp2}, 0, 2));
            }
            i++;
        }
        if (i != size) {
            z = false;
        }
        Validate.isTrue(z, "Unexpected count of entities loaded");
    }
}
