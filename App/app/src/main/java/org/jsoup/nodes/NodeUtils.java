package org.jsoup.nodes;

import org.jsoup.nodes.Document;
import org.jsoup.parser.HtmlTreeBuilder;
import org.jsoup.parser.Parser;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class NodeUtils {
    NodeUtils() {
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Document.OutputSettings outputSettings(Node node) {
        Document owner = node.ownerDocument();
        return owner != null ? owner.outputSettings() : new Document("").outputSettings();
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static Parser parser(Node node) {
        Document doc = node.ownerDocument();
        return (doc == null || doc.parser() == null) ? new Parser(new HtmlTreeBuilder()) : doc.parser();
    }
}
