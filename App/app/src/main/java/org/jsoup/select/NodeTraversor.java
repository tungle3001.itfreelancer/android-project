package org.jsoup.select;

import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.NodeFilter;

/* loaded from: classes2.dex */
public class NodeTraversor {
    public static void traverse(NodeVisitor visitor, Node root) {
        Node node = root;
        int depth = 0;
        while (node != null) {
            visitor.head(node, depth);
            if (node.childNodeSize() > 0) {
                node = node.childNode(0);
                depth++;
            } else {
                while (node.nextSibling() == null && depth > 0) {
                    visitor.tail(node, depth);
                    node = node.parentNode();
                    depth--;
                }
                visitor.tail(node, depth);
                if (node != root) {
                    node = node.nextSibling();
                } else {
                    return;
                }
            }
        }
    }

    public static void traverse(NodeVisitor visitor, Elements elements) {
        Validate.notNull(visitor);
        Validate.notNull(elements);
        Iterator<Element> it = elements.iterator();
        while (it.hasNext()) {
            Element el = it.next();
            traverse(visitor, el);
        }
    }

    public static NodeFilter.FilterResult filter(NodeFilter filter, Node root) {
        Node node = root;
        int depth = 0;
        while (node != null) {
            NodeFilter.FilterResult result = filter.head(node, depth);
            if (result == NodeFilter.FilterResult.STOP) {
                return result;
            }
            if (result != NodeFilter.FilterResult.CONTINUE || node.childNodeSize() <= 0) {
                while (node.nextSibling() == null && depth > 0) {
                    if ((result == NodeFilter.FilterResult.CONTINUE || result == NodeFilter.FilterResult.SKIP_CHILDREN) && (result = filter.tail(node, depth)) == NodeFilter.FilterResult.STOP) {
                        return result;
                    }
                    node = node.parentNode();
                    depth--;
                    if (result == NodeFilter.FilterResult.REMOVE) {
                        node.remove();
                    }
                    result = NodeFilter.FilterResult.CONTINUE;
                }
                if ((result == NodeFilter.FilterResult.CONTINUE || result == NodeFilter.FilterResult.SKIP_CHILDREN) && (result = filter.tail(node, depth)) == NodeFilter.FilterResult.STOP) {
                    return result;
                }
                if (node == root) {
                    return result;
                }
                node = node.nextSibling();
                if (result == NodeFilter.FilterResult.REMOVE) {
                    node.remove();
                }
            } else {
                node = node.childNode(0);
                depth++;
            }
        }
        return NodeFilter.FilterResult.CONTINUE;
    }

    public static void filter(NodeFilter filter, Elements elements) {
        Validate.notNull(filter);
        Validate.notNull(elements);
        Iterator<Element> it = elements.iterator();
        while (it.hasNext()) {
            Element el = it.next();
            if (filter(filter, el) == NodeFilter.FilterResult.STOP) {
                return;
            }
        }
    }
}
