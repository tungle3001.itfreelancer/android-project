package org.jsoup.parser;

import org.jsoup.helper.Validate;
import org.jsoup.internal.StringUtil;

/* loaded from: classes2.dex */
public class TokenQueue {
    private static final char ESC = '\\';
    private int pos = 0;
    private String queue;

    public TokenQueue(String data) {
        Validate.notNull(data);
        this.queue = data;
    }

    public boolean isEmpty() {
        return remainingLength() == 0;
    }

    private int remainingLength() {
        return this.queue.length() - this.pos;
    }

    public char peek() {
        if (isEmpty()) {
            return (char) 0;
        }
        return this.queue.charAt(this.pos);
    }

    public void addFirst(Character c) {
        addFirst(c.toString());
    }

    public void addFirst(String seq) {
        this.queue = seq + this.queue.substring(this.pos);
        this.pos = 0;
    }

    public boolean matches(String seq) {
        return this.queue.regionMatches(true, this.pos, seq, 0, seq.length());
    }

    public boolean matchesCS(String seq) {
        return this.queue.startsWith(seq, this.pos);
    }

    public boolean matchesAny(String... seq) {
        for (String s : seq) {
            if (matches(s)) {
                return true;
            }
        }
        return false;
    }

    public boolean matchesAny(char... seq) {
        if (isEmpty()) {
            return false;
        }
        for (char c : seq) {
            if (this.queue.charAt(this.pos) == c) {
                return true;
            }
        }
        return false;
    }

    public boolean matchesStartTag() {
        return remainingLength() >= 2 && this.queue.charAt(this.pos) == '<' && Character.isLetter(this.queue.charAt(this.pos + 1));
    }

    public boolean matchChomp(String seq) {
        if (!matches(seq)) {
            return false;
        }
        this.pos += seq.length();
        return true;
    }

    public boolean matchesWhitespace() {
        return !isEmpty() && StringUtil.isWhitespace(this.queue.charAt(this.pos));
    }

    public boolean matchesWord() {
        return !isEmpty() && Character.isLetterOrDigit(this.queue.charAt(this.pos));
    }

    public void advance() {
        if (!isEmpty()) {
            this.pos++;
        }
    }

    public char consume() {
        String str = this.queue;
        int i = this.pos;
        this.pos = i + 1;
        return str.charAt(i);
    }

    public void consume(String seq) {
        if (matches(seq)) {
            int len = seq.length();
            if (len <= remainingLength()) {
                this.pos += len;
                return;
            }
            throw new IllegalStateException("Queue not long enough to consume sequence");
        }
        throw new IllegalStateException("Queue did not match expected sequence");
    }

    public String consumeTo(String seq) {
        int offset = this.queue.indexOf(seq, this.pos);
        if (offset == -1) {
            return remainder();
        }
        String consumed = this.queue.substring(this.pos, offset);
        this.pos += consumed.length();
        return consumed;
    }

    public String consumeToIgnoreCase(String seq) {
        int start = this.pos;
        String first = seq.substring(0, 1);
        boolean canScan = first.toLowerCase().equals(first.toUpperCase());
        while (!isEmpty() && !matches(seq)) {
            if (canScan) {
                int indexOf = this.queue.indexOf(first, this.pos);
                int i = this.pos;
                int skip = indexOf - i;
                if (skip == 0) {
                    this.pos = i + 1;
                } else if (skip < 0) {
                    this.pos = this.queue.length();
                } else {
                    this.pos = i + skip;
                }
            } else {
                this.pos++;
            }
        }
        return this.queue.substring(start, this.pos);
    }

    public String consumeToAny(String... seq) {
        int start = this.pos;
        while (!isEmpty() && !matchesAny(seq)) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String chompTo(String seq) {
        String data = consumeTo(seq);
        matchChomp(seq);
        return data;
    }

    public String chompToIgnoreCase(String seq) {
        String data = consumeToIgnoreCase(seq);
        matchChomp(seq);
        return data;
    }

    /* JADX WARN: Removed duplicated region for block: B:43:0x004e A[EDGE_INSN: B:43:0x004e->B:36:0x004e ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public String chompBalanced(char r11, char r12) {
        /*
            r10 = this;
            r0 = -1
            r1 = -1
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
        L6:
            boolean r6 = r10.isEmpty()
            if (r6 == 0) goto Ld
            goto L4e
        Ld:
            char r6 = r10.consume()
            if (r3 == 0) goto L17
            r7 = 92
            if (r3 == r7) goto L45
        L17:
            r7 = 39
            r8 = 0
            r9 = 1
            if (r6 != r7) goto L26
            if (r6 == r11) goto L26
            if (r5 != 0) goto L26
            if (r4 != 0) goto L24
            r8 = 1
        L24:
            r4 = r8
            goto L32
        L26:
            r7 = 34
            if (r6 != r7) goto L32
            if (r6 == r11) goto L32
            if (r4 != 0) goto L32
            if (r5 != 0) goto L31
            r8 = 1
        L31:
            r5 = r8
        L32:
            if (r4 != 0) goto L4c
            if (r5 == 0) goto L37
            goto L4c
        L37:
            if (r6 != r11) goto L41
            int r2 = r2 + 1
            r7 = -1
            if (r0 != r7) goto L45
            int r0 = r10.pos
            goto L45
        L41:
            if (r6 != r12) goto L45
            int r2 = r2 + (-1)
        L45:
            if (r2 <= 0) goto L4b
            if (r3 == 0) goto L4b
            int r1 = r10.pos
        L4b:
            r3 = r6
        L4c:
            if (r2 > 0) goto L6
        L4e:
            if (r1 < 0) goto L57
            java.lang.String r6 = r10.queue
            java.lang.String r6 = r6.substring(r0, r1)
            goto L59
        L57:
            java.lang.String r6 = ""
        L59:
            if (r2 <= 0) goto L74
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Did not find balanced marker at '"
            r7.append(r8)
            r7.append(r6)
            java.lang.String r8 = "'"
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            org.jsoup.helper.Validate.fail(r7)
        L74:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TokenQueue.chompBalanced(char, char):java.lang.String");
    }

    public static String unescape(String in) {
        char[] charArray;
        StringBuilder out = StringUtil.borrowBuilder();
        char last = 0;
        for (char c : in.toCharArray()) {
            if (c != '\\') {
                out.append(c);
            } else if (last != 0 && last == '\\') {
                out.append(c);
            }
            last = c;
        }
        return StringUtil.releaseBuilder(out);
    }

    public boolean consumeWhitespace() {
        boolean seen = false;
        while (matchesWhitespace()) {
            this.pos++;
            seen = true;
        }
        return seen;
    }

    public String consumeWord() {
        int start = this.pos;
        while (matchesWord()) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String consumeTagName() {
        int start = this.pos;
        while (!isEmpty() && (matchesWord() || matchesAny(':', '_', '-'))) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String consumeElementSelector() {
        int start = this.pos;
        while (!isEmpty() && (matchesWord() || matchesAny("*|", "|", "_", "-"))) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String consumeCssIdentifier() {
        int start = this.pos;
        while (!isEmpty() && (matchesWord() || matchesAny('-', '_'))) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String consumeAttributeKey() {
        int start = this.pos;
        while (!isEmpty() && (matchesWord() || matchesAny('-', '_', ':'))) {
            this.pos++;
        }
        return this.queue.substring(start, this.pos);
    }

    public String remainder() {
        String str = this.queue;
        String remainder = str.substring(this.pos, str.length());
        this.pos = this.queue.length();
        return remainder;
    }

    public String toString() {
        return this.queue.substring(this.pos);
    }
}
