package org.jsoup.parser;

import android.support.v4.app.FrameMetricsAggregator;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Locale;
import org.jsoup.UncheckedIOException;
import org.jsoup.helper.Validate;

/* loaded from: classes2.dex */
public final class CharacterReader {
    static final char EOF = 65535;
    static final int maxBufferLen = 32768;
    private static final int maxStringCacheLen = 12;
    private static final int minReadAheadLen = 1024;
    static final int readAheadLimit = 24576;
    private static final int stringCacheSize = 512;
    private int bufLength;
    private int bufMark;
    private int bufPos;
    private int bufSplitPoint;
    private char[] charBuf;
    private boolean readFully;
    private Reader reader;
    private int readerPos;
    private String[] stringCache;

    public CharacterReader(Reader input, int sz) {
        this.bufMark = -1;
        this.stringCache = new String[512];
        Validate.notNull(input);
        Validate.isTrue(input.markSupported());
        this.reader = input;
        this.charBuf = new char[sz <= 32768 ? sz : 32768];
        bufferUp();
    }

    public CharacterReader(Reader input) {
        this(input, 32768);
    }

    public CharacterReader(String input) {
        this(new StringReader(input), input.length());
    }

    public void close() {
        Reader reader = this.reader;
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
            } catch (Throwable th) {
                this.reader = null;
                this.charBuf = null;
                this.stringCache = null;
                throw th;
            }
            this.reader = null;
            this.charBuf = null;
            this.stringCache = null;
        }
    }

    private void bufferUp() {
        int i;
        int pos;
        int offset;
        boolean z;
        if (!this.readFully && (i = this.bufPos) >= this.bufSplitPoint) {
            int i2 = this.bufMark;
            if (i2 != -1) {
                pos = this.bufMark;
                offset = i - i2;
            } else {
                pos = this.bufPos;
                offset = 0;
            }
            try {
                long skipped = this.reader.skip(pos);
                this.reader.mark(32768);
                int read = 0;
                while (true) {
                    z = true;
                    if (read > 1024) {
                        break;
                    }
                    int thisRead = this.reader.read(this.charBuf, read, this.charBuf.length - read);
                    if (thisRead == -1) {
                        this.readFully = true;
                    }
                    if (thisRead <= 0) {
                        break;
                    }
                    read += thisRead;
                }
                this.reader.reset();
                if (read > 0) {
                    if (skipped != pos) {
                        z = false;
                    }
                    Validate.isTrue(z);
                    this.bufLength = read;
                    this.readerPos += pos;
                    this.bufPos = offset;
                    if (this.bufMark != -1) {
                        this.bufMark = 0;
                    }
                    int i3 = this.bufLength;
                    int i4 = readAheadLimit;
                    if (i3 <= readAheadLimit) {
                        i4 = this.bufLength;
                    }
                    this.bufSplitPoint = i4;
                }
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }

    public int pos() {
        return this.readerPos + this.bufPos;
    }

    public boolean isEmpty() {
        bufferUp();
        return this.bufPos >= this.bufLength;
    }

    private boolean isEmptyNoBufferUp() {
        return this.bufPos >= this.bufLength;
    }

    public char current() {
        bufferUp();
        if (isEmptyNoBufferUp()) {
            return (char) 65535;
        }
        return this.charBuf[this.bufPos];
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public char consume() {
        bufferUp();
        char val = isEmptyNoBufferUp() ? (char) 65535 : this.charBuf[this.bufPos];
        this.bufPos++;
        return val;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public void unconsume() {
        int i = this.bufPos;
        if (i >= 1) {
            this.bufPos = i - 1;
            return;
        }
        throw new UncheckedIOException(new IOException("No buffer left to unconsume"));
    }

    public void advance() {
        this.bufPos++;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public void mark() {
        if (this.bufLength - this.bufPos < 1024) {
            this.bufSplitPoint = 0;
        }
        bufferUp();
        this.bufMark = this.bufPos;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public void unmark() {
        this.bufMark = -1;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public void rewindToMark() {
        int i = this.bufMark;
        if (i != -1) {
            this.bufPos = i;
            unmark();
            return;
        }
        throw new UncheckedIOException(new IOException("Mark invalid"));
    }

    int nextIndexOf(char c) {
        bufferUp();
        for (int i = this.bufPos; i < this.bufLength; i++) {
            if (c == this.charBuf[i]) {
                return i - this.bufPos;
            }
        }
        return -1;
    }

    int nextIndexOf(CharSequence seq) {
        bufferUp();
        char startChar = seq.charAt(0);
        int offset = this.bufPos;
        while (offset < this.bufLength) {
            if (startChar != this.charBuf[offset]) {
                do {
                    offset++;
                    if (offset >= this.bufLength) {
                        break;
                    }
                } while (startChar != this.charBuf[offset]);
            }
            int i = offset + 1;
            int last = (seq.length() + i) - 1;
            int i2 = this.bufLength;
            if (offset < i2 && last <= i2) {
                for (int j = 1; i < last && seq.charAt(j) == this.charBuf[i]; j++) {
                    i++;
                }
                if (i == last) {
                    return offset - this.bufPos;
                }
            }
            offset++;
        }
        return -1;
    }

    public String consumeTo(char c) {
        int offset = nextIndexOf(c);
        if (offset != -1) {
            String consumed = cacheString(this.charBuf, this.stringCache, this.bufPos, offset);
            this.bufPos += offset;
            return consumed;
        }
        String consumed2 = consumeToEnd();
        return consumed2;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeTo(String seq) {
        int offset = nextIndexOf(seq);
        if (offset != -1) {
            String consumed = cacheString(this.charBuf, this.stringCache, this.bufPos, offset);
            this.bufPos += offset;
            return consumed;
        } else if (this.bufLength - this.bufPos < seq.length()) {
            return consumeToEnd();
        } else {
            int endPos = (this.bufLength - seq.length()) + 1;
            char[] cArr = this.charBuf;
            String[] strArr = this.stringCache;
            int i = this.bufPos;
            String consumed2 = cacheString(cArr, strArr, i, endPos - i);
            this.bufPos = endPos;
            return consumed2;
        }
    }

    public String consumeToAny(char... chars) {
        bufferUp();
        int pos = this.bufPos;
        int remaining = this.bufLength;
        char[] val = this.charBuf;
        loop0: while (pos < remaining) {
            for (char c : chars) {
                if (val[pos] == c) {
                    break loop0;
                }
            }
            pos++;
        }
        this.bufPos = pos;
        return pos > pos ? cacheString(this.charBuf, this.stringCache, pos, pos - pos) : "";
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeToAnySorted(char... chars) {
        bufferUp();
        int pos = this.bufPos;
        int remaining = this.bufLength;
        char[] val = this.charBuf;
        while (pos < remaining && Arrays.binarySearch(chars, val[pos]) < 0) {
            pos++;
        }
        this.bufPos = pos;
        return pos > pos ? cacheString(this.charBuf, this.stringCache, pos, pos - pos) : "";
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeData() {
        int pos = this.bufPos;
        int remaining = this.bufLength;
        char[] val = this.charBuf;
        while (pos < remaining) {
            char c = val[pos];
            if (c == 0 || c == '&' || c == '<') {
                break;
            }
            pos++;
        }
        this.bufPos = pos;
        return pos > pos ? cacheString(this.charBuf, this.stringCache, pos, pos - pos) : "";
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeRawData() {
        int pos = this.bufPos;
        int remaining = this.bufLength;
        char[] val = this.charBuf;
        while (pos < remaining) {
            char c = val[pos];
            if (c == 0 || c == '<') {
                break;
            }
            pos++;
        }
        this.bufPos = pos;
        return pos > pos ? cacheString(this.charBuf, this.stringCache, pos, pos - pos) : "";
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeTagName() {
        bufferUp();
        int pos = this.bufPos;
        int remaining = this.bufLength;
        char[] val = this.charBuf;
        while (pos < remaining) {
            char c = val[pos];
            if (c == 0 || c == ' ' || c == '/' || c == '<' || c == '>' || c == '\t' || c == '\n' || c == '\f' || c == '\r') {
                break;
            }
            pos++;
        }
        this.bufPos = pos;
        return pos > pos ? cacheString(this.charBuf, this.stringCache, pos, pos - pos) : "";
    }

    String consumeToEnd() {
        bufferUp();
        char[] cArr = this.charBuf;
        String[] strArr = this.stringCache;
        int i = this.bufPos;
        String data = cacheString(cArr, strArr, i, this.bufLength - i);
        this.bufPos = this.bufLength;
        return data;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeLetterSequence() {
        char c;
        bufferUp();
        int start = this.bufPos;
        while (true) {
            int i = this.bufPos;
            if (i >= this.bufLength || (((c = this.charBuf[i]) < 'A' || c > 'Z') && ((c < 'a' || c > 'z') && !Character.isLetter(c)))) {
                break;
            }
            this.bufPos++;
        }
        return cacheString(this.charBuf, this.stringCache, start, this.bufPos - start);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeLetterThenDigitSequence() {
        char c;
        bufferUp();
        int start = this.bufPos;
        while (true) {
            int i = this.bufPos;
            if (i >= this.bufLength || (((c = this.charBuf[i]) < 'A' || c > 'Z') && ((c < 'a' || c > 'z') && !Character.isLetter(c)))) {
                break;
            }
            this.bufPos++;
        }
        while (!isEmptyNoBufferUp()) {
            char[] cArr = this.charBuf;
            int i2 = this.bufPos;
            char c2 = cArr[i2];
            if (c2 < '0' || c2 > '9') {
                break;
            }
            this.bufPos = i2 + 1;
        }
        return cacheString(this.charBuf, this.stringCache, start, this.bufPos - start);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeHexSequence() {
        char c;
        bufferUp();
        int start = this.bufPos;
        while (true) {
            int i = this.bufPos;
            if (i >= this.bufLength || (((c = this.charBuf[i]) < '0' || c > '9') && ((c < 'A' || c > 'F') && (c < 'a' || c > 'f')))) {
                break;
            }
            this.bufPos++;
        }
        return cacheString(this.charBuf, this.stringCache, start, this.bufPos - start);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public String consumeDigitSequence() {
        char c;
        bufferUp();
        int start = this.bufPos;
        while (true) {
            int i = this.bufPos;
            if (i >= this.bufLength || (c = this.charBuf[i]) < '0' || c > '9') {
                break;
            }
            this.bufPos = i + 1;
        }
        return cacheString(this.charBuf, this.stringCache, start, this.bufPos - start);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matches(char c) {
        return !isEmpty() && this.charBuf[this.bufPos] == c;
    }

    boolean matches(String seq) {
        bufferUp();
        int scanLength = seq.length();
        if (scanLength > this.bufLength - this.bufPos) {
            return false;
        }
        for (int offset = 0; offset < scanLength; offset++) {
            if (seq.charAt(offset) != this.charBuf[this.bufPos + offset]) {
                return false;
            }
        }
        return true;
    }

    boolean matchesIgnoreCase(String seq) {
        bufferUp();
        int scanLength = seq.length();
        if (scanLength > this.bufLength - this.bufPos) {
            return false;
        }
        for (int offset = 0; offset < scanLength; offset++) {
            char upScan = Character.toUpperCase(seq.charAt(offset));
            char upTarget = Character.toUpperCase(this.charBuf[this.bufPos + offset]);
            if (upScan != upTarget) {
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchesAny(char... seq) {
        if (isEmpty()) {
            return false;
        }
        bufferUp();
        char c = this.charBuf[this.bufPos];
        for (char seek : seq) {
            if (seek == c) {
                return true;
            }
        }
        return false;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchesAnySorted(char[] seq) {
        bufferUp();
        return !isEmpty() && Arrays.binarySearch(seq, this.charBuf[this.bufPos]) >= 0;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchesLetter() {
        if (isEmpty()) {
            return false;
        }
        char c = this.charBuf[this.bufPos];
        return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || Character.isLetter(c);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchesDigit() {
        char c;
        return !isEmpty() && (c = this.charBuf[this.bufPos]) >= '0' && c <= '9';
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchConsume(String seq) {
        bufferUp();
        if (!matches(seq)) {
            return false;
        }
        this.bufPos += seq.length();
        return true;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean matchConsumeIgnoreCase(String seq) {
        if (!matchesIgnoreCase(seq)) {
            return false;
        }
        this.bufPos += seq.length();
        return true;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public boolean containsIgnoreCase(String seq) {
        String loScan = seq.toLowerCase(Locale.ENGLISH);
        String hiScan = seq.toUpperCase(Locale.ENGLISH);
        return nextIndexOf(loScan) > -1 || nextIndexOf(hiScan) > -1;
    }

    public String toString() {
        if (this.bufLength - this.bufPos < 0) {
            return "";
        }
        char[] cArr = this.charBuf;
        int i = this.bufPos;
        return new String(cArr, i, this.bufLength - i);
    }

    private static String cacheString(char[] charBuf, String[] stringCache, int start, int count) {
        if (count > 12) {
            return new String(charBuf, start, count);
        }
        if (count < 1) {
            return "";
        }
        int hash = count * 31;
        int offset = start;
        int i = 0;
        while (i < count) {
            hash = (hash * 31) + charBuf[offset];
            i++;
            offset++;
        }
        int i2 = hash & FrameMetricsAggregator.EVERY_DURATION;
        String cached = stringCache[i2];
        if (cached == null) {
            String cached2 = new String(charBuf, start, count);
            stringCache[i2] = cached2;
            return cached2;
        } else if (rangeEquals(charBuf, start, count, cached)) {
            return cached;
        } else {
            String cached3 = new String(charBuf, start, count);
            stringCache[i2] = cached3;
            return cached3;
        }
    }

    static boolean rangeEquals(char[] charBuf, int start, int i, String cached) {
        if (i != cached.length()) {
            return false;
        }
        int i2 = start;
        int j = 0;
        while (true) {
            int count = i - 1;
            if (i == 0) {
                return true;
            }
            int i3 = i2 + 1;
            int j2 = j + 1;
            if (charBuf[i2] != cached.charAt(j)) {
                return false;
            }
            i2 = i3;
            i = count;
            j = j2;
        }
    }

    boolean rangeEquals(int start, int count, String cached) {
        return rangeEquals(this.charBuf, start, count, cached);
    }
}
