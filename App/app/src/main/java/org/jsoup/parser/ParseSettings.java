package org.jsoup.parser;

import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Attributes;

/* loaded from: classes2.dex */
public class ParseSettings {
    public static final ParseSettings htmlDefault = new ParseSettings(false, false);
    public static final ParseSettings preserveCase = new ParseSettings(true, true);
    private final boolean preserveAttributeCase;
    private final boolean preserveTagCase;

    public boolean preserveTagCase() {
        return this.preserveTagCase;
    }

    public boolean preserveAttributeCase() {
        return this.preserveAttributeCase;
    }

    public ParseSettings(boolean tag, boolean attribute) {
        this.preserveTagCase = tag;
        this.preserveAttributeCase = attribute;
    }

    public String normalizeTag(String name) {
        String name2 = name.trim();
        if (!this.preserveTagCase) {
            return Normalizer.lowerCase(name2);
        }
        return name2;
    }

    public String normalizeAttribute(String name) {
        String name2 = name.trim();
        if (!this.preserveAttributeCase) {
            return Normalizer.lowerCase(name2);
        }
        return name2;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public Attributes normalizeAttributes(Attributes attributes) {
        if (attributes != null && !this.preserveAttributeCase) {
            attributes.normalize();
        }
        return attributes;
    }
}
