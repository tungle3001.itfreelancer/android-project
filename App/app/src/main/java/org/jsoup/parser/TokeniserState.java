package org.jsoup.parser;

import kotlin.text.Typography;
import org.jsoup.nodes.DocumentType;
import org.jsoup.parser.Token;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public enum TokeniserState {
    Data { // from class: org.jsoup.parser.TokeniserState.1
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char current = r.current();
            if (current == 0) {
                t.error(this);
                t.emit(r.consume());
            } else if (current == '&') {
                t.advanceTransition(CharacterReferenceInData);
            } else if (current == '<') {
                t.advanceTransition(TagOpen);
            } else if (current != 65535) {
                String data = r.consumeData();
                t.emit(data);
            } else {
                t.emit(new Token.EOF());
            }
        }
    },
    CharacterReferenceInData { // from class: org.jsoup.parser.TokeniserState.2
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readCharRef(t, Data);
        }
    },
    Rcdata { // from class: org.jsoup.parser.TokeniserState.3
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char current = r.current();
            if (current == 0) {
                t.error(this);
                r.advance();
                t.emit((char) 65533);
            } else if (current == '&') {
                t.advanceTransition(CharacterReferenceInRcdata);
            } else if (current == '<') {
                t.advanceTransition(RcdataLessthanSign);
            } else if (current != 65535) {
                String data = r.consumeData();
                t.emit(data);
            } else {
                t.emit(new Token.EOF());
            }
        }
    },
    CharacterReferenceInRcdata { // from class: org.jsoup.parser.TokeniserState.4
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readCharRef(t, Rcdata);
        }
    },
    Rawtext { // from class: org.jsoup.parser.TokeniserState.5
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readRawData(t, r, this, RawtextLessthanSign);
        }
    },
    ScriptData { // from class: org.jsoup.parser.TokeniserState.6
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readRawData(t, r, this, ScriptDataLessthanSign);
        }
    },
    PLAINTEXT { // from class: org.jsoup.parser.TokeniserState.7
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char current = r.current();
            if (current == 0) {
                t.error(this);
                r.advance();
                t.emit((char) 65533);
            } else if (current != 65535) {
                String data = r.consumeTo((char) 0);
                t.emit(data);
            } else {
                t.emit(new Token.EOF());
            }
        }
    },
    TagOpen { // from class: org.jsoup.parser.TokeniserState.8
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char current = r.current();
            if (current == '!') {
                t.advanceTransition(MarkupDeclarationOpen);
            } else if (current == '/') {
                t.advanceTransition(EndTagOpen);
            } else if (current == '?') {
                t.createBogusCommentPending();
                t.advanceTransition(BogusComment);
            } else if (r.matchesLetter()) {
                t.createTagPending(true);
                t.transition(TagName);
            } else {
                t.error(this);
                t.emit(Typography.less);
                t.transition(Data);
            }
        }
    },
    EndTagOpen { // from class: org.jsoup.parser.TokeniserState.9
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.isEmpty()) {
                t.eofError(this);
                t.emit("</");
                t.transition(Data);
            } else if (r.matchesLetter()) {
                t.createTagPending(false);
                t.transition(TagName);
            } else if (r.matches(Typography.greater)) {
                t.error(this);
                t.advanceTransition(Data);
            } else {
                t.error(this);
                t.createBogusCommentPending();
                t.advanceTransition(BogusComment);
            }
        }
    },
    TagName { // from class: org.jsoup.parser.TokeniserState.10
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String tagName = r.consumeTagName();
            t.tagPending.appendTagName(tagName);
            char c = r.consume();
            if (c != 0) {
                if (c != ' ') {
                    if (c != '/') {
                        if (c == '<') {
                            r.unconsume();
                            t.error(this);
                        } else if (c != '>') {
                            if (c == 65535) {
                                t.eofError(this);
                                t.transition(Data);
                                return;
                            } else if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r')) {
                                t.tagPending.appendTagName(c);
                                return;
                            }
                        }
                        t.emitTagPending();
                        t.transition(Data);
                        return;
                    }
                    t.transition(SelfClosingStartTag);
                    return;
                }
                t.transition(BeforeAttributeName);
                return;
            }
            t.tagPending.appendTagName(TokeniserState.replacementStr);
        }
    },
    RcdataLessthanSign { // from class: org.jsoup.parser.TokeniserState.11
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matches('/')) {
                t.createTempBuffer();
                t.advanceTransition(RCDATAEndTagOpen);
                return;
            }
            if (r.matchesLetter() && t.appropriateEndTagName() != null) {
                if (!r.containsIgnoreCase("</" + t.appropriateEndTagName())) {
                    t.tagPending = t.createTagPending(false).name(t.appropriateEndTagName());
                    t.emitTagPending();
                    r.unconsume();
                    t.transition(Data);
                    return;
                }
            }
            t.emit("<");
            t.transition(Rcdata);
        }
    },
    RCDATAEndTagOpen { // from class: org.jsoup.parser.TokeniserState.12
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                t.createTagPending(false);
                t.tagPending.appendTagName(r.current());
                t.dataBuffer.append(r.current());
                t.advanceTransition(RCDATAEndTagName);
                return;
            }
            t.emit("</");
            t.transition(Rcdata);
        }
    },
    RCDATAEndTagName { // from class: org.jsoup.parser.TokeniserState.13
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                String name = r.consumeLetterSequence();
                t.tagPending.appendTagName(name);
                t.dataBuffer.append(name);
                return;
            }
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                if (t.isAppropriateEndTagToken()) {
                    t.transition(BeforeAttributeName);
                } else {
                    anythingElse(t, r);
                }
            } else if (c != '/') {
                if (c != '>') {
                    anythingElse(t, r);
                } else if (t.isAppropriateEndTagToken()) {
                    t.emitTagPending();
                    t.transition(Data);
                } else {
                    anythingElse(t, r);
                }
            } else if (t.isAppropriateEndTagToken()) {
                t.transition(SelfClosingStartTag);
            } else {
                anythingElse(t, r);
            }
        }

        private void anythingElse(Tokeniser t, CharacterReader r) {
            t.emit("</" + t.dataBuffer.toString());
            r.unconsume();
            t.transition(Rcdata);
        }
    },
    RawtextLessthanSign { // from class: org.jsoup.parser.TokeniserState.14
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matches('/')) {
                t.createTempBuffer();
                t.advanceTransition(RawtextEndTagOpen);
                return;
            }
            t.emit(Typography.less);
            t.transition(Rawtext);
        }
    },
    RawtextEndTagOpen { // from class: org.jsoup.parser.TokeniserState.15
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readEndTag(t, r, RawtextEndTagName, Rawtext);
        }
    },
    RawtextEndTagName { // from class: org.jsoup.parser.TokeniserState.16
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.handleDataEndTag(t, r, Rawtext);
        }
    },
    ScriptDataLessthanSign { // from class: org.jsoup.parser.TokeniserState.17
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char consume = r.consume();
            if (consume == '!') {
                t.emit("<!");
                t.transition(ScriptDataEscapeStart);
            } else if (consume == '/') {
                t.createTempBuffer();
                t.transition(ScriptDataEndTagOpen);
            } else if (consume != 65535) {
                t.emit("<");
                r.unconsume();
                t.transition(ScriptData);
            } else {
                t.emit("<");
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    ScriptDataEndTagOpen { // from class: org.jsoup.parser.TokeniserState.18
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.readEndTag(t, r, ScriptDataEndTagName, ScriptData);
        }
    },
    ScriptDataEndTagName { // from class: org.jsoup.parser.TokeniserState.19
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.handleDataEndTag(t, r, ScriptData);
        }
    },
    ScriptDataEscapeStart { // from class: org.jsoup.parser.TokeniserState.20
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matches('-')) {
                t.emit('-');
                t.advanceTransition(ScriptDataEscapeStartDash);
                return;
            }
            t.transition(ScriptData);
        }
    },
    ScriptDataEscapeStartDash { // from class: org.jsoup.parser.TokeniserState.21
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matches('-')) {
                t.emit('-');
                t.advanceTransition(ScriptDataEscapedDashDash);
                return;
            }
            t.transition(ScriptData);
        }
    },
    ScriptDataEscaped { // from class: org.jsoup.parser.TokeniserState.22
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.isEmpty()) {
                t.eofError(this);
                t.transition(Data);
                return;
            }
            char current = r.current();
            if (current == 0) {
                t.error(this);
                r.advance();
                t.emit((char) 65533);
            } else if (current == '-') {
                t.emit('-');
                t.advanceTransition(ScriptDataEscapedDash);
            } else if (current != '<') {
                String data = r.consumeToAny('-', Typography.less, 0);
                t.emit(data);
            } else {
                t.advanceTransition(ScriptDataEscapedLessthanSign);
            }
        }
    },
    ScriptDataEscapedDash { // from class: org.jsoup.parser.TokeniserState.23
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.isEmpty()) {
                t.eofError(this);
                t.transition(Data);
                return;
            }
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.emit((char) 65533);
                t.transition(ScriptDataEscaped);
            } else if (c == '-') {
                t.emit(c);
                t.transition(ScriptDataEscapedDashDash);
            } else if (c != '<') {
                t.emit(c);
                t.transition(ScriptDataEscaped);
            } else {
                t.transition(ScriptDataEscapedLessthanSign);
            }
        }
    },
    ScriptDataEscapedDashDash { // from class: org.jsoup.parser.TokeniserState.24
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.isEmpty()) {
                t.eofError(this);
                t.transition(Data);
                return;
            }
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.emit((char) 65533);
                t.transition(ScriptDataEscaped);
            } else if (c == '-') {
                t.emit(c);
            } else if (c == '<') {
                t.transition(ScriptDataEscapedLessthanSign);
            } else if (c != '>') {
                t.emit(c);
                t.transition(ScriptDataEscaped);
            } else {
                t.emit(c);
                t.transition(ScriptData);
            }
        }
    },
    ScriptDataEscapedLessthanSign { // from class: org.jsoup.parser.TokeniserState.25
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                t.createTempBuffer();
                t.dataBuffer.append(r.current());
                t.emit("<" + r.current());
                t.advanceTransition(ScriptDataDoubleEscapeStart);
            } else if (r.matches('/')) {
                t.createTempBuffer();
                t.advanceTransition(ScriptDataEscapedEndTagOpen);
            } else {
                t.emit(Typography.less);
                t.transition(ScriptDataEscaped);
            }
        }
    },
    ScriptDataEscapedEndTagOpen { // from class: org.jsoup.parser.TokeniserState.26
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                t.createTagPending(false);
                t.tagPending.appendTagName(r.current());
                t.dataBuffer.append(r.current());
                t.advanceTransition(ScriptDataEscapedEndTagName);
                return;
            }
            t.emit("</");
            t.transition(ScriptDataEscaped);
        }
    },
    ScriptDataEscapedEndTagName { // from class: org.jsoup.parser.TokeniserState.27
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.handleDataEndTag(t, r, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscapeStart { // from class: org.jsoup.parser.TokeniserState.28
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.handleDataDoubleEscapeTag(t, r, ScriptDataDoubleEscaped, ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscaped { // from class: org.jsoup.parser.TokeniserState.29
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.current();
            if (c == 0) {
                t.error(this);
                r.advance();
                t.emit((char) 65533);
            } else if (c == '-') {
                t.emit(c);
                t.advanceTransition(ScriptDataDoubleEscapedDash);
            } else if (c == '<') {
                t.emit(c);
                t.advanceTransition(ScriptDataDoubleEscapedLessthanSign);
            } else if (c != 65535) {
                String data = r.consumeToAny('-', Typography.less, 0);
                t.emit(data);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    ScriptDataDoubleEscapedDash { // from class: org.jsoup.parser.TokeniserState.30
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.emit((char) 65533);
                t.transition(ScriptDataDoubleEscaped);
            } else if (c == '-') {
                t.emit(c);
                t.transition(ScriptDataDoubleEscapedDashDash);
            } else if (c == '<') {
                t.emit(c);
                t.transition(ScriptDataDoubleEscapedLessthanSign);
            } else if (c != 65535) {
                t.emit(c);
                t.transition(ScriptDataDoubleEscaped);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    ScriptDataDoubleEscapedDashDash { // from class: org.jsoup.parser.TokeniserState.31
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.emit((char) 65533);
                t.transition(ScriptDataDoubleEscaped);
            } else if (c == '-') {
                t.emit(c);
            } else if (c == '<') {
                t.emit(c);
                t.transition(ScriptDataDoubleEscapedLessthanSign);
            } else if (c == '>') {
                t.emit(c);
                t.transition(ScriptData);
            } else if (c != 65535) {
                t.emit(c);
                t.transition(ScriptDataDoubleEscaped);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    ScriptDataDoubleEscapedLessthanSign { // from class: org.jsoup.parser.TokeniserState.32
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matches('/')) {
                t.emit('/');
                t.createTempBuffer();
                t.advanceTransition(ScriptDataDoubleEscapeEnd);
                return;
            }
            t.transition(ScriptDataDoubleEscaped);
        }
    },
    ScriptDataDoubleEscapeEnd { // from class: org.jsoup.parser.TokeniserState.33
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            TokeniserState.handleDataDoubleEscapeTag(t, r, ScriptDataEscaped, ScriptDataDoubleEscaped);
        }
    },
    BeforeAttributeName { // from class: org.jsoup.parser.TokeniserState.34
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                r.unconsume();
                t.error(this);
                t.tagPending.newAttribute();
                t.transition(AttributeName);
            } else if (c != ' ') {
                if (!(c == '\"' || c == '\'')) {
                    if (c == '/') {
                        t.transition(SelfClosingStartTag);
                        return;
                    } else if (c == 65535) {
                        t.eofError(this);
                        t.transition(Data);
                        return;
                    } else if (c != '\t' && c != '\n' && c != '\f' && c != '\r') {
                        switch (c) {
                            case '<':
                                r.unconsume();
                                t.error(this);
                                t.emitTagPending();
                                t.transition(Data);
                                return;
                            case '=':
                                break;
                            case '>':
                                t.emitTagPending();
                                t.transition(Data);
                                return;
                            default:
                                t.tagPending.newAttribute();
                                r.unconsume();
                                t.transition(AttributeName);
                                return;
                        }
                    } else {
                        return;
                    }
                }
                t.error(this);
                t.tagPending.newAttribute();
                t.tagPending.appendAttributeName(c);
                t.transition(AttributeName);
            }
        }
    },
    AttributeName { // from class: org.jsoup.parser.TokeniserState.35
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String name = r.consumeToAnySorted(attributeNameCharsSorted);
            t.tagPending.appendAttributeName(name);
            char c = r.consume();
            if (c != 0) {
                if (c != ' ') {
                    if (!(c == '\"' || c == '\'')) {
                        if (c == '/') {
                            t.transition(SelfClosingStartTag);
                            return;
                        } else if (c == 65535) {
                            t.eofError(this);
                            t.transition(Data);
                            return;
                        } else if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r')) {
                            switch (c) {
                                case '<':
                                    break;
                                case '=':
                                    t.transition(BeforeAttributeValue);
                                    return;
                                case '>':
                                    t.emitTagPending();
                                    t.transition(Data);
                                    return;
                                default:
                                    t.tagPending.appendAttributeName(c);
                                    return;
                            }
                        }
                    }
                    t.error(this);
                    t.tagPending.appendAttributeName(c);
                    return;
                }
                t.transition(AfterAttributeName);
                return;
            }
            t.error(this);
            t.tagPending.appendAttributeName((char) 65533);
        }
    },
    AfterAttributeName { // from class: org.jsoup.parser.TokeniserState.36
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.tagPending.appendAttributeName((char) 65533);
                t.transition(AttributeName);
            } else if (c != ' ') {
                if (!(c == '\"' || c == '\'')) {
                    if (c == '/') {
                        t.transition(SelfClosingStartTag);
                        return;
                    } else if (c == 65535) {
                        t.eofError(this);
                        t.transition(Data);
                        return;
                    } else if (c != '\t' && c != '\n' && c != '\f' && c != '\r') {
                        switch (c) {
                            case '<':
                                break;
                            case '=':
                                t.transition(BeforeAttributeValue);
                                return;
                            case '>':
                                t.emitTagPending();
                                t.transition(Data);
                                return;
                            default:
                                t.tagPending.newAttribute();
                                r.unconsume();
                                t.transition(AttributeName);
                                return;
                        }
                    } else {
                        return;
                    }
                }
                t.error(this);
                t.tagPending.newAttribute();
                t.tagPending.appendAttributeName(c);
                t.transition(AttributeName);
            }
        }
    },
    BeforeAttributeValue { // from class: org.jsoup.parser.TokeniserState.37
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.tagPending.appendAttributeValue((char) 65533);
                t.transition(AttributeValue_unquoted);
            } else if (c == ' ') {
            } else {
                if (c != '\"') {
                    if (c != '`') {
                        if (c == 65535) {
                            t.eofError(this);
                            t.emitTagPending();
                            t.transition(Data);
                            return;
                        } else if (c != '\t' && c != '\n' && c != '\f' && c != '\r') {
                            if (c == '&') {
                                r.unconsume();
                                t.transition(AttributeValue_unquoted);
                                return;
                            } else if (c != '\'') {
                                switch (c) {
                                    case '<':
                                    case '=':
                                        break;
                                    case '>':
                                        t.error(this);
                                        t.emitTagPending();
                                        t.transition(Data);
                                        return;
                                    default:
                                        r.unconsume();
                                        t.transition(AttributeValue_unquoted);
                                        return;
                                }
                            } else {
                                t.transition(AttributeValue_singleQuoted);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    t.error(this);
                    t.tagPending.appendAttributeValue(c);
                    t.transition(AttributeValue_unquoted);
                    return;
                }
                t.transition(AttributeValue_doubleQuoted);
            }
        }
    },
    AttributeValue_doubleQuoted { // from class: org.jsoup.parser.TokeniserState.38
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String value = r.consumeToAnySorted(attributeDoubleValueCharsSorted);
            if (value.length() > 0) {
                t.tagPending.appendAttributeValue(value);
            } else {
                t.tagPending.setEmptyAttributeValue();
            }
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.tagPending.appendAttributeValue((char) 65533);
            } else if (c == '\"') {
                t.transition(AfterAttributeValue_quoted);
            } else if (c == '&') {
                int[] ref = t.consumeCharacterReference(Character.valueOf(Typography.quote), true);
                if (ref != null) {
                    t.tagPending.appendAttributeValue(ref);
                } else {
                    t.tagPending.appendAttributeValue(Typography.amp);
                }
            } else if (c != 65535) {
                t.tagPending.appendAttributeValue(c);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    AttributeValue_singleQuoted { // from class: org.jsoup.parser.TokeniserState.39
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String value = r.consumeToAnySorted(attributeSingleValueCharsSorted);
            if (value.length() > 0) {
                t.tagPending.appendAttributeValue(value);
            } else {
                t.tagPending.setEmptyAttributeValue();
            }
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.tagPending.appendAttributeValue((char) 65533);
            } else if (c == 65535) {
                t.eofError(this);
                t.transition(Data);
            } else if (c == '&') {
                int[] ref = t.consumeCharacterReference('\'', true);
                if (ref != null) {
                    t.tagPending.appendAttributeValue(ref);
                } else {
                    t.tagPending.appendAttributeValue(Typography.amp);
                }
            } else if (c != '\'') {
                t.tagPending.appendAttributeValue(c);
            } else {
                t.transition(AfterAttributeValue_quoted);
            }
        }
    },
    AttributeValue_unquoted { // from class: org.jsoup.parser.TokeniserState.40
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String value = r.consumeToAnySorted(attributeValueUnquoted);
            if (value.length() > 0) {
                t.tagPending.appendAttributeValue(value);
            }
            char c = r.consume();
            if (c != 0) {
                if (c != ' ') {
                    if (!(c == '\"' || c == '`')) {
                        if (c == 65535) {
                            t.eofError(this);
                            t.transition(Data);
                            return;
                        } else if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r')) {
                            if (c == '&') {
                                int[] ref = t.consumeCharacterReference(Character.valueOf(Typography.greater), true);
                                if (ref != null) {
                                    t.tagPending.appendAttributeValue(ref);
                                    return;
                                } else {
                                    t.tagPending.appendAttributeValue(Typography.amp);
                                    return;
                                }
                            } else if (c != '\'') {
                                switch (c) {
                                    case '<':
                                    case '=':
                                        break;
                                    case '>':
                                        t.emitTagPending();
                                        t.transition(Data);
                                        return;
                                    default:
                                        t.tagPending.appendAttributeValue(c);
                                        return;
                                }
                            }
                        }
                    }
                    t.error(this);
                    t.tagPending.appendAttributeValue(c);
                    return;
                }
                t.transition(BeforeAttributeName);
                return;
            }
            t.error(this);
            t.tagPending.appendAttributeValue((char) 65533);
        }
    },
    AfterAttributeValue_quoted { // from class: org.jsoup.parser.TokeniserState.41
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BeforeAttributeName);
            } else if (c == '/') {
                t.transition(SelfClosingStartTag);
            } else if (c == '>') {
                t.emitTagPending();
                t.transition(Data);
            } else if (c != 65535) {
                r.unconsume();
                t.error(this);
                t.transition(BeforeAttributeName);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    SelfClosingStartTag { // from class: org.jsoup.parser.TokeniserState.42
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '>') {
                t.tagPending.selfClosing = true;
                t.emitTagPending();
                t.transition(Data);
            } else if (c != 65535) {
                r.unconsume();
                t.error(this);
                t.transition(BeforeAttributeName);
            } else {
                t.eofError(this);
                t.transition(Data);
            }
        }
    },
    BogusComment { // from class: org.jsoup.parser.TokeniserState.43
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            r.unconsume();
            t.commentPending.append(r.consumeTo(Typography.greater));
            char next = r.consume();
            if (next == '>' || next == 65535) {
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    MarkupDeclarationOpen { // from class: org.jsoup.parser.TokeniserState.44
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchConsume("--")) {
                t.createCommentPending();
                t.transition(CommentStart);
            } else if (r.matchConsumeIgnoreCase("DOCTYPE")) {
                t.transition(Doctype);
            } else if (r.matchConsume("[CDATA[")) {
                t.createTempBuffer();
                t.transition(CdataSection);
            } else {
                t.error(this);
                t.createBogusCommentPending();
                t.advanceTransition(BogusComment);
            }
        }
    },
    CommentStart { // from class: org.jsoup.parser.TokeniserState.45
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.commentPending.append((char) 65533);
                t.transition(Comment);
            } else if (c == '-') {
                t.transition(CommentStartDash);
            } else if (c == '>') {
                t.error(this);
                t.emitCommentPending();
                t.transition(Data);
            } else if (c != 65535) {
                r.unconsume();
                t.transition(Comment);
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    CommentStartDash { // from class: org.jsoup.parser.TokeniserState.46
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.commentPending.append((char) 65533);
                t.transition(Comment);
            } else if (c == '-') {
                t.transition(CommentStartDash);
            } else if (c == '>') {
                t.error(this);
                t.emitCommentPending();
                t.transition(Data);
            } else if (c != 65535) {
                t.commentPending.append(c);
                t.transition(Comment);
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    Comment { // from class: org.jsoup.parser.TokeniserState.47
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.current();
            if (c == 0) {
                t.error(this);
                r.advance();
                t.commentPending.append((char) 65533);
            } else if (c == '-') {
                t.advanceTransition(CommentEndDash);
            } else if (c != 65535) {
                t.commentPending.append(r.consumeToAny('-', 0));
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    CommentEndDash { // from class: org.jsoup.parser.TokeniserState.48
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.commentPending.append('-').append((char) 65533);
                t.transition(Comment);
            } else if (c == '-') {
                t.transition(CommentEnd);
            } else if (c != 65535) {
                t.commentPending.append('-').append(c);
                t.transition(Comment);
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    CommentEnd { // from class: org.jsoup.parser.TokeniserState.49
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.commentPending.append("--").append((char) 65533);
                t.transition(Comment);
            } else if (c == '!') {
                t.error(this);
                t.transition(CommentEndBang);
            } else if (c == '-') {
                t.error(this);
                t.commentPending.append('-');
            } else if (c == '>') {
                t.emitCommentPending();
                t.transition(Data);
            } else if (c != 65535) {
                t.error(this);
                t.commentPending.append("--").append(c);
                t.transition(Comment);
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    CommentEndBang { // from class: org.jsoup.parser.TokeniserState.50
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.commentPending.append("--!").append((char) 65533);
                t.transition(Comment);
            } else if (c == '-') {
                t.commentPending.append("--!");
                t.transition(CommentEndDash);
            } else if (c == '>') {
                t.emitCommentPending();
                t.transition(Data);
            } else if (c != 65535) {
                t.commentPending.append("--!").append(c);
                t.transition(Comment);
            } else {
                t.eofError(this);
                t.emitCommentPending();
                t.transition(Data);
            }
        }
    },
    Doctype { // from class: org.jsoup.parser.TokeniserState.51
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BeforeDoctypeName);
                return;
            }
            if (c != '>') {
                if (c != 65535) {
                    t.error(this);
                    t.transition(BeforeDoctypeName);
                    return;
                }
                t.eofError(this);
            }
            t.error(this);
            t.createDoctypePending();
            t.doctypePending.forceQuirks = true;
            t.emitDoctypePending();
            t.transition(Data);
        }
    },
    BeforeDoctypeName { // from class: org.jsoup.parser.TokeniserState.52
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                t.createDoctypePending();
                t.transition(DoctypeName);
                return;
            }
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.createDoctypePending();
                t.doctypePending.name.append((char) 65533);
                t.transition(DoctypeName);
            } else if (c == ' ') {
            } else {
                if (c == 65535) {
                    t.eofError(this);
                    t.createDoctypePending();
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                } else if (c != '\t' && c != '\n' && c != '\f' && c != '\r') {
                    t.createDoctypePending();
                    t.doctypePending.name.append(c);
                    t.transition(DoctypeName);
                }
            }
        }
    },
    DoctypeName { // from class: org.jsoup.parser.TokeniserState.53
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.matchesLetter()) {
                String name = r.consumeLetterSequence();
                t.doctypePending.name.append(name);
                return;
            }
            char c = r.consume();
            if (c != 0) {
                if (c != ' ') {
                    if (c == '>') {
                        t.emitDoctypePending();
                        t.transition(Data);
                        return;
                    } else if (c == 65535) {
                        t.eofError(this);
                        t.doctypePending.forceQuirks = true;
                        t.emitDoctypePending();
                        t.transition(Data);
                        return;
                    } else if (!(c == '\t' || c == '\n' || c == '\f' || c == '\r')) {
                        t.doctypePending.name.append(c);
                        return;
                    }
                }
                t.transition(AfterDoctypeName);
                return;
            }
            t.error(this);
            t.doctypePending.name.append((char) 65533);
        }
    },
    AfterDoctypeName { // from class: org.jsoup.parser.TokeniserState.54
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            if (r.isEmpty()) {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (r.matchesAny('\t', '\n', '\r', '\f', ' ')) {
                r.advance();
            } else if (r.matches(Typography.greater)) {
                t.emitDoctypePending();
                t.advanceTransition(Data);
            } else if (r.matchConsumeIgnoreCase(DocumentType.PUBLIC_KEY)) {
                t.doctypePending.pubSysKey = DocumentType.PUBLIC_KEY;
                t.transition(AfterDoctypePublicKeyword);
            } else if (r.matchConsumeIgnoreCase(DocumentType.SYSTEM_KEY)) {
                t.doctypePending.pubSysKey = DocumentType.SYSTEM_KEY;
                t.transition(AfterDoctypeSystemKeyword);
            } else {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.advanceTransition(BogusDoctype);
            }
        }
    },
    AfterDoctypePublicKeyword { // from class: org.jsoup.parser.TokeniserState.55
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BeforeDoctypePublicIdentifier);
            } else if (c == '\"') {
                t.error(this);
                t.transition(DoctypePublicIdentifier_doubleQuoted);
            } else if (c == '\'') {
                t.error(this);
                t.transition(DoctypePublicIdentifier_singleQuoted);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.transition(BogusDoctype);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    BeforeDoctypePublicIdentifier { // from class: org.jsoup.parser.TokeniserState.56
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != ' ') {
                if (c == '\"') {
                    t.transition(DoctypePublicIdentifier_doubleQuoted);
                } else if (c == '\'') {
                    t.transition(DoctypePublicIdentifier_singleQuoted);
                } else if (c == '>') {
                    t.error(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                } else if (c != 65535) {
                    t.error(this);
                    t.doctypePending.forceQuirks = true;
                    t.transition(BogusDoctype);
                } else {
                    t.eofError(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                }
            }
        }
    },
    DoctypePublicIdentifier_doubleQuoted { // from class: org.jsoup.parser.TokeniserState.57
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.doctypePending.publicIdentifier.append((char) 65533);
            } else if (c == '\"') {
                t.transition(AfterDoctypePublicIdentifier);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.doctypePending.publicIdentifier.append(c);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    DoctypePublicIdentifier_singleQuoted { // from class: org.jsoup.parser.TokeniserState.58
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.doctypePending.publicIdentifier.append((char) 65533);
            } else if (c == '\'') {
                t.transition(AfterDoctypePublicIdentifier);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.doctypePending.publicIdentifier.append(c);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    AfterDoctypePublicIdentifier { // from class: org.jsoup.parser.TokeniserState.59
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BetweenDoctypePublicAndSystemIdentifiers);
            } else if (c == '\"') {
                t.error(this);
                t.transition(DoctypeSystemIdentifier_doubleQuoted);
            } else if (c == '\'') {
                t.error(this);
                t.transition(DoctypeSystemIdentifier_singleQuoted);
            } else if (c == '>') {
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.transition(BogusDoctype);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    BetweenDoctypePublicAndSystemIdentifiers { // from class: org.jsoup.parser.TokeniserState.60
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != ' ') {
                if (c == '\"') {
                    t.error(this);
                    t.transition(DoctypeSystemIdentifier_doubleQuoted);
                } else if (c == '\'') {
                    t.error(this);
                    t.transition(DoctypeSystemIdentifier_singleQuoted);
                } else if (c == '>') {
                    t.emitDoctypePending();
                    t.transition(Data);
                } else if (c != 65535) {
                    t.error(this);
                    t.doctypePending.forceQuirks = true;
                    t.transition(BogusDoctype);
                } else {
                    t.eofError(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                }
            }
        }
    },
    AfterDoctypeSystemKeyword { // from class: org.jsoup.parser.TokeniserState.61
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BeforeDoctypeSystemIdentifier);
            } else if (c == '\"') {
                t.error(this);
                t.transition(DoctypeSystemIdentifier_doubleQuoted);
            } else if (c == '\'') {
                t.error(this);
                t.transition(DoctypeSystemIdentifier_singleQuoted);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    BeforeDoctypeSystemIdentifier { // from class: org.jsoup.parser.TokeniserState.62
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != ' ') {
                if (c == '\"') {
                    t.transition(DoctypeSystemIdentifier_doubleQuoted);
                } else if (c == '\'') {
                    t.transition(DoctypeSystemIdentifier_singleQuoted);
                } else if (c == '>') {
                    t.error(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                } else if (c != 65535) {
                    t.error(this);
                    t.doctypePending.forceQuirks = true;
                    t.transition(BogusDoctype);
                } else {
                    t.eofError(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                }
            }
        }
    },
    DoctypeSystemIdentifier_doubleQuoted { // from class: org.jsoup.parser.TokeniserState.63
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.doctypePending.systemIdentifier.append((char) 65533);
            } else if (c == '\"') {
                t.transition(AfterDoctypeSystemIdentifier);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.doctypePending.systemIdentifier.append(c);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    DoctypeSystemIdentifier_singleQuoted { // from class: org.jsoup.parser.TokeniserState.64
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == 0) {
                t.error(this);
                t.doctypePending.systemIdentifier.append((char) 65533);
            } else if (c == '\'') {
                t.transition(AfterDoctypeSystemIdentifier);
            } else if (c == '>') {
                t.error(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c != 65535) {
                t.doctypePending.systemIdentifier.append(c);
            } else {
                t.eofError(this);
                t.doctypePending.forceQuirks = true;
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    AfterDoctypeSystemIdentifier { // from class: org.jsoup.parser.TokeniserState.65
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c != '\t' && c != '\n' && c != '\f' && c != '\r' && c != ' ') {
                if (c == '>') {
                    t.emitDoctypePending();
                    t.transition(Data);
                } else if (c != 65535) {
                    t.error(this);
                    t.transition(BogusDoctype);
                } else {
                    t.eofError(this);
                    t.doctypePending.forceQuirks = true;
                    t.emitDoctypePending();
                    t.transition(Data);
                }
            }
        }
    },
    BogusDoctype { // from class: org.jsoup.parser.TokeniserState.66
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            char c = r.consume();
            if (c == '>') {
                t.emitDoctypePending();
                t.transition(Data);
            } else if (c == 65535) {
                t.emitDoctypePending();
                t.transition(Data);
            }
        }
    },
    CdataSection { // from class: org.jsoup.parser.TokeniserState.67
        @Override // org.jsoup.parser.TokeniserState
        void read(Tokeniser t, CharacterReader r) {
            String data = r.consumeTo("]]>");
            t.dataBuffer.append(data);
            if (r.matchConsume("]]>") || r.isEmpty()) {
                t.emit(new Token.CData(t.dataBuffer.toString()));
                t.transition(Data);
            }
        }
    };
    
    private static final char eof = 65535;
    static final char nullChar = 0;
    private static final char replacementChar = 65533;
    static final char[] attributeSingleValueCharsSorted = {0, Typography.amp, '\''};
    static final char[] attributeDoubleValueCharsSorted = {0, Typography.quote, Typography.amp};
    static final char[] attributeNameCharsSorted = {0, '\t', '\n', '\f', '\r', ' ', Typography.quote, '\'', '/', Typography.less, '=', Typography.greater};
    static final char[] attributeValueUnquoted = {0, '\t', '\n', '\f', '\r', ' ', Typography.quote, Typography.amp, '\'', Typography.less, '=', Typography.greater, '`'};
    private static final String replacementStr = String.valueOf((char) 65533);

    /* JADX INFO: Access modifiers changed from: package-private */
    public abstract void read(Tokeniser tokeniser, CharacterReader characterReader);

    /* JADX INFO: Access modifiers changed from: private */
    public static void handleDataEndTag(Tokeniser t, CharacterReader r, TokeniserState elseTransition) {
        if (r.matchesLetter()) {
            String name = r.consumeLetterSequence();
            t.tagPending.appendTagName(name);
            t.dataBuffer.append(name);
            return;
        }
        boolean needsExitTransition = false;
        if (!t.isAppropriateEndTagToken() || r.isEmpty()) {
            needsExitTransition = true;
        } else {
            char c = r.consume();
            if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ') {
                t.transition(BeforeAttributeName);
            } else if (c == '/') {
                t.transition(SelfClosingStartTag);
            } else if (c != '>') {
                t.dataBuffer.append(c);
                needsExitTransition = true;
            } else {
                t.emitTagPending();
                t.transition(Data);
            }
        }
        if (needsExitTransition) {
            t.emit("</" + t.dataBuffer.toString());
            t.transition(elseTransition);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void readRawData(Tokeniser t, CharacterReader r, TokeniserState current, TokeniserState advance) {
        char current2 = r.current();
        if (current2 == 0) {
            t.error(current);
            r.advance();
            t.emit((char) 65533);
        } else if (current2 == '<') {
            t.advanceTransition(advance);
        } else if (current2 != 65535) {
            String data = r.consumeRawData();
            t.emit(data);
        } else {
            t.emit(new Token.EOF());
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void readCharRef(Tokeniser t, TokeniserState advance) {
        int[] c = t.consumeCharacterReference(null, false);
        if (c == null) {
            t.emit(Typography.amp);
        } else {
            t.emit(c);
        }
        t.transition(advance);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void readEndTag(Tokeniser t, CharacterReader r, TokeniserState a, TokeniserState b) {
        if (r.matchesLetter()) {
            t.createTagPending(false);
            t.transition(a);
            return;
        }
        t.emit("</");
        t.transition(b);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void handleDataDoubleEscapeTag(Tokeniser t, CharacterReader r, TokeniserState primary, TokeniserState fallback) {
        if (r.matchesLetter()) {
            String name = r.consumeLetterSequence();
            t.dataBuffer.append(name);
            t.emit(name);
            return;
        }
        char c = r.consume();
        if (c == '\t' || c == '\n' || c == '\f' || c == '\r' || c == ' ' || c == '/' || c == '>') {
            if (t.dataBuffer.toString().equals("script")) {
                t.transition(primary);
            } else {
                t.transition(fallback);
            }
            t.emit(c);
            return;
        }
        r.unconsume();
        t.transition(fallback);
    }
}
