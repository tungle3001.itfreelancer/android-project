package org.jsoup.parser;

import java.util.ArrayList;
import java.util.Iterator;
import kotlin.text.Typography;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Token;

/* JADX INFO: Access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public enum HtmlTreeBuilderState {
    Initial { // from class: org.jsoup.parser.HtmlTreeBuilderState.1
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                return true;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                Token.Doctype d = t.asDoctype();
                DocumentType doctype = new DocumentType(tb.settings.normalizeTag(d.getName()), d.getPublicIdentifier(), d.getSystemIdentifier());
                doctype.setPubSysKey(d.getPubSysKey());
                tb.getDocument().appendChild(doctype);
                if (d.isForceQuirks()) {
                    tb.getDocument().quirksMode(Document.QuirksMode.quirks);
                }
                tb.transition(BeforeHtml);
            } else {
                tb.transition(BeforeHtml);
                return tb.process(t);
            }
            return true;
        }
    },
    BeforeHtml { // from class: org.jsoup.parser.HtmlTreeBuilderState.2
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isStartTag() && t.asStartTag().normalName().equals("html")) {
                tb.insert(t.asStartTag());
                tb.transition(BeforeHead);
                return true;
            } else if (t.isEndTag() && StringUtil.inSorted(t.asEndTag().normalName(), Constants.BeforeHtmlToHead)) {
                return anythingElse(t, tb);
            } else {
                if (!t.isEndTag()) {
                    return anythingElse(t, tb);
                }
                tb.error(this);
                return false;
            }
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            tb.insertStartTag("html");
            tb.transition(BeforeHead);
            return tb.process(t);
        }
    },
    BeforeHead { // from class: org.jsoup.parser.HtmlTreeBuilderState.3
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().normalName().equals("html")) {
                return InBody.process(t, tb);
            } else {
                if (t.isStartTag() && t.asStartTag().normalName().equals("head")) {
                    Element head = tb.insert(t.asStartTag());
                    tb.setHeadElement(head);
                    tb.transition(InHead);
                    return true;
                } else if (t.isEndTag() && StringUtil.inSorted(t.asEndTag().normalName(), Constants.BeforeHtmlToHead)) {
                    tb.processStartTag("head");
                    return tb.process(t);
                } else if (t.isEndTag()) {
                    tb.error(this);
                    return false;
                } else {
                    tb.processStartTag("head");
                    return tb.process(t);
                }
            }
        }
    },
    InHead { // from class: org.jsoup.parser.HtmlTreeBuilderState.4
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            }
            int i = AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()];
            if (i == 1) {
                tb.insert(t.asComment());
            } else if (i == 2) {
                tb.error(this);
                return false;
            } else if (i == 3) {
                Token.StartTag start = t.asStartTag();
                String name = start.normalName();
                if (name.equals("html")) {
                    return InBody.process(t, tb);
                }
                if (StringUtil.inSorted(name, Constants.InHeadEmpty)) {
                    Element el = tb.insertEmpty(start);
                    if (name.equals("base") && el.hasAttr("href")) {
                        tb.maybeSetBaseUri(el);
                    }
                } else if (name.equals("meta")) {
                    tb.insertEmpty(start);
                } else if (name.equals("title")) {
                    HtmlTreeBuilderState.handleRcData(start, tb);
                } else if (StringUtil.inSorted(name, Constants.InHeadRaw)) {
                    HtmlTreeBuilderState.handleRawtext(start, tb);
                } else if (name.equals("noscript")) {
                    tb.insert(start);
                    tb.transition(InHeadNoscript);
                } else if (name.equals("script")) {
                    tb.tokeniser.transition(TokeniserState.ScriptData);
                    tb.markInsertionMode();
                    tb.transition(Text);
                    tb.insert(start);
                } else if (!name.equals("head")) {
                    return anythingElse(t, tb);
                } else {
                    tb.error(this);
                    return false;
                }
            } else if (i != 4) {
                return anythingElse(t, tb);
            } else {
                Token.EndTag end = t.asEndTag();
                String name2 = end.normalName();
                if (name2.equals("head")) {
                    tb.pop();
                    tb.transition(AfterHead);
                } else if (StringUtil.inSorted(name2, Constants.InHeadEnd)) {
                    return anythingElse(t, tb);
                } else {
                    tb.error(this);
                    return false;
                }
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.processEndTag("head");
            return tb.process(t);
        }
    },
    InHeadNoscript { // from class: org.jsoup.parser.HtmlTreeBuilderState.5
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isDoctype()) {
                tb.error(this);
                return true;
            } else if (t.isStartTag() && t.asStartTag().normalName().equals("html")) {
                return tb.process(t, InBody);
            } else {
                if (t.isEndTag() && t.asEndTag().normalName().equals("noscript")) {
                    tb.pop();
                    tb.transition(InHead);
                    return true;
                } else if (HtmlTreeBuilderState.isWhitespace(t) || t.isComment() || (t.isStartTag() && StringUtil.inSorted(t.asStartTag().normalName(), Constants.InHeadNoScriptHead))) {
                    return tb.process(t, InHead);
                } else {
                    if (t.isEndTag() && t.asEndTag().normalName().equals("br")) {
                        return anythingElse(t, tb);
                    }
                    if ((!t.isStartTag() || !StringUtil.inSorted(t.asStartTag().normalName(), Constants.InHeadNoscriptIgnore)) && !t.isEndTag()) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                }
            }
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            tb.error(this);
            tb.insert(new Token.Character().data(t.toString()));
            return true;
        }
    },
    AfterHead { // from class: org.jsoup.parser.HtmlTreeBuilderState.6
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return true;
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.normalName();
                if (name.equals("html")) {
                    return tb.process(t, InBody);
                }
                if (name.equals("body")) {
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    tb.transition(InBody);
                    return true;
                } else if (name.equals("frameset")) {
                    tb.insert(startTag);
                    tb.transition(InFrameset);
                    return true;
                } else if (StringUtil.inSorted(name, Constants.InBodyStartToHead)) {
                    tb.error(this);
                    Element head = tb.getHeadElement();
                    tb.push(head);
                    tb.process(t, InHead);
                    tb.removeFromStack(head);
                    return true;
                } else if (name.equals("head")) {
                    tb.error(this);
                    return false;
                } else {
                    anythingElse(t, tb);
                    return true;
                }
            } else if (!t.isEndTag()) {
                anythingElse(t, tb);
                return true;
            } else if (StringUtil.inSorted(t.asEndTag().normalName(), Constants.AfterHeadBody)) {
                anythingElse(t, tb);
                return true;
            } else {
                tb.error(this);
                return false;
            }
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            tb.processStartTag("body");
            tb.framesetOk(true);
            return tb.process(t);
        }
    },
    InBody { // from class: org.jsoup.parser.HtmlTreeBuilderState.7
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            int i = AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()];
            if (i == 1) {
                tb.insert(t.asComment());
            } else if (i == 2) {
                tb.error(this);
                return false;
            } else if (i == 3) {
                return inBodyStartTag(t, tb);
            } else {
                if (i == 4) {
                    return inBodyEndTag(t, tb);
                }
                if (i == 5) {
                    Token.Character c = t.asCharacter();
                    if (c.getData().equals(HtmlTreeBuilderState.nullString)) {
                        tb.error(this);
                        return false;
                    } else if (!tb.framesetOk() || !HtmlTreeBuilderState.isWhitespace(c)) {
                        tb.reconstructFormattingElements();
                        tb.insert(c);
                        tb.framesetOk(false);
                    } else {
                        tb.reconstructFormattingElements();
                        tb.insert(c);
                    }
                }
            }
            return true;
        }

        /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
        private boolean inBodyStartTag(Token t, HtmlTreeBuilder tb) {
            char c;
            String prompt;
            Token.StartTag startTag = t.asStartTag();
            String name = startTag.normalName();
            int hashCode = name.hashCode();
            switch (hashCode) {
                case -1644953643:
                    if (name.equals("frameset")) {
                        c = 5;
                        break;
                    }
                    c = 65535;
                    break;
                case -1377687758:
                    if (name.equals("button")) {
                        c = '\b';
                        break;
                    }
                    c = 65535;
                    break;
                case -1191214428:
                    if (name.equals("iframe")) {
                        c = 17;
                        break;
                    }
                    c = 65535;
                    break;
                case -1010136971:
                    if (name.equals("option")) {
                        c = '!';
                        break;
                    }
                    c = 65535;
                    break;
                case -1003243718:
                    if (name.equals("textarea")) {
                        c = 15;
                        break;
                    }
                    c = 65535;
                    break;
                case -906021636:
                    if (name.equals("select")) {
                        c = 19;
                        break;
                    }
                    c = 65535;
                    break;
                case -80773204:
                    if (name.equals("optgroup")) {
                        c = ' ';
                        break;
                    }
                    c = 65535;
                    break;
                case 97:
                    if (name.equals("a")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                case 3200:
                    if (name.equals("dd")) {
                        c = 30;
                        break;
                    }
                    c = 65535;
                    break;
                case 3216:
                    if (name.equals("dt")) {
                        c = 31;
                        break;
                    }
                    c = 65535;
                    break;
                case 3338:
                    if (name.equals("hr")) {
                        c = '\f';
                        break;
                    }
                    c = 65535;
                    break;
                case 3453:
                    if (name.equals("li")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 3646:
                    if (name.equals("rp")) {
                        c = Typography.quote;
                        break;
                    }
                    c = 65535;
                    break;
                case 3650:
                    if (name.equals("rt")) {
                        c = '#';
                        break;
                    }
                    c = 65535;
                    break;
                case 111267:
                    if (name.equals("pre")) {
                        c = 28;
                        break;
                    }
                    c = 65535;
                    break;
                case 114276:
                    if (name.equals("svg")) {
                        c = 21;
                        break;
                    }
                    c = 65535;
                    break;
                case 118811:
                    if (name.equals("xmp")) {
                        c = 16;
                        break;
                    }
                    c = 65535;
                    break;
                case 3029410:
                    if (name.equals("body")) {
                        c = 4;
                        break;
                    }
                    c = 65535;
                    break;
                case 3148996:
                    if (name.equals("form")) {
                        c = 6;
                        break;
                    }
                    c = 65535;
                    break;
                case 3213227:
                    if (name.equals("html")) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                case 3344136:
                    if (name.equals("math")) {
                        c = 20;
                        break;
                    }
                    c = 65535;
                    break;
                case 3386833:
                    if (name.equals("nobr")) {
                        c = '\t';
                        break;
                    }
                    c = 65535;
                    break;
                case 3536714:
                    if (name.equals("span")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 100313435:
                    if (name.equals("image")) {
                        c = '\r';
                        break;
                    }
                    c = 65535;
                    break;
                case 100358090:
                    if (name.equals("input")) {
                        c = 11;
                        break;
                    }
                    c = 65535;
                    break;
                case 110115790:
                    if (name.equals("table")) {
                        c = '\n';
                        break;
                    }
                    c = 65535;
                    break;
                case 181975684:
                    if (name.equals("listing")) {
                        c = 29;
                        break;
                    }
                    c = 65535;
                    break;
                case 1973234167:
                    if (name.equals("plaintext")) {
                        c = 7;
                        break;
                    }
                    c = 65535;
                    break;
                case 2091304424:
                    if (name.equals("isindex")) {
                        c = 14;
                        break;
                    }
                    c = 65535;
                    break;
                case 2115613112:
                    if (name.equals("noembed")) {
                        c = 18;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    switch (hashCode) {
                        case 3273:
                            if (name.equals("h1")) {
                                c = 22;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3274:
                            if (name.equals("h2")) {
                                c = 23;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3275:
                            if (name.equals("h3")) {
                                c = 24;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3276:
                            if (name.equals("h4")) {
                                c = 25;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3277:
                            if (name.equals("h5")) {
                                c = 26;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3278:
                            if (name.equals("h6")) {
                                c = 27;
                                break;
                            }
                            c = 65535;
                            break;
                        default:
                            c = 65535;
                            break;
                    }
            }
            switch (c) {
                case 0:
                    if (tb.getActiveFormattingElement("a") != null) {
                        tb.error(this);
                        tb.processEndTag("a");
                        Element remainingA = tb.getFromStack("a");
                        if (remainingA != null) {
                            tb.removeFromActiveFormattingElements(remainingA);
                            tb.removeFromStack(remainingA);
                        }
                    }
                    tb.reconstructFormattingElements();
                    tb.pushActiveFormattingElements(tb.insert(startTag));
                    return true;
                case 1:
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    return true;
                case 2:
                    tb.framesetOk(false);
                    ArrayList<Element> stack = tb.getStack();
                    int i = stack.size() - 1;
                    while (true) {
                        if (i > 0) {
                            Element el = stack.get(i);
                            if (el.normalName().equals("li")) {
                                tb.processEndTag("li");
                            } else if (!tb.isSpecial(el) || StringUtil.inSorted(el.normalName(), Constants.InBodyStartLiBreakers)) {
                                i--;
                            }
                        }
                    }
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insert(startTag);
                    return true;
                case 3:
                    tb.error(this);
                    Element html = tb.getStack().get(0);
                    Iterator<Attribute> it = startTag.getAttributes().iterator();
                    while (it.hasNext()) {
                        Attribute attribute = it.next();
                        if (!html.hasAttr(attribute.getKey())) {
                            html.attributes().put(attribute);
                        }
                    }
                    return true;
                case 4:
                    tb.error(this);
                    ArrayList<Element> stack2 = tb.getStack();
                    if (stack2.size() == 1) {
                        return false;
                    }
                    if (stack2.size() > 2 && !stack2.get(1).normalName().equals("body")) {
                        return false;
                    }
                    tb.framesetOk(false);
                    Element body = stack2.get(1);
                    Iterator<Attribute> it2 = startTag.getAttributes().iterator();
                    while (it2.hasNext()) {
                        Attribute attribute2 = it2.next();
                        if (!body.hasAttr(attribute2.getKey())) {
                            body.attributes().put(attribute2);
                        }
                    }
                    return true;
                case 5:
                    tb.error(this);
                    ArrayList<Element> stack3 = tb.getStack();
                    if (stack3.size() == 1) {
                        return false;
                    }
                    if ((stack3.size() > 2 && !stack3.get(1).normalName().equals("body")) || !tb.framesetOk()) {
                        return false;
                    }
                    Element second = stack3.get(1);
                    if (second.parent() != null) {
                        second.remove();
                    }
                    while (stack3.size() > 1) {
                        stack3.remove(stack3.size() - 1);
                    }
                    tb.insert(startTag);
                    tb.transition(InFrameset);
                    return true;
                case 6:
                    if (tb.getFormElement() != null) {
                        tb.error(this);
                        return false;
                    }
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insertForm(startTag, true);
                    return true;
                case 7:
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insert(startTag);
                    tb.tokeniser.transition(TokeniserState.PLAINTEXT);
                    return true;
                case '\b':
                    if (tb.inButtonScope("button")) {
                        tb.error(this);
                        tb.processEndTag("button");
                        tb.process(startTag);
                        return true;
                    }
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    return true;
                case '\t':
                    tb.reconstructFormattingElements();
                    if (tb.inScope("nobr")) {
                        tb.error(this);
                        tb.processEndTag("nobr");
                        tb.reconstructFormattingElements();
                    }
                    tb.pushActiveFormattingElements(tb.insert(startTag));
                    return true;
                case '\n':
                    if (tb.getDocument().quirksMode() != Document.QuirksMode.quirks && tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    tb.transition(InTable);
                    return true;
                case 11:
                    tb.reconstructFormattingElements();
                    if (tb.insertEmpty(startTag).attr("type").equalsIgnoreCase("hidden")) {
                        return true;
                    }
                    tb.framesetOk(false);
                    return true;
                case '\f':
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insertEmpty(startTag);
                    tb.framesetOk(false);
                    return true;
                case '\r':
                    if (tb.getFromStack("svg") == null) {
                        return tb.process(startTag.name("img"));
                    }
                    tb.insert(startTag);
                    return true;
                case 14:
                    tb.error(this);
                    if (tb.getFormElement() != null) {
                        return false;
                    }
                    tb.processStartTag("form");
                    if (startTag.attributes.hasKey("action")) {
                        Element form = tb.getFormElement();
                        form.attr("action", startTag.attributes.get("action"));
                    }
                    tb.processStartTag("hr");
                    tb.processStartTag("label");
                    if (startTag.attributes.hasKey("prompt")) {
                        prompt = startTag.attributes.get("prompt");
                    } else {
                        prompt = "This is a searchable index. Enter search keywords: ";
                    }
                    tb.process(new Token.Character().data(prompt));
                    Attributes inputAttribs = new Attributes();
                    Iterator<Attribute> it3 = startTag.attributes.iterator();
                    while (it3.hasNext()) {
                        Attribute attr = it3.next();
                        if (!StringUtil.inSorted(attr.getKey(), Constants.InBodyStartInputAttribs)) {
                            inputAttribs.put(attr);
                        }
                    }
                    inputAttribs.put("name", "isindex");
                    tb.processStartTag("input", inputAttribs);
                    tb.processEndTag("label");
                    tb.processStartTag("hr");
                    tb.processEndTag("form");
                    return true;
                case 15:
                    tb.insert(startTag);
                    if (startTag.isSelfClosing()) {
                        return true;
                    }
                    tb.tokeniser.transition(TokeniserState.Rcdata);
                    tb.markInsertionMode();
                    tb.framesetOk(false);
                    tb.transition(Text);
                    return true;
                case 16:
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.reconstructFormattingElements();
                    tb.framesetOk(false);
                    HtmlTreeBuilderState.handleRawtext(startTag, tb);
                    return true;
                case 17:
                    tb.framesetOk(false);
                    HtmlTreeBuilderState.handleRawtext(startTag, tb);
                    return true;
                case 18:
                    HtmlTreeBuilderState.handleRawtext(startTag, tb);
                    return true;
                case 19:
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    HtmlTreeBuilderState state = tb.state();
                    if (state.equals(InTable) || state.equals(InCaption) || state.equals(InTableBody) || state.equals(InRow) || state.equals(InCell)) {
                        tb.transition(InSelectInTable);
                        return true;
                    }
                    tb.transition(InSelect);
                    return true;
                case 20:
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    return true;
                case 21:
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    return true;
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    if (StringUtil.inSorted(tb.currentElement().normalName(), Constants.Headings)) {
                        tb.error(this);
                        tb.pop();
                    }
                    tb.insert(startTag);
                    return true;
                case 28:
                case 29:
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insert(startTag);
                    tb.reader.matchConsume("\n");
                    tb.framesetOk(false);
                    return true;
                case 30:
                case 31:
                    tb.framesetOk(false);
                    ArrayList<Element> stack4 = tb.getStack();
                    int i2 = stack4.size() - 1;
                    while (true) {
                        if (i2 > 0) {
                            Element el2 = stack4.get(i2);
                            if (StringUtil.inSorted(el2.normalName(), Constants.DdDt)) {
                                tb.processEndTag(el2.normalName());
                            } else if (!tb.isSpecial(el2) || StringUtil.inSorted(el2.normalName(), Constants.InBodyStartLiBreakers)) {
                                i2--;
                            }
                        }
                    }
                    if (tb.inButtonScope("p")) {
                        tb.processEndTag("p");
                    }
                    tb.insert(startTag);
                    return true;
                case ' ':
                case '!':
                    if (tb.currentElement().normalName().equals("option")) {
                        tb.processEndTag("option");
                    }
                    tb.reconstructFormattingElements();
                    tb.insert(startTag);
                    return true;
                case '\"':
                case '#':
                    if (!tb.inScope("ruby")) {
                        return true;
                    }
                    tb.generateImpliedEndTags();
                    if (!tb.currentElement().normalName().equals("ruby")) {
                        tb.error(this);
                        tb.popStackToBefore("ruby");
                    }
                    tb.insert(startTag);
                    return true;
                default:
                    if (StringUtil.inSorted(name, Constants.InBodyStartEmptyFormatters)) {
                        tb.reconstructFormattingElements();
                        tb.insertEmpty(startTag);
                        tb.framesetOk(false);
                        return true;
                    } else if (StringUtil.inSorted(name, Constants.InBodyStartPClosers)) {
                        if (tb.inButtonScope("p")) {
                            tb.processEndTag("p");
                        }
                        tb.insert(startTag);
                        return true;
                    } else if (StringUtil.inSorted(name, Constants.InBodyStartToHead)) {
                        return tb.process(t, InHead);
                    } else {
                        if (StringUtil.inSorted(name, Constants.Formatters)) {
                            tb.reconstructFormattingElements();
                            tb.pushActiveFormattingElements(tb.insert(startTag));
                            return true;
                        } else if (StringUtil.inSorted(name, Constants.InBodyStartApplets)) {
                            tb.reconstructFormattingElements();
                            tb.insert(startTag);
                            tb.insertMarkerToFormattingElements();
                            tb.framesetOk(false);
                            return true;
                        } else if (StringUtil.inSorted(name, Constants.InBodyStartMedia)) {
                            tb.insertEmpty(startTag);
                            return true;
                        } else if (StringUtil.inSorted(name, Constants.InBodyStartDrop)) {
                            tb.error(this);
                            return false;
                        } else {
                            tb.reconstructFormattingElements();
                            tb.insert(startTag);
                            return true;
                        }
                    }
            }
        }

        /* JADX WARN: Can't fix incorrect switch cases order, some code will duplicate */
        private boolean inBodyEndTag(Token t, HtmlTreeBuilder tb) {
            char c;
            Token.EndTag endTag = t.asEndTag();
            String name = endTag.normalName();
            int hashCode = name.hashCode();
            switch (hashCode) {
                case 112:
                    if (name.equals("p")) {
                        c = 6;
                        break;
                    }
                    c = 65535;
                    break;
                case 3152:
                    if (name.equals("br")) {
                        c = 15;
                        break;
                    }
                    c = 65535;
                    break;
                case 3200:
                    if (name.equals("dd")) {
                        c = 7;
                        break;
                    }
                    c = 65535;
                    break;
                case 3216:
                    if (name.equals("dt")) {
                        c = '\b';
                        break;
                    }
                    c = 65535;
                    break;
                case 3453:
                    if (name.equals("li")) {
                        c = 2;
                        break;
                    }
                    c = 65535;
                    break;
                case 3029410:
                    if (name.equals("body")) {
                        c = 3;
                        break;
                    }
                    c = 65535;
                    break;
                case 3148996:
                    if (name.equals("form")) {
                        c = 5;
                        break;
                    }
                    c = 65535;
                    break;
                case 3213227:
                    if (name.equals("html")) {
                        c = 4;
                        break;
                    }
                    c = 65535;
                    break;
                case 3536714:
                    if (name.equals("span")) {
                        c = 1;
                        break;
                    }
                    c = 65535;
                    break;
                case 1869063452:
                    if (name.equals("sarcasm")) {
                        c = 0;
                        break;
                    }
                    c = 65535;
                    break;
                default:
                    switch (hashCode) {
                        case 3273:
                            if (name.equals("h1")) {
                                c = '\t';
                                break;
                            }
                            c = 65535;
                            break;
                        case 3274:
                            if (name.equals("h2")) {
                                c = '\n';
                                break;
                            }
                            c = 65535;
                            break;
                        case 3275:
                            if (name.equals("h3")) {
                                c = 11;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3276:
                            if (name.equals("h4")) {
                                c = '\f';
                                break;
                            }
                            c = 65535;
                            break;
                        case 3277:
                            if (name.equals("h5")) {
                                c = '\r';
                                break;
                            }
                            c = 65535;
                            break;
                        case 3278:
                            if (name.equals("h6")) {
                                c = 14;
                                break;
                            }
                            c = 65535;
                            break;
                        default:
                            c = 65535;
                            break;
                    }
            }
            switch (c) {
                case 0:
                case 1:
                    return anyOtherEndTag(t, tb);
                case 2:
                    if (tb.inListItemScope(name)) {
                        tb.generateImpliedEndTags(name);
                        if (!tb.currentElement().normalName().equals(name)) {
                            tb.error(this);
                        }
                        tb.popStackToClose(name);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 3:
                    boolean notIgnored = tb.inScope("body");
                    if (notIgnored) {
                        tb.transition(AfterBody);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 4:
                    boolean notIgnored2 = tb.processEndTag("body");
                    if (notIgnored2) {
                        return tb.process(endTag);
                    }
                    break;
                case 5:
                    Element currentForm = tb.getFormElement();
                    tb.setFormElement(null);
                    if (currentForm != null && tb.inScope(name)) {
                        tb.generateImpliedEndTags();
                        if (!tb.currentElement().normalName().equals(name)) {
                            tb.error(this);
                        }
                        tb.removeFromStack(currentForm);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 6:
                    if (tb.inButtonScope(name)) {
                        tb.generateImpliedEndTags(name);
                        if (!tb.currentElement().normalName().equals(name)) {
                            tb.error(this);
                        }
                        tb.popStackToClose(name);
                        break;
                    } else {
                        tb.error(this);
                        tb.processStartTag(name);
                        return tb.process(endTag);
                    }
                case 7:
                case '\b':
                    if (tb.inScope(name)) {
                        tb.generateImpliedEndTags(name);
                        if (!tb.currentElement().normalName().equals(name)) {
                            tb.error(this);
                        }
                        tb.popStackToClose(name);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case '\t':
                case '\n':
                case 11:
                case '\f':
                case '\r':
                case 14:
                    if (tb.inScope(Constants.Headings)) {
                        tb.generateImpliedEndTags(name);
                        if (!tb.currentElement().normalName().equals(name)) {
                            tb.error(this);
                        }
                        tb.popStackToClose(Constants.Headings);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 15:
                    tb.error(this);
                    tb.processStartTag("br");
                    return false;
                default:
                    if (StringUtil.inSorted(name, Constants.InBodyEndAdoptionFormatters)) {
                        return inBodyEndTagAdoption(t, tb);
                    }
                    if (StringUtil.inSorted(name, Constants.InBodyEndClosers)) {
                        if (tb.inScope(name)) {
                            tb.generateImpliedEndTags();
                            if (!tb.currentElement().normalName().equals(name)) {
                                tb.error(this);
                            }
                            tb.popStackToClose(name);
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else if (StringUtil.inSorted(name, Constants.InBodyStartApplets)) {
                        if (!tb.inScope("name")) {
                            if (tb.inScope(name)) {
                                tb.generateImpliedEndTags();
                                if (!tb.currentElement().normalName().equals(name)) {
                                    tb.error(this);
                                }
                                tb.popStackToClose(name);
                                tb.clearFormattingElementsToLastMarker();
                                break;
                            } else {
                                tb.error(this);
                                return false;
                            }
                        }
                    } else {
                        return anyOtherEndTag(t, tb);
                    }
                    break;
            }
            return true;
        }

        /* JADX WARN: Code restructure failed: missing block: B:15:0x0048, code lost:
            return true;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        boolean anyOtherEndTag(Token r7, HtmlTreeBuilder r8) {
            /*
                r6 = this;
                org.jsoup.parser.Token$EndTag r0 = r7.asEndTag()
                java.lang.String r0 = r0.normalName
                java.util.ArrayList r1 = r8.getStack()
                int r2 = r1.size()
                r3 = 1
                int r2 = r2 - r3
            L10:
                if (r2 < 0) goto L48
                java.lang.Object r4 = r1.get(r2)
                org.jsoup.nodes.Element r4 = (org.jsoup.nodes.Element) r4
                java.lang.String r5 = r4.normalName()
                boolean r5 = r5.equals(r0)
                if (r5 == 0) goto L3a
                r8.generateImpliedEndTags(r0)
                org.jsoup.nodes.Element r5 = r8.currentElement()
                java.lang.String r5 = r5.normalName()
                boolean r5 = r0.equals(r5)
                if (r5 != 0) goto L36
                r8.error(r6)
            L36:
                r8.popStackToClose(r0)
                goto L48
            L3a:
                boolean r5 = r8.isSpecial(r4)
                if (r5 == 0) goto L45
                r8.error(r6)
                r3 = 0
                return r3
            L45:
                int r2 = r2 + (-1)
                goto L10
            L48:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass7.anyOtherEndTag(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        private boolean inBodyEndTagAdoption(Token t, HtmlTreeBuilder tb) {
            HtmlTreeBuilderState htmlTreeBuilderState = this;
            Token.EndTag endTag = t.asEndTag();
            String name = endTag.normalName();
            ArrayList<Element> stack = tb.getStack();
            int i = 0;
            while (i < 8) {
                Element formatEl = tb.getActiveFormattingElement(name);
                if (formatEl == null) {
                    return anyOtherEndTag(t, tb);
                }
                if (!tb.onStack(formatEl)) {
                    tb.error(htmlTreeBuilderState);
                    tb.removeFromActiveFormattingElements(formatEl);
                    return true;
                } else if (!tb.inScope(formatEl.normalName())) {
                    tb.error(htmlTreeBuilderState);
                    return false;
                } else {
                    if (tb.currentElement() != formatEl) {
                        tb.error(htmlTreeBuilderState);
                    }
                    Element furthestBlock = null;
                    Element commonAncestor = null;
                    boolean seenFormattingElement = false;
                    int stackSize = stack.size();
                    int si = 0;
                    while (true) {
                        if (si >= stackSize || si >= 64) {
                            break;
                        }
                        Element el = stack.get(si);
                        if (el != formatEl) {
                            if (seenFormattingElement && tb.isSpecial(el)) {
                                furthestBlock = el;
                                break;
                            }
                        } else {
                            Element commonAncestor2 = stack.get(si - 1);
                            commonAncestor = commonAncestor2;
                            seenFormattingElement = true;
                        }
                        si++;
                    }
                    if (furthestBlock == null) {
                        tb.popStackToClose(formatEl.normalName());
                        tb.removeFromActiveFormattingElements(formatEl);
                        return true;
                    }
                    Element node = furthestBlock;
                    Element lastNode = furthestBlock;
                    int j = 0;
                    while (j < 3) {
                        if (tb.onStack(node)) {
                            node = tb.aboveOnStack(node);
                        }
                        if (!tb.isInActiveFormattingElements(node)) {
                            tb.removeFromStack(node);
                        } else if (node == formatEl) {
                            break;
                        } else {
                            Element replacement = new Element(Tag.valueOf(node.nodeName(), ParseSettings.preserveCase), tb.getBaseUri());
                            tb.replaceActiveFormattingElement(node, replacement);
                            tb.replaceOnStack(node, replacement);
                            node = replacement;
                            if (lastNode.parent() != null) {
                                lastNode.remove();
                            }
                            node.appendChild(lastNode);
                            lastNode = node;
                        }
                        j++;
                        lastNode = lastNode;
                    }
                    if (StringUtil.inSorted(commonAncestor.normalName(), Constants.InBodyEndTableFosters)) {
                        if (lastNode.parent() != null) {
                            lastNode.remove();
                        }
                        tb.insertInFosterParent(lastNode);
                    } else {
                        if (lastNode.parent() != null) {
                            lastNode.remove();
                        }
                        commonAncestor.appendChild(lastNode);
                    }
                    Element adopter = new Element(formatEl.tag(), tb.getBaseUri());
                    adopter.attributes().addAll(formatEl.attributes());
                    int i2 = 0;
                    Node[] childNodes = (Node[]) furthestBlock.childNodes().toArray(new Node[0]);
                    int length = childNodes.length;
                    while (i2 < length) {
                        Node childNode = childNodes[i2];
                        adopter.appendChild(childNode);
                        i2++;
                        endTag = endTag;
                    }
                    furthestBlock.appendChild(adopter);
                    tb.removeFromActiveFormattingElements(formatEl);
                    tb.removeFromStack(formatEl);
                    tb.insertOnStackAfter(furthestBlock, adopter);
                    i++;
                    htmlTreeBuilderState = this;
                }
            }
            return true;
        }
    },
    Text { // from class: org.jsoup.parser.HtmlTreeBuilderState.8
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isCharacter()) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isEOF()) {
                tb.error(this);
                tb.pop();
                tb.transition(tb.originalState());
                return tb.process(t);
            } else if (!t.isEndTag()) {
                return true;
            } else {
                tb.pop();
                tb.transition(tb.originalState());
                return true;
            }
        }
    },
    InTable { // from class: org.jsoup.parser.HtmlTreeBuilderState.9
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isCharacter()) {
                tb.newPendingTableCharacters();
                tb.markInsertionMode();
                tb.transition(InTableText);
                return tb.process(t);
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.normalName();
                if (name.equals("caption")) {
                    tb.clearStackToTableContext();
                    tb.insertMarkerToFormattingElements();
                    tb.insert(startTag);
                    tb.transition(InCaption);
                } else if (name.equals("colgroup")) {
                    tb.clearStackToTableContext();
                    tb.insert(startTag);
                    tb.transition(InColumnGroup);
                } else if (name.equals("col")) {
                    tb.processStartTag("colgroup");
                    return tb.process(t);
                } else if (StringUtil.inSorted(name, Constants.InTableToBody)) {
                    tb.clearStackToTableContext();
                    tb.insert(startTag);
                    tb.transition(InTableBody);
                } else if (StringUtil.inSorted(name, Constants.InTableAddBody)) {
                    tb.processStartTag("tbody");
                    return tb.process(t);
                } else if (name.equals("table")) {
                    tb.error(this);
                    boolean processed = tb.processEndTag("table");
                    if (processed) {
                        return tb.process(t);
                    }
                } else if (StringUtil.inSorted(name, Constants.InTableToHead)) {
                    return tb.process(t, InHead);
                } else {
                    if (name.equals("input")) {
                        if (!startTag.attributes.get("type").equalsIgnoreCase("hidden")) {
                            return anythingElse(t, tb);
                        }
                        tb.insertEmpty(startTag);
                    } else if (!name.equals("form")) {
                        return anythingElse(t, tb);
                    } else {
                        tb.error(this);
                        if (tb.getFormElement() != null) {
                            return false;
                        }
                        tb.insertForm(startTag, false);
                    }
                }
                return true;
            } else if (t.isEndTag()) {
                Token.EndTag endTag = t.asEndTag();
                String name2 = endTag.normalName();
                if (name2.equals("table")) {
                    if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    }
                    tb.popStackToClose("table");
                    tb.resetInsertionMode();
                    return true;
                } else if (!StringUtil.inSorted(name2, Constants.InTableEndErr)) {
                    return anythingElse(t, tb);
                } else {
                    tb.error(this);
                    return false;
                }
            } else if (!t.isEOF()) {
                return anythingElse(t, tb);
            } else {
                if (tb.currentElement().normalName().equals("html")) {
                    tb.error(this);
                }
                return true;
            }
        }

        boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            tb.error(this);
            if (StringUtil.inSorted(tb.currentElement().normalName(), Constants.InTableFoster)) {
                tb.setFosterInserts(true);
                boolean processed = tb.process(t, InBody);
                tb.setFosterInserts(false);
                return processed;
            }
            boolean processed2 = tb.process(t, InBody);
            return processed2;
        }
    },
    InTableText { // from class: org.jsoup.parser.HtmlTreeBuilderState.10
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.type == Token.TokenType.Character) {
                Token.Character c = t.asCharacter();
                if (c.getData().equals(HtmlTreeBuilderState.nullString)) {
                    tb.error(this);
                    return false;
                }
                tb.getPendingTableCharacters().add(c.getData());
                return true;
            }
            if (tb.getPendingTableCharacters().size() > 0) {
                for (String character : tb.getPendingTableCharacters()) {
                    if (!HtmlTreeBuilderState.isWhitespace(character)) {
                        tb.error(this);
                        if (StringUtil.inSorted(tb.currentElement().normalName(), Constants.InTableFoster)) {
                            tb.setFosterInserts(true);
                            tb.process(new Token.Character().data(character), InBody);
                            tb.setFosterInserts(false);
                        } else {
                            tb.process(new Token.Character().data(character), InBody);
                        }
                    } else {
                        tb.insert(new Token.Character().data(character));
                    }
                }
                tb.newPendingTableCharacters();
            }
            tb.transition(tb.originalState());
            return tb.process(t);
        }
    },
    InCaption { // from class: org.jsoup.parser.HtmlTreeBuilderState.11
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isEndTag() && t.asEndTag().normalName().equals("caption")) {
                Token.EndTag endTag = t.asEndTag();
                String name = endTag.normalName();
                if (!tb.inTableScope(name)) {
                    tb.error(this);
                    return false;
                }
                tb.generateImpliedEndTags();
                if (!tb.currentElement().normalName().equals("caption")) {
                    tb.error(this);
                }
                tb.popStackToClose("caption");
                tb.clearFormattingElementsToLastMarker();
                tb.transition(InTable);
                return true;
            } else if ((t.isStartTag() && StringUtil.inSorted(t.asStartTag().normalName(), Constants.InCellCol)) || (t.isEndTag() && t.asEndTag().normalName().equals("table"))) {
                tb.error(this);
                boolean processed = tb.processEndTag("caption");
                if (processed) {
                    return tb.process(t);
                }
                return true;
            } else if (!t.isEndTag() || !StringUtil.inSorted(t.asEndTag().normalName(), Constants.InCaptionIgnore)) {
                return tb.process(t, InBody);
            } else {
                tb.error(this);
                return false;
            }
        }
    },
    InColumnGroup { // from class: org.jsoup.parser.HtmlTreeBuilderState.12
        /* JADX WARN: Code restructure failed: missing block: B:37:0x008d, code lost:
            if (r2.equals("html") == false) goto L41;
         */
        /* JADX WARN: Removed duplicated region for block: B:43:0x009d  */
        /* JADX WARN: Removed duplicated region for block: B:47:0x00a9  */
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct code enable 'Show inconsistent code' option in preferences
        */
        boolean process(Token r9, HtmlTreeBuilder r10) {
            /*
                r8 = this;
                boolean r0 = org.jsoup.parser.HtmlTreeBuilderState.access$100(r9)
                r1 = 1
                if (r0 == 0) goto Lf
                org.jsoup.parser.Token$Character r0 = r9.asCharacter()
                r10.insert(r0)
                return r1
            Lf:
                int[] r0 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType
                org.jsoup.parser.Token$TokenType r2 = r9.type
                int r2 = r2.ordinal()
                r0 = r0[r2]
                if (r0 == r1) goto Lb4
                r2 = 2
                if (r0 == r2) goto Lb0
                r2 = 3
                r3 = 0
                java.lang.String r4 = "html"
                if (r0 == r2) goto L71
                r2 = 4
                if (r0 == r2) goto L43
                r2 = 6
                if (r0 == r2) goto L2f
                boolean r0 = r8.anythingElse(r9, r10)
                return r0
            L2f:
                org.jsoup.nodes.Element r0 = r10.currentElement()
                java.lang.String r0 = r0.normalName()
                boolean r0 = r0.equals(r4)
                if (r0 == 0) goto L3e
                return r1
            L3e:
                boolean r0 = r8.anythingElse(r9, r10)
                return r0
            L43:
                org.jsoup.parser.Token$EndTag r0 = r9.asEndTag()
                java.lang.String r2 = r0.normalName
                java.lang.String r5 = "colgroup"
                boolean r2 = r2.equals(r5)
                if (r2 == 0) goto L6c
                org.jsoup.nodes.Element r2 = r10.currentElement()
                java.lang.String r2 = r2.normalName()
                boolean r2 = r2.equals(r4)
                if (r2 == 0) goto L63
                r10.error(r8)
                return r3
            L63:
                r10.pop()
                org.jsoup.parser.HtmlTreeBuilderState r2 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.InTable
                r10.transition(r2)
                goto Lbc
            L6c:
                boolean r1 = r8.anythingElse(r9, r10)
                return r1
            L71:
                org.jsoup.parser.Token$StartTag r0 = r9.asStartTag()
                java.lang.String r2 = r0.normalName()
                r5 = -1
                int r6 = r2.hashCode()
                r7 = 98688(0x18180, float:1.38291E-40)
                if (r6 == r7) goto L90
                r7 = 3213227(0x3107ab, float:4.50269E-39)
                if (r6 == r7) goto L89
            L88:
                goto L9a
            L89:
                boolean r2 = r2.equals(r4)
                if (r2 == 0) goto L88
                goto L9b
            L90:
                java.lang.String r3 = "col"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L88
                r3 = 1
                goto L9b
            L9a:
                r3 = -1
            L9b:
                if (r3 == 0) goto La9
                if (r3 == r1) goto La4
                boolean r1 = r8.anythingElse(r9, r10)
                return r1
            La4:
                r10.insertEmpty(r0)
                goto Lbc
            La9:
                org.jsoup.parser.HtmlTreeBuilderState r1 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.InBody
                boolean r1 = r10.process(r9, r1)
                return r1
            Lb0:
                r10.error(r8)
                goto Lbc
            Lb4:
                org.jsoup.parser.Token$Comment r0 = r9.asComment()
                r10.insert(r0)
            Lbc:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.process(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            boolean processed = tb.processEndTag("colgroup");
            if (processed) {
                return tb.process(t);
            }
            return true;
        }
    },
    InTableBody { // from class: org.jsoup.parser.HtmlTreeBuilderState.13
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            int i = AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()];
            if (i == 3) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.normalName();
                if (name.equals("template")) {
                    tb.insert(startTag);
                    return true;
                } else if (name.equals("tr")) {
                    tb.clearStackToTableBodyContext();
                    tb.insert(startTag);
                    tb.transition(InRow);
                    return true;
                } else if (StringUtil.inSorted(name, Constants.InCellNames)) {
                    tb.error(this);
                    tb.processStartTag("tr");
                    return tb.process(startTag);
                } else if (StringUtil.inSorted(name, Constants.InTableBodyExit)) {
                    return exitTableBody(t, tb);
                } else {
                    return anythingElse(t, tb);
                }
            } else if (i != 4) {
                return anythingElse(t, tb);
            } else {
                Token.EndTag endTag = t.asEndTag();
                String name2 = endTag.normalName();
                if (StringUtil.inSorted(name2, Constants.InTableEndIgnore)) {
                    if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    }
                    tb.clearStackToTableBodyContext();
                    tb.pop();
                    tb.transition(InTable);
                    return true;
                } else if (name2.equals("table")) {
                    return exitTableBody(t, tb);
                } else {
                    if (!StringUtil.inSorted(name2, Constants.InTableBodyEndIgnore)) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                }
            }
        }

        private boolean exitTableBody(Token t, HtmlTreeBuilder tb) {
            if (tb.inTableScope("tbody") || tb.inTableScope("thead") || tb.inScope("tfoot")) {
                tb.clearStackToTableBodyContext();
                tb.processEndTag(tb.currentElement().normalName());
                return tb.process(t);
            }
            tb.error(this);
            return false;
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            return tb.process(t, InTable);
        }
    },
    InRow { // from class: org.jsoup.parser.HtmlTreeBuilderState.14
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.normalName();
                if (name.equals("template")) {
                    tb.insert(startTag);
                    return true;
                } else if (StringUtil.inSorted(name, Constants.InCellNames)) {
                    tb.clearStackToTableRowContext();
                    tb.insert(startTag);
                    tb.transition(InCell);
                    tb.insertMarkerToFormattingElements();
                    return true;
                } else if (StringUtil.inSorted(name, Constants.InRowMissing)) {
                    return handleMissingTr(t, tb);
                } else {
                    return anythingElse(t, tb);
                }
            } else if (!t.isEndTag()) {
                return anythingElse(t, tb);
            } else {
                Token.EndTag endTag = t.asEndTag();
                String name2 = endTag.normalName();
                if (name2.equals("tr")) {
                    if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    }
                    tb.clearStackToTableRowContext();
                    tb.pop();
                    tb.transition(InTableBody);
                    return true;
                } else if (name2.equals("table")) {
                    return handleMissingTr(t, tb);
                } else {
                    if (StringUtil.inSorted(name2, Constants.InTableToBody)) {
                        if (!tb.inTableScope(name2)) {
                            tb.error(this);
                            return false;
                        }
                        tb.processEndTag("tr");
                        return tb.process(t);
                    } else if (!StringUtil.inSorted(name2, Constants.InRowIgnore)) {
                        return anythingElse(t, tb);
                    } else {
                        tb.error(this);
                        return false;
                    }
                }
            }
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            return tb.process(t, InTable);
        }

        private boolean handleMissingTr(Token t, TreeBuilder tb) {
            boolean processed = tb.processEndTag("tr");
            if (processed) {
                return tb.process(t);
            }
            return false;
        }
    },
    InCell { // from class: org.jsoup.parser.HtmlTreeBuilderState.15
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isEndTag()) {
                Token.EndTag endTag = t.asEndTag();
                String name = endTag.normalName();
                if (StringUtil.inSorted(name, Constants.InCellNames)) {
                    if (!tb.inTableScope(name)) {
                        tb.error(this);
                        tb.transition(InRow);
                        return false;
                    }
                    tb.generateImpliedEndTags();
                    if (!tb.currentElement().normalName().equals(name)) {
                        tb.error(this);
                    }
                    tb.popStackToClose(name);
                    tb.clearFormattingElementsToLastMarker();
                    tb.transition(InRow);
                    return true;
                } else if (StringUtil.inSorted(name, Constants.InCellBody)) {
                    tb.error(this);
                    return false;
                } else if (!StringUtil.inSorted(name, Constants.InCellTable)) {
                    return anythingElse(t, tb);
                } else {
                    if (!tb.inTableScope(name)) {
                        tb.error(this);
                        return false;
                    }
                    closeCell(tb);
                    return tb.process(t);
                }
            } else if (!t.isStartTag() || !StringUtil.inSorted(t.asStartTag().normalName(), Constants.InCellCol)) {
                return anythingElse(t, tb);
            } else {
                if (tb.inTableScope("td") || tb.inTableScope("th")) {
                    closeCell(tb);
                    return tb.process(t);
                }
                tb.error(this);
                return false;
            }
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            return tb.process(t, InBody);
        }

        private void closeCell(HtmlTreeBuilder tb) {
            if (tb.inTableScope("td")) {
                tb.processEndTag("td");
            } else {
                tb.processEndTag("th");
            }
        }
    },
    InSelect { // from class: org.jsoup.parser.HtmlTreeBuilderState.16
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case 1:
                    tb.insert(t.asComment());
                    break;
                case 2:
                    tb.error(this);
                    return false;
                case 3:
                    Token.StartTag start = t.asStartTag();
                    String name = start.normalName();
                    if (name.equals("html")) {
                        return tb.process(start, InBody);
                    }
                    if (name.equals("option")) {
                        if (tb.currentElement().normalName().equals("option")) {
                            tb.processEndTag("option");
                        }
                        tb.insert(start);
                        break;
                    } else if (name.equals("optgroup")) {
                        if (tb.currentElement().normalName().equals("option")) {
                            tb.processEndTag("option");
                        }
                        if (tb.currentElement().normalName().equals("optgroup")) {
                            tb.processEndTag("optgroup");
                        }
                        tb.insert(start);
                        break;
                    } else if (name.equals("select")) {
                        tb.error(this);
                        return tb.processEndTag("select");
                    } else if (StringUtil.inSorted(name, Constants.InSelectEnd)) {
                        tb.error(this);
                        if (!tb.inSelectScope("select")) {
                            return false;
                        }
                        tb.processEndTag("select");
                        return tb.process(start);
                    } else if (name.equals("script")) {
                        return tb.process(t, InHead);
                    } else {
                        return anythingElse(t, tb);
                    }
                case 4:
                    Token.EndTag end = t.asEndTag();
                    String name2 = end.normalName();
                    char c = 65535;
                    int hashCode = name2.hashCode();
                    if (hashCode != -1010136971) {
                        if (hashCode != -906021636) {
                            if (hashCode == -80773204 && name2.equals("optgroup")) {
                                c = 0;
                            }
                        } else if (name2.equals("select")) {
                            c = 2;
                        }
                    } else if (name2.equals("option")) {
                        c = 1;
                    }
                    if (c != 0) {
                        if (c == 1) {
                            if (!tb.currentElement().normalName().equals("option")) {
                                tb.error(this);
                                break;
                            } else {
                                tb.pop();
                                break;
                            }
                        } else if (c == 2) {
                            if (tb.inSelectScope(name2)) {
                                tb.popStackToClose(name2);
                                tb.resetInsertionMode();
                                break;
                            } else {
                                tb.error(this);
                                return false;
                            }
                        } else {
                            return anythingElse(t, tb);
                        }
                    } else {
                        if (tb.currentElement().normalName().equals("option") && tb.aboveOnStack(tb.currentElement()) != null && tb.aboveOnStack(tb.currentElement()).normalName().equals("optgroup")) {
                            tb.processEndTag("option");
                        }
                        if (!tb.currentElement().normalName().equals("optgroup")) {
                            tb.error(this);
                            break;
                        } else {
                            tb.pop();
                            break;
                        }
                    }
                    break;
                case 5:
                    Token.Character c2 = t.asCharacter();
                    if (!c2.getData().equals(HtmlTreeBuilderState.nullString)) {
                        tb.insert(c2);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 6:
                    if (!tb.currentElement().normalName().equals("html")) {
                        tb.error(this);
                        break;
                    }
                    break;
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, HtmlTreeBuilder tb) {
            tb.error(this);
            return false;
        }
    },
    InSelectInTable { // from class: org.jsoup.parser.HtmlTreeBuilderState.17
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isStartTag() && StringUtil.inSorted(t.asStartTag().normalName(), Constants.InSelecTableEnd)) {
                tb.error(this);
                tb.processEndTag("select");
                return tb.process(t);
            } else if (!t.isEndTag() || !StringUtil.inSorted(t.asEndTag().normalName(), Constants.InSelecTableEnd)) {
                return tb.process(t, InSelect);
            } else {
                tb.error(this);
                if (!tb.inTableScope(t.asEndTag().normalName())) {
                    return false;
                }
                tb.processEndTag("select");
                return tb.process(t);
            }
        }
    },
    AfterBody { // from class: org.jsoup.parser.HtmlTreeBuilderState.18
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().normalName().equals("html")) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEndTag() || !t.asEndTag().normalName().equals("html")) {
                    if (t.isEOF()) {
                        return true;
                    }
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                } else if (tb.isFragmentParsing()) {
                    tb.error(this);
                    return false;
                } else {
                    tb.transition(AfterAfterBody);
                    return true;
                }
            }
        }
    },
    InFrameset { // from class: org.jsoup.parser.HtmlTreeBuilderState.19
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag start = t.asStartTag();
                String normalName = start.normalName();
                char c = 65535;
                switch (normalName.hashCode()) {
                    case -1644953643:
                        if (normalName.equals("frameset")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 3213227:
                        if (normalName.equals("html")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 97692013:
                        if (normalName.equals("frame")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1192721831:
                        if (normalName.equals("noframes")) {
                            c = 3;
                            break;
                        }
                        break;
                }
                if (c == 0) {
                    return tb.process(start, InBody);
                }
                if (c == 1) {
                    tb.insert(start);
                } else if (c == 2) {
                    tb.insertEmpty(start);
                } else if (c == 3) {
                    return tb.process(start, InHead);
                } else {
                    tb.error(this);
                    return false;
                }
            } else if (!t.isEndTag() || !t.asEndTag().normalName().equals("frameset")) {
                if (!t.isEOF()) {
                    tb.error(this);
                    return false;
                } else if (!tb.currentElement().normalName().equals("html")) {
                    tb.error(this);
                    return true;
                }
            } else if (tb.currentElement().normalName().equals("html")) {
                tb.error(this);
                return false;
            } else {
                tb.pop();
                if (!tb.isFragmentParsing() && !tb.currentElement().normalName().equals("frameset")) {
                    tb.transition(AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset { // from class: org.jsoup.parser.HtmlTreeBuilderState.20
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (HtmlTreeBuilderState.isWhitespace(t)) {
                tb.insert(t.asCharacter());
                return true;
            } else if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().normalName().equals("html")) {
                return tb.process(t, InBody);
            } else {
                if (t.isEndTag() && t.asEndTag().normalName().equals("html")) {
                    tb.transition(AfterAfterFrameset);
                    return true;
                } else if (t.isStartTag() && t.asStartTag().normalName().equals("noframes")) {
                    return tb.process(t, InHead);
                } else {
                    if (t.isEOF()) {
                        return true;
                    }
                    tb.error(this);
                    return false;
                }
            }
        }
    },
    AfterAfterBody { // from class: org.jsoup.parser.HtmlTreeBuilderState.21
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype() || (t.isStartTag() && t.asStartTag().normalName().equals("html"))) {
                return tb.process(t, InBody);
            } else {
                if (HtmlTreeBuilderState.isWhitespace(t)) {
                    Element html = tb.popStackToClose("html");
                    tb.insert(t.asCharacter());
                    tb.stack.add(html);
                    tb.stack.add(html.selectFirst("body"));
                    return true;
                } else if (t.isEOF()) {
                    return true;
                } else {
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                }
            }
        }
    },
    AfterAfterFrameset { // from class: org.jsoup.parser.HtmlTreeBuilderState.22
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype() || HtmlTreeBuilderState.isWhitespace(t) || (t.isStartTag() && t.asStartTag().normalName().equals("html"))) {
                return tb.process(t, InBody);
            } else {
                if (t.isEOF()) {
                    return true;
                }
                if (t.isStartTag() && t.asStartTag().normalName().equals("noframes")) {
                    return tb.process(t, InHead);
                }
                tb.error(this);
                return false;
            }
        }
    },
    ForeignContent { // from class: org.jsoup.parser.HtmlTreeBuilderState.23
        @Override // org.jsoup.parser.HtmlTreeBuilderState
        boolean process(Token t, HtmlTreeBuilder tb) {
            return true;
        }
    };
    
    private static final String nullString = String.valueOf((char) 0);

    /* JADX INFO: Access modifiers changed from: package-private */
    public abstract boolean process(Token token, HtmlTreeBuilder htmlTreeBuilder);

    /* renamed from: org.jsoup.parser.HtmlTreeBuilderState$24  reason: invalid class name */
    /* loaded from: classes2.dex */
    static /* synthetic */ class AnonymousClass24 {
        static final /* synthetic */ int[] $SwitchMap$org$jsoup$parser$Token$TokenType;

        static {
            int[] iArr = new int[Token.TokenType.values().length];
            $SwitchMap$org$jsoup$parser$Token$TokenType = iArr;
            try {
                iArr[Token.TokenType.Comment.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Doctype.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.StartTag.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EndTag.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Character.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EOF.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static boolean isWhitespace(Token t) {
        if (!t.isCharacter()) {
            return false;
        }
        String data = t.asCharacter().getData();
        return StringUtil.isBlank(data);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static boolean isWhitespace(String data) {
        return StringUtil.isBlank(data);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void handleRcData(Token.StartTag startTag, HtmlTreeBuilder tb) {
        tb.tokeniser.transition(TokeniserState.Rcdata);
        tb.markInsertionMode();
        tb.transition(Text);
        tb.insert(startTag);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static void handleRawtext(Token.StartTag startTag, HtmlTreeBuilder tb) {
        tb.tokeniser.transition(TokeniserState.Rawtext);
        tb.markInsertionMode();
        tb.transition(Text);
        tb.insert(startTag);
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes2.dex */
    public static final class Constants {
        static final String[] InHeadEmpty = {"base", "basefont", "bgsound", "command", "link"};
        static final String[] InHeadRaw = {"noframes", "style"};
        static final String[] InHeadEnd = {"body", "br", "html"};
        static final String[] AfterHeadBody = {"body", "html"};
        static final String[] BeforeHtmlToHead = {"body", "br", "head", "html"};
        static final String[] InHeadNoScriptHead = {"basefont", "bgsound", "link", "meta", "noframes", "style"};
        static final String[] InBodyStartToHead = {"base", "basefont", "bgsound", "command", "link", "meta", "noframes", "script", "style", "title"};
        static final String[] InBodyStartPClosers = {"address", "article", "aside", "blockquote", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "menu", "nav", "ol", "p", "section", "summary", "ul"};
        static final String[] Headings = {"h1", "h2", "h3", "h4", "h5", "h6"};
        static final String[] InBodyStartLiBreakers = {"address", "div", "p"};
        static final String[] DdDt = {"dd", "dt"};
        static final String[] Formatters = {"b", "big", "code", "em", "font", "i", "s", "small", "strike", "strong", "tt", "u"};
        static final String[] InBodyStartApplets = {"applet", "marquee", "object"};
        static final String[] InBodyStartEmptyFormatters = {"area", "br", "embed", "img", "keygen", "wbr"};
        static final String[] InBodyStartMedia = {"param", "source", "track"};
        static final String[] InBodyStartInputAttribs = {"action", "name", "prompt"};
        static final String[] InBodyStartDrop = {"caption", "col", "colgroup", "frame", "head", "tbody", "td", "tfoot", "th", "thead", "tr"};
        static final String[] InBodyEndClosers = {"address", "article", "aside", "blockquote", "button", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "summary", "ul"};
        static final String[] InBodyEndAdoptionFormatters = {"a", "b", "big", "code", "em", "font", "i", "nobr", "s", "small", "strike", "strong", "tt", "u"};
        static final String[] InBodyEndTableFosters = {"table", "tbody", "tfoot", "thead", "tr"};
        static final String[] InTableToBody = {"tbody", "tfoot", "thead"};
        static final String[] InTableAddBody = {"td", "th", "tr"};
        static final String[] InTableToHead = {"script", "style"};
        static final String[] InCellNames = {"td", "th"};
        static final String[] InCellBody = {"body", "caption", "col", "colgroup", "html"};
        static final String[] InCellTable = {"table", "tbody", "tfoot", "thead", "tr"};
        static final String[] InCellCol = {"caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr"};
        static final String[] InTableEndErr = {"body", "caption", "col", "colgroup", "html", "tbody", "td", "tfoot", "th", "thead", "tr"};
        static final String[] InTableFoster = {"table", "tbody", "tfoot", "thead", "tr"};
        static final String[] InTableBodyExit = {"caption", "col", "colgroup", "tbody", "tfoot", "thead"};
        static final String[] InTableBodyEndIgnore = {"body", "caption", "col", "colgroup", "html", "td", "th", "tr"};
        static final String[] InRowMissing = {"caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr"};
        static final String[] InRowIgnore = {"body", "caption", "col", "colgroup", "html", "td", "th"};
        static final String[] InSelectEnd = {"input", "keygen", "textarea"};
        static final String[] InSelecTableEnd = {"caption", "table", "tbody", "td", "tfoot", "th", "thead", "tr"};
        static final String[] InTableEndIgnore = {"tbody", "tfoot", "thead"};
        static final String[] InHeadNoscriptIgnore = {"head", "noscript"};
        static final String[] InCaptionIgnore = {"body", "col", "colgroup", "html", "tbody", "td", "tfoot", "th", "thead", "tr"};

        Constants() {
        }
    }
}
