package org.drinkless.td.libcore.telegram;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* loaded from: classes2.dex */
public class TdApi {

    /* loaded from: classes2.dex */
    public static abstract class AuthenticationCodeType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class AuthorizationState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class BackgroundFill extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class BackgroundType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CallDiscardReason extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CallProblem extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CallState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CallbackQueryPayload extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CanTransferOwnershipResult extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatAction extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatActionBar extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatEventAction extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatList extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatMemberStatus extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatMembersFilter extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatReportReason extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ChatType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class CheckChatUsernameResult extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ConnectionState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class DeviceToken extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class FileType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class Function extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public native String toString();
    }

    /* loaded from: classes2.dex */
    public static abstract class InlineKeyboardButtonType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InlineQueryResult extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputBackground extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputCredentials extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputFile extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputInlineQueryResult extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputMessageContent extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputPassportElement extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class InputPassportElementErrorSource extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class JsonValue extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class KeyboardButtonType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class LanguagePackStringValue extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class LogStream extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class LoginUrlInfo extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class MaskPoint extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class MessageContent extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class MessageForwardOrigin extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class MessageSchedulingState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class MessageSendingState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class NetworkStatisticsEntry extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class NetworkType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class NotificationGroupType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class NotificationSettingsScope extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class NotificationType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class Object {
        public abstract int getConstructor();

        public native String toString();
    }

    /* loaded from: classes2.dex */
    public static abstract class OptionValue extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PageBlock extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PageBlockHorizontalAlignment extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PageBlockVerticalAlignment extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PassportElement extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PassportElementErrorSource extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PassportElementType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PollType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ProxyType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PublicChatType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class PushMessageContent extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class ReplyMarkup extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class RichText extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class SearchMessagesFilter extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class SecretChatState extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class SupergroupMembersFilter extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class TMeUrlType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class TextEntityType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class TextParseMode extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class TopChatCategory extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class Update extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class UserPrivacySetting extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class UserPrivacySettingRule extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class UserStatus extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static abstract class UserType extends Object {

        @Retention(RetentionPolicy.SOURCE)
        /* loaded from: classes.dex */
        public @interface Constructors {
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public abstract int getConstructor();
    }

    /* loaded from: classes2.dex */
    public static class AccountTtl extends Object {
        public static final int CONSTRUCTOR = 1324495492;
        public int days;

        public AccountTtl() {
        }

        public AccountTtl(int days) {
            this.days = days;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Address extends Object {
        public static final int CONSTRUCTOR = -2043654342;
        public String city;
        public String countryCode;
        public String postalCode;
        public String state;
        public String streetLine1;
        public String streetLine2;

        public Address() {
        }

        public Address(String countryCode, String state, String city, String streetLine1, String streetLine2, String postalCode) {
            this.countryCode = countryCode;
            this.state = state;
            this.city = city;
            this.streetLine1 = streetLine1;
            this.streetLine2 = streetLine2;
            this.postalCode = postalCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Animation extends Object {
        public static final int CONSTRUCTOR = -1629245379;
        public File animation;
        public int duration;
        public String fileName;
        public int height;
        public String mimeType;
        public Minithumbnail minithumbnail;
        public PhotoSize thumbnail;
        public int width;

        public Animation() {
        }

        public Animation(int duration, int width, int height, String fileName, String mimeType, Minithumbnail minithumbnail, PhotoSize thumbnail, File animation) {
            this.duration = duration;
            this.width = width;
            this.height = height;
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.minithumbnail = minithumbnail;
            this.thumbnail = thumbnail;
            this.animation = animation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Animations extends Object {
        public static final int CONSTRUCTOR = 344216945;
        public Animation[] animations;

        public Animations() {
        }

        public Animations(Animation[] animations) {
            this.animations = animations;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Audio extends Object {
        public static final int CONSTRUCTOR = 1475294302;
        public Minithumbnail albumCoverMinithumbnail;
        public PhotoSize albumCoverThumbnail;
        public File audio;
        public int duration;
        public String fileName;
        public String mimeType;
        public String performer;
        public String title;

        public Audio() {
        }

        public Audio(int duration, String title, String performer, String fileName, String mimeType, Minithumbnail albumCoverMinithumbnail, PhotoSize albumCoverThumbnail, File audio) {
            this.duration = duration;
            this.title = title;
            this.performer = performer;
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.albumCoverMinithumbnail = albumCoverMinithumbnail;
            this.albumCoverThumbnail = albumCoverThumbnail;
            this.audio = audio;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthenticationCodeInfo extends Object {
        public static final int CONSTRUCTOR = -860345416;
        public AuthenticationCodeType nextType;
        public String phoneNumber;
        public int timeout;
        public AuthenticationCodeType type;

        public AuthenticationCodeInfo() {
        }

        public AuthenticationCodeInfo(String phoneNumber, AuthenticationCodeType type, AuthenticationCodeType nextType, int timeout) {
            this.phoneNumber = phoneNumber;
            this.type = type;
            this.nextType = nextType;
            this.timeout = timeout;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthenticationCodeTypeTelegramMessage extends AuthenticationCodeType {
        public static final int CONSTRUCTOR = 2079628074;
        public int length;

        public AuthenticationCodeTypeTelegramMessage() {
        }

        public AuthenticationCodeTypeTelegramMessage(int length) {
            this.length = length;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthenticationCodeType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthenticationCodeTypeSms extends AuthenticationCodeType {
        public static final int CONSTRUCTOR = 962650760;
        public int length;

        public AuthenticationCodeTypeSms() {
        }

        public AuthenticationCodeTypeSms(int length) {
            this.length = length;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthenticationCodeType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthenticationCodeTypeCall extends AuthenticationCodeType {
        public static final int CONSTRUCTOR = 1636265063;
        public int length;

        public AuthenticationCodeTypeCall() {
        }

        public AuthenticationCodeTypeCall(int length) {
            this.length = length;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthenticationCodeType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthenticationCodeTypeFlashCall extends AuthenticationCodeType {
        public static final int CONSTRUCTOR = 1395882402;
        public String pattern;

        public AuthenticationCodeTypeFlashCall() {
        }

        public AuthenticationCodeTypeFlashCall(String pattern) {
            this.pattern = pattern;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthenticationCodeType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitTdlibParameters extends AuthorizationState {
        public static final int CONSTRUCTOR = 904720988;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitEncryptionKey extends AuthorizationState {
        public static final int CONSTRUCTOR = 612103496;
        public boolean isEncrypted;

        public AuthorizationStateWaitEncryptionKey() {
        }

        public AuthorizationStateWaitEncryptionKey(boolean isEncrypted) {
            this.isEncrypted = isEncrypted;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitPhoneNumber extends AuthorizationState {
        public static final int CONSTRUCTOR = 306402531;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitCode extends AuthorizationState {
        public static final int CONSTRUCTOR = 52643073;
        public AuthenticationCodeInfo codeInfo;

        public AuthorizationStateWaitCode() {
        }

        public AuthorizationStateWaitCode(AuthenticationCodeInfo codeInfo) {
            this.codeInfo = codeInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitOtherDeviceConfirmation extends AuthorizationState {
        public static final int CONSTRUCTOR = 860166378;
        public String link;

        public AuthorizationStateWaitOtherDeviceConfirmation() {
        }

        public AuthorizationStateWaitOtherDeviceConfirmation(String link) {
            this.link = link;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitRegistration extends AuthorizationState {
        public static final int CONSTRUCTOR = 550350511;
        public TermsOfService termsOfService;

        public AuthorizationStateWaitRegistration() {
        }

        public AuthorizationStateWaitRegistration(TermsOfService termsOfService) {
            this.termsOfService = termsOfService;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateWaitPassword extends AuthorizationState {
        public static final int CONSTRUCTOR = 187548796;
        public boolean hasRecoveryEmailAddress;
        public String passwordHint;
        public String recoveryEmailAddressPattern;

        public AuthorizationStateWaitPassword() {
        }

        public AuthorizationStateWaitPassword(String passwordHint, boolean hasRecoveryEmailAddress, String recoveryEmailAddressPattern) {
            this.passwordHint = passwordHint;
            this.hasRecoveryEmailAddress = hasRecoveryEmailAddress;
            this.recoveryEmailAddressPattern = recoveryEmailAddressPattern;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateReady extends AuthorizationState {
        public static final int CONSTRUCTOR = -1834871737;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateLoggingOut extends AuthorizationState {
        public static final int CONSTRUCTOR = 154449270;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateClosing extends AuthorizationState {
        public static final int CONSTRUCTOR = 445855311;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AuthorizationStateClosed extends AuthorizationState {
        public static final int CONSTRUCTOR = 1526047584;

        @Override // org.drinkless.td.libcore.telegram.TdApi.AuthorizationState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AutoDownloadSettings extends Object {
        public static final int CONSTRUCTOR = -2144418333;
        public boolean isAutoDownloadEnabled;
        public int maxOtherFileSize;
        public int maxPhotoFileSize;
        public int maxVideoFileSize;
        public boolean preloadLargeVideos;
        public boolean preloadNextAudio;
        public boolean useLessDataForCalls;
        public int videoUploadBitrate;

        public AutoDownloadSettings() {
        }

        public AutoDownloadSettings(boolean isAutoDownloadEnabled, int maxPhotoFileSize, int maxVideoFileSize, int maxOtherFileSize, int videoUploadBitrate, boolean preloadLargeVideos, boolean preloadNextAudio, boolean useLessDataForCalls) {
            this.isAutoDownloadEnabled = isAutoDownloadEnabled;
            this.maxPhotoFileSize = maxPhotoFileSize;
            this.maxVideoFileSize = maxVideoFileSize;
            this.maxOtherFileSize = maxOtherFileSize;
            this.videoUploadBitrate = videoUploadBitrate;
            this.preloadLargeVideos = preloadLargeVideos;
            this.preloadNextAudio = preloadNextAudio;
            this.useLessDataForCalls = useLessDataForCalls;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AutoDownloadSettingsPresets extends Object {
        public static final int CONSTRUCTOR = -782099166;
        public AutoDownloadSettings high;
        public AutoDownloadSettings low;
        public AutoDownloadSettings medium;

        public AutoDownloadSettingsPresets() {
        }

        public AutoDownloadSettingsPresets(AutoDownloadSettings low, AutoDownloadSettings medium, AutoDownloadSettings high) {
            this.low = low;
            this.medium = medium;
            this.high = high;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Background extends Object {
        public static final int CONSTRUCTOR = -429971172;
        public Document document;
        public long id;
        public boolean isDark;
        public boolean isDefault;
        public String name;
        public BackgroundType type;

        public Background() {
        }

        public Background(long id, boolean isDefault, boolean isDark, String name, Document document, BackgroundType type) {
            this.id = id;
            this.isDefault = isDefault;
            this.isDark = isDark;
            this.name = name;
            this.document = document;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BackgroundFillSolid extends BackgroundFill {
        public static final int CONSTRUCTOR = 1010678813;
        public int color;

        public BackgroundFillSolid() {
        }

        public BackgroundFillSolid(int color) {
            this.color = color;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.BackgroundFill, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BackgroundFillGradient extends BackgroundFill {
        public static final int CONSTRUCTOR = -1839206017;
        public int bottomColor;
        public int rotationAngle;
        public int topColor;

        public BackgroundFillGradient() {
        }

        public BackgroundFillGradient(int topColor, int bottomColor, int rotationAngle) {
            this.topColor = topColor;
            this.bottomColor = bottomColor;
            this.rotationAngle = rotationAngle;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.BackgroundFill, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BackgroundTypeWallpaper extends BackgroundType {
        public static final int CONSTRUCTOR = 1972128891;
        public boolean isBlurred;
        public boolean isMoving;

        public BackgroundTypeWallpaper() {
        }

        public BackgroundTypeWallpaper(boolean isBlurred, boolean isMoving) {
            this.isBlurred = isBlurred;
            this.isMoving = isMoving;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.BackgroundType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BackgroundTypePattern extends BackgroundType {
        public static final int CONSTRUCTOR = 649993914;
        public BackgroundFill fill;
        public int intensity;
        public boolean isMoving;

        public BackgroundTypePattern() {
        }

        public BackgroundTypePattern(BackgroundFill fill, int intensity, boolean isMoving) {
            this.fill = fill;
            this.intensity = intensity;
            this.isMoving = isMoving;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.BackgroundType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BackgroundTypeFill extends BackgroundType {
        public static final int CONSTRUCTOR = 993008684;
        public BackgroundFill fill;

        public BackgroundTypeFill() {
        }

        public BackgroundTypeFill(BackgroundFill fill) {
            this.fill = fill;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.BackgroundType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Backgrounds extends Object {
        public static final int CONSTRUCTOR = 724728704;
        public Background[] backgrounds;

        public Backgrounds() {
        }

        public Backgrounds(Background[] backgrounds) {
            this.backgrounds = backgrounds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BasicGroup extends Object {
        public static final int CONSTRUCTOR = -317839045;
        public int id;
        public boolean isActive;
        public int memberCount;
        public ChatMemberStatus status;
        public int upgradedToSupergroupId;

        public BasicGroup() {
        }

        public BasicGroup(int id, int memberCount, ChatMemberStatus status, boolean isActive, int upgradedToSupergroupId) {
            this.id = id;
            this.memberCount = memberCount;
            this.status = status;
            this.isActive = isActive;
            this.upgradedToSupergroupId = upgradedToSupergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BasicGroupFullInfo extends Object {
        public static final int CONSTRUCTOR = 161500149;
        public int creatorUserId;
        public String description;
        public String inviteLink;
        public ChatMember[] members;

        public BasicGroupFullInfo() {
        }

        public BasicGroupFullInfo(String description, int creatorUserId, ChatMember[] members, String inviteLink) {
            this.description = description;
            this.creatorUserId = creatorUserId;
            this.members = members;
            this.inviteLink = inviteLink;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BotCommand extends Object {
        public static final int CONSTRUCTOR = -1032140601;
        public String command;
        public String description;

        public BotCommand() {
        }

        public BotCommand(String command, String description) {
            this.command = command;
            this.description = description;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BotInfo extends Object {
        public static final int CONSTRUCTOR = 1296528907;
        public BotCommand[] commands;
        public String description;

        public BotInfo() {
        }

        public BotInfo(String description, BotCommand[] commands) {
            this.description = description;
            this.commands = commands;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Call extends Object {
        public static final int CONSTRUCTOR = -1837599107;
        public int id;
        public boolean isOutgoing;
        public CallState state;
        public int userId;

        public Call() {
        }

        public Call(int id, int userId, boolean isOutgoing, CallState state) {
            this.id = id;
            this.userId = userId;
            this.isOutgoing = isOutgoing;
            this.state = state;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallConnection extends Object {
        public static final int CONSTRUCTOR = 1318542714;
        public long id;
        public String ip;
        public String ipv6;
        public byte[] peerTag;
        public int port;

        public CallConnection() {
        }

        public CallConnection(long id, String ip, String ipv6, int port, byte[] peerTag) {
            this.id = id;
            this.ip = ip;
            this.ipv6 = ipv6;
            this.port = port;
            this.peerTag = peerTag;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallDiscardReasonEmpty extends CallDiscardReason {
        public static final int CONSTRUCTOR = -1258917949;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallDiscardReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallDiscardReasonMissed extends CallDiscardReason {
        public static final int CONSTRUCTOR = 1680358012;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallDiscardReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallDiscardReasonDeclined extends CallDiscardReason {
        public static final int CONSTRUCTOR = -1729926094;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallDiscardReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallDiscardReasonDisconnected extends CallDiscardReason {
        public static final int CONSTRUCTOR = -1342872670;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallDiscardReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallDiscardReasonHungUp extends CallDiscardReason {
        public static final int CONSTRUCTOR = 438216166;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallDiscardReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallId extends Object {
        public static final int CONSTRUCTOR = 65717769;
        public int id;

        public CallId() {
        }

        public CallId(int id) {
            this.id = id;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemEcho extends CallProblem {
        public static final int CONSTRUCTOR = 801116548;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemNoise extends CallProblem {
        public static final int CONSTRUCTOR = 1053065359;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemInterruptions extends CallProblem {
        public static final int CONSTRUCTOR = 1119493218;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemDistortedSpeech extends CallProblem {
        public static final int CONSTRUCTOR = 379960581;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemSilentLocal extends CallProblem {
        public static final int CONSTRUCTOR = 253652790;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemSilentRemote extends CallProblem {
        public static final int CONSTRUCTOR = 573634714;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProblemDropped extends CallProblem {
        public static final int CONSTRUCTOR = -1207311487;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallProblem, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallProtocol extends Object {
        public static final int CONSTRUCTOR = -1042830667;
        public int maxLayer;
        public int minLayer;
        public boolean udpP2p;
        public boolean udpReflector;

        public CallProtocol() {
        }

        public CallProtocol(boolean udpP2p, boolean udpReflector, int minLayer, int maxLayer) {
            this.udpP2p = udpP2p;
            this.udpReflector = udpReflector;
            this.minLayer = minLayer;
            this.maxLayer = maxLayer;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStatePending extends CallState {
        public static final int CONSTRUCTOR = 1073048620;
        public boolean isCreated;
        public boolean isReceived;

        public CallStatePending() {
        }

        public CallStatePending(boolean isCreated, boolean isReceived) {
            this.isCreated = isCreated;
            this.isReceived = isReceived;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStateExchangingKeys extends CallState {
        public static final int CONSTRUCTOR = -1848149403;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStateReady extends CallState {
        public static final int CONSTRUCTOR = 1848397705;
        public boolean allowP2p;
        public String config;
        public CallConnection[] connections;
        public String[] emojis;
        public byte[] encryptionKey;
        public CallProtocol protocol;

        public CallStateReady() {
        }

        public CallStateReady(CallProtocol protocol, CallConnection[] connections, String config, byte[] encryptionKey, String[] emojis, boolean allowP2p) {
            this.protocol = protocol;
            this.connections = connections;
            this.config = config;
            this.encryptionKey = encryptionKey;
            this.emojis = emojis;
            this.allowP2p = allowP2p;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStateHangingUp extends CallState {
        public static final int CONSTRUCTOR = -2133790038;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStateDiscarded extends CallState {
        public static final int CONSTRUCTOR = -190853167;
        public boolean needDebugInformation;
        public boolean needRating;
        public CallDiscardReason reason;

        public CallStateDiscarded() {
        }

        public CallStateDiscarded(CallDiscardReason reason, boolean needRating, boolean needDebugInformation) {
            this.reason = reason;
            this.needRating = needRating;
            this.needDebugInformation = needDebugInformation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallStateError extends CallState {
        public static final int CONSTRUCTOR = -975215467;
        public Error error;

        public CallStateError() {
        }

        public CallStateError(Error error) {
            this.error = error;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallbackQueryAnswer extends Object {
        public static final int CONSTRUCTOR = 360867933;
        public boolean showAlert;
        public String text;
        public String url;

        public CallbackQueryAnswer() {
        }

        public CallbackQueryAnswer(String text, boolean showAlert, String url) {
            this.text = text;
            this.showAlert = showAlert;
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallbackQueryPayloadData extends CallbackQueryPayload {
        public static final int CONSTRUCTOR = -1977729946;
        public byte[] data;

        public CallbackQueryPayloadData() {
        }

        public CallbackQueryPayloadData(byte[] data) {
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallbackQueryPayload, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CallbackQueryPayloadGame extends CallbackQueryPayload {
        public static final int CONSTRUCTOR = 1303571512;
        public String gameShortName;

        public CallbackQueryPayloadGame() {
        }

        public CallbackQueryPayloadGame(String gameShortName) {
            this.gameShortName = gameShortName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CallbackQueryPayload, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CanTransferOwnershipResultOk extends CanTransferOwnershipResult {
        public static final int CONSTRUCTOR = -89881021;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CanTransferOwnershipResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CanTransferOwnershipResultPasswordNeeded extends CanTransferOwnershipResult {
        public static final int CONSTRUCTOR = 1548372703;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CanTransferOwnershipResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CanTransferOwnershipResultPasswordTooFresh extends CanTransferOwnershipResult {
        public static final int CONSTRUCTOR = 811440913;
        public int retryAfter;

        public CanTransferOwnershipResultPasswordTooFresh() {
        }

        public CanTransferOwnershipResultPasswordTooFresh(int retryAfter) {
            this.retryAfter = retryAfter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CanTransferOwnershipResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CanTransferOwnershipResultSessionTooFresh extends CanTransferOwnershipResult {
        public static final int CONSTRUCTOR = 984664289;
        public int retryAfter;

        public CanTransferOwnershipResultSessionTooFresh() {
        }

        public CanTransferOwnershipResultSessionTooFresh(int retryAfter) {
            this.retryAfter = retryAfter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.CanTransferOwnershipResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Chat extends Object {
        public static final int CONSTRUCTOR = -861487386;
        public ChatActionBar actionBar;
        public boolean canBeDeletedForAllUsers;
        public boolean canBeDeletedOnlyForSelf;
        public boolean canBeReported;
        public ChatList chatList;
        public String clientData;
        public boolean defaultDisableNotification;
        public DraftMessage draftMessage;
        public boolean hasScheduledMessages;
        public long id;
        public boolean isMarkedAsUnread;
        public boolean isPinned;
        public boolean isSponsored;
        public Message lastMessage;
        public long lastReadInboxMessageId;
        public long lastReadOutboxMessageId;
        public ChatNotificationSettings notificationSettings;
        public long order;
        public ChatPermissions permissions;
        public ChatPhoto photo;
        public long pinnedMessageId;
        public long replyMarkupMessageId;
        public String title;
        public ChatType type;
        public int unreadCount;
        public int unreadMentionCount;

        public Chat() {
        }

        public Chat(long id, ChatType type, ChatList chatList, String title, ChatPhoto photo, ChatPermissions permissions, Message lastMessage, long order, boolean isPinned, boolean isMarkedAsUnread, boolean isSponsored, boolean hasScheduledMessages, boolean canBeDeletedOnlyForSelf, boolean canBeDeletedForAllUsers, boolean canBeReported, boolean defaultDisableNotification, int unreadCount, long lastReadInboxMessageId, long lastReadOutboxMessageId, int unreadMentionCount, ChatNotificationSettings notificationSettings, ChatActionBar actionBar, long pinnedMessageId, long replyMarkupMessageId, DraftMessage draftMessage, String clientData) {
            this.id = id;
            this.type = type;
            this.chatList = chatList;
            this.title = title;
            this.photo = photo;
            this.permissions = permissions;
            this.lastMessage = lastMessage;
            this.order = order;
            this.isPinned = isPinned;
            this.isMarkedAsUnread = isMarkedAsUnread;
            this.isSponsored = isSponsored;
            this.hasScheduledMessages = hasScheduledMessages;
            this.canBeDeletedOnlyForSelf = canBeDeletedOnlyForSelf;
            this.canBeDeletedForAllUsers = canBeDeletedForAllUsers;
            this.canBeReported = canBeReported;
            this.defaultDisableNotification = defaultDisableNotification;
            this.unreadCount = unreadCount;
            this.lastReadInboxMessageId = lastReadInboxMessageId;
            this.lastReadOutboxMessageId = lastReadOutboxMessageId;
            this.unreadMentionCount = unreadMentionCount;
            this.notificationSettings = notificationSettings;
            this.actionBar = actionBar;
            this.pinnedMessageId = pinnedMessageId;
            this.replyMarkupMessageId = replyMarkupMessageId;
            this.draftMessage = draftMessage;
            this.clientData = clientData;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionTyping extends ChatAction {
        public static final int CONSTRUCTOR = 380122167;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionRecordingVideo extends ChatAction {
        public static final int CONSTRUCTOR = 216553362;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionUploadingVideo extends ChatAction {
        public static final int CONSTRUCTOR = 1234185270;
        public int progress;

        public ChatActionUploadingVideo() {
        }

        public ChatActionUploadingVideo(int progress) {
            this.progress = progress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionRecordingVoiceNote extends ChatAction {
        public static final int CONSTRUCTOR = -808850058;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionUploadingVoiceNote extends ChatAction {
        public static final int CONSTRUCTOR = -613643666;
        public int progress;

        public ChatActionUploadingVoiceNote() {
        }

        public ChatActionUploadingVoiceNote(int progress) {
            this.progress = progress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionUploadingPhoto extends ChatAction {
        public static final int CONSTRUCTOR = 654240583;
        public int progress;

        public ChatActionUploadingPhoto() {
        }

        public ChatActionUploadingPhoto(int progress) {
            this.progress = progress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionUploadingDocument extends ChatAction {
        public static final int CONSTRUCTOR = 167884362;
        public int progress;

        public ChatActionUploadingDocument() {
        }

        public ChatActionUploadingDocument(int progress) {
            this.progress = progress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionChoosingLocation extends ChatAction {
        public static final int CONSTRUCTOR = -2017893596;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionChoosingContact extends ChatAction {
        public static final int CONSTRUCTOR = -1222507496;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionStartPlayingGame extends ChatAction {
        public static final int CONSTRUCTOR = -865884164;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionRecordingVideoNote extends ChatAction {
        public static final int CONSTRUCTOR = 16523393;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionUploadingVideoNote extends ChatAction {
        public static final int CONSTRUCTOR = 1172364918;
        public int progress;

        public ChatActionUploadingVideoNote() {
        }

        public ChatActionUploadingVideoNote(int progress) {
            this.progress = progress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionCancel extends ChatAction {
        public static final int CONSTRUCTOR = 1160523958;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionBarReportSpam extends ChatActionBar {
        public static final int CONSTRUCTOR = -1603417249;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatActionBar, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionBarReportUnrelatedLocation extends ChatActionBar {
        public static final int CONSTRUCTOR = 758175489;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatActionBar, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionBarReportAddBlock extends ChatActionBar {
        public static final int CONSTRUCTOR = -87894249;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatActionBar, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionBarAddContact extends ChatActionBar {
        public static final int CONSTRUCTOR = -733325295;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatActionBar, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatActionBarSharePhoneNumber extends ChatActionBar {
        public static final int CONSTRUCTOR = 35188697;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatActionBar, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatAdministrator extends Object {
        public static final int CONSTRUCTOR = 487220942;
        public String customTitle;
        public boolean isOwner;
        public int userId;

        public ChatAdministrator() {
        }

        public ChatAdministrator(int userId, String customTitle, boolean isOwner) {
            this.userId = userId;
            this.customTitle = customTitle;
            this.isOwner = isOwner;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatAdministrators extends Object {
        public static final int CONSTRUCTOR = -2126186435;
        public ChatAdministrator[] administrators;

        public ChatAdministrators() {
        }

        public ChatAdministrators(ChatAdministrator[] administrators) {
            this.administrators = administrators;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEvent extends Object {
        public static final int CONSTRUCTOR = -609912404;
        public ChatEventAction action;
        public int date;
        public long id;
        public int userId;

        public ChatEvent() {
        }

        public ChatEvent(long id, int date, int userId, ChatEventAction action) {
            this.id = id;
            this.date = date;
            this.userId = userId;
            this.action = action;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMessageEdited extends ChatEventAction {
        public static final int CONSTRUCTOR = -430967304;
        public Message newMessage;
        public Message oldMessage;

        public ChatEventMessageEdited() {
        }

        public ChatEventMessageEdited(Message oldMessage, Message newMessage) {
            this.oldMessage = oldMessage;
            this.newMessage = newMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMessageDeleted extends ChatEventAction {
        public static final int CONSTRUCTOR = -892974601;
        public Message message;

        public ChatEventMessageDeleted() {
        }

        public ChatEventMessageDeleted(Message message) {
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventPollStopped extends ChatEventAction {
        public static final int CONSTRUCTOR = 2009893861;
        public Message message;

        public ChatEventPollStopped() {
        }

        public ChatEventPollStopped(Message message) {
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMessagePinned extends ChatEventAction {
        public static final int CONSTRUCTOR = 438742298;
        public Message message;

        public ChatEventMessagePinned() {
        }

        public ChatEventMessagePinned(Message message) {
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMessageUnpinned extends ChatEventAction {
        public static final int CONSTRUCTOR = 2002594849;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMemberJoined extends ChatEventAction {
        public static final int CONSTRUCTOR = -235468508;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMemberLeft extends ChatEventAction {
        public static final int CONSTRUCTOR = -948420593;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMemberInvited extends ChatEventAction {
        public static final int CONSTRUCTOR = -2093688706;
        public ChatMemberStatus status;
        public int userId;

        public ChatEventMemberInvited() {
        }

        public ChatEventMemberInvited(int userId, ChatMemberStatus status) {
            this.userId = userId;
            this.status = status;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMemberPromoted extends ChatEventAction {
        public static final int CONSTRUCTOR = 1887176186;
        public ChatMemberStatus newStatus;
        public ChatMemberStatus oldStatus;
        public int userId;

        public ChatEventMemberPromoted() {
        }

        public ChatEventMemberPromoted(int userId, ChatMemberStatus oldStatus, ChatMemberStatus newStatus) {
            this.userId = userId;
            this.oldStatus = oldStatus;
            this.newStatus = newStatus;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventMemberRestricted extends ChatEventAction {
        public static final int CONSTRUCTOR = 584946294;
        public ChatMemberStatus newStatus;
        public ChatMemberStatus oldStatus;
        public int userId;

        public ChatEventMemberRestricted() {
        }

        public ChatEventMemberRestricted(int userId, ChatMemberStatus oldStatus, ChatMemberStatus newStatus) {
            this.userId = userId;
            this.oldStatus = oldStatus;
            this.newStatus = newStatus;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventTitleChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = 1134103250;
        public String newTitle;
        public String oldTitle;

        public ChatEventTitleChanged() {
        }

        public ChatEventTitleChanged(String oldTitle, String newTitle) {
            this.oldTitle = oldTitle;
            this.newTitle = newTitle;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventPermissionsChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = -1311557720;
        public ChatPermissions newPermissions;
        public ChatPermissions oldPermissions;

        public ChatEventPermissionsChanged() {
        }

        public ChatEventPermissionsChanged(ChatPermissions oldPermissions, ChatPermissions newPermissions) {
            this.oldPermissions = oldPermissions;
            this.newPermissions = newPermissions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventDescriptionChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = 39112478;
        public String newDescription;
        public String oldDescription;

        public ChatEventDescriptionChanged() {
        }

        public ChatEventDescriptionChanged(String oldDescription, String newDescription) {
            this.oldDescription = oldDescription;
            this.newDescription = newDescription;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventUsernameChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = 1728558443;
        public String newUsername;
        public String oldUsername;

        public ChatEventUsernameChanged() {
        }

        public ChatEventUsernameChanged(String oldUsername, String newUsername) {
            this.oldUsername = oldUsername;
            this.newUsername = newUsername;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventPhotoChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = 1037662734;
        public Photo newPhoto;
        public Photo oldPhoto;

        public ChatEventPhotoChanged() {
        }

        public ChatEventPhotoChanged(Photo oldPhoto, Photo newPhoto) {
            this.oldPhoto = oldPhoto;
            this.newPhoto = newPhoto;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventInvitesToggled extends ChatEventAction {
        public static final int CONSTRUCTOR = -62548373;
        public boolean canInviteUsers;

        public ChatEventInvitesToggled() {
        }

        public ChatEventInvitesToggled(boolean canInviteUsers) {
            this.canInviteUsers = canInviteUsers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventLinkedChatChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = 1797419439;
        public long newLinkedChatId;
        public long oldLinkedChatId;

        public ChatEventLinkedChatChanged() {
        }

        public ChatEventLinkedChatChanged(long oldLinkedChatId, long newLinkedChatId) {
            this.oldLinkedChatId = oldLinkedChatId;
            this.newLinkedChatId = newLinkedChatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventSlowModeDelayChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = -1653195765;
        public int newSlowModeDelay;
        public int oldSlowModeDelay;

        public ChatEventSlowModeDelayChanged() {
        }

        public ChatEventSlowModeDelayChanged(int oldSlowModeDelay, int newSlowModeDelay) {
            this.oldSlowModeDelay = oldSlowModeDelay;
            this.newSlowModeDelay = newSlowModeDelay;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventSignMessagesToggled extends ChatEventAction {
        public static final int CONSTRUCTOR = -1313265634;
        public boolean signMessages;

        public ChatEventSignMessagesToggled() {
        }

        public ChatEventSignMessagesToggled(boolean signMessages) {
            this.signMessages = signMessages;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventStickerSetChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = -1243130481;
        public long newStickerSetId;
        public long oldStickerSetId;

        public ChatEventStickerSetChanged() {
        }

        public ChatEventStickerSetChanged(long oldStickerSetId, long newStickerSetId) {
            this.oldStickerSetId = oldStickerSetId;
            this.newStickerSetId = newStickerSetId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventLocationChanged extends ChatEventAction {
        public static final int CONSTRUCTOR = -405930674;
        public ChatLocation newLocation;
        public ChatLocation oldLocation;

        public ChatEventLocationChanged() {
        }

        public ChatEventLocationChanged(ChatLocation oldLocation, ChatLocation newLocation) {
            this.oldLocation = oldLocation;
            this.newLocation = newLocation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventIsAllHistoryAvailableToggled extends ChatEventAction {
        public static final int CONSTRUCTOR = -1599063019;
        public boolean isAllHistoryAvailable;

        public ChatEventIsAllHistoryAvailableToggled() {
        }

        public ChatEventIsAllHistoryAvailableToggled(boolean isAllHistoryAvailable) {
            this.isAllHistoryAvailable = isAllHistoryAvailable;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatEventAction, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEventLogFilters extends Object {
        public static final int CONSTRUCTOR = 941939684;
        public boolean infoChanges;
        public boolean memberInvites;
        public boolean memberJoins;
        public boolean memberLeaves;
        public boolean memberPromotions;
        public boolean memberRestrictions;
        public boolean messageDeletions;
        public boolean messageEdits;
        public boolean messagePins;
        public boolean settingChanges;

        public ChatEventLogFilters() {
        }

        public ChatEventLogFilters(boolean messageEdits, boolean messageDeletions, boolean messagePins, boolean memberJoins, boolean memberLeaves, boolean memberInvites, boolean memberPromotions, boolean memberRestrictions, boolean infoChanges, boolean settingChanges) {
            this.messageEdits = messageEdits;
            this.messageDeletions = messageDeletions;
            this.messagePins = messagePins;
            this.memberJoins = memberJoins;
            this.memberLeaves = memberLeaves;
            this.memberInvites = memberInvites;
            this.memberPromotions = memberPromotions;
            this.memberRestrictions = memberRestrictions;
            this.infoChanges = infoChanges;
            this.settingChanges = settingChanges;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatEvents extends Object {
        public static final int CONSTRUCTOR = -585329664;
        public ChatEvent[] events;

        public ChatEvents() {
        }

        public ChatEvents(ChatEvent[] events) {
            this.events = events;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatInviteLink extends Object {
        public static final int CONSTRUCTOR = -882072492;
        public String inviteLink;

        public ChatInviteLink() {
        }

        public ChatInviteLink(String inviteLink) {
            this.inviteLink = inviteLink;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatInviteLinkInfo extends Object {
        public static final int CONSTRUCTOR = -323394424;
        public long chatId;
        public boolean isPublic;
        public int memberCount;
        public int[] memberUserIds;
        public ChatPhoto photo;
        public String title;
        public ChatType type;

        public ChatInviteLinkInfo() {
        }

        public ChatInviteLinkInfo(long chatId, ChatType type, String title, ChatPhoto photo, int memberCount, int[] memberUserIds, boolean isPublic) {
            this.chatId = chatId;
            this.type = type;
            this.title = title;
            this.photo = photo;
            this.memberCount = memberCount;
            this.memberUserIds = memberUserIds;
            this.isPublic = isPublic;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatListMain extends ChatList {
        public static final int CONSTRUCTOR = -400991316;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatList, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatListArchive extends ChatList {
        public static final int CONSTRUCTOR = 362770115;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatList, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatLocation extends Object {
        public static final int CONSTRUCTOR = -1566863583;
        public String address;
        public Location location;

        public ChatLocation() {
        }

        public ChatLocation(Location location, String address) {
            this.location = location;
            this.address = address;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMember extends Object {
        public static final int CONSTRUCTOR = -806137076;
        public BotInfo botInfo;
        public int inviterUserId;
        public int joinedChatDate;
        public ChatMemberStatus status;
        public int userId;

        public ChatMember() {
        }

        public ChatMember(int userId, int inviterUserId, int joinedChatDate, ChatMemberStatus status, BotInfo botInfo) {
            this.userId = userId;
            this.inviterUserId = inviterUserId;
            this.joinedChatDate = joinedChatDate;
            this.status = status;
            this.botInfo = botInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusCreator extends ChatMemberStatus {
        public static final int CONSTRUCTOR = 2038475849;
        public String customTitle;
        public boolean isMember;

        public ChatMemberStatusCreator() {
        }

        public ChatMemberStatusCreator(String customTitle, boolean isMember) {
            this.customTitle = customTitle;
            this.isMember = isMember;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusAdministrator extends ChatMemberStatus {
        public static final int CONSTRUCTOR = 1800612058;
        public boolean canBeEdited;
        public boolean canChangeInfo;
        public boolean canDeleteMessages;
        public boolean canEditMessages;
        public boolean canInviteUsers;
        public boolean canPinMessages;
        public boolean canPostMessages;
        public boolean canPromoteMembers;
        public boolean canRestrictMembers;
        public String customTitle;

        public ChatMemberStatusAdministrator() {
        }

        public ChatMemberStatusAdministrator(String customTitle, boolean canBeEdited, boolean canChangeInfo, boolean canPostMessages, boolean canEditMessages, boolean canDeleteMessages, boolean canInviteUsers, boolean canRestrictMembers, boolean canPinMessages, boolean canPromoteMembers) {
            this.customTitle = customTitle;
            this.canBeEdited = canBeEdited;
            this.canChangeInfo = canChangeInfo;
            this.canPostMessages = canPostMessages;
            this.canEditMessages = canEditMessages;
            this.canDeleteMessages = canDeleteMessages;
            this.canInviteUsers = canInviteUsers;
            this.canRestrictMembers = canRestrictMembers;
            this.canPinMessages = canPinMessages;
            this.canPromoteMembers = canPromoteMembers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusMember extends ChatMemberStatus {
        public static final int CONSTRUCTOR = 844723285;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusRestricted extends ChatMemberStatus {
        public static final int CONSTRUCTOR = 1661432998;
        public boolean isMember;
        public ChatPermissions permissions;
        public int restrictedUntilDate;

        public ChatMemberStatusRestricted() {
        }

        public ChatMemberStatusRestricted(boolean isMember, int restrictedUntilDate, ChatPermissions permissions) {
            this.isMember = isMember;
            this.restrictedUntilDate = restrictedUntilDate;
            this.permissions = permissions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusLeft extends ChatMemberStatus {
        public static final int CONSTRUCTOR = -5815259;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMemberStatusBanned extends ChatMemberStatus {
        public static final int CONSTRUCTOR = -1653518666;
        public int bannedUntilDate;

        public ChatMemberStatusBanned() {
        }

        public ChatMemberStatusBanned(int bannedUntilDate) {
            this.bannedUntilDate = bannedUntilDate;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMemberStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembers extends Object {
        public static final int CONSTRUCTOR = -497558622;
        public ChatMember[] members;
        public int totalCount;

        public ChatMembers() {
        }

        public ChatMembers(int totalCount, ChatMember[] members) {
            this.totalCount = totalCount;
            this.members = members;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterContacts extends ChatMembersFilter {
        public static final int CONSTRUCTOR = 1774485671;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterAdministrators extends ChatMembersFilter {
        public static final int CONSTRUCTOR = -1266893796;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterMembers extends ChatMembersFilter {
        public static final int CONSTRUCTOR = 670504342;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterRestricted extends ChatMembersFilter {
        public static final int CONSTRUCTOR = 1256282813;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterBanned extends ChatMembersFilter {
        public static final int CONSTRUCTOR = -1863102648;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatMembersFilterBots extends ChatMembersFilter {
        public static final int CONSTRUCTOR = -1422567288;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatNearby extends Object {
        public static final int CONSTRUCTOR = 48120405;
        public long chatId;
        public int distance;

        public ChatNearby() {
        }

        public ChatNearby(long chatId, int distance) {
            this.chatId = chatId;
            this.distance = distance;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatNotificationSettings extends Object {
        public static final int CONSTRUCTOR = 1503183218;
        public boolean disableMentionNotifications;
        public boolean disablePinnedMessageNotifications;
        public int muteFor;
        public boolean showPreview;
        public String sound;
        public boolean useDefaultDisableMentionNotifications;
        public boolean useDefaultDisablePinnedMessageNotifications;
        public boolean useDefaultMuteFor;
        public boolean useDefaultShowPreview;
        public boolean useDefaultSound;

        public ChatNotificationSettings() {
        }

        public ChatNotificationSettings(boolean useDefaultMuteFor, int muteFor, boolean useDefaultSound, String sound, boolean useDefaultShowPreview, boolean showPreview, boolean useDefaultDisablePinnedMessageNotifications, boolean disablePinnedMessageNotifications, boolean useDefaultDisableMentionNotifications, boolean disableMentionNotifications) {
            this.useDefaultMuteFor = useDefaultMuteFor;
            this.muteFor = muteFor;
            this.useDefaultSound = useDefaultSound;
            this.sound = sound;
            this.useDefaultShowPreview = useDefaultShowPreview;
            this.showPreview = showPreview;
            this.useDefaultDisablePinnedMessageNotifications = useDefaultDisablePinnedMessageNotifications;
            this.disablePinnedMessageNotifications = disablePinnedMessageNotifications;
            this.useDefaultDisableMentionNotifications = useDefaultDisableMentionNotifications;
            this.disableMentionNotifications = disableMentionNotifications;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatPermissions extends Object {
        public static final int CONSTRUCTOR = 1584650463;
        public boolean canAddWebPagePreviews;
        public boolean canChangeInfo;
        public boolean canInviteUsers;
        public boolean canPinMessages;
        public boolean canSendMediaMessages;
        public boolean canSendMessages;
        public boolean canSendOtherMessages;
        public boolean canSendPolls;

        public ChatPermissions() {
        }

        public ChatPermissions(boolean canSendMessages, boolean canSendMediaMessages, boolean canSendPolls, boolean canSendOtherMessages, boolean canAddWebPagePreviews, boolean canChangeInfo, boolean canInviteUsers, boolean canPinMessages) {
            this.canSendMessages = canSendMessages;
            this.canSendMediaMessages = canSendMediaMessages;
            this.canSendPolls = canSendPolls;
            this.canSendOtherMessages = canSendOtherMessages;
            this.canAddWebPagePreviews = canAddWebPagePreviews;
            this.canChangeInfo = canChangeInfo;
            this.canInviteUsers = canInviteUsers;
            this.canPinMessages = canPinMessages;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatPhoto extends Object {
        public static final int CONSTRUCTOR = -217062456;
        public File big;
        public File small;

        public ChatPhoto() {
        }

        public ChatPhoto(File small, File big) {
            this.small = small;
            this.big = big;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonSpam extends ChatReportReason {
        public static final int CONSTRUCTOR = -510848863;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonViolence extends ChatReportReason {
        public static final int CONSTRUCTOR = -1330235395;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonPornography extends ChatReportReason {
        public static final int CONSTRUCTOR = 722614385;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonChildAbuse extends ChatReportReason {
        public static final int CONSTRUCTOR = -1070686531;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonCopyright extends ChatReportReason {
        public static final int CONSTRUCTOR = 986898080;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonUnrelatedLocation extends ChatReportReason {
        public static final int CONSTRUCTOR = 2632403;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatReportReasonCustom extends ChatReportReason {
        public static final int CONSTRUCTOR = 544575454;
        public String text;

        public ChatReportReasonCustom() {
        }

        public ChatReportReasonCustom(String text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatReportReason, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatTypePrivate extends ChatType {
        public static final int CONSTRUCTOR = 1700720838;
        public int userId;

        public ChatTypePrivate() {
        }

        public ChatTypePrivate(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatTypeBasicGroup extends ChatType {
        public static final int CONSTRUCTOR = 21815278;
        public int basicGroupId;

        public ChatTypeBasicGroup() {
        }

        public ChatTypeBasicGroup(int basicGroupId) {
            this.basicGroupId = basicGroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatTypeSupergroup extends ChatType {
        public static final int CONSTRUCTOR = 955152366;
        public boolean isChannel;
        public int supergroupId;

        public ChatTypeSupergroup() {
        }

        public ChatTypeSupergroup(int supergroupId, boolean isChannel) {
            this.supergroupId = supergroupId;
            this.isChannel = isChannel;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatTypeSecret extends ChatType {
        public static final int CONSTRUCTOR = 136722563;
        public int secretChatId;
        public int userId;

        public ChatTypeSecret() {
        }

        public ChatTypeSecret(int secretChatId, int userId) {
            this.secretChatId = secretChatId;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Chats extends Object {
        public static final int CONSTRUCTOR = -1687756019;
        public long[] chatIds;

        public Chats() {
        }

        public Chats(long[] chatIds) {
            this.chatIds = chatIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChatsNearby extends Object {
        public static final int CONSTRUCTOR = 187746081;
        public ChatNearby[] supergroupsNearby;
        public ChatNearby[] usersNearby;

        public ChatsNearby() {
        }

        public ChatsNearby(ChatNearby[] usersNearby, ChatNearby[] supergroupsNearby) {
            this.usersNearby = usersNearby;
            this.supergroupsNearby = supergroupsNearby;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsernameResultOk extends CheckChatUsernameResult {
        public static final int CONSTRUCTOR = -1498956964;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CheckChatUsernameResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsernameResultUsernameInvalid extends CheckChatUsernameResult {
        public static final int CONSTRUCTOR = -636979370;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CheckChatUsernameResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsernameResultUsernameOccupied extends CheckChatUsernameResult {
        public static final int CONSTRUCTOR = 1320892201;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CheckChatUsernameResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsernameResultPublicChatsTooMuch extends CheckChatUsernameResult {
        public static final int CONSTRUCTOR = 858247741;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CheckChatUsernameResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsernameResultPublicGroupsUnavailable extends CheckChatUsernameResult {
        public static final int CONSTRUCTOR = -51833641;

        @Override // org.drinkless.td.libcore.telegram.TdApi.CheckChatUsernameResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectedWebsite extends Object {
        public static final int CONSTRUCTOR = -1538986855;
        public int botUserId;
        public String browser;
        public String domainName;
        public long id;
        public String ip;
        public int lastActiveDate;
        public String location;
        public int logInDate;
        public String platform;

        public ConnectedWebsite() {
        }

        public ConnectedWebsite(long id, String domainName, int botUserId, String browser, String platform, int logInDate, int lastActiveDate, String ip, String location) {
            this.id = id;
            this.domainName = domainName;
            this.botUserId = botUserId;
            this.browser = browser;
            this.platform = platform;
            this.logInDate = logInDate;
            this.lastActiveDate = lastActiveDate;
            this.ip = ip;
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectedWebsites extends Object {
        public static final int CONSTRUCTOR = -1727949694;
        public ConnectedWebsite[] websites;

        public ConnectedWebsites() {
        }

        public ConnectedWebsites(ConnectedWebsite[] websites) {
            this.websites = websites;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectionStateWaitingForNetwork extends ConnectionState {
        public static final int CONSTRUCTOR = 1695405912;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ConnectionState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectionStateConnectingToProxy extends ConnectionState {
        public static final int CONSTRUCTOR = -93187239;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ConnectionState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectionStateConnecting extends ConnectionState {
        public static final int CONSTRUCTOR = -1298400670;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ConnectionState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectionStateUpdating extends ConnectionState {
        public static final int CONSTRUCTOR = -188104009;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ConnectionState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConnectionStateReady extends ConnectionState {
        public static final int CONSTRUCTOR = 48608492;

        @Override // org.drinkless.td.libcore.telegram.TdApi.ConnectionState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Contact extends Object {
        public static final int CONSTRUCTOR = -1483002540;
        public String firstName;
        public String lastName;
        public String phoneNumber;
        public int userId;
        public String vcard;

        public Contact() {
        }

        public Contact(String phoneNumber, String firstName, String lastName, String vcard, int userId) {
            this.phoneNumber = phoneNumber;
            this.firstName = firstName;
            this.lastName = lastName;
            this.vcard = vcard;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Count extends Object {
        public static final int CONSTRUCTOR = 1295577348;
        public int count;

        public Count() {
        }

        public Count(int count) {
            this.count = count;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CustomRequestResult extends Object {
        public static final int CONSTRUCTOR = -2009960452;
        public String result;

        public CustomRequestResult() {
        }

        public CustomRequestResult(String result) {
            this.result = result;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DatabaseStatistics extends Object {
        public static final int CONSTRUCTOR = -1123912880;
        public String statistics;

        public DatabaseStatistics() {
        }

        public DatabaseStatistics(String statistics) {
            this.statistics = statistics;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Date extends Object {
        public static final int CONSTRUCTOR = -277956960;
        public int day;
        public int month;
        public int year;

        public Date() {
        }

        public Date(int day, int month, int year) {
            this.day = day;
            this.month = month;
            this.year = year;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DatedFile extends Object {
        public static final int CONSTRUCTOR = -1840795491;
        public int date;
        public File file;

        public DatedFile() {
        }

        public DatedFile(File file, int date) {
            this.file = file;
            this.date = date;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeepLinkInfo extends Object {
        public static final int CONSTRUCTOR = 1864081662;
        public boolean needUpdateApplication;
        public FormattedText text;

        public DeepLinkInfo() {
        }

        public DeepLinkInfo(FormattedText text, boolean needUpdateApplication) {
            this.text = text;
            this.needUpdateApplication = needUpdateApplication;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenFirebaseCloudMessaging extends DeviceToken {
        public static final int CONSTRUCTOR = -797881849;
        public boolean encrypt;
        public String token;

        public DeviceTokenFirebaseCloudMessaging() {
        }

        public DeviceTokenFirebaseCloudMessaging(String token, boolean encrypt) {
            this.token = token;
            this.encrypt = encrypt;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenApplePush extends DeviceToken {
        public static final int CONSTRUCTOR = 387541955;
        public String deviceToken;
        public boolean isAppSandbox;

        public DeviceTokenApplePush() {
        }

        public DeviceTokenApplePush(String deviceToken, boolean isAppSandbox) {
            this.deviceToken = deviceToken;
            this.isAppSandbox = isAppSandbox;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenApplePushVoIP extends DeviceToken {
        public static final int CONSTRUCTOR = 804275689;
        public String deviceToken;
        public boolean encrypt;
        public boolean isAppSandbox;

        public DeviceTokenApplePushVoIP() {
        }

        public DeviceTokenApplePushVoIP(String deviceToken, boolean isAppSandbox, boolean encrypt) {
            this.deviceToken = deviceToken;
            this.isAppSandbox = isAppSandbox;
            this.encrypt = encrypt;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenWindowsPush extends DeviceToken {
        public static final int CONSTRUCTOR = -1410514289;
        public String accessToken;

        public DeviceTokenWindowsPush() {
        }

        public DeviceTokenWindowsPush(String accessToken) {
            this.accessToken = accessToken;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenMicrosoftPush extends DeviceToken {
        public static final int CONSTRUCTOR = 1224269900;
        public String channelUri;

        public DeviceTokenMicrosoftPush() {
        }

        public DeviceTokenMicrosoftPush(String channelUri) {
            this.channelUri = channelUri;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenMicrosoftPushVoIP extends DeviceToken {
        public static final int CONSTRUCTOR = -785603759;
        public String channelUri;

        public DeviceTokenMicrosoftPushVoIP() {
        }

        public DeviceTokenMicrosoftPushVoIP(String channelUri) {
            this.channelUri = channelUri;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenWebPush extends DeviceToken {
        public static final int CONSTRUCTOR = -1694507273;
        public String authBase64url;
        public String endpoint;
        public String p256dhBase64url;

        public DeviceTokenWebPush() {
        }

        public DeviceTokenWebPush(String endpoint, String p256dhBase64url, String authBase64url) {
            this.endpoint = endpoint;
            this.p256dhBase64url = p256dhBase64url;
            this.authBase64url = authBase64url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenSimplePush extends DeviceToken {
        public static final int CONSTRUCTOR = 49584736;
        public String endpoint;

        public DeviceTokenSimplePush() {
        }

        public DeviceTokenSimplePush(String endpoint) {
            this.endpoint = endpoint;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenUbuntuPush extends DeviceToken {
        public static final int CONSTRUCTOR = 1782320422;
        public String token;

        public DeviceTokenUbuntuPush() {
        }

        public DeviceTokenUbuntuPush(String token) {
            this.token = token;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenBlackBerryPush extends DeviceToken {
        public static final int CONSTRUCTOR = 1559167234;
        public String token;

        public DeviceTokenBlackBerryPush() {
        }

        public DeviceTokenBlackBerryPush(String token) {
            this.token = token;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeviceTokenTizenPush extends DeviceToken {
        public static final int CONSTRUCTOR = -1359947213;
        public String regId;

        public DeviceTokenTizenPush() {
        }

        public DeviceTokenTizenPush(String regId) {
            this.regId = regId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.DeviceToken, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Document extends Object {
        public static final int CONSTRUCTOR = 21881988;
        public File document;
        public String fileName;
        public String mimeType;
        public Minithumbnail minithumbnail;
        public PhotoSize thumbnail;

        public Document() {
        }

        public Document(String fileName, String mimeType, Minithumbnail minithumbnail, PhotoSize thumbnail, File document) {
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.minithumbnail = minithumbnail;
            this.thumbnail = thumbnail;
            this.document = document;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DraftMessage extends Object {
        public static final int CONSTRUCTOR = 1902914742;
        public InputMessageContent inputMessageText;
        public long replyToMessageId;

        public DraftMessage() {
        }

        public DraftMessage(long replyToMessageId, InputMessageContent inputMessageText) {
            this.replyToMessageId = replyToMessageId;
            this.inputMessageText = inputMessageText;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EmailAddressAuthenticationCodeInfo extends Object {
        public static final int CONSTRUCTOR = 1151066659;
        public String emailAddressPattern;
        public int length;

        public EmailAddressAuthenticationCodeInfo() {
        }

        public EmailAddressAuthenticationCodeInfo(String emailAddressPattern, int length) {
            this.emailAddressPattern = emailAddressPattern;
            this.length = length;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Emojis extends Object {
        public static final int CONSTRUCTOR = 950339552;
        public String[] emojis;

        public Emojis() {
        }

        public Emojis(String[] emojis) {
            this.emojis = emojis;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EncryptedCredentials extends Object {
        public static final int CONSTRUCTOR = 1331106766;
        public byte[] data;
        public byte[] hash;
        public byte[] secret;

        public EncryptedCredentials() {
        }

        public EncryptedCredentials(byte[] data, byte[] hash, byte[] secret) {
            this.data = data;
            this.hash = hash;
            this.secret = secret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EncryptedPassportElement extends Object {
        public static final int CONSTRUCTOR = 2002386193;
        public byte[] data;
        public DatedFile[] files;
        public DatedFile frontSide;
        public String hash;
        public DatedFile reverseSide;
        public DatedFile selfie;
        public DatedFile[] translation;
        public PassportElementType type;
        public String value;

        public EncryptedPassportElement() {
        }

        public EncryptedPassportElement(PassportElementType type, byte[] data, DatedFile frontSide, DatedFile reverseSide, DatedFile selfie, DatedFile[] translation, DatedFile[] files, String value, String hash) {
            this.type = type;
            this.data = data;
            this.frontSide = frontSide;
            this.reverseSide = reverseSide;
            this.selfie = selfie;
            this.translation = translation;
            this.files = files;
            this.value = value;
            this.hash = hash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Error extends Object {
        public static final int CONSTRUCTOR = -1679978726;
        public int code;
        public String message;

        public Error() {
        }

        public Error(int code, String message) {
            this.code = code;
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class File extends Object {
        public static final int CONSTRUCTOR = 766337656;
        public int expectedSize;
        public int id;
        public LocalFile local;
        public RemoteFile remote;
        public int size;

        public File() {
        }

        public File(int id, int size, int expectedSize, LocalFile local, RemoteFile remote) {
            this.id = id;
            this.size = size;
            this.expectedSize = expectedSize;
            this.local = local;
            this.remote = remote;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FilePart extends Object {
        public static final int CONSTRUCTOR = 911821878;
        public byte[] data;

        public FilePart() {
        }

        public FilePart(byte[] data) {
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeNone extends FileType {
        public static final int CONSTRUCTOR = 2003009189;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeAnimation extends FileType {
        public static final int CONSTRUCTOR = -290816582;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeAudio extends FileType {
        public static final int CONSTRUCTOR = -709112160;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeDocument extends FileType {
        public static final int CONSTRUCTOR = -564722929;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypePhoto extends FileType {
        public static final int CONSTRUCTOR = -1718914651;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeProfilePhoto extends FileType {
        public static final int CONSTRUCTOR = 1795089315;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeSecret extends FileType {
        public static final int CONSTRUCTOR = -1871899401;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeSecretThumbnail extends FileType {
        public static final int CONSTRUCTOR = -1401326026;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeSecure extends FileType {
        public static final int CONSTRUCTOR = -1419133146;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeSticker extends FileType {
        public static final int CONSTRUCTOR = 475233385;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeThumbnail extends FileType {
        public static final int CONSTRUCTOR = -12443298;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeUnknown extends FileType {
        public static final int CONSTRUCTOR = -2011566768;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeVideo extends FileType {
        public static final int CONSTRUCTOR = 1430816539;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeVideoNote extends FileType {
        public static final int CONSTRUCTOR = -518412385;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeVoiceNote extends FileType {
        public static final int CONSTRUCTOR = -588681661;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FileTypeWallpaper extends FileType {
        public static final int CONSTRUCTOR = 1854930076;

        @Override // org.drinkless.td.libcore.telegram.TdApi.FileType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FormattedText extends Object {
        public static final int CONSTRUCTOR = -252624564;
        public TextEntity[] entities;
        public String text;

        public FormattedText() {
        }

        public FormattedText(String text, TextEntity[] entities) {
            this.text = text;
            this.entities = entities;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FoundMessages extends Object {
        public static final int CONSTRUCTOR = 2135623881;
        public Message[] messages;
        public long nextFromSearchId;

        public FoundMessages() {
        }

        public FoundMessages(Message[] messages, long nextFromSearchId) {
            this.messages = messages;
            this.nextFromSearchId = nextFromSearchId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Game extends Object {
        public static final int CONSTRUCTOR = -1565597752;
        public Animation animation;
        public String description;
        public long id;
        public Photo photo;
        public String shortName;
        public FormattedText text;
        public String title;

        public Game() {
        }

        public Game(long id, String shortName, String title, FormattedText text, String description, Photo photo, Animation animation) {
            this.id = id;
            this.shortName = shortName;
            this.title = title;
            this.text = text;
            this.description = description;
            this.photo = photo;
            this.animation = animation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GameHighScore extends Object {
        public static final int CONSTRUCTOR = -30778358;
        public int position;
        public int score;
        public int userId;

        public GameHighScore() {
        }

        public GameHighScore(int position, int userId, int score) {
            this.position = position;
            this.userId = userId;
            this.score = score;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GameHighScores extends Object {
        public static final int CONSTRUCTOR = -725770727;
        public GameHighScore[] scores;

        public GameHighScores() {
        }

        public GameHighScores(GameHighScore[] scores) {
            this.scores = scores;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Hashtags extends Object {
        public static final int CONSTRUCTOR = 676798885;
        public String[] hashtags;

        public Hashtags() {
        }

        public Hashtags(String[] hashtags) {
            this.hashtags = hashtags;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class HttpUrl extends Object {
        public static final int CONSTRUCTOR = -2018019930;
        public String url;

        public HttpUrl() {
        }

        public HttpUrl(String url) {
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class IdentityDocument extends Object {
        public static final int CONSTRUCTOR = 445952972;
        public Date expiryDate;
        public DatedFile frontSide;
        public String number;
        public DatedFile reverseSide;
        public DatedFile selfie;
        public DatedFile[] translation;

        public IdentityDocument() {
        }

        public IdentityDocument(String number, Date expiryDate, DatedFile frontSide, DatedFile reverseSide, DatedFile selfie, DatedFile[] translation) {
            this.number = number;
            this.expiryDate = expiryDate;
            this.frontSide = frontSide;
            this.reverseSide = reverseSide;
            this.selfie = selfie;
            this.translation = translation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ImportedContacts extends Object {
        public static final int CONSTRUCTOR = -741685354;
        public int[] importerCount;
        public int[] userIds;

        public ImportedContacts() {
        }

        public ImportedContacts(int[] userIds, int[] importerCount) {
            this.userIds = userIds;
            this.importerCount = importerCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButton extends Object {
        public static final int CONSTRUCTOR = -372105704;
        public String text;
        public InlineKeyboardButtonType type;

        public InlineKeyboardButton() {
        }

        public InlineKeyboardButton(String text, InlineKeyboardButtonType type) {
            this.text = text;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeUrl extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = 1130741420;
        public String url;

        public InlineKeyboardButtonTypeUrl() {
        }

        public InlineKeyboardButtonTypeUrl(String url) {
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeLoginUrl extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = 281435539;
        public String forwardText;
        public int id;
        public String url;

        public InlineKeyboardButtonTypeLoginUrl() {
        }

        public InlineKeyboardButtonTypeLoginUrl(String url, int id, String forwardText) {
            this.url = url;
            this.id = id;
            this.forwardText = forwardText;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeCallback extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = -1127515139;
        public byte[] data;

        public InlineKeyboardButtonTypeCallback() {
        }

        public InlineKeyboardButtonTypeCallback(byte[] data) {
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeCallbackGame extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = -383429528;

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeSwitchInline extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = -2035563307;
        public boolean inCurrentChat;
        public String query;

        public InlineKeyboardButtonTypeSwitchInline() {
        }

        public InlineKeyboardButtonTypeSwitchInline(String query, boolean inCurrentChat) {
            this.query = query;
            this.inCurrentChat = inCurrentChat;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineKeyboardButtonTypeBuy extends InlineKeyboardButtonType {
        public static final int CONSTRUCTOR = 1360739440;

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineKeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultArticle extends InlineQueryResult {
        public static final int CONSTRUCTOR = -518366710;
        public String description;
        public boolean hideUrl;
        public String id;
        public PhotoSize thumbnail;
        public String title;
        public String url;

        public InlineQueryResultArticle() {
        }

        public InlineQueryResultArticle(String id, String url, boolean hideUrl, String title, String description, PhotoSize thumbnail) {
            this.id = id;
            this.url = url;
            this.hideUrl = hideUrl;
            this.title = title;
            this.description = description;
            this.thumbnail = thumbnail;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultContact extends InlineQueryResult {
        public static final int CONSTRUCTOR = 410081985;
        public Contact contact;
        public String id;
        public PhotoSize thumbnail;

        public InlineQueryResultContact() {
        }

        public InlineQueryResultContact(String id, Contact contact, PhotoSize thumbnail) {
            this.id = id;
            this.contact = contact;
            this.thumbnail = thumbnail;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultLocation extends InlineQueryResult {
        public static final int CONSTRUCTOR = -158305341;
        public String id;
        public Location location;
        public PhotoSize thumbnail;
        public String title;

        public InlineQueryResultLocation() {
        }

        public InlineQueryResultLocation(String id, Location location, String title, PhotoSize thumbnail) {
            this.id = id;
            this.location = location;
            this.title = title;
            this.thumbnail = thumbnail;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultVenue extends InlineQueryResult {
        public static final int CONSTRUCTOR = -1592932211;
        public String id;
        public PhotoSize thumbnail;
        public Venue venue;

        public InlineQueryResultVenue() {
        }

        public InlineQueryResultVenue(String id, Venue venue, PhotoSize thumbnail) {
            this.id = id;
            this.venue = venue;
            this.thumbnail = thumbnail;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultGame extends InlineQueryResult {
        public static final int CONSTRUCTOR = 1706916987;
        public Game game;
        public String id;

        public InlineQueryResultGame() {
        }

        public InlineQueryResultGame(String id, Game game) {
            this.id = id;
            this.game = game;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultAnimation extends InlineQueryResult {
        public static final int CONSTRUCTOR = 2009984267;
        public Animation animation;
        public String id;
        public String title;

        public InlineQueryResultAnimation() {
        }

        public InlineQueryResultAnimation(String id, Animation animation, String title) {
            this.id = id;
            this.animation = animation;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultAudio extends InlineQueryResult {
        public static final int CONSTRUCTOR = 842650360;
        public Audio audio;
        public String id;

        public InlineQueryResultAudio() {
        }

        public InlineQueryResultAudio(String id, Audio audio) {
            this.id = id;
            this.audio = audio;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultDocument extends InlineQueryResult {
        public static final int CONSTRUCTOR = -1491268539;
        public String description;
        public Document document;
        public String id;
        public String title;

        public InlineQueryResultDocument() {
        }

        public InlineQueryResultDocument(String id, Document document, String title, String description) {
            this.id = id;
            this.document = document;
            this.title = title;
            this.description = description;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultPhoto extends InlineQueryResult {
        public static final int CONSTRUCTOR = 1848319440;
        public String description;
        public String id;
        public Photo photo;
        public String title;

        public InlineQueryResultPhoto() {
        }

        public InlineQueryResultPhoto(String id, Photo photo, String title, String description) {
            this.id = id;
            this.photo = photo;
            this.title = title;
            this.description = description;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultSticker extends InlineQueryResult {
        public static final int CONSTRUCTOR = -1848224245;
        public String id;
        public Sticker sticker;

        public InlineQueryResultSticker() {
        }

        public InlineQueryResultSticker(String id, Sticker sticker) {
            this.id = id;
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultVideo extends InlineQueryResult {
        public static final int CONSTRUCTOR = -1373158683;
        public String description;
        public String id;
        public String title;
        public Video video;

        public InlineQueryResultVideo() {
        }

        public InlineQueryResultVideo(String id, Video video, String title, String description) {
            this.id = id;
            this.video = video;
            this.title = title;
            this.description = description;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResultVoiceNote extends InlineQueryResult {
        public static final int CONSTRUCTOR = -1897393105;
        public String id;
        public String title;
        public VoiceNote voiceNote;

        public InlineQueryResultVoiceNote() {
        }

        public InlineQueryResultVoiceNote(String id, VoiceNote voiceNote, String title) {
            this.id = id;
            this.voiceNote = voiceNote;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InlineQueryResults extends Object {
        public static final int CONSTRUCTOR = 1000709656;
        public long inlineQueryId;
        public String nextOffset;
        public InlineQueryResult[] results;
        public String switchPmParameter;
        public String switchPmText;

        public InlineQueryResults() {
        }

        public InlineQueryResults(long inlineQueryId, String nextOffset, InlineQueryResult[] results, String switchPmText, String switchPmParameter) {
            this.inlineQueryId = inlineQueryId;
            this.nextOffset = nextOffset;
            this.results = results;
            this.switchPmText = switchPmText;
            this.switchPmParameter = switchPmParameter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputBackgroundLocal extends InputBackground {
        public static final int CONSTRUCTOR = -1747094364;
        public InputFile background;

        public InputBackgroundLocal() {
        }

        public InputBackgroundLocal(InputFile background) {
            this.background = background;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputBackground, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputBackgroundRemote extends InputBackground {
        public static final int CONSTRUCTOR = -274976231;
        public long backgroundId;

        public InputBackgroundRemote() {
        }

        public InputBackgroundRemote(long backgroundId) {
            this.backgroundId = backgroundId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputBackground, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputCredentialsSaved extends InputCredentials {
        public static final int CONSTRUCTOR = -2034385364;
        public String savedCredentialsId;

        public InputCredentialsSaved() {
        }

        public InputCredentialsSaved(String savedCredentialsId) {
            this.savedCredentialsId = savedCredentialsId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputCredentials, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputCredentialsNew extends InputCredentials {
        public static final int CONSTRUCTOR = -829689558;
        public boolean allowSave;
        public String data;

        public InputCredentialsNew() {
        }

        public InputCredentialsNew(String data, boolean allowSave) {
            this.data = data;
            this.allowSave = allowSave;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputCredentials, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputCredentialsAndroidPay extends InputCredentials {
        public static final int CONSTRUCTOR = 1979566832;
        public String data;

        public InputCredentialsAndroidPay() {
        }

        public InputCredentialsAndroidPay(String data) {
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputCredentials, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputCredentialsApplePay extends InputCredentials {
        public static final int CONSTRUCTOR = -1246570799;
        public String data;

        public InputCredentialsApplePay() {
        }

        public InputCredentialsApplePay(String data) {
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputCredentials, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputFileId extends InputFile {
        public static final int CONSTRUCTOR = 1788906253;
        public int id;

        public InputFileId() {
        }

        public InputFileId(int id) {
            this.id = id;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputFile, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputFileRemote extends InputFile {
        public static final int CONSTRUCTOR = -107574466;
        public String id;

        public InputFileRemote() {
        }

        public InputFileRemote(String id) {
            this.id = id;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputFile, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputFileLocal extends InputFile {
        public static final int CONSTRUCTOR = 2056030919;
        public String path;

        public InputFileLocal() {
        }

        public InputFileLocal(String path) {
            this.path = path;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputFile, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputFileGenerated extends InputFile {
        public static final int CONSTRUCTOR = -1781351885;
        public String conversion;
        public int expectedSize;
        public String originalPath;

        public InputFileGenerated() {
        }

        public InputFileGenerated(String originalPath, String conversion, int expectedSize) {
            this.originalPath = originalPath;
            this.conversion = conversion;
            this.expectedSize = expectedSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputFile, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputIdentityDocument extends Object {
        public static final int CONSTRUCTOR = -381776063;
        public Date expiryDate;
        public InputFile frontSide;
        public String number;
        public InputFile reverseSide;
        public InputFile selfie;
        public InputFile[] translation;

        public InputIdentityDocument() {
        }

        public InputIdentityDocument(String number, Date expiryDate, InputFile frontSide, InputFile reverseSide, InputFile selfie, InputFile[] translation) {
            this.number = number;
            this.expiryDate = expiryDate;
            this.frontSide = frontSide;
            this.reverseSide = reverseSide;
            this.selfie = selfie;
            this.translation = translation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultAnimatedGif extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = -891474894;
        public int gifDuration;
        public int gifHeight;
        public String gifUrl;
        public int gifWidth;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public String thumbnailUrl;
        public String title;

        public InputInlineQueryResultAnimatedGif() {
        }

        public InputInlineQueryResultAnimatedGif(String id, String title, String thumbnailUrl, String gifUrl, int gifDuration, int gifWidth, int gifHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.thumbnailUrl = thumbnailUrl;
            this.gifUrl = gifUrl;
            this.gifDuration = gifDuration;
            this.gifWidth = gifWidth;
            this.gifHeight = gifHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultAnimatedMpeg4 extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = -1629529888;
        public String id;
        public InputMessageContent inputMessageContent;
        public int mpeg4Duration;
        public int mpeg4Height;
        public String mpeg4Url;
        public int mpeg4Width;
        public ReplyMarkup replyMarkup;
        public String thumbnailUrl;
        public String title;

        public InputInlineQueryResultAnimatedMpeg4() {
        }

        public InputInlineQueryResultAnimatedMpeg4(String id, String title, String thumbnailUrl, String mpeg4Url, int mpeg4Duration, int mpeg4Width, int mpeg4Height, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.thumbnailUrl = thumbnailUrl;
            this.mpeg4Url = mpeg4Url;
            this.mpeg4Duration = mpeg4Duration;
            this.mpeg4Width = mpeg4Width;
            this.mpeg4Height = mpeg4Height;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultArticle extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 1973670156;
        public String description;
        public boolean hideUrl;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public int thumbnailHeight;
        public String thumbnailUrl;
        public int thumbnailWidth;
        public String title;
        public String url;

        public InputInlineQueryResultArticle() {
        }

        public InputInlineQueryResultArticle(String id, String url, boolean hideUrl, String title, String description, String thumbnailUrl, int thumbnailWidth, int thumbnailHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.url = url;
            this.hideUrl = hideUrl;
            this.title = title;
            this.description = description;
            this.thumbnailUrl = thumbnailUrl;
            this.thumbnailWidth = thumbnailWidth;
            this.thumbnailHeight = thumbnailHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultAudio extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 1260139988;
        public int audioDuration;
        public String audioUrl;
        public String id;
        public InputMessageContent inputMessageContent;
        public String performer;
        public ReplyMarkup replyMarkup;
        public String title;

        public InputInlineQueryResultAudio() {
        }

        public InputInlineQueryResultAudio(String id, String title, String performer, String audioUrl, int audioDuration, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.performer = performer;
            this.audioUrl = audioUrl;
            this.audioDuration = audioDuration;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultContact extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 1846064594;
        public Contact contact;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public int thumbnailHeight;
        public String thumbnailUrl;
        public int thumbnailWidth;

        public InputInlineQueryResultContact() {
        }

        public InputInlineQueryResultContact(String id, Contact contact, String thumbnailUrl, int thumbnailWidth, int thumbnailHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.contact = contact;
            this.thumbnailUrl = thumbnailUrl;
            this.thumbnailWidth = thumbnailWidth;
            this.thumbnailHeight = thumbnailHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultDocument extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 578801869;
        public String description;
        public String documentUrl;
        public String id;
        public InputMessageContent inputMessageContent;
        public String mimeType;
        public ReplyMarkup replyMarkup;
        public int thumbnailHeight;
        public String thumbnailUrl;
        public int thumbnailWidth;
        public String title;

        public InputInlineQueryResultDocument() {
        }

        public InputInlineQueryResultDocument(String id, String title, String description, String documentUrl, String mimeType, String thumbnailUrl, int thumbnailWidth, int thumbnailHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.documentUrl = documentUrl;
            this.mimeType = mimeType;
            this.thumbnailUrl = thumbnailUrl;
            this.thumbnailWidth = thumbnailWidth;
            this.thumbnailHeight = thumbnailHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultGame extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 966074327;
        public String gameShortName;
        public String id;
        public ReplyMarkup replyMarkup;

        public InputInlineQueryResultGame() {
        }

        public InputInlineQueryResultGame(String id, String gameShortName, ReplyMarkup replyMarkup) {
            this.id = id;
            this.gameShortName = gameShortName;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultLocation extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = -1887650218;
        public String id;
        public InputMessageContent inputMessageContent;
        public int livePeriod;
        public Location location;
        public ReplyMarkup replyMarkup;
        public int thumbnailHeight;
        public String thumbnailUrl;
        public int thumbnailWidth;
        public String title;

        public InputInlineQueryResultLocation() {
        }

        public InputInlineQueryResultLocation(String id, Location location, int livePeriod, String title, String thumbnailUrl, int thumbnailWidth, int thumbnailHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.location = location;
            this.livePeriod = livePeriod;
            this.title = title;
            this.thumbnailUrl = thumbnailUrl;
            this.thumbnailWidth = thumbnailWidth;
            this.thumbnailHeight = thumbnailHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultPhoto extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = -1123338721;
        public String description;
        public String id;
        public InputMessageContent inputMessageContent;
        public int photoHeight;
        public String photoUrl;
        public int photoWidth;
        public ReplyMarkup replyMarkup;
        public String thumbnailUrl;
        public String title;

        public InputInlineQueryResultPhoto() {
        }

        public InputInlineQueryResultPhoto(String id, String title, String description, String thumbnailUrl, String photoUrl, int photoWidth, int photoHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.thumbnailUrl = thumbnailUrl;
            this.photoUrl = photoUrl;
            this.photoWidth = photoWidth;
            this.photoHeight = photoHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultSticker extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 274007129;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public int stickerHeight;
        public String stickerUrl;
        public int stickerWidth;
        public String thumbnailUrl;

        public InputInlineQueryResultSticker() {
        }

        public InputInlineQueryResultSticker(String id, String thumbnailUrl, String stickerUrl, int stickerWidth, int stickerHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.thumbnailUrl = thumbnailUrl;
            this.stickerUrl = stickerUrl;
            this.stickerWidth = stickerWidth;
            this.stickerHeight = stickerHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultVenue extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 541704509;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public int thumbnailHeight;
        public String thumbnailUrl;
        public int thumbnailWidth;
        public Venue venue;

        public InputInlineQueryResultVenue() {
        }

        public InputInlineQueryResultVenue(String id, Venue venue, String thumbnailUrl, int thumbnailWidth, int thumbnailHeight, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.venue = venue;
            this.thumbnailUrl = thumbnailUrl;
            this.thumbnailWidth = thumbnailWidth;
            this.thumbnailHeight = thumbnailHeight;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultVideo extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = 1724073191;
        public String description;
        public String id;
        public InputMessageContent inputMessageContent;
        public String mimeType;
        public ReplyMarkup replyMarkup;
        public String thumbnailUrl;
        public String title;
        public int videoDuration;
        public int videoHeight;
        public String videoUrl;
        public int videoWidth;

        public InputInlineQueryResultVideo() {
        }

        public InputInlineQueryResultVideo(String id, String title, String description, String thumbnailUrl, String videoUrl, String mimeType, int videoWidth, int videoHeight, int videoDuration, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.thumbnailUrl = thumbnailUrl;
            this.videoUrl = videoUrl;
            this.mimeType = mimeType;
            this.videoWidth = videoWidth;
            this.videoHeight = videoHeight;
            this.videoDuration = videoDuration;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputInlineQueryResultVoiceNote extends InputInlineQueryResult {
        public static final int CONSTRUCTOR = -1790072503;
        public String id;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;
        public String title;
        public int voiceNoteDuration;
        public String voiceNoteUrl;

        public InputInlineQueryResultVoiceNote() {
        }

        public InputInlineQueryResultVoiceNote(String id, String title, String voiceNoteUrl, int voiceNoteDuration, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.id = id;
            this.title = title;
            this.voiceNoteUrl = voiceNoteUrl;
            this.voiceNoteDuration = voiceNoteDuration;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputInlineQueryResult, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageText extends InputMessageContent {
        public static final int CONSTRUCTOR = 247050392;
        public boolean clearDraft;
        public boolean disableWebPagePreview;
        public FormattedText text;

        public InputMessageText() {
        }

        public InputMessageText(FormattedText text, boolean disableWebPagePreview, boolean clearDraft) {
            this.text = text;
            this.disableWebPagePreview = disableWebPagePreview;
            this.clearDraft = clearDraft;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageAnimation extends InputMessageContent {
        public static final int CONSTRUCTOR = 926542724;
        public InputFile animation;
        public FormattedText caption;
        public int duration;
        public int height;
        public InputThumbnail thumbnail;
        public int width;

        public InputMessageAnimation() {
        }

        public InputMessageAnimation(InputFile animation, InputThumbnail thumbnail, int duration, int width, int height, FormattedText caption) {
            this.animation = animation;
            this.thumbnail = thumbnail;
            this.duration = duration;
            this.width = width;
            this.height = height;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageAudio extends InputMessageContent {
        public static final int CONSTRUCTOR = -626786126;
        public InputThumbnail albumCoverThumbnail;
        public InputFile audio;
        public FormattedText caption;
        public int duration;
        public String performer;
        public String title;

        public InputMessageAudio() {
        }

        public InputMessageAudio(InputFile audio, InputThumbnail albumCoverThumbnail, int duration, String title, String performer, FormattedText caption) {
            this.audio = audio;
            this.albumCoverThumbnail = albumCoverThumbnail;
            this.duration = duration;
            this.title = title;
            this.performer = performer;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageDocument extends InputMessageContent {
        public static final int CONSTRUCTOR = 937970604;
        public FormattedText caption;
        public InputFile document;
        public InputThumbnail thumbnail;

        public InputMessageDocument() {
        }

        public InputMessageDocument(InputFile document, InputThumbnail thumbnail, FormattedText caption) {
            this.document = document;
            this.thumbnail = thumbnail;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessagePhoto extends InputMessageContent {
        public static final int CONSTRUCTOR = 1648801584;
        public int[] addedStickerFileIds;
        public FormattedText caption;
        public int height;
        public InputFile photo;
        public InputThumbnail thumbnail;
        public int ttl;
        public int width;

        public InputMessagePhoto() {
        }

        public InputMessagePhoto(InputFile photo, InputThumbnail thumbnail, int[] addedStickerFileIds, int width, int height, FormattedText caption, int ttl) {
            this.photo = photo;
            this.thumbnail = thumbnail;
            this.addedStickerFileIds = addedStickerFileIds;
            this.width = width;
            this.height = height;
            this.caption = caption;
            this.ttl = ttl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageSticker extends InputMessageContent {
        public static final int CONSTRUCTOR = 740776325;
        public int height;
        public InputFile sticker;
        public InputThumbnail thumbnail;
        public int width;

        public InputMessageSticker() {
        }

        public InputMessageSticker(InputFile sticker, InputThumbnail thumbnail, int width, int height) {
            this.sticker = sticker;
            this.thumbnail = thumbnail;
            this.width = width;
            this.height = height;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageVideo extends InputMessageContent {
        public static final int CONSTRUCTOR = -2108486755;
        public int[] addedStickerFileIds;
        public FormattedText caption;
        public int duration;
        public int height;
        public boolean supportsStreaming;
        public InputThumbnail thumbnail;
        public int ttl;
        public InputFile video;
        public int width;

        public InputMessageVideo() {
        }

        public InputMessageVideo(InputFile video, InputThumbnail thumbnail, int[] addedStickerFileIds, int duration, int width, int height, boolean supportsStreaming, FormattedText caption, int ttl) {
            this.video = video;
            this.thumbnail = thumbnail;
            this.addedStickerFileIds = addedStickerFileIds;
            this.duration = duration;
            this.width = width;
            this.height = height;
            this.supportsStreaming = supportsStreaming;
            this.caption = caption;
            this.ttl = ttl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageVideoNote extends InputMessageContent {
        public static final int CONSTRUCTOR = 279108859;
        public int duration;
        public int length;
        public InputThumbnail thumbnail;
        public InputFile videoNote;

        public InputMessageVideoNote() {
        }

        public InputMessageVideoNote(InputFile videoNote, InputThumbnail thumbnail, int duration, int length) {
            this.videoNote = videoNote;
            this.thumbnail = thumbnail;
            this.duration = duration;
            this.length = length;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageVoiceNote extends InputMessageContent {
        public static final int CONSTRUCTOR = 2136519657;
        public FormattedText caption;
        public int duration;
        public InputFile voiceNote;
        public byte[] waveform;

        public InputMessageVoiceNote() {
        }

        public InputMessageVoiceNote(InputFile voiceNote, int duration, byte[] waveform, FormattedText caption) {
            this.voiceNote = voiceNote;
            this.duration = duration;
            this.waveform = waveform;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageLocation extends InputMessageContent {
        public static final int CONSTRUCTOR = -1624179655;
        public int livePeriod;
        public Location location;

        public InputMessageLocation() {
        }

        public InputMessageLocation(Location location, int livePeriod) {
            this.location = location;
            this.livePeriod = livePeriod;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageVenue extends InputMessageContent {
        public static final int CONSTRUCTOR = 1447926269;
        public Venue venue;

        public InputMessageVenue() {
        }

        public InputMessageVenue(Venue venue) {
            this.venue = venue;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageContact extends InputMessageContent {
        public static final int CONSTRUCTOR = -982446849;
        public Contact contact;

        public InputMessageContact() {
        }

        public InputMessageContact(Contact contact) {
            this.contact = contact;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageGame extends InputMessageContent {
        public static final int CONSTRUCTOR = -1728000914;
        public int botUserId;
        public String gameShortName;

        public InputMessageGame() {
        }

        public InputMessageGame(int botUserId, String gameShortName) {
            this.botUserId = botUserId;
            this.gameShortName = gameShortName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageInvoice extends InputMessageContent {
        public static final int CONSTRUCTOR = 1038812175;
        public String description;
        public Invoice invoice;
        public byte[] payload;
        public int photoHeight;
        public int photoSize;
        public String photoUrl;
        public int photoWidth;
        public String providerData;
        public String providerToken;
        public String startParameter;
        public String title;

        public InputMessageInvoice() {
        }

        public InputMessageInvoice(Invoice invoice, String title, String description, String photoUrl, int photoSize, int photoWidth, int photoHeight, byte[] payload, String providerToken, String providerData, String startParameter) {
            this.invoice = invoice;
            this.title = title;
            this.description = description;
            this.photoUrl = photoUrl;
            this.photoSize = photoSize;
            this.photoWidth = photoWidth;
            this.photoHeight = photoHeight;
            this.payload = payload;
            this.providerToken = providerToken;
            this.providerData = providerData;
            this.startParameter = startParameter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessagePoll extends InputMessageContent {
        public static final int CONSTRUCTOR = 743307780;
        public boolean isAnonymous;
        public boolean isClosed;
        public String[] options;
        public String question;
        public PollType type;

        public InputMessagePoll() {
        }

        public InputMessagePoll(String question, String[] options, boolean isAnonymous, PollType type, boolean isClosed) {
            this.question = question;
            this.options = options;
            this.isAnonymous = isAnonymous;
            this.type = type;
            this.isClosed = isClosed;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputMessageForwarded extends InputMessageContent {
        public static final int CONSTRUCTOR = 1503132333;
        public long fromChatId;
        public boolean inGameShare;
        public long messageId;
        public boolean removeCaption;
        public boolean sendCopy;

        public InputMessageForwarded() {
        }

        public InputMessageForwarded(long fromChatId, long messageId, boolean inGameShare, boolean sendCopy, boolean removeCaption) {
            this.fromChatId = fromChatId;
            this.messageId = messageId;
            this.inGameShare = inGameShare;
            this.sendCopy = sendCopy;
            this.removeCaption = removeCaption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementPersonalDetails extends InputPassportElement {
        public static final int CONSTRUCTOR = 164791359;
        public PersonalDetails personalDetails;

        public InputPassportElementPersonalDetails() {
        }

        public InputPassportElementPersonalDetails(PersonalDetails personalDetails) {
            this.personalDetails = personalDetails;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementPassport extends InputPassportElement {
        public static final int CONSTRUCTOR = -497011356;
        public InputIdentityDocument passport;

        public InputPassportElementPassport() {
        }

        public InputPassportElementPassport(InputIdentityDocument passport) {
            this.passport = passport;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementDriverLicense extends InputPassportElement {
        public static final int CONSTRUCTOR = 304813264;
        public InputIdentityDocument driverLicense;

        public InputPassportElementDriverLicense() {
        }

        public InputPassportElementDriverLicense(InputIdentityDocument driverLicense) {
            this.driverLicense = driverLicense;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementIdentityCard extends InputPassportElement {
        public static final int CONSTRUCTOR = -9963390;
        public InputIdentityDocument identityCard;

        public InputPassportElementIdentityCard() {
        }

        public InputPassportElementIdentityCard(InputIdentityDocument identityCard) {
            this.identityCard = identityCard;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementInternalPassport extends InputPassportElement {
        public static final int CONSTRUCTOR = 715360043;
        public InputIdentityDocument internalPassport;

        public InputPassportElementInternalPassport() {
        }

        public InputPassportElementInternalPassport(InputIdentityDocument internalPassport) {
            this.internalPassport = internalPassport;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementAddress extends InputPassportElement {
        public static final int CONSTRUCTOR = 461630480;
        public Address address;

        public InputPassportElementAddress() {
        }

        public InputPassportElementAddress(Address address) {
            this.address = address;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementUtilityBill extends InputPassportElement {
        public static final int CONSTRUCTOR = 1389203841;
        public InputPersonalDocument utilityBill;

        public InputPassportElementUtilityBill() {
        }

        public InputPassportElementUtilityBill(InputPersonalDocument utilityBill) {
            this.utilityBill = utilityBill;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementBankStatement extends InputPassportElement {
        public static final int CONSTRUCTOR = -26585208;
        public InputPersonalDocument bankStatement;

        public InputPassportElementBankStatement() {
        }

        public InputPassportElementBankStatement(InputPersonalDocument bankStatement) {
            this.bankStatement = bankStatement;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementRentalAgreement extends InputPassportElement {
        public static final int CONSTRUCTOR = 1736154155;
        public InputPersonalDocument rentalAgreement;

        public InputPassportElementRentalAgreement() {
        }

        public InputPassportElementRentalAgreement(InputPersonalDocument rentalAgreement) {
            this.rentalAgreement = rentalAgreement;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementPassportRegistration extends InputPassportElement {
        public static final int CONSTRUCTOR = 1314562128;
        public InputPersonalDocument passportRegistration;

        public InputPassportElementPassportRegistration() {
        }

        public InputPassportElementPassportRegistration(InputPersonalDocument passportRegistration) {
            this.passportRegistration = passportRegistration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementTemporaryRegistration extends InputPassportElement {
        public static final int CONSTRUCTOR = -1913238047;
        public InputPersonalDocument temporaryRegistration;

        public InputPassportElementTemporaryRegistration() {
        }

        public InputPassportElementTemporaryRegistration(InputPersonalDocument temporaryRegistration) {
            this.temporaryRegistration = temporaryRegistration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementPhoneNumber extends InputPassportElement {
        public static final int CONSTRUCTOR = 1319357497;
        public String phoneNumber;

        public InputPassportElementPhoneNumber() {
        }

        public InputPassportElementPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementEmailAddress extends InputPassportElement {
        public static final int CONSTRUCTOR = -248605659;
        public String emailAddress;

        public InputPassportElementEmailAddress() {
        }

        public InputPassportElementEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementError extends Object {
        public static final int CONSTRUCTOR = 285756898;
        public String message;
        public InputPassportElementErrorSource source;
        public PassportElementType type;

        public InputPassportElementError() {
        }

        public InputPassportElementError(PassportElementType type, String message, InputPassportElementErrorSource source) {
            this.type = type;
            this.message = message;
            this.source = source;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceUnspecified extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = 267230319;
        public byte[] elementHash;

        public InputPassportElementErrorSourceUnspecified() {
        }

        public InputPassportElementErrorSourceUnspecified(byte[] elementHash) {
            this.elementHash = elementHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceDataField extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = -426795002;
        public byte[] dataHash;
        public String fieldName;

        public InputPassportElementErrorSourceDataField() {
        }

        public InputPassportElementErrorSourceDataField(String fieldName, byte[] dataHash) {
            this.fieldName = fieldName;
            this.dataHash = dataHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceFrontSide extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = 588023741;
        public byte[] fileHash;

        public InputPassportElementErrorSourceFrontSide() {
        }

        public InputPassportElementErrorSourceFrontSide(byte[] fileHash) {
            this.fileHash = fileHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceReverseSide extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = 413072891;
        public byte[] fileHash;

        public InputPassportElementErrorSourceReverseSide() {
        }

        public InputPassportElementErrorSourceReverseSide(byte[] fileHash) {
            this.fileHash = fileHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceSelfie extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = -773575528;
        public byte[] fileHash;

        public InputPassportElementErrorSourceSelfie() {
        }

        public InputPassportElementErrorSourceSelfie(byte[] fileHash) {
            this.fileHash = fileHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceTranslationFile extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = 505842299;
        public byte[] fileHash;

        public InputPassportElementErrorSourceTranslationFile() {
        }

        public InputPassportElementErrorSourceTranslationFile(byte[] fileHash) {
            this.fileHash = fileHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceTranslationFiles extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = -527254048;
        public byte[][] fileHashes;

        public InputPassportElementErrorSourceTranslationFiles() {
        }

        public InputPassportElementErrorSourceTranslationFiles(byte[][] fileHashes) {
            this.fileHashes = fileHashes;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceFile extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = -298492469;
        public byte[] fileHash;

        public InputPassportElementErrorSourceFile() {
        }

        public InputPassportElementErrorSourceFile(byte[] fileHash) {
            this.fileHash = fileHash;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPassportElementErrorSourceFiles extends InputPassportElementErrorSource {
        public static final int CONSTRUCTOR = -2008541640;
        public byte[][] fileHashes;

        public InputPassportElementErrorSourceFiles() {
        }

        public InputPassportElementErrorSourceFiles(byte[][] fileHashes) {
            this.fileHashes = fileHashes;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.InputPassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputPersonalDocument extends Object {
        public static final int CONSTRUCTOR = 1676966826;
        public InputFile[] files;
        public InputFile[] translation;

        public InputPersonalDocument() {
        }

        public InputPersonalDocument(InputFile[] files, InputFile[] translation) {
            this.files = files;
            this.translation = translation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputSticker extends Object {
        public static final int CONSTRUCTOR = -1998602205;
        public String emojis;
        public MaskPosition maskPosition;
        public InputFile pngSticker;

        public InputSticker() {
        }

        public InputSticker(InputFile pngSticker, String emojis, MaskPosition maskPosition) {
            this.pngSticker = pngSticker;
            this.emojis = emojis;
            this.maskPosition = maskPosition;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class InputThumbnail extends Object {
        public static final int CONSTRUCTOR = 1582387236;
        public int height;
        public InputFile thumbnail;
        public int width;

        public InputThumbnail() {
        }

        public InputThumbnail(InputFile thumbnail, int width, int height) {
            this.thumbnail = thumbnail;
            this.width = width;
            this.height = height;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Invoice extends Object {
        public static final int CONSTRUCTOR = -368451690;
        public String currency;
        public boolean isFlexible;
        public boolean isTest;
        public boolean needEmailAddress;
        public boolean needName;
        public boolean needPhoneNumber;
        public boolean needShippingAddress;
        public LabeledPricePart[] priceParts;
        public boolean sendEmailAddressToProvider;
        public boolean sendPhoneNumberToProvider;

        public Invoice() {
        }

        public Invoice(String currency, LabeledPricePart[] priceParts, boolean isTest, boolean needName, boolean needPhoneNumber, boolean needEmailAddress, boolean needShippingAddress, boolean sendPhoneNumberToProvider, boolean sendEmailAddressToProvider, boolean isFlexible) {
            this.currency = currency;
            this.priceParts = priceParts;
            this.isTest = isTest;
            this.needName = needName;
            this.needPhoneNumber = needPhoneNumber;
            this.needEmailAddress = needEmailAddress;
            this.needShippingAddress = needShippingAddress;
            this.sendPhoneNumberToProvider = sendPhoneNumberToProvider;
            this.sendEmailAddressToProvider = sendEmailAddressToProvider;
            this.isFlexible = isFlexible;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonObjectMember extends Object {
        public static final int CONSTRUCTOR = -1803309418;
        public String key;
        public JsonValue value;

        public JsonObjectMember() {
        }

        public JsonObjectMember(String key, JsonValue value) {
            this.key = key;
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueNull extends JsonValue {
        public static final int CONSTRUCTOR = -92872499;

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueBoolean extends JsonValue {
        public static final int CONSTRUCTOR = -2142186576;
        public boolean value;

        public JsonValueBoolean() {
        }

        public JsonValueBoolean(boolean value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueNumber extends JsonValue {
        public static final int CONSTRUCTOR = -1010822033;
        public double value;

        public JsonValueNumber() {
        }

        public JsonValueNumber(double value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueString extends JsonValue {
        public static final int CONSTRUCTOR = 1597947313;
        public String value;

        public JsonValueString() {
        }

        public JsonValueString(String value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueArray extends JsonValue {
        public static final int CONSTRUCTOR = -183913546;
        public JsonValue[] values;

        public JsonValueArray() {
        }

        public JsonValueArray(JsonValue[] values) {
            this.values = values;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JsonValueObject extends JsonValue {
        public static final int CONSTRUCTOR = 520252026;
        public JsonObjectMember[] members;

        public JsonValueObject() {
        }

        public JsonValueObject(JsonObjectMember[] members) {
            this.members = members;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.JsonValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class KeyboardButton extends Object {
        public static final int CONSTRUCTOR = -2069836172;
        public String text;
        public KeyboardButtonType type;

        public KeyboardButton() {
        }

        public KeyboardButton(String text, KeyboardButtonType type) {
            this.text = text;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class KeyboardButtonTypeText extends KeyboardButtonType {
        public static final int CONSTRUCTOR = -1773037256;

        @Override // org.drinkless.td.libcore.telegram.TdApi.KeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class KeyboardButtonTypeRequestPhoneNumber extends KeyboardButtonType {
        public static final int CONSTRUCTOR = -1529235527;

        @Override // org.drinkless.td.libcore.telegram.TdApi.KeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class KeyboardButtonTypeRequestLocation extends KeyboardButtonType {
        public static final int CONSTRUCTOR = -125661955;

        @Override // org.drinkless.td.libcore.telegram.TdApi.KeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class KeyboardButtonTypeRequestPoll extends KeyboardButtonType {
        public static final int CONSTRUCTOR = 1902435512;
        public boolean forceQuiz;
        public boolean forceRegular;

        public KeyboardButtonTypeRequestPoll() {
        }

        public KeyboardButtonTypeRequestPoll(boolean forceRegular, boolean forceQuiz) {
            this.forceRegular = forceRegular;
            this.forceQuiz = forceQuiz;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.KeyboardButtonType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LabeledPricePart extends Object {
        public static final int CONSTRUCTOR = 552789798;
        public long amount;
        public String label;

        public LabeledPricePart() {
        }

        public LabeledPricePart(String label, long amount) {
            this.label = label;
            this.amount = amount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackInfo extends Object {
        public static final int CONSTRUCTOR = 542199642;
        public String baseLanguagePackId;
        public String id;
        public boolean isBeta;
        public boolean isInstalled;
        public boolean isOfficial;
        public boolean isRtl;
        public int localStringCount;
        public String name;
        public String nativeName;
        public String pluralCode;
        public int totalStringCount;
        public int translatedStringCount;
        public String translationUrl;

        public LanguagePackInfo() {
        }

        public LanguagePackInfo(String id, String baseLanguagePackId, String name, String nativeName, String pluralCode, boolean isOfficial, boolean isRtl, boolean isBeta, boolean isInstalled, int totalStringCount, int translatedStringCount, int localStringCount, String translationUrl) {
            this.id = id;
            this.baseLanguagePackId = baseLanguagePackId;
            this.name = name;
            this.nativeName = nativeName;
            this.pluralCode = pluralCode;
            this.isOfficial = isOfficial;
            this.isRtl = isRtl;
            this.isBeta = isBeta;
            this.isInstalled = isInstalled;
            this.totalStringCount = totalStringCount;
            this.translatedStringCount = translatedStringCount;
            this.localStringCount = localStringCount;
            this.translationUrl = translationUrl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackString extends Object {
        public static final int CONSTRUCTOR = 1307632736;
        public String key;
        public LanguagePackStringValue value;

        public LanguagePackString() {
        }

        public LanguagePackString(String key, LanguagePackStringValue value) {
            this.key = key;
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackStringValueOrdinary extends LanguagePackStringValue {
        public static final int CONSTRUCTOR = -249256352;
        public String value;

        public LanguagePackStringValueOrdinary() {
        }

        public LanguagePackStringValueOrdinary(String value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.LanguagePackStringValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackStringValuePluralized extends LanguagePackStringValue {
        public static final int CONSTRUCTOR = 1906840261;
        public String fewValue;
        public String manyValue;
        public String oneValue;
        public String otherValue;
        public String twoValue;
        public String zeroValue;

        public LanguagePackStringValuePluralized() {
        }

        public LanguagePackStringValuePluralized(String zeroValue, String oneValue, String twoValue, String fewValue, String manyValue, String otherValue) {
            this.zeroValue = zeroValue;
            this.oneValue = oneValue;
            this.twoValue = twoValue;
            this.fewValue = fewValue;
            this.manyValue = manyValue;
            this.otherValue = otherValue;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.LanguagePackStringValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackStringValueDeleted extends LanguagePackStringValue {
        public static final int CONSTRUCTOR = 1834792698;

        @Override // org.drinkless.td.libcore.telegram.TdApi.LanguagePackStringValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LanguagePackStrings extends Object {
        public static final int CONSTRUCTOR = 1172082922;
        public LanguagePackString[] strings;

        public LanguagePackStrings() {
        }

        public LanguagePackStrings(LanguagePackString[] strings) {
            this.strings = strings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LocalFile extends Object {
        public static final int CONSTRUCTOR = -1166400317;
        public boolean canBeDeleted;
        public boolean canBeDownloaded;
        public int downloadOffset;
        public int downloadedPrefixSize;
        public int downloadedSize;
        public boolean isDownloadingActive;
        public boolean isDownloadingCompleted;
        public String path;

        public LocalFile() {
        }

        public LocalFile(String path, boolean canBeDownloaded, boolean canBeDeleted, boolean isDownloadingActive, boolean isDownloadingCompleted, int downloadOffset, int downloadedPrefixSize, int downloadedSize) {
            this.path = path;
            this.canBeDownloaded = canBeDownloaded;
            this.canBeDeleted = canBeDeleted;
            this.isDownloadingActive = isDownloadingActive;
            this.isDownloadingCompleted = isDownloadingCompleted;
            this.downloadOffset = downloadOffset;
            this.downloadedPrefixSize = downloadedPrefixSize;
            this.downloadedSize = downloadedSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LocalizationTargetInfo extends Object {
        public static final int CONSTRUCTOR = -2048670809;
        public LanguagePackInfo[] languagePacks;

        public LocalizationTargetInfo() {
        }

        public LocalizationTargetInfo(LanguagePackInfo[] languagePacks) {
            this.languagePacks = languagePacks;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Location extends Object {
        public static final int CONSTRUCTOR = 749028016;
        public double latitude;
        public double longitude;

        public Location() {
        }

        public Location(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogStreamDefault extends LogStream {
        public static final int CONSTRUCTOR = 1390581436;

        @Override // org.drinkless.td.libcore.telegram.TdApi.LogStream, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogStreamFile extends LogStream {
        public static final int CONSTRUCTOR = -1880085930;
        public long maxFileSize;
        public String path;

        public LogStreamFile() {
        }

        public LogStreamFile(String path, long maxFileSize) {
            this.path = path;
            this.maxFileSize = maxFileSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.LogStream, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogStreamEmpty extends LogStream {
        public static final int CONSTRUCTOR = -499912244;

        @Override // org.drinkless.td.libcore.telegram.TdApi.LogStream, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogTags extends Object {
        public static final int CONSTRUCTOR = -1604930601;
        public String[] tags;

        public LogTags() {
        }

        public LogTags(String[] tags) {
            this.tags = tags;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogVerbosityLevel extends Object {
        public static final int CONSTRUCTOR = 1734624234;
        public int verbosityLevel;

        public LogVerbosityLevel() {
        }

        public LogVerbosityLevel(int verbosityLevel) {
            this.verbosityLevel = verbosityLevel;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LoginUrlInfoOpen extends LoginUrlInfo {
        public static final int CONSTRUCTOR = -1079045420;
        public boolean skipConfirm;
        public String url;

        public LoginUrlInfoOpen() {
        }

        public LoginUrlInfoOpen(String url, boolean skipConfirm) {
            this.url = url;
            this.skipConfirm = skipConfirm;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.LoginUrlInfo, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LoginUrlInfoRequestConfirmation extends LoginUrlInfo {
        public static final int CONSTRUCTOR = -1761898342;
        public int botUserId;
        public String domain;
        public boolean requestWriteAccess;
        public String url;

        public LoginUrlInfoRequestConfirmation() {
        }

        public LoginUrlInfoRequestConfirmation(String url, String domain, int botUserId, boolean requestWriteAccess) {
            this.url = url;
            this.domain = domain;
            this.botUserId = botUserId;
            this.requestWriteAccess = requestWriteAccess;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.LoginUrlInfo, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MaskPointForehead extends MaskPoint {
        public static final int CONSTRUCTOR = 1027512005;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MaskPoint, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MaskPointEyes extends MaskPoint {
        public static final int CONSTRUCTOR = 1748310861;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MaskPoint, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MaskPointMouth extends MaskPoint {
        public static final int CONSTRUCTOR = 411773406;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MaskPoint, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MaskPointChin extends MaskPoint {
        public static final int CONSTRUCTOR = 534995335;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MaskPoint, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MaskPosition extends Object {
        public static final int CONSTRUCTOR = -2097433026;
        public MaskPoint point;
        public double scale;
        public double xShift;
        public double yShift;

        public MaskPosition() {
        }

        public MaskPosition(MaskPoint point, double xShift, double yShift, double scale) {
            this.point = point;
            this.xShift = xShift;
            this.yShift = yShift;
            this.scale = scale;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Message extends Object {
        public static final int CONSTRUCTOR = 1169109781;
        public String authorSignature;
        public boolean canBeDeletedForAllUsers;
        public boolean canBeDeletedOnlyForSelf;
        public boolean canBeEdited;
        public boolean canBeForwarded;
        public long chatId;
        public boolean containsUnreadMention;
        public MessageContent content;
        public int date;
        public int editDate;
        public MessageForwardInfo forwardInfo;
        public long id;
        public boolean isChannelPost;
        public boolean isOutgoing;
        public long mediaAlbumId;
        public ReplyMarkup replyMarkup;
        public long replyToMessageId;
        public String restrictionReason;
        public MessageSchedulingState schedulingState;
        public int senderUserId;
        public MessageSendingState sendingState;
        public int ttl;
        public double ttlExpiresIn;
        public int viaBotUserId;
        public int views;

        public Message() {
        }

        public Message(long id, int senderUserId, long chatId, MessageSendingState sendingState, MessageSchedulingState schedulingState, boolean isOutgoing, boolean canBeEdited, boolean canBeForwarded, boolean canBeDeletedOnlyForSelf, boolean canBeDeletedForAllUsers, boolean isChannelPost, boolean containsUnreadMention, int date, int editDate, MessageForwardInfo forwardInfo, long replyToMessageId, int ttl, double ttlExpiresIn, int viaBotUserId, String authorSignature, int views, long mediaAlbumId, String restrictionReason, MessageContent content, ReplyMarkup replyMarkup) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.chatId = chatId;
            this.sendingState = sendingState;
            this.schedulingState = schedulingState;
            this.isOutgoing = isOutgoing;
            this.canBeEdited = canBeEdited;
            this.canBeForwarded = canBeForwarded;
            this.canBeDeletedOnlyForSelf = canBeDeletedOnlyForSelf;
            this.canBeDeletedForAllUsers = canBeDeletedForAllUsers;
            this.isChannelPost = isChannelPost;
            this.containsUnreadMention = containsUnreadMention;
            this.date = date;
            this.editDate = editDate;
            this.forwardInfo = forwardInfo;
            this.replyToMessageId = replyToMessageId;
            this.ttl = ttl;
            this.ttlExpiresIn = ttlExpiresIn;
            this.viaBotUserId = viaBotUserId;
            this.authorSignature = authorSignature;
            this.views = views;
            this.mediaAlbumId = mediaAlbumId;
            this.restrictionReason = restrictionReason;
            this.content = content;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageText extends MessageContent {
        public static final int CONSTRUCTOR = 1989037971;
        public FormattedText text;
        public WebPage webPage;

        public MessageText() {
        }

        public MessageText(FormattedText text, WebPage webPage) {
            this.text = text;
            this.webPage = webPage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageAnimation extends MessageContent {
        public static final int CONSTRUCTOR = 1306939396;
        public Animation animation;
        public FormattedText caption;
        public boolean isSecret;

        public MessageAnimation() {
        }

        public MessageAnimation(Animation animation, FormattedText caption, boolean isSecret) {
            this.animation = animation;
            this.caption = caption;
            this.isSecret = isSecret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageAudio extends MessageContent {
        public static final int CONSTRUCTOR = 276722716;
        public Audio audio;
        public FormattedText caption;

        public MessageAudio() {
        }

        public MessageAudio(Audio audio, FormattedText caption) {
            this.audio = audio;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageDocument extends MessageContent {
        public static final int CONSTRUCTOR = 596945783;
        public FormattedText caption;
        public Document document;

        public MessageDocument() {
        }

        public MessageDocument(Document document, FormattedText caption) {
            this.document = document;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePhoto extends MessageContent {
        public static final int CONSTRUCTOR = -1851395174;
        public FormattedText caption;
        public boolean isSecret;
        public Photo photo;

        public MessagePhoto() {
        }

        public MessagePhoto(Photo photo, FormattedText caption, boolean isSecret) {
            this.photo = photo;
            this.caption = caption;
            this.isSecret = isSecret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageExpiredPhoto extends MessageContent {
        public static final int CONSTRUCTOR = -1404641801;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSticker extends MessageContent {
        public static final int CONSTRUCTOR = 1779022878;
        public Sticker sticker;

        public MessageSticker() {
        }

        public MessageSticker(Sticker sticker) {
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageVideo extends MessageContent {
        public static final int CONSTRUCTOR = 2021281344;
        public FormattedText caption;
        public boolean isSecret;
        public Video video;

        public MessageVideo() {
        }

        public MessageVideo(Video video, FormattedText caption, boolean isSecret) {
            this.video = video;
            this.caption = caption;
            this.isSecret = isSecret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageExpiredVideo extends MessageContent {
        public static final int CONSTRUCTOR = -1212209981;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageVideoNote extends MessageContent {
        public static final int CONSTRUCTOR = 963323014;
        public boolean isSecret;
        public boolean isViewed;
        public VideoNote videoNote;

        public MessageVideoNote() {
        }

        public MessageVideoNote(VideoNote videoNote, boolean isViewed, boolean isSecret) {
            this.videoNote = videoNote;
            this.isViewed = isViewed;
            this.isSecret = isSecret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageVoiceNote extends MessageContent {
        public static final int CONSTRUCTOR = 527777781;
        public FormattedText caption;
        public boolean isListened;
        public VoiceNote voiceNote;

        public MessageVoiceNote() {
        }

        public MessageVoiceNote(VoiceNote voiceNote, FormattedText caption, boolean isListened) {
            this.voiceNote = voiceNote;
            this.caption = caption;
            this.isListened = isListened;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageLocation extends MessageContent {
        public static final int CONSTRUCTOR = -1301887786;
        public int expiresIn;
        public int livePeriod;
        public Location location;

        public MessageLocation() {
        }

        public MessageLocation(Location location, int livePeriod, int expiresIn) {
            this.location = location;
            this.livePeriod = livePeriod;
            this.expiresIn = expiresIn;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageVenue extends MessageContent {
        public static final int CONSTRUCTOR = -2146492043;
        public Venue venue;

        public MessageVenue() {
        }

        public MessageVenue(Venue venue) {
            this.venue = venue;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageContact extends MessageContent {
        public static final int CONSTRUCTOR = -512684966;
        public Contact contact;

        public MessageContact() {
        }

        public MessageContact(Contact contact) {
            this.contact = contact;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageGame extends MessageContent {
        public static final int CONSTRUCTOR = -69441162;
        public Game game;

        public MessageGame() {
        }

        public MessageGame(Game game) {
            this.game = game;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePoll extends MessageContent {
        public static final int CONSTRUCTOR = -662130099;
        public Poll poll;

        public MessagePoll() {
        }

        public MessagePoll(Poll poll) {
            this.poll = poll;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageInvoice extends MessageContent {
        public static final int CONSTRUCTOR = -1916671476;
        public String currency;
        public String description;
        public boolean isTest;
        public boolean needShippingAddress;
        public Photo photo;
        public long receiptMessageId;
        public String startParameter;
        public String title;
        public long totalAmount;

        public MessageInvoice() {
        }

        public MessageInvoice(String title, String description, Photo photo, String currency, long totalAmount, String startParameter, boolean isTest, boolean needShippingAddress, long receiptMessageId) {
            this.title = title;
            this.description = description;
            this.photo = photo;
            this.currency = currency;
            this.totalAmount = totalAmount;
            this.startParameter = startParameter;
            this.isTest = isTest;
            this.needShippingAddress = needShippingAddress;
            this.receiptMessageId = receiptMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageCall extends MessageContent {
        public static final int CONSTRUCTOR = 366512596;
        public CallDiscardReason discardReason;
        public int duration;

        public MessageCall() {
        }

        public MessageCall(CallDiscardReason discardReason, int duration) {
            this.discardReason = discardReason;
            this.duration = duration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageBasicGroupChatCreate extends MessageContent {
        public static final int CONSTRUCTOR = 1575377646;
        public int[] memberUserIds;
        public String title;

        public MessageBasicGroupChatCreate() {
        }

        public MessageBasicGroupChatCreate(String title, int[] memberUserIds) {
            this.title = title;
            this.memberUserIds = memberUserIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSupergroupChatCreate extends MessageContent {
        public static final int CONSTRUCTOR = -434325733;
        public String title;

        public MessageSupergroupChatCreate() {
        }

        public MessageSupergroupChatCreate(String title) {
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatChangeTitle extends MessageContent {
        public static final int CONSTRUCTOR = 748272449;
        public String title;

        public MessageChatChangeTitle() {
        }

        public MessageChatChangeTitle(String title) {
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatChangePhoto extends MessageContent {
        public static final int CONSTRUCTOR = 319630249;
        public Photo photo;

        public MessageChatChangePhoto() {
        }

        public MessageChatChangePhoto(Photo photo) {
            this.photo = photo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatDeletePhoto extends MessageContent {
        public static final int CONSTRUCTOR = -184374809;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatAddMembers extends MessageContent {
        public static final int CONSTRUCTOR = 401228326;
        public int[] memberUserIds;

        public MessageChatAddMembers() {
        }

        public MessageChatAddMembers(int[] memberUserIds) {
            this.memberUserIds = memberUserIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatJoinByLink extends MessageContent {
        public static final int CONSTRUCTOR = 1846493311;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatDeleteMember extends MessageContent {
        public static final int CONSTRUCTOR = 1164414043;
        public int userId;

        public MessageChatDeleteMember() {
        }

        public MessageChatDeleteMember(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatUpgradeTo extends MessageContent {
        public static final int CONSTRUCTOR = 1957816681;
        public int supergroupId;

        public MessageChatUpgradeTo() {
        }

        public MessageChatUpgradeTo(int supergroupId) {
            this.supergroupId = supergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatUpgradeFrom extends MessageContent {
        public static final int CONSTRUCTOR = 1642272558;
        public int basicGroupId;
        public String title;

        public MessageChatUpgradeFrom() {
        }

        public MessageChatUpgradeFrom(String title, int basicGroupId) {
            this.title = title;
            this.basicGroupId = basicGroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePinMessage extends MessageContent {
        public static final int CONSTRUCTOR = 953503801;
        public long messageId;

        public MessagePinMessage() {
        }

        public MessagePinMessage(long messageId) {
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageScreenshotTaken extends MessageContent {
        public static final int CONSTRUCTOR = -1564971605;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageChatSetTtl extends MessageContent {
        public static final int CONSTRUCTOR = 1810060209;
        public int ttl;

        public MessageChatSetTtl() {
        }

        public MessageChatSetTtl(int ttl) {
            this.ttl = ttl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageCustomServiceAction extends MessageContent {
        public static final int CONSTRUCTOR = 1435879282;
        public String text;

        public MessageCustomServiceAction() {
        }

        public MessageCustomServiceAction(String text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageGameScore extends MessageContent {
        public static final int CONSTRUCTOR = 1344904575;
        public long gameId;
        public long gameMessageId;
        public int score;

        public MessageGameScore() {
        }

        public MessageGameScore(long gameMessageId, long gameId, int score) {
            this.gameMessageId = gameMessageId;
            this.gameId = gameId;
            this.score = score;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePaymentSuccessful extends MessageContent {
        public static final int CONSTRUCTOR = -595962993;
        public String currency;
        public long invoiceMessageId;
        public long totalAmount;

        public MessagePaymentSuccessful() {
        }

        public MessagePaymentSuccessful(long invoiceMessageId, String currency, long totalAmount) {
            this.invoiceMessageId = invoiceMessageId;
            this.currency = currency;
            this.totalAmount = totalAmount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePaymentSuccessfulBot extends MessageContent {
        public static final int CONSTRUCTOR = -412310696;
        public String currency;
        public long invoiceMessageId;
        public byte[] invoicePayload;
        public OrderInfo orderInfo;
        public String providerPaymentChargeId;
        public String shippingOptionId;
        public String telegramPaymentChargeId;
        public long totalAmount;

        public MessagePaymentSuccessfulBot() {
        }

        public MessagePaymentSuccessfulBot(long invoiceMessageId, String currency, long totalAmount, byte[] invoicePayload, String shippingOptionId, OrderInfo orderInfo, String telegramPaymentChargeId, String providerPaymentChargeId) {
            this.invoiceMessageId = invoiceMessageId;
            this.currency = currency;
            this.totalAmount = totalAmount;
            this.invoicePayload = invoicePayload;
            this.shippingOptionId = shippingOptionId;
            this.orderInfo = orderInfo;
            this.telegramPaymentChargeId = telegramPaymentChargeId;
            this.providerPaymentChargeId = providerPaymentChargeId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageContactRegistered extends MessageContent {
        public static final int CONSTRUCTOR = -1502020353;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageWebsiteConnected extends MessageContent {
        public static final int CONSTRUCTOR = -1074551800;
        public String domainName;

        public MessageWebsiteConnected() {
        }

        public MessageWebsiteConnected(String domainName) {
            this.domainName = domainName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePassportDataSent extends MessageContent {
        public static final int CONSTRUCTOR = 1017405171;
        public PassportElementType[] types;

        public MessagePassportDataSent() {
        }

        public MessagePassportDataSent(PassportElementType[] types) {
            this.types = types;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessagePassportDataReceived extends MessageContent {
        public static final int CONSTRUCTOR = -1367863624;
        public EncryptedCredentials credentials;
        public EncryptedPassportElement[] elements;

        public MessagePassportDataReceived() {
        }

        public MessagePassportDataReceived(EncryptedPassportElement[] elements, EncryptedCredentials credentials) {
            this.elements = elements;
            this.credentials = credentials;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageUnsupported extends MessageContent {
        public static final int CONSTRUCTOR = -1816726139;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageForwardInfo extends Object {
        public static final int CONSTRUCTOR = -1622371186;
        public int date;
        public long fromChatId;
        public long fromMessageId;
        public MessageForwardOrigin origin;

        public MessageForwardInfo() {
        }

        public MessageForwardInfo(MessageForwardOrigin origin, int date, long fromChatId, long fromMessageId) {
            this.origin = origin;
            this.date = date;
            this.fromChatId = fromChatId;
            this.fromMessageId = fromMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageForwardOriginUser extends MessageForwardOrigin {
        public static final int CONSTRUCTOR = 2781520;
        public int senderUserId;

        public MessageForwardOriginUser() {
        }

        public MessageForwardOriginUser(int senderUserId) {
            this.senderUserId = senderUserId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageForwardOrigin, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageForwardOriginHiddenUser extends MessageForwardOrigin {
        public static final int CONSTRUCTOR = -271257885;
        public String senderName;

        public MessageForwardOriginHiddenUser() {
        }

        public MessageForwardOriginHiddenUser(String senderName) {
            this.senderName = senderName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageForwardOrigin, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageForwardOriginChannel extends MessageForwardOrigin {
        public static final int CONSTRUCTOR = 1490730723;
        public String authorSignature;
        public long chatId;
        public long messageId;

        public MessageForwardOriginChannel() {
        }

        public MessageForwardOriginChannel(long chatId, long messageId, String authorSignature) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.authorSignature = authorSignature;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageForwardOrigin, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageLinkInfo extends Object {
        public static final int CONSTRUCTOR = 657372995;
        public long chatId;
        public boolean forAlbum;
        public boolean isPublic;
        public Message message;

        public MessageLinkInfo() {
        }

        public MessageLinkInfo(boolean isPublic, long chatId, Message message, boolean forAlbum) {
            this.isPublic = isPublic;
            this.chatId = chatId;
            this.message = message;
            this.forAlbum = forAlbum;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSchedulingStateSendAtDate extends MessageSchedulingState {
        public static final int CONSTRUCTOR = -1485570073;
        public int sendDate;

        public MessageSchedulingStateSendAtDate() {
        }

        public MessageSchedulingStateSendAtDate(int sendDate) {
            this.sendDate = sendDate;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageSchedulingState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSchedulingStateSendWhenOnline extends MessageSchedulingState {
        public static final int CONSTRUCTOR = 2092947464;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageSchedulingState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSendingStatePending extends MessageSendingState {
        public static final int CONSTRUCTOR = -1381803582;

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageSendingState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class MessageSendingStateFailed extends MessageSendingState {
        public static final int CONSTRUCTOR = 2054476087;
        public boolean canRetry;
        public int errorCode;
        public String errorMessage;
        public double retryAfter;

        public MessageSendingStateFailed() {
        }

        public MessageSendingStateFailed(int errorCode, String errorMessage, boolean canRetry, double retryAfter) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.canRetry = canRetry;
            this.retryAfter = retryAfter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.MessageSendingState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Messages extends Object {
        public static final int CONSTRUCTOR = -16498159;
        public Message[] messages;
        public int totalCount;

        public Messages() {
        }

        public Messages(int totalCount, Message[] messages) {
            this.totalCount = totalCount;
            this.messages = messages;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Minithumbnail extends Object {
        public static final int CONSTRUCTOR = -328540758;
        public byte[] data;
        public int height;
        public int width;

        public Minithumbnail() {
        }

        public Minithumbnail(int width, int height, byte[] data) {
            this.width = width;
            this.height = height;
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkStatistics extends Object {
        public static final int CONSTRUCTOR = 1615554212;
        public NetworkStatisticsEntry[] entries;
        public int sinceDate;

        public NetworkStatistics() {
        }

        public NetworkStatistics(int sinceDate, NetworkStatisticsEntry[] entries) {
            this.sinceDate = sinceDate;
            this.entries = entries;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkStatisticsEntryFile extends NetworkStatisticsEntry {
        public static final int CONSTRUCTOR = 188452706;
        public FileType fileType;
        public NetworkType networkType;
        public long receivedBytes;
        public long sentBytes;

        public NetworkStatisticsEntryFile() {
        }

        public NetworkStatisticsEntryFile(FileType fileType, NetworkType networkType, long sentBytes, long receivedBytes) {
            this.fileType = fileType;
            this.networkType = networkType;
            this.sentBytes = sentBytes;
            this.receivedBytes = receivedBytes;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkStatisticsEntry, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkStatisticsEntryCall extends NetworkStatisticsEntry {
        public static final int CONSTRUCTOR = 737000365;
        public double duration;
        public NetworkType networkType;
        public long receivedBytes;
        public long sentBytes;

        public NetworkStatisticsEntryCall() {
        }

        public NetworkStatisticsEntryCall(NetworkType networkType, long sentBytes, long receivedBytes, double duration) {
            this.networkType = networkType;
            this.sentBytes = sentBytes;
            this.receivedBytes = receivedBytes;
            this.duration = duration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkStatisticsEntry, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkTypeNone extends NetworkType {
        public static final int CONSTRUCTOR = -1971691759;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkTypeMobile extends NetworkType {
        public static final int CONSTRUCTOR = 819228239;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkTypeMobileRoaming extends NetworkType {
        public static final int CONSTRUCTOR = -1435199760;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkTypeWiFi extends NetworkType {
        public static final int CONSTRUCTOR = -633872070;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NetworkTypeOther extends NetworkType {
        public static final int CONSTRUCTOR = 1942128539;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NetworkType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Notification extends Object {
        public static final int CONSTRUCTOR = 788743120;
        public int date;
        public int id;
        public boolean isSilent;
        public NotificationType type;

        public Notification() {
        }

        public Notification(int id, int date, boolean isSilent, NotificationType type) {
            this.id = id;
            this.date = date;
            this.isSilent = isSilent;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationGroup extends Object {
        public static final int CONSTRUCTOR = 780691541;
        public long chatId;
        public int id;
        public Notification[] notifications;
        public int totalCount;
        public NotificationGroupType type;

        public NotificationGroup() {
        }

        public NotificationGroup(int id, NotificationGroupType type, long chatId, int totalCount, Notification[] notifications) {
            this.id = id;
            this.type = type;
            this.chatId = chatId;
            this.totalCount = totalCount;
            this.notifications = notifications;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationGroupTypeMessages extends NotificationGroupType {
        public static final int CONSTRUCTOR = -1702481123;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationGroupType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationGroupTypeMentions extends NotificationGroupType {
        public static final int CONSTRUCTOR = -2050324051;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationGroupType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationGroupTypeSecretChat extends NotificationGroupType {
        public static final int CONSTRUCTOR = 1390759476;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationGroupType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationGroupTypeCalls extends NotificationGroupType {
        public static final int CONSTRUCTOR = 1379123538;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationGroupType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationSettingsScopePrivateChats extends NotificationSettingsScope {
        public static final int CONSTRUCTOR = 937446759;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationSettingsScope, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationSettingsScopeGroupChats extends NotificationSettingsScope {
        public static final int CONSTRUCTOR = 1212142067;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationSettingsScope, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationSettingsScopeChannelChats extends NotificationSettingsScope {
        public static final int CONSTRUCTOR = 548013448;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationSettingsScope, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationTypeNewMessage extends NotificationType {
        public static final int CONSTRUCTOR = 1885935159;
        public Message message;

        public NotificationTypeNewMessage() {
        }

        public NotificationTypeNewMessage(Message message) {
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationTypeNewSecretChat extends NotificationType {
        public static final int CONSTRUCTOR = 1198638768;

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationTypeNewCall extends NotificationType {
        public static final int CONSTRUCTOR = 1712734585;
        public int callId;

        public NotificationTypeNewCall() {
        }

        public NotificationTypeNewCall(int callId) {
            this.callId = callId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class NotificationTypeNewPushMessage extends NotificationType {
        public static final int CONSTRUCTOR = 1167232404;
        public PushMessageContent content;
        public long messageId;
        public int senderUserId;

        public NotificationTypeNewPushMessage() {
        }

        public NotificationTypeNewPushMessage(long messageId, int senderUserId, PushMessageContent content) {
            this.messageId = messageId;
            this.senderUserId = senderUserId;
            this.content = content;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.NotificationType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Ok extends Object {
        public static final int CONSTRUCTOR = -722616727;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OptionValueBoolean extends OptionValue {
        public static final int CONSTRUCTOR = 63135518;
        public boolean value;

        public OptionValueBoolean() {
        }

        public OptionValueBoolean(boolean value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.OptionValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OptionValueEmpty extends OptionValue {
        public static final int CONSTRUCTOR = 918955155;

        @Override // org.drinkless.td.libcore.telegram.TdApi.OptionValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OptionValueInteger extends OptionValue {
        public static final int CONSTRUCTOR = -1400911104;
        public int value;

        public OptionValueInteger() {
        }

        public OptionValueInteger(int value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.OptionValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OptionValueString extends OptionValue {
        public static final int CONSTRUCTOR = 756248212;
        public String value;

        public OptionValueString() {
        }

        public OptionValueString(String value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.OptionValue, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OrderInfo extends Object {
        public static final int CONSTRUCTOR = 783997294;
        public String emailAddress;
        public String name;
        public String phoneNumber;
        public Address shippingAddress;

        public OrderInfo() {
        }

        public OrderInfo(String name, String phoneNumber, String emailAddress, Address shippingAddress) {
            this.name = name;
            this.phoneNumber = phoneNumber;
            this.emailAddress = emailAddress;
            this.shippingAddress = shippingAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockTitle extends PageBlock {
        public static final int CONSTRUCTOR = 1629664784;
        public RichText title;

        public PageBlockTitle() {
        }

        public PageBlockTitle(RichText title) {
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockSubtitle extends PageBlock {
        public static final int CONSTRUCTOR = 264524263;
        public RichText subtitle;

        public PageBlockSubtitle() {
        }

        public PageBlockSubtitle(RichText subtitle) {
            this.subtitle = subtitle;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockAuthorDate extends PageBlock {
        public static final int CONSTRUCTOR = 1300231184;
        public RichText author;
        public int publishDate;

        public PageBlockAuthorDate() {
        }

        public PageBlockAuthorDate(RichText author, int publishDate) {
            this.author = author;
            this.publishDate = publishDate;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockHeader extends PageBlock {
        public static final int CONSTRUCTOR = 1402854811;
        public RichText header;

        public PageBlockHeader() {
        }

        public PageBlockHeader(RichText header) {
            this.header = header;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockSubheader extends PageBlock {
        public static final int CONSTRUCTOR = 1263956774;
        public RichText subheader;

        public PageBlockSubheader() {
        }

        public PageBlockSubheader(RichText subheader) {
            this.subheader = subheader;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockKicker extends PageBlock {
        public static final int CONSTRUCTOR = 1361282635;
        public RichText kicker;

        public PageBlockKicker() {
        }

        public PageBlockKicker(RichText kicker) {
            this.kicker = kicker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockParagraph extends PageBlock {
        public static final int CONSTRUCTOR = 1182402406;
        public RichText text;

        public PageBlockParagraph() {
        }

        public PageBlockParagraph(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockPreformatted extends PageBlock {
        public static final int CONSTRUCTOR = -1066346178;
        public String language;
        public RichText text;

        public PageBlockPreformatted() {
        }

        public PageBlockPreformatted(RichText text, String language) {
            this.text = text;
            this.language = language;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockFooter extends PageBlock {
        public static final int CONSTRUCTOR = 886429480;
        public RichText footer;

        public PageBlockFooter() {
        }

        public PageBlockFooter(RichText footer) {
            this.footer = footer;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockDivider extends PageBlock {
        public static final int CONSTRUCTOR = -618614392;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockAnchor extends PageBlock {
        public static final int CONSTRUCTOR = -837994576;
        public String name;

        public PageBlockAnchor() {
        }

        public PageBlockAnchor(String name) {
            this.name = name;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockList extends PageBlock {
        public static final int CONSTRUCTOR = -1037074852;
        public PageBlockListItem[] items;

        public PageBlockList() {
        }

        public PageBlockList(PageBlockListItem[] items) {
            this.items = items;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockBlockQuote extends PageBlock {
        public static final int CONSTRUCTOR = 1657834142;
        public RichText credit;
        public RichText text;

        public PageBlockBlockQuote() {
        }

        public PageBlockBlockQuote(RichText text, RichText credit) {
            this.text = text;
            this.credit = credit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockPullQuote extends PageBlock {
        public static final int CONSTRUCTOR = 490242317;
        public RichText credit;
        public RichText text;

        public PageBlockPullQuote() {
        }

        public PageBlockPullQuote(RichText text, RichText credit) {
            this.text = text;
            this.credit = credit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockAnimation extends PageBlock {
        public static final int CONSTRUCTOR = 1355669513;
        public Animation animation;
        public PageBlockCaption caption;
        public boolean needAutoplay;

        public PageBlockAnimation() {
        }

        public PageBlockAnimation(Animation animation, PageBlockCaption caption, boolean needAutoplay) {
            this.animation = animation;
            this.caption = caption;
            this.needAutoplay = needAutoplay;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockAudio extends PageBlock {
        public static final int CONSTRUCTOR = -63371245;
        public Audio audio;
        public PageBlockCaption caption;

        public PageBlockAudio() {
        }

        public PageBlockAudio(Audio audio, PageBlockCaption caption) {
            this.audio = audio;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockPhoto extends PageBlock {
        public static final int CONSTRUCTOR = 417601156;
        public PageBlockCaption caption;
        public Photo photo;
        public String url;

        public PageBlockPhoto() {
        }

        public PageBlockPhoto(Photo photo, PageBlockCaption caption, String url) {
            this.photo = photo;
            this.caption = caption;
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockVideo extends PageBlock {
        public static final int CONSTRUCTOR = 510041394;
        public PageBlockCaption caption;
        public boolean isLooped;
        public boolean needAutoplay;
        public Video video;

        public PageBlockVideo() {
        }

        public PageBlockVideo(Video video, PageBlockCaption caption, boolean needAutoplay, boolean isLooped) {
            this.video = video;
            this.caption = caption;
            this.needAutoplay = needAutoplay;
            this.isLooped = isLooped;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockVoiceNote extends PageBlock {
        public static final int CONSTRUCTOR = 1823310463;
        public PageBlockCaption caption;
        public VoiceNote voiceNote;

        public PageBlockVoiceNote() {
        }

        public PageBlockVoiceNote(VoiceNote voiceNote, PageBlockCaption caption) {
            this.voiceNote = voiceNote;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockCover extends PageBlock {
        public static final int CONSTRUCTOR = 972174080;
        public PageBlock cover;

        public PageBlockCover() {
        }

        public PageBlockCover(PageBlock cover) {
            this.cover = cover;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockEmbedded extends PageBlock {
        public static final int CONSTRUCTOR = -1942577763;
        public boolean allowScrolling;
        public PageBlockCaption caption;
        public int height;
        public String html;
        public boolean isFullWidth;
        public Photo posterPhoto;
        public String url;
        public int width;

        public PageBlockEmbedded() {
        }

        public PageBlockEmbedded(String url, String html, Photo posterPhoto, int width, int height, PageBlockCaption caption, boolean isFullWidth, boolean allowScrolling) {
            this.url = url;
            this.html = html;
            this.posterPhoto = posterPhoto;
            this.width = width;
            this.height = height;
            this.caption = caption;
            this.isFullWidth = isFullWidth;
            this.allowScrolling = allowScrolling;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockEmbeddedPost extends PageBlock {
        public static final int CONSTRUCTOR = 397600949;
        public String author;
        public Photo authorPhoto;
        public PageBlockCaption caption;
        public int date;
        public PageBlock[] pageBlocks;
        public String url;

        public PageBlockEmbeddedPost() {
        }

        public PageBlockEmbeddedPost(String url, String author, Photo authorPhoto, int date, PageBlock[] pageBlocks, PageBlockCaption caption) {
            this.url = url;
            this.author = author;
            this.authorPhoto = authorPhoto;
            this.date = date;
            this.pageBlocks = pageBlocks;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockCollage extends PageBlock {
        public static final int CONSTRUCTOR = 1163760110;
        public PageBlockCaption caption;
        public PageBlock[] pageBlocks;

        public PageBlockCollage() {
        }

        public PageBlockCollage(PageBlock[] pageBlocks, PageBlockCaption caption) {
            this.pageBlocks = pageBlocks;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockSlideshow extends PageBlock {
        public static final int CONSTRUCTOR = 539217375;
        public PageBlockCaption caption;
        public PageBlock[] pageBlocks;

        public PageBlockSlideshow() {
        }

        public PageBlockSlideshow(PageBlock[] pageBlocks, PageBlockCaption caption) {
            this.pageBlocks = pageBlocks;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockChatLink extends PageBlock {
        public static final int CONSTRUCTOR = 214606645;
        public ChatPhoto photo;
        public String title;
        public String username;

        public PageBlockChatLink() {
        }

        public PageBlockChatLink(String title, ChatPhoto photo, String username) {
            this.title = title;
            this.photo = photo;
            this.username = username;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockTable extends PageBlock {
        public static final int CONSTRUCTOR = -942649288;
        public RichText caption;
        public PageBlockTableCell[][] cells;
        public boolean isBordered;
        public boolean isStriped;

        public PageBlockTable() {
        }

        public PageBlockTable(RichText caption, PageBlockTableCell[][] cells, boolean isBordered, boolean isStriped) {
            this.caption = caption;
            this.cells = cells;
            this.isBordered = isBordered;
            this.isStriped = isStriped;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockDetails extends PageBlock {
        public static final int CONSTRUCTOR = -1599869809;
        public RichText header;
        public boolean isOpen;
        public PageBlock[] pageBlocks;

        public PageBlockDetails() {
        }

        public PageBlockDetails(RichText header, PageBlock[] pageBlocks, boolean isOpen) {
            this.header = header;
            this.pageBlocks = pageBlocks;
            this.isOpen = isOpen;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockRelatedArticles extends PageBlock {
        public static final int CONSTRUCTOR = -1807324374;
        public PageBlockRelatedArticle[] articles;
        public RichText header;

        public PageBlockRelatedArticles() {
        }

        public PageBlockRelatedArticles(RichText header, PageBlockRelatedArticle[] articles) {
            this.header = header;
            this.articles = articles;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockMap extends PageBlock {
        public static final int CONSTRUCTOR = 1510961171;
        public PageBlockCaption caption;
        public int height;
        public Location location;
        public int width;
        public int zoom;

        public PageBlockMap() {
        }

        public PageBlockMap(Location location, int zoom, int width, int height, PageBlockCaption caption) {
            this.location = location;
            this.zoom = zoom;
            this.width = width;
            this.height = height;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlock, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockCaption extends Object {
        public static final int CONSTRUCTOR = -1180064650;
        public RichText credit;
        public RichText text;

        public PageBlockCaption() {
        }

        public PageBlockCaption(RichText text, RichText credit) {
            this.text = text;
            this.credit = credit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockHorizontalAlignmentLeft extends PageBlockHorizontalAlignment {
        public static final int CONSTRUCTOR = 848701417;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockHorizontalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockHorizontalAlignmentCenter extends PageBlockHorizontalAlignment {
        public static final int CONSTRUCTOR = -1009203990;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockHorizontalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockHorizontalAlignmentRight extends PageBlockHorizontalAlignment {
        public static final int CONSTRUCTOR = 1371369214;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockHorizontalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockListItem extends Object {
        public static final int CONSTRUCTOR = 323186259;
        public String label;
        public PageBlock[] pageBlocks;

        public PageBlockListItem() {
        }

        public PageBlockListItem(String label, PageBlock[] pageBlocks) {
            this.label = label;
            this.pageBlocks = pageBlocks;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockRelatedArticle extends Object {
        public static final int CONSTRUCTOR = 481199251;
        public String author;
        public String description;
        public Photo photo;
        public int publishDate;
        public String title;
        public String url;

        public PageBlockRelatedArticle() {
        }

        public PageBlockRelatedArticle(String url, String title, String description, Photo photo, String author, int publishDate) {
            this.url = url;
            this.title = title;
            this.description = description;
            this.photo = photo;
            this.author = author;
            this.publishDate = publishDate;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockTableCell extends Object {
        public static final int CONSTRUCTOR = 1417658214;
        public PageBlockHorizontalAlignment align;
        public int colspan;
        public boolean isHeader;
        public int rowspan;
        public RichText text;
        public PageBlockVerticalAlignment valign;

        public PageBlockTableCell() {
        }

        public PageBlockTableCell(RichText text, boolean isHeader, int colspan, int rowspan, PageBlockHorizontalAlignment align, PageBlockVerticalAlignment valign) {
            this.text = text;
            this.isHeader = isHeader;
            this.colspan = colspan;
            this.rowspan = rowspan;
            this.align = align;
            this.valign = valign;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockVerticalAlignmentTop extends PageBlockVerticalAlignment {
        public static final int CONSTRUCTOR = 195500454;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockVerticalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockVerticalAlignmentMiddle extends PageBlockVerticalAlignment {
        public static final int CONSTRUCTOR = -2123096587;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockVerticalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PageBlockVerticalAlignmentBottom extends PageBlockVerticalAlignment {
        public static final int CONSTRUCTOR = 2092531158;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PageBlockVerticalAlignment, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportAuthorizationForm extends Object {
        public static final int CONSTRUCTOR = -1070673218;
        public int id;
        public String privacyPolicyUrl;
        public PassportRequiredElement[] requiredElements;

        public PassportAuthorizationForm() {
        }

        public PassportAuthorizationForm(int id, PassportRequiredElement[] requiredElements, String privacyPolicyUrl) {
            this.id = id;
            this.requiredElements = requiredElements;
            this.privacyPolicyUrl = privacyPolicyUrl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementPersonalDetails extends PassportElement {
        public static final int CONSTRUCTOR = 1217724035;
        public PersonalDetails personalDetails;

        public PassportElementPersonalDetails() {
        }

        public PassportElementPersonalDetails(PersonalDetails personalDetails) {
            this.personalDetails = personalDetails;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementPassport extends PassportElement {
        public static final int CONSTRUCTOR = -263985373;
        public IdentityDocument passport;

        public PassportElementPassport() {
        }

        public PassportElementPassport(IdentityDocument passport) {
            this.passport = passport;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementDriverLicense extends PassportElement {
        public static final int CONSTRUCTOR = 1643580589;
        public IdentityDocument driverLicense;

        public PassportElementDriverLicense() {
        }

        public PassportElementDriverLicense(IdentityDocument driverLicense) {
            this.driverLicense = driverLicense;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementIdentityCard extends PassportElement {
        public static final int CONSTRUCTOR = 2083775797;
        public IdentityDocument identityCard;

        public PassportElementIdentityCard() {
        }

        public PassportElementIdentityCard(IdentityDocument identityCard) {
            this.identityCard = identityCard;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementInternalPassport extends PassportElement {
        public static final int CONSTRUCTOR = 36220295;
        public IdentityDocument internalPassport;

        public PassportElementInternalPassport() {
        }

        public PassportElementInternalPassport(IdentityDocument internalPassport) {
            this.internalPassport = internalPassport;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementAddress extends PassportElement {
        public static final int CONSTRUCTOR = -782625232;
        public Address address;

        public PassportElementAddress() {
        }

        public PassportElementAddress(Address address) {
            this.address = address;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementUtilityBill extends PassportElement {
        public static final int CONSTRUCTOR = -234611246;
        public PersonalDocument utilityBill;

        public PassportElementUtilityBill() {
        }

        public PassportElementUtilityBill(PersonalDocument utilityBill) {
            this.utilityBill = utilityBill;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementBankStatement extends PassportElement {
        public static final int CONSTRUCTOR = -366464408;
        public PersonalDocument bankStatement;

        public PassportElementBankStatement() {
        }

        public PassportElementBankStatement(PersonalDocument bankStatement) {
            this.bankStatement = bankStatement;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementRentalAgreement extends PassportElement {
        public static final int CONSTRUCTOR = -290141400;
        public PersonalDocument rentalAgreement;

        public PassportElementRentalAgreement() {
        }

        public PassportElementRentalAgreement(PersonalDocument rentalAgreement) {
            this.rentalAgreement = rentalAgreement;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementPassportRegistration extends PassportElement {
        public static final int CONSTRUCTOR = 618323071;
        public PersonalDocument passportRegistration;

        public PassportElementPassportRegistration() {
        }

        public PassportElementPassportRegistration(PersonalDocument passportRegistration) {
            this.passportRegistration = passportRegistration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTemporaryRegistration extends PassportElement {
        public static final int CONSTRUCTOR = 1237626864;
        public PersonalDocument temporaryRegistration;

        public PassportElementTemporaryRegistration() {
        }

        public PassportElementTemporaryRegistration(PersonalDocument temporaryRegistration) {
            this.temporaryRegistration = temporaryRegistration;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementPhoneNumber extends PassportElement {
        public static final int CONSTRUCTOR = -1320118375;
        public String phoneNumber;

        public PassportElementPhoneNumber() {
        }

        public PassportElementPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementEmailAddress extends PassportElement {
        public static final int CONSTRUCTOR = -1528129531;
        public String emailAddress;

        public PassportElementEmailAddress() {
        }

        public PassportElementEmailAddress(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElement, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementError extends Object {
        public static final int CONSTRUCTOR = -1861902395;
        public String message;
        public PassportElementErrorSource source;
        public PassportElementType type;

        public PassportElementError() {
        }

        public PassportElementError(PassportElementType type, String message, PassportElementErrorSource source) {
            this.type = type;
            this.message = message;
            this.source = source;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceUnspecified extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = -378320830;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceDataField extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = -308650776;
        public String fieldName;

        public PassportElementErrorSourceDataField() {
        }

        public PassportElementErrorSourceDataField(String fieldName) {
            this.fieldName = fieldName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceFrontSide extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = 1895658292;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceReverseSide extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = 1918630391;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceSelfie extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = -797043672;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceTranslationFile extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = -689621228;
        public int fileIndex;

        public PassportElementErrorSourceTranslationFile() {
        }

        public PassportElementErrorSourceTranslationFile(int fileIndex) {
            this.fileIndex = fileIndex;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceTranslationFiles extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = 581280796;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceFile extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = 2020358960;
        public int fileIndex;

        public PassportElementErrorSourceFile() {
        }

        public PassportElementErrorSourceFile(int fileIndex) {
            this.fileIndex = fileIndex;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementErrorSourceFiles extends PassportElementErrorSource {
        public static final int CONSTRUCTOR = 1894164178;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementErrorSource, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypePersonalDetails extends PassportElementType {
        public static final int CONSTRUCTOR = -1032136365;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypePassport extends PassportElementType {
        public static final int CONSTRUCTOR = -436360376;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeDriverLicense extends PassportElementType {
        public static final int CONSTRUCTOR = 1827298379;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeIdentityCard extends PassportElementType {
        public static final int CONSTRUCTOR = -502356132;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeInternalPassport extends PassportElementType {
        public static final int CONSTRUCTOR = -793781959;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeAddress extends PassportElementType {
        public static final int CONSTRUCTOR = 496327874;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeUtilityBill extends PassportElementType {
        public static final int CONSTRUCTOR = 627084906;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeBankStatement extends PassportElementType {
        public static final int CONSTRUCTOR = 574095667;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeRentalAgreement extends PassportElementType {
        public static final int CONSTRUCTOR = -2060583280;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypePassportRegistration extends PassportElementType {
        public static final int CONSTRUCTOR = -159478209;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeTemporaryRegistration extends PassportElementType {
        public static final int CONSTRUCTOR = 1092498527;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypePhoneNumber extends PassportElementType {
        public static final int CONSTRUCTOR = -995361172;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementTypeEmailAddress extends PassportElementType {
        public static final int CONSTRUCTOR = -79321405;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PassportElementType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElements extends Object {
        public static final int CONSTRUCTOR = 1264617556;
        public PassportElement[] elements;

        public PassportElements() {
        }

        public PassportElements(PassportElement[] elements) {
            this.elements = elements;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportElementsWithErrors extends Object {
        public static final int CONSTRUCTOR = 1308923044;
        public PassportElement[] elements;
        public PassportElementError[] errors;

        public PassportElementsWithErrors() {
        }

        public PassportElementsWithErrors(PassportElement[] elements, PassportElementError[] errors) {
            this.elements = elements;
            this.errors = errors;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportRequiredElement extends Object {
        public static final int CONSTRUCTOR = -1983641651;
        public PassportSuitableElement[] suitableElements;

        public PassportRequiredElement() {
        }

        public PassportRequiredElement(PassportSuitableElement[] suitableElements) {
            this.suitableElements = suitableElements;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PassportSuitableElement extends Object {
        public static final int CONSTRUCTOR = -789019876;
        public boolean isNativeNameRequired;
        public boolean isSelfieRequired;
        public boolean isTranslationRequired;
        public PassportElementType type;

        public PassportSuitableElement() {
        }

        public PassportSuitableElement(PassportElementType type, boolean isSelfieRequired, boolean isTranslationRequired, boolean isNativeNameRequired) {
            this.type = type;
            this.isSelfieRequired = isSelfieRequired;
            this.isTranslationRequired = isTranslationRequired;
            this.isNativeNameRequired = isNativeNameRequired;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PasswordState extends Object {
        public static final int CONSTRUCTOR = -1154797731;
        public boolean hasPassportData;
        public boolean hasPassword;
        public boolean hasRecoveryEmailAddress;
        public String passwordHint;
        public EmailAddressAuthenticationCodeInfo recoveryEmailAddressCodeInfo;

        public PasswordState() {
        }

        public PasswordState(boolean hasPassword, String passwordHint, boolean hasRecoveryEmailAddress, boolean hasPassportData, EmailAddressAuthenticationCodeInfo recoveryEmailAddressCodeInfo) {
            this.hasPassword = hasPassword;
            this.passwordHint = passwordHint;
            this.hasRecoveryEmailAddress = hasRecoveryEmailAddress;
            this.hasPassportData = hasPassportData;
            this.recoveryEmailAddressCodeInfo = recoveryEmailAddressCodeInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PaymentForm extends Object {
        public static final int CONSTRUCTOR = -200418230;
        public boolean canSaveCredentials;
        public Invoice invoice;
        public boolean needPassword;
        public PaymentsProviderStripe paymentsProvider;
        public SavedCredentials savedCredentials;
        public OrderInfo savedOrderInfo;
        public String url;

        public PaymentForm() {
        }

        public PaymentForm(Invoice invoice, String url, PaymentsProviderStripe paymentsProvider, OrderInfo savedOrderInfo, SavedCredentials savedCredentials, boolean canSaveCredentials, boolean needPassword) {
            this.invoice = invoice;
            this.url = url;
            this.paymentsProvider = paymentsProvider;
            this.savedOrderInfo = savedOrderInfo;
            this.savedCredentials = savedCredentials;
            this.canSaveCredentials = canSaveCredentials;
            this.needPassword = needPassword;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PaymentReceipt extends Object {
        public static final int CONSTRUCTOR = -1171223545;
        public String credentialsTitle;
        public int date;
        public Invoice invoice;
        public OrderInfo orderInfo;
        public int paymentsProviderUserId;
        public ShippingOption shippingOption;

        public PaymentReceipt() {
        }

        public PaymentReceipt(int date, int paymentsProviderUserId, Invoice invoice, OrderInfo orderInfo, ShippingOption shippingOption, String credentialsTitle) {
            this.date = date;
            this.paymentsProviderUserId = paymentsProviderUserId;
            this.invoice = invoice;
            this.orderInfo = orderInfo;
            this.shippingOption = shippingOption;
            this.credentialsTitle = credentialsTitle;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PaymentResult extends Object {
        public static final int CONSTRUCTOR = -804263843;
        public boolean success;
        public String verificationUrl;

        public PaymentResult() {
        }

        public PaymentResult(boolean success, String verificationUrl) {
            this.success = success;
            this.verificationUrl = verificationUrl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PaymentsProviderStripe extends Object {
        public static final int CONSTRUCTOR = 1090791032;
        public boolean needCardholderName;
        public boolean needCountry;
        public boolean needPostalCode;
        public String publishableKey;

        public PaymentsProviderStripe() {
        }

        public PaymentsProviderStripe(String publishableKey, boolean needCountry, boolean needPostalCode, boolean needCardholderName) {
            this.publishableKey = publishableKey;
            this.needCountry = needCountry;
            this.needPostalCode = needPostalCode;
            this.needCardholderName = needCardholderName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PersonalDetails extends Object {
        public static final int CONSTRUCTOR = -1061656137;
        public Date birthdate;
        public String countryCode;
        public String firstName;
        public String gender;
        public String lastName;
        public String middleName;
        public String nativeFirstName;
        public String nativeLastName;
        public String nativeMiddleName;
        public String residenceCountryCode;

        public PersonalDetails() {
        }

        public PersonalDetails(String firstName, String middleName, String lastName, String nativeFirstName, String nativeMiddleName, String nativeLastName, Date birthdate, String gender, String countryCode, String residenceCountryCode) {
            this.firstName = firstName;
            this.middleName = middleName;
            this.lastName = lastName;
            this.nativeFirstName = nativeFirstName;
            this.nativeMiddleName = nativeMiddleName;
            this.nativeLastName = nativeLastName;
            this.birthdate = birthdate;
            this.gender = gender;
            this.countryCode = countryCode;
            this.residenceCountryCode = residenceCountryCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PersonalDocument extends Object {
        public static final int CONSTRUCTOR = -1011634661;
        public DatedFile[] files;
        public DatedFile[] translation;

        public PersonalDocument() {
        }

        public PersonalDocument(DatedFile[] files, DatedFile[] translation) {
            this.files = files;
            this.translation = translation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PhoneNumberAuthenticationSettings extends Object {
        public static final int CONSTRUCTOR = -859198743;
        public boolean allowFlashCall;
        public boolean allowSmsRetrieverApi;
        public boolean isCurrentPhoneNumber;

        public PhoneNumberAuthenticationSettings() {
        }

        public PhoneNumberAuthenticationSettings(boolean allowFlashCall, boolean isCurrentPhoneNumber, boolean allowSmsRetrieverApi) {
            this.allowFlashCall = allowFlashCall;
            this.isCurrentPhoneNumber = isCurrentPhoneNumber;
            this.allowSmsRetrieverApi = allowSmsRetrieverApi;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Photo extends Object {
        public static final int CONSTRUCTOR = -2022871583;
        public boolean hasStickers;
        public Minithumbnail minithumbnail;
        public PhotoSize[] sizes;

        public Photo() {
        }

        public Photo(boolean hasStickers, Minithumbnail minithumbnail, PhotoSize[] sizes) {
            this.hasStickers = hasStickers;
            this.minithumbnail = minithumbnail;
            this.sizes = sizes;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PhotoSize extends Object {
        public static final int CONSTRUCTOR = 421980227;
        public int height;
        public File photo;
        public String type;
        public int width;

        public PhotoSize() {
        }

        public PhotoSize(String type, File photo, int width, int height) {
            this.type = type;
            this.photo = photo;
            this.width = width;
            this.height = height;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Poll extends Object {
        public static final int CONSTRUCTOR = -964385924;
        public long id;
        public boolean isAnonymous;
        public boolean isClosed;
        public PollOption[] options;
        public String question;
        public int[] recentVoterUserIds;
        public int totalVoterCount;
        public PollType type;

        public Poll() {
        }

        public Poll(long id, String question, PollOption[] options, int totalVoterCount, int[] recentVoterUserIds, boolean isAnonymous, PollType type, boolean isClosed) {
            this.id = id;
            this.question = question;
            this.options = options;
            this.totalVoterCount = totalVoterCount;
            this.recentVoterUserIds = recentVoterUserIds;
            this.isAnonymous = isAnonymous;
            this.type = type;
            this.isClosed = isClosed;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PollOption extends Object {
        public static final int CONSTRUCTOR = 1473893797;
        public boolean isBeingChosen;
        public boolean isChosen;
        public String text;
        public int votePercentage;
        public int voterCount;

        public PollOption() {
        }

        public PollOption(String text, int voterCount, int votePercentage, boolean isChosen, boolean isBeingChosen) {
            this.text = text;
            this.voterCount = voterCount;
            this.votePercentage = votePercentage;
            this.isChosen = isChosen;
            this.isBeingChosen = isBeingChosen;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PollTypeRegular extends PollType {
        public static final int CONSTRUCTOR = 641265698;
        public boolean allowMultipleAnswers;

        public PollTypeRegular() {
        }

        public PollTypeRegular(boolean allowMultipleAnswers) {
            this.allowMultipleAnswers = allowMultipleAnswers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PollType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PollTypeQuiz extends PollType {
        public static final int CONSTRUCTOR = -354461930;
        public int correctOptionId;

        public PollTypeQuiz() {
        }

        public PollTypeQuiz(int correctOptionId) {
            this.correctOptionId = correctOptionId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PollType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ProfilePhoto extends Object {
        public static final int CONSTRUCTOR = 978085937;
        public File big;
        public long id;
        public File small;

        public ProfilePhoto() {
        }

        public ProfilePhoto(long id, File small, File big) {
            this.id = id;
            this.small = small;
            this.big = big;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Proxies extends Object {
        public static final int CONSTRUCTOR = 1200447205;
        public Proxy[] proxies;

        public Proxies() {
        }

        public Proxies(Proxy[] proxies) {
            this.proxies = proxies;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Proxy extends Object {
        public static final int CONSTRUCTOR = 196049779;
        public int id;
        public boolean isEnabled;
        public int lastUsedDate;
        public int port;
        public String server;
        public ProxyType type;

        public Proxy() {
        }

        public Proxy(int id, String server, int port, int lastUsedDate, boolean isEnabled, ProxyType type) {
            this.id = id;
            this.server = server;
            this.port = port;
            this.lastUsedDate = lastUsedDate;
            this.isEnabled = isEnabled;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ProxyTypeSocks5 extends ProxyType {
        public static final int CONSTRUCTOR = -890027341;
        public String password;
        public String username;

        public ProxyTypeSocks5() {
        }

        public ProxyTypeSocks5(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ProxyType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ProxyTypeHttp extends ProxyType {
        public static final int CONSTRUCTOR = -1547188361;
        public boolean httpOnly;
        public String password;
        public String username;

        public ProxyTypeHttp() {
        }

        public ProxyTypeHttp(String username, String password, boolean httpOnly) {
            this.username = username;
            this.password = password;
            this.httpOnly = httpOnly;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ProxyType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ProxyTypeMtproto extends ProxyType {
        public static final int CONSTRUCTOR = -1964826627;
        public String secret;

        public ProxyTypeMtproto() {
        }

        public ProxyTypeMtproto(String secret) {
            this.secret = secret;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ProxyType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PublicChatTypeHasUsername extends PublicChatType {
        public static final int CONSTRUCTOR = 350789758;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PublicChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PublicChatTypeIsLocationBased extends PublicChatType {
        public static final int CONSTRUCTOR = 1183735952;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PublicChatType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PublicMessageLink extends Object {
        public static final int CONSTRUCTOR = -679603433;
        public String html;
        public String link;

        public PublicMessageLink() {
        }

        public PublicMessageLink(String link, String html) {
            this.link = link;
            this.html = html;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentHidden extends PushMessageContent {
        public static final int CONSTRUCTOR = -316950436;
        public boolean isPinned;

        public PushMessageContentHidden() {
        }

        public PushMessageContentHidden(boolean isPinned) {
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentAnimation extends PushMessageContent {
        public static final int CONSTRUCTOR = 1034215396;
        public Animation animation;
        public String caption;
        public boolean isPinned;

        public PushMessageContentAnimation() {
        }

        public PushMessageContentAnimation(Animation animation, String caption, boolean isPinned) {
            this.animation = animation;
            this.caption = caption;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentAudio extends PushMessageContent {
        public static final int CONSTRUCTOR = 381581426;
        public Audio audio;
        public boolean isPinned;

        public PushMessageContentAudio() {
        }

        public PushMessageContentAudio(Audio audio, boolean isPinned) {
            this.audio = audio;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentContact extends PushMessageContent {
        public static final int CONSTRUCTOR = -12219820;
        public boolean isPinned;
        public String name;

        public PushMessageContentContact() {
        }

        public PushMessageContentContact(String name, boolean isPinned) {
            this.name = name;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentContactRegistered extends PushMessageContent {
        public static final int CONSTRUCTOR = -303962720;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentDocument extends PushMessageContent {
        public static final int CONSTRUCTOR = -458379775;
        public Document document;
        public boolean isPinned;

        public PushMessageContentDocument() {
        }

        public PushMessageContentDocument(Document document, boolean isPinned) {
            this.document = document;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentGame extends PushMessageContent {
        public static final int CONSTRUCTOR = -515131109;
        public boolean isPinned;
        public String title;

        public PushMessageContentGame() {
        }

        public PushMessageContentGame(String title, boolean isPinned) {
            this.title = title;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentGameScore extends PushMessageContent {
        public static final int CONSTRUCTOR = 901303688;
        public boolean isPinned;
        public int score;
        public String title;

        public PushMessageContentGameScore() {
        }

        public PushMessageContentGameScore(String title, int score, boolean isPinned) {
            this.title = title;
            this.score = score;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentInvoice extends PushMessageContent {
        public static final int CONSTRUCTOR = -1731687492;
        public boolean isPinned;
        public String price;

        public PushMessageContentInvoice() {
        }

        public PushMessageContentInvoice(String price, boolean isPinned) {
            this.price = price;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentLocation extends PushMessageContent {
        public static final int CONSTRUCTOR = -1288005709;
        public boolean isLive;
        public boolean isPinned;

        public PushMessageContentLocation() {
        }

        public PushMessageContentLocation(boolean isLive, boolean isPinned) {
            this.isLive = isLive;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentPhoto extends PushMessageContent {
        public static final int CONSTRUCTOR = 140631122;
        public String caption;
        public boolean isPinned;
        public boolean isSecret;
        public Photo photo;

        public PushMessageContentPhoto() {
        }

        public PushMessageContentPhoto(Photo photo, String caption, boolean isSecret, boolean isPinned) {
            this.photo = photo;
            this.caption = caption;
            this.isSecret = isSecret;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentPoll extends PushMessageContent {
        public static final int CONSTRUCTOR = -44403654;
        public boolean isPinned;
        public boolean isRegular;
        public String question;

        public PushMessageContentPoll() {
        }

        public PushMessageContentPoll(String question, boolean isRegular, boolean isPinned) {
            this.question = question;
            this.isRegular = isRegular;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentScreenshotTaken extends PushMessageContent {
        public static final int CONSTRUCTOR = 214245369;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentSticker extends PushMessageContent {
        public static final int CONSTRUCTOR = 1553513939;
        public String emoji;
        public boolean isPinned;
        public Sticker sticker;

        public PushMessageContentSticker() {
        }

        public PushMessageContentSticker(Sticker sticker, String emoji, boolean isPinned) {
            this.sticker = sticker;
            this.emoji = emoji;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentText extends PushMessageContent {
        public static final int CONSTRUCTOR = 274587305;
        public boolean isPinned;
        public String text;

        public PushMessageContentText() {
        }

        public PushMessageContentText(String text, boolean isPinned) {
            this.text = text;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentVideo extends PushMessageContent {
        public static final int CONSTRUCTOR = 310038831;
        public String caption;
        public boolean isPinned;
        public boolean isSecret;
        public Video video;

        public PushMessageContentVideo() {
        }

        public PushMessageContentVideo(Video video, String caption, boolean isSecret, boolean isPinned) {
            this.video = video;
            this.caption = caption;
            this.isSecret = isSecret;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentVideoNote extends PushMessageContent {
        public static final int CONSTRUCTOR = -1122764417;
        public boolean isPinned;
        public VideoNote videoNote;

        public PushMessageContentVideoNote() {
        }

        public PushMessageContentVideoNote(VideoNote videoNote, boolean isPinned) {
            this.videoNote = videoNote;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentVoiceNote extends PushMessageContent {
        public static final int CONSTRUCTOR = 88910987;
        public boolean isPinned;
        public VoiceNote voiceNote;

        public PushMessageContentVoiceNote() {
        }

        public PushMessageContentVoiceNote(VoiceNote voiceNote, boolean isPinned) {
            this.voiceNote = voiceNote;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentBasicGroupChatCreate extends PushMessageContent {
        public static final int CONSTRUCTOR = -2114855172;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentChatAddMembers extends PushMessageContent {
        public static final int CONSTRUCTOR = -1087145158;
        public boolean isCurrentUser;
        public boolean isReturned;
        public String memberName;

        public PushMessageContentChatAddMembers() {
        }

        public PushMessageContentChatAddMembers(String memberName, boolean isCurrentUser, boolean isReturned) {
            this.memberName = memberName;
            this.isCurrentUser = isCurrentUser;
            this.isReturned = isReturned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentChatChangePhoto extends PushMessageContent {
        public static final int CONSTRUCTOR = -1114222051;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentChatChangeTitle extends PushMessageContent {
        public static final int CONSTRUCTOR = -1964902749;
        public String title;

        public PushMessageContentChatChangeTitle() {
        }

        public PushMessageContentChatChangeTitle(String title) {
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentChatDeleteMember extends PushMessageContent {
        public static final int CONSTRUCTOR = 598714783;
        public boolean isCurrentUser;
        public boolean isLeft;
        public String memberName;

        public PushMessageContentChatDeleteMember() {
        }

        public PushMessageContentChatDeleteMember(String memberName, boolean isCurrentUser, boolean isLeft) {
            this.memberName = memberName;
            this.isCurrentUser = isCurrentUser;
            this.isLeft = isLeft;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentChatJoinByLink extends PushMessageContent {
        public static final int CONSTRUCTOR = 1553719113;

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentMessageForwards extends PushMessageContent {
        public static final int CONSTRUCTOR = -1913083876;
        public int totalCount;

        public PushMessageContentMessageForwards() {
        }

        public PushMessageContentMessageForwards(int totalCount) {
            this.totalCount = totalCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushMessageContentMediaAlbum extends PushMessageContent {
        public static final int CONSTRUCTOR = -874278109;
        public boolean hasPhotos;
        public boolean hasVideos;
        public int totalCount;

        public PushMessageContentMediaAlbum() {
        }

        public PushMessageContentMediaAlbum(int totalCount, boolean hasPhotos, boolean hasVideos) {
            this.totalCount = totalCount;
            this.hasPhotos = hasPhotos;
            this.hasVideos = hasVideos;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.PushMessageContent, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PushReceiverId extends Object {
        public static final int CONSTRUCTOR = 371056428;
        public long id;

        public PushReceiverId() {
        }

        public PushReceiverId(long id) {
            this.id = id;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RecoveryEmailAddress extends Object {
        public static final int CONSTRUCTOR = 1290526187;
        public String recoveryEmailAddress;

        public RecoveryEmailAddress() {
        }

        public RecoveryEmailAddress(String recoveryEmailAddress) {
            this.recoveryEmailAddress = recoveryEmailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoteFile extends Object {
        public static final int CONSTRUCTOR = -1822143022;
        public String id;
        public boolean isUploadingActive;
        public boolean isUploadingCompleted;
        public String uniqueId;
        public int uploadedSize;

        public RemoteFile() {
        }

        public RemoteFile(String id, String uniqueId, boolean isUploadingActive, boolean isUploadingCompleted, int uploadedSize) {
            this.id = id;
            this.uniqueId = uniqueId;
            this.isUploadingActive = isUploadingActive;
            this.isUploadingCompleted = isUploadingCompleted;
            this.uploadedSize = uploadedSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReplyMarkupRemoveKeyboard extends ReplyMarkup {
        public static final int CONSTRUCTOR = -691252879;
        public boolean isPersonal;

        public ReplyMarkupRemoveKeyboard() {
        }

        public ReplyMarkupRemoveKeyboard(boolean isPersonal) {
            this.isPersonal = isPersonal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ReplyMarkup, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReplyMarkupForceReply extends ReplyMarkup {
        public static final int CONSTRUCTOR = 1039104593;
        public boolean isPersonal;

        public ReplyMarkupForceReply() {
        }

        public ReplyMarkupForceReply(boolean isPersonal) {
            this.isPersonal = isPersonal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ReplyMarkup, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReplyMarkupShowKeyboard extends ReplyMarkup {
        public static final int CONSTRUCTOR = -992627133;
        public boolean isPersonal;
        public boolean oneTime;
        public boolean resizeKeyboard;
        public KeyboardButton[][] rows;

        public ReplyMarkupShowKeyboard() {
        }

        public ReplyMarkupShowKeyboard(KeyboardButton[][] rows, boolean resizeKeyboard, boolean oneTime, boolean isPersonal) {
            this.rows = rows;
            this.resizeKeyboard = resizeKeyboard;
            this.oneTime = oneTime;
            this.isPersonal = isPersonal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ReplyMarkup, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReplyMarkupInlineKeyboard extends ReplyMarkup {
        public static final int CONSTRUCTOR = -619317658;
        public InlineKeyboardButton[][] rows;

        public ReplyMarkupInlineKeyboard() {
        }

        public ReplyMarkupInlineKeyboard(InlineKeyboardButton[][] rows) {
            this.rows = rows;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.ReplyMarkup, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextPlain extends RichText {
        public static final int CONSTRUCTOR = 482617702;
        public String text;

        public RichTextPlain() {
        }

        public RichTextPlain(String text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextBold extends RichText {
        public static final int CONSTRUCTOR = 1670844268;
        public RichText text;

        public RichTextBold() {
        }

        public RichTextBold(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextItalic extends RichText {
        public static final int CONSTRUCTOR = 1853354047;
        public RichText text;

        public RichTextItalic() {
        }

        public RichTextItalic(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextUnderline extends RichText {
        public static final int CONSTRUCTOR = -536019572;
        public RichText text;

        public RichTextUnderline() {
        }

        public RichTextUnderline(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextStrikethrough extends RichText {
        public static final int CONSTRUCTOR = 723413585;
        public RichText text;

        public RichTextStrikethrough() {
        }

        public RichTextStrikethrough(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextFixed extends RichText {
        public static final int CONSTRUCTOR = -1271496249;
        public RichText text;

        public RichTextFixed() {
        }

        public RichTextFixed(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextUrl extends RichText {
        public static final int CONSTRUCTOR = 83939092;
        public boolean isCached;
        public RichText text;
        public String url;

        public RichTextUrl() {
        }

        public RichTextUrl(RichText text, String url, boolean isCached) {
            this.text = text;
            this.url = url;
            this.isCached = isCached;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextEmailAddress extends RichText {
        public static final int CONSTRUCTOR = 40018679;
        public String emailAddress;
        public RichText text;

        public RichTextEmailAddress() {
        }

        public RichTextEmailAddress(RichText text, String emailAddress) {
            this.text = text;
            this.emailAddress = emailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextSubscript extends RichText {
        public static final int CONSTRUCTOR = -868197812;
        public RichText text;

        public RichTextSubscript() {
        }

        public RichTextSubscript(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextSuperscript extends RichText {
        public static final int CONSTRUCTOR = -382241437;
        public RichText text;

        public RichTextSuperscript() {
        }

        public RichTextSuperscript(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextMarked extends RichText {
        public static final int CONSTRUCTOR = -1271999614;
        public RichText text;

        public RichTextMarked() {
        }

        public RichTextMarked(RichText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextPhoneNumber extends RichText {
        public static final int CONSTRUCTOR = 128521539;
        public String phoneNumber;
        public RichText text;

        public RichTextPhoneNumber() {
        }

        public RichTextPhoneNumber(RichText text, String phoneNumber) {
            this.text = text;
            this.phoneNumber = phoneNumber;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextIcon extends RichText {
        public static final int CONSTRUCTOR = -1480316158;
        public Document document;
        public int height;
        public int width;

        public RichTextIcon() {
        }

        public RichTextIcon(Document document, int width, int height) {
            this.document = document;
            this.width = width;
            this.height = height;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTextAnchor extends RichText {
        public static final int CONSTRUCTOR = 673137292;
        public String name;
        public RichText text;

        public RichTextAnchor() {
        }

        public RichTextAnchor(RichText text, String name) {
            this.text = text;
            this.name = name;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RichTexts extends RichText {
        public static final int CONSTRUCTOR = 1647457821;
        public RichText[] texts;

        public RichTexts() {
        }

        public RichTexts(RichText[] texts) {
            this.texts = texts;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.RichText, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SavedCredentials extends Object {
        public static final int CONSTRUCTOR = -370273060;
        public String id;
        public String title;

        public SavedCredentials() {
        }

        public SavedCredentials(String id, String title) {
            this.id = id;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ScopeNotificationSettings extends Object {
        public static final int CONSTRUCTOR = -426103745;
        public boolean disableMentionNotifications;
        public boolean disablePinnedMessageNotifications;
        public int muteFor;
        public boolean showPreview;
        public String sound;

        public ScopeNotificationSettings() {
        }

        public ScopeNotificationSettings(int muteFor, String sound, boolean showPreview, boolean disablePinnedMessageNotifications, boolean disableMentionNotifications) {
            this.muteFor = muteFor;
            this.sound = sound;
            this.showPreview = showPreview;
            this.disablePinnedMessageNotifications = disablePinnedMessageNotifications;
            this.disableMentionNotifications = disableMentionNotifications;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterEmpty extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = -869395657;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterAnimation extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = -155713339;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterAudio extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 867505275;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterDocument extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 1526331215;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterPhoto extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 925932293;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterVideo extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 115538222;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterVoiceNote extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 1841439357;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterPhotoAndVideo extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 1352130963;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterUrl extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = -1828724341;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterChatPhoto extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = -1247751329;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterCall extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 1305231012;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterMissedCall extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 970663098;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterVideoNote extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 564323321;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterVoiceAndVideoNote extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 664174819;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterMention extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = 2001258652;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessagesFilterUnreadMention extends SearchMessagesFilter {
        public static final int CONSTRUCTOR = -95769149;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SearchMessagesFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Seconds extends Object {
        public static final int CONSTRUCTOR = 959899022;
        public double seconds;

        public Seconds() {
        }

        public Seconds(double seconds) {
            this.seconds = seconds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SecretChat extends Object {
        public static final int CONSTRUCTOR = 1279231629;
        public int id;
        public boolean isOutbound;
        public byte[] keyHash;
        public int layer;
        public SecretChatState state;
        public int ttl;
        public int userId;

        public SecretChat() {
        }

        public SecretChat(int id, int userId, SecretChatState state, boolean isOutbound, int ttl, byte[] keyHash, int layer) {
            this.id = id;
            this.userId = userId;
            this.state = state;
            this.isOutbound = isOutbound;
            this.ttl = ttl;
            this.keyHash = keyHash;
            this.layer = layer;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SecretChatStatePending extends SecretChatState {
        public static final int CONSTRUCTOR = -1637050756;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SecretChatState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SecretChatStateReady extends SecretChatState {
        public static final int CONSTRUCTOR = -1611352087;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SecretChatState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SecretChatStateClosed extends SecretChatState {
        public static final int CONSTRUCTOR = -1945106707;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SecretChatState, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendMessageOptions extends Object {
        public static final int CONSTRUCTOR = 669760254;
        public boolean disableNotification;
        public boolean fromBackground;
        public MessageSchedulingState schedulingState;

        public SendMessageOptions() {
        }

        public SendMessageOptions(boolean disableNotification, boolean fromBackground, MessageSchedulingState schedulingState) {
            this.disableNotification = disableNotification;
            this.fromBackground = fromBackground;
            this.schedulingState = schedulingState;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Session extends Object {
        public static final int CONSTRUCTOR = 1920553176;
        public int apiId;
        public String applicationName;
        public String applicationVersion;
        public String country;
        public String deviceModel;
        public long id;
        public String ip;
        public boolean isCurrent;
        public boolean isOfficialApplication;
        public boolean isPasswordPending;
        public int lastActiveDate;
        public int logInDate;
        public String platform;
        public String region;
        public String systemVersion;

        public Session() {
        }

        public Session(long id, boolean isCurrent, boolean isPasswordPending, int apiId, String applicationName, String applicationVersion, boolean isOfficialApplication, String deviceModel, String platform, String systemVersion, int logInDate, int lastActiveDate, String ip, String country, String region) {
            this.id = id;
            this.isCurrent = isCurrent;
            this.isPasswordPending = isPasswordPending;
            this.apiId = apiId;
            this.applicationName = applicationName;
            this.applicationVersion = applicationVersion;
            this.isOfficialApplication = isOfficialApplication;
            this.deviceModel = deviceModel;
            this.platform = platform;
            this.systemVersion = systemVersion;
            this.logInDate = logInDate;
            this.lastActiveDate = lastActiveDate;
            this.ip = ip;
            this.country = country;
            this.region = region;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Sessions extends Object {
        public static final int CONSTRUCTOR = -463118121;
        public Session[] sessions;

        public Sessions() {
        }

        public Sessions(Session[] sessions) {
            this.sessions = sessions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ShippingOption extends Object {
        public static final int CONSTRUCTOR = 1425690001;
        public String id;
        public LabeledPricePart[] priceParts;
        public String title;

        public ShippingOption() {
        }

        public ShippingOption(String id, String title, LabeledPricePart[] priceParts) {
            this.id = id;
            this.title = title;
            this.priceParts = priceParts;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Sticker extends Object {
        public static final int CONSTRUCTOR = -1835470627;
        public String emoji;
        public int height;
        public boolean isAnimated;
        public boolean isMask;
        public MaskPosition maskPosition;
        public long setId;
        public File sticker;
        public PhotoSize thumbnail;
        public int width;

        public Sticker() {
        }

        public Sticker(long setId, int width, int height, String emoji, boolean isAnimated, boolean isMask, MaskPosition maskPosition, PhotoSize thumbnail, File sticker) {
            this.setId = setId;
            this.width = width;
            this.height = height;
            this.emoji = emoji;
            this.isAnimated = isAnimated;
            this.isMask = isMask;
            this.maskPosition = maskPosition;
            this.thumbnail = thumbnail;
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StickerSet extends Object {
        public static final int CONSTRUCTOR = 734588298;
        public Emojis[] emojis;
        public long id;
        public boolean isAnimated;
        public boolean isArchived;
        public boolean isInstalled;
        public boolean isMasks;
        public boolean isOfficial;
        public boolean isViewed;
        public String name;
        public Sticker[] stickers;
        public PhotoSize thumbnail;
        public String title;

        public StickerSet() {
        }

        public StickerSet(long id, String title, String name, PhotoSize thumbnail, boolean isInstalled, boolean isArchived, boolean isOfficial, boolean isAnimated, boolean isMasks, boolean isViewed, Sticker[] stickers, Emojis[] emojis) {
            this.id = id;
            this.title = title;
            this.name = name;
            this.thumbnail = thumbnail;
            this.isInstalled = isInstalled;
            this.isArchived = isArchived;
            this.isOfficial = isOfficial;
            this.isAnimated = isAnimated;
            this.isMasks = isMasks;
            this.isViewed = isViewed;
            this.stickers = stickers;
            this.emojis = emojis;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StickerSetInfo extends Object {
        public static final int CONSTRUCTOR = 228054782;
        public Sticker[] covers;
        public long id;
        public boolean isAnimated;
        public boolean isArchived;
        public boolean isInstalled;
        public boolean isMasks;
        public boolean isOfficial;
        public boolean isViewed;
        public String name;
        public int size;
        public PhotoSize thumbnail;
        public String title;

        public StickerSetInfo() {
        }

        public StickerSetInfo(long id, String title, String name, PhotoSize thumbnail, boolean isInstalled, boolean isArchived, boolean isOfficial, boolean isAnimated, boolean isMasks, boolean isViewed, int size, Sticker[] covers) {
            this.id = id;
            this.title = title;
            this.name = name;
            this.thumbnail = thumbnail;
            this.isInstalled = isInstalled;
            this.isArchived = isArchived;
            this.isOfficial = isOfficial;
            this.isAnimated = isAnimated;
            this.isMasks = isMasks;
            this.isViewed = isViewed;
            this.size = size;
            this.covers = covers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StickerSets extends Object {
        public static final int CONSTRUCTOR = -1883828812;
        public StickerSetInfo[] sets;
        public int totalCount;

        public StickerSets() {
        }

        public StickerSets(int totalCount, StickerSetInfo[] sets) {
            this.totalCount = totalCount;
            this.sets = sets;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Stickers extends Object {
        public static final int CONSTRUCTOR = 1974859260;
        public Sticker[] stickers;

        public Stickers() {
        }

        public Stickers(Sticker[] stickers) {
            this.stickers = stickers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StorageStatistics extends Object {
        public static final int CONSTRUCTOR = 217237013;
        public StorageStatisticsByChat[] byChat;
        public int count;
        public long size;

        public StorageStatistics() {
        }

        public StorageStatistics(long size, int count, StorageStatisticsByChat[] byChat) {
            this.size = size;
            this.count = count;
            this.byChat = byChat;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StorageStatisticsByChat extends Object {
        public static final int CONSTRUCTOR = 635434531;
        public StorageStatisticsByFileType[] byFileType;
        public long chatId;
        public int count;
        public long size;

        public StorageStatisticsByChat() {
        }

        public StorageStatisticsByChat(long chatId, long size, int count, StorageStatisticsByFileType[] byFileType) {
            this.chatId = chatId;
            this.size = size;
            this.count = count;
            this.byFileType = byFileType;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StorageStatisticsByFileType extends Object {
        public static final int CONSTRUCTOR = 714012840;
        public int count;
        public FileType fileType;
        public long size;

        public StorageStatisticsByFileType() {
        }

        public StorageStatisticsByFileType(FileType fileType, long size, int count) {
            this.fileType = fileType;
            this.size = size;
            this.count = count;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StorageStatisticsFast extends Object {
        public static final int CONSTRUCTOR = -884922271;
        public long databaseSize;
        public int fileCount;
        public long filesSize;
        public long languagePackDatabaseSize;
        public long logSize;

        public StorageStatisticsFast() {
        }

        public StorageStatisticsFast(long filesSize, int fileCount, long databaseSize, long languagePackDatabaseSize, long logSize) {
            this.filesSize = filesSize;
            this.fileCount = fileCount;
            this.databaseSize = databaseSize;
            this.languagePackDatabaseSize = languagePackDatabaseSize;
            this.logSize = logSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Supergroup extends Object {
        public static final int CONSTRUCTOR = -103091;
        public int date;
        public boolean hasLinkedChat;
        public boolean hasLocation;
        public int id;
        public boolean isChannel;
        public boolean isScam;
        public boolean isSlowModeEnabled;
        public boolean isVerified;
        public int memberCount;
        public String restrictionReason;
        public boolean signMessages;
        public ChatMemberStatus status;
        public String username;

        public Supergroup() {
        }

        public Supergroup(int id, String username, int date, ChatMemberStatus status, int memberCount, boolean hasLinkedChat, boolean hasLocation, boolean signMessages, boolean isSlowModeEnabled, boolean isChannel, boolean isVerified, String restrictionReason, boolean isScam) {
            this.id = id;
            this.username = username;
            this.date = date;
            this.status = status;
            this.memberCount = memberCount;
            this.hasLinkedChat = hasLinkedChat;
            this.hasLocation = hasLocation;
            this.signMessages = signMessages;
            this.isSlowModeEnabled = isSlowModeEnabled;
            this.isChannel = isChannel;
            this.isVerified = isVerified;
            this.restrictionReason = restrictionReason;
            this.isScam = isScam;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupFullInfo extends Object {
        public static final int CONSTRUCTOR = -1562832718;
        public int administratorCount;
        public int bannedCount;
        public boolean canGetMembers;
        public boolean canSetLocation;
        public boolean canSetStickerSet;
        public boolean canSetUsername;
        public boolean canViewStatistics;
        public String description;
        public String inviteLink;
        public boolean isAllHistoryAvailable;
        public long linkedChatId;
        public ChatLocation location;
        public int memberCount;
        public int restrictedCount;
        public int slowModeDelay;
        public double slowModeDelayExpiresIn;
        public long stickerSetId;
        public int upgradedFromBasicGroupId;
        public long upgradedFromMaxMessageId;

        public SupergroupFullInfo() {
        }

        public SupergroupFullInfo(String description, int memberCount, int administratorCount, int restrictedCount, int bannedCount, long linkedChatId, int slowModeDelay, double slowModeDelayExpiresIn, boolean canGetMembers, boolean canSetUsername, boolean canSetStickerSet, boolean canSetLocation, boolean canViewStatistics, boolean isAllHistoryAvailable, long stickerSetId, ChatLocation location, String inviteLink, int upgradedFromBasicGroupId, long upgradedFromMaxMessageId) {
            this.description = description;
            this.memberCount = memberCount;
            this.administratorCount = administratorCount;
            this.restrictedCount = restrictedCount;
            this.bannedCount = bannedCount;
            this.linkedChatId = linkedChatId;
            this.slowModeDelay = slowModeDelay;
            this.slowModeDelayExpiresIn = slowModeDelayExpiresIn;
            this.canGetMembers = canGetMembers;
            this.canSetUsername = canSetUsername;
            this.canSetStickerSet = canSetStickerSet;
            this.canSetLocation = canSetLocation;
            this.canViewStatistics = canViewStatistics;
            this.isAllHistoryAvailable = isAllHistoryAvailable;
            this.stickerSetId = stickerSetId;
            this.location = location;
            this.inviteLink = inviteLink;
            this.upgradedFromBasicGroupId = upgradedFromBasicGroupId;
            this.upgradedFromMaxMessageId = upgradedFromMaxMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterRecent extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = 1178199509;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterContacts extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = -1282910856;
        public String query;

        public SupergroupMembersFilterContacts() {
        }

        public SupergroupMembersFilterContacts(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterAdministrators extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = -2097380265;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterSearch extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = -1696358469;
        public String query;

        public SupergroupMembersFilterSearch() {
        }

        public SupergroupMembersFilterSearch(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterRestricted extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = -1107800034;
        public String query;

        public SupergroupMembersFilterRestricted() {
        }

        public SupergroupMembersFilterRestricted(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterBanned extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = -1210621683;
        public String query;

        public SupergroupMembersFilterBanned() {
        }

        public SupergroupMembersFilterBanned(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SupergroupMembersFilterBots extends SupergroupMembersFilter {
        public static final int CONSTRUCTOR = 492138918;

        @Override // org.drinkless.td.libcore.telegram.TdApi.SupergroupMembersFilter, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrl extends Object {
        public static final int CONSTRUCTOR = -1140786622;
        public TMeUrlType type;
        public String url;

        public TMeUrl() {
        }

        public TMeUrl(String url, TMeUrlType type) {
            this.url = url;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrlTypeUser extends TMeUrlType {
        public static final int CONSTRUCTOR = -1198700130;
        public int userId;

        public TMeUrlTypeUser() {
        }

        public TMeUrlTypeUser(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TMeUrlType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrlTypeSupergroup extends TMeUrlType {
        public static final int CONSTRUCTOR = -1353369944;
        public long supergroupId;

        public TMeUrlTypeSupergroup() {
        }

        public TMeUrlTypeSupergroup(long supergroupId) {
            this.supergroupId = supergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TMeUrlType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrlTypeChatInvite extends TMeUrlType {
        public static final int CONSTRUCTOR = 313907785;
        public ChatInviteLinkInfo info;

        public TMeUrlTypeChatInvite() {
        }

        public TMeUrlTypeChatInvite(ChatInviteLinkInfo info) {
            this.info = info;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TMeUrlType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrlTypeStickerSet extends TMeUrlType {
        public static final int CONSTRUCTOR = 1602473196;
        public long stickerSetId;

        public TMeUrlTypeStickerSet() {
        }

        public TMeUrlTypeStickerSet(long stickerSetId) {
            this.stickerSetId = stickerSetId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TMeUrlType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TMeUrls extends Object {
        public static final int CONSTRUCTOR = -1130595098;
        public TMeUrl[] urls;

        public TMeUrls() {
        }

        public TMeUrls(TMeUrl[] urls) {
            this.urls = urls;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TdlibParameters extends Object {
        public static final int CONSTRUCTOR = -761520773;
        public String apiHash;
        public int apiId;
        public String applicationVersion;
        public String databaseDirectory;
        public String deviceModel;
        public boolean enableStorageOptimizer;
        public String filesDirectory;
        public boolean ignoreFileNames;
        public String systemLanguageCode;
        public String systemVersion;
        public boolean useChatInfoDatabase;
        public boolean useFileDatabase;
        public boolean useMessageDatabase;
        public boolean useSecretChats;
        public boolean useTestDc;

        public TdlibParameters() {
        }

        public TdlibParameters(boolean useTestDc, String databaseDirectory, String filesDirectory, boolean useFileDatabase, boolean useChatInfoDatabase, boolean useMessageDatabase, boolean useSecretChats, int apiId, String apiHash, String systemLanguageCode, String deviceModel, String systemVersion, String applicationVersion, boolean enableStorageOptimizer, boolean ignoreFileNames) {
            this.useTestDc = useTestDc;
            this.databaseDirectory = databaseDirectory;
            this.filesDirectory = filesDirectory;
            this.useFileDatabase = useFileDatabase;
            this.useChatInfoDatabase = useChatInfoDatabase;
            this.useMessageDatabase = useMessageDatabase;
            this.useSecretChats = useSecretChats;
            this.apiId = apiId;
            this.apiHash = apiHash;
            this.systemLanguageCode = systemLanguageCode;
            this.deviceModel = deviceModel;
            this.systemVersion = systemVersion;
            this.applicationVersion = applicationVersion;
            this.enableStorageOptimizer = enableStorageOptimizer;
            this.ignoreFileNames = ignoreFileNames;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TemporaryPasswordState extends Object {
        public static final int CONSTRUCTOR = 939837410;
        public boolean hasPassword;
        public int validFor;

        public TemporaryPasswordState() {
        }

        public TemporaryPasswordState(boolean hasPassword, int validFor) {
            this.hasPassword = hasPassword;
            this.validFor = validFor;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TermsOfService extends Object {
        public static final int CONSTRUCTOR = 739422597;
        public int minUserAge;
        public boolean showPopup;
        public FormattedText text;

        public TermsOfService() {
        }

        public TermsOfService(FormattedText text, int minUserAge, boolean showPopup) {
            this.text = text;
            this.minUserAge = minUserAge;
            this.showPopup = showPopup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestBytes extends Object {
        public static final int CONSTRUCTOR = -1541225250;
        public byte[] value;

        public TestBytes() {
        }

        public TestBytes(byte[] value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestInt extends Object {
        public static final int CONSTRUCTOR = -574804983;
        public int value;

        public TestInt() {
        }

        public TestInt(int value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestString extends Object {
        public static final int CONSTRUCTOR = -27891572;
        public String value;

        public TestString() {
        }

        public TestString(String value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestVectorInt extends Object {
        public static final int CONSTRUCTOR = 593682027;
        public int[] value;

        public TestVectorInt() {
        }

        public TestVectorInt(int[] value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestVectorIntObject extends Object {
        public static final int CONSTRUCTOR = 125891546;
        public TestInt[] value;

        public TestVectorIntObject() {
        }

        public TestVectorIntObject(TestInt[] value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestVectorString extends Object {
        public static final int CONSTRUCTOR = 79339995;
        public String[] value;

        public TestVectorString() {
        }

        public TestVectorString(String[] value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestVectorStringObject extends Object {
        public static final int CONSTRUCTOR = 80780537;
        public TestString[] value;

        public TestVectorStringObject() {
        }

        public TestVectorStringObject(TestString[] value) {
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Text extends Object {
        public static final int CONSTRUCTOR = 578181272;
        public String text;

        public Text() {
        }

        public Text(String text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntities extends Object {
        public static final int CONSTRUCTOR = -933199172;
        public TextEntity[] entities;

        public TextEntities() {
        }

        public TextEntities(TextEntity[] entities) {
            this.entities = entities;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntity extends Object {
        public static final int CONSTRUCTOR = -1951688280;
        public int length;
        public int offset;
        public TextEntityType type;

        public TextEntity() {
        }

        public TextEntity(int offset, int length, TextEntityType type) {
            this.offset = offset;
            this.length = length;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeMention extends TextEntityType {
        public static final int CONSTRUCTOR = 934535013;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeHashtag extends TextEntityType {
        public static final int CONSTRUCTOR = -1023958307;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeCashtag extends TextEntityType {
        public static final int CONSTRUCTOR = 1222915915;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeBotCommand extends TextEntityType {
        public static final int CONSTRUCTOR = -1150997581;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeUrl extends TextEntityType {
        public static final int CONSTRUCTOR = -1312762756;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeEmailAddress extends TextEntityType {
        public static final int CONSTRUCTOR = 1425545249;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypePhoneNumber extends TextEntityType {
        public static final int CONSTRUCTOR = -1160140246;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeBold extends TextEntityType {
        public static final int CONSTRUCTOR = -1128210000;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeItalic extends TextEntityType {
        public static final int CONSTRUCTOR = -118253987;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeUnderline extends TextEntityType {
        public static final int CONSTRUCTOR = 792317842;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeStrikethrough extends TextEntityType {
        public static final int CONSTRUCTOR = 961529082;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeCode extends TextEntityType {
        public static final int CONSTRUCTOR = -974534326;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypePre extends TextEntityType {
        public static final int CONSTRUCTOR = 1648958606;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypePreCode extends TextEntityType {
        public static final int CONSTRUCTOR = -945325397;
        public String language;

        public TextEntityTypePreCode() {
        }

        public TextEntityTypePreCode(String language) {
            this.language = language;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeTextUrl extends TextEntityType {
        public static final int CONSTRUCTOR = 445719651;
        public String url;

        public TextEntityTypeTextUrl() {
        }

        public TextEntityTypeTextUrl(String url) {
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextEntityTypeMentionName extends TextEntityType {
        public static final int CONSTRUCTOR = -791517091;
        public int userId;

        public TextEntityTypeMentionName() {
        }

        public TextEntityTypeMentionName(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextEntityType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextParseModeMarkdown extends TextParseMode {
        public static final int CONSTRUCTOR = 360073407;
        public int version;

        public TextParseModeMarkdown() {
        }

        public TextParseModeMarkdown(int version) {
            this.version = version;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextParseMode, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TextParseModeHTML extends TextParseMode {
        public static final int CONSTRUCTOR = 1660208627;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TextParseMode, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryUsers extends TopChatCategory {
        public static final int CONSTRUCTOR = 1026706816;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryBots extends TopChatCategory {
        public static final int CONSTRUCTOR = -1577129195;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryGroups extends TopChatCategory {
        public static final int CONSTRUCTOR = 1530056846;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryChannels extends TopChatCategory {
        public static final int CONSTRUCTOR = -500825885;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryInlineBots extends TopChatCategory {
        public static final int CONSTRUCTOR = 377023356;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryCalls extends TopChatCategory {
        public static final int CONSTRUCTOR = 356208861;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TopChatCategoryForwardChats extends TopChatCategory {
        public static final int CONSTRUCTOR = 1695922133;

        @Override // org.drinkless.td.libcore.telegram.TdApi.TopChatCategory, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateAuthorizationState extends Update {
        public static final int CONSTRUCTOR = 1622347490;
        public AuthorizationState authorizationState;

        public UpdateAuthorizationState() {
        }

        public UpdateAuthorizationState(AuthorizationState authorizationState) {
            this.authorizationState = authorizationState;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewMessage extends Update {
        public static final int CONSTRUCTOR = -563105266;
        public Message message;

        public UpdateNewMessage() {
        }

        public UpdateNewMessage(Message message) {
            this.message = message;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageSendAcknowledged extends Update {
        public static final int CONSTRUCTOR = 1302843961;
        public long chatId;
        public long messageId;

        public UpdateMessageSendAcknowledged() {
        }

        public UpdateMessageSendAcknowledged(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageSendSucceeded extends Update {
        public static final int CONSTRUCTOR = 1815715197;
        public Message message;
        public long oldMessageId;

        public UpdateMessageSendSucceeded() {
        }

        public UpdateMessageSendSucceeded(Message message, long oldMessageId) {
            this.message = message;
            this.oldMessageId = oldMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageSendFailed extends Update {
        public static final int CONSTRUCTOR = -1032335779;
        public int errorCode;
        public String errorMessage;
        public Message message;
        public long oldMessageId;

        public UpdateMessageSendFailed() {
        }

        public UpdateMessageSendFailed(Message message, long oldMessageId, int errorCode, String errorMessage) {
            this.message = message;
            this.oldMessageId = oldMessageId;
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageContent extends Update {
        public static final int CONSTRUCTOR = 506903332;
        public long chatId;
        public long messageId;
        public MessageContent newContent;

        public UpdateMessageContent() {
        }

        public UpdateMessageContent(long chatId, long messageId, MessageContent newContent) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.newContent = newContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageEdited extends Update {
        public static final int CONSTRUCTOR = -559545626;
        public long chatId;
        public int editDate;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public UpdateMessageEdited() {
        }

        public UpdateMessageEdited(long chatId, long messageId, int editDate, ReplyMarkup replyMarkup) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.editDate = editDate;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageViews extends Update {
        public static final int CONSTRUCTOR = -1854131125;
        public long chatId;
        public long messageId;
        public int views;

        public UpdateMessageViews() {
        }

        public UpdateMessageViews(long chatId, long messageId, int views) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.views = views;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageContentOpened extends Update {
        public static final int CONSTRUCTOR = -1520523131;
        public long chatId;
        public long messageId;

        public UpdateMessageContentOpened() {
        }

        public UpdateMessageContentOpened(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageMentionRead extends Update {
        public static final int CONSTRUCTOR = -252228282;
        public long chatId;
        public long messageId;
        public int unreadMentionCount;

        public UpdateMessageMentionRead() {
        }

        public UpdateMessageMentionRead(long chatId, long messageId, int unreadMentionCount) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.unreadMentionCount = unreadMentionCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateMessageLiveLocationViewed extends Update {
        public static final int CONSTRUCTOR = -1308260971;
        public long chatId;
        public long messageId;

        public UpdateMessageLiveLocationViewed() {
        }

        public UpdateMessageLiveLocationViewed(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewChat extends Update {
        public static final int CONSTRUCTOR = 2075757773;
        public Chat chat;

        public UpdateNewChat() {
        }

        public UpdateNewChat(Chat chat) {
            this.chat = chat;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatChatList extends Update {
        public static final int CONSTRUCTOR = -170455894;
        public long chatId;
        public ChatList chatList;

        public UpdateChatChatList() {
        }

        public UpdateChatChatList(long chatId, ChatList chatList) {
            this.chatId = chatId;
            this.chatList = chatList;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatTitle extends Update {
        public static final int CONSTRUCTOR = -175405660;
        public long chatId;
        public String title;

        public UpdateChatTitle() {
        }

        public UpdateChatTitle(long chatId, String title) {
            this.chatId = chatId;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatPhoto extends Update {
        public static final int CONSTRUCTOR = -209353966;
        public long chatId;
        public ChatPhoto photo;

        public UpdateChatPhoto() {
        }

        public UpdateChatPhoto(long chatId, ChatPhoto photo) {
            this.chatId = chatId;
            this.photo = photo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatPermissions extends Update {
        public static final int CONSTRUCTOR = -1622010003;
        public long chatId;
        public ChatPermissions permissions;

        public UpdateChatPermissions() {
        }

        public UpdateChatPermissions(long chatId, ChatPermissions permissions) {
            this.chatId = chatId;
            this.permissions = permissions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatLastMessage extends Update {
        public static final int CONSTRUCTOR = 580348828;
        public long chatId;
        public Message lastMessage;
        public long order;

        public UpdateChatLastMessage() {
        }

        public UpdateChatLastMessage(long chatId, Message lastMessage, long order) {
            this.chatId = chatId;
            this.lastMessage = lastMessage;
            this.order = order;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatOrder extends Update {
        public static final int CONSTRUCTOR = -1601888026;
        public long chatId;
        public long order;

        public UpdateChatOrder() {
        }

        public UpdateChatOrder(long chatId, long order) {
            this.chatId = chatId;
            this.order = order;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatIsPinned extends Update {
        public static final int CONSTRUCTOR = 488876260;
        public long chatId;
        public boolean isPinned;
        public long order;

        public UpdateChatIsPinned() {
        }

        public UpdateChatIsPinned(long chatId, boolean isPinned, long order) {
            this.chatId = chatId;
            this.isPinned = isPinned;
            this.order = order;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatIsMarkedAsUnread extends Update {
        public static final int CONSTRUCTOR = 1468347188;
        public long chatId;
        public boolean isMarkedAsUnread;

        public UpdateChatIsMarkedAsUnread() {
        }

        public UpdateChatIsMarkedAsUnread(long chatId, boolean isMarkedAsUnread) {
            this.chatId = chatId;
            this.isMarkedAsUnread = isMarkedAsUnread;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatIsSponsored extends Update {
        public static final int CONSTRUCTOR = -1196180070;
        public long chatId;
        public boolean isSponsored;
        public long order;

        public UpdateChatIsSponsored() {
        }

        public UpdateChatIsSponsored(long chatId, boolean isSponsored, long order) {
            this.chatId = chatId;
            this.isSponsored = isSponsored;
            this.order = order;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatHasScheduledMessages extends Update {
        public static final int CONSTRUCTOR = 2064958167;
        public long chatId;
        public boolean hasScheduledMessages;

        public UpdateChatHasScheduledMessages() {
        }

        public UpdateChatHasScheduledMessages(long chatId, boolean hasScheduledMessages) {
            this.chatId = chatId;
            this.hasScheduledMessages = hasScheduledMessages;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatDefaultDisableNotification extends Update {
        public static final int CONSTRUCTOR = 464087707;
        public long chatId;
        public boolean defaultDisableNotification;

        public UpdateChatDefaultDisableNotification() {
        }

        public UpdateChatDefaultDisableNotification(long chatId, boolean defaultDisableNotification) {
            this.chatId = chatId;
            this.defaultDisableNotification = defaultDisableNotification;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatReadInbox extends Update {
        public static final int CONSTRUCTOR = -797952281;
        public long chatId;
        public long lastReadInboxMessageId;
        public int unreadCount;

        public UpdateChatReadInbox() {
        }

        public UpdateChatReadInbox(long chatId, long lastReadInboxMessageId, int unreadCount) {
            this.chatId = chatId;
            this.lastReadInboxMessageId = lastReadInboxMessageId;
            this.unreadCount = unreadCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatReadOutbox extends Update {
        public static final int CONSTRUCTOR = 708334213;
        public long chatId;
        public long lastReadOutboxMessageId;

        public UpdateChatReadOutbox() {
        }

        public UpdateChatReadOutbox(long chatId, long lastReadOutboxMessageId) {
            this.chatId = chatId;
            this.lastReadOutboxMessageId = lastReadOutboxMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatUnreadMentionCount extends Update {
        public static final int CONSTRUCTOR = -2131461348;
        public long chatId;
        public int unreadMentionCount;

        public UpdateChatUnreadMentionCount() {
        }

        public UpdateChatUnreadMentionCount(long chatId, int unreadMentionCount) {
            this.chatId = chatId;
            this.unreadMentionCount = unreadMentionCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatNotificationSettings extends Update {
        public static final int CONSTRUCTOR = -803163050;
        public long chatId;
        public ChatNotificationSettings notificationSettings;

        public UpdateChatNotificationSettings() {
        }

        public UpdateChatNotificationSettings(long chatId, ChatNotificationSettings notificationSettings) {
            this.chatId = chatId;
            this.notificationSettings = notificationSettings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateScopeNotificationSettings extends Update {
        public static final int CONSTRUCTOR = -1203975309;
        public ScopeNotificationSettings notificationSettings;
        public NotificationSettingsScope scope;

        public UpdateScopeNotificationSettings() {
        }

        public UpdateScopeNotificationSettings(NotificationSettingsScope scope, ScopeNotificationSettings notificationSettings) {
            this.scope = scope;
            this.notificationSettings = notificationSettings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatActionBar extends Update {
        public static final int CONSTRUCTOR = -643671870;
        public ChatActionBar actionBar;
        public long chatId;

        public UpdateChatActionBar() {
        }

        public UpdateChatActionBar(long chatId, ChatActionBar actionBar) {
            this.chatId = chatId;
            this.actionBar = actionBar;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatPinnedMessage extends Update {
        public static final int CONSTRUCTOR = 802160507;
        public long chatId;
        public long pinnedMessageId;

        public UpdateChatPinnedMessage() {
        }

        public UpdateChatPinnedMessage(long chatId, long pinnedMessageId) {
            this.chatId = chatId;
            this.pinnedMessageId = pinnedMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatReplyMarkup extends Update {
        public static final int CONSTRUCTOR = 1309386144;
        public long chatId;
        public long replyMarkupMessageId;

        public UpdateChatReplyMarkup() {
        }

        public UpdateChatReplyMarkup(long chatId, long replyMarkupMessageId) {
            this.chatId = chatId;
            this.replyMarkupMessageId = replyMarkupMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatDraftMessage extends Update {
        public static final int CONSTRUCTOR = -1436617498;
        public long chatId;
        public DraftMessage draftMessage;
        public long order;

        public UpdateChatDraftMessage() {
        }

        public UpdateChatDraftMessage(long chatId, DraftMessage draftMessage, long order) {
            this.chatId = chatId;
            this.draftMessage = draftMessage;
            this.order = order;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateChatOnlineMemberCount extends Update {
        public static final int CONSTRUCTOR = 487369373;
        public long chatId;
        public int onlineMemberCount;

        public UpdateChatOnlineMemberCount() {
        }

        public UpdateChatOnlineMemberCount(long chatId, int onlineMemberCount) {
            this.chatId = chatId;
            this.onlineMemberCount = onlineMemberCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNotification extends Update {
        public static final int CONSTRUCTOR = -1897496876;
        public Notification notification;
        public int notificationGroupId;

        public UpdateNotification() {
        }

        public UpdateNotification(int notificationGroupId, Notification notification) {
            this.notificationGroupId = notificationGroupId;
            this.notification = notification;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNotificationGroup extends Update {
        public static final int CONSTRUCTOR = -2049005665;
        public Notification[] addedNotifications;
        public long chatId;
        public boolean isSilent;
        public int notificationGroupId;
        public long notificationSettingsChatId;
        public int[] removedNotificationIds;
        public int totalCount;
        public NotificationGroupType type;

        public UpdateNotificationGroup() {
        }

        public UpdateNotificationGroup(int notificationGroupId, NotificationGroupType type, long chatId, long notificationSettingsChatId, boolean isSilent, int totalCount, Notification[] addedNotifications, int[] removedNotificationIds) {
            this.notificationGroupId = notificationGroupId;
            this.type = type;
            this.chatId = chatId;
            this.notificationSettingsChatId = notificationSettingsChatId;
            this.isSilent = isSilent;
            this.totalCount = totalCount;
            this.addedNotifications = addedNotifications;
            this.removedNotificationIds = removedNotificationIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateActiveNotifications extends Update {
        public static final int CONSTRUCTOR = -1306672221;
        public NotificationGroup[] groups;

        public UpdateActiveNotifications() {
        }

        public UpdateActiveNotifications(NotificationGroup[] groups) {
            this.groups = groups;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateHavePendingNotifications extends Update {
        public static final int CONSTRUCTOR = 179233243;
        public boolean haveDelayedNotifications;
        public boolean haveUnreceivedNotifications;

        public UpdateHavePendingNotifications() {
        }

        public UpdateHavePendingNotifications(boolean haveDelayedNotifications, boolean haveUnreceivedNotifications) {
            this.haveDelayedNotifications = haveDelayedNotifications;
            this.haveUnreceivedNotifications = haveUnreceivedNotifications;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateDeleteMessages extends Update {
        public static final int CONSTRUCTOR = 1669252686;
        public long chatId;
        public boolean fromCache;
        public boolean isPermanent;
        public long[] messageIds;

        public UpdateDeleteMessages() {
        }

        public UpdateDeleteMessages(long chatId, long[] messageIds, boolean isPermanent, boolean fromCache) {
            this.chatId = chatId;
            this.messageIds = messageIds;
            this.isPermanent = isPermanent;
            this.fromCache = fromCache;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUserChatAction extends Update {
        public static final int CONSTRUCTOR = 1444133514;
        public ChatAction action;
        public long chatId;
        public int userId;

        public UpdateUserChatAction() {
        }

        public UpdateUserChatAction(long chatId, int userId, ChatAction action) {
            this.chatId = chatId;
            this.userId = userId;
            this.action = action;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUserStatus extends Update {
        public static final int CONSTRUCTOR = -1443545195;
        public UserStatus status;
        public int userId;

        public UpdateUserStatus() {
        }

        public UpdateUserStatus(int userId, UserStatus status) {
            this.userId = userId;
            this.status = status;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUser extends Update {
        public static final int CONSTRUCTOR = 1183394041;
        public User user;

        public UpdateUser() {
        }

        public UpdateUser(User user) {
            this.user = user;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateBasicGroup extends Update {
        public static final int CONSTRUCTOR = -1003239581;
        public BasicGroup basicGroup;

        public UpdateBasicGroup() {
        }

        public UpdateBasicGroup(BasicGroup basicGroup) {
            this.basicGroup = basicGroup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateSupergroup extends Update {
        public static final int CONSTRUCTOR = -76782300;
        public Supergroup supergroup;

        public UpdateSupergroup() {
        }

        public UpdateSupergroup(Supergroup supergroup) {
            this.supergroup = supergroup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateSecretChat extends Update {
        public static final int CONSTRUCTOR = -1666903253;
        public SecretChat secretChat;

        public UpdateSecretChat() {
        }

        public UpdateSecretChat(SecretChat secretChat) {
            this.secretChat = secretChat;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUserFullInfo extends Update {
        public static final int CONSTRUCTOR = 222103874;
        public UserFullInfo userFullInfo;
        public int userId;

        public UpdateUserFullInfo() {
        }

        public UpdateUserFullInfo(int userId, UserFullInfo userFullInfo) {
            this.userId = userId;
            this.userFullInfo = userFullInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateBasicGroupFullInfo extends Update {
        public static final int CONSTRUCTOR = 924030531;
        public BasicGroupFullInfo basicGroupFullInfo;
        public int basicGroupId;

        public UpdateBasicGroupFullInfo() {
        }

        public UpdateBasicGroupFullInfo(int basicGroupId, BasicGroupFullInfo basicGroupFullInfo) {
            this.basicGroupId = basicGroupId;
            this.basicGroupFullInfo = basicGroupFullInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateSupergroupFullInfo extends Update {
        public static final int CONSTRUCTOR = 1288828758;
        public SupergroupFullInfo supergroupFullInfo;
        public int supergroupId;

        public UpdateSupergroupFullInfo() {
        }

        public UpdateSupergroupFullInfo(int supergroupId, SupergroupFullInfo supergroupFullInfo) {
            this.supergroupId = supergroupId;
            this.supergroupFullInfo = supergroupFullInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateServiceNotification extends Update {
        public static final int CONSTRUCTOR = 1318622637;
        public MessageContent content;
        public String type;

        public UpdateServiceNotification() {
        }

        public UpdateServiceNotification(String type, MessageContent content) {
            this.type = type;
            this.content = content;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateFile extends Update {
        public static final int CONSTRUCTOR = 114132831;
        public File file;

        public UpdateFile() {
        }

        public UpdateFile(File file) {
            this.file = file;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateFileGenerationStart extends Update {
        public static final int CONSTRUCTOR = 216817388;
        public String conversion;
        public String destinationPath;
        public long generationId;
        public String originalPath;

        public UpdateFileGenerationStart() {
        }

        public UpdateFileGenerationStart(long generationId, String originalPath, String destinationPath, String conversion) {
            this.generationId = generationId;
            this.originalPath = originalPath;
            this.destinationPath = destinationPath;
            this.conversion = conversion;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateFileGenerationStop extends Update {
        public static final int CONSTRUCTOR = -1894449685;
        public long generationId;

        public UpdateFileGenerationStop() {
        }

        public UpdateFileGenerationStop(long generationId) {
            this.generationId = generationId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateCall extends Update {
        public static final int CONSTRUCTOR = 1337184477;
        public Call call;

        public UpdateCall() {
        }

        public UpdateCall(Call call) {
            this.call = call;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUserPrivacySettingRules extends Update {
        public static final int CONSTRUCTOR = -912960778;
        public UserPrivacySettingRules rules;
        public UserPrivacySetting setting;

        public UpdateUserPrivacySettingRules() {
        }

        public UpdateUserPrivacySettingRules(UserPrivacySetting setting, UserPrivacySettingRules rules) {
            this.setting = setting;
            this.rules = rules;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUnreadMessageCount extends Update {
        public static final int CONSTRUCTOR = 78987721;
        public ChatList chatList;
        public int unreadCount;
        public int unreadUnmutedCount;

        public UpdateUnreadMessageCount() {
        }

        public UpdateUnreadMessageCount(ChatList chatList, int unreadCount, int unreadUnmutedCount) {
            this.chatList = chatList;
            this.unreadCount = unreadCount;
            this.unreadUnmutedCount = unreadUnmutedCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUnreadChatCount extends Update {
        public static final int CONSTRUCTOR = 1994494530;
        public ChatList chatList;
        public int markedAsUnreadCount;
        public int markedAsUnreadUnmutedCount;
        public int totalCount;
        public int unreadCount;
        public int unreadUnmutedCount;

        public UpdateUnreadChatCount() {
        }

        public UpdateUnreadChatCount(ChatList chatList, int totalCount, int unreadCount, int unreadUnmutedCount, int markedAsUnreadCount, int markedAsUnreadUnmutedCount) {
            this.chatList = chatList;
            this.totalCount = totalCount;
            this.unreadCount = unreadCount;
            this.unreadUnmutedCount = unreadUnmutedCount;
            this.markedAsUnreadCount = markedAsUnreadCount;
            this.markedAsUnreadUnmutedCount = markedAsUnreadUnmutedCount;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateOption extends Update {
        public static final int CONSTRUCTOR = 900822020;
        public String name;
        public OptionValue value;

        public UpdateOption() {
        }

        public UpdateOption(String name, OptionValue value) {
            this.name = name;
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateInstalledStickerSets extends Update {
        public static final int CONSTRUCTOR = 1125575977;
        public boolean isMasks;
        public long[] stickerSetIds;

        public UpdateInstalledStickerSets() {
        }

        public UpdateInstalledStickerSets(boolean isMasks, long[] stickerSetIds) {
            this.isMasks = isMasks;
            this.stickerSetIds = stickerSetIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateTrendingStickerSets extends Update {
        public static final int CONSTRUCTOR = 450714593;
        public StickerSets stickerSets;

        public UpdateTrendingStickerSets() {
        }

        public UpdateTrendingStickerSets(StickerSets stickerSets) {
            this.stickerSets = stickerSets;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateRecentStickers extends Update {
        public static final int CONSTRUCTOR = 1906403540;
        public boolean isAttached;
        public int[] stickerIds;

        public UpdateRecentStickers() {
        }

        public UpdateRecentStickers(boolean isAttached, int[] stickerIds) {
            this.isAttached = isAttached;
            this.stickerIds = stickerIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateFavoriteStickers extends Update {
        public static final int CONSTRUCTOR = 1662240999;
        public int[] stickerIds;

        public UpdateFavoriteStickers() {
        }

        public UpdateFavoriteStickers(int[] stickerIds) {
            this.stickerIds = stickerIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateSavedAnimations extends Update {
        public static final int CONSTRUCTOR = 65563814;
        public int[] animationIds;

        public UpdateSavedAnimations() {
        }

        public UpdateSavedAnimations(int[] animationIds) {
            this.animationIds = animationIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateSelectedBackground extends Update {
        public static final int CONSTRUCTOR = -1715658659;
        public Background background;
        public boolean forDarkTheme;

        public UpdateSelectedBackground() {
        }

        public UpdateSelectedBackground(boolean forDarkTheme, Background background) {
            this.forDarkTheme = forDarkTheme;
            this.background = background;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateLanguagePackStrings extends Update {
        public static final int CONSTRUCTOR = -1056319886;
        public String languagePackId;
        public String localizationTarget;
        public LanguagePackString[] strings;

        public UpdateLanguagePackStrings() {
        }

        public UpdateLanguagePackStrings(String localizationTarget, String languagePackId, LanguagePackString[] strings) {
            this.localizationTarget = localizationTarget;
            this.languagePackId = languagePackId;
            this.strings = strings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateConnectionState extends Update {
        public static final int CONSTRUCTOR = 1469292078;
        public ConnectionState state;

        public UpdateConnectionState() {
        }

        public UpdateConnectionState(ConnectionState state) {
            this.state = state;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateTermsOfService extends Update {
        public static final int CONSTRUCTOR = -1304640162;
        public TermsOfService termsOfService;
        public String termsOfServiceId;

        public UpdateTermsOfService() {
        }

        public UpdateTermsOfService(String termsOfServiceId, TermsOfService termsOfService) {
            this.termsOfServiceId = termsOfServiceId;
            this.termsOfService = termsOfService;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateUsersNearby extends Update {
        public static final int CONSTRUCTOR = -1517109163;
        public ChatNearby[] usersNearby;

        public UpdateUsersNearby() {
        }

        public UpdateUsersNearby(ChatNearby[] usersNearby) {
            this.usersNearby = usersNearby;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewInlineQuery extends Update {
        public static final int CONSTRUCTOR = 2064730634;
        public long id;
        public String offset;
        public String query;
        public int senderUserId;
        public Location userLocation;

        public UpdateNewInlineQuery() {
        }

        public UpdateNewInlineQuery(long id, int senderUserId, Location userLocation, String query, String offset) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.userLocation = userLocation;
            this.query = query;
            this.offset = offset;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewChosenInlineResult extends Update {
        public static final int CONSTRUCTOR = 527526965;
        public String inlineMessageId;
        public String query;
        public String resultId;
        public int senderUserId;
        public Location userLocation;

        public UpdateNewChosenInlineResult() {
        }

        public UpdateNewChosenInlineResult(int senderUserId, Location userLocation, String query, String resultId, String inlineMessageId) {
            this.senderUserId = senderUserId;
            this.userLocation = userLocation;
            this.query = query;
            this.resultId = resultId;
            this.inlineMessageId = inlineMessageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewCallbackQuery extends Update {
        public static final int CONSTRUCTOR = -2044226370;
        public long chatId;
        public long chatInstance;
        public long id;
        public long messageId;
        public CallbackQueryPayload payload;
        public int senderUserId;

        public UpdateNewCallbackQuery() {
        }

        public UpdateNewCallbackQuery(long id, int senderUserId, long chatId, long messageId, long chatInstance, CallbackQueryPayload payload) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.chatId = chatId;
            this.messageId = messageId;
            this.chatInstance = chatInstance;
            this.payload = payload;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewInlineCallbackQuery extends Update {
        public static final int CONSTRUCTOR = -1879154829;
        public long chatInstance;
        public long id;
        public String inlineMessageId;
        public CallbackQueryPayload payload;
        public int senderUserId;

        public UpdateNewInlineCallbackQuery() {
        }

        public UpdateNewInlineCallbackQuery(long id, int senderUserId, String inlineMessageId, long chatInstance, CallbackQueryPayload payload) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.inlineMessageId = inlineMessageId;
            this.chatInstance = chatInstance;
            this.payload = payload;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewShippingQuery extends Update {
        public static final int CONSTRUCTOR = -817474682;
        public long id;
        public String invoicePayload;
        public int senderUserId;
        public Address shippingAddress;

        public UpdateNewShippingQuery() {
        }

        public UpdateNewShippingQuery(long id, int senderUserId, String invoicePayload, Address shippingAddress) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.invoicePayload = invoicePayload;
            this.shippingAddress = shippingAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewPreCheckoutQuery extends Update {
        public static final int CONSTRUCTOR = 87964006;
        public String currency;
        public long id;
        public byte[] invoicePayload;
        public OrderInfo orderInfo;
        public int senderUserId;
        public String shippingOptionId;
        public long totalAmount;

        public UpdateNewPreCheckoutQuery() {
        }

        public UpdateNewPreCheckoutQuery(long id, int senderUserId, String currency, long totalAmount, byte[] invoicePayload, String shippingOptionId, OrderInfo orderInfo) {
            this.id = id;
            this.senderUserId = senderUserId;
            this.currency = currency;
            this.totalAmount = totalAmount;
            this.invoicePayload = invoicePayload;
            this.shippingOptionId = shippingOptionId;
            this.orderInfo = orderInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewCustomEvent extends Update {
        public static final int CONSTRUCTOR = 1994222092;
        public String event;

        public UpdateNewCustomEvent() {
        }

        public UpdateNewCustomEvent(String event) {
            this.event = event;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdateNewCustomQuery extends Update {
        public static final int CONSTRUCTOR = -687670874;
        public String data;
        public long id;
        public int timeout;

        public UpdateNewCustomQuery() {
        }

        public UpdateNewCustomQuery(long id, String data, int timeout) {
            this.id = id;
            this.data = data;
            this.timeout = timeout;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdatePoll extends Update {
        public static final int CONSTRUCTOR = -1771342902;
        public Poll poll;

        public UpdatePoll() {
        }

        public UpdatePoll(Poll poll) {
            this.poll = poll;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpdatePollAnswer extends Update {
        public static final int CONSTRUCTOR = 1606139344;
        public int[] optionIds;
        public long pollId;
        public int userId;

        public UpdatePollAnswer() {
        }

        public UpdatePollAnswer(long pollId, int userId, int[] optionIds) {
            this.pollId = pollId;
            this.userId = userId;
            this.optionIds = optionIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Update, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Updates extends Object {
        public static final int CONSTRUCTOR = 475842347;
        public Update[] updates;

        public Updates() {
        }

        public Updates(Update[] updates) {
            this.updates = updates;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class User extends Object {
        public static final int CONSTRUCTOR = -824771497;
        public String firstName;
        public boolean haveAccess;
        public int id;
        public boolean isContact;
        public boolean isMutualContact;
        public boolean isScam;
        public boolean isSupport;
        public boolean isVerified;
        public String languageCode;
        public String lastName;
        public String phoneNumber;
        public ProfilePhoto profilePhoto;
        public String restrictionReason;
        public UserStatus status;
        public UserType type;
        public String username;

        public User() {
        }

        public User(int id, String firstName, String lastName, String username, String phoneNumber, UserStatus status, ProfilePhoto profilePhoto, boolean isContact, boolean isMutualContact, boolean isVerified, boolean isSupport, String restrictionReason, boolean isScam, boolean haveAccess, UserType type, String languageCode) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.username = username;
            this.phoneNumber = phoneNumber;
            this.status = status;
            this.profilePhoto = profilePhoto;
            this.isContact = isContact;
            this.isMutualContact = isMutualContact;
            this.isVerified = isVerified;
            this.isSupport = isSupport;
            this.restrictionReason = restrictionReason;
            this.isScam = isScam;
            this.haveAccess = haveAccess;
            this.type = type;
            this.languageCode = languageCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserFullInfo extends Object {
        public static final int CONSTRUCTOR = 333888500;
        public String bio;
        public BotInfo botInfo;
        public boolean canBeCalled;
        public int groupInCommonCount;
        public boolean hasPrivateCalls;
        public boolean isBlocked;
        public boolean needPhoneNumberPrivacyException;
        public String shareText;

        public UserFullInfo() {
        }

        public UserFullInfo(boolean isBlocked, boolean canBeCalled, boolean hasPrivateCalls, boolean needPhoneNumberPrivacyException, String bio, String shareText, int groupInCommonCount, BotInfo botInfo) {
            this.isBlocked = isBlocked;
            this.canBeCalled = canBeCalled;
            this.hasPrivateCalls = hasPrivateCalls;
            this.needPhoneNumberPrivacyException = needPhoneNumberPrivacyException;
            this.bio = bio;
            this.shareText = shareText;
            this.groupInCommonCount = groupInCommonCount;
            this.botInfo = botInfo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingShowStatus extends UserPrivacySetting {
        public static final int CONSTRUCTOR = 1862829310;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingShowProfilePhoto extends UserPrivacySetting {
        public static final int CONSTRUCTOR = 1408485877;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingShowLinkInForwardedMessages extends UserPrivacySetting {
        public static final int CONSTRUCTOR = 592688870;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingShowPhoneNumber extends UserPrivacySetting {
        public static final int CONSTRUCTOR = -791567831;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingAllowChatInvites extends UserPrivacySetting {
        public static final int CONSTRUCTOR = 1271668007;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingAllowCalls extends UserPrivacySetting {
        public static final int CONSTRUCTOR = -906967291;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingAllowPeerToPeerCalls extends UserPrivacySetting {
        public static final int CONSTRUCTOR = 352500032;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingAllowFindingByPhoneNumber extends UserPrivacySetting {
        public static final int CONSTRUCTOR = -1846645423;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySetting, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleAllowAll extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = -1967186881;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleAllowContacts extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = -1892733680;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleAllowUsers extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = 427601278;
        public int[] userIds;

        public UserPrivacySettingRuleAllowUsers() {
        }

        public UserPrivacySettingRuleAllowUsers(int[] userIds) {
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleAllowChatMembers extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = -2048749863;
        public long[] chatIds;

        public UserPrivacySettingRuleAllowChatMembers() {
        }

        public UserPrivacySettingRuleAllowChatMembers(long[] chatIds) {
            this.chatIds = chatIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleRestrictAll extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = -1406495408;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleRestrictContacts extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = 1008389378;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleRestrictUsers extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = 2119951802;
        public int[] userIds;

        public UserPrivacySettingRuleRestrictUsers() {
        }

        public UserPrivacySettingRuleRestrictUsers(int[] userIds) {
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRuleRestrictChatMembers extends UserPrivacySettingRule {
        public static final int CONSTRUCTOR = 392530897;
        public long[] chatIds;

        public UserPrivacySettingRuleRestrictChatMembers() {
        }

        public UserPrivacySettingRuleRestrictChatMembers(long[] chatIds) {
            this.chatIds = chatIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserPrivacySettingRule, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserPrivacySettingRules extends Object {
        public static final int CONSTRUCTOR = 322477541;
        public UserPrivacySettingRule[] rules;

        public UserPrivacySettingRules() {
        }

        public UserPrivacySettingRules(UserPrivacySettingRule[] rules) {
            this.rules = rules;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserProfilePhoto extends Object {
        public static final int CONSTRUCTOR = -1882596466;
        public int addedDate;
        public long id;
        public PhotoSize[] sizes;

        public UserProfilePhoto() {
        }

        public UserProfilePhoto(long id, int addedDate, PhotoSize[] sizes) {
            this.id = id;
            this.addedDate = addedDate;
            this.sizes = sizes;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserProfilePhotos extends Object {
        public static final int CONSTRUCTOR = 1512709690;
        public UserProfilePhoto[] photos;
        public int totalCount;

        public UserProfilePhotos() {
        }

        public UserProfilePhotos(int totalCount, UserProfilePhoto[] photos) {
            this.totalCount = totalCount;
            this.photos = photos;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusEmpty extends UserStatus {
        public static final int CONSTRUCTOR = 164646985;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusOnline extends UserStatus {
        public static final int CONSTRUCTOR = -1529460876;
        public int expires;

        public UserStatusOnline() {
        }

        public UserStatusOnline(int expires) {
            this.expires = expires;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusOffline extends UserStatus {
        public static final int CONSTRUCTOR = -759984891;
        public int wasOnline;

        public UserStatusOffline() {
        }

        public UserStatusOffline(int wasOnline) {
            this.wasOnline = wasOnline;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusRecently extends UserStatus {
        public static final int CONSTRUCTOR = -496024847;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusLastWeek extends UserStatus {
        public static final int CONSTRUCTOR = 129960444;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserStatusLastMonth extends UserStatus {
        public static final int CONSTRUCTOR = 2011940674;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserStatus, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserTypeRegular extends UserType {
        public static final int CONSTRUCTOR = -598644325;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserTypeDeleted extends UserType {
        public static final int CONSTRUCTOR = -1807729372;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserTypeBot extends UserType {
        public static final int CONSTRUCTOR = 1262387765;
        public boolean canJoinGroups;
        public boolean canReadAllGroupMessages;
        public String inlineQueryPlaceholder;
        public boolean isInline;
        public boolean needLocation;

        public UserTypeBot() {
        }

        public UserTypeBot(boolean canJoinGroups, boolean canReadAllGroupMessages, boolean isInline, String inlineQueryPlaceholder, boolean needLocation) {
            this.canJoinGroups = canJoinGroups;
            this.canReadAllGroupMessages = canReadAllGroupMessages;
            this.isInline = isInline;
            this.inlineQueryPlaceholder = inlineQueryPlaceholder;
            this.needLocation = needLocation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UserTypeUnknown extends UserType {
        public static final int CONSTRUCTOR = -724541123;

        @Override // org.drinkless.td.libcore.telegram.TdApi.UserType, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Users extends Object {
        public static final int CONSTRUCTOR = 273760088;
        public int totalCount;
        public int[] userIds;

        public Users() {
        }

        public Users(int totalCount, int[] userIds) {
            this.totalCount = totalCount;
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ValidatedOrderInfo extends Object {
        public static final int CONSTRUCTOR = 1511451484;
        public String orderInfoId;
        public ShippingOption[] shippingOptions;

        public ValidatedOrderInfo() {
        }

        public ValidatedOrderInfo(String orderInfoId, ShippingOption[] shippingOptions) {
            this.orderInfoId = orderInfoId;
            this.shippingOptions = shippingOptions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Venue extends Object {
        public static final int CONSTRUCTOR = 1070406393;
        public String address;
        public String id;
        public Location location;
        public String provider;
        public String title;
        public String type;

        public Venue() {
        }

        public Venue(Location location, String title, String address, String provider, String id, String type) {
            this.location = location;
            this.title = title;
            this.address = address;
            this.provider = provider;
            this.id = id;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Video extends Object {
        public static final int CONSTRUCTOR = -536898740;
        public int duration;
        public String fileName;
        public boolean hasStickers;
        public int height;
        public String mimeType;
        public Minithumbnail minithumbnail;
        public boolean supportsStreaming;
        public PhotoSize thumbnail;
        public File video;
        public int width;

        public Video() {
        }

        public Video(int duration, int width, int height, String fileName, String mimeType, boolean hasStickers, boolean supportsStreaming, Minithumbnail minithumbnail, PhotoSize thumbnail, File video) {
            this.duration = duration;
            this.width = width;
            this.height = height;
            this.fileName = fileName;
            this.mimeType = mimeType;
            this.hasStickers = hasStickers;
            this.supportsStreaming = supportsStreaming;
            this.minithumbnail = minithumbnail;
            this.thumbnail = thumbnail;
            this.video = video;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class VideoNote extends Object {
        public static final int CONSTRUCTOR = -1080075672;
        public int duration;
        public int length;
        public Minithumbnail minithumbnail;
        public PhotoSize thumbnail;
        public File video;

        public VideoNote() {
        }

        public VideoNote(int duration, int length, Minithumbnail minithumbnail, PhotoSize thumbnail, File video) {
            this.duration = duration;
            this.length = length;
            this.minithumbnail = minithumbnail;
            this.thumbnail = thumbnail;
            this.video = video;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class VoiceNote extends Object {
        public static final int CONSTRUCTOR = -2066012058;
        public int duration;
        public String mimeType;
        public File voice;
        public byte[] waveform;

        public VoiceNote() {
        }

        public VoiceNote(int duration, byte[] waveform, String mimeType, File voice) {
            this.duration = duration;
            this.waveform = waveform;
            this.mimeType = mimeType;
            this.voice = voice;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class WebPage extends Object {
        public static final int CONSTRUCTOR = 1092898169;
        public Animation animation;
        public Audio audio;
        public String author;
        public String description;
        public String displayUrl;
        public Document document;
        public int duration;
        public int embedHeight;
        public String embedType;
        public String embedUrl;
        public int embedWidth;
        public int instantViewVersion;
        public Photo photo;
        public String siteName;
        public Sticker sticker;
        public String title;
        public String type;
        public String url;
        public Video video;
        public VideoNote videoNote;
        public VoiceNote voiceNote;

        public WebPage() {
        }

        public WebPage(String url, String displayUrl, String type, String siteName, String title, String description, Photo photo, String embedUrl, String embedType, int embedWidth, int embedHeight, int duration, String author, Animation animation, Audio audio, Document document, Sticker sticker, Video video, VideoNote videoNote, VoiceNote voiceNote, int instantViewVersion) {
            this.url = url;
            this.displayUrl = displayUrl;
            this.type = type;
            this.siteName = siteName;
            this.title = title;
            this.description = description;
            this.photo = photo;
            this.embedUrl = embedUrl;
            this.embedType = embedType;
            this.embedWidth = embedWidth;
            this.embedHeight = embedHeight;
            this.duration = duration;
            this.author = author;
            this.animation = animation;
            this.audio = audio;
            this.document = document;
            this.sticker = sticker;
            this.video = video;
            this.videoNote = videoNote;
            this.voiceNote = voiceNote;
            this.instantViewVersion = instantViewVersion;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class WebPageInstantView extends Object {
        public static final int CONSTRUCTOR = 957287214;
        public boolean isFull;
        public boolean isRtl;
        public PageBlock[] pageBlocks;
        public String url;
        public int version;

        public WebPageInstantView() {
        }

        public WebPageInstantView(PageBlock[] pageBlocks, int version, String url, boolean isRtl, boolean isFull) {
            this.pageBlocks = pageBlocks;
            this.version = version;
            this.url = url;
            this.isRtl = isRtl;
            this.isFull = isFull;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AcceptCall extends Function {
        public static final int CONSTRUCTOR = -646618416;
        public int callId;
        public CallProtocol protocol;

        public AcceptCall() {
        }

        public AcceptCall(int callId, CallProtocol protocol) {
            this.callId = callId;
            this.protocol = protocol;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AcceptTermsOfService extends Function {
        public static final int CONSTRUCTOR = 2130576356;
        public String termsOfServiceId;

        public AcceptTermsOfService() {
        }

        public AcceptTermsOfService(String termsOfServiceId) {
            this.termsOfServiceId = termsOfServiceId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddChatMember extends Function {
        public static final int CONSTRUCTOR = 1182817962;
        public long chatId;
        public int forwardLimit;
        public int userId;

        public AddChatMember() {
        }

        public AddChatMember(long chatId, int userId, int forwardLimit) {
            this.chatId = chatId;
            this.userId = userId;
            this.forwardLimit = forwardLimit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddChatMembers extends Function {
        public static final int CONSTRUCTOR = 1234094617;
        public long chatId;
        public int[] userIds;

        public AddChatMembers() {
        }

        public AddChatMembers(long chatId, int[] userIds) {
            this.chatId = chatId;
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddContact extends Function {
        public static final int CONSTRUCTOR = 1869640000;
        public Contact contact;
        public boolean sharePhoneNumber;

        public AddContact() {
        }

        public AddContact(Contact contact, boolean sharePhoneNumber) {
            this.contact = contact;
            this.sharePhoneNumber = sharePhoneNumber;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddCustomServerLanguagePack extends Function {
        public static final int CONSTRUCTOR = 4492771;
        public String languagePackId;

        public AddCustomServerLanguagePack() {
        }

        public AddCustomServerLanguagePack(String languagePackId) {
            this.languagePackId = languagePackId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddFavoriteSticker extends Function {
        public static final int CONSTRUCTOR = 324504799;
        public InputFile sticker;

        public AddFavoriteSticker() {
        }

        public AddFavoriteSticker(InputFile sticker) {
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddLocalMessage extends Function {
        public static final int CONSTRUCTOR = -348943149;
        public long chatId;
        public boolean disableNotification;
        public InputMessageContent inputMessageContent;
        public long replyToMessageId;
        public int senderUserId;

        public AddLocalMessage() {
        }

        public AddLocalMessage(long chatId, int senderUserId, long replyToMessageId, boolean disableNotification, InputMessageContent inputMessageContent) {
            this.chatId = chatId;
            this.senderUserId = senderUserId;
            this.replyToMessageId = replyToMessageId;
            this.disableNotification = disableNotification;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddLogMessage extends Function {
        public static final int CONSTRUCTOR = 1597427692;
        public String text;
        public int verbosityLevel;

        public AddLogMessage() {
        }

        public AddLogMessage(int verbosityLevel, String text) {
            this.verbosityLevel = verbosityLevel;
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddNetworkStatistics extends Function {
        public static final int CONSTRUCTOR = 1264825305;
        public NetworkStatisticsEntry entry;

        public AddNetworkStatistics() {
        }

        public AddNetworkStatistics(NetworkStatisticsEntry entry) {
            this.entry = entry;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddProxy extends Function {
        public static final int CONSTRUCTOR = 331529432;
        public boolean enable;
        public int port;
        public String server;
        public ProxyType type;

        public AddProxy() {
        }

        public AddProxy(String server, int port, boolean enable, ProxyType type) {
            this.server = server;
            this.port = port;
            this.enable = enable;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddRecentSticker extends Function {
        public static final int CONSTRUCTOR = -1478109026;
        public boolean isAttached;
        public InputFile sticker;

        public AddRecentSticker() {
        }

        public AddRecentSticker(boolean isAttached, InputFile sticker) {
            this.isAttached = isAttached;
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddRecentlyFoundChat extends Function {
        public static final int CONSTRUCTOR = -1746396787;
        public long chatId;

        public AddRecentlyFoundChat() {
        }

        public AddRecentlyFoundChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddSavedAnimation extends Function {
        public static final int CONSTRUCTOR = -1538525088;
        public InputFile animation;

        public AddSavedAnimation() {
        }

        public AddSavedAnimation(InputFile animation) {
            this.animation = animation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AddStickerToSet extends Function {
        public static final int CONSTRUCTOR = 1422402800;
        public String name;
        public InputSticker sticker;
        public int userId;

        public AddStickerToSet() {
        }

        public AddStickerToSet(int userId, String name, InputSticker sticker) {
            this.userId = userId;
            this.name = name;
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AnswerCallbackQuery extends Function {
        public static final int CONSTRUCTOR = -1153028490;
        public int cacheTime;
        public long callbackQueryId;
        public boolean showAlert;
        public String text;
        public String url;

        public AnswerCallbackQuery() {
        }

        public AnswerCallbackQuery(long callbackQueryId, String text, boolean showAlert, String url, int cacheTime) {
            this.callbackQueryId = callbackQueryId;
            this.text = text;
            this.showAlert = showAlert;
            this.url = url;
            this.cacheTime = cacheTime;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AnswerCustomQuery extends Function {
        public static final int CONSTRUCTOR = -1293603521;
        public long customQueryId;
        public String data;

        public AnswerCustomQuery() {
        }

        public AnswerCustomQuery(long customQueryId, String data) {
            this.customQueryId = customQueryId;
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AnswerInlineQuery extends Function {
        public static final int CONSTRUCTOR = 485879477;
        public int cacheTime;
        public long inlineQueryId;
        public boolean isPersonal;
        public String nextOffset;
        public InputInlineQueryResult[] results;
        public String switchPmParameter;
        public String switchPmText;

        public AnswerInlineQuery() {
        }

        public AnswerInlineQuery(long inlineQueryId, boolean isPersonal, InputInlineQueryResult[] results, int cacheTime, String nextOffset, String switchPmText, String switchPmParameter) {
            this.inlineQueryId = inlineQueryId;
            this.isPersonal = isPersonal;
            this.results = results;
            this.cacheTime = cacheTime;
            this.nextOffset = nextOffset;
            this.switchPmText = switchPmText;
            this.switchPmParameter = switchPmParameter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AnswerPreCheckoutQuery extends Function {
        public static final int CONSTRUCTOR = -1486789653;
        public String errorMessage;
        public long preCheckoutQueryId;

        public AnswerPreCheckoutQuery() {
        }

        public AnswerPreCheckoutQuery(long preCheckoutQueryId, String errorMessage) {
            this.preCheckoutQueryId = preCheckoutQueryId;
            this.errorMessage = errorMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class AnswerShippingQuery extends Function {
        public static final int CONSTRUCTOR = -434601324;
        public String errorMessage;
        public ShippingOption[] shippingOptions;
        public long shippingQueryId;

        public AnswerShippingQuery() {
        }

        public AnswerShippingQuery(long shippingQueryId, ShippingOption[] shippingOptions, String errorMessage) {
            this.shippingQueryId = shippingQueryId;
            this.shippingOptions = shippingOptions;
            this.errorMessage = errorMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class BlockUser extends Function {
        public static final int CONSTRUCTOR = -1239315139;
        public int userId;

        public BlockUser() {
        }

        public BlockUser(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CanTransferOwnership extends Function {
        public static final int CONSTRUCTOR = 634602508;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CancelDownloadFile extends Function {
        public static final int CONSTRUCTOR = -1954524450;
        public int fileId;
        public boolean onlyIfPending;

        public CancelDownloadFile() {
        }

        public CancelDownloadFile(int fileId, boolean onlyIfPending) {
            this.fileId = fileId;
            this.onlyIfPending = onlyIfPending;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CancelUploadFile extends Function {
        public static final int CONSTRUCTOR = 1623539600;
        public int fileId;

        public CancelUploadFile() {
        }

        public CancelUploadFile(int fileId) {
            this.fileId = fileId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChangeImportedContacts extends Function {
        public static final int CONSTRUCTOR = 1968207955;
        public Contact[] contacts;

        public ChangeImportedContacts() {
        }

        public ChangeImportedContacts(Contact[] contacts) {
            this.contacts = contacts;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChangePhoneNumber extends Function {
        public static final int CONSTRUCTOR = -124666973;
        public String phoneNumber;
        public PhoneNumberAuthenticationSettings settings;

        public ChangePhoneNumber() {
        }

        public ChangePhoneNumber(String phoneNumber, PhoneNumberAuthenticationSettings settings) {
            this.phoneNumber = phoneNumber;
            this.settings = settings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ChangeStickerSet extends Function {
        public static final int CONSTRUCTOR = 449357293;
        public boolean isArchived;
        public boolean isInstalled;
        public long setId;

        public ChangeStickerSet() {
        }

        public ChangeStickerSet(long setId, boolean isInstalled, boolean isArchived) {
            this.setId = setId;
            this.isInstalled = isInstalled;
            this.isArchived = isArchived;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckAuthenticationBotToken extends Function {
        public static final int CONSTRUCTOR = 639321206;
        public String token;

        public CheckAuthenticationBotToken() {
        }

        public CheckAuthenticationBotToken(String token) {
            this.token = token;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckAuthenticationCode extends Function {
        public static final int CONSTRUCTOR = -302103382;
        public String code;

        public CheckAuthenticationCode() {
        }

        public CheckAuthenticationCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckAuthenticationPassword extends Function {
        public static final int CONSTRUCTOR = -2025698400;
        public String password;

        public CheckAuthenticationPassword() {
        }

        public CheckAuthenticationPassword(String password) {
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChangePhoneNumberCode extends Function {
        public static final int CONSTRUCTOR = -1720278429;
        public String code;

        public CheckChangePhoneNumberCode() {
        }

        public CheckChangePhoneNumberCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatInviteLink extends Function {
        public static final int CONSTRUCTOR = -496940997;
        public String inviteLink;

        public CheckChatInviteLink() {
        }

        public CheckChatInviteLink(String inviteLink) {
            this.inviteLink = inviteLink;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckChatUsername extends Function {
        public static final int CONSTRUCTOR = -119119344;
        public long chatId;
        public String username;

        public CheckChatUsername() {
        }

        public CheckChatUsername(long chatId, String username) {
            this.chatId = chatId;
            this.username = username;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckCreatedPublicChatsLimit extends Function {
        public static final int CONSTRUCTOR = -445546591;
        public PublicChatType type;

        public CheckCreatedPublicChatsLimit() {
        }

        public CheckCreatedPublicChatsLimit(PublicChatType type) {
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckDatabaseEncryptionKey extends Function {
        public static final int CONSTRUCTOR = 1018769307;
        public byte[] encryptionKey;

        public CheckDatabaseEncryptionKey() {
        }

        public CheckDatabaseEncryptionKey(byte[] encryptionKey) {
            this.encryptionKey = encryptionKey;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckEmailAddressVerificationCode extends Function {
        public static final int CONSTRUCTOR = -426386685;
        public String code;

        public CheckEmailAddressVerificationCode() {
        }

        public CheckEmailAddressVerificationCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckPhoneNumberConfirmationCode extends Function {
        public static final int CONSTRUCTOR = -1348060966;
        public String code;

        public CheckPhoneNumberConfirmationCode() {
        }

        public CheckPhoneNumberConfirmationCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckPhoneNumberVerificationCode extends Function {
        public static final int CONSTRUCTOR = 1497462718;
        public String code;

        public CheckPhoneNumberVerificationCode() {
        }

        public CheckPhoneNumberVerificationCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CheckRecoveryEmailAddressCode extends Function {
        public static final int CONSTRUCTOR = -1997039589;
        public String code;

        public CheckRecoveryEmailAddressCode() {
        }

        public CheckRecoveryEmailAddressCode(String code) {
            this.code = code;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CleanFileName extends Function {
        public static final int CONSTRUCTOR = 967964667;
        public String fileName;

        public CleanFileName() {
        }

        public CleanFileName(String fileName) {
            this.fileName = fileName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ClearAllDraftMessages extends Function {
        public static final int CONSTRUCTOR = -46369573;
        public boolean excludeSecretChats;

        public ClearAllDraftMessages() {
        }

        public ClearAllDraftMessages(boolean excludeSecretChats) {
            this.excludeSecretChats = excludeSecretChats;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ClearImportedContacts extends Function {
        public static final int CONSTRUCTOR = 869503298;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ClearRecentStickers extends Function {
        public static final int CONSTRUCTOR = -321242684;
        public boolean isAttached;

        public ClearRecentStickers() {
        }

        public ClearRecentStickers(boolean isAttached) {
            this.isAttached = isAttached;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ClearRecentlyFoundChats extends Function {
        public static final int CONSTRUCTOR = -285582542;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Close extends Function {
        public static final int CONSTRUCTOR = -1187782273;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CloseChat extends Function {
        public static final int CONSTRUCTOR = 39749353;
        public long chatId;

        public CloseChat() {
        }

        public CloseChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CloseSecretChat extends Function {
        public static final int CONSTRUCTOR = -471006133;
        public int secretChatId;

        public CloseSecretChat() {
        }

        public CloseSecretChat(int secretChatId) {
            this.secretChatId = secretChatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ConfirmQrCodeAuthentication extends Function {
        public static final int CONSTRUCTOR = -376199379;
        public String link;

        public ConfirmQrCodeAuthentication() {
        }

        public ConfirmQrCodeAuthentication(String link) {
            this.link = link;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateBasicGroupChat extends Function {
        public static final int CONSTRUCTOR = 642492777;
        public int basicGroupId;
        public boolean force;

        public CreateBasicGroupChat() {
        }

        public CreateBasicGroupChat(int basicGroupId, boolean force) {
            this.basicGroupId = basicGroupId;
            this.force = force;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateCall extends Function {
        public static final int CONSTRUCTOR = -1742408159;
        public CallProtocol protocol;
        public int userId;

        public CreateCall() {
        }

        public CreateCall(int userId, CallProtocol protocol) {
            this.userId = userId;
            this.protocol = protocol;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateNewBasicGroupChat extends Function {
        public static final int CONSTRUCTOR = 1874532069;
        public String title;
        public int[] userIds;

        public CreateNewBasicGroupChat() {
        }

        public CreateNewBasicGroupChat(int[] userIds, String title) {
            this.userIds = userIds;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateNewSecretChat extends Function {
        public static final int CONSTRUCTOR = 1689344881;
        public int userId;

        public CreateNewSecretChat() {
        }

        public CreateNewSecretChat(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateNewStickerSet extends Function {
        public static final int CONSTRUCTOR = 205093058;
        public boolean isMasks;
        public String name;
        public InputSticker[] stickers;
        public String title;
        public int userId;

        public CreateNewStickerSet() {
        }

        public CreateNewStickerSet(int userId, String title, String name, boolean isMasks, InputSticker[] stickers) {
            this.userId = userId;
            this.title = title;
            this.name = name;
            this.isMasks = isMasks;
            this.stickers = stickers;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateNewSupergroupChat extends Function {
        public static final int CONSTRUCTOR = -8999251;
        public String description;
        public boolean isChannel;
        public ChatLocation location;
        public String title;

        public CreateNewSupergroupChat() {
        }

        public CreateNewSupergroupChat(String title, boolean isChannel, String description, ChatLocation location) {
            this.title = title;
            this.isChannel = isChannel;
            this.description = description;
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreatePrivateChat extends Function {
        public static final int CONSTRUCTOR = -1807530364;
        public boolean force;
        public int userId;

        public CreatePrivateChat() {
        }

        public CreatePrivateChat(int userId, boolean force) {
            this.userId = userId;
            this.force = force;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateSecretChat extends Function {
        public static final int CONSTRUCTOR = 1930285615;
        public int secretChatId;

        public CreateSecretChat() {
        }

        public CreateSecretChat(int secretChatId) {
            this.secretChatId = secretChatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateSupergroupChat extends Function {
        public static final int CONSTRUCTOR = 352742758;
        public boolean force;
        public int supergroupId;

        public CreateSupergroupChat() {
        }

        public CreateSupergroupChat(int supergroupId, boolean force) {
            this.supergroupId = supergroupId;
            this.force = force;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class CreateTemporaryPassword extends Function {
        public static final int CONSTRUCTOR = -1626509434;
        public String password;
        public int validFor;

        public CreateTemporaryPassword() {
        }

        public CreateTemporaryPassword(String password, int validFor) {
            this.password = password;
            this.validFor = validFor;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteAccount extends Function {
        public static final int CONSTRUCTOR = -1203056508;
        public String reason;

        public DeleteAccount() {
        }

        public DeleteAccount(String reason) {
            this.reason = reason;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteChatHistory extends Function {
        public static final int CONSTRUCTOR = -1472081761;
        public long chatId;
        public boolean removeFromChatList;
        public boolean revoke;

        public DeleteChatHistory() {
        }

        public DeleteChatHistory(long chatId, boolean removeFromChatList, boolean revoke) {
            this.chatId = chatId;
            this.removeFromChatList = removeFromChatList;
            this.revoke = revoke;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteChatMessagesFromUser extends Function {
        public static final int CONSTRUCTOR = -1599689199;
        public long chatId;
        public int userId;

        public DeleteChatMessagesFromUser() {
        }

        public DeleteChatMessagesFromUser(long chatId, int userId) {
            this.chatId = chatId;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteChatReplyMarkup extends Function {
        public static final int CONSTRUCTOR = 100637531;
        public long chatId;
        public long messageId;

        public DeleteChatReplyMarkup() {
        }

        public DeleteChatReplyMarkup(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteFile extends Function {
        public static final int CONSTRUCTOR = 1807653676;
        public int fileId;

        public DeleteFile() {
        }

        public DeleteFile(int fileId) {
            this.fileId = fileId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteLanguagePack extends Function {
        public static final int CONSTRUCTOR = -2108761026;
        public String languagePackId;

        public DeleteLanguagePack() {
        }

        public DeleteLanguagePack(String languagePackId) {
            this.languagePackId = languagePackId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteMessages extends Function {
        public static final int CONSTRUCTOR = 1130090173;
        public long chatId;
        public long[] messageIds;
        public boolean revoke;

        public DeleteMessages() {
        }

        public DeleteMessages(long chatId, long[] messageIds, boolean revoke) {
            this.chatId = chatId;
            this.messageIds = messageIds;
            this.revoke = revoke;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeletePassportElement extends Function {
        public static final int CONSTRUCTOR = -1719555468;
        public PassportElementType type;

        public DeletePassportElement() {
        }

        public DeletePassportElement(PassportElementType type) {
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteProfilePhoto extends Function {
        public static final int CONSTRUCTOR = 1319794625;
        public long profilePhotoId;

        public DeleteProfilePhoto() {
        }

        public DeleteProfilePhoto(long profilePhotoId) {
            this.profilePhotoId = profilePhotoId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteSavedCredentials extends Function {
        public static final int CONSTRUCTOR = 826300114;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteSavedOrderInfo extends Function {
        public static final int CONSTRUCTOR = 1629058164;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DeleteSupergroup extends Function {
        public static final int CONSTRUCTOR = -1999855965;
        public int supergroupId;

        public DeleteSupergroup() {
        }

        public DeleteSupergroup(int supergroupId) {
            this.supergroupId = supergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class Destroy extends Function {
        public static final int CONSTRUCTOR = 685331274;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DisableProxy extends Function {
        public static final int CONSTRUCTOR = -2100095102;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DiscardCall extends Function {
        public static final int CONSTRUCTOR = -923187372;
        public int callId;
        public long connectionId;
        public int duration;
        public boolean isDisconnected;

        public DiscardCall() {
        }

        public DiscardCall(int callId, boolean isDisconnected, int duration, long connectionId) {
            this.callId = callId;
            this.isDisconnected = isDisconnected;
            this.duration = duration;
            this.connectionId = connectionId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DisconnectAllWebsites extends Function {
        public static final int CONSTRUCTOR = -1082985981;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DisconnectWebsite extends Function {
        public static final int CONSTRUCTOR = -778767395;
        public long websiteId;

        public DisconnectWebsite() {
        }

        public DisconnectWebsite(long websiteId) {
            this.websiteId = websiteId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class DownloadFile extends Function {
        public static final int CONSTRUCTOR = -1102026662;
        public int fileId;
        public int limit;
        public int offset;
        public int priority;
        public boolean synchronous;

        public DownloadFile() {
        }

        public DownloadFile(int fileId, int priority, int offset, int limit, boolean synchronous) {
            this.fileId = fileId;
            this.priority = priority;
            this.offset = offset;
            this.limit = limit;
            this.synchronous = synchronous;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditCustomLanguagePackInfo extends Function {
        public static final int CONSTRUCTOR = 1320751257;
        public LanguagePackInfo info;

        public EditCustomLanguagePackInfo() {
        }

        public EditCustomLanguagePackInfo(LanguagePackInfo info) {
            this.info = info;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditInlineMessageCaption extends Function {
        public static final int CONSTRUCTOR = -760985929;
        public FormattedText caption;
        public String inlineMessageId;
        public ReplyMarkup replyMarkup;

        public EditInlineMessageCaption() {
        }

        public EditInlineMessageCaption(String inlineMessageId, ReplyMarkup replyMarkup, FormattedText caption) {
            this.inlineMessageId = inlineMessageId;
            this.replyMarkup = replyMarkup;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditInlineMessageLiveLocation extends Function {
        public static final int CONSTRUCTOR = 655046316;
        public String inlineMessageId;
        public Location location;
        public ReplyMarkup replyMarkup;

        public EditInlineMessageLiveLocation() {
        }

        public EditInlineMessageLiveLocation(String inlineMessageId, ReplyMarkup replyMarkup, Location location) {
            this.inlineMessageId = inlineMessageId;
            this.replyMarkup = replyMarkup;
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditInlineMessageMedia extends Function {
        public static final int CONSTRUCTOR = 23553921;
        public String inlineMessageId;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;

        public EditInlineMessageMedia() {
        }

        public EditInlineMessageMedia(String inlineMessageId, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.inlineMessageId = inlineMessageId;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditInlineMessageReplyMarkup extends Function {
        public static final int CONSTRUCTOR = -67565858;
        public String inlineMessageId;
        public ReplyMarkup replyMarkup;

        public EditInlineMessageReplyMarkup() {
        }

        public EditInlineMessageReplyMarkup(String inlineMessageId, ReplyMarkup replyMarkup) {
            this.inlineMessageId = inlineMessageId;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditInlineMessageText extends Function {
        public static final int CONSTRUCTOR = -855457307;
        public String inlineMessageId;
        public InputMessageContent inputMessageContent;
        public ReplyMarkup replyMarkup;

        public EditInlineMessageText() {
        }

        public EditInlineMessageText(String inlineMessageId, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.inlineMessageId = inlineMessageId;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageCaption extends Function {
        public static final int CONSTRUCTOR = 1154677038;
        public FormattedText caption;
        public long chatId;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public EditMessageCaption() {
        }

        public EditMessageCaption(long chatId, long messageId, ReplyMarkup replyMarkup, FormattedText caption) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
            this.caption = caption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageLiveLocation extends Function {
        public static final int CONSTRUCTOR = -1146772745;
        public long chatId;
        public Location location;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public EditMessageLiveLocation() {
        }

        public EditMessageLiveLocation(long chatId, long messageId, ReplyMarkup replyMarkup, Location location) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageMedia extends Function {
        public static final int CONSTRUCTOR = -1152678125;
        public long chatId;
        public InputMessageContent inputMessageContent;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public EditMessageMedia() {
        }

        public EditMessageMedia(long chatId, long messageId, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageReplyMarkup extends Function {
        public static final int CONSTRUCTOR = 332127881;
        public long chatId;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public EditMessageReplyMarkup() {
        }

        public EditMessageReplyMarkup(long chatId, long messageId, ReplyMarkup replyMarkup) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageSchedulingState extends Function {
        public static final int CONSTRUCTOR = -1372976192;
        public long chatId;
        public long messageId;
        public MessageSchedulingState schedulingState;

        public EditMessageSchedulingState() {
        }

        public EditMessageSchedulingState(long chatId, long messageId, MessageSchedulingState schedulingState) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.schedulingState = schedulingState;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditMessageText extends Function {
        public static final int CONSTRUCTOR = 196272567;
        public long chatId;
        public InputMessageContent inputMessageContent;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public EditMessageText() {
        }

        public EditMessageText(long chatId, long messageId, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EditProxy extends Function {
        public static final int CONSTRUCTOR = -1605883821;
        public boolean enable;
        public int port;
        public int proxyId;
        public String server;
        public ProxyType type;

        public EditProxy() {
        }

        public EditProxy(int proxyId, String server, int port, boolean enable, ProxyType type) {
            this.proxyId = proxyId;
            this.server = server;
            this.port = port;
            this.enable = enable;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class EnableProxy extends Function {
        public static final int CONSTRUCTOR = 1494450838;
        public int proxyId;

        public EnableProxy() {
        }

        public EnableProxy(int proxyId) {
            this.proxyId = proxyId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class FinishFileGeneration extends Function {
        public static final int CONSTRUCTOR = -1055060835;
        public Error error;
        public long generationId;

        public FinishFileGeneration() {
        }

        public FinishFileGeneration(long generationId, Error error) {
            this.generationId = generationId;
            this.error = error;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ForwardMessages extends Function {
        public static final int CONSTRUCTOR = -1633531094;
        public boolean asAlbum;
        public long chatId;
        public long fromChatId;
        public long[] messageIds;
        public SendMessageOptions options;
        public boolean removeCaption;
        public boolean sendCopy;

        public ForwardMessages() {
        }

        public ForwardMessages(long chatId, long fromChatId, long[] messageIds, SendMessageOptions options, boolean asAlbum, boolean sendCopy, boolean removeCaption) {
            this.chatId = chatId;
            this.fromChatId = fromChatId;
            this.messageIds = messageIds;
            this.options = options;
            this.asAlbum = asAlbum;
            this.sendCopy = sendCopy;
            this.removeCaption = removeCaption;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GenerateChatInviteLink extends Function {
        public static final int CONSTRUCTOR = 1945532500;
        public long chatId;

        public GenerateChatInviteLink() {
        }

        public GenerateChatInviteLink(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetAccountTtl extends Function {
        public static final int CONSTRUCTOR = -443905161;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetActiveLiveLocationMessages extends Function {
        public static final int CONSTRUCTOR = -1425459567;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetActiveSessions extends Function {
        public static final int CONSTRUCTOR = 1119710526;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetAllPassportElements extends Function {
        public static final int CONSTRUCTOR = -2038945045;
        public String password;

        public GetAllPassportElements() {
        }

        public GetAllPassportElements(String password) {
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetApplicationConfig extends Function {
        public static final int CONSTRUCTOR = -1823144318;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetArchivedStickerSets extends Function {
        public static final int CONSTRUCTOR = 1996943238;
        public boolean isMasks;
        public int limit;
        public long offsetStickerSetId;

        public GetArchivedStickerSets() {
        }

        public GetArchivedStickerSets(boolean isMasks, long offsetStickerSetId, int limit) {
            this.isMasks = isMasks;
            this.offsetStickerSetId = offsetStickerSetId;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetAttachedStickerSets extends Function {
        public static final int CONSTRUCTOR = 1302172429;
        public int fileId;

        public GetAttachedStickerSets() {
        }

        public GetAttachedStickerSets(int fileId) {
            this.fileId = fileId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetAuthorizationState extends Function {
        public static final int CONSTRUCTOR = 1949154877;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetAutoDownloadSettingsPresets extends Function {
        public static final int CONSTRUCTOR = -1721088201;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetBackgroundUrl extends Function {
        public static final int CONSTRUCTOR = 733769682;
        public String name;
        public BackgroundType type;

        public GetBackgroundUrl() {
        }

        public GetBackgroundUrl(String name, BackgroundType type) {
            this.name = name;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetBackgrounds extends Function {
        public static final int CONSTRUCTOR = 249072633;
        public boolean forDarkTheme;

        public GetBackgrounds() {
        }

        public GetBackgrounds(boolean forDarkTheme) {
            this.forDarkTheme = forDarkTheme;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetBasicGroup extends Function {
        public static final int CONSTRUCTOR = 561775568;
        public int basicGroupId;

        public GetBasicGroup() {
        }

        public GetBasicGroup(int basicGroupId) {
            this.basicGroupId = basicGroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetBasicGroupFullInfo extends Function {
        public static final int CONSTRUCTOR = 1770517905;
        public int basicGroupId;

        public GetBasicGroupFullInfo() {
        }

        public GetBasicGroupFullInfo(int basicGroupId) {
            this.basicGroupId = basicGroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetBlockedUsers extends Function {
        public static final int CONSTRUCTOR = -742912777;
        public int limit;
        public int offset;

        public GetBlockedUsers() {
        }

        public GetBlockedUsers(int offset, int limit) {
            this.offset = offset;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetCallbackQueryAnswer extends Function {
        public static final int CONSTRUCTOR = 116357727;
        public long chatId;
        public long messageId;
        public CallbackQueryPayload payload;

        public GetCallbackQueryAnswer() {
        }

        public GetCallbackQueryAnswer(long chatId, long messageId, CallbackQueryPayload payload) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.payload = payload;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChat extends Function {
        public static final int CONSTRUCTOR = 1866601536;
        public long chatId;

        public GetChat() {
        }

        public GetChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatAdministrators extends Function {
        public static final int CONSTRUCTOR = 1544468155;
        public long chatId;

        public GetChatAdministrators() {
        }

        public GetChatAdministrators(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatEventLog extends Function {
        public static final int CONSTRUCTOR = 206900967;
        public long chatId;
        public ChatEventLogFilters filters;
        public long fromEventId;
        public int limit;
        public String query;
        public int[] userIds;

        public GetChatEventLog() {
        }

        public GetChatEventLog(long chatId, String query, long fromEventId, int limit, ChatEventLogFilters filters, int[] userIds) {
            this.chatId = chatId;
            this.query = query;
            this.fromEventId = fromEventId;
            this.limit = limit;
            this.filters = filters;
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatHistory extends Function {
        public static final int CONSTRUCTOR = -799960451;
        public long chatId;
        public long fromMessageId;
        public int limit;
        public int offset;
        public boolean onlyLocal;

        public GetChatHistory() {
        }

        public GetChatHistory(long chatId, long fromMessageId, int offset, int limit, boolean onlyLocal) {
            this.chatId = chatId;
            this.fromMessageId = fromMessageId;
            this.offset = offset;
            this.limit = limit;
            this.onlyLocal = onlyLocal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatMember extends Function {
        public static final int CONSTRUCTOR = 677085892;
        public long chatId;
        public int userId;

        public GetChatMember() {
        }

        public GetChatMember(long chatId, int userId) {
            this.chatId = chatId;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatMessageByDate extends Function {
        public static final int CONSTRUCTOR = 1062564150;
        public long chatId;
        public int date;

        public GetChatMessageByDate() {
        }

        public GetChatMessageByDate(long chatId, int date) {
            this.chatId = chatId;
            this.date = date;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatMessageCount extends Function {
        public static final int CONSTRUCTOR = 205435308;
        public long chatId;
        public SearchMessagesFilter filter;
        public boolean returnLocal;

        public GetChatMessageCount() {
        }

        public GetChatMessageCount(long chatId, SearchMessagesFilter filter, boolean returnLocal) {
            this.chatId = chatId;
            this.filter = filter;
            this.returnLocal = returnLocal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatNotificationSettingsExceptions extends Function {
        public static final int CONSTRUCTOR = 201199121;
        public boolean compareSound;
        public NotificationSettingsScope scope;

        public GetChatNotificationSettingsExceptions() {
        }

        public GetChatNotificationSettingsExceptions(NotificationSettingsScope scope, boolean compareSound) {
            this.scope = scope;
            this.compareSound = compareSound;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatPinnedMessage extends Function {
        public static final int CONSTRUCTOR = 359865008;
        public long chatId;

        public GetChatPinnedMessage() {
        }

        public GetChatPinnedMessage(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatScheduledMessages extends Function {
        public static final int CONSTRUCTOR = -549638149;
        public long chatId;

        public GetChatScheduledMessages() {
        }

        public GetChatScheduledMessages(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChatStatisticsUrl extends Function {
        public static final int CONSTRUCTOR = 1114621183;
        public long chatId;
        public boolean isDark;
        public String parameters;

        public GetChatStatisticsUrl() {
        }

        public GetChatStatisticsUrl(long chatId, String parameters, boolean isDark) {
            this.chatId = chatId;
            this.parameters = parameters;
            this.isDark = isDark;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetChats extends Function {
        public static final int CONSTRUCTOR = 1847129537;
        public ChatList chatList;
        public int limit;
        public long offsetChatId;
        public long offsetOrder;

        public GetChats() {
        }

        public GetChats(long offsetOrder, long offsetChatId, int limit) {
            this.chatList = this.chatList;
            this.offsetOrder = offsetOrder;
            this.offsetChatId = offsetChatId;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetConnectedWebsites extends Function {
        public static final int CONSTRUCTOR = -170536110;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetContacts extends Function {
        public static final int CONSTRUCTOR = -1417722768;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetCountryCode extends Function {
        public static final int CONSTRUCTOR = 1540593906;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetCreatedPublicChats extends Function {
        public static final int CONSTRUCTOR = 710354415;
        public PublicChatType type;

        public GetCreatedPublicChats() {
        }

        public GetCreatedPublicChats(PublicChatType type) {
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetCurrentState extends Function {
        public static final int CONSTRUCTOR = -1191417719;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetDatabaseStatistics extends Function {
        public static final int CONSTRUCTOR = -1942760263;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetDeepLinkInfo extends Function {
        public static final int CONSTRUCTOR = 680673150;
        public String link;

        public GetDeepLinkInfo() {
        }

        public GetDeepLinkInfo(String link) {
            this.link = link;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetEmojiSuggestionsUrl extends Function {
        public static final int CONSTRUCTOR = -1404101841;
        public String languageCode;

        public GetEmojiSuggestionsUrl() {
        }

        public GetEmojiSuggestionsUrl(String languageCode) {
            this.languageCode = languageCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetFavoriteStickers extends Function {
        public static final int CONSTRUCTOR = -338964672;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetFile extends Function {
        public static final int CONSTRUCTOR = 1553923406;
        public int fileId;

        public GetFile() {
        }

        public GetFile(int fileId) {
            this.fileId = fileId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetFileDownloadedPrefixSize extends Function {
        public static final int CONSTRUCTOR = -1668864864;
        public int fileId;
        public int offset;

        public GetFileDownloadedPrefixSize() {
        }

        public GetFileDownloadedPrefixSize(int fileId, int offset) {
            this.fileId = fileId;
            this.offset = offset;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetFileExtension extends Function {
        public static final int CONSTRUCTOR = -106055372;
        public String mimeType;

        public GetFileExtension() {
        }

        public GetFileExtension(String mimeType) {
            this.mimeType = mimeType;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetFileMimeType extends Function {
        public static final int CONSTRUCTOR = -2073879671;
        public String fileName;

        public GetFileMimeType() {
        }

        public GetFileMimeType(String fileName) {
            this.fileName = fileName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetGameHighScores extends Function {
        public static final int CONSTRUCTOR = 1920923753;
        public long chatId;
        public long messageId;
        public int userId;

        public GetGameHighScores() {
        }

        public GetGameHighScores(long chatId, long messageId, int userId) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetGroupsInCommon extends Function {
        public static final int CONSTRUCTOR = -23238689;
        public int limit;
        public long offsetChatId;
        public int userId;

        public GetGroupsInCommon() {
        }

        public GetGroupsInCommon(int userId, long offsetChatId, int limit) {
            this.userId = userId;
            this.offsetChatId = offsetChatId;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetImportedContactCount extends Function {
        public static final int CONSTRUCTOR = -656336346;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetInactiveSupergroupChats extends Function {
        public static final int CONSTRUCTOR = -657720907;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetInlineGameHighScores extends Function {
        public static final int CONSTRUCTOR = -1833445800;
        public String inlineMessageId;
        public int userId;

        public GetInlineGameHighScores() {
        }

        public GetInlineGameHighScores(String inlineMessageId, int userId) {
            this.inlineMessageId = inlineMessageId;
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetInlineQueryResults extends Function {
        public static final int CONSTRUCTOR = -1182511172;
        public int botUserId;
        public long chatId;
        public String offset;
        public String query;
        public Location userLocation;

        public GetInlineQueryResults() {
        }

        public GetInlineQueryResults(int botUserId, long chatId, Location userLocation, String query, String offset) {
            this.botUserId = botUserId;
            this.chatId = chatId;
            this.userLocation = userLocation;
            this.query = query;
            this.offset = offset;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetInstalledStickerSets extends Function {
        public static final int CONSTRUCTOR = 1214523749;
        public boolean isMasks;

        public GetInstalledStickerSets() {
        }

        public GetInstalledStickerSets(boolean isMasks) {
            this.isMasks = isMasks;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetInviteText extends Function {
        public static final int CONSTRUCTOR = 794573512;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetJsonString extends Function {
        public static final int CONSTRUCTOR = 663458849;
        public JsonValue jsonValue;

        public GetJsonString() {
        }

        public GetJsonString(JsonValue jsonValue) {
            this.jsonValue = jsonValue;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetJsonValue extends Function {
        public static final int CONSTRUCTOR = -1829086715;
        public String json;

        public GetJsonValue() {
        }

        public GetJsonValue(String json) {
            this.json = json;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLanguagePackInfo extends Function {
        public static final int CONSTRUCTOR = 2077809320;
        public String languagePackId;

        public GetLanguagePackInfo() {
        }

        public GetLanguagePackInfo(String languagePackId) {
            this.languagePackId = languagePackId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLanguagePackString extends Function {
        public static final int CONSTRUCTOR = 150789747;
        public String key;
        public String languagePackDatabasePath;
        public String languagePackId;
        public String localizationTarget;

        public GetLanguagePackString() {
        }

        public GetLanguagePackString(String languagePackDatabasePath, String localizationTarget, String languagePackId, String key) {
            this.languagePackDatabasePath = languagePackDatabasePath;
            this.localizationTarget = localizationTarget;
            this.languagePackId = languagePackId;
            this.key = key;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLanguagePackStrings extends Function {
        public static final int CONSTRUCTOR = 1246259088;
        public String[] keys;
        public String languagePackId;

        public GetLanguagePackStrings() {
        }

        public GetLanguagePackStrings(String languagePackId, String[] keys) {
            this.languagePackId = languagePackId;
            this.keys = keys;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLocalizationTargetInfo extends Function {
        public static final int CONSTRUCTOR = 1849499526;
        public boolean onlyLocal;

        public GetLocalizationTargetInfo() {
        }

        public GetLocalizationTargetInfo(boolean onlyLocal) {
            this.onlyLocal = onlyLocal;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLogStream extends Function {
        public static final int CONSTRUCTOR = 1167608667;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLogTagVerbosityLevel extends Function {
        public static final int CONSTRUCTOR = 951004547;
        public String tag;

        public GetLogTagVerbosityLevel() {
        }

        public GetLogTagVerbosityLevel(String tag) {
            this.tag = tag;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLogTags extends Function {
        public static final int CONSTRUCTOR = -254449190;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLogVerbosityLevel extends Function {
        public static final int CONSTRUCTOR = 594057956;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLoginUrl extends Function {
        public static final int CONSTRUCTOR = 694973925;
        public boolean allowWriteAccess;
        public int buttonId;
        public long chatId;
        public long messageId;

        public GetLoginUrl() {
        }

        public GetLoginUrl(long chatId, long messageId, int buttonId, boolean allowWriteAccess) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.buttonId = buttonId;
            this.allowWriteAccess = allowWriteAccess;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetLoginUrlInfo extends Function {
        public static final int CONSTRUCTOR = -980042966;
        public int buttonId;
        public long chatId;
        public long messageId;

        public GetLoginUrlInfo() {
        }

        public GetLoginUrlInfo(long chatId, long messageId, int buttonId) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.buttonId = buttonId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMapThumbnailFile extends Function {
        public static final int CONSTRUCTOR = -152660070;
        public long chatId;
        public int height;
        public Location location;
        public int scale;
        public int width;
        public int zoom;

        public GetMapThumbnailFile() {
        }

        public GetMapThumbnailFile(Location location, int zoom, int width, int height, int scale, long chatId) {
            this.location = location;
            this.zoom = zoom;
            this.width = width;
            this.height = height;
            this.scale = scale;
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMe extends Function {
        public static final int CONSTRUCTOR = -191516033;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMessage extends Function {
        public static final int CONSTRUCTOR = -1821196160;
        public long chatId;
        public long messageId;

        public GetMessage() {
        }

        public GetMessage(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMessageLink extends Function {
        public static final int CONSTRUCTOR = 1362732326;
        public long chatId;
        public long messageId;

        public GetMessageLink() {
        }

        public GetMessageLink(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMessageLinkInfo extends Function {
        public static final int CONSTRUCTOR = -700533672;
        public String url;

        public GetMessageLinkInfo() {
        }

        public GetMessageLinkInfo(String url) {
            this.url = url;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMessageLocally extends Function {
        public static final int CONSTRUCTOR = -603575444;
        public long chatId;
        public long messageId;

        public GetMessageLocally() {
        }

        public GetMessageLocally(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetMessages extends Function {
        public static final int CONSTRUCTOR = 425299338;
        public long chatId;
        public long[] messageIds;

        public GetMessages() {
        }

        public GetMessages(long chatId, long[] messageIds) {
            this.chatId = chatId;
            this.messageIds = messageIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetNetworkStatistics extends Function {
        public static final int CONSTRUCTOR = -986228706;
        public boolean onlyCurrent;

        public GetNetworkStatistics() {
        }

        public GetNetworkStatistics(boolean onlyCurrent) {
            this.onlyCurrent = onlyCurrent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetOption extends Function {
        public static final int CONSTRUCTOR = -1572495746;
        public String name;

        public GetOption() {
        }

        public GetOption(String name) {
            this.name = name;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPassportAuthorizationForm extends Function {
        public static final int CONSTRUCTOR = -1468394095;
        public int botUserId;
        public String nonce;
        public String publicKey;
        public String scope;

        public GetPassportAuthorizationForm() {
        }

        public GetPassportAuthorizationForm(int botUserId, String scope, String publicKey, String nonce) {
            this.botUserId = botUserId;
            this.scope = scope;
            this.publicKey = publicKey;
            this.nonce = nonce;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPassportAuthorizationFormAvailableElements extends Function {
        public static final int CONSTRUCTOR = 1738134754;
        public int autorizationFormId;
        public String password;

        public GetPassportAuthorizationFormAvailableElements() {
        }

        public GetPassportAuthorizationFormAvailableElements(int autorizationFormId, String password) {
            this.autorizationFormId = autorizationFormId;
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPassportElement extends Function {
        public static final int CONSTRUCTOR = -1882398342;
        public String password;
        public PassportElementType type;

        public GetPassportElement() {
        }

        public GetPassportElement(PassportElementType type, String password) {
            this.type = type;
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPasswordState extends Function {
        public static final int CONSTRUCTOR = -174752904;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPaymentForm extends Function {
        public static final int CONSTRUCTOR = -2146950882;
        public long chatId;
        public long messageId;

        public GetPaymentForm() {
        }

        public GetPaymentForm(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPaymentReceipt extends Function {
        public static final int CONSTRUCTOR = 1013758294;
        public long chatId;
        public long messageId;

        public GetPaymentReceipt() {
        }

        public GetPaymentReceipt(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPollVoters extends Function {
        public static final int CONSTRUCTOR = 2075288734;
        public long chatId;
        public int limit;
        public long messageId;
        public int offset;
        public int optionId;

        public GetPollVoters() {
        }

        public GetPollVoters(long chatId, long messageId, int optionId, int offset, int limit) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.optionId = optionId;
            this.offset = offset;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPreferredCountryLanguage extends Function {
        public static final int CONSTRUCTOR = -933049386;
        public String countryCode;

        public GetPreferredCountryLanguage() {
        }

        public GetPreferredCountryLanguage(String countryCode) {
            this.countryCode = countryCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetProxies extends Function {
        public static final int CONSTRUCTOR = -95026381;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetProxyLink extends Function {
        public static final int CONSTRUCTOR = -1285597664;
        public int proxyId;

        public GetProxyLink() {
        }

        public GetProxyLink(int proxyId) {
            this.proxyId = proxyId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPublicMessageLink extends Function {
        public static final int CONSTRUCTOR = -374642839;
        public long chatId;
        public boolean forAlbum;
        public long messageId;

        public GetPublicMessageLink() {
        }

        public GetPublicMessageLink(long chatId, long messageId, boolean forAlbum) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.forAlbum = forAlbum;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetPushReceiverId extends Function {
        public static final int CONSTRUCTOR = -286505294;
        public String payload;

        public GetPushReceiverId() {
        }

        public GetPushReceiverId(String payload) {
            this.payload = payload;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRecentInlineBots extends Function {
        public static final int CONSTRUCTOR = 1437823548;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRecentStickers extends Function {
        public static final int CONSTRUCTOR = -579622241;
        public boolean isAttached;

        public GetRecentStickers() {
        }

        public GetRecentStickers(boolean isAttached) {
            this.isAttached = isAttached;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRecentlyVisitedTMeUrls extends Function {
        public static final int CONSTRUCTOR = 806754961;
        public String referrer;

        public GetRecentlyVisitedTMeUrls() {
        }

        public GetRecentlyVisitedTMeUrls(String referrer) {
            this.referrer = referrer;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRecoveryEmailAddress extends Function {
        public static final int CONSTRUCTOR = -1594770947;
        public String password;

        public GetRecoveryEmailAddress() {
        }

        public GetRecoveryEmailAddress(String password) {
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRemoteFile extends Function {
        public static final int CONSTRUCTOR = 2137204530;
        public FileType fileType;
        public String remoteFileId;

        public GetRemoteFile() {
        }

        public GetRemoteFile(String remoteFileId, FileType fileType) {
            this.remoteFileId = remoteFileId;
            this.fileType = fileType;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetRepliedMessage extends Function {
        public static final int CONSTRUCTOR = -641918531;
        public long chatId;
        public long messageId;

        public GetRepliedMessage() {
        }

        public GetRepliedMessage(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSavedAnimations extends Function {
        public static final int CONSTRUCTOR = 7051032;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSavedOrderInfo extends Function {
        public static final int CONSTRUCTOR = -1152016675;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetScopeNotificationSettings extends Function {
        public static final int CONSTRUCTOR = -995613361;
        public NotificationSettingsScope scope;

        public GetScopeNotificationSettings() {
        }

        public GetScopeNotificationSettings(NotificationSettingsScope scope) {
            this.scope = scope;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSecretChat extends Function {
        public static final int CONSTRUCTOR = 40599169;
        public int secretChatId;

        public GetSecretChat() {
        }

        public GetSecretChat(int secretChatId) {
            this.secretChatId = secretChatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetStickerEmojis extends Function {
        public static final int CONSTRUCTOR = -1895508665;
        public InputFile sticker;

        public GetStickerEmojis() {
        }

        public GetStickerEmojis(InputFile sticker) {
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetStickerSet extends Function {
        public static final int CONSTRUCTOR = 1052318659;
        public long setId;

        public GetStickerSet() {
        }

        public GetStickerSet(long setId) {
            this.setId = setId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetStickers extends Function {
        public static final int CONSTRUCTOR = -1594919556;
        public String emoji;
        public int limit;

        public GetStickers() {
        }

        public GetStickers(String emoji, int limit) {
            this.emoji = emoji;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetStorageStatistics extends Function {
        public static final int CONSTRUCTOR = -853193929;
        public int chatLimit;

        public GetStorageStatistics() {
        }

        public GetStorageStatistics(int chatLimit) {
            this.chatLimit = chatLimit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetStorageStatisticsFast extends Function {
        public static final int CONSTRUCTOR = 61368066;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSuitableDiscussionChats extends Function {
        public static final int CONSTRUCTOR = 49044982;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSupergroup extends Function {
        public static final int CONSTRUCTOR = -2063063706;
        public int supergroupId;

        public GetSupergroup() {
        }

        public GetSupergroup(int supergroupId) {
            this.supergroupId = supergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSupergroupFullInfo extends Function {
        public static final int CONSTRUCTOR = -1150331262;
        public int supergroupId;

        public GetSupergroupFullInfo() {
        }

        public GetSupergroupFullInfo(int supergroupId) {
            this.supergroupId = supergroupId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSupergroupMembers extends Function {
        public static final int CONSTRUCTOR = 1427643098;
        public SupergroupMembersFilter filter;
        public int limit;
        public int offset;
        public int supergroupId;

        public GetSupergroupMembers() {
        }

        public GetSupergroupMembers(int supergroupId, SupergroupMembersFilter filter, int offset, int limit) {
            this.supergroupId = supergroupId;
            this.filter = filter;
            this.offset = offset;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetSupportUser extends Function {
        public static final int CONSTRUCTOR = -1733497700;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetTemporaryPasswordState extends Function {
        public static final int CONSTRUCTOR = -12670830;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetTextEntities extends Function {
        public static final int CONSTRUCTOR = -341490693;
        public String text;

        public GetTextEntities() {
        }

        public GetTextEntities(String text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetTopChats extends Function {
        public static final int CONSTRUCTOR = -388410847;
        public TopChatCategory category;
        public int limit;

        public GetTopChats() {
        }

        public GetTopChats(TopChatCategory category, int limit) {
            this.category = category;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetTrendingStickerSets extends Function {
        public static final int CONSTRUCTOR = -1729129957;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetUser extends Function {
        public static final int CONSTRUCTOR = -47586017;
        public int userId;

        public GetUser() {
        }

        public GetUser(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetUserFullInfo extends Function {
        public static final int CONSTRUCTOR = -655443263;
        public int userId;

        public GetUserFullInfo() {
        }

        public GetUserFullInfo(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetUserPrivacySettingRules extends Function {
        public static final int CONSTRUCTOR = -2077223311;
        public UserPrivacySetting setting;

        public GetUserPrivacySettingRules() {
        }

        public GetUserPrivacySettingRules(UserPrivacySetting setting) {
            this.setting = setting;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetUserProfilePhotos extends Function {
        public static final int CONSTRUCTOR = -2062927433;
        public int limit;
        public int offset;
        public int userId;

        public GetUserProfilePhotos() {
        }

        public GetUserProfilePhotos(int userId, int offset, int limit) {
            this.userId = userId;
            this.offset = offset;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetWebPageInstantView extends Function {
        public static final int CONSTRUCTOR = -1962649975;
        public boolean forceFull;
        public String url;

        public GetWebPageInstantView() {
        }

        public GetWebPageInstantView(String url, boolean forceFull) {
            this.url = url;
            this.forceFull = forceFull;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class GetWebPagePreview extends Function {
        public static final int CONSTRUCTOR = 573441580;
        public FormattedText text;

        public GetWebPagePreview() {
        }

        public GetWebPagePreview(FormattedText text) {
            this.text = text;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ImportContacts extends Function {
        public static final int CONSTRUCTOR = -215132767;
        public Contact[] contacts;

        public ImportContacts() {
        }

        public ImportContacts(Contact[] contacts) {
            this.contacts = contacts;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JoinChat extends Function {
        public static final int CONSTRUCTOR = 326769313;
        public long chatId;

        public JoinChat() {
        }

        public JoinChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class JoinChatByInviteLink extends Function {
        public static final int CONSTRUCTOR = -1049973882;
        public String inviteLink;

        public JoinChatByInviteLink() {
        }

        public JoinChatByInviteLink(String inviteLink) {
            this.inviteLink = inviteLink;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LeaveChat extends Function {
        public static final int CONSTRUCTOR = -1825080735;
        public long chatId;

        public LeaveChat() {
        }

        public LeaveChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class LogOut extends Function {
        public static final int CONSTRUCTOR = -1581923301;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OpenChat extends Function {
        public static final int CONSTRUCTOR = -323371509;
        public long chatId;

        public OpenChat() {
        }

        public OpenChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OpenMessageContent extends Function {
        public static final int CONSTRUCTOR = -739088005;
        public long chatId;
        public long messageId;

        public OpenMessageContent() {
        }

        public OpenMessageContent(long chatId, long messageId) {
            this.chatId = chatId;
            this.messageId = messageId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class OptimizeStorage extends Function {
        public static final int CONSTRUCTOR = 980397489;
        public long[] chatIds;
        public int chatLimit;
        public int count;
        public long[] excludeChatIds;
        public FileType[] fileTypes;
        public int immunityDelay;
        public long size;
        public int ttl;

        public OptimizeStorage() {
        }

        public OptimizeStorage(long size, int ttl, int count, int immunityDelay, FileType[] fileTypes, long[] chatIds, long[] excludeChatIds, int chatLimit) {
            this.size = size;
            this.ttl = ttl;
            this.count = count;
            this.immunityDelay = immunityDelay;
            this.fileTypes = fileTypes;
            this.chatIds = chatIds;
            this.excludeChatIds = excludeChatIds;
            this.chatLimit = chatLimit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ParseTextEntities extends Function {
        public static final int CONSTRUCTOR = -1709194593;
        public TextParseMode parseMode;
        public String text;

        public ParseTextEntities() {
        }

        public ParseTextEntities(String text, TextParseMode parseMode) {
            this.text = text;
            this.parseMode = parseMode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PinChatMessage extends Function {
        public static final int CONSTRUCTOR = -554712351;
        public long chatId;
        public boolean disableNotification;
        public long messageId;

        public PinChatMessage() {
        }

        public PinChatMessage(long chatId, long messageId, boolean disableNotification) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.disableNotification = disableNotification;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class PingProxy extends Function {
        public static final int CONSTRUCTOR = -979681103;
        public int proxyId;

        public PingProxy() {
        }

        public PingProxy(int proxyId) {
            this.proxyId = proxyId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ProcessPushNotification extends Function {
        public static final int CONSTRUCTOR = 786679952;
        public String payload;

        public ProcessPushNotification() {
        }

        public ProcessPushNotification(String payload) {
            this.payload = payload;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReadAllChatMentions extends Function {
        public static final int CONSTRUCTOR = 1357558453;
        public long chatId;

        public ReadAllChatMentions() {
        }

        public ReadAllChatMentions(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReadFilePart extends Function {
        public static final int CONSTRUCTOR = -407749314;
        public int count;
        public int fileId;
        public int offset;

        public ReadFilePart() {
        }

        public ReadFilePart(int fileId, int offset, int count) {
            this.fileId = fileId;
            this.offset = offset;
            this.count = count;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RecoverAuthenticationPassword extends Function {
        public static final int CONSTRUCTOR = 787436412;
        public String recoveryCode;

        public RecoverAuthenticationPassword() {
        }

        public RecoverAuthenticationPassword(String recoveryCode) {
            this.recoveryCode = recoveryCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RecoverPassword extends Function {
        public static final int CONSTRUCTOR = 1660185903;
        public String recoveryCode;

        public RecoverPassword() {
        }

        public RecoverPassword(String recoveryCode) {
            this.recoveryCode = recoveryCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RegisterDevice extends Function {
        public static final int CONSTRUCTOR = 1734127493;
        public DeviceToken deviceToken;
        public int[] otherUserIds;

        public RegisterDevice() {
        }

        public RegisterDevice(DeviceToken deviceToken, int[] otherUserIds) {
            this.deviceToken = deviceToken;
            this.otherUserIds = otherUserIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RegisterUser extends Function {
        public static final int CONSTRUCTOR = -109994467;
        public String firstName;
        public String lastName;

        public RegisterUser() {
        }

        public RegisterUser(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveBackground extends Function {
        public static final int CONSTRUCTOR = -1484545642;
        public long backgroundId;

        public RemoveBackground() {
        }

        public RemoveBackground(long backgroundId) {
            this.backgroundId = backgroundId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveChatActionBar extends Function {
        public static final int CONSTRUCTOR = -1650968070;
        public long chatId;

        public RemoveChatActionBar() {
        }

        public RemoveChatActionBar(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveContacts extends Function {
        public static final int CONSTRUCTOR = -615510759;
        public int[] userIds;

        public RemoveContacts() {
        }

        public RemoveContacts(int[] userIds) {
            this.userIds = userIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveFavoriteSticker extends Function {
        public static final int CONSTRUCTOR = 1152945264;
        public InputFile sticker;

        public RemoveFavoriteSticker() {
        }

        public RemoveFavoriteSticker(InputFile sticker) {
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveNotification extends Function {
        public static final int CONSTRUCTOR = 862630734;
        public int notificationGroupId;
        public int notificationId;

        public RemoveNotification() {
        }

        public RemoveNotification(int notificationGroupId, int notificationId) {
            this.notificationGroupId = notificationGroupId;
            this.notificationId = notificationId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveNotificationGroup extends Function {
        public static final int CONSTRUCTOR = 1713005454;
        public int maxNotificationId;
        public int notificationGroupId;

        public RemoveNotificationGroup() {
        }

        public RemoveNotificationGroup(int notificationGroupId, int maxNotificationId) {
            this.notificationGroupId = notificationGroupId;
            this.maxNotificationId = maxNotificationId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveProxy extends Function {
        public static final int CONSTRUCTOR = 1369219847;
        public int proxyId;

        public RemoveProxy() {
        }

        public RemoveProxy(int proxyId) {
            this.proxyId = proxyId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveRecentHashtag extends Function {
        public static final int CONSTRUCTOR = -1013735260;
        public String hashtag;

        public RemoveRecentHashtag() {
        }

        public RemoveRecentHashtag(String hashtag) {
            this.hashtag = hashtag;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveRecentSticker extends Function {
        public static final int CONSTRUCTOR = 1246577677;
        public boolean isAttached;
        public InputFile sticker;

        public RemoveRecentSticker() {
        }

        public RemoveRecentSticker(boolean isAttached, InputFile sticker) {
            this.isAttached = isAttached;
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveRecentlyFoundChat extends Function {
        public static final int CONSTRUCTOR = 717340444;
        public long chatId;

        public RemoveRecentlyFoundChat() {
        }

        public RemoveRecentlyFoundChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveSavedAnimation extends Function {
        public static final int CONSTRUCTOR = -495605479;
        public InputFile animation;

        public RemoveSavedAnimation() {
        }

        public RemoveSavedAnimation(InputFile animation) {
            this.animation = animation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveStickerFromSet extends Function {
        public static final int CONSTRUCTOR = 1642196644;
        public InputFile sticker;

        public RemoveStickerFromSet() {
        }

        public RemoveStickerFromSet(InputFile sticker) {
            this.sticker = sticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RemoveTopChat extends Function {
        public static final int CONSTRUCTOR = -1907876267;
        public TopChatCategory category;
        public long chatId;

        public RemoveTopChat() {
        }

        public RemoveTopChat(TopChatCategory category, long chatId) {
            this.category = category;
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReorderInstalledStickerSets extends Function {
        public static final int CONSTRUCTOR = 1114537563;
        public boolean isMasks;
        public long[] stickerSetIds;

        public ReorderInstalledStickerSets() {
        }

        public ReorderInstalledStickerSets(boolean isMasks, long[] stickerSetIds) {
            this.isMasks = isMasks;
            this.stickerSetIds = stickerSetIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReportChat extends Function {
        public static final int CONSTRUCTOR = -312579772;
        public long chatId;
        public long[] messageIds;
        public ChatReportReason reason;

        public ReportChat() {
        }

        public ReportChat(long chatId, ChatReportReason reason, long[] messageIds) {
            this.chatId = chatId;
            this.reason = reason;
            this.messageIds = messageIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ReportSupergroupSpam extends Function {
        public static final int CONSTRUCTOR = -2125451498;
        public long[] messageIds;
        public int supergroupId;
        public int userId;

        public ReportSupergroupSpam() {
        }

        public ReportSupergroupSpam(int supergroupId, int userId, long[] messageIds) {
            this.supergroupId = supergroupId;
            this.userId = userId;
            this.messageIds = messageIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RequestAuthenticationPasswordRecovery extends Function {
        public static final int CONSTRUCTOR = 1393896118;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RequestPasswordRecovery extends Function {
        public static final int CONSTRUCTOR = -13777582;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class RequestQrCodeAuthentication extends Function {
        public static final int CONSTRUCTOR = -104224560;
        public int[] otherUserIds;

        public RequestQrCodeAuthentication() {
        }

        public RequestQrCodeAuthentication(int[] otherUserIds) {
            this.otherUserIds = otherUserIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendAuthenticationCode extends Function {
        public static final int CONSTRUCTOR = -814377191;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendChangePhoneNumberCode extends Function {
        public static final int CONSTRUCTOR = -786772060;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendEmailAddressVerificationCode extends Function {
        public static final int CONSTRUCTOR = -1872416732;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendMessages extends Function {
        public static final int CONSTRUCTOR = -940655817;
        public long chatId;
        public long[] messageIds;

        public ResendMessages() {
        }

        public ResendMessages(long chatId, long[] messageIds) {
            this.chatId = chatId;
            this.messageIds = messageIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendPhoneNumberConfirmationCode extends Function {
        public static final int CONSTRUCTOR = 2069068522;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendPhoneNumberVerificationCode extends Function {
        public static final int CONSTRUCTOR = 1367629820;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResendRecoveryEmailAddressCode extends Function {
        public static final int CONSTRUCTOR = 433483548;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResetAllNotificationSettings extends Function {
        public static final int CONSTRUCTOR = -174020359;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResetBackgrounds extends Function {
        public static final int CONSTRUCTOR = 204852088;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ResetNetworkStatistics extends Function {
        public static final int CONSTRUCTOR = 1646452102;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SaveApplicationLogEvent extends Function {
        public static final int CONSTRUCTOR = -811154930;
        public long chatId;
        public JsonValue data;
        public String type;

        public SaveApplicationLogEvent() {
        }

        public SaveApplicationLogEvent(String type, long chatId, JsonValue data) {
            this.type = type;
            this.chatId = chatId;
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchBackground extends Function {
        public static final int CONSTRUCTOR = -2130996959;
        public String name;

        public SearchBackground() {
        }

        public SearchBackground(String name) {
            this.name = name;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchCallMessages extends Function {
        public static final int CONSTRUCTOR = -1077230820;
        public long fromMessageId;
        public int limit;
        public boolean onlyMissed;

        public SearchCallMessages() {
        }

        public SearchCallMessages(long fromMessageId, int limit, boolean onlyMissed) {
            this.fromMessageId = fromMessageId;
            this.limit = limit;
            this.onlyMissed = onlyMissed;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChatMembers extends Function {
        public static final int CONSTRUCTOR = -445823291;
        public long chatId;
        public ChatMembersFilter filter;
        public int limit;
        public String query;

        public SearchChatMembers() {
        }

        public SearchChatMembers(long chatId, String query, int limit, ChatMembersFilter filter) {
            this.chatId = chatId;
            this.query = query;
            this.limit = limit;
            this.filter = filter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChatMessages extends Function {
        public static final int CONSTRUCTOR = -1528846671;
        public long chatId;
        public SearchMessagesFilter filter;
        public long fromMessageId;
        public int limit;
        public int offset;
        public String query;
        public int senderUserId;

        public SearchChatMessages() {
        }

        public SearchChatMessages(long chatId, String query, int senderUserId, long fromMessageId, int offset, int limit, SearchMessagesFilter filter) {
            this.chatId = chatId;
            this.query = query;
            this.senderUserId = senderUserId;
            this.fromMessageId = fromMessageId;
            this.offset = offset;
            this.limit = limit;
            this.filter = filter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChatRecentLocationMessages extends Function {
        public static final int CONSTRUCTOR = 950238950;
        public long chatId;
        public int limit;

        public SearchChatRecentLocationMessages() {
        }

        public SearchChatRecentLocationMessages(long chatId, int limit) {
            this.chatId = chatId;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChats extends Function {
        public static final int CONSTRUCTOR = -1879787060;
        public int limit;
        public String query;

        public SearchChats() {
        }

        public SearchChats(String query, int limit) {
            this.query = query;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChatsNearby extends Function {
        public static final int CONSTRUCTOR = -196753377;
        public Location location;

        public SearchChatsNearby() {
        }

        public SearchChatsNearby(Location location) {
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchChatsOnServer extends Function {
        public static final int CONSTRUCTOR = -1158402188;
        public int limit;
        public String query;

        public SearchChatsOnServer() {
        }

        public SearchChatsOnServer(String query, int limit) {
            this.query = query;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchContacts extends Function {
        public static final int CONSTRUCTOR = -1794690715;
        public int limit;
        public String query;

        public SearchContacts() {
        }

        public SearchContacts(String query, int limit) {
            this.query = query;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchEmojis extends Function {
        public static final int CONSTRUCTOR = 453292808;
        public boolean exactMatch;
        public String inputLanguageCode;
        public String text;

        public SearchEmojis() {
        }

        public SearchEmojis(String text, boolean exactMatch, String inputLanguageCode) {
            this.text = text;
            this.exactMatch = exactMatch;
            this.inputLanguageCode = inputLanguageCode;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchHashtags extends Function {
        public static final int CONSTRUCTOR = 1043637617;
        public int limit;
        public String prefix;

        public SearchHashtags() {
        }

        public SearchHashtags(String prefix, int limit) {
            this.prefix = prefix;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchInstalledStickerSets extends Function {
        public static final int CONSTRUCTOR = 681171344;
        public boolean isMasks;
        public int limit;
        public String query;

        public SearchInstalledStickerSets() {
        }

        public SearchInstalledStickerSets(boolean isMasks, String query, int limit) {
            this.isMasks = isMasks;
            this.query = query;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchMessages extends Function {
        public static final int CONSTRUCTOR = -455843835;
        public ChatList chatList;
        public int limit;
        public long offsetChatId;
        public int offsetDate;
        public long offsetMessageId;
        public String query;

        public SearchMessages() {
        }

        public SearchMessages(ChatList chatList, String query, int offsetDate, long offsetChatId, long offsetMessageId, int limit) {
            this.chatList = chatList;
            this.query = query;
            this.offsetDate = offsetDate;
            this.offsetChatId = offsetChatId;
            this.offsetMessageId = offsetMessageId;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchPublicChat extends Function {
        public static final int CONSTRUCTOR = 857135533;
        public String username;

        public SearchPublicChat() {
        }

        public SearchPublicChat(String username) {
            this.username = username;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchPublicChats extends Function {
        public static final int CONSTRUCTOR = 970385337;
        public String query;

        public SearchPublicChats() {
        }

        public SearchPublicChats(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchSecretMessages extends Function {
        public static final int CONSTRUCTOR = -1670627915;
        public long chatId;
        public SearchMessagesFilter filter;
        public long fromSearchId;
        public int limit;
        public String query;

        public SearchSecretMessages() {
        }

        public SearchSecretMessages(long chatId, String query, long fromSearchId, int limit, SearchMessagesFilter filter) {
            this.chatId = chatId;
            this.query = query;
            this.fromSearchId = fromSearchId;
            this.limit = limit;
            this.filter = filter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchStickerSet extends Function {
        public static final int CONSTRUCTOR = 1157930222;
        public String name;

        public SearchStickerSet() {
        }

        public SearchStickerSet(String name) {
            this.name = name;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchStickerSets extends Function {
        public static final int CONSTRUCTOR = -1082314629;
        public String query;

        public SearchStickerSets() {
        }

        public SearchStickerSets(String query) {
            this.query = query;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SearchStickers extends Function {
        public static final int CONSTRUCTOR = 1555771203;
        public String emoji;
        public int limit;

        public SearchStickers() {
        }

        public SearchStickers(String emoji, int limit) {
            this.emoji = emoji;
            this.limit = limit;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendBotStartMessage extends Function {
        public static final int CONSTRUCTOR = 1112181339;
        public int botUserId;
        public long chatId;
        public String parameter;

        public SendBotStartMessage() {
        }

        public SendBotStartMessage(int botUserId, long chatId, String parameter) {
            this.botUserId = botUserId;
            this.chatId = chatId;
            this.parameter = parameter;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendCallDebugInformation extends Function {
        public static final int CONSTRUCTOR = 2019243839;
        public int callId;
        public String debugInformation;

        public SendCallDebugInformation() {
        }

        public SendCallDebugInformation(int callId, String debugInformation) {
            this.callId = callId;
            this.debugInformation = debugInformation;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendCallRating extends Function {
        public static final int CONSTRUCTOR = -1402719502;
        public int callId;
        public String comment;
        public CallProblem[] problems;
        public int rating;

        public SendCallRating() {
        }

        public SendCallRating(int callId, int rating, String comment, CallProblem[] problems) {
            this.callId = callId;
            this.rating = rating;
            this.comment = comment;
            this.problems = problems;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendChatAction extends Function {
        public static final int CONSTRUCTOR = -841357536;
        public ChatAction action;
        public long chatId;

        public SendChatAction() {
        }

        public SendChatAction(long chatId, ChatAction action) {
            this.chatId = chatId;
            this.action = action;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendChatScreenshotTakenNotification extends Function {
        public static final int CONSTRUCTOR = 448399457;
        public long chatId;

        public SendChatScreenshotTakenNotification() {
        }

        public SendChatScreenshotTakenNotification(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendChatSetTtlMessage extends Function {
        public static final int CONSTRUCTOR = 1432535564;
        public long chatId;
        public int ttl;

        public SendChatSetTtlMessage() {
        }

        public SendChatSetTtlMessage(long chatId, int ttl) {
            this.chatId = chatId;
            this.ttl = ttl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendCustomRequest extends Function {
        public static final int CONSTRUCTOR = 285045153;
        public String method;
        public String parameters;

        public SendCustomRequest() {
        }

        public SendCustomRequest(String method, String parameters) {
            this.method = method;
            this.parameters = parameters;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendEmailAddressVerificationCode extends Function {
        public static final int CONSTRUCTOR = -221621379;
        public String emailAddress;

        public SendEmailAddressVerificationCode() {
        }

        public SendEmailAddressVerificationCode(String emailAddress) {
            this.emailAddress = emailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendInlineQueryResultMessage extends Function {
        public static final int CONSTRUCTOR = 729880339;
        public long chatId;
        public boolean hideViaBot;
        public SendMessageOptions options;
        public long queryId;
        public long replyToMessageId;
        public String resultId;

        public SendInlineQueryResultMessage() {
        }

        public SendInlineQueryResultMessage(long chatId, long replyToMessageId, SendMessageOptions options, long queryId, String resultId, boolean hideViaBot) {
            this.chatId = chatId;
            this.replyToMessageId = replyToMessageId;
            this.options = options;
            this.queryId = queryId;
            this.resultId = resultId;
            this.hideViaBot = hideViaBot;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendMessage extends Function {
        public static final int CONSTRUCTOR = -1314396596;
        public long chatId;
        public InputMessageContent inputMessageContent;
        public SendMessageOptions options;
        public ReplyMarkup replyMarkup;
        public long replyToMessageId;

        public SendMessage() {
        }

        public SendMessage(long chatId, long replyToMessageId, SendMessageOptions options, ReplyMarkup replyMarkup, InputMessageContent inputMessageContent) {
            this.chatId = chatId;
            this.replyToMessageId = replyToMessageId;
            this.options = options;
            this.replyMarkup = replyMarkup;
            this.inputMessageContent = inputMessageContent;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendMessageAlbum extends Function {
        public static final int CONSTRUCTOR = -818794592;
        public long chatId;
        public InputMessageContent[] inputMessageContents;
        public SendMessageOptions options;
        public long replyToMessageId;

        public SendMessageAlbum() {
        }

        public SendMessageAlbum(long chatId, long replyToMessageId, SendMessageOptions options, InputMessageContent[] inputMessageContents) {
            this.chatId = chatId;
            this.replyToMessageId = replyToMessageId;
            this.options = options;
            this.inputMessageContents = inputMessageContents;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendPassportAuthorizationForm extends Function {
        public static final int CONSTRUCTOR = -602402218;
        public int autorizationFormId;
        public PassportElementType[] types;

        public SendPassportAuthorizationForm() {
        }

        public SendPassportAuthorizationForm(int autorizationFormId, PassportElementType[] types) {
            this.autorizationFormId = autorizationFormId;
            this.types = types;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendPaymentForm extends Function {
        public static final int CONSTRUCTOR = 591581572;
        public long chatId;
        public InputCredentials credentials;
        public long messageId;
        public String orderInfoId;
        public String shippingOptionId;

        public SendPaymentForm() {
        }

        public SendPaymentForm(long chatId, long messageId, String orderInfoId, String shippingOptionId, InputCredentials credentials) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.orderInfoId = orderInfoId;
            this.shippingOptionId = shippingOptionId;
            this.credentials = credentials;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendPhoneNumberConfirmationCode extends Function {
        public static final int CONSTRUCTOR = -1901171495;
        public String hash;
        public String phoneNumber;
        public PhoneNumberAuthenticationSettings settings;

        public SendPhoneNumberConfirmationCode() {
        }

        public SendPhoneNumberConfirmationCode(String hash, String phoneNumber, PhoneNumberAuthenticationSettings settings) {
            this.hash = hash;
            this.phoneNumber = phoneNumber;
            this.settings = settings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SendPhoneNumberVerificationCode extends Function {
        public static final int CONSTRUCTOR = 2081689035;
        public String phoneNumber;
        public PhoneNumberAuthenticationSettings settings;

        public SendPhoneNumberVerificationCode() {
        }

        public SendPhoneNumberVerificationCode(String phoneNumber, PhoneNumberAuthenticationSettings settings) {
            this.phoneNumber = phoneNumber;
            this.settings = settings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetAccountTtl extends Function {
        public static final int CONSTRUCTOR = 701389032;
        public AccountTtl ttl;

        public SetAccountTtl() {
        }

        public SetAccountTtl(AccountTtl ttl) {
            this.ttl = ttl;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetAlarm extends Function {
        public static final int CONSTRUCTOR = -873497067;
        public double seconds;

        public SetAlarm() {
        }

        public SetAlarm(double seconds) {
            this.seconds = seconds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetAuthenticationPhoneNumber extends Function {
        public static final int CONSTRUCTOR = 868276259;
        public String phoneNumber;
        public PhoneNumberAuthenticationSettings settings;

        public SetAuthenticationPhoneNumber() {
        }

        public SetAuthenticationPhoneNumber(String phoneNumber, PhoneNumberAuthenticationSettings settings) {
            this.phoneNumber = phoneNumber;
            this.settings = settings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetAutoDownloadSettings extends Function {
        public static final int CONSTRUCTOR = -353671948;
        public AutoDownloadSettings settings;
        public NetworkType type;

        public SetAutoDownloadSettings() {
        }

        public SetAutoDownloadSettings(AutoDownloadSettings settings, NetworkType type) {
            this.settings = settings;
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetBackground extends Function {
        public static final int CONSTRUCTOR = -1035439225;
        public InputBackground background;
        public boolean forDarkTheme;
        public BackgroundType type;

        public SetBackground() {
        }

        public SetBackground(InputBackground background, BackgroundType type, boolean forDarkTheme) {
            this.background = background;
            this.type = type;
            this.forDarkTheme = forDarkTheme;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetBio extends Function {
        public static final int CONSTRUCTOR = -1619582124;
        public String bio;

        public SetBio() {
        }

        public SetBio(String bio) {
            this.bio = bio;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetBotUpdatesStatus extends Function {
        public static final int CONSTRUCTOR = -1154926191;
        public String errorMessage;
        public int pendingUpdateCount;

        public SetBotUpdatesStatus() {
        }

        public SetBotUpdatesStatus(int pendingUpdateCount, String errorMessage) {
            this.pendingUpdateCount = pendingUpdateCount;
            this.errorMessage = errorMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatChatList extends Function {
        public static final int CONSTRUCTOR = -1396517321;
        public long chatId;
        public ChatList chatList;

        public SetChatChatList() {
        }

        public SetChatChatList(long chatId, ChatList chatList) {
            this.chatId = chatId;
            this.chatList = chatList;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatClientData extends Function {
        public static final int CONSTRUCTOR = -827119811;
        public long chatId;
        public String clientData;

        public SetChatClientData() {
        }

        public SetChatClientData(long chatId, String clientData) {
            this.chatId = chatId;
            this.clientData = clientData;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatDescription extends Function {
        public static final int CONSTRUCTOR = 1957213277;
        public long chatId;
        public String description;

        public SetChatDescription() {
        }

        public SetChatDescription(long chatId, String description) {
            this.chatId = chatId;
            this.description = description;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatDiscussionGroup extends Function {
        public static final int CONSTRUCTOR = -918801736;
        public long chatId;
        public long discussionChatId;

        public SetChatDiscussionGroup() {
        }

        public SetChatDiscussionGroup(long chatId, long discussionChatId) {
            this.chatId = chatId;
            this.discussionChatId = discussionChatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatDraftMessage extends Function {
        public static final int CONSTRUCTOR = -588175579;
        public long chatId;
        public DraftMessage draftMessage;

        public SetChatDraftMessage() {
        }

        public SetChatDraftMessage(long chatId, DraftMessage draftMessage) {
            this.chatId = chatId;
            this.draftMessage = draftMessage;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatLocation extends Function {
        public static final int CONSTRUCTOR = -767091286;
        public long chatId;
        public ChatLocation location;

        public SetChatLocation() {
        }

        public SetChatLocation(long chatId, ChatLocation location) {
            this.chatId = chatId;
            this.location = location;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatMemberStatus extends Function {
        public static final int CONSTRUCTOR = -1754439241;
        public long chatId;
        public ChatMemberStatus status;
        public int userId;

        public SetChatMemberStatus() {
        }

        public SetChatMemberStatus(long chatId, int userId, ChatMemberStatus status) {
            this.chatId = chatId;
            this.userId = userId;
            this.status = status;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatNotificationSettings extends Function {
        public static final int CONSTRUCTOR = 777199614;
        public long chatId;
        public ChatNotificationSettings notificationSettings;

        public SetChatNotificationSettings() {
        }

        public SetChatNotificationSettings(long chatId, ChatNotificationSettings notificationSettings) {
            this.chatId = chatId;
            this.notificationSettings = notificationSettings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatPermissions extends Function {
        public static final int CONSTRUCTOR = 2138507006;
        public long chatId;
        public ChatPermissions permissions;

        public SetChatPermissions() {
        }

        public SetChatPermissions(long chatId, ChatPermissions permissions) {
            this.chatId = chatId;
            this.permissions = permissions;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatPhoto extends Function {
        public static final int CONSTRUCTOR = 132244217;
        public long chatId;
        public InputFile photo;

        public SetChatPhoto() {
        }

        public SetChatPhoto(long chatId, InputFile photo) {
            this.chatId = chatId;
            this.photo = photo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatSlowModeDelay extends Function {
        public static final int CONSTRUCTOR = -540350914;
        public long chatId;
        public int slowModeDelay;

        public SetChatSlowModeDelay() {
        }

        public SetChatSlowModeDelay(long chatId, int slowModeDelay) {
            this.chatId = chatId;
            this.slowModeDelay = slowModeDelay;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetChatTitle extends Function {
        public static final int CONSTRUCTOR = 164282047;
        public long chatId;
        public String title;

        public SetChatTitle() {
        }

        public SetChatTitle(long chatId, String title) {
            this.chatId = chatId;
            this.title = title;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetCustomLanguagePack extends Function {
        public static final int CONSTRUCTOR = -296742819;
        public LanguagePackInfo info;
        public LanguagePackString[] strings;

        public SetCustomLanguagePack() {
        }

        public SetCustomLanguagePack(LanguagePackInfo info, LanguagePackString[] strings) {
            this.info = info;
            this.strings = strings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetCustomLanguagePackString extends Function {
        public static final int CONSTRUCTOR = 1316365592;
        public String languagePackId;
        public LanguagePackString newString;

        public SetCustomLanguagePackString() {
        }

        public SetCustomLanguagePackString(String languagePackId, LanguagePackString newString) {
            this.languagePackId = languagePackId;
            this.newString = newString;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetDatabaseEncryptionKey extends Function {
        public static final int CONSTRUCTOR = -1204599371;
        public byte[] newEncryptionKey;

        public SetDatabaseEncryptionKey() {
        }

        public SetDatabaseEncryptionKey(byte[] newEncryptionKey) {
            this.newEncryptionKey = newEncryptionKey;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetFileGenerationProgress extends Function {
        public static final int CONSTRUCTOR = -540459953;
        public int expectedSize;
        public long generationId;
        public int localPrefixSize;

        public SetFileGenerationProgress() {
        }

        public SetFileGenerationProgress(long generationId, int expectedSize, int localPrefixSize) {
            this.generationId = generationId;
            this.expectedSize = expectedSize;
            this.localPrefixSize = localPrefixSize;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetGameScore extends Function {
        public static final int CONSTRUCTOR = -1768307069;
        public long chatId;
        public boolean editMessage;
        public boolean force;
        public long messageId;
        public int score;
        public int userId;

        public SetGameScore() {
        }

        public SetGameScore(long chatId, long messageId, boolean editMessage, int userId, int score, boolean force) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.editMessage = editMessage;
            this.userId = userId;
            this.score = score;
            this.force = force;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetInlineGameScore extends Function {
        public static final int CONSTRUCTOR = 758435487;
        public boolean editMessage;
        public boolean force;
        public String inlineMessageId;
        public int score;
        public int userId;

        public SetInlineGameScore() {
        }

        public SetInlineGameScore(String inlineMessageId, boolean editMessage, int userId, int score, boolean force) {
            this.inlineMessageId = inlineMessageId;
            this.editMessage = editMessage;
            this.userId = userId;
            this.score = score;
            this.force = force;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetLogStream extends Function {
        public static final int CONSTRUCTOR = -1364199535;
        public LogStream logStream;

        public SetLogStream() {
        }

        public SetLogStream(LogStream logStream) {
            this.logStream = logStream;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetLogTagVerbosityLevel extends Function {
        public static final int CONSTRUCTOR = -2095589738;
        public int newVerbosityLevel;
        public String tag;

        public SetLogTagVerbosityLevel() {
        }

        public SetLogTagVerbosityLevel(String tag, int newVerbosityLevel) {
            this.tag = tag;
            this.newVerbosityLevel = newVerbosityLevel;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetLogVerbosityLevel extends Function {
        public static final int CONSTRUCTOR = -303429678;
        public int newVerbosityLevel;

        public SetLogVerbosityLevel() {
        }

        public SetLogVerbosityLevel(int newVerbosityLevel) {
            this.newVerbosityLevel = newVerbosityLevel;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetName extends Function {
        public static final int CONSTRUCTOR = 1711693584;
        public String firstName;
        public String lastName;

        public SetName() {
        }

        public SetName(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetNetworkType extends Function {
        public static final int CONSTRUCTOR = -701635234;
        public NetworkType type;

        public SetNetworkType() {
        }

        public SetNetworkType(NetworkType type) {
            this.type = type;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetOption extends Function {
        public static final int CONSTRUCTOR = 2114670322;
        public String name;
        public OptionValue value;

        public SetOption() {
        }

        public SetOption(String name, OptionValue value) {
            this.name = name;
            this.value = value;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetPassportElement extends Function {
        public static final int CONSTRUCTOR = 2068173212;
        public InputPassportElement element;
        public String password;

        public SetPassportElement() {
        }

        public SetPassportElement(InputPassportElement element, String password) {
            this.element = element;
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetPassportElementErrors extends Function {
        public static final int CONSTRUCTOR = 1455869875;
        public InputPassportElementError[] errors;
        public int userId;

        public SetPassportElementErrors() {
        }

        public SetPassportElementErrors(int userId, InputPassportElementError[] errors) {
            this.userId = userId;
            this.errors = errors;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetPassword extends Function {
        public static final int CONSTRUCTOR = -1193589027;
        public String newHint;
        public String newPassword;
        public String newRecoveryEmailAddress;
        public String oldPassword;
        public boolean setRecoveryEmailAddress;

        public SetPassword() {
        }

        public SetPassword(String oldPassword, String newPassword, String newHint, boolean setRecoveryEmailAddress, String newRecoveryEmailAddress) {
            this.oldPassword = oldPassword;
            this.newPassword = newPassword;
            this.newHint = newHint;
            this.setRecoveryEmailAddress = setRecoveryEmailAddress;
            this.newRecoveryEmailAddress = newRecoveryEmailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetPinnedChats extends Function {
        public static final int CONSTRUCTOR = -695640000;
        public long[] chatIds;
        public ChatList chatList;

        public SetPinnedChats() {
        }

        public SetPinnedChats(ChatList chatList, long[] chatIds) {
            this.chatList = chatList;
            this.chatIds = chatIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetPollAnswer extends Function {
        public static final int CONSTRUCTOR = -1399388792;
        public long chatId;
        public long messageId;
        public int[] optionIds;

        public SetPollAnswer() {
        }

        public SetPollAnswer(long chatId, long messageId, int[] optionIds) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.optionIds = optionIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetProfilePhoto extends Function {
        public static final int CONSTRUCTOR = 1594734550;
        public InputFile photo;

        public SetProfilePhoto() {
        }

        public SetProfilePhoto(InputFile photo) {
            this.photo = photo;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetRecoveryEmailAddress extends Function {
        public static final int CONSTRUCTOR = -1981836385;
        public String newRecoveryEmailAddress;
        public String password;

        public SetRecoveryEmailAddress() {
        }

        public SetRecoveryEmailAddress(String password, String newRecoveryEmailAddress) {
            this.password = password;
            this.newRecoveryEmailAddress = newRecoveryEmailAddress;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetScopeNotificationSettings extends Function {
        public static final int CONSTRUCTOR = -2049984966;
        public ScopeNotificationSettings notificationSettings;
        public NotificationSettingsScope scope;

        public SetScopeNotificationSettings() {
        }

        public SetScopeNotificationSettings(NotificationSettingsScope scope, ScopeNotificationSettings notificationSettings) {
            this.scope = scope;
            this.notificationSettings = notificationSettings;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetStickerPositionInSet extends Function {
        public static final int CONSTRUCTOR = 2075281185;
        public int position;
        public InputFile sticker;

        public SetStickerPositionInSet() {
        }

        public SetStickerPositionInSet(InputFile sticker, int position) {
            this.sticker = sticker;
            this.position = position;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetSupergroupStickerSet extends Function {
        public static final int CONSTRUCTOR = -295782298;
        public long stickerSetId;
        public int supergroupId;

        public SetSupergroupStickerSet() {
        }

        public SetSupergroupStickerSet(int supergroupId, long stickerSetId) {
            this.supergroupId = supergroupId;
            this.stickerSetId = stickerSetId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetSupergroupUsername extends Function {
        public static final int CONSTRUCTOR = -1428333122;
        public int supergroupId;
        public String username;

        public SetSupergroupUsername() {
        }

        public SetSupergroupUsername(int supergroupId, String username) {
            this.supergroupId = supergroupId;
            this.username = username;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetTdlibParameters extends Function {
        public static final int CONSTRUCTOR = -1912557997;
        public TdlibParameters parameters;

        public SetTdlibParameters() {
        }

        public SetTdlibParameters(TdlibParameters parameters) {
            this.parameters = parameters;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetUserPrivacySettingRules extends Function {
        public static final int CONSTRUCTOR = -473812741;
        public UserPrivacySettingRules rules;
        public UserPrivacySetting setting;

        public SetUserPrivacySettingRules() {
        }

        public SetUserPrivacySettingRules(UserPrivacySetting setting, UserPrivacySettingRules rules) {
            this.setting = setting;
            this.rules = rules;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SetUsername extends Function {
        public static final int CONSTRUCTOR = 439901214;
        public String username;

        public SetUsername() {
        }

        public SetUsername(String username) {
            this.username = username;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SharePhoneNumber extends Function {
        public static final int CONSTRUCTOR = -370669878;
        public int userId;

        public SharePhoneNumber() {
        }

        public SharePhoneNumber(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class StopPoll extends Function {
        public static final int CONSTRUCTOR = 1659374253;
        public long chatId;
        public long messageId;
        public ReplyMarkup replyMarkup;

        public StopPoll() {
        }

        public StopPoll(long chatId, long messageId, ReplyMarkup replyMarkup) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.replyMarkup = replyMarkup;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class SynchronizeLanguagePack extends Function {
        public static final int CONSTRUCTOR = -2065307858;
        public String languagePackId;

        public SynchronizeLanguagePack() {
        }

        public SynchronizeLanguagePack(String languagePackId) {
            this.languagePackId = languagePackId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TerminateAllOtherSessions extends Function {
        public static final int CONSTRUCTOR = 1874485523;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TerminateSession extends Function {
        public static final int CONSTRUCTOR = -407385812;
        public long sessionId;

        public TerminateSession() {
        }

        public TerminateSession(long sessionId) {
            this.sessionId = sessionId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallBytes extends Function {
        public static final int CONSTRUCTOR = -736011607;
        public byte[] x;

        public TestCallBytes() {
        }

        public TestCallBytes(byte[] x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallEmpty extends Function {
        public static final int CONSTRUCTOR = -627291626;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallString extends Function {
        public static final int CONSTRUCTOR = -1732818385;
        public String x;

        public TestCallString() {
        }

        public TestCallString(String x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallVectorInt extends Function {
        public static final int CONSTRUCTOR = -2137277793;
        public int[] x;

        public TestCallVectorInt() {
        }

        public TestCallVectorInt(int[] x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallVectorIntObject extends Function {
        public static final int CONSTRUCTOR = 1825428218;
        public TestInt[] x;

        public TestCallVectorIntObject() {
        }

        public TestCallVectorIntObject(TestInt[] x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallVectorString extends Function {
        public static final int CONSTRUCTOR = -408600900;
        public String[] x;

        public TestCallVectorString() {
        }

        public TestCallVectorString(String[] x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestCallVectorStringObject extends Function {
        public static final int CONSTRUCTOR = 1527666429;
        public TestString[] x;

        public TestCallVectorStringObject() {
        }

        public TestCallVectorStringObject(TestString[] x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestGetDifference extends Function {
        public static final int CONSTRUCTOR = 1747084069;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestNetwork extends Function {
        public static final int CONSTRUCTOR = -1343998901;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestProxy extends Function {
        public static final int CONSTRUCTOR = -1197366626;
        public int dcId;
        public int port;
        public String server;
        public double timeout;
        public ProxyType type;

        public TestProxy() {
        }

        public TestProxy(String server, int port, ProxyType type, int dcId, double timeout) {
            this.server = server;
            this.port = port;
            this.type = type;
            this.dcId = dcId;
            this.timeout = timeout;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestReturnError extends Function {
        public static final int CONSTRUCTOR = 455179506;
        public Error error;

        public TestReturnError() {
        }

        public TestReturnError(Error error) {
            this.error = error;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestSquareInt extends Function {
        public static final int CONSTRUCTOR = -60135024;
        public int x;

        public TestSquareInt() {
        }

        public TestSquareInt(int x) {
            this.x = x;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TestUseUpdate extends Function {
        public static final int CONSTRUCTOR = 717094686;

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ToggleChatDefaultDisableNotification extends Function {
        public static final int CONSTRUCTOR = 314794002;
        public long chatId;
        public boolean defaultDisableNotification;

        public ToggleChatDefaultDisableNotification() {
        }

        public ToggleChatDefaultDisableNotification(long chatId, boolean defaultDisableNotification) {
            this.chatId = chatId;
            this.defaultDisableNotification = defaultDisableNotification;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ToggleChatIsMarkedAsUnread extends Function {
        public static final int CONSTRUCTOR = -986129697;
        public long chatId;
        public boolean isMarkedAsUnread;

        public ToggleChatIsMarkedAsUnread() {
        }

        public ToggleChatIsMarkedAsUnread(long chatId, boolean isMarkedAsUnread) {
            this.chatId = chatId;
            this.isMarkedAsUnread = isMarkedAsUnread;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ToggleChatIsPinned extends Function {
        public static final int CONSTRUCTOR = -1166802621;
        public long chatId;
        public boolean isPinned;

        public ToggleChatIsPinned() {
        }

        public ToggleChatIsPinned(long chatId, boolean isPinned) {
            this.chatId = chatId;
            this.isPinned = isPinned;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ToggleSupergroupIsAllHistoryAvailable extends Function {
        public static final int CONSTRUCTOR = 1701526555;
        public boolean isAllHistoryAvailable;
        public int supergroupId;

        public ToggleSupergroupIsAllHistoryAvailable() {
        }

        public ToggleSupergroupIsAllHistoryAvailable(int supergroupId, boolean isAllHistoryAvailable) {
            this.supergroupId = supergroupId;
            this.isAllHistoryAvailable = isAllHistoryAvailable;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ToggleSupergroupSignMessages extends Function {
        public static final int CONSTRUCTOR = -558196581;
        public boolean signMessages;
        public int supergroupId;

        public ToggleSupergroupSignMessages() {
        }

        public ToggleSupergroupSignMessages(int supergroupId, boolean signMessages) {
            this.supergroupId = supergroupId;
            this.signMessages = signMessages;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class TransferChatOwnership extends Function {
        public static final int CONSTRUCTOR = -1925047127;
        public long chatId;
        public String password;
        public int userId;

        public TransferChatOwnership() {
        }

        public TransferChatOwnership(long chatId, int userId, String password) {
            this.chatId = chatId;
            this.userId = userId;
            this.password = password;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UnblockUser extends Function {
        public static final int CONSTRUCTOR = -307286367;
        public int userId;

        public UnblockUser() {
        }

        public UnblockUser(int userId) {
            this.userId = userId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UnpinChatMessage extends Function {
        public static final int CONSTRUCTOR = 277557690;
        public long chatId;

        public UnpinChatMessage() {
        }

        public UnpinChatMessage(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UpgradeBasicGroupChatToSupergroupChat extends Function {
        public static final int CONSTRUCTOR = 300488122;
        public long chatId;

        public UpgradeBasicGroupChatToSupergroupChat() {
        }

        public UpgradeBasicGroupChatToSupergroupChat(long chatId) {
            this.chatId = chatId;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UploadFile extends Function {
        public static final int CONSTRUCTOR = -745597786;
        public InputFile file;
        public FileType fileType;
        public int priority;

        public UploadFile() {
        }

        public UploadFile(InputFile file, FileType fileType, int priority) {
            this.file = file;
            this.fileType = fileType;
            this.priority = priority;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class UploadStickerFile extends Function {
        public static final int CONSTRUCTOR = 1134087551;
        public InputFile pngSticker;
        public int userId;

        public UploadStickerFile() {
        }

        public UploadStickerFile(int userId, InputFile pngSticker) {
            this.userId = userId;
            this.pngSticker = pngSticker;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ValidateOrderInfo extends Function {
        public static final int CONSTRUCTOR = 9480644;
        public boolean allowSave;
        public long chatId;
        public long messageId;
        public OrderInfo orderInfo;

        public ValidateOrderInfo() {
        }

        public ValidateOrderInfo(long chatId, long messageId, OrderInfo orderInfo, boolean allowSave) {
            this.chatId = chatId;
            this.messageId = messageId;
            this.orderInfo = orderInfo;
            this.allowSave = allowSave;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ViewMessages extends Function {
        public static final int CONSTRUCTOR = -1925784915;
        public long chatId;
        public boolean forceRead;
        public long[] messageIds;

        public ViewMessages() {
        }

        public ViewMessages(long chatId, long[] messageIds, boolean forceRead) {
            this.chatId = chatId;
            this.messageIds = messageIds;
            this.forceRead = forceRead;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class ViewTrendingStickerSets extends Function {
        public static final int CONSTRUCTOR = -952416520;
        public long[] stickerSetIds;

        public ViewTrendingStickerSets() {
        }

        public ViewTrendingStickerSets(long[] stickerSetIds) {
            this.stickerSetIds = stickerSetIds;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }

    /* loaded from: classes2.dex */
    public static class WriteGeneratedFilePart extends Function {
        public static final int CONSTRUCTOR = -2062358189;
        public byte[] data;
        public long generationId;
        public int offset;

        public WriteGeneratedFilePart() {
        }

        public WriteGeneratedFilePart(long generationId, int offset, byte[] data) {
            this.generationId = generationId;
            this.offset = offset;
            this.data = data;
        }

        @Override // org.drinkless.td.libcore.telegram.TdApi.Function, org.drinkless.td.libcore.telegram.TdApi.Object
        public int getConstructor() {
            return CONSTRUCTOR;
        }
    }
}
