package org.drinkless.td.libcore.telegram;

import android.util.Log;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.drinkless.td.libcore.telegram.TdApi;

/* loaded from: classes2.dex */
public final class Client implements Runnable {
    private static final int MAX_EVENTS = 1000;
    private static AtomicLong clientCount = new AtomicLong();
    private volatile ExceptionHandler defaultExceptionHandler;
    private final Lock readLock;
    private final ReadWriteLock readWriteLock;
    private final Lock writeLock;
    private volatile boolean stopFlag = false;
    private volatile boolean isClientDestroyed = false;
    private final ConcurrentHashMap<Long, Handler> handlers = new ConcurrentHashMap<>();
    private final AtomicLong currentQueryId = new AtomicLong();
    private final long[] eventIds = new long[1000];
    private final TdApi.Object[] events = new TdApi.Object[1000];
    private final long nativeClientId = NativeClient.createClient();

    /* loaded from: classes2.dex */
    public interface ExceptionHandler {
        void onException(Throwable th);
    }

    /* loaded from: classes2.dex */
    public interface ResultHandler {
        void onResult(TdApi.Object object);
    }

    public void send(TdApi.Function query, ResultHandler resultHandler, ExceptionHandler exceptionHandler) {
        if (query != null) {
            this.readLock.lock();
            try {
                if (this.isClientDestroyed) {
                    if (resultHandler != null) {
                        handleResult(new TdApi.Error(500, "Client is closed"), resultHandler, exceptionHandler);
                    }
                    return;
                }
                long queryId = this.currentQueryId.incrementAndGet();
                this.handlers.put(Long.valueOf(queryId), new Handler(resultHandler, exceptionHandler));
                NativeClient.clientSend(this.nativeClientId, queryId, query);
            } finally {
                this.readLock.unlock();
            }
        } else {
            throw new NullPointerException("query is null");
        }
    }

    public void send(TdApi.Function query, ResultHandler resultHandler) {
        send(query, resultHandler, null);
    }

    public static TdApi.Object execute(TdApi.Function query) {
        if (query != null) {
            return NativeClient.clientExecute(query);
        }
        throw new NullPointerException("query is null");
    }

    public void setUpdatesHandler(ResultHandler updatesHandler, ExceptionHandler exceptionHandler) {
        this.handlers.put(0L, new Handler(updatesHandler, exceptionHandler));
    }

    public void setUpdatesHandler(ResultHandler updatesHandler) {
        setUpdatesHandler(updatesHandler, null);
    }

    public void setDefaultExceptionHandler(ExceptionHandler defaultExceptionHandler) {
        this.defaultExceptionHandler = defaultExceptionHandler;
    }

    public void bench(TdApi.Function query, ResultHandler handler, int count) {
        if (query != null) {
            for (int i = 0; i < count; i++) {
                send(query, handler);
            }
            return;
        }
        throw new NullPointerException("query is null");
    }

    @Override // java.lang.Runnable
    public void run() {
        while (!this.stopFlag) {
            receiveQueries(300.0d);
        }
        Log.d("DLTD", "Stop TDLib thread");
    }

    public static Client create(ResultHandler updatesHandler, ExceptionHandler updatesExceptionHandler, ExceptionHandler defaultExceptionHandler) {
        Client client = new Client(updatesHandler, updatesExceptionHandler, defaultExceptionHandler);
        new Thread(client, "TDLib thread").start();
        return client;
    }

    @Deprecated
    public static void setLogVerbosityLevel(int newLogVerbosity) {
        if (newLogVerbosity >= 0) {
            NativeClient.setLogVerbosityLevel(newLogVerbosity);
            return;
        }
        throw new IllegalArgumentException("newLogVerbosity can't be negative");
    }

    @Deprecated
    public static boolean setLogFilePath(String filePath) {
        return NativeClient.setLogFilePath(filePath);
    }

    @Deprecated
    public static void setLogMaxFileSize(long maxFileSize) {
        if (maxFileSize > 0) {
            NativeClient.setLogMaxFileSize(maxFileSize);
            return;
        }
        throw new IllegalArgumentException("maxFileSize should be positive");
    }

    public void close() {
        this.writeLock.lock();
        try {
            if (!this.isClientDestroyed) {
                if (!this.stopFlag) {
                    send(new TdApi.Close(), null);
                }
                this.isClientDestroyed = true;
                while (!this.stopFlag) {
                    Thread.yield();
                }
                if (this.handlers.size() != 1) {
                    receiveQueries(0.0d);
                    for (Long key : this.handlers.keySet()) {
                        if (key.longValue() != 0) {
                            processResult(key.longValue(), new TdApi.Error(500, "Client is closed"));
                        }
                    }
                }
                NativeClient.destroyClient(this.nativeClientId);
                clientCount.decrementAndGet();
            }
        } finally {
            this.writeLock.unlock();
        }
    }

    static void onFatalError(String errorMessage) {
        new Thread(new Runnable(errorMessage) { // from class: org.drinkless.td.libcore.telegram.Client.1ThrowError
            private final String errorMessage;

            {
                this.errorMessage = errorMessage;
            }

            @Override // java.lang.Runnable
            public void run() {
                if (Client.isExternalError(this.errorMessage)) {
                    processExternalError();
                    return;
                }
                throw new ClientException("TDLib fatal error (" + Client.clientCount.get() + "): " + this.errorMessage);
            }

            private void processExternalError() {
                throw new ClientException("Fatal error (" + Client.clientCount.get() + "): " + this.errorMessage);
            }
        }, "TDLib fatal error thread").start();
        while (true) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    private static boolean isDatabaseBrokenError(String message) {
        return message.contains("Wrong key or database is corrupted") || message.contains("SQL logic error or missing database") || message.contains("database disk image is malformed") || message.contains("file is encrypted or is not a database") || message.contains("unsupported file format");
    }

    private static boolean isDiskFullError(String message) {
        return message.contains("PosixError : No space left on device") || message.contains("database or disk is full");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public static boolean isExternalError(String message) {
        return isDatabaseBrokenError(message) || isDiskFullError(message) || message.contains("I/O error");
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: classes2.dex */
    public static final class ClientException extends RuntimeException {
        private ClientException(String message) {
            super(message);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* loaded from: classes2.dex */
    public static class Handler {
        final ExceptionHandler exceptionHandler;
        final ResultHandler resultHandler;

        Handler(ResultHandler resultHandler, ExceptionHandler exceptionHandler) {
            this.resultHandler = resultHandler;
            this.exceptionHandler = exceptionHandler;
        }
    }

    private Client(ResultHandler updatesHandler, ExceptionHandler updateExceptionHandler, ExceptionHandler defaultExceptionHandler) {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
        this.readWriteLock = reentrantReadWriteLock;
        this.readLock = reentrantReadWriteLock.readLock();
        this.writeLock = this.readWriteLock.writeLock();
        this.defaultExceptionHandler = null;
        clientCount.incrementAndGet();
        this.handlers.put(0L, new Handler(updatesHandler, updateExceptionHandler));
        this.defaultExceptionHandler = defaultExceptionHandler;
    }

    protected void finalize() throws Throwable {
        try {
            close();
        } finally {
            super.finalize();
        }
    }

    private void processResult(long id, TdApi.Object object) {
        Handler handler;
        if (id == 0) {
            handler = this.handlers.get(Long.valueOf(id));
            if ((object instanceof TdApi.UpdateAuthorizationState) && (((TdApi.UpdateAuthorizationState) object).authorizationState instanceof TdApi.AuthorizationStateClosed)) {
                this.stopFlag = true;
            }
        } else {
            handler = this.handlers.remove(Long.valueOf(id));
        }
        if (handler == null) {
            Log.e("DLTD", "Can't find handler for the result " + id + " -- ignore result");
            return;
        }
        handleResult(object, handler.resultHandler, handler.exceptionHandler);
    }

    private void handleResult(TdApi.Object object, ResultHandler resultHandler, ExceptionHandler exceptionHandler) {
        if (resultHandler != null) {
            try {
                resultHandler.onResult(object);
            } catch (Throwable cause) {
                if (exceptionHandler == null) {
                    exceptionHandler = this.defaultExceptionHandler;
                }
                if (exceptionHandler != null) {
                    try {
                        exceptionHandler.onException(cause);
                    } catch (Throwable th) {
                    }
                }
            }
        }
    }

    private void receiveQueries(double timeout) {
        int resultN = NativeClient.clientReceive(this.nativeClientId, this.eventIds, this.events, timeout);
        for (int i = 0; i < resultN; i++) {
            processResult(this.eventIds[i], this.events[i]);
            this.events[i] = null;
        }
    }
}
