package okio;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.ByteCompanionObject;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Utf8.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000D\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0005\n\u0000\n\u0002\u0010\f\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0012\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\u001a\u0011\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0001H\u0080\b\u001a\u0011\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0007H\u0080\b\u001a4\u0010\u0010\u001a\u00020\u0001*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a4\u0010\u0017\u001a\u00020\u0001*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a4\u0010\u0018\u001a\u00020\u0001*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a4\u0010\u0019\u001a\u00020\u0016*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a4\u0010\u001a\u001a\u00020\u0016*\u00020\u001b2\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a4\u0010\u001c\u001a\u00020\u0016*\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00012\u0006\u0010\u0013\u001a\u00020\u00012\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00160\u0015H\u0080\bø\u0001\u0000\u001a%\u0010\u001d\u001a\u00020\u001e*\u00020\u001b2\b\b\u0002\u0010\u0012\u001a\u00020\u00012\b\b\u0002\u0010\u0013\u001a\u00020\u0001H\u0007¢\u0006\u0002\b\u001f\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0007X\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\tX\u0080T¢\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0080T¢\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\b\u009920\u0001¨\u0006 "}, d2 = {"HIGH_SURROGATE_HEADER", "", "LOG_SURROGATE_HEADER", "MASK_2BYTES", "MASK_3BYTES", "MASK_4BYTES", "REPLACEMENT_BYTE", "", "REPLACEMENT_CHARACTER", "", "REPLACEMENT_CODE_POINT", "isIsoControl", "", "codePoint", "isUtf8Continuation", "byte", "process2Utf8Bytes", "", "beginIndex", "endIndex", "yield", "Lkotlin/Function1;", "", "process3Utf8Bytes", "process4Utf8Bytes", "processUtf16Chars", "processUtf8Bytes", "", "processUtf8CodePoints", "utf8Size", "", "size", "okio"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes2.dex */
public final class Utf8 {
    public static final int HIGH_SURROGATE_HEADER = 55232;
    public static final int LOG_SURROGATE_HEADER = 56320;
    public static final int MASK_2BYTES = 3968;
    public static final int MASK_3BYTES = -123008;
    public static final int MASK_4BYTES = 3678080;
    public static final byte REPLACEMENT_BYTE = 63;
    public static final char REPLACEMENT_CHARACTER = 65533;
    public static final int REPLACEMENT_CODE_POINT = 65533;

    public static final long size(String str) {
        return size$default(str, 0, 0, 3, null);
    }

    public static final long size(String str, int i) {
        return size$default(str, i, 0, 2, null);
    }

    public static /* synthetic */ long size$default(String str, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = 0;
        }
        if ((i3 & 2) != 0) {
            i2 = str.length();
        }
        return size(str, i, i2);
    }

    public static final long size(String utf8Size, int beginIndex, int endIndex) {
        Intrinsics.checkNotNullParameter(utf8Size, "$this$utf8Size");
        boolean z = true;
        if (beginIndex >= 0) {
            if (endIndex >= beginIndex) {
                if (endIndex > utf8Size.length()) {
                    z = false;
                }
                if (z) {
                    long result = 0;
                    int i = beginIndex;
                    while (i < endIndex) {
                        int c = utf8Size.charAt(i);
                        if (c < 128) {
                            result++;
                            i++;
                        } else if (c < 2048) {
                            result += 2;
                            i++;
                        } else if (c < 55296 || c > 57343) {
                            result += 3;
                            i++;
                        } else {
                            int low = i + 1 < endIndex ? utf8Size.charAt(i + 1) : 0;
                            if (c > 56319 || low < 56320 || low > 57343) {
                                result++;
                                i++;
                            } else {
                                result += 4;
                                i += 2;
                            }
                        }
                    }
                    return result;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + endIndex + " > " + utf8Size.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + endIndex + " < " + beginIndex).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + beginIndex).toString());
    }

    public static final boolean isIsoControl(int codePoint) {
        return (codePoint >= 0 && 31 >= codePoint) || (127 <= codePoint && 159 >= codePoint);
    }

    public static final boolean isUtf8Continuation(byte b) {
        int other$iv = 192 & b;
        return other$iv == 128;
    }

    public static final void processUtf8Bytes(String processUtf8Bytes, int beginIndex, int endIndex, Function1<? super Byte, Unit> yield) {
        char charAt;
        Intrinsics.checkNotNullParameter(processUtf8Bytes, "$this$processUtf8Bytes");
        Intrinsics.checkNotNullParameter(yield, "yield");
        int index = beginIndex;
        while (index < endIndex) {
            char c = processUtf8Bytes.charAt(index);
            if (Intrinsics.compare((int) c, 128) < 0) {
                yield.invoke(Byte.valueOf((byte) c));
                index++;
                while (index < endIndex && Intrinsics.compare((int) processUtf8Bytes.charAt(index), 128) < 0) {
                    yield.invoke(Byte.valueOf((byte) processUtf8Bytes.charAt(index)));
                    index++;
                }
            } else if (Intrinsics.compare((int) c, 2048) < 0) {
                yield.invoke(Byte.valueOf((byte) ((c >> 6) | 192)));
                yield.invoke(Byte.valueOf((byte) (128 | (c & '?'))));
                index++;
            } else if (55296 > c || 57343 < c) {
                yield.invoke(Byte.valueOf((byte) ((c >> '\f') | 224)));
                yield.invoke(Byte.valueOf((byte) (((c >> 6) & 63) | 128)));
                yield.invoke(Byte.valueOf((byte) (128 | (c & '?'))));
                index++;
            } else if (Intrinsics.compare((int) c, 56319) > 0 || endIndex <= index + 1 || 56320 > (charAt = processUtf8Bytes.charAt(index + 1)) || 57343 < charAt) {
                yield.invoke(Byte.valueOf((byte) REPLACEMENT_BYTE));
                index++;
            } else {
                int codePoint = ((c << '\n') + processUtf8Bytes.charAt(index + 1)) - 56613888;
                yield.invoke(Byte.valueOf((byte) ((codePoint >> 18) | 240)));
                yield.invoke(Byte.valueOf((byte) (((codePoint >> 12) & 63) | 128)));
                yield.invoke(Byte.valueOf((byte) ((63 & (codePoint >> 6)) | 128)));
                yield.invoke(Byte.valueOf((byte) (128 | (codePoint & 63))));
                index += 2;
            }
        }
    }

    public static final void processUtf8CodePoints(byte[] processUtf8CodePoints, int beginIndex, int endIndex, Function1<? super Integer, Unit> yield) {
        int i;
        int it;
        int i2;
        int it2;
        byte b1$iv;
        int it3;
        Intrinsics.checkNotNullParameter(processUtf8CodePoints, "$this$processUtf8CodePoints");
        Intrinsics.checkNotNullParameter(yield, "yield");
        int index = beginIndex;
        while (index < endIndex) {
            byte b0 = processUtf8CodePoints[index];
            if (b0 >= 0) {
                yield.invoke(Integer.valueOf(b0));
                index++;
                while (index < endIndex && processUtf8CodePoints[index] >= 0) {
                    yield.invoke(Integer.valueOf(processUtf8CodePoints[index]));
                    index++;
                }
            } else {
                int other$iv = b0 >> 5;
                if (other$iv == -2) {
                    if (endIndex <= index + 1) {
                        yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                        Unit unit = Unit.INSTANCE;
                        i = 1;
                    } else {
                        byte b0$iv = processUtf8CodePoints[index];
                        byte b1$iv2 = processUtf8CodePoints[index + 1];
                        int other$iv$iv$iv = b1$iv2 & 192;
                        if (!(other$iv$iv$iv == 128)) {
                            yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                            Unit unit2 = Unit.INSTANCE;
                            i = 1;
                        } else {
                            int codePoint$iv = (b1$iv2 ^ ByteCompanionObject.MIN_VALUE) ^ (b0$iv << 6);
                            if (codePoint$iv < 128) {
                                it = REPLACEMENT_CODE_POINT;
                            } else {
                                it = codePoint$iv;
                            }
                            yield.invoke(Integer.valueOf(it));
                            Unit unit3 = Unit.INSTANCE;
                            i = 2;
                        }
                    }
                    index += i;
                } else {
                    int other$iv2 = b0 >> 4;
                    if (other$iv2 == -2) {
                        if (endIndex <= index + 2) {
                            yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                            Unit unit4 = Unit.INSTANCE;
                            int it4 = index + 1;
                            if (endIndex > it4) {
                                byte byte$iv$iv = processUtf8CodePoints[index + 1];
                                int other$iv$iv$iv2 = 192 & byte$iv$iv;
                                if (other$iv$iv$iv2 == 128) {
                                    i2 = 2;
                                }
                            }
                            i2 = 1;
                        } else {
                            byte b0$iv2 = processUtf8CodePoints[index];
                            byte b1$iv3 = processUtf8CodePoints[index + 1];
                            if (!((b1$iv3 & 192) == 128)) {
                                yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                Unit unit5 = Unit.INSTANCE;
                                i2 = 1;
                            } else {
                                byte b2$iv = processUtf8CodePoints[index + 2];
                                if (!((b2$iv & 192) == 128)) {
                                    yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                    Unit unit6 = Unit.INSTANCE;
                                    i2 = 2;
                                } else {
                                    int codePoint$iv2 = (((-123008) ^ b2$iv) ^ (b1$iv3 << 6)) ^ (b0$iv2 << 12);
                                    if (codePoint$iv2 < 2048) {
                                        it2 = REPLACEMENT_CODE_POINT;
                                    } else if (55296 <= codePoint$iv2 && 57343 >= codePoint$iv2) {
                                        it2 = REPLACEMENT_CODE_POINT;
                                    } else {
                                        it2 = codePoint$iv2;
                                    }
                                    yield.invoke(Integer.valueOf(it2));
                                    Unit unit7 = Unit.INSTANCE;
                                    i2 = 3;
                                }
                            }
                        }
                        index += i2;
                    } else {
                        int other$iv3 = b0 >> 3;
                        if (other$iv3 == -2) {
                            if (endIndex <= index + 3) {
                                yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                Unit unit8 = Unit.INSTANCE;
                                int it5 = index + 1;
                                if (endIndex > it5) {
                                    byte byte$iv$iv2 = processUtf8CodePoints[index + 1];
                                    int other$iv$iv$iv3 = 192 & byte$iv$iv2;
                                    if (other$iv$iv$iv3 == 128) {
                                        if (endIndex > index + 2) {
                                            byte byte$iv$iv3 = processUtf8CodePoints[index + 2];
                                            int other$iv$iv$iv4 = 192 & byte$iv$iv3;
                                            if (other$iv$iv$iv4 == 128) {
                                                b1$iv = 3;
                                            }
                                        }
                                        b1$iv = 2;
                                    }
                                }
                                b1$iv = 1;
                            } else {
                                byte b0$iv3 = processUtf8CodePoints[index];
                                byte b1$iv4 = processUtf8CodePoints[index + 1];
                                if (!((b1$iv4 & 192) == 128)) {
                                    yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                    Unit unit9 = Unit.INSTANCE;
                                    b1$iv = 1;
                                } else {
                                    byte b2$iv2 = processUtf8CodePoints[index + 2];
                                    if (!((b2$iv2 & 192) == 128)) {
                                        yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                        Unit unit10 = Unit.INSTANCE;
                                        b1$iv = 2;
                                    } else {
                                        byte b3$iv = processUtf8CodePoints[index + 3];
                                        if (!((b3$iv & 192) == 128)) {
                                            yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                                            Unit unit11 = Unit.INSTANCE;
                                            b1$iv = 3;
                                        } else {
                                            int codePoint$iv3 = (((3678080 ^ b3$iv) ^ (b2$iv2 << 6)) ^ (b1$iv4 << 12)) ^ (b0$iv3 << 18);
                                            if (codePoint$iv3 > 1114111) {
                                                it3 = REPLACEMENT_CODE_POINT;
                                            } else if (55296 <= codePoint$iv3 && 57343 >= codePoint$iv3) {
                                                it3 = REPLACEMENT_CODE_POINT;
                                            } else if (codePoint$iv3 < 65536) {
                                                it3 = REPLACEMENT_CODE_POINT;
                                            } else {
                                                it3 = codePoint$iv3;
                                            }
                                            yield.invoke(Integer.valueOf(it3));
                                            Unit unit12 = Unit.INSTANCE;
                                            b1$iv = 4;
                                        }
                                    }
                                }
                            }
                            index += b1$iv;
                        } else {
                            yield.invoke(Integer.valueOf((int) REPLACEMENT_CODE_POINT));
                            index++;
                        }
                    }
                }
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:121:0x02ba, code lost:
        if (65533(0xfffd, float:9.1831E-41) != 65533(0xfffd, float:9.1831E-41)) goto L122;
     */
    /* JADX WARN: Code restructure failed: missing block: B:122:0x02bc, code lost:
        r27.invoke(java.lang.Character.valueOf((char) ((r12 >>> 10) + okio.Utf8.HIGH_SURROGATE_HEADER)));
        r27.invoke(java.lang.Character.valueOf((char) ((r12 & 1023) + okio.Utf8.LOG_SURROGATE_HEADER)));
     */
    /* JADX WARN: Code restructure failed: missing block: B:123:0x02d4, code lost:
        r27.invoke(java.lang.Character.valueOf(okio.Utf8.REPLACEMENT_CHARACTER));
     */
    /* JADX WARN: Code restructure failed: missing block: B:129:0x02eb, code lost:
        if (65533(0xfffd, float:9.1831E-41) != 65533(0xfffd, float:9.1831E-41)) goto L122;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x02f6, code lost:
        if (65533(0xfffd, float:9.1831E-41) != 65533(0xfffd, float:9.1831E-41)) goto L122;
     */
    /* JADX WARN: Code restructure failed: missing block: B:135:0x02fb, code lost:
        if (r12 != 65533) goto L122;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final void processUtf16Chars(byte[] r24, int r25, int r26, Function1<? super Character, Unit> r27) {
        /*
            Method dump skipped, instructions count: 784
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Utf8.processUtf16Chars(byte[], int, int, kotlin.jvm.functions.Function1):void");
    }

    public static final int process2Utf8Bytes(byte[] process2Utf8Bytes, int beginIndex, int endIndex, Function1<? super Integer, Unit> yield) {
        Intrinsics.checkNotNullParameter(process2Utf8Bytes, "$this$process2Utf8Bytes");
        Intrinsics.checkNotNullParameter(yield, "yield");
        int i = beginIndex + 1;
        Integer valueOf = Integer.valueOf((int) REPLACEMENT_CODE_POINT);
        if (endIndex <= i) {
            yield.invoke(valueOf);
            return 1;
        }
        byte b0 = process2Utf8Bytes[beginIndex];
        byte b1 = process2Utf8Bytes[beginIndex + 1];
        int other$iv$iv = 192 & b1;
        if (!(other$iv$iv == 128)) {
            yield.invoke(valueOf);
            return 1;
        }
        int codePoint = (b1 ^ ByteCompanionObject.MIN_VALUE) ^ (b0 << 6);
        if (codePoint < 128) {
            yield.invoke(valueOf);
            return 2;
        }
        yield.invoke(Integer.valueOf(codePoint));
        return 2;
    }

    public static final int process3Utf8Bytes(byte[] process3Utf8Bytes, int beginIndex, int endIndex, Function1<? super Integer, Unit> yield) {
        Intrinsics.checkNotNullParameter(process3Utf8Bytes, "$this$process3Utf8Bytes");
        Intrinsics.checkNotNullParameter(yield, "yield");
        int i = beginIndex + 2;
        boolean z = false;
        Integer valueOf = Integer.valueOf((int) REPLACEMENT_CODE_POINT);
        if (endIndex <= i) {
            yield.invoke(valueOf);
            if (endIndex > beginIndex + 1) {
                byte byte$iv = process3Utf8Bytes[beginIndex + 1];
                int other$iv$iv = 192 & byte$iv;
                if (other$iv$iv == 128) {
                    z = true;
                }
                if (z) {
                    return 2;
                }
            }
            return 1;
        }
        byte b0 = process3Utf8Bytes[beginIndex];
        byte b1 = process3Utf8Bytes[beginIndex + 1];
        int other$iv$iv2 = 192 & b1;
        if (!(other$iv$iv2 == 128)) {
            yield.invoke(valueOf);
            return 1;
        }
        byte b2 = process3Utf8Bytes[beginIndex + 2];
        int other$iv$iv3 = 192 & b2;
        if (other$iv$iv3 == 128) {
            z = true;
        }
        if (!z) {
            yield.invoke(valueOf);
            return 2;
        }
        int codePoint = (((-123008) ^ b2) ^ (b1 << 6)) ^ (b0 << 12);
        if (codePoint < 2048) {
            yield.invoke(valueOf);
            return 3;
        } else if (55296 <= codePoint && 57343 >= codePoint) {
            yield.invoke(valueOf);
            return 3;
        } else {
            yield.invoke(Integer.valueOf(codePoint));
            return 3;
        }
    }

    public static final int process4Utf8Bytes(byte[] process4Utf8Bytes, int beginIndex, int endIndex, Function1<? super Integer, Unit> yield) {
        Intrinsics.checkNotNullParameter(process4Utf8Bytes, "$this$process4Utf8Bytes");
        Intrinsics.checkNotNullParameter(yield, "yield");
        int i = beginIndex + 3;
        boolean z = false;
        Integer valueOf = Integer.valueOf((int) REPLACEMENT_CODE_POINT);
        if (endIndex <= i) {
            yield.invoke(valueOf);
            if (endIndex > beginIndex + 1) {
                byte byte$iv = process4Utf8Bytes[beginIndex + 1];
                int other$iv$iv = 192 & byte$iv;
                if (other$iv$iv == 128) {
                    if (endIndex > beginIndex + 2) {
                        byte byte$iv2 = process4Utf8Bytes[beginIndex + 2];
                        int other$iv$iv2 = 192 & byte$iv2;
                        if (other$iv$iv2 == 128) {
                            z = true;
                        }
                        if (z) {
                            return 3;
                        }
                    }
                    return 2;
                }
            }
            return 1;
        }
        byte b0 = process4Utf8Bytes[beginIndex];
        byte b1 = process4Utf8Bytes[beginIndex + 1];
        int other$iv$iv3 = 192 & b1;
        if (!(other$iv$iv3 == 128)) {
            yield.invoke(valueOf);
            return 1;
        }
        byte b2 = process4Utf8Bytes[beginIndex + 2];
        int other$iv$iv4 = 192 & b2;
        if (!(other$iv$iv4 == 128)) {
            yield.invoke(valueOf);
            return 2;
        }
        byte b3 = process4Utf8Bytes[beginIndex + 3];
        int other$iv$iv5 = 192 & b3;
        if (other$iv$iv5 == 128) {
            z = true;
        }
        if (!z) {
            yield.invoke(valueOf);
            return 3;
        }
        int codePoint = (((3678080 ^ b3) ^ (b2 << 6)) ^ (b1 << 12)) ^ (b0 << 18);
        if (codePoint > 1114111) {
            yield.invoke(valueOf);
            return 4;
        } else if (55296 <= codePoint && 57343 >= codePoint) {
            yield.invoke(valueOf);
            return 4;
        } else if (codePoint < 65536) {
            yield.invoke(valueOf);
            return 4;
        } else {
            yield.invoke(Integer.valueOf(codePoint));
            return 4;
        }
    }
}
