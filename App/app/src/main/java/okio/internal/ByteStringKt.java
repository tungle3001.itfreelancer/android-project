package okio.internal;

import java.util.Arrays;
import kotlin.Metadata;
import kotlin.UByte;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt;
import okio.Base64;
import okio.Buffer;
import okio.ByteString;
import okio.Platform;
import okio.SegmentedByteString;
import okio.Util;

/* compiled from: ByteString.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000X\n\u0000\n\u0002\u0010\u0019\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\f\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0005\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0018\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H\u0002\u001a\u0011\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0007H\u0080\b\u001a\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0002\u001a\r\u0010\u000f\u001a\u00020\u0010*\u00020\nH\u0080\b\u001a\r\u0010\u0011\u001a\u00020\u0010*\u00020\nH\u0080\b\u001a\u0015\u0010\u0012\u001a\u00020\u0005*\u00020\n2\u0006\u0010\u0013\u001a\u00020\nH\u0080\b\u001a\u000f\u0010\u0014\u001a\u0004\u0018\u00010\n*\u00020\u0010H\u0080\b\u001a\r\u0010\u0015\u001a\u00020\n*\u00020\u0010H\u0080\b\u001a\u0014\u0010\u0016\u001a\u00020\n*\u00020\n2\u0006\u0010\u0017\u001a\u00020\u0010H\u0000\u001a\r\u0010\u0018\u001a\u00020\n*\u00020\u0010H\u0080\b\u001a\u0015\u0010\u0019\u001a\u00020\u001a*\u00020\n2\u0006\u0010\u001b\u001a\u00020\u0007H\u0080\b\u001a\u0015\u0010\u0019\u001a\u00020\u001a*\u00020\n2\u0006\u0010\u001b\u001a\u00020\nH\u0080\b\u001a\u0017\u0010\u001c\u001a\u00020\u001a*\u00020\n2\b\u0010\u0013\u001a\u0004\u0018\u00010\u001dH\u0080\b\u001a\u0015\u0010\u001e\u001a\u00020\u001f*\u00020\n2\u0006\u0010 \u001a\u00020\u0005H\u0080\b\u001a\r\u0010!\u001a\u00020\u0005*\u00020\nH\u0080\b\u001a\r\u0010\"\u001a\u00020\u0005*\u00020\nH\u0080\b\u001a\r\u0010#\u001a\u00020\u0010*\u00020\nH\u0080\b\u001a\u001d\u0010$\u001a\u00020\u0005*\u00020\n2\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0005H\u0080\b\u001a\r\u0010&\u001a\u00020\u0007*\u00020\nH\u0080\b\u001a\u001d\u0010'\u001a\u00020\u0005*\u00020\n2\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010%\u001a\u00020\u0005H\u0080\b\u001a\u001d\u0010'\u001a\u00020\u0005*\u00020\n2\u0006\u0010\u0013\u001a\u00020\n2\u0006\u0010%\u001a\u00020\u0005H\u0080\b\u001a-\u0010(\u001a\u00020\u001a*\u00020\n2\u0006\u0010)\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010*\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0005H\u0080\b\u001a-\u0010(\u001a\u00020\u001a*\u00020\n2\u0006\u0010)\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\n2\u0006\u0010*\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0005H\u0080\b\u001a\u0014\u0010,\u001a\u00020\n*\u00020-2\u0006\u0010\u0017\u001a\u00020\u0010H\u0000\u001a\u0015\u0010.\u001a\u00020\u001a*\u00020\n2\u0006\u0010/\u001a\u00020\u0007H\u0080\b\u001a\u0015\u0010.\u001a\u00020\u001a*\u00020\n2\u0006\u0010/\u001a\u00020\nH\u0080\b\u001a\u001d\u00100\u001a\u00020\n*\u00020\n2\u0006\u00101\u001a\u00020\u00052\u0006\u00102\u001a\u00020\u0005H\u0080\b\u001a\r\u00103\u001a\u00020\n*\u00020\nH\u0080\b\u001a\r\u00104\u001a\u00020\n*\u00020\nH\u0080\b\u001a\r\u00105\u001a\u00020\u0007*\u00020\nH\u0080\b\u001a\u001d\u00106\u001a\u00020\n*\u00020\u00072\u0006\u0010)\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0005H\u0080\b\u001a\r\u00107\u001a\u00020\u0010*\u00020\nH\u0080\b\u001a\r\u00108\u001a\u00020\u0010*\u00020\nH\u0080\b\u001a$\u00109\u001a\u00020:*\u00020\n2\u0006\u0010;\u001a\u00020<2\u0006\u0010)\u001a\u00020\u00052\u0006\u0010+\u001a\u00020\u0005H\u0000\"\u0014\u0010\u0000\u001a\u00020\u0001X\u0080\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0003¨\u0006="}, d2 = {"HEX_DIGIT_CHARS", "", "getHEX_DIGIT_CHARS", "()[C", "codePointIndexToCharIndex", "", "s", "", "codePointCount", "commonOf", "Lokio/ByteString;", "data", "decodeHexDigit", "c", "", "commonBase64", "", "commonBase64Url", "commonCompareTo", "other", "commonDecodeBase64", "commonDecodeHex", "commonDigest", "algorithm", "commonEncodeUtf8", "commonEndsWith", "", "suffix", "commonEquals", "", "commonGetByte", "", "pos", "commonGetSize", "commonHashCode", "commonHex", "commonIndexOf", "fromIndex", "commonInternalArray", "commonLastIndexOf", "commonRangeEquals", "offset", "otherOffset", "byteCount", "commonSegmentDigest", "Lokio/SegmentedByteString;", "commonStartsWith", "prefix", "commonSubstring", "beginIndex", "endIndex", "commonToAsciiLowercase", "commonToAsciiUppercase", "commonToByteArray", "commonToByteString", "commonToString", "commonUtf8", "commonWrite", "", "buffer", "Lokio/Buffer;", "okio"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes2.dex */
public final class ByteStringKt {
    private static final char[] HEX_DIGIT_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static final /* synthetic */ int access$codePointIndexToCharIndex(byte[] s, int codePointCount) {
        return codePointIndexToCharIndex(s, codePointCount);
    }

    public static final /* synthetic */ int access$decodeHexDigit(char c) {
        return decodeHexDigit(c);
    }

    public static final String commonUtf8(ByteString commonUtf8) {
        Intrinsics.checkNotNullParameter(commonUtf8, "$this$commonUtf8");
        String result = commonUtf8.getUtf8$okio();
        if (result != null) {
            return result;
        }
        String result2 = Platform.toUtf8String(commonUtf8.internalArray$okio());
        commonUtf8.setUtf8$okio(result2);
        return result2;
    }

    public static final String commonBase64(ByteString commonBase64) {
        Intrinsics.checkNotNullParameter(commonBase64, "$this$commonBase64");
        return Base64.encodeBase64$default(commonBase64.getData$okio(), null, 1, null);
    }

    public static final String commonBase64Url(ByteString commonBase64Url) {
        Intrinsics.checkNotNullParameter(commonBase64Url, "$this$commonBase64Url");
        return Base64.encodeBase64(commonBase64Url.getData$okio(), Base64.getBASE64_URL_SAFE());
    }

    public static final char[] getHEX_DIGIT_CHARS() {
        return HEX_DIGIT_CHARS;
    }

    public static final String commonHex(ByteString commonHex) {
        byte[] data$okio;
        Intrinsics.checkNotNullParameter(commonHex, "$this$commonHex");
        char[] result = new char[commonHex.getData$okio().length * 2];
        int c = 0;
        for (byte b : commonHex.getData$okio()) {
            int c2 = c + 1;
            int other$iv = b >> 4;
            result[c] = getHEX_DIGIT_CHARS()[other$iv & 15];
            c = c2 + 1;
            int other$iv2 = 15 & b;
            result[c2] = getHEX_DIGIT_CHARS()[other$iv2];
        }
        return new String(result);
    }

    public static final ByteString commonDigest(ByteString commonDigest, String algorithm) {
        Intrinsics.checkNotNullParameter(commonDigest, "$this$commonDigest");
        Intrinsics.checkNotNullParameter(algorithm, "algorithm");
        HashFunction $this$run = HashFunctionKt.newHashFunction(algorithm);
        $this$run.update(commonDigest.getData$okio(), 0, commonDigest.size());
        byte[] digestBytes = $this$run.digest();
        return new ByteString(digestBytes);
    }

    public static final ByteString commonSegmentDigest(SegmentedByteString commonSegmentDigest, String algorithm) {
        Intrinsics.checkNotNullParameter(commonSegmentDigest, "$this$commonSegmentDigest");
        Intrinsics.checkNotNullParameter(algorithm, "algorithm");
        HashFunction $this$run = HashFunctionKt.newHashFunction(algorithm);
        int segmentCount$iv = commonSegmentDigest.getSegments$okio().length;
        int pos$iv = 0;
        for (int s$iv = 0; s$iv < segmentCount$iv; s$iv++) {
            int segmentPos$iv = commonSegmentDigest.getDirectory$okio()[segmentCount$iv + s$iv];
            int nextSegmentOffset$iv = commonSegmentDigest.getDirectory$okio()[s$iv];
            byte[] data = commonSegmentDigest.getSegments$okio()[s$iv];
            int byteCount = nextSegmentOffset$iv - pos$iv;
            $this$run.update(data, segmentPos$iv, byteCount);
            pos$iv = nextSegmentOffset$iv;
        }
        byte[] digestBytes = $this$run.digest();
        return new ByteString(digestBytes);
    }

    public static final ByteString commonToAsciiLowercase(ByteString commonToAsciiLowercase) {
        byte b;
        Intrinsics.checkNotNullParameter(commonToAsciiLowercase, "$this$commonToAsciiLowercase");
        for (int i = 0; i < commonToAsciiLowercase.getData$okio().length; i++) {
            byte c = commonToAsciiLowercase.getData$okio()[i];
            byte b2 = (byte) 65;
            if (c >= b2 && c <= (b = (byte) 90)) {
                byte[] data$okio = commonToAsciiLowercase.getData$okio();
                byte[] lowercase = Arrays.copyOf(data$okio, data$okio.length);
                Intrinsics.checkNotNullExpressionValue(lowercase, "java.util.Arrays.copyOf(this, size)");
                int i2 = i + 1;
                lowercase[i] = (byte) (c + 32);
                while (i2 < lowercase.length) {
                    byte c2 = lowercase[i2];
                    if (c2 < b2 || c2 > b) {
                        i2++;
                    } else {
                        lowercase[i2] = (byte) (c2 + 32);
                        i2++;
                    }
                }
                return new ByteString(lowercase);
            }
        }
        return commonToAsciiLowercase;
    }

    public static final ByteString commonToAsciiUppercase(ByteString commonToAsciiUppercase) {
        byte b;
        Intrinsics.checkNotNullParameter(commonToAsciiUppercase, "$this$commonToAsciiUppercase");
        for (int i = 0; i < commonToAsciiUppercase.getData$okio().length; i++) {
            byte c = commonToAsciiUppercase.getData$okio()[i];
            byte b2 = (byte) 97;
            if (c >= b2 && c <= (b = (byte) 122)) {
                byte[] data$okio = commonToAsciiUppercase.getData$okio();
                byte[] lowercase = Arrays.copyOf(data$okio, data$okio.length);
                Intrinsics.checkNotNullExpressionValue(lowercase, "java.util.Arrays.copyOf(this, size)");
                int i2 = i + 1;
                lowercase[i] = (byte) (c - 32);
                while (i2 < lowercase.length) {
                    byte c2 = lowercase[i2];
                    if (c2 < b2 || c2 > b) {
                        i2++;
                    } else {
                        lowercase[i2] = (byte) (c2 - 32);
                        i2++;
                    }
                }
                return new ByteString(lowercase);
            }
        }
        return commonToAsciiUppercase;
    }

    public static final ByteString commonSubstring(ByteString commonSubstring, int beginIndex, int endIndex) {
        Intrinsics.checkNotNullParameter(commonSubstring, "$this$commonSubstring");
        boolean z = true;
        if (beginIndex >= 0) {
            if (endIndex <= commonSubstring.getData$okio().length) {
                int subLen = endIndex - beginIndex;
                if (subLen < 0) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException("endIndex < beginIndex".toString());
                } else if (beginIndex == 0 && endIndex == commonSubstring.getData$okio().length) {
                    return commonSubstring;
                } else {
                    return new ByteString(ArraysKt.copyOfRange(commonSubstring.getData$okio(), beginIndex, endIndex));
                }
            } else {
                throw new IllegalArgumentException(("endIndex > length(" + commonSubstring.getData$okio().length + ')').toString());
            }
        } else {
            throw new IllegalArgumentException("beginIndex < 0".toString());
        }
    }

    public static final byte commonGetByte(ByteString commonGetByte, int pos) {
        Intrinsics.checkNotNullParameter(commonGetByte, "$this$commonGetByte");
        return commonGetByte.getData$okio()[pos];
    }

    public static final int commonGetSize(ByteString commonGetSize) {
        Intrinsics.checkNotNullParameter(commonGetSize, "$this$commonGetSize");
        return commonGetSize.getData$okio().length;
    }

    public static final byte[] commonToByteArray(ByteString commonToByteArray) {
        Intrinsics.checkNotNullParameter(commonToByteArray, "$this$commonToByteArray");
        byte[] data$okio = commonToByteArray.getData$okio();
        byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
        Intrinsics.checkNotNullExpressionValue(copyOf, "java.util.Arrays.copyOf(this, size)");
        return copyOf;
    }

    public static final byte[] commonInternalArray(ByteString commonInternalArray) {
        Intrinsics.checkNotNullParameter(commonInternalArray, "$this$commonInternalArray");
        return commonInternalArray.getData$okio();
    }

    public static final boolean commonRangeEquals(ByteString commonRangeEquals, int offset, ByteString other, int otherOffset, int byteCount) {
        Intrinsics.checkNotNullParameter(commonRangeEquals, "$this$commonRangeEquals");
        Intrinsics.checkNotNullParameter(other, "other");
        return other.rangeEquals(otherOffset, commonRangeEquals.getData$okio(), offset, byteCount);
    }

    public static final boolean commonRangeEquals(ByteString commonRangeEquals, int offset, byte[] other, int otherOffset, int byteCount) {
        Intrinsics.checkNotNullParameter(commonRangeEquals, "$this$commonRangeEquals");
        Intrinsics.checkNotNullParameter(other, "other");
        return offset >= 0 && offset <= commonRangeEquals.getData$okio().length - byteCount && otherOffset >= 0 && otherOffset <= other.length - byteCount && Util.arrayRangeEquals(commonRangeEquals.getData$okio(), offset, other, otherOffset, byteCount);
    }

    public static final boolean commonStartsWith(ByteString commonStartsWith, ByteString prefix) {
        Intrinsics.checkNotNullParameter(commonStartsWith, "$this$commonStartsWith");
        Intrinsics.checkNotNullParameter(prefix, "prefix");
        return commonStartsWith.rangeEquals(0, prefix, 0, prefix.size());
    }

    public static final boolean commonStartsWith(ByteString commonStartsWith, byte[] prefix) {
        Intrinsics.checkNotNullParameter(commonStartsWith, "$this$commonStartsWith");
        Intrinsics.checkNotNullParameter(prefix, "prefix");
        return commonStartsWith.rangeEquals(0, prefix, 0, prefix.length);
    }

    public static final boolean commonEndsWith(ByteString commonEndsWith, ByteString suffix) {
        Intrinsics.checkNotNullParameter(commonEndsWith, "$this$commonEndsWith");
        Intrinsics.checkNotNullParameter(suffix, "suffix");
        return commonEndsWith.rangeEquals(commonEndsWith.size() - suffix.size(), suffix, 0, suffix.size());
    }

    public static final boolean commonEndsWith(ByteString commonEndsWith, byte[] suffix) {
        Intrinsics.checkNotNullParameter(commonEndsWith, "$this$commonEndsWith");
        Intrinsics.checkNotNullParameter(suffix, "suffix");
        return commonEndsWith.rangeEquals(commonEndsWith.size() - suffix.length, suffix, 0, suffix.length);
    }

    public static final int commonIndexOf(ByteString commonIndexOf, byte[] other, int fromIndex) {
        Intrinsics.checkNotNullParameter(commonIndexOf, "$this$commonIndexOf");
        Intrinsics.checkNotNullParameter(other, "other");
        int limit = commonIndexOf.getData$okio().length - other.length;
        int i = Math.max(fromIndex, 0);
        if (i > limit) {
            return -1;
        }
        while (!Util.arrayRangeEquals(commonIndexOf.getData$okio(), i, other, 0, other.length)) {
            if (i == limit) {
                return -1;
            }
            i++;
        }
        return i;
    }

    public static final int commonLastIndexOf(ByteString commonLastIndexOf, ByteString other, int fromIndex) {
        Intrinsics.checkNotNullParameter(commonLastIndexOf, "$this$commonLastIndexOf");
        Intrinsics.checkNotNullParameter(other, "other");
        return commonLastIndexOf.lastIndexOf(other.internalArray$okio(), fromIndex);
    }

    public static final int commonLastIndexOf(ByteString commonLastIndexOf, byte[] other, int fromIndex) {
        Intrinsics.checkNotNullParameter(commonLastIndexOf, "$this$commonLastIndexOf");
        Intrinsics.checkNotNullParameter(other, "other");
        int limit = commonLastIndexOf.getData$okio().length - other.length;
        for (int i = Math.min(fromIndex, limit); i >= 0; i--) {
            if (Util.arrayRangeEquals(commonLastIndexOf.getData$okio(), i, other, 0, other.length)) {
                return i;
            }
        }
        return -1;
    }

    public static final boolean commonEquals(ByteString commonEquals, Object other) {
        Intrinsics.checkNotNullParameter(commonEquals, "$this$commonEquals");
        if (other == commonEquals) {
            return true;
        }
        return (other instanceof ByteString) && ((ByteString) other).size() == commonEquals.getData$okio().length && ((ByteString) other).rangeEquals(0, commonEquals.getData$okio(), 0, commonEquals.getData$okio().length);
    }

    public static final int commonHashCode(ByteString commonHashCode) {
        Intrinsics.checkNotNullParameter(commonHashCode, "$this$commonHashCode");
        int result = commonHashCode.getHashCode$okio();
        if (result != 0) {
            return result;
        }
        int it = Arrays.hashCode(commonHashCode.getData$okio());
        commonHashCode.setHashCode$okio(it);
        return it;
    }

    public static final int commonCompareTo(ByteString commonCompareTo, ByteString other) {
        Intrinsics.checkNotNullParameter(commonCompareTo, "$this$commonCompareTo");
        Intrinsics.checkNotNullParameter(other, "other");
        int sizeA = commonCompareTo.size();
        int sizeB = other.size();
        int size = Math.min(sizeA, sizeB);
        for (int i = 0; i < size; i++) {
            byte $this$and$iv = commonCompareTo.getByte(i);
            int byteA = $this$and$iv & UByte.MAX_VALUE;
            byte $this$and$iv2 = other.getByte(i);
            int byteB = $this$and$iv2 & UByte.MAX_VALUE;
            if (byteA != byteB) {
                return byteA < byteB ? -1 : 1;
            }
        }
        if (sizeA == sizeB) {
            return 0;
        }
        return sizeA < sizeB ? -1 : 1;
    }

    public static final ByteString commonOf(byte[] data) {
        Intrinsics.checkNotNullParameter(data, "data");
        byte[] copyOf = Arrays.copyOf(data, data.length);
        Intrinsics.checkNotNullExpressionValue(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new ByteString(copyOf);
    }

    public static final ByteString commonToByteString(byte[] commonToByteString, int offset, int byteCount) {
        Intrinsics.checkNotNullParameter(commonToByteString, "$this$commonToByteString");
        Util.checkOffsetAndCount(commonToByteString.length, offset, byteCount);
        return new ByteString(ArraysKt.copyOfRange(commonToByteString, offset, offset + byteCount));
    }

    public static final ByteString commonEncodeUtf8(String commonEncodeUtf8) {
        Intrinsics.checkNotNullParameter(commonEncodeUtf8, "$this$commonEncodeUtf8");
        ByteString byteString = new ByteString(Platform.asUtf8ToByteArray(commonEncodeUtf8));
        byteString.setUtf8$okio(commonEncodeUtf8);
        return byteString;
    }

    public static final ByteString commonDecodeBase64(String commonDecodeBase64) {
        Intrinsics.checkNotNullParameter(commonDecodeBase64, "$this$commonDecodeBase64");
        byte[] decoded = Base64.decodeBase64ToArray(commonDecodeBase64);
        if (decoded != null) {
            return new ByteString(decoded);
        }
        return null;
    }

    public static final ByteString commonDecodeHex(String commonDecodeHex) {
        Intrinsics.checkNotNullParameter(commonDecodeHex, "$this$commonDecodeHex");
        if (commonDecodeHex.length() % 2 == 0) {
            byte[] result = new byte[commonDecodeHex.length() / 2];
            int length = result.length;
            for (int i = 0; i < length; i++) {
                int d1 = decodeHexDigit(commonDecodeHex.charAt(i * 2)) << 4;
                int d2 = decodeHexDigit(commonDecodeHex.charAt((i * 2) + 1));
                result[i] = (byte) (d1 + d2);
            }
            return new ByteString(result);
        }
        throw new IllegalArgumentException(("Unexpected hex string: " + commonDecodeHex).toString());
    }

    public static final void commonWrite(ByteString commonWrite, Buffer buffer, int offset, int byteCount) {
        Intrinsics.checkNotNullParameter(commonWrite, "$this$commonWrite");
        Intrinsics.checkNotNullParameter(buffer, "buffer");
        buffer.write(commonWrite.getData$okio(), offset, byteCount);
    }

    public static final int decodeHexDigit(char c) {
        if ('0' <= c && '9' >= c) {
            return c - '0';
        }
        if ('a' <= c && 'f' >= c) {
            return (c - 'a') + 10;
        }
        if ('A' <= c && 'F' >= c) {
            return (c - 'A') + 10;
        }
        throw new IllegalArgumentException("Unexpected hex digit: " + c);
    }

    public static final String commonToString(ByteString commonToString) {
        Intrinsics.checkNotNullParameter(commonToString, "$this$commonToString");
        boolean z = true;
        if (commonToString.getData$okio().length == 0) {
            return "[size=0]";
        }
        int i = codePointIndexToCharIndex(commonToString.getData$okio(), 64);
        if (i != -1) {
            String text = commonToString.utf8();
            if (text != null) {
                String substring = text.substring(0, i);
                Intrinsics.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                String safeText = StringsKt.replace$default(StringsKt.replace$default(StringsKt.replace$default(substring, "\\", "\\\\", false, 4, (Object) null), "\n", "\\n", false, 4, (Object) null), "\r", "\\r", false, 4, (Object) null);
                if (i < text.length()) {
                    return "[size=" + commonToString.getData$okio().length + " text=" + safeText + "…]";
                }
                return "[text=" + safeText + ']';
            }
            throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
        } else if (commonToString.getData$okio().length <= 64) {
            return "[hex=" + commonToString.hex() + ']';
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[size=");
            sb.append(commonToString.getData$okio().length);
            sb.append(" hex=");
            ByteString $this$commonSubstring$iv = commonToString;
            if (64 <= $this$commonSubstring$iv.getData$okio().length) {
                int subLen$iv = 64 - 0;
                if (subLen$iv < 0) {
                    z = false;
                }
                if (z) {
                    if (64 != $this$commonSubstring$iv.getData$okio().length) {
                        $this$commonSubstring$iv = new ByteString(ArraysKt.copyOfRange($this$commonSubstring$iv.getData$okio(), 0, 64));
                    }
                    sb.append($this$commonSubstring$iv.hex());
                    sb.append("…]");
                    return sb.toString();
                }
                throw new IllegalArgumentException("endIndex < beginIndex".toString());
            }
            throw new IllegalArgumentException(("endIndex > length(" + $this$commonSubstring$iv.getData$okio().length + ')').toString());
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:120:0x017d, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L126;
     */
    /* JADX WARN: Code restructure failed: missing block: B:125:0x0189, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L127;
     */
    /* JADX WARN: Code restructure failed: missing block: B:126:0x018b, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:145:0x01c0, code lost:
        if (31 >= r3) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x01cc, code lost:
        if (159 < r3) goto L152;
     */
    /* JADX WARN: Code restructure failed: missing block: B:151:0x01ce, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:195:0x024f, code lost:
        if (r16 == false) goto L196;
     */
    /* JADX WARN: Code restructure failed: missing block: B:212:0x028d, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L218;
     */
    /* JADX WARN: Code restructure failed: missing block: B:217:0x0299, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L219;
     */
    /* JADX WARN: Code restructure failed: missing block: B:218:0x029b, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:242:0x02eb, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L248;
     */
    /* JADX WARN: Code restructure failed: missing block: B:247:0x02f7, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L249;
     */
    /* JADX WARN: Code restructure failed: missing block: B:248:0x02f9, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:269:0x0347, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L275;
     */
    /* JADX WARN: Code restructure failed: missing block: B:274:0x0353, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L276;
     */
    /* JADX WARN: Code restructure failed: missing block: B:275:0x0355, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:299:0x0397, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L305;
     */
    /* JADX WARN: Code restructure failed: missing block: B:304:0x03a3, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L306;
     */
    /* JADX WARN: Code restructure failed: missing block: B:305:0x03a5, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:311:0x03b2, code lost:
        if (65533(0xfffd, float:9.1831E-41) < 65536(0x10000, float:9.18355E-41)) goto L335;
     */
    /* JADX WARN: Code restructure failed: missing block: B:322:0x03d0, code lost:
        if (31 >= r4) goto L328;
     */
    /* JADX WARN: Code restructure failed: missing block: B:327:0x03dc, code lost:
        if (159 < r4) goto L329;
     */
    /* JADX WARN: Code restructure failed: missing block: B:328:0x03de, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:334:0x03eb, code lost:
        if (r4 < 65536) goto L335;
     */
    /* JADX WARN: Code restructure failed: missing block: B:335:0x03ed, code lost:
        r17 = 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:336:0x03ef, code lost:
        r1 = r1 + r17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:398:0x04b4, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L404;
     */
    /* JADX WARN: Code restructure failed: missing block: B:403:0x04c0, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L405;
     */
    /* JADX WARN: Code restructure failed: missing block: B:404:0x04c2, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:428:0x0510, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L434;
     */
    /* JADX WARN: Code restructure failed: missing block: B:433:0x051c, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L435;
     */
    /* JADX WARN: Code restructure failed: missing block: B:434:0x051e, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:458:0x056f, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L464;
     */
    /* JADX WARN: Code restructure failed: missing block: B:463:0x057b, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L465;
     */
    /* JADX WARN: Code restructure failed: missing block: B:464:0x057d, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:485:0x05d0, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L491;
     */
    /* JADX WARN: Code restructure failed: missing block: B:490:0x05dc, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L492;
     */
    /* JADX WARN: Code restructure failed: missing block: B:491:0x05de, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:515:0x0622, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L521;
     */
    /* JADX WARN: Code restructure failed: missing block: B:520:0x062e, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L522;
     */
    /* JADX WARN: Code restructure failed: missing block: B:521:0x0630, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:527:0x063d, code lost:
        if (65533(0xfffd, float:9.1831E-41) < 65536(0x10000, float:9.18355E-41)) goto L528;
     */
    /* JADX WARN: Code restructure failed: missing block: B:528:0x063f, code lost:
        r17 = 1;
     */
    /* JADX WARN: Code restructure failed: missing block: B:529:0x0641, code lost:
        r1 = r1 + r17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:542:0x0666, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L548;
     */
    /* JADX WARN: Code restructure failed: missing block: B:547:0x0672, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L549;
     */
    /* JADX WARN: Code restructure failed: missing block: B:548:0x0674, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:554:0x0681, code lost:
        if (65533(0xfffd, float:9.1831E-41) < 65536(0x10000, float:9.18355E-41)) goto L528;
     */
    /* JADX WARN: Code restructure failed: missing block: B:565:0x069f, code lost:
        if (31 >= r3) goto L571;
     */
    /* JADX WARN: Code restructure failed: missing block: B:570:0x06ab, code lost:
        if (159 < r3) goto L572;
     */
    /* JADX WARN: Code restructure failed: missing block: B:571:0x06ad, code lost:
        r16 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:577:0x06ba, code lost:
        if (r3 < 65536) goto L528;
     */
    /* JADX WARN: Code restructure failed: missing block: B:93:0x0125, code lost:
        if (31 >= 65533(0xfffd, float:9.1831E-41)) goto L99;
     */
    /* JADX WARN: Code restructure failed: missing block: B:98:0x0131, code lost:
        if (159(0x9f, float:2.23E-43) < 65533(0xfffd, float:9.1831E-41)) goto L100;
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x0133, code lost:
        r16 = true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public static final int codePointIndexToCharIndex(byte[] r29, int r30) {
        /*
            Method dump skipped, instructions count: 1799
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.internal.ByteStringKt.codePointIndexToCharIndex(byte[], int):int");
    }
}
