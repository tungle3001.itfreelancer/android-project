package okio;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import kotlin.Deprecated;
import kotlin.DeprecationLevel;
import kotlin.Metadata;
import kotlin.ReplaceWith;
import kotlin.UByte;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.ByteCompanionObject;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.LongCompanionObject;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.Charsets;
import kotlin.text.Typography;
import okhttp3.internal.connection.RealConnection;
import okio.internal.BufferKt;
import tamhoang.ldpro4.constants.Constants;

/* compiled from: Buffer.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000ª\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u001a\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u0005\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\n\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0017\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u0090\u0001B\u0005¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0000H\u0016J\u0006\u0010\u0011\u001a\u00020\u0012J\b\u0010\u0013\u001a\u00020\u0000H\u0016J\b\u0010\u0014\u001a\u00020\u0012H\u0016J\u0006\u0010\u0015\u001a\u00020\fJ\u0006\u0010\u0016\u001a\u00020\u0000J$\u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\f2\b\b\u0002\u0010\u001b\u001a\u00020\fH\u0007J\u0018\u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u001a\u001a\u00020\fJ \u0010\u0017\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00002\b\b\u0002\u0010\u001a\u001a\u00020\f2\u0006\u0010\u001b\u001a\u00020\fJ\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\b\u0010 \u001a\u00020\u0000H\u0016J\b\u0010!\u001a\u00020\u0000H\u0016J\u0013\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%H\u0096\u0002J\b\u0010&\u001a\u00020#H\u0016J\b\u0010'\u001a\u00020\u0012H\u0016J\u0016\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\fH\u0087\u0002¢\u0006\u0002\b+J\u0015\u0010+\u001a\u00020)2\u0006\u0010,\u001a\u00020\fH\u0007¢\u0006\u0002\b-J\b\u0010.\u001a\u00020/H\u0016J\u0018\u00100\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u00101\u001a\u00020\u001dH\u0002J\u000e\u00102\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\u001dJ\u000e\u00103\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\u001dJ\u000e\u00104\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\u001dJ\u0010\u00105\u001a\u00020\f2\u0006\u00106\u001a\u00020)H\u0016J\u0018\u00105\u001a\u00020\f2\u0006\u00106\u001a\u00020)2\u0006\u00107\u001a\u00020\fH\u0016J \u00105\u001a\u00020\f2\u0006\u00106\u001a\u00020)2\u0006\u00107\u001a\u00020\f2\u0006\u00108\u001a\u00020\fH\u0016J\u0010\u00105\u001a\u00020\f2\u0006\u00109\u001a\u00020\u001dH\u0016J\u0018\u00105\u001a\u00020\f2\u0006\u00109\u001a\u00020\u001d2\u0006\u00107\u001a\u00020\fH\u0016J\u0010\u0010:\u001a\u00020\f2\u0006\u0010;\u001a\u00020\u001dH\u0016J\u0018\u0010:\u001a\u00020\f2\u0006\u0010;\u001a\u00020\u001d2\u0006\u00107\u001a\u00020\fH\u0016J\b\u0010<\u001a\u00020=H\u0016J\b\u0010>\u001a\u00020#H\u0016J\u0006\u0010?\u001a\u00020\u001dJ\b\u0010@\u001a\u00020\u0019H\u0016J\b\u0010A\u001a\u00020\u0001H\u0016J\u0018\u0010B\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\f2\u0006\u00109\u001a\u00020\u001dH\u0016J(\u0010B\u001a\u00020#2\u0006\u0010\u001a\u001a\u00020\f2\u0006\u00109\u001a\u00020\u001d2\u0006\u0010C\u001a\u00020/2\u0006\u0010\u001b\u001a\u00020/H\u0016J\u0010\u0010D\u001a\u00020/2\u0006\u0010E\u001a\u00020FH\u0016J\u0010\u0010D\u001a\u00020/2\u0006\u0010E\u001a\u00020GH\u0016J \u0010D\u001a\u00020/2\u0006\u0010E\u001a\u00020G2\u0006\u0010\u001a\u001a\u00020/2\u0006\u0010\u001b\u001a\u00020/H\u0016J\u0018\u0010D\u001a\u00020\f2\u0006\u0010E\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010H\u001a\u00020\f2\u0006\u0010E\u001a\u00020IH\u0016J\u0012\u0010J\u001a\u00020K2\b\b\u0002\u0010L\u001a\u00020KH\u0007J\b\u0010M\u001a\u00020)H\u0016J\b\u0010N\u001a\u00020GH\u0016J\u0010\u0010N\u001a\u00020G2\u0006\u0010\u001b\u001a\u00020\fH\u0016J\b\u0010O\u001a\u00020\u001dH\u0016J\u0010\u0010O\u001a\u00020\u001d2\u0006\u0010\u001b\u001a\u00020\fH\u0016J\b\u0010P\u001a\u00020\fH\u0016J\u000e\u0010Q\u001a\u00020\u00002\u0006\u0010R\u001a\u00020=J\u0016\u0010Q\u001a\u00020\u00002\u0006\u0010R\u001a\u00020=2\u0006\u0010\u001b\u001a\u00020\fJ \u0010Q\u001a\u00020\u00122\u0006\u0010R\u001a\u00020=2\u0006\u0010\u001b\u001a\u00020\f2\u0006\u0010S\u001a\u00020#H\u0002J\u0010\u0010T\u001a\u00020\u00122\u0006\u0010E\u001a\u00020GH\u0016J\u0018\u0010T\u001a\u00020\u00122\u0006\u0010E\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\fH\u0016J\b\u0010U\u001a\u00020\fH\u0016J\b\u0010V\u001a\u00020/H\u0016J\b\u0010W\u001a\u00020/H\u0016J\b\u0010X\u001a\u00020\fH\u0016J\b\u0010Y\u001a\u00020\fH\u0016J\b\u0010Z\u001a\u00020[H\u0016J\b\u0010\\\u001a\u00020[H\u0016J\u0010\u0010]\u001a\u00020\u001f2\u0006\u0010^\u001a\u00020_H\u0016J\u0018\u0010]\u001a\u00020\u001f2\u0006\u0010\u001b\u001a\u00020\f2\u0006\u0010^\u001a\u00020_H\u0016J\u0012\u0010`\u001a\u00020K2\b\b\u0002\u0010L\u001a\u00020KH\u0007J\b\u0010a\u001a\u00020\u001fH\u0016J\u0010\u0010a\u001a\u00020\u001f2\u0006\u0010\u001b\u001a\u00020\fH\u0016J\b\u0010b\u001a\u00020/H\u0016J\n\u0010c\u001a\u0004\u0018\u00010\u001fH\u0016J\b\u0010d\u001a\u00020\u001fH\u0016J\u0010\u0010d\u001a\u00020\u001f2\u0006\u0010e\u001a\u00020\fH\u0016J\u0010\u0010f\u001a\u00020#2\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010g\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010h\u001a\u00020/2\u0006\u0010i\u001a\u00020jH\u0016J\u0006\u0010k\u001a\u00020\u001dJ\u0006\u0010l\u001a\u00020\u001dJ\u0006\u0010m\u001a\u00020\u001dJ\r\u0010\r\u001a\u00020\fH\u0007¢\u0006\u0002\bnJ\u0010\u0010o\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0006\u0010p\u001a\u00020\u001dJ\u000e\u0010p\u001a\u00020\u001d2\u0006\u0010\u001b\u001a\u00020/J\b\u0010q\u001a\u00020rH\u0016J\b\u0010s\u001a\u00020\u001fH\u0016J\u0015\u0010t\u001a\u00020\n2\u0006\u0010u\u001a\u00020/H\u0000¢\u0006\u0002\bvJ\u0010\u0010w\u001a\u00020/2\u0006\u0010x\u001a\u00020FH\u0016J\u0010\u0010w\u001a\u00020\u00002\u0006\u0010x\u001a\u00020GH\u0016J \u0010w\u001a\u00020\u00002\u0006\u0010x\u001a\u00020G2\u0006\u0010\u001a\u001a\u00020/2\u0006\u0010\u001b\u001a\u00020/H\u0016J\u0018\u0010w\u001a\u00020\u00122\u0006\u0010x\u001a\u00020\u00002\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010w\u001a\u00020\u00002\u0006\u0010y\u001a\u00020\u001dH\u0016J \u0010w\u001a\u00020\u00002\u0006\u0010y\u001a\u00020\u001d2\u0006\u0010\u001a\u001a\u00020/2\u0006\u0010\u001b\u001a\u00020/H\u0016J\u0018\u0010w\u001a\u00020\u00002\u0006\u0010x\u001a\u00020z2\u0006\u0010\u001b\u001a\u00020\fH\u0016J\u0010\u0010{\u001a\u00020\f2\u0006\u0010x\u001a\u00020zH\u0016J\u0010\u0010|\u001a\u00020\u00002\u0006\u00106\u001a\u00020/H\u0016J\u0010\u0010}\u001a\u00020\u00002\u0006\u0010~\u001a\u00020\fH\u0016J\u0010\u0010\u007f\u001a\u00020\u00002\u0006\u0010~\u001a\u00020\fH\u0016J\u0012\u0010\u0080\u0001\u001a\u00020\u00002\u0007\u0010\u0081\u0001\u001a\u00020/H\u0016J\u0012\u0010\u0082\u0001\u001a\u00020\u00002\u0007\u0010\u0081\u0001\u001a\u00020/H\u0016J\u0011\u0010\u0083\u0001\u001a\u00020\u00002\u0006\u0010~\u001a\u00020\fH\u0016J\u0011\u0010\u0084\u0001\u001a\u00020\u00002\u0006\u0010~\u001a\u00020\fH\u0016J\u0012\u0010\u0085\u0001\u001a\u00020\u00002\u0007\u0010\u0086\u0001\u001a\u00020/H\u0016J\u0012\u0010\u0087\u0001\u001a\u00020\u00002\u0007\u0010\u0086\u0001\u001a\u00020/H\u0016J\u001a\u0010\u0088\u0001\u001a\u00020\u00002\u0007\u0010\u0089\u0001\u001a\u00020\u001f2\u0006\u0010^\u001a\u00020_H\u0016J,\u0010\u0088\u0001\u001a\u00020\u00002\u0007\u0010\u0089\u0001\u001a\u00020\u001f2\u0007\u0010\u008a\u0001\u001a\u00020/2\u0007\u0010\u008b\u0001\u001a\u00020/2\u0006\u0010^\u001a\u00020_H\u0016J\u001b\u0010\u008c\u0001\u001a\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001b\u001a\u00020\fH\u0007J\u0012\u0010\u008d\u0001\u001a\u00020\u00002\u0007\u0010\u0089\u0001\u001a\u00020\u001fH\u0016J$\u0010\u008d\u0001\u001a\u00020\u00002\u0007\u0010\u0089\u0001\u001a\u00020\u001f2\u0007\u0010\u008a\u0001\u001a\u00020/2\u0007\u0010\u008b\u0001\u001a\u00020/H\u0016J\u0012\u0010\u008e\u0001\u001a\u00020\u00002\u0007\u0010\u008f\u0001\u001a\u00020/H\u0016R\u0014\u0010\u0006\u001a\u00020\u00008VX\u0096\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0000@\u0000X\u0081\u000e¢\u0006\u0002\n\u0000R&\u0010\r\u001a\u00020\f2\u0006\u0010\u000b\u001a\u00020\f8G@@X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u0091\u0001"}, d2 = {"Lokio/Buffer;", "Lokio/BufferedSource;", "Lokio/BufferedSink;", "", "Ljava/nio/channels/ByteChannel;", "()V", "buffer", "getBuffer", "()Lokio/Buffer;", "head", "Lokio/Segment;", "<set-?>", "", "size", "()J", "setSize$okio", "(J)V", "clear", "", "clone", "close", "completeSegmentByteCount", "copy", "copyTo", "out", "Ljava/io/OutputStream;", "offset", "byteCount", "digest", "Lokio/ByteString;", "algorithm", "", "emit", "emitCompleteSegments", "equals", "", "other", "", "exhausted", "flush", "get", "", "pos", "getByte", "index", "-deprecated_getByte", "hashCode", "", "hmac", "key", "hmacSha1", "hmacSha256", "hmacSha512", "indexOf", "b", "fromIndex", "toIndex", "bytes", "indexOfElement", "targetBytes", "inputStream", "Ljava/io/InputStream;", "isOpen", "md5", "outputStream", "peek", "rangeEquals", "bytesOffset", Constants.READ, "sink", "Ljava/nio/ByteBuffer;", "", "readAll", "Lokio/Sink;", "readAndWriteUnsafe", "Lokio/Buffer$UnsafeCursor;", "unsafeCursor", "readByte", "readByteArray", "readByteString", "readDecimalLong", "readFrom", "input", "forever", "readFully", "readHexadecimalUnsignedLong", "readInt", "readIntLe", "readLong", "readLongLe", "readShort", "", "readShortLe", "readString", "charset", "Ljava/nio/charset/Charset;", "readUnsafe", "readUtf8", "readUtf8CodePoint", "readUtf8Line", "readUtf8LineStrict", "limit", "request", "require", "select", "options", "Lokio/Options;", "sha1", "sha256", "sha512", "-deprecated_size", "skip", "snapshot", "timeout", "Lokio/Timeout;", "toString", "writableSegment", "minimumCapacity", "writableSegment$okio", "write", "source", "byteString", "Lokio/Source;", "writeAll", "writeByte", "writeDecimalLong", "v", "writeHexadecimalUnsignedLong", "writeInt", "i", "writeIntLe", "writeLong", "writeLongLe", "writeShort", "s", "writeShortLe", "writeString", "string", "beginIndex", "endIndex", "writeTo", "writeUtf8", "writeUtf8CodePoint", "codePoint", "UnsafeCursor", "okio"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class Buffer implements BufferedSource, BufferedSink, Cloneable, ByteChannel {
    public Segment head;
    private long size;

    public static /* synthetic */ Buffer copyTo$default(Buffer buffer, Buffer buffer2, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = 0;
        }
        return buffer.copyTo(buffer2, j);
    }

    public static /* synthetic */ Buffer copyTo$default(Buffer buffer, Buffer buffer2, long j, long j2, int i, Object obj) {
        if ((i & 2) != 0) {
            j = 0;
        }
        return buffer.copyTo(buffer2, j, j2);
    }

    public final Buffer copyTo(OutputStream outputStream) throws IOException {
        return copyTo$default(this, outputStream, 0L, 0L, 6, (Object) null);
    }

    public final Buffer copyTo(OutputStream outputStream, long j) throws IOException {
        return copyTo$default(this, outputStream, j, 0L, 4, (Object) null);
    }

    public final UnsafeCursor readAndWriteUnsafe() {
        return readAndWriteUnsafe$default(this, null, 1, null);
    }

    public final UnsafeCursor readUnsafe() {
        return readUnsafe$default(this, null, 1, null);
    }

    public final Buffer writeTo(OutputStream outputStream) throws IOException {
        return writeTo$default(this, outputStream, 0L, 2, null);
    }

    public final void setSize$okio(long j) {
        this.size = j;
    }

    public final long size() {
        return this.size;
    }

    @Override // okio.BufferedSource, okio.BufferedSink
    public Buffer buffer() {
        return this;
    }

    @Override // okio.BufferedSource, okio.BufferedSink
    public Buffer getBuffer() {
        return this;
    }

    @Override // okio.BufferedSink
    public OutputStream outputStream() {
        return new OutputStream() { // from class: okio.Buffer$outputStream$1
            @Override // java.io.OutputStream
            public void write(int b) {
                Buffer.this.writeByte(b);
            }

            @Override // java.io.OutputStream
            public void write(byte[] data, int offset, int byteCount) {
                Intrinsics.checkNotNullParameter(data, "data");
                Buffer.this.write(data, offset, byteCount);
            }

            @Override // java.io.OutputStream, java.io.Flushable
            public void flush() {
            }

            @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            public String toString() {
                return Buffer.this + ".outputStream()";
            }
        };
    }

    @Override // okio.BufferedSink
    public Buffer emitCompleteSegments() {
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer emit() {
        return this;
    }

    @Override // okio.BufferedSource
    public boolean exhausted() {
        return this.size == 0;
    }

    @Override // okio.BufferedSource
    public void require(long byteCount) throws EOFException {
        if (this.size < byteCount) {
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public boolean request(long byteCount) {
        return this.size >= byteCount;
    }

    @Override // okio.BufferedSource
    public BufferedSource peek() {
        return Okio.buffer(new PeekSource(this));
    }

    @Override // okio.BufferedSource
    public InputStream inputStream() {
        return new InputStream() { // from class: okio.Buffer$inputStream$1
            @Override // java.io.InputStream
            public int read() {
                if (Buffer.this.size() <= 0) {
                    return -1;
                }
                byte $this$and$iv = Buffer.this.readByte();
                return $this$and$iv & UByte.MAX_VALUE;
            }

            @Override // java.io.InputStream
            public int read(byte[] sink, int offset, int byteCount) {
                Intrinsics.checkNotNullParameter(sink, "sink");
                return Buffer.this.read(sink, offset, byteCount);
            }

            @Override // java.io.InputStream
            public int available() {
                long a$iv = Buffer.this.size();
                return (int) Math.min(a$iv, Integer.MAX_VALUE);
            }

            @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
            public void close() {
            }

            public String toString() {
                return Buffer.this + ".inputStream()";
            }
        };
    }

    public static /* synthetic */ Buffer copyTo$default(Buffer buffer, OutputStream outputStream, long j, long j2, int i, Object obj) throws IOException {
        long j3;
        long j4 = (i & 2) != 0 ? 0L : j;
        if ((i & 4) != 0) {
            j3 = buffer.size - j4;
        } else {
            j3 = j2;
        }
        return buffer.copyTo(outputStream, j4, j3);
    }

    public final Buffer copyTo(OutputStream out, long offset, long byteCount) throws IOException {
        Intrinsics.checkNotNullParameter(out, "out");
        long offset2 = offset;
        long byteCount2 = byteCount;
        Util.checkOffsetAndCount(this.size, offset2, byteCount2);
        if (byteCount2 == 0) {
            return this;
        }
        Segment s = this.head;
        while (true) {
            Intrinsics.checkNotNull(s);
            if (offset2 >= s.limit - s.pos) {
                offset2 -= s.limit - s.pos;
                s = s.next;
            }
        }
        while (byteCount2 > 0) {
            Intrinsics.checkNotNull(s);
            int pos = (int) (s.pos + offset2);
            int a$iv = (int) Math.min(s.limit - pos, byteCount2);
            out.write(s.data, pos, a$iv);
            byteCount2 -= a$iv;
            offset2 = 0;
            s = s.next;
        }
        return this;
    }

    public final Buffer copyTo(Buffer out, long offset, long byteCount) {
        Intrinsics.checkNotNullParameter(out, "out");
        long offset$iv = offset;
        long byteCount$iv = byteCount;
        Util.checkOffsetAndCount(size(), offset$iv, byteCount$iv);
        if (byteCount$iv != 0) {
            out.setSize$okio(out.size() + byteCount$iv);
            Segment s$iv = this.head;
            while (true) {
                Intrinsics.checkNotNull(s$iv);
                if (offset$iv >= s$iv.limit - s$iv.pos) {
                    offset$iv -= s$iv.limit - s$iv.pos;
                    s$iv = s$iv.next;
                }
            }
            while (byteCount$iv > 0) {
                Intrinsics.checkNotNull(s$iv);
                Segment copy$iv = s$iv.sharedCopy();
                copy$iv.pos += (int) offset$iv;
                copy$iv.limit = Math.min(copy$iv.pos + ((int) byteCount$iv), copy$iv.limit);
                Segment segment = out.head;
                if (segment == null) {
                    copy$iv.prev = copy$iv;
                    copy$iv.next = copy$iv.prev;
                    out.head = copy$iv.next;
                } else {
                    Intrinsics.checkNotNull(segment);
                    Segment segment2 = segment.prev;
                    Intrinsics.checkNotNull(segment2);
                    segment2.push(copy$iv);
                }
                byteCount$iv -= copy$iv.limit - copy$iv.pos;
                offset$iv = 0;
                s$iv = s$iv.next;
            }
        }
        return this;
    }

    public final Buffer copyTo(Buffer out, long offset) {
        Intrinsics.checkNotNullParameter(out, "out");
        return copyTo(out, offset, this.size - offset);
    }

    public static /* synthetic */ Buffer writeTo$default(Buffer buffer, OutputStream outputStream, long j, int i, Object obj) throws IOException {
        if ((i & 2) != 0) {
            j = buffer.size;
        }
        return buffer.writeTo(outputStream, j);
    }

    public final Buffer writeTo(OutputStream out, long byteCount) throws IOException {
        Intrinsics.checkNotNullParameter(out, "out");
        long byteCount2 = byteCount;
        Util.checkOffsetAndCount(this.size, 0L, byteCount2);
        Segment s = this.head;
        while (byteCount2 > 0) {
            Intrinsics.checkNotNull(s);
            int b$iv = (int) Math.min(byteCount2, s.limit - s.pos);
            out.write(s.data, s.pos, b$iv);
            s.pos += b$iv;
            this.size -= b$iv;
            byteCount2 -= b$iv;
            if (s.pos == s.limit) {
                s = s.pop();
                this.head = s;
                SegmentPool.recycle(s);
            }
        }
        return this;
    }

    public final Buffer readFrom(InputStream input) throws IOException {
        Intrinsics.checkNotNullParameter(input, "input");
        readFrom(input, LongCompanionObject.MAX_VALUE, true);
        return this;
    }

    public final Buffer readFrom(InputStream input, long byteCount) throws IOException {
        Intrinsics.checkNotNullParameter(input, "input");
        if (byteCount >= 0) {
            readFrom(input, byteCount, false);
            return this;
        }
        throw new IllegalArgumentException(("byteCount < 0: " + byteCount).toString());
    }

    private final void readFrom(InputStream input, long byteCount, boolean forever) throws IOException {
        long byteCount2 = byteCount;
        while (true) {
            if (byteCount2 > 0 || forever) {
                Segment tail = writableSegment$okio(1);
                int b$iv = 8192 - tail.limit;
                int bytesRead = input.read(tail.data, tail.limit, (int) Math.min(byteCount2, b$iv));
                if (bytesRead == -1) {
                    if (tail.pos == tail.limit) {
                        this.head = tail.pop();
                        SegmentPool.recycle(tail);
                    }
                    if (!forever) {
                        throw new EOFException();
                    }
                    return;
                }
                tail.limit += bytesRead;
                this.size += bytesRead;
                byteCount2 -= bytesRead;
            } else {
                return;
            }
        }
    }

    public final long completeSegmentByteCount() {
        long result$iv = size();
        if (result$iv == 0) {
            return 0L;
        }
        Segment segment = this.head;
        Intrinsics.checkNotNull(segment);
        Segment tail$iv = segment.prev;
        Intrinsics.checkNotNull(tail$iv);
        if (tail$iv.limit < 8192 && tail$iv.owner) {
            result$iv -= tail$iv.limit - tail$iv.pos;
        }
        return result$iv;
    }

    @Override // okio.BufferedSource
    public byte readByte() throws EOFException {
        if (size() != 0) {
            Segment segment$iv = this.head;
            Intrinsics.checkNotNull(segment$iv);
            int pos$iv = segment$iv.pos;
            int limit$iv = segment$iv.limit;
            byte[] data$iv = segment$iv.data;
            int pos$iv2 = pos$iv + 1;
            byte b$iv = data$iv[pos$iv];
            setSize$okio(size() - 1);
            if (pos$iv2 == limit$iv) {
                this.head = segment$iv.pop();
                SegmentPool.recycle(segment$iv);
            } else {
                segment$iv.pos = pos$iv2;
            }
            return b$iv;
        }
        throw new EOFException();
    }

    public final byte getByte(long pos) {
        Util.checkOffsetAndCount(size(), pos, 1L);
        Segment s$iv$iv = this.head;
        if (s$iv$iv == null) {
            Segment s$iv = null;
            Intrinsics.checkNotNull(s$iv);
            return s$iv.data[(int) ((s$iv.pos + pos) - (-1))];
        } else if (size() - pos < pos) {
            long offset$iv$iv = size();
            while (offset$iv$iv > pos) {
                Segment segment = s$iv$iv.prev;
                Intrinsics.checkNotNull(segment);
                s$iv$iv = segment;
                offset$iv$iv -= s$iv$iv.limit - s$iv$iv.pos;
            }
            Intrinsics.checkNotNull(s$iv$iv);
            return s$iv$iv.data[(int) ((s$iv$iv.pos + pos) - offset$iv$iv)];
        } else {
            long offset$iv$iv2 = 0;
            while (true) {
                long nextOffset$iv$iv = (s$iv$iv.limit - s$iv$iv.pos) + offset$iv$iv2;
                if (nextOffset$iv$iv > pos) {
                    Intrinsics.checkNotNull(s$iv$iv);
                    return s$iv$iv.data[(int) ((s$iv$iv.pos + pos) - offset$iv$iv2)];
                }
                Segment s$iv2 = s$iv$iv.next;
                Intrinsics.checkNotNull(s$iv2);
                s$iv$iv = s$iv2;
                offset$iv$iv2 = nextOffset$iv$iv;
            }
        }
    }

    @Override // okio.BufferedSource
    public short readShort() throws EOFException {
        if (size() >= 2) {
            Segment segment$iv = this.head;
            Intrinsics.checkNotNull(segment$iv);
            int pos$iv = segment$iv.pos;
            int limit$iv = segment$iv.limit;
            if (limit$iv - pos$iv < 2) {
                byte $this$and$iv$iv = readByte();
                byte $this$and$iv$iv2 = readByte();
                int s$iv = (($this$and$iv$iv & UByte.MAX_VALUE) << 8) | ($this$and$iv$iv2 & UByte.MAX_VALUE);
                return (short) s$iv;
            }
            byte[] data$iv = segment$iv.data;
            int pos$iv2 = pos$iv + 1;
            byte $this$and$iv$iv3 = data$iv[pos$iv];
            int pos$iv3 = pos$iv2 + 1;
            byte $this$and$iv$iv4 = data$iv[pos$iv2];
            int s$iv2 = (($this$and$iv$iv3 & UByte.MAX_VALUE) << 8) | ($this$and$iv$iv4 & UByte.MAX_VALUE);
            setSize$okio(size() - 2);
            if (pos$iv3 == limit$iv) {
                this.head = segment$iv.pop();
                SegmentPool.recycle(segment$iv);
            } else {
                segment$iv.pos = pos$iv3;
            }
            return (short) s$iv2;
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public int readInt() throws EOFException {
        if (size() >= 4) {
            Segment segment$iv = this.head;
            Intrinsics.checkNotNull(segment$iv);
            int pos$iv = segment$iv.pos;
            int limit$iv = segment$iv.limit;
            if (limit$iv - pos$iv < 4) {
                byte $this$and$iv$iv = readByte();
                byte $this$and$iv$iv2 = readByte();
                int i = (($this$and$iv$iv & UByte.MAX_VALUE) << 24) | (($this$and$iv$iv2 & UByte.MAX_VALUE) << 16);
                byte $this$and$iv$iv3 = readByte();
                int i2 = i | (($this$and$iv$iv3 & UByte.MAX_VALUE) << 8);
                byte $this$and$iv$iv4 = readByte();
                return i2 | ($this$and$iv$iv4 & UByte.MAX_VALUE);
            }
            byte[] data$iv = segment$iv.data;
            int pos$iv2 = pos$iv + 1;
            byte $this$and$iv$iv5 = data$iv[pos$iv];
            int pos$iv3 = pos$iv2 + 1;
            byte $this$and$iv$iv6 = data$iv[pos$iv2];
            int i3 = (($this$and$iv$iv5 & UByte.MAX_VALUE) << 24) | (($this$and$iv$iv6 & UByte.MAX_VALUE) << 16);
            int pos$iv4 = pos$iv3 + 1;
            byte $this$and$iv$iv7 = data$iv[pos$iv3];
            int i4 = i3 | (($this$and$iv$iv7 & UByte.MAX_VALUE) << 8);
            int pos$iv5 = pos$iv4 + 1;
            byte $this$and$iv$iv8 = data$iv[pos$iv4];
            int i$iv = i4 | ($this$and$iv$iv8 & UByte.MAX_VALUE);
            setSize$okio(size() - 4);
            if (pos$iv5 == limit$iv) {
                this.head = segment$iv.pop();
                SegmentPool.recycle(segment$iv);
            } else {
                segment$iv.pos = pos$iv5;
            }
            return i$iv;
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public long readLong() throws EOFException {
        if (size() >= 8) {
            Segment segment$iv = this.head;
            Intrinsics.checkNotNull(segment$iv);
            int pos$iv = segment$iv.pos;
            int limit$iv = segment$iv.limit;
            if (limit$iv - pos$iv < 8) {
                int $this$and$iv$iv = readInt();
                int $this$and$iv$iv2 = readInt();
                long other$iv$iv = 4294967295L & $this$and$iv$iv2;
                return (($this$and$iv$iv & 4294967295L) << 32) | other$iv$iv;
            }
            byte[] data$iv = segment$iv.data;
            int pos$iv2 = pos$iv + 1;
            byte $this$and$iv$iv3 = data$iv[pos$iv];
            long other$iv$iv2 = 255 & $this$and$iv$iv3;
            int pos$iv3 = pos$iv2 + 1;
            byte $this$and$iv$iv4 = data$iv[pos$iv2];
            long j = (($this$and$iv$iv4 & 255) << 48) | (other$iv$iv2 << 56);
            int pos$iv4 = pos$iv3 + 1;
            byte $this$and$iv$iv5 = data$iv[pos$iv3];
            long other$iv$iv3 = 255 & $this$and$iv$iv5;
            long j2 = j | (other$iv$iv3 << 40);
            int pos$iv5 = pos$iv4 + 1;
            byte $this$and$iv$iv6 = data$iv[pos$iv4];
            long other$iv$iv4 = 255 & $this$and$iv$iv6;
            int pos$iv6 = pos$iv5 + 1;
            byte $this$and$iv$iv7 = data$iv[pos$iv5];
            long other$iv$iv5 = 255 & $this$and$iv$iv7;
            long j3 = j2 | (other$iv$iv4 << 32) | (other$iv$iv5 << 24);
            int pos$iv7 = pos$iv6 + 1;
            byte $this$and$iv$iv8 = data$iv[pos$iv6];
            long other$iv$iv6 = 255 & $this$and$iv$iv8;
            long j4 = j3 | (other$iv$iv6 << 16);
            int pos$iv8 = pos$iv7 + 1;
            byte $this$and$iv$iv9 = data$iv[pos$iv7];
            long other$iv$iv7 = 255 & $this$and$iv$iv9;
            long j5 = j4 | (other$iv$iv7 << 8);
            int pos$iv9 = pos$iv8 + 1;
            byte $this$and$iv$iv10 = data$iv[pos$iv8];
            long other$iv$iv8 = 255 & $this$and$iv$iv10;
            long v$iv = j5 | other$iv$iv8;
            setSize$okio(size() - 8);
            if (pos$iv9 == limit$iv) {
                this.head = segment$iv.pop();
                SegmentPool.recycle(segment$iv);
                return v$iv;
            }
            segment$iv.pos = pos$iv9;
            return v$iv;
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public short readShortLe() throws EOFException {
        return Util.reverseBytes(readShort());
    }

    @Override // okio.BufferedSource
    public int readIntLe() throws EOFException {
        return Util.reverseBytes(readInt());
    }

    @Override // okio.BufferedSource
    public long readLongLe() throws EOFException {
        return Util.reverseBytes(readLong());
    }

    /* JADX WARN: Code restructure failed: missing block: B:42:0x00ef, code lost:
        r1.setSize$okio(r1.size() - r4);
     */
    /* JADX WARN: Code restructure failed: missing block: B:43:0x00f8, code lost:
        if (r5 == false) goto L44;
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00fd, code lost:
        return -r2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:58:?, code lost:
        return r2;
     */
    /* JADX WARN: Removed duplicated region for block: B:36:0x00d4  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x00df  */
    @Override // okio.BufferedSource
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long readDecimalLong() throws EOFException {
        /*
            Method dump skipped, instructions count: 267
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.readDecimalLong():long");
    }

    /* JADX WARN: Removed duplicated region for block: B:32:0x00aa  */
    /* JADX WARN: Removed duplicated region for block: B:33:0x00b4  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x00b9  */
    /* JADX WARN: Removed duplicated region for block: B:43:0x00bd A[EDGE_INSN: B:43:0x00bd->B:37:0x00bd ?: BREAK  , SYNTHETIC] */
    @Override // okio.BufferedSource
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct code enable 'Show inconsistent code' option in preferences
    */
    public long readHexadecimalUnsignedLong() throws EOFException {
        /*
            Method dump skipped, instructions count: 210
            To view this dump change 'Code comments level' option to 'DEBUG'
        */
        throw new UnsupportedOperationException("Method not decompiled: okio.Buffer.readHexadecimalUnsignedLong():long");
    }

    @Override // okio.BufferedSource
    public ByteString readByteString() {
        return readByteString(size());
    }

    @Override // okio.BufferedSource
    public ByteString readByteString(long byteCount) throws EOFException {
        if (!(byteCount >= 0 && byteCount <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + byteCount).toString());
        } else if (size() < byteCount) {
            throw new EOFException();
        } else if (byteCount < 4096) {
            return new ByteString(readByteArray(byteCount));
        } else {
            ByteString snapshot = snapshot((int) byteCount);
            skip(byteCount);
            return snapshot;
        }
    }

    @Override // okio.BufferedSource
    public int select(Options options) {
        Intrinsics.checkNotNullParameter(options, "options");
        int index$iv = BufferKt.selectPrefix$default(this, options, false, 2, null);
        if (index$iv == -1) {
            return -1;
        }
        int selectedSize$iv = options.getByteStrings$okio()[index$iv].size();
        skip(selectedSize$iv);
        return index$iv;
    }

    @Override // okio.BufferedSource
    public void readFully(Buffer sink, long byteCount) throws EOFException {
        Intrinsics.checkNotNullParameter(sink, "sink");
        if (size() >= byteCount) {
            sink.write(this, byteCount);
        } else {
            sink.write(this, size());
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public long readAll(Sink sink) throws IOException {
        Intrinsics.checkNotNullParameter(sink, "sink");
        long byteCount$iv = size();
        if (byteCount$iv > 0) {
            sink.write(this, byteCount$iv);
        }
        return byteCount$iv;
    }

    @Override // okio.BufferedSource
    public String readUtf8() {
        return readString(this.size, Charsets.UTF_8);
    }

    @Override // okio.BufferedSource
    public String readUtf8(long byteCount) throws EOFException {
        return readString(byteCount, Charsets.UTF_8);
    }

    @Override // okio.BufferedSource
    public String readString(Charset charset) {
        Intrinsics.checkNotNullParameter(charset, "charset");
        return readString(this.size, charset);
    }

    @Override // okio.BufferedSource
    public String readString(long byteCount, Charset charset) throws EOFException {
        Intrinsics.checkNotNullParameter(charset, "charset");
        if (!(byteCount >= 0 && byteCount <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + byteCount).toString());
        } else if (this.size < byteCount) {
            throw new EOFException();
        } else if (byteCount == 0) {
            return "";
        } else {
            Segment s = this.head;
            Intrinsics.checkNotNull(s);
            if (s.pos + byteCount > s.limit) {
                return new String(readByteArray(byteCount), charset);
            }
            String result = new String(s.data, s.pos, (int) byteCount, charset);
            s.pos += (int) byteCount;
            this.size -= byteCount;
            if (s.pos == s.limit) {
                this.head = s.pop();
                SegmentPool.recycle(s);
            }
            return result;
        }
    }

    @Override // okio.BufferedSource
    public String readUtf8Line() throws EOFException {
        long newline$iv = indexOf((byte) 10);
        if (newline$iv != -1) {
            return BufferKt.readUtf8Line(this, newline$iv);
        }
        if (size() != 0) {
            return readUtf8(size());
        }
        return null;
    }

    @Override // okio.BufferedSource
    public String readUtf8LineStrict() throws EOFException {
        return readUtf8LineStrict(LongCompanionObject.MAX_VALUE);
    }

    @Override // okio.BufferedSource
    public String readUtf8LineStrict(long limit) throws EOFException {
        if (limit >= 0) {
            long scanLength$iv = LongCompanionObject.MAX_VALUE;
            if (limit != LongCompanionObject.MAX_VALUE) {
                scanLength$iv = limit + 1;
            }
            byte b = (byte) 10;
            long newline$iv = indexOf(b, 0L, scanLength$iv);
            if (newline$iv != -1) {
                return BufferKt.readUtf8Line(this, newline$iv);
            }
            if (scanLength$iv < size() && getByte(scanLength$iv - 1) == ((byte) 13) && getByte(scanLength$iv) == b) {
                return BufferKt.readUtf8Line(this, scanLength$iv);
            }
            Buffer data$iv = new Buffer();
            long b$iv$iv = size();
            copyTo(data$iv, 0L, Math.min(32, b$iv$iv));
            throw new EOFException("\\n not found: limit=" + Math.min(size(), limit) + " content=" + data$iv.readByteString().hex() + Typography.ellipsis);
        }
        throw new IllegalArgumentException(("limit < 0: " + limit).toString());
    }

    @Override // okio.BufferedSource
    public int readUtf8CodePoint() throws EOFException {
        int min$iv;
        int byteCount$iv;
        int codePoint$iv;
        if (size() != 0) {
            byte b0$iv = getByte(0L);
            int other$iv$iv = 128 & b0$iv;
            if (other$iv$iv == 0) {
                codePoint$iv = b0$iv & ByteCompanionObject.MAX_VALUE;
                byteCount$iv = 1;
                min$iv = 0;
            } else {
                int other$iv$iv2 = 224 & b0$iv;
                if (other$iv$iv2 == 192) {
                    codePoint$iv = b0$iv & 31;
                    byteCount$iv = 2;
                    min$iv = 128;
                } else {
                    int other$iv$iv3 = 240 & b0$iv;
                    if (other$iv$iv3 == 224) {
                        codePoint$iv = b0$iv & 15;
                        byteCount$iv = 3;
                        min$iv = 2048;
                    } else {
                        int other$iv$iv4 = 248 & b0$iv;
                        if (other$iv$iv4 == 240) {
                            codePoint$iv = b0$iv & 7;
                            byteCount$iv = 4;
                            min$iv = 65536;
                        } else {
                            skip(1L);
                            return Utf8.REPLACEMENT_CODE_POINT;
                        }
                    }
                }
            }
            if (size() >= byteCount$iv) {
                for (int i$iv = 1; i$iv < byteCount$iv; i$iv++) {
                    byte b$iv = getByte(i$iv);
                    int other$iv$iv5 = 192 & b$iv;
                    if (other$iv$iv5 == 128) {
                        int other$iv$iv6 = 63 & b$iv;
                        codePoint$iv = (codePoint$iv << 6) | other$iv$iv6;
                    } else {
                        skip(i$iv);
                        return Utf8.REPLACEMENT_CODE_POINT;
                    }
                }
                skip(byteCount$iv);
                if (codePoint$iv > 1114111) {
                    return Utf8.REPLACEMENT_CODE_POINT;
                }
                if ((55296 <= codePoint$iv && 57343 >= codePoint$iv) || codePoint$iv < min$iv) {
                    return Utf8.REPLACEMENT_CODE_POINT;
                }
                return codePoint$iv;
            }
            throw new EOFException("size < " + byteCount$iv + ": " + size() + " (to read code point prefixed 0x" + Util.toHexString(b0$iv) + ')');
        }
        throw new EOFException();
    }

    @Override // okio.BufferedSource
    public byte[] readByteArray() {
        return readByteArray(size());
    }

    @Override // okio.BufferedSource
    public byte[] readByteArray(long byteCount) throws EOFException {
        if (!(byteCount >= 0 && byteCount <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(("byteCount: " + byteCount).toString());
        } else if (size() >= byteCount) {
            byte[] result$iv = new byte[(int) byteCount];
            readFully(result$iv);
            return result$iv;
        } else {
            throw new EOFException();
        }
    }

    @Override // okio.BufferedSource
    public int read(byte[] sink) {
        Intrinsics.checkNotNullParameter(sink, "sink");
        return read(sink, 0, sink.length);
    }

    @Override // okio.BufferedSource
    public void readFully(byte[] sink) throws EOFException {
        Intrinsics.checkNotNullParameter(sink, "sink");
        int offset$iv = 0;
        while (offset$iv < sink.length) {
            int read$iv = read(sink, offset$iv, sink.length - offset$iv);
            if (read$iv != -1) {
                offset$iv += read$iv;
            } else {
                throw new EOFException();
            }
        }
    }

    @Override // okio.BufferedSource
    public int read(byte[] sink, int offset, int byteCount) {
        Intrinsics.checkNotNullParameter(sink, "sink");
        Util.checkOffsetAndCount(sink.length, offset, byteCount);
        Segment s$iv = this.head;
        if (s$iv == null) {
            return -1;
        }
        int toCopy$iv = Math.min(byteCount, s$iv.limit - s$iv.pos);
        ArraysKt.copyInto(s$iv.data, sink, offset, s$iv.pos, s$iv.pos + toCopy$iv);
        s$iv.pos += toCopy$iv;
        setSize$okio(size() - toCopy$iv);
        if (s$iv.pos != s$iv.limit) {
            return toCopy$iv;
        }
        this.head = s$iv.pop();
        SegmentPool.recycle(s$iv);
        return toCopy$iv;
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer sink) throws IOException {
        Intrinsics.checkNotNullParameter(sink, "sink");
        Segment s = this.head;
        if (s == null) {
            return -1;
        }
        int toCopy = Math.min(sink.remaining(), s.limit - s.pos);
        sink.put(s.data, s.pos, toCopy);
        s.pos += toCopy;
        this.size -= toCopy;
        if (s.pos == s.limit) {
            this.head = s.pop();
            SegmentPool.recycle(s);
        }
        return toCopy;
    }

    public final void clear() {
        skip(size());
    }

    @Override // okio.BufferedSource
    public void skip(long byteCount) throws EOFException {
        long byteCount$iv = byteCount;
        while (byteCount$iv > 0) {
            Segment head$iv = this.head;
            if (head$iv != null) {
                int b$iv$iv = (int) Math.min(byteCount$iv, head$iv.limit - head$iv.pos);
                setSize$okio(size() - b$iv$iv);
                byteCount$iv -= b$iv$iv;
                head$iv.pos += b$iv$iv;
                if (head$iv.pos == head$iv.limit) {
                    this.head = head$iv.pop();
                    SegmentPool.recycle(head$iv);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    @Override // okio.BufferedSink
    public Buffer write(ByteString byteString) {
        Intrinsics.checkNotNullParameter(byteString, "byteString");
        int byteCount$iv = byteString.size();
        byteString.write$okio(this, 0, byteCount$iv);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer write(ByteString byteString, int offset, int byteCount) {
        Intrinsics.checkNotNullParameter(byteString, "byteString");
        byteString.write$okio(this, offset, byteCount);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeUtf8(String string) {
        Intrinsics.checkNotNullParameter(string, "string");
        return writeUtf8(string, 0, string.length());
    }

    @Override // okio.BufferedSink
    public Buffer writeUtf8(String string, int beginIndex, int endIndex) {
        Intrinsics.checkNotNullParameter(string, "string");
        int i = 1;
        if (beginIndex >= 0) {
            if (endIndex >= beginIndex) {
                if (endIndex <= string.length()) {
                    int runSize$iv = beginIndex;
                    while (runSize$iv < endIndex) {
                        int c$iv = string.charAt(runSize$iv);
                        if (c$iv < 128) {
                            Segment tail$iv = writableSegment$okio(i);
                            byte[] data$iv = tail$iv.data;
                            int segmentOffset$iv = tail$iv.limit - runSize$iv;
                            int runLimit$iv = Math.min(endIndex, 8192 - segmentOffset$iv);
                            int i$iv = runSize$iv + 1;
                            int i$iv2 = runSize$iv + segmentOffset$iv;
                            data$iv[i$iv2] = (byte) c$iv;
                            while (i$iv < runLimit$iv) {
                                int c$iv2 = string.charAt(i$iv);
                                if (c$iv2 >= 128) {
                                    break;
                                }
                                data$iv[i$iv + segmentOffset$iv] = (byte) c$iv2;
                                i$iv++;
                            }
                            int runSize$iv2 = (i$iv + segmentOffset$iv) - tail$iv.limit;
                            tail$iv.limit += runSize$iv2;
                            setSize$okio(runSize$iv2 + size());
                            runSize$iv = i$iv;
                        } else if (c$iv < 2048) {
                            Segment tail$iv2 = writableSegment$okio(2);
                            tail$iv2.data[tail$iv2.limit] = (byte) ((c$iv >> 6) | 192);
                            tail$iv2.data[tail$iv2.limit + 1] = (byte) (128 | (c$iv & 63));
                            tail$iv2.limit += 2;
                            setSize$okio(size() + 2);
                            runSize$iv++;
                        } else if (c$iv < 55296 || c$iv > 57343) {
                            Segment tail$iv3 = writableSegment$okio(3);
                            tail$iv3.data[tail$iv3.limit] = (byte) ((c$iv >> 12) | 224);
                            tail$iv3.data[tail$iv3.limit + 1] = (byte) ((63 & (c$iv >> 6)) | 128);
                            tail$iv3.data[tail$iv3.limit + 2] = (byte) ((c$iv & 63) | 128);
                            tail$iv3.limit += 3;
                            setSize$okio(size() + 3);
                            runSize$iv++;
                        } else {
                            int low$iv = runSize$iv + 1 < endIndex ? string.charAt(runSize$iv + 1) : 0;
                            if (c$iv > 56319 || 56320 > low$iv || 57343 < low$iv) {
                                writeByte(63);
                                runSize$iv++;
                            } else {
                                int codePoint$iv = (((c$iv & 1023) << 10) | (low$iv & 1023)) + 65536;
                                Segment tail$iv4 = writableSegment$okio(4);
                                tail$iv4.data[tail$iv4.limit] = (byte) ((codePoint$iv >> 18) | 240);
                                tail$iv4.data[tail$iv4.limit + 1] = (byte) (((codePoint$iv >> 12) & 63) | 128);
                                tail$iv4.data[tail$iv4.limit + 2] = (byte) (((codePoint$iv >> 6) & 63) | 128);
                                tail$iv4.data[tail$iv4.limit + 3] = (byte) (128 | (codePoint$iv & 63));
                                tail$iv4.limit += 4;
                                setSize$okio(size() + 4);
                                runSize$iv += 2;
                            }
                        }
                        i = 1;
                    }
                    return this;
                }
                throw new IllegalArgumentException(("endIndex > string.length: " + endIndex + " > " + string.length()).toString());
            }
            throw new IllegalArgumentException(("endIndex < beginIndex: " + endIndex + " < " + beginIndex).toString());
        }
        throw new IllegalArgumentException(("beginIndex < 0: " + beginIndex).toString());
    }

    @Override // okio.BufferedSink
    public Buffer writeUtf8CodePoint(int codePoint) {
        if (codePoint < 128) {
            writeByte(codePoint);
        } else if (codePoint < 2048) {
            Segment tail$iv = writableSegment$okio(2);
            tail$iv.data[tail$iv.limit] = (byte) ((codePoint >> 6) | 192);
            tail$iv.data[tail$iv.limit + 1] = (byte) (128 | (codePoint & 63));
            tail$iv.limit += 2;
            setSize$okio(size() + 2);
        } else if (55296 <= codePoint && 57343 >= codePoint) {
            writeByte(63);
        } else if (codePoint < 65536) {
            Segment tail$iv2 = writableSegment$okio(3);
            tail$iv2.data[tail$iv2.limit] = (byte) ((codePoint >> 12) | 224);
            tail$iv2.data[tail$iv2.limit + 1] = (byte) ((63 & (codePoint >> 6)) | 128);
            tail$iv2.data[tail$iv2.limit + 2] = (byte) (128 | (codePoint & 63));
            tail$iv2.limit += 3;
            setSize$okio(size() + 3);
        } else if (codePoint <= 1114111) {
            Segment tail$iv3 = writableSegment$okio(4);
            tail$iv3.data[tail$iv3.limit] = (byte) ((codePoint >> 18) | 240);
            tail$iv3.data[tail$iv3.limit + 1] = (byte) (((codePoint >> 12) & 63) | 128);
            tail$iv3.data[tail$iv3.limit + 2] = (byte) (((codePoint >> 6) & 63) | 128);
            tail$iv3.data[tail$iv3.limit + 3] = (byte) (128 | (codePoint & 63));
            tail$iv3.limit += 4;
            setSize$okio(size() + 4);
        } else {
            throw new IllegalArgumentException("Unexpected code point: 0x" + Util.toHexString(codePoint));
        }
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeString(String string, Charset charset) {
        Intrinsics.checkNotNullParameter(string, "string");
        Intrinsics.checkNotNullParameter(charset, "charset");
        return writeString(string, 0, string.length(), charset);
    }

    @Override // okio.BufferedSink
    public Buffer writeString(String string, int beginIndex, int endIndex, Charset charset) {
        Intrinsics.checkNotNullParameter(string, "string");
        Intrinsics.checkNotNullParameter(charset, "charset");
        boolean z = true;
        if (beginIndex >= 0) {
            if (endIndex >= beginIndex) {
                if (endIndex > string.length()) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(("endIndex > string.length: " + endIndex + " > " + string.length()).toString());
                } else if (Intrinsics.areEqual(charset, Charsets.UTF_8)) {
                    return writeUtf8(string, beginIndex, endIndex);
                } else {
                    String substring = string.substring(beginIndex, endIndex);
                    Intrinsics.checkNotNullExpressionValue(substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    if (substring != null) {
                        byte[] data = substring.getBytes(charset);
                        Intrinsics.checkNotNullExpressionValue(data, "(this as java.lang.String).getBytes(charset)");
                        return write(data, 0, data.length);
                    }
                    throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new IllegalArgumentException(("endIndex < beginIndex: " + endIndex + " < " + beginIndex).toString());
            }
        } else {
            throw new IllegalArgumentException(("beginIndex < 0: " + beginIndex).toString());
        }
    }

    @Override // okio.BufferedSink
    public Buffer write(byte[] source) {
        Intrinsics.checkNotNullParameter(source, "source");
        Buffer $this$commonWrite$iv = write(source, 0, source.length);
        return $this$commonWrite$iv;
    }

    @Override // okio.BufferedSink
    public Buffer write(byte[] source, int offset, int byteCount) {
        Intrinsics.checkNotNullParameter(source, "source");
        int offset$iv = offset;
        Util.checkOffsetAndCount(source.length, offset$iv, byteCount);
        int limit$iv = offset$iv + byteCount;
        while (offset$iv < limit$iv) {
            Segment tail$iv = writableSegment$okio(1);
            int toCopy$iv = Math.min(limit$iv - offset$iv, 8192 - tail$iv.limit);
            ArraysKt.copyInto(source, tail$iv.data, tail$iv.limit, offset$iv, offset$iv + toCopy$iv);
            offset$iv += toCopy$iv;
            tail$iv.limit += toCopy$iv;
        }
        setSize$okio(size() + byteCount);
        return this;
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer source) throws IOException {
        Intrinsics.checkNotNullParameter(source, "source");
        int byteCount = source.remaining();
        int remaining = byteCount;
        while (remaining > 0) {
            Segment tail = writableSegment$okio(1);
            int toCopy = Math.min(remaining, 8192 - tail.limit);
            source.get(tail.data, tail.limit, toCopy);
            remaining -= toCopy;
            tail.limit += toCopy;
        }
        this.size += byteCount;
        return byteCount;
    }

    @Override // okio.BufferedSink
    public long writeAll(Source source) throws IOException {
        Intrinsics.checkNotNullParameter(source, "source");
        long totalBytesRead$iv = 0;
        while (true) {
            long readCount$iv = source.read(this, 8192);
            if (readCount$iv == -1) {
                return totalBytesRead$iv;
            }
            totalBytesRead$iv += readCount$iv;
        }
    }

    @Override // okio.BufferedSink
    public Buffer write(Source source, long byteCount) throws IOException {
        Intrinsics.checkNotNullParameter(source, "source");
        long byteCount$iv = byteCount;
        while (byteCount$iv > 0) {
            long read$iv = source.read(this, byteCount$iv);
            if (read$iv != -1) {
                byteCount$iv -= read$iv;
            } else {
                throw new EOFException();
            }
        }
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeByte(int b) {
        Segment tail$iv = writableSegment$okio(1);
        byte[] bArr = tail$iv.data;
        int i = tail$iv.limit;
        tail$iv.limit = i + 1;
        bArr[i] = (byte) b;
        setSize$okio(size() + 1);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeShort(int s) {
        Segment tail$iv = writableSegment$okio(2);
        byte[] data$iv = tail$iv.data;
        int limit$iv = tail$iv.limit;
        int limit$iv2 = limit$iv + 1;
        data$iv[limit$iv] = (byte) ((s >>> 8) & 255);
        data$iv[limit$iv2] = (byte) (s & 255);
        tail$iv.limit = limit$iv2 + 1;
        setSize$okio(size() + 2);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeShortLe(int s) {
        return writeShort((int) Util.reverseBytes((short) s));
    }

    @Override // okio.BufferedSink
    public Buffer writeInt(int i) {
        Segment tail$iv = writableSegment$okio(4);
        byte[] data$iv = tail$iv.data;
        int limit$iv = tail$iv.limit;
        int limit$iv2 = limit$iv + 1;
        data$iv[limit$iv] = (byte) ((i >>> 24) & 255);
        int limit$iv3 = limit$iv2 + 1;
        data$iv[limit$iv2] = (byte) ((i >>> 16) & 255);
        int limit$iv4 = limit$iv3 + 1;
        data$iv[limit$iv3] = (byte) ((i >>> 8) & 255);
        data$iv[limit$iv4] = (byte) (i & 255);
        tail$iv.limit = limit$iv4 + 1;
        setSize$okio(size() + 4);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeIntLe(int i) {
        return writeInt(Util.reverseBytes(i));
    }

    @Override // okio.BufferedSink
    public Buffer writeLong(long v) {
        Segment tail$iv = writableSegment$okio(8);
        byte[] data$iv = tail$iv.data;
        int limit$iv = tail$iv.limit;
        int limit$iv2 = limit$iv + 1;
        data$iv[limit$iv] = (byte) ((v >>> 56) & 255);
        int limit$iv3 = limit$iv2 + 1;
        data$iv[limit$iv2] = (byte) ((v >>> 48) & 255);
        int limit$iv4 = limit$iv3 + 1;
        data$iv[limit$iv3] = (byte) ((v >>> 40) & 255);
        int limit$iv5 = limit$iv4 + 1;
        data$iv[limit$iv4] = (byte) ((v >>> 32) & 255);
        int limit$iv6 = limit$iv5 + 1;
        data$iv[limit$iv5] = (byte) ((v >>> 24) & 255);
        int limit$iv7 = limit$iv6 + 1;
        data$iv[limit$iv6] = (byte) ((v >>> 16) & 255);
        int limit$iv8 = limit$iv7 + 1;
        data$iv[limit$iv7] = (byte) ((v >>> 8) & 255);
        data$iv[limit$iv8] = (byte) (v & 255);
        tail$iv.limit = limit$iv8 + 1;
        setSize$okio(size() + 8);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeLongLe(long v) {
        return writeLong(Util.reverseBytes(v));
    }

    @Override // okio.BufferedSink
    public Buffer writeDecimalLong(long v) {
        int width$iv;
        long v$iv = v;
        if (v$iv == 0) {
            return writeByte(48);
        }
        boolean negative$iv = false;
        if (v$iv < 0) {
            v$iv = -v$iv;
            if (v$iv < 0) {
                return writeUtf8("-9223372036854775808");
            }
            negative$iv = true;
        }
        if (v$iv < 100000000) {
            if (v$iv < 10000) {
                if (v$iv < 100) {
                    width$iv = v$iv < 10 ? 1 : 2;
                } else {
                    width$iv = v$iv < 1000 ? 3 : 4;
                }
            } else if (v$iv < 1000000) {
                width$iv = v$iv < 100000 ? 5 : 6;
            } else {
                width$iv = v$iv < 10000000 ? 7 : 8;
            }
        } else if (v$iv < 1000000000000L) {
            if (v$iv < RealConnection.IDLE_CONNECTION_HEALTHY_NS) {
                width$iv = v$iv < 1000000000 ? 9 : 10;
            } else {
                width$iv = v$iv < 100000000000L ? 11 : 12;
            }
        } else if (v$iv < 1000000000000000L) {
            if (v$iv < 10000000000000L) {
                width$iv = 13;
            } else {
                width$iv = v$iv < 100000000000000L ? 14 : 15;
            }
        } else if (v$iv < 100000000000000000L) {
            width$iv = v$iv < 10000000000000000L ? 16 : 17;
        } else {
            width$iv = v$iv < 1000000000000000000L ? 18 : 19;
        }
        if (negative$iv) {
            width$iv++;
        }
        Segment tail$iv = writableSegment$okio(width$iv);
        byte[] data$iv = tail$iv.data;
        int pos$iv = tail$iv.limit + width$iv;
        while (v$iv != 0) {
            long j = 10;
            int digit$iv = (int) (v$iv % j);
            pos$iv--;
            data$iv[pos$iv] = BufferKt.getHEX_DIGIT_BYTES()[digit$iv];
            v$iv /= j;
        }
        if (negative$iv) {
            data$iv[pos$iv - 1] = (byte) 45;
        }
        tail$iv.limit += width$iv;
        setSize$okio(size() + width$iv);
        return this;
    }

    @Override // okio.BufferedSink
    public Buffer writeHexadecimalUnsignedLong(long v) {
        long v$iv = v;
        if (v$iv == 0) {
            return writeByte(48);
        }
        long x$iv = v$iv | (v$iv >>> 1);
        long x$iv2 = x$iv | (x$iv >>> 2);
        long x$iv3 = x$iv2 | (x$iv2 >>> 4);
        long x$iv4 = x$iv3 | (x$iv3 >>> 8);
        long x$iv5 = x$iv4 | (x$iv4 >>> 16);
        long x$iv6 = x$iv5 | (x$iv5 >>> 32);
        long x$iv7 = x$iv6 - ((x$iv6 >>> 1) & 6148914691236517205L);
        long x$iv8 = ((x$iv7 >>> 2) & 3689348814741910323L) + (3689348814741910323L & x$iv7);
        long x$iv9 = ((x$iv8 >>> 4) + x$iv8) & 1085102592571150095L;
        long x$iv10 = x$iv9 + (x$iv9 >>> 8);
        long x$iv11 = x$iv10 + (x$iv10 >>> 16);
        int width$iv = (int) ((3 + ((x$iv11 & 63) + (63 & (x$iv11 >>> 32)))) / 4);
        Segment tail$iv = writableSegment$okio(width$iv);
        byte[] data$iv = tail$iv.data;
        int start$iv = tail$iv.limit;
        for (int pos$iv = (tail$iv.limit + width$iv) - 1; pos$iv >= start$iv; pos$iv--) {
            data$iv[pos$iv] = BufferKt.getHEX_DIGIT_BYTES()[(int) (15 & v$iv)];
            v$iv >>>= 4;
        }
        tail$iv.limit += width$iv;
        setSize$okio(size() + width$iv);
        return this;
    }

    public final Segment writableSegment$okio(int minimumCapacity) {
        boolean z = true;
        if (minimumCapacity < 1 || minimumCapacity > 8192) {
            z = false;
        }
        if (z) {
            Segment segment = this.head;
            if (segment == null) {
                Segment result$iv = SegmentPool.take();
                this.head = result$iv;
                result$iv.prev = result$iv;
                result$iv.next = result$iv;
                return result$iv;
            }
            Intrinsics.checkNotNull(segment);
            Segment tail$iv = segment.prev;
            Intrinsics.checkNotNull(tail$iv);
            return (tail$iv.limit + minimumCapacity > 8192 || !tail$iv.owner) ? tail$iv.push(SegmentPool.take()) : tail$iv;
        }
        throw new IllegalArgumentException("unexpected capacity".toString());
    }

    @Override // okio.Sink
    public void write(Buffer source, long byteCount) {
        Segment segment;
        Segment tail$iv;
        Intrinsics.checkNotNullParameter(source, "source");
        long byteCount$iv = byteCount;
        if (source != this) {
            Util.checkOffsetAndCount(source.size(), 0L, byteCount$iv);
            while (byteCount$iv > 0) {
                Segment segment2 = source.head;
                Intrinsics.checkNotNull(segment2);
                int i = segment2.limit;
                Intrinsics.checkNotNull(source.head);
                if (byteCount$iv < i - segment.pos) {
                    Segment segment3 = this.head;
                    if (segment3 != null) {
                        Intrinsics.checkNotNull(segment3);
                        tail$iv = segment3.prev;
                    } else {
                        tail$iv = null;
                    }
                    if (tail$iv != null && tail$iv.owner) {
                        if ((tail$iv.limit + byteCount$iv) - (tail$iv.shared ? 0 : tail$iv.pos) <= 8192) {
                            Segment segment4 = source.head;
                            Intrinsics.checkNotNull(segment4);
                            segment4.writeTo(tail$iv, (int) byteCount$iv);
                            source.setSize$okio(source.size() - byteCount$iv);
                            setSize$okio(size() + byteCount$iv);
                            return;
                        }
                    }
                    Segment segment5 = source.head;
                    Intrinsics.checkNotNull(segment5);
                    source.head = segment5.split((int) byteCount$iv);
                }
                Segment segmentToMove$iv = source.head;
                Intrinsics.checkNotNull(segmentToMove$iv);
                long movedByteCount$iv = segmentToMove$iv.limit - segmentToMove$iv.pos;
                source.head = segmentToMove$iv.pop();
                Segment segment6 = this.head;
                if (segment6 == null) {
                    this.head = segmentToMove$iv;
                    segmentToMove$iv.prev = segmentToMove$iv;
                    segmentToMove$iv.next = segmentToMove$iv.prev;
                } else {
                    Intrinsics.checkNotNull(segment6);
                    Segment tail$iv2 = segment6.prev;
                    Intrinsics.checkNotNull(tail$iv2);
                    tail$iv2.push(segmentToMove$iv).compact();
                }
                source.setSize$okio(source.size() - movedByteCount$iv);
                setSize$okio(size() + movedByteCount$iv);
                byteCount$iv -= movedByteCount$iv;
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    @Override // okio.Source
    public long read(Buffer sink, long byteCount) {
        Intrinsics.checkNotNullParameter(sink, "sink");
        long byteCount$iv = byteCount;
        if (!(byteCount$iv >= 0)) {
            throw new IllegalArgumentException(("byteCount < 0: " + byteCount$iv).toString());
        } else if (size() == 0) {
            return -1L;
        } else {
            if (byteCount$iv > size()) {
                byteCount$iv = size();
            }
            sink.write(this, byteCount$iv);
            return byteCount$iv;
        }
    }

    @Override // okio.BufferedSource
    public long indexOf(byte b) {
        return indexOf(b, 0L, LongCompanionObject.MAX_VALUE);
    }

    @Override // okio.BufferedSource
    public long indexOf(byte b, long fromIndex) {
        return indexOf(b, fromIndex, LongCompanionObject.MAX_VALUE);
    }

    @Override // okio.BufferedSource
    public long indexOf(byte b, long fromIndex, long toIndex) {
        long fromIndex$iv = fromIndex;
        long toIndex$iv = toIndex;
        if (0 <= fromIndex$iv && toIndex$iv >= fromIndex$iv) {
            if (toIndex$iv > size()) {
                toIndex$iv = size();
            }
            if (fromIndex$iv == toIndex$iv) {
                return -1L;
            }
            long fromIndex$iv$iv = fromIndex$iv;
            Buffer $this$seek$iv$iv = this;
            int $i$f$seek = 0;
            Segment s$iv$iv = $this$seek$iv$iv.head;
            if (s$iv$iv == null) {
                return -1L;
            }
            if ($this$seek$iv$iv.size() - fromIndex$iv$iv < fromIndex$iv$iv) {
                long offset$iv$iv = $this$seek$iv$iv.size();
                while (offset$iv$iv > fromIndex$iv$iv) {
                    Segment segment = s$iv$iv.prev;
                    Intrinsics.checkNotNull(segment);
                    s$iv$iv = segment;
                    offset$iv$iv -= s$iv$iv.limit - s$iv$iv.pos;
                }
                Segment s$iv = s$iv$iv;
                int i = 0;
                if (s$iv == null) {
                    return -1L;
                }
                long offset$iv = offset$iv$iv;
                Segment s$iv2 = s$iv;
                while (offset$iv < toIndex$iv) {
                    byte[] data$iv = s$iv2.data;
                    int limit$iv = (int) Math.min(s$iv2.limit, (s$iv2.pos + toIndex$iv) - offset$iv);
                    for (int pos$iv = (int) ((s$iv2.pos + fromIndex$iv) - offset$iv); pos$iv < limit$iv; pos$iv++) {
                        if (data$iv[pos$iv] == b) {
                            return (pos$iv - s$iv2.pos) + offset$iv;
                        }
                    }
                    offset$iv += s$iv2.limit - s$iv2.pos;
                    fromIndex$iv = offset$iv;
                    Segment segment2 = s$iv2.next;
                    Intrinsics.checkNotNull(segment2);
                    s$iv2 = segment2;
                    s$iv = s$iv;
                    i = i;
                    $this$seek$iv$iv = $this$seek$iv$iv;
                    $i$f$seek = $i$f$seek;
                    s$iv$iv = s$iv$iv;
                    offset$iv$iv = offset$iv$iv;
                }
                return -1L;
            }
            long offset$iv$iv2 = 0;
            while (true) {
                long nextOffset$iv$iv = (s$iv$iv.limit - s$iv$iv.pos) + offset$iv$iv2;
                if (nextOffset$iv$iv > fromIndex$iv$iv) {
                    break;
                }
                Segment segment3 = s$iv$iv.next;
                Intrinsics.checkNotNull(segment3);
                s$iv$iv = segment3;
                offset$iv$iv2 = nextOffset$iv$iv;
            }
            Segment s$iv3 = s$iv$iv;
            if (s$iv3 == null) {
                return -1L;
            }
            Segment s$iv4 = s$iv3;
            long offset$iv2 = offset$iv$iv2;
            while (offset$iv2 < toIndex$iv) {
                byte[] data$iv2 = s$iv4.data;
                int limit$iv2 = (int) Math.min(s$iv4.limit, (s$iv4.pos + toIndex$iv) - offset$iv2);
                for (int pos$iv2 = (int) ((s$iv4.pos + fromIndex$iv) - offset$iv2); pos$iv2 < limit$iv2; pos$iv2++) {
                    if (data$iv2[pos$iv2] == b) {
                        return (pos$iv2 - s$iv4.pos) + offset$iv2;
                    }
                }
                offset$iv2 += s$iv4.limit - s$iv4.pos;
                fromIndex$iv = offset$iv2;
                Segment segment4 = s$iv4.next;
                Intrinsics.checkNotNull(segment4);
                s$iv4 = segment4;
                s$iv3 = s$iv3;
                offset$iv$iv2 = offset$iv$iv2;
                fromIndex$iv$iv = fromIndex$iv$iv;
            }
            return -1L;
        }
        throw new IllegalArgumentException(("size=" + size() + " fromIndex=" + fromIndex$iv + " toIndex=" + toIndex$iv).toString());
    }

    @Override // okio.BufferedSource
    public long indexOf(ByteString bytes) throws IOException {
        Intrinsics.checkNotNullParameter(bytes, "bytes");
        return indexOf(bytes, 0L);
    }

    @Override // okio.BufferedSource
    public long indexOf(ByteString bytes, long fromIndex) throws IOException {
        byte b0$iv;
        int segmentLimit$iv;
        byte[] targetByteArray$iv;
        Intrinsics.checkNotNullParameter(bytes, "bytes");
        Buffer $this$commonIndexOf$iv = this;
        int segmentLimit$iv2 = 0;
        long fromIndex$iv = fromIndex;
        if (bytes.size() > 0) {
            if (fromIndex$iv >= 0) {
                Buffer $this$seek$iv$iv = $this$commonIndexOf$iv;
                Segment s$iv$iv = $this$seek$iv$iv.head;
                if (s$iv$iv == null) {
                    return -1L;
                }
                if ($this$seek$iv$iv.size() - fromIndex$iv < fromIndex$iv) {
                    long offset$iv$iv = $this$seek$iv$iv.size();
                    while (offset$iv$iv > fromIndex$iv) {
                        Segment segment = s$iv$iv.prev;
                        Intrinsics.checkNotNull(segment);
                        s$iv$iv = segment;
                        offset$iv$iv -= s$iv$iv.limit - s$iv$iv.pos;
                    }
                    Segment s$iv = s$iv$iv;
                    int i = 0;
                    if (s$iv == null) {
                        return -1L;
                    }
                    long offset$iv = offset$iv$iv;
                    byte[] targetByteArray$iv2 = bytes.internalArray$okio();
                    byte b0$iv2 = targetByteArray$iv2[0];
                    int bytesSize$iv = bytes.size();
                    long resultLimit$iv = ($this$commonIndexOf$iv.size() - bytesSize$iv) + 1;
                    Segment s$iv2 = s$iv;
                    while (offset$iv < resultLimit$iv) {
                        byte[] data$iv = s$iv2.data;
                        int $i$f$commonIndexOf = s$iv2.limit;
                        long b$iv$iv = (s$iv2.pos + resultLimit$iv) - offset$iv;
                        long offset$iv$iv2 = $i$f$commonIndexOf;
                        int a$iv$iv = (int) Math.min(offset$iv$iv2, b$iv$iv);
                        for (int pos$iv = (int) ((s$iv2.pos + fromIndex$iv) - offset$iv); pos$iv < a$iv$iv; pos$iv++) {
                            if (data$iv[pos$iv] == b0$iv2 && BufferKt.rangeEquals(s$iv2, pos$iv + 1, targetByteArray$iv2, 1, bytesSize$iv)) {
                                return (pos$iv - s$iv2.pos) + offset$iv;
                            }
                        }
                        offset$iv += s$iv2.limit - s$iv2.pos;
                        fromIndex$iv = offset$iv;
                        Segment segment2 = s$iv2.next;
                        Intrinsics.checkNotNull(segment2);
                        s$iv2 = segment2;
                        segmentLimit$iv2 = segmentLimit$iv2;
                        $this$seek$iv$iv = $this$seek$iv$iv;
                        s$iv = s$iv;
                        i = i;
                        offset$iv$iv = offset$iv$iv;
                    }
                    return -1L;
                }
                long offset$iv$iv3 = 0;
                while (true) {
                    long nextOffset$iv$iv = (s$iv$iv.limit - s$iv$iv.pos) + offset$iv$iv3;
                    if (nextOffset$iv$iv > fromIndex$iv) {
                        break;
                    }
                    Segment segment3 = s$iv$iv.next;
                    Intrinsics.checkNotNull(segment3);
                    s$iv$iv = segment3;
                    offset$iv$iv3 = nextOffset$iv$iv;
                    $this$commonIndexOf$iv = $this$commonIndexOf$iv;
                }
                long offset$iv2 = offset$iv$iv3;
                if (s$iv$iv == null) {
                    return -1L;
                }
                Segment s$iv3 = s$iv$iv;
                long offset$iv3 = offset$iv2;
                byte[] targetByteArray$iv3 = bytes.internalArray$okio();
                byte b0$iv3 = targetByteArray$iv3[0];
                int bytesSize$iv2 = bytes.size();
                long resultLimit$iv2 = ($this$commonIndexOf$iv.size() - bytesSize$iv2) + 1;
                while (offset$iv3 < resultLimit$iv2) {
                    byte[] data$iv2 = s$iv3.data;
                    int a$iv$iv2 = s$iv3.limit;
                    long b$iv$iv2 = (s$iv3.pos + resultLimit$iv2) - offset$iv3;
                    byte[] targetByteArray$iv4 = targetByteArray$iv3;
                    int segmentLimit$iv3 = (int) Math.min(a$iv$iv2, b$iv$iv2);
                    int pos$iv2 = (int) ((s$iv3.pos + fromIndex$iv) - offset$iv3);
                    while (pos$iv2 < segmentLimit$iv3) {
                        if (data$iv2[pos$iv2] == b0$iv3) {
                            targetByteArray$iv = targetByteArray$iv4;
                            if (BufferKt.rangeEquals(s$iv3, pos$iv2 + 1, targetByteArray$iv, 1, bytesSize$iv2)) {
                                return (pos$iv2 - s$iv3.pos) + offset$iv3;
                            }
                            segmentLimit$iv = segmentLimit$iv3;
                            b0$iv = b0$iv3;
                        } else {
                            segmentLimit$iv = segmentLimit$iv3;
                            b0$iv = b0$iv3;
                            targetByteArray$iv = targetByteArray$iv4;
                        }
                        pos$iv2++;
                        targetByteArray$iv4 = targetByteArray$iv;
                        segmentLimit$iv3 = segmentLimit$iv;
                        b0$iv3 = b0$iv;
                    }
                    offset$iv3 += s$iv3.limit - s$iv3.pos;
                    fromIndex$iv = offset$iv3;
                    Segment segment4 = s$iv3.next;
                    Intrinsics.checkNotNull(segment4);
                    s$iv3 = segment4;
                    targetByteArray$iv3 = targetByteArray$iv4;
                    $this$commonIndexOf$iv = $this$commonIndexOf$iv;
                    offset$iv2 = offset$iv2;
                }
                return -1L;
            }
            throw new IllegalArgumentException(("fromIndex < 0: " + fromIndex$iv).toString());
        }
        throw new IllegalArgumentException("bytes is empty".toString());
    }

    @Override // okio.BufferedSource
    public long indexOfElement(ByteString targetBytes) {
        Intrinsics.checkNotNullParameter(targetBytes, "targetBytes");
        return indexOfElement(targetBytes, 0L);
    }

    @Override // okio.BufferedSource
    public long indexOfElement(ByteString targetBytes, long fromIndex) {
        ByteString targetBytes2 = targetBytes;
        Intrinsics.checkNotNullParameter(targetBytes2, "targetBytes");
        Buffer $this$commonIndexOfElement$iv = this;
        long fromIndex$iv = fromIndex;
        if (fromIndex$iv >= 0) {
            Buffer $this$seek$iv$iv = $this$commonIndexOfElement$iv;
            int $i$f$seek = 0;
            Segment s$iv$iv = $this$seek$iv$iv.head;
            if (s$iv$iv == null) {
                return -1L;
            }
            if ($this$seek$iv$iv.size() - fromIndex$iv < fromIndex$iv) {
                long offset$iv$iv = $this$seek$iv$iv.size();
                while (offset$iv$iv > fromIndex$iv) {
                    Segment segment = s$iv$iv.prev;
                    Intrinsics.checkNotNull(segment);
                    s$iv$iv = segment;
                    offset$iv$iv -= s$iv$iv.limit - s$iv$iv.pos;
                }
                Segment s$iv = s$iv$iv;
                int i = 0;
                if (s$iv == null) {
                    return -1L;
                }
                Segment s$iv2 = s$iv;
                long offset$iv = offset$iv$iv;
                int $i$f$commonIndexOfElement = targetBytes.size();
                if ($i$f$commonIndexOfElement == 2) {
                    int b0$iv = targetBytes2.getByte(0);
                    int b1$iv = targetBytes2.getByte(1);
                    while (offset$iv < $this$commonIndexOfElement$iv.size()) {
                        byte[] data$iv = s$iv2.data;
                        int limit$iv = s$iv2.limit;
                        for (int pos$iv = (int) ((s$iv2.pos + fromIndex$iv) - offset$iv); pos$iv < limit$iv; pos$iv++) {
                            int b$iv = data$iv[pos$iv];
                            if (b$iv == b0$iv || b$iv == b1$iv) {
                                return (pos$iv - s$iv2.pos) + offset$iv;
                            }
                        }
                        offset$iv += s$iv2.limit - s$iv2.pos;
                        fromIndex$iv = offset$iv;
                        Segment segment2 = s$iv2.next;
                        Intrinsics.checkNotNull(segment2);
                        s$iv2 = segment2;
                        s$iv = s$iv;
                        b0$iv = b0$iv;
                        i = i;
                        $this$seek$iv$iv = $this$seek$iv$iv;
                        $i$f$seek = $i$f$seek;
                    }
                } else {
                    byte[] targetByteArray$iv = targetBytes.internalArray$okio();
                    while (offset$iv < $this$commonIndexOfElement$iv.size()) {
                        byte[] data$iv2 = s$iv2.data;
                        int pos$iv2 = (int) ((s$iv2.pos + fromIndex$iv) - offset$iv);
                        int limit$iv2 = s$iv2.limit;
                        while (pos$iv2 < limit$iv2) {
                            byte b$iv2 = data$iv2[pos$iv2];
                            for (byte t$iv : targetByteArray$iv) {
                                if (b$iv2 == t$iv) {
                                    return (pos$iv2 - s$iv2.pos) + offset$iv;
                                }
                            }
                            pos$iv2++;
                            fromIndex$iv = fromIndex$iv;
                        }
                        offset$iv += s$iv2.limit - s$iv2.pos;
                        fromIndex$iv = offset$iv;
                        Segment segment3 = s$iv2.next;
                        Intrinsics.checkNotNull(segment3);
                        s$iv2 = segment3;
                        targetByteArray$iv = targetByteArray$iv;
                    }
                }
                return -1L;
            }
            long offset$iv$iv2 = 0;
            while (true) {
                long nextOffset$iv$iv = (s$iv$iv.limit - s$iv$iv.pos) + offset$iv$iv2;
                if (nextOffset$iv$iv > fromIndex$iv) {
                    break;
                }
                Segment segment4 = s$iv$iv.next;
                Intrinsics.checkNotNull(segment4);
                s$iv$iv = segment4;
                offset$iv$iv2 = nextOffset$iv$iv;
                targetBytes2 = targetBytes;
                $this$commonIndexOfElement$iv = $this$commonIndexOfElement$iv;
            }
            Segment s$iv3 = s$iv$iv;
            if (s$iv3 == null) {
                return -1L;
            }
            Segment s$iv4 = s$iv3;
            long offset$iv2 = offset$iv$iv2;
            if (targetBytes.size() == 2) {
                int b0$iv2 = targetBytes2.getByte(0);
                int b1$iv2 = targetBytes2.getByte(1);
                while (offset$iv2 < $this$commonIndexOfElement$iv.size()) {
                    byte[] data$iv3 = s$iv4.data;
                    long offset$iv$iv3 = s$iv4.pos;
                    int limit$iv3 = s$iv4.limit;
                    for (int pos$iv3 = (int) ((offset$iv$iv3 + fromIndex$iv) - offset$iv2); pos$iv3 < limit$iv3; pos$iv3++) {
                        int b$iv3 = data$iv3[pos$iv3];
                        if (b$iv3 == b0$iv2 || b$iv3 == b1$iv2) {
                            return (pos$iv3 - s$iv4.pos) + offset$iv2;
                        }
                    }
                    offset$iv2 += s$iv4.limit - s$iv4.pos;
                    Segment segment5 = s$iv4.next;
                    Intrinsics.checkNotNull(segment5);
                    s$iv4 = segment5;
                    fromIndex$iv = offset$iv2;
                    s$iv3 = s$iv3;
                    offset$iv$iv2 = offset$iv$iv2;
                }
            } else {
                byte[] targetByteArray$iv2 = targetBytes.internalArray$okio();
                while (offset$iv2 < $this$commonIndexOfElement$iv.size()) {
                    byte[] data$iv4 = s$iv4.data;
                    int pos$iv4 = (int) ((s$iv4.pos + fromIndex$iv) - offset$iv2);
                    int limit$iv4 = s$iv4.limit;
                    while (pos$iv4 < limit$iv4) {
                        byte b$iv4 = data$iv4[pos$iv4];
                        int length = targetByteArray$iv2.length;
                        int i2 = 0;
                        while (i2 < length) {
                            byte t$iv2 = targetByteArray$iv2[i2];
                            if (b$iv4 == t$iv2) {
                                return (pos$iv4 - s$iv4.pos) + offset$iv2;
                            }
                            i2++;
                            data$iv4 = data$iv4;
                        }
                        pos$iv4++;
                        $this$commonIndexOfElement$iv = $this$commonIndexOfElement$iv;
                    }
                    offset$iv2 += s$iv4.limit - s$iv4.pos;
                    fromIndex$iv = offset$iv2;
                    Segment segment6 = s$iv4.next;
                    Intrinsics.checkNotNull(segment6);
                    s$iv4 = segment6;
                    $this$commonIndexOfElement$iv = $this$commonIndexOfElement$iv;
                    targetByteArray$iv2 = targetByteArray$iv2;
                }
            }
            return -1L;
        }
        throw new IllegalArgumentException(("fromIndex < 0: " + fromIndex$iv).toString());
    }

    @Override // okio.BufferedSource
    public boolean rangeEquals(long offset, ByteString bytes) {
        Intrinsics.checkNotNullParameter(bytes, "bytes");
        return rangeEquals(offset, bytes, 0, bytes.size());
    }

    @Override // okio.BufferedSource
    public boolean rangeEquals(long offset, ByteString bytes, int bytesOffset, int byteCount) {
        Intrinsics.checkNotNullParameter(bytes, "bytes");
        if (offset < 0 || bytesOffset < 0 || byteCount < 0 || size() - offset < byteCount || bytes.size() - bytesOffset < byteCount) {
            return false;
        }
        for (int i$iv = 0; i$iv < byteCount; i$iv++) {
            if (getByte(i$iv + offset) != bytes.getByte(bytesOffset + i$iv)) {
                return false;
            }
        }
        return true;
    }

    @Override // okio.BufferedSink, okio.Sink, java.io.Flushable
    public void flush() {
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return true;
    }

    @Override // okio.Source, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // okio.Source
    public Timeout timeout() {
        return Timeout.NONE;
    }

    public final ByteString md5() {
        return digest("MD5");
    }

    public final ByteString sha1() {
        return digest("SHA-1");
    }

    public final ByteString sha256() {
        return digest("SHA-256");
    }

    public final ByteString sha512() {
        return digest("SHA-512");
    }

    private final ByteString digest(String algorithm) {
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        Segment head = this.head;
        if (head != null) {
            messageDigest.update(head.data, head.pos, head.limit - head.pos);
            Segment s = head.next;
            Intrinsics.checkNotNull(s);
            while (s != head) {
                messageDigest.update(s.data, s.pos, s.limit - s.pos);
                Segment segment = s.next;
                Intrinsics.checkNotNull(segment);
                s = segment;
            }
        }
        byte[] digest = messageDigest.digest();
        Intrinsics.checkNotNullExpressionValue(digest, "messageDigest.digest()");
        return new ByteString(digest);
    }

    public final ByteString hmacSha1(ByteString key) {
        Intrinsics.checkNotNullParameter(key, "key");
        return hmac("HmacSHA1", key);
    }

    public final ByteString hmacSha256(ByteString key) {
        Intrinsics.checkNotNullParameter(key, "key");
        return hmac("HmacSHA256", key);
    }

    public final ByteString hmacSha512(ByteString key) {
        Intrinsics.checkNotNullParameter(key, "key");
        return hmac("HmacSHA512", key);
    }

    private final ByteString hmac(String algorithm, ByteString key) {
        try {
            Mac mac = Mac.getInstance(algorithm);
            mac.init(new SecretKeySpec(key.internalArray$okio(), algorithm));
            Segment head = this.head;
            if (head != null) {
                mac.update(head.data, head.pos, head.limit - head.pos);
                Segment s = head.next;
                Intrinsics.checkNotNull(s);
                while (s != head) {
                    mac.update(s.data, s.pos, s.limit - s.pos);
                    Segment segment = s.next;
                    Intrinsics.checkNotNull(segment);
                    s = segment;
                }
            }
            byte[] doFinal = mac.doFinal();
            Intrinsics.checkNotNullExpressionValue(doFinal, "mac.doFinal()");
            return new ByteString(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof Buffer) || size() != ((Buffer) other).size()) {
            return false;
        }
        if (size() == 0) {
            return true;
        }
        Segment sa$iv = this.head;
        Intrinsics.checkNotNull(sa$iv);
        Segment sb$iv = ((Buffer) other).head;
        Intrinsics.checkNotNull(sb$iv);
        int posA$iv = sa$iv.pos;
        int posB$iv = sb$iv.pos;
        long pos$iv = 0;
        while (pos$iv < size()) {
            long count$iv = Math.min(sa$iv.limit - posA$iv, sb$iv.limit - posB$iv);
            long i$iv = 0;
            while (i$iv < count$iv) {
                int posA$iv2 = posA$iv + 1;
                int posB$iv2 = posB$iv + 1;
                if (sa$iv.data[posA$iv] != sb$iv.data[posB$iv]) {
                    return false;
                }
                i$iv++;
                posA$iv = posA$iv2;
                posB$iv = posB$iv2;
            }
            if (posA$iv == sa$iv.limit) {
                Segment sa$iv2 = sa$iv.next;
                Intrinsics.checkNotNull(sa$iv2);
                posA$iv = sa$iv2.pos;
                sa$iv = sa$iv2;
            }
            if (posB$iv == sb$iv.limit) {
                Segment sb$iv2 = sb$iv.next;
                Intrinsics.checkNotNull(sb$iv2);
                posB$iv = sb$iv2.pos;
                sb$iv = sb$iv2;
            }
            pos$iv += count$iv;
        }
        return true;
    }

    public int hashCode() {
        Segment s$iv = this.head;
        if (s$iv == null) {
            return 0;
        }
        int result$iv = 1;
        do {
            int limit$iv = s$iv.limit;
            for (int pos$iv = s$iv.pos; pos$iv < limit$iv; pos$iv++) {
                result$iv = (result$iv * 31) + s$iv.data[pos$iv];
            }
            Segment segment = s$iv.next;
            Intrinsics.checkNotNull(segment);
            s$iv = segment;
        } while (s$iv != this.head);
        return result$iv;
    }

    public String toString() {
        return snapshot().toString();
    }

    public final Buffer copy() {
        Buffer result$iv = new Buffer();
        if (size() != 0) {
            Segment head$iv = this.head;
            Intrinsics.checkNotNull(head$iv);
            Segment headCopy$iv = head$iv.sharedCopy();
            result$iv.head = headCopy$iv;
            headCopy$iv.prev = headCopy$iv;
            headCopy$iv.next = headCopy$iv.prev;
            for (Segment s$iv = head$iv.next; s$iv != head$iv; s$iv = s$iv.next) {
                Segment segment = headCopy$iv.prev;
                Intrinsics.checkNotNull(segment);
                Intrinsics.checkNotNull(s$iv);
                segment.push(s$iv.sharedCopy());
            }
            result$iv.setSize$okio(size());
        }
        return result$iv;
    }

    public Buffer clone() {
        return copy();
    }

    public final ByteString snapshot() {
        if (size() <= ((long) Integer.MAX_VALUE)) {
            return snapshot((int) size());
        }
        throw new IllegalStateException(("size > Int.MAX_VALUE: " + size()).toString());
    }

    public final ByteString snapshot(int byteCount) {
        if (byteCount == 0) {
            return ByteString.EMPTY;
        }
        Util.checkOffsetAndCount(size(), 0L, byteCount);
        int offset$iv = 0;
        int segmentCount$iv = 0;
        Segment s$iv = this.head;
        while (offset$iv < byteCount) {
            Intrinsics.checkNotNull(s$iv);
            if (s$iv.limit != s$iv.pos) {
                offset$iv += s$iv.limit - s$iv.pos;
                segmentCount$iv++;
                s$iv = s$iv.next;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        byte[][] segments$iv = new byte[segmentCount$iv];
        int[] directory$iv = new int[segmentCount$iv * 2];
        int offset$iv2 = 0;
        int segmentCount$iv2 = 0;
        Segment s$iv2 = this.head;
        while (offset$iv2 < byteCount) {
            Intrinsics.checkNotNull(s$iv2);
            segments$iv[segmentCount$iv2] = s$iv2.data;
            offset$iv2 += s$iv2.limit - s$iv2.pos;
            directory$iv[segmentCount$iv2] = Math.min(offset$iv2, byteCount);
            directory$iv[segments$iv.length + segmentCount$iv2] = s$iv2.pos;
            s$iv2.shared = true;
            segmentCount$iv2++;
            s$iv2 = s$iv2.next;
        }
        return new SegmentedByteString(segments$iv, directory$iv);
    }

    public static /* synthetic */ UnsafeCursor readUnsafe$default(Buffer buffer, UnsafeCursor unsafeCursor, int i, Object obj) {
        if ((i & 1) != 0) {
            unsafeCursor = new UnsafeCursor();
        }
        return buffer.readUnsafe(unsafeCursor);
    }

    public final UnsafeCursor readUnsafe(UnsafeCursor unsafeCursor) {
        Intrinsics.checkNotNullParameter(unsafeCursor, "unsafeCursor");
        if (unsafeCursor.buffer == null) {
            unsafeCursor.buffer = this;
            unsafeCursor.readWrite = false;
            return unsafeCursor;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    public static /* synthetic */ UnsafeCursor readAndWriteUnsafe$default(Buffer buffer, UnsafeCursor unsafeCursor, int i, Object obj) {
        if ((i & 1) != 0) {
            unsafeCursor = new UnsafeCursor();
        }
        return buffer.readAndWriteUnsafe(unsafeCursor);
    }

    public final UnsafeCursor readAndWriteUnsafe(UnsafeCursor unsafeCursor) {
        Intrinsics.checkNotNullParameter(unsafeCursor, "unsafeCursor");
        if (unsafeCursor.buffer == null) {
            unsafeCursor.buffer = this;
            unsafeCursor.readWrite = true;
            return unsafeCursor;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    @Deprecated(level = DeprecationLevel.ERROR, message = "moved to operator function", replaceWith = @ReplaceWith(expression = "this[index]", imports = {}))
    /* renamed from: -deprecated_getByte  reason: not valid java name */
    public final byte m1452deprecated_getByte(long index) {
        return getByte(index);
    }

    @Deprecated(level = DeprecationLevel.ERROR, message = "moved to val", replaceWith = @ReplaceWith(expression = "size", imports = {}))
    /* renamed from: -deprecated_size  reason: not valid java name */
    public final long m1453deprecated_size() {
        return this.size;
    }

    /* compiled from: Buffer.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u000e\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\bJ\u0006\u0010\u0014\u001a\u00020\bJ\u000e\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\nJ\u000e\u0010\u0017\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00020\b8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u00020\n8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u00020\f8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u00020\b8\u0006@\u0006X\u0087\u000e¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lokio/Buffer$UnsafeCursor;", "Ljava/io/Closeable;", "()V", "buffer", "Lokio/Buffer;", "data", "", "end", "", "offset", "", "readWrite", "", "segment", "Lokio/Segment;", "start", "close", "", "expandBuffer", "minByteCount", "next", "resizeBuffer", "newSize", "seek", "okio"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes2.dex */
    public static final class UnsafeCursor implements Closeable {
        public Buffer buffer;
        public byte[] data;
        public boolean readWrite;
        private Segment segment;
        public long offset = -1;
        public int start = -1;
        public int end = -1;

        public final int next() {
            long j = this.offset;
            Buffer buffer = this.buffer;
            Intrinsics.checkNotNull(buffer);
            if (j != buffer.size()) {
                long j2 = this.offset;
                return seek(j2 == -1 ? 0L : j2 + (this.end - this.start));
            }
            throw new IllegalStateException("no more bytes".toString());
        }

        public final int seek(long offset) {
            long nextOffset;
            Segment next;
            Buffer buffer = this.buffer;
            if (buffer == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (offset < -1 || offset > buffer.size()) {
                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                String format = String.format("offset=%s > size=%s", Arrays.copyOf(new Object[]{Long.valueOf(offset), Long.valueOf(buffer.size())}, 2));
                Intrinsics.checkNotNullExpressionValue(format, "java.lang.String.format(format, *args)");
                throw new ArrayIndexOutOfBoundsException(format);
            } else if (offset == -1 || offset == buffer.size()) {
                this.segment = null;
                this.offset = offset;
                this.data = null;
                this.start = -1;
                this.end = -1;
                return -1;
            } else {
                long min = 0;
                long max = buffer.size();
                Segment head = buffer.head;
                Segment tail = buffer.head;
                Segment segment = this.segment;
                if (segment != null) {
                    long j = this.offset;
                    int i = this.start;
                    Intrinsics.checkNotNull(segment);
                    long segmentOffset = j - (i - segment.pos);
                    if (segmentOffset > offset) {
                        max = segmentOffset;
                        tail = this.segment;
                    } else {
                        min = segmentOffset;
                        head = this.segment;
                    }
                }
                if (max - offset > offset - min) {
                    next = head;
                    nextOffset = min;
                    while (true) {
                        Intrinsics.checkNotNull(next);
                        if (offset < (next.limit - next.pos) + nextOffset) {
                            break;
                        }
                        nextOffset += next.limit - next.pos;
                        next = next.next;
                    }
                } else {
                    next = tail;
                    nextOffset = max;
                    while (nextOffset > offset) {
                        Intrinsics.checkNotNull(next);
                        next = next.prev;
                        Intrinsics.checkNotNull(next);
                        nextOffset -= next.limit - next.pos;
                    }
                }
                if (this.readWrite) {
                    Intrinsics.checkNotNull(next);
                    if (next.shared) {
                        Segment unsharedNext = next.unsharedCopy();
                        if (buffer.head == next) {
                            buffer.head = unsharedNext;
                        }
                        next = next.push(unsharedNext);
                        Segment segment2 = next.prev;
                        Intrinsics.checkNotNull(segment2);
                        segment2.pop();
                    }
                }
                this.segment = next;
                this.offset = offset;
                Intrinsics.checkNotNull(next);
                this.data = next.data;
                this.start = next.pos + ((int) (offset - nextOffset));
                int i2 = next.limit;
                this.end = i2;
                return i2 - this.start;
            }
        }

        public final long resizeBuffer(long newSize) {
            Buffer buffer = this.buffer;
            if (buffer == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (this.readWrite) {
                long oldSize = buffer.size();
                int i = 1;
                if (newSize <= oldSize) {
                    if (newSize < 0) {
                        i = 0;
                    }
                    if (i != 0) {
                        long bytesToSubtract = oldSize - newSize;
                        while (true) {
                            if (bytesToSubtract <= 0) {
                                break;
                            }
                            Segment segment = buffer.head;
                            Intrinsics.checkNotNull(segment);
                            Segment tail = segment.prev;
                            Intrinsics.checkNotNull(tail);
                            int tailSize = tail.limit - tail.pos;
                            if (tailSize > bytesToSubtract) {
                                tail.limit -= (int) bytesToSubtract;
                                break;
                            }
                            buffer.head = tail.pop();
                            SegmentPool.recycle(tail);
                            bytesToSubtract -= tailSize;
                        }
                        this.segment = null;
                        this.offset = newSize;
                        this.data = null;
                        this.start = -1;
                        this.end = -1;
                    } else {
                        throw new IllegalArgumentException(("newSize < 0: " + newSize).toString());
                    }
                } else if (newSize > oldSize) {
                    boolean needsToSeek = true;
                    long bytesToAdd = newSize - oldSize;
                    for (long j = 0; bytesToAdd > j; j = 0) {
                        Segment tail2 = buffer.writableSegment$okio(i);
                        int b$iv = 8192 - tail2.limit;
                        int segmentBytesToAdd = (int) Math.min(bytesToAdd, b$iv);
                        tail2.limit += segmentBytesToAdd;
                        bytesToAdd -= segmentBytesToAdd;
                        if (needsToSeek) {
                            this.segment = tail2;
                            this.offset = oldSize;
                            this.data = tail2.data;
                            this.start = tail2.limit - segmentBytesToAdd;
                            this.end = tail2.limit;
                            needsToSeek = false;
                        }
                        i = 1;
                    }
                }
                buffer.setSize$okio(newSize);
                return oldSize;
            } else {
                throw new IllegalStateException("resizeBuffer() only permitted for read/write buffers".toString());
            }
        }

        public final long expandBuffer(int minByteCount) {
            boolean z = true;
            if (minByteCount > 0) {
                if (minByteCount > 8192) {
                    z = false;
                }
                if (z) {
                    Buffer buffer = this.buffer;
                    if (buffer == null) {
                        throw new IllegalStateException("not attached to a buffer".toString());
                    } else if (this.readWrite) {
                        long oldSize = buffer.size();
                        Segment tail = buffer.writableSegment$okio(minByteCount);
                        int result = 8192 - tail.limit;
                        tail.limit = 8192;
                        buffer.setSize$okio(result + oldSize);
                        this.segment = tail;
                        this.offset = oldSize;
                        this.data = tail.data;
                        this.start = 8192 - result;
                        this.end = 8192;
                        return result;
                    } else {
                        throw new IllegalStateException("expandBuffer() only permitted for read/write buffers".toString());
                    }
                } else {
                    throw new IllegalArgumentException(("minByteCount > Segment.SIZE: " + minByteCount).toString());
                }
            } else {
                throw new IllegalArgumentException(("minByteCount <= 0: " + minByteCount).toString());
            }
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (this.buffer != null) {
                this.buffer = null;
                this.segment = null;
                this.offset = -1L;
                this.data = null;
                this.start = -1;
                this.end = -1;
                return;
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }
    }
}
